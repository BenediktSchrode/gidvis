function cm_data = gray_reversed(m)
% Returns the RGB color values for the standard MATLAB colormap gray, but
% reversed in order, i.e. high values will appear dark/black, whereas low
% values will appear light/white.
%
% Usage:
%     cm_data = gray_reversed(m);
%
% Input:
%     m ... number of colors the output should have
%
% Output:
%     cm_data ... m x 3 matrix of colors
%
% see also: colormap

cm_data = flipud(gray(m));
end