function CheckOutRequiredToolboxes
% Get required Matlab files fList and required Matlab products pList to run
% GIDVis.m 
% Since this call takes a long time, it is not run. The required toolboxes
% are simply written in the cell below instead of running this line.
% [fList, pList] = matlab.codetools.requiredFilesAndProducts('GIDVis.m');
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2018 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Symbolic Math Toolbox (Symbolic_Toolbox): not sure what functions are used from it
% Image Processing Toolbox (Image_Toolbox): imread, imline, impoint, etc.
% Statistics and Machine Learning Toolbox (Statistics_Toolbox): not sure what functions are used from it
% Curve Fitting Toolbox (Curve_Fitting_Toolbox): fit, fittype, etc.
RequiredToolboxes = {'Symbolic_Toolbox', 'Image_Toolbox', ...
    'Statistics_Toolbox', 'Curve_Fitting_Toolbox'};

for iTBX = 1:numel(RequiredToolboxes)
    [stat, ~] = license('checkout', RequiredToolboxes{iTBX});
    fprintf('License checkout for %s: Status %d.\n', RequiredToolboxes{iTBX}, stat);
end