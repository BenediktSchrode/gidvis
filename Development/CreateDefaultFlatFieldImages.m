% This file creates default flat field images for some detectors. Just add
% yours with a similar scheme.
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2020 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% define detectors: Name, detlenx, detlenz, number of single detector
% panels (rows), number of single detector panels (columns)
Dets = {'Pilatus 100K', 487, 195, 1, 1; ...
    'Pilatus 300K', 487, 619, 3, 1; ...
    'Pilatus 300K-W', 1475, 195, 1, 3; ...
    'Pilatus 1M', 981, 1043, 5, 2; ...
    'Pilatus 2M', 1475, 1679, 8, 3};

PanelDelta = [17 7]; % gap between panels (number of rows, number of columns)
PanelSize = [195 487]; % size of one panel (number of rows, number of columns)
MP = ones(PanelSize); % create one panel

% set the edges of the panels to NaN
MP(1, :) = NaN;
MP(end, :) = NaN;
MP(:, 1) = NaN;
MP(:, end) = NaN;

% loop over all the detectors
for iD = 1:size(Dets, 1)
    M = ones(Dets{iD, 3}, Dets{iD, 2});
    PanelNum = [Dets{iD, 4:5}];
    for iP = 1:PanelNum(1)
        for jP = 1:PanelNum(2)
            M((iP-1)*PanelSize(1)+1+(iP-1)*PanelDelta(1):iP*PanelSize(1)+(iP-1)*PanelDelta(1), ...
                (jP-1)*PanelSize(2)+1+(jP-1)*PanelDelta(2):jP*PanelSize(2)+(jP-1)*PanelDelta(2)) = MP;
        end
    end
    
    % use singular or plural form of the words for the file description
    if Dets{iD, 4} == 1
        RowStr = 'row';
    else
        RowStr = 'rows';
    end
    if Dets{iD, 5} == 1
        ColStr = 'column';
    else
        ColStr = 'columns';
    end
    
    % to store as a *.mat file which can be imported to GIDVis, the
    % variables have to be Intensity and FileInfo
    Intensity = M;
    FileInfo = sprintf('Default flat field image for a %s detector (%.0f %s, %.0f %s of single detector panels). This flat field image will set the outermost pixels of each detector panel to NaN. This helps to remove the overshooting intensity values at the edge of each detector panel.', ...
        Dets{iD, 1}, Dets{iD, 4}, RowStr, Dets{iD, 5}, ColStr);

    % create file name
    FN = sprintf('FF_%s_1Additional.mat', strrep(Dets{iD, 1}, ' ', '_'));
    % save the flat field image
    save(fullfile('DefaultFlatFieldImages', FN), 'Intensity', 'FileInfo');
end

%% Rayonix SX165 Detector

% define the detector names
DetNames = {'Rayonix SX165', 'Rayonix SX165 2x2', 'Rayonix SX165 4x4', ...
    'Rayonix SX165 8x8'};
% define how many pixels should be removed from the circular area
% additionally
NumAdditional = 3;

% loop over the detectors
for iD = 1:numel(DetNames)
    % create the detector
    D = XDetector(DetNames{iD});
    
    % create a matrix where all pixels are valid
    M = ones(D.detlenz, D.detlenx);

    % calculate the center of the matrix
    C = [D.detlenx/2 D.detlenz/2];
    % as radius, take the minimum
    R = min(C);
    % subtract the number of additional pixels
    R = R-NumAdditional;

    % create the x and y values of the pixels
    [xq, yq] = meshgrid(1:D.detlenx, 1:D.detlenz);
    % calculate the distance of each pixel to the center of the detector
    Rq = sqrt((xq(:)-C(1)).^2 + (yq(:)-C(2)).^2);
    % find pixels outside the radius
    L = Rq > R;
    % set pixels outside the radius to invalid
    M(L) = NaN;

    % variable holding the pixel data has to be named Intensity
    Intensity = M;
    % create a file description
    FileInfo = sprintf('Default flat field image for a %s detector. Removes pixels outside a radius of %g pixels from the center pixel (%g, %g).', ...
        D.Name, R, C(1), C(2));

    % create file name
    FN = ['FF_', strrep(D.Name, ' ', '_'), '_', num2str(NumAdditional), 'Additional_.mat'];
    % save the flat field image
    save(fullfile('DefaultFlatFieldImages', FN), 'Intensity', 'FileInfo');
end