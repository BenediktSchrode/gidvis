function DeleteSettingsFiles(Path)
% This function deletes Settings.mat and the SettingsLoadProject.mat
% created by the Project Module.
% 
% Usage: 
%     DeleteSettingsFiles
%     DeleteSettingsFiles(Path)
%
% Input:
%     Path ... Deletes settings files in the path specified. If no path is
%              given, this function deletes settings files in the GIDVis
%              root folder and its subdirectories
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin < 1 || isempty(Path)
    % we use the parent parent directory of this file, this should be the
    % directory where GIDVis.m is in
    Path = fileparts(fileparts(mfilename('fullpath')));
    % Settings files are appearing in the subdirectories of /modules/ and next
    % to GIDVis.m
    % find files and folders in /modules/
    F1 = dir(fullfile(Path, 'modules'));
    % find the Settings.mat file next to GIDVis.m
    F2 = dir(fullfile(Path, 'Settings.mat'));
    % concatenate F1 and F2
    F = [F1; F2];
else
    % find all files and directories in Path
    F = dir(Path);
end

% loop over the elements of F (this includes files and directories)
for iF = 1:numel(F)
    % to avoid infinite loops, we have to skip parent directory (..) and
    % the current directory (.)
    if any(strcmp(F(iF).name, {'.', '..'}))
        continue;
    end
    % if the current element is a directory, we recursively call
    % DeleteSettingsFiles with input of this directory
    if F(iF).isdir
        DeleteSettingsFiles(fullfile(F(iF).folder, F(iF).name));
    end
    % if the file name is Settings.mat or SettingsLoadProject.mat, delete
    % the file
    if strcmp(F(iF).name, 'Settings.mat') || strcmp(F(iF).name, 'SettingsLoadProject.mat')
        fprintf('Deleting ''%s''.\n', fullfile(Path, F(iF).name));
        delete(fullfile(F(iF).folder, F(iF).name))
    end
end