% Use this file to create an executable of GIDVis.
% Running this file on a Windows pc results in an executable *.exe file.
% Running this file on a Linux machine results in executable files for
% Linux.
%
% This file
%     1) checks whether the MATLAB version is 2017b,
%     2) adds the GIDVis root directory and all subdirectories to the
%        MATLAB path,
%     3) checks if there is only the 'Demo.mat' beamtime in the 'Beamtimes'
%        directory,
%     4) deletes all settings files in the GIDVis root directory and all
%        subdirectories,
%     5) changes the GIDVis.fig file to have the compilation date in the
%        window title and
%     6) performs the actual compilation process.
%
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2020 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% check MATLAB version
if ~strcmp(version('-release'), '2017b')
    error('GIDVis should be compiled with MATLAB R2017b.')
end

% get the root directory (i.e. where the GIDVis.m file is)
RootDir = fileparts(fileparts(mfilename('fullpath')));

% make sure that all the files in the GIDVis root directory are on the
% MATLAB search path
addpath(genpath(RootDir))

% check the beamtime directory: there should be one file, 'Demo.mat'
BeamtimePath = fullfile(RootDir, 'Beamtimes');
BeamtimeFiles = dir(BeamtimePath);
BeamtimeFiles = BeamtimeFiles(~[BeamtimeFiles.isdir]);
if numel(BeamtimeFiles) ~= 1
    error('There should be exactly one beamtime file (''Demo.mat'') in the ''Beamtimes'' directory.')
else
    Names = {BeamtimeFiles.name}';
    if ~strcmp(Names, 'Demo.mat')
        error('The beamtime file in the ''Beamtimes'' directory should be the file ''Demo.mat''.');
    end
end

% delete all settings files
DeleteSettingsFiles

% get current date and time
CurDat = now;
% get the currently used Matlab version
v = version;

% open the GIDVis main figure in invisible state
f = openfig(fullfile(RootDir, 'GIDVis.fig'), 'invisible');
% set the name of the figure to include the compilation date
set(f, 'Name', ['GIDVis - compiled on ' datestr(CurDat, 1)])
% save the figure
savefig(f, fullfile(RootDir, 'GIDVis.fig'));

% perform the actual compilation
mcc('-m', ... % create a standalone application
    'GIDVis.m', ... % main file
    '-v', ... % display the compilation steps
    '-R', ... % add Matlab runtime options
        ['-startmsg,', sprintf('Starting GIDVis compiled on %s using MATLAB %s.\n\nThis will take some time.', ...
        datestr(CurDat, 1), v)], ... % shows in terminal window when starting
    '-a', 'Beamtimes', ... % add directory Beamtimes
    '-a', 'classes', ... % add directory classes
    '-a', 'Colormaps', ... % add directory Colormaps
    '-a', 'Development', ... % add directory Development
    '-a', 'functions', ... % add directory functions
    '-a', 'modules', ... % add directory modules
    '-a', 'DefaultFlatFieldImages', ... % add directory DefaultFlatFieldImages
    '-a', 'Demo', ... % add directory Demo
	'-a', 'Manual', ... % add directory Manual
    '-a', 'GIDVis_gray.png', ... % add file GIDVis_gray.png
    '-o', ['GIDVis_', datestr(CurDat, 'yyyy_mm_dd')]); % final file name of the exe.

% reset the name of the figure
set(f, 'Name', 'GIDVis')
% save the figure
savefig(f, fullfile(RootDir, 'GIDVis.fig'));
% delete the opened figure
delete(f)