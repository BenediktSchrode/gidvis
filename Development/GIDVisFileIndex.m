% This file creates a file index of the files, functions and classes in the
% GIDVis root directory and its subdirectories (only *.m files). The file
% index is a *.tex file, which can be compiled into a PDF document or
% included into an existing document using LATEX.
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2018 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear
clc

Standalone = false;

% get the GIDVis root directory
P = fileparts(fileparts(mfilename('fullpath')));

% read all files recursively
F = RecursiveDir(P);

% create the *.tex file
ResultFile = fopen(fullfile(P, 'Development', 'FileIndex.tex'), 'w', 'n', ...
    'UTF-8');
% add header to *.tex file if standalone
if Standalone
    fprintf(ResultFile, '\\documentclass{scrartcl}\n\n');
    fprintf(ResultFile, '\\usepackage[utf8]{inputenc}\n');
    fprintf(ResultFile, '\\usepackage{matlab-prettifier}\n');
    fprintf(ResultFile, '\\usepackage{hyperref}\n\n');
    fprintf(ResultFile, '\\begin{document}\n');
    fprintf(ResultFile, '\\title{GIDVis File Index\\footnote{\\url{http://www.if.tugraz.at/amd/GIDVis/}}}\n');
    fprintf(ResultFile, '\\author{Automatically created by \\texttt{GIDVisFileIndex.m}}\n');
    fprintf(ResultFile, '\\date{%s}\n', datestr(now));
    fprintf(ResultFile, '\\maketitle\n\n');
    fprintf(ResultFile, '\\tableofcontents\n\n');
end

% variables to store the last used section and subsection names
LastSection = '';
LastSubSection = '';

% loop over all files
for iF = 1:numel(F)
    File = F(iF);
    % get file extension
    [~, FileName, Ext] = fileparts(File.name);
    if ~strcmp(Ext, '.m') % only take *.m files into account
        continue;
    end
    
    % get the section and subsection names
    if strcmp(File.folder, P)
        % in case the file is in the root, use Root Directory as section
        % name, and the last used sub section name as the subsection name
        C{1} = 'Root Directory';
        C{2} = LastSubSection;
    else
        % extract section and subsection name from the file path
        D = File.folder(numel(P)+2:end);
        C = strsplit(D, filesep);
        if numel(C) == 1
            C{2} = '';
        end
    end
    Section = C{1};
    SubSection = C{2};
    
    % variable defining if a lstlisting environment is open
    LstOpened = false;
    % is the comment line directly after the function/classdef keyword/at
    % the beginning of the file
    DirectlyAfter = true;
    % counter for number of documentation lines
    cnt = 0;
    
    % if section name differs from last used, write a new section and
    % update LastSection
    if ~strcmp(Section, LastSection)
        fprintf(ResultFile, '\\section{%s}\n', strrep(Section, '_', '\_'));
        LastSection = Section;
    end
    % if subsection name differs from last used, write a new subsection and
    % update LastSubSection
    if ~isempty(SubSection) && ~strcmp(SubSection, LastSubSection)
        fprintf(ResultFile, '\\subsection{%s}\n', strrep(SubSection, '_', '\_'));
        LastSubSection = SubSection;
    end
    
    % in case SubSection is empty, the file is in the root. The file name
    % then should be written into the subsection
    if isempty(SubSection)
        fprintf(ResultFile, '\\subsection{%s}\n', strrep(File.name, '_', '\_'));
    else
        % otherwise the file name is written into a subsubsection
        fprintf(ResultFile, '\\subsubsection{%s}\n', strrep(File.name, '_', '\_'));
    end
    fprintf(ResultFile, '\\label{CodeFile:%s}', File.name);
    
    % open the code file
    fid = fopen(fullfile(File.folder, File.name));
    tline = fgetl(fid);
    LineCounter = 1;
    % read line-by-line
    while ischar(tline)
        tlineTrimmed = strtrim(tline); % remove leading/trailing whitespace
        % check for classdef or function keyword or comment in the first
        % line
        if (numel(tlineTrimmed) >= 8 && ...
                (strcmp(tlineTrimmed(1:8), 'classdef') || ...
                strcmp(tlineTrimmed(1:8), 'function'))) || ...
                (LineCounter == 1 && numel(tlineTrimmed) >= 1 && ...
                strcmp(tlineTrimmed(1), '%'))
            % remove _Callback, _CreateFcn, _OutputFcn, .get, .set,
            % _OnCallback, _OffCallback, saveobj, loadobj, reload,
            % _OutputFcn functions from the listing
            if ~contains(tlineTrimmed, {'_Callback', '_CreateFcn', ...
                    '_OutputFcn', 'get.', 'set.', '_OnCallback', ...
                    '_OffCallback', '_OutputFcn'})
                % start lstlisting environment
                fprintf(ResultFile, '\\begin{lstlisting}[style=Matlab-editor,breaklines=true]\n');
                % print current line
                fprintf(ResultFile, '%s\n', tlineTrimmed);
                % a lstlisting environment was opened
                LstOpened = true;
                % the last line was a function, classdef or a comment at
                % the beginning of the file, so set DirectlyAfter to true
                DirectlyAfter = true;
            end
            if strcmp(tlineTrimmed(1), '%')
                % in case the line started with a % add 1 to the
                % documentation line counter
                cnt = cnt + 1;
            end
        elseif numel(tlineTrimmed) >= 1 && strcmp(tlineTrimmed(1), '%') && DirectlyAfter && LstOpened
            % the line is still part of the documentation
            fprintf(ResultFile, '%s\n', tlineTrimmed);
            cnt = cnt + 1;
        else
            % line not part of the documentation anymore
            if LstOpened % if lstlisting environment is opened
                % close environment and reset the boolean variables
                fprintf(ResultFile, '\\end{lstlisting}\n');
                LstOpened = false;
                DirectlyAfter = false;
                % check if documentation for the file was found, otherwise
                % write message
                if cnt == 0
                    fprintf('No documentation found for file %s.\n', fullfile(File.folder, File.name));
                end
            end
        end
        % read next line
        tline = fgetl(fid);
        % update number of lines
        LineCounter = LineCounter + 1;
    end
    
    % close code file
    fclose(fid);
end

% write end of the *.tex file
if Standalone
    fprintf(ResultFile, '\\end{document}');
end
% close *.tex file
fclose(ResultFile);