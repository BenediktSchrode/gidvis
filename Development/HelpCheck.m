% This function checks if there is a help section in the function files.
%
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2020 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% clear command window
clc

% get the parent directory of the Development directory (i.e. the GIDVis
% root directory)
P = fileparts(fileparts(mfilename('fullpath')));
% read out all files including files in subdirectories
F = RecursiveDir(P);

for iF = 1:numel(F)
    % check if it is a *.m file
    [~, ~, Ext] = fileparts(F(iF).name);
    if ~strcmp(Ext, '.m'); continue; end
    
    % skip the ExternalFunctions directory
    if contains(F(iF).folder, 'ExternalFunctions')
        continue;
    end
    
    % initialize the variables holding the values if the line has been
    % found and if the function has an input and output argument
    FoundUsage = false;
    FoundInput = false;
    FoundOutput = false;
    InHeader = true;
    Output = false;
    Input = false;
    
    % open the file
    fid = fopen(fullfile(F(iF).folder, F(iF).name));
    % get first line
    tline = fgetl(fid);
    
    trimmed = strtrim(tline);
    
    if numel(trimmed) < numel('function') || ...
            ~strcmp(trimmed(1:numel('function')), 'function')
        fclose(fid);
        continue;
    elseif numel(trimmed) >= numel('function') && ...
            strcmp(trimmed(1:numel('function')), 'function')
            if contains(trimmed, '=')
                Output = true;
            end
            if ~isempty(regexp(trimmed, '\(\s*\S+.*\)', 'once'))
                Input = true;
            end
            tline = fgetl(fid);
    end
    
    trimmed = strtrim(tline);
    if numel(trimmed) >= 1 && strcmp(trimmed(1), '%')
        InHeader = true;
    else
        InHeader = false;
    end
    
    % loop over lines
    while ischar(tline) && InHeader
        % check if line contains Usage:
        if contains(tline, '%') && contains(tline, 'Usage:')
            FoundUsage = true;
        end
        % check if line contains Input:
        if contains(tline, '%') && contains(tline, 'Input:')
            FoundInput = true;
        end
        % check if line contains Output:
        if contains(tline, '%') && contains(tline, 'Output:')
            FoundOutput = true;
        end
        
        tline = strtrim(fgetl(fid));
        if numel(tline) >= 1 && strcmp(tline(1), '%')
            InHeader = true;
        else
            InHeader = false;
        end
    end
    fclose(fid);
    
    MsgPrint = false;
    % print messages if something is missing or appearing multiple times
    if (Input || Output) && ~FoundUsage
        fprintf('Could not find ''Usage:'' for %s.\n', F(iF).name);
        MsgPrint = true;
    end
    if Input && ~FoundInput
        fprintf('Could not find ''Input:'' for %s.\n', F(iF).name);
        MsgPrint = true;
    end
    if Output && ~FoundOutput
        fprintf('Could not find ''Output:'' for %s.\n', F(iF).name);
        MsgPrint = true;
    end
    if MsgPrint
        fprintf('\n');
    end
end