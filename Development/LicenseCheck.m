% This function checks if there is a 
%     see also: GIDVisLicense
% and
%     the license text
% in each of the *.m files.
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2020 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% clear command window
clc

% get the parent directory of the Development directory (i.e. the GIDVis
% root directory)
P = fileparts(fileparts(mfilename('fullpath')));
% read out all files including files in subdirectories
F = RecursiveDir(P);

for iF = 1:numel(F)
    % check if it is a *.m file
    [~, ~, Ext] = fileparts(F(iF).name);
    if ~strcmp(Ext, '.m'); continue; end
    
    % skip the ExternalFunctions directory and the Colormaps directory
    if contains(F(iF).folder, 'ExternalFunctions') || ...
        contains(F(iF).folder, 'Colormaps')
        continue;
    end
    
    % initialize the variables holding the values if the line has been
    % found
    FoundSeeAlsoLicense = false;
    FoundLicense = false;
    SeeAlsoCnt = 0;
    
    % open the file
    fid = fopen(fullfile(F(iF).folder, F(iF).name));
    % get first line
    tline = fgetl(fid);
    % loop over lines
    while ischar(tline) && ~(FoundSeeAlsoLicense && FoundLicense)
        % check if line contains see also: and GIDVisLicense
        if contains(tline, '%') && contains(tline, 'see also:')
            while numel(tline) > 1 && ~FoundSeeAlsoLicense
                if contains(tline, 'GIDVisLicense')
                    FoundSeeAlsoLicense = true;
                else
                    tline = strtrim(fgetl(fid));
                end
            end
        end
        % check if license contains the license text
        if contains(tline, '%') && contains(tline, 'Copyright (C) 20')
            FoundLicense = true;
        end
        % check how many see also: there are
        if contains(tline, '%') && contains(tline, 'see also:')
            SeeAlsoCnt = SeeAlsoCnt + 1;
        end
        
        tline = fgetl(fid);
    end
    fclose(fid);
    
    MsgPrint = false;
    % print messages if something is missing or appearing multiple times
    if ~FoundSeeAlsoLicense
        fprintf('Could not find ''see also'' in combination with ''GIDVisLicense'' for %s.\n', F(iF).name);
        MsgPrint = true;
    end
    if ~FoundLicense
        fprintf('Could not find license text for %s.\n', F(iF).name);
        MsgPrint = true;
    end
    if SeeAlsoCnt > 1
        fprintf('Found multiple ''see also'' for %s.\n', F(iF).name);
        MsgPrint = true;
    end
    if MsgPrint
        fprintf('\n');
    end
end