% Unit tests for the function ApplyReflectionConditions
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2020 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% create a GIDVis_Crystal and a matrix of HKL indices
Cr = GIDVis_Crystal;
HKL = Cr.CreateHKLCombinations(-6:6);

% Apply a reflection condition
HKLR1 = ApplyReflectionConditions(HKL, '00L: L = 5*n');
% Find rows which are affected by the reflection condition
L = HKLR1(:, 1) == 0 & HKLR1(:, 2) == 0;
% check if their l index is a multiple of 5
assert(all(mod(HKLR1(L, 3), 5) == 0))

% Apply the same reflection condition but with multiple blanks in the
% expression
HKLR1_2 = ApplyReflectionConditions(HKL, '0  0 L   : L   =  5  *n  ');
% Find rows which are affected by the reflection condition
L = HKLR1_2(:, 1) == 0 & HKLR1_2(:, 2) == 0;
% check if their l index is a multiple of 5
assert(all(mod(HKLR1_2(L, 3), 5) == 0))
% the result should be exactly the same as for HKLR1
assert(all(all(HKLR1 == HKLR1_2)))

% Apply a reflection condition
HKLR2 = ApplyReflectionConditions(HKL, 'HKL: -H+K+L = 3*n');
% check outcome (-H+K+L should be a multiple of 3)
assert(all(mod(-HKLR2(:, 1) + HKLR2(:, 2) + HKLR2(:, 3), 3) == 0))

% Apply a reflection condition
HKLR3 = ApplyReflectionConditions(HKL, 'HHL: L = 3*n');
% find rows which are affected by the reflection condition
L = HKLR3(:, 1) == HKLR3(:, 2);
% check outcome (l index should be a multiple of three for these rows)
assert(all(mod(HKLR3(L, 3), 3) == 0))

% Apply multiple reflection conditions at once
HKLR4 = ApplyReflectionConditions(HKL, ...
  {'00L: L = 2*n' 'HKL: -H+K+L=3*n' 'HHL: L = 3*n'});
% find rows which are affected by the first reflection condition
L = HKLR4(:, 1) == 0 & HKLR4(:, 2) == 0;
% check if their l index is a multiple of 2
assert(all(mod(HKLR4(L, 3), 2) == 0))
% check outcome of second reflection condition (-H+K+L should be a multiple
% of 3)
assert(all(mod(-HKLR4(:, 1) + HKLR4(:, 2) + HKLR4(:, 3), 3) == 0))
% find rows which are affected by the third reflection condition
L = HKLR4(:, 1) == HKLR4(:, 2);
% check outcome (l index should be a multiple of three for these rows)
assert(all(mod(HKLR4(L, 3), 3) == 0))