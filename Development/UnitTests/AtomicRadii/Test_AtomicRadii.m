% testing of the AtomicRadii function
%
% This file is part of GIDVis.
% 
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% Dimension test
M = AtomicRadii('H');
assert(all(size(M) == [1 1]))

M = AtomicRadii({'H'});
assert(all(size(M) == [1 1]))

M = AtomicRadii({'H', 'He', 'Li', 'Be'});
assert(all(size(M) == [4 1]))

M = AtomicRadii({'H', 'He', 'Li', 'Be'}');
assert(all(size(M) == [4 1]))

M = AtomicRadii({'H', 'He'; 'Li', 'Be'});
assert(all(size(M) == [4 1]))

%% Value test
% load the expected result
S = load('ExpectedAtomicRadii.mat');
ExpectedResult = S.ExpectedResult;

for ind = 1:size(ExpectedResult, 1)
    % create the element
    Element = ChEl(ExpectedResult{ind, 1});
    % get the radius
    Rad = Element.Radius;
    % compare
    assert(abs(Rad-ExpectedResult{ind, 2}) < 1e-8, 'Failed radius of element %s. Expected: %g. Actual value: %g.', ExpectedResult{ind, 1}, ExpectedResult{ind, 2}, Rad)
end