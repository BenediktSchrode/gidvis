% testing of ChEl
%
% This file is part of GIDVis.
% 
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Main test
q = linspace(0, 3, 300);

A = ChEl('H');
assert(numel(A) == 1)
b = aff(A, q);
assert(all(size(b) == [numel(q) numel(A)]));
c = f2(A, 8000);
assert(all(size(c) == size(A)))
c = f2(A, 8000, 'nearest');
assert(all(size(c) == size(A)))

A = ChEl.H;
assert(numel(A) == 1)
b = aff(A, q);
assert(all(size(b) == [numel(q) numel(A)]));
c = f2(A, 8000);
assert(all(size(c) == size(A)))
c = f2(A, 8000, 'nearest');
assert(all(size(c) == size(A)))

A = ChEl({'H', 'He', 'Li', 'Be'});
assert(all(size(A) == [1 4]))
b = aff(A, q);
assert(all(size(b) == [numel(q) numel(A)]));
c = f2(A, 8000);
assert(all(size(c) == size(A)))
c = f2(A, 8000, 'nearest');
assert(all(size(c) == size(A)))

A = ChEl({'H', 'He', 'Li', 'Be'}');
assert(all(size(A) == [4 1]))
b = aff(A, q);
assert(all(size(b) == [numel(q) numel(A)]));
c = f2(A, 8000);
assert(all(size(c) == size(A)))
c = f2(A, 8000, 'nearest');
assert(all(size(c) == size(A)))

A = ChEl({'H', 'He'; 'Li', 'Be'; 'B', 'C'});
assert(all(size(A) == [3 2]))
b = aff(A, q);
assert(all(size(b) == [numel(q) numel(A)]));
c = f2(A, 8000);
assert(all(size(c) == size(A)))
c = f2(A, 8000, 'nearest');
assert(all(size(c) == size(A)))