% testing of the ColorCode function
%
% This file is part of GIDVis.
% 
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Dimension and input test
ColorCode

M = ColorCode('H');
assert(all(size(M) == [1 3]))

M = ColorCode({'H'});
assert(all(size(M) == [1 3]))

M = ColorCode({'H', 'He', 'Li', 'Be'});
assert(all(size(M) == [4 3]))

M = ColorCode({'H', 'He', 'Li', 'Be'}');
assert(all(size(M) == [4 3]))

M = ColorCode({'H', 'He'; 'Li', 'Be'});
assert(all(size(M) == [4 3]))

%% Value test
% load the expected result
S = load('ExpectedColors.mat');
ExpectedResult = S.ExpectedResult;

for ind = 1:size(ExpectedResult, 1)
    % get the color
    Col = ColorCode(ExpectedResult{ind, 1});
    % compare
    assert(abs(Col(1)-ExpectedResult{ind, 2}) < 1e-8, 'Failed R component of element %s', ExpectedResult{ind, 1})
    assert(abs(Col(2)-ExpectedResult{ind, 3}) < 1e-8, 'Failed G component of element %s', ExpectedResult{ind, 1})
    assert(abs(Col(3)-ExpectedResult{ind, 4}) < 1e-8, 'Failed B component of element %s', ExpectedResult{ind, 1})
end