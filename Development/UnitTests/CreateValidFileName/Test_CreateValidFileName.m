% Unit tests for the function CreateValidFileName
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2018 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

FN = 'TestFileName.txt';
[Str, Defaulted] = CreateValidFileName(FN);
assert(strcmp(Str, FN));
assert(Defaulted == false);

FN = 'TestFi<leName?".txt';
[Str, Defaulted] = CreateValidFileName(FN);
assert(strcmp(Str, 'TestFileName.txt'));
assert(Defaulted == false);

FN = 'a�.txt';
[Str, Defaulted] = CreateValidFileName(FN);
assert(strcmp(Str, 'aOe.txt'));
assert(Defaulted == false);

FN = '?.txt';
[Str, Defaulted] = CreateValidFileName(FN, 'Default');
assert(strcmp(Str, 'Default.txt'));
assert(Defaulted == true);

FN = 'COM1.txt';
[Str, Defaulted] = CreateValidFileName(FN, 'Default');
assert(strcmp(Str, 'Default.txt'));
assert(Defaulted == true);