% Testing of Crystal_Peak_New.m
%
% This test tests all the methods of Crystal_Peak_New and setting and
% getting all properites of Crystal_Peak_New. Additionally, it will test
% loading of saved Crystal_Peak_New variables. During the testing process,
% several figures will be opened. The content of these figures is NOT
% tested if it matches a reference.
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Main Test
% context menu after changeparent not working
f = figure;
ax1 = axes('Parent', f);
title(ax1, 'Old Figure');
Cr = GIDVis_Crystal('TestStructure.res');
obj = Crystal_Peak_New(ax1, Cr, [1 1 1; 1 0 0; 0 1 0; 3 -3 4], [1 1 1]);

ChangeCrystal(obj, GIDVis_Crystal('TestStructure.res'));

ChangeHKL(obj, [1 2 3; -1 1 1; 0 -1 0; 3 -3 4]);

ChangeOrientation(obj, [1 0 0]);

ChangePlusMinusqxy(obj, -1);

ChangeSFMultiplier(obj, 0.02)

ChangeSFProportionality(obj, 'radius')

ChangeSpace(obj, 'polar')
ChangeSpace(obj, 'q')

xlim(ax1, [-10 10])
ylim(ax1, [-10 10])

f = figure;
ax2 = axes('Parent', f);
title(ax2, 'New Figure');
ChangeParent(obj, ax2)
xlim(ax2, [-10, 10])
ylim(ax2, [-10, 10])
axis(ax2, 'equal')

% properties
obj.BraggVisible = 'off';
obj.BraggVisible = 'on';
obj.BraggVisible = {'on', 'off', 'on', 'on'};

obj.BraggSize = [1 2 3 4];
obj.BraggSize = 7;

obj.BraggEdgeColor = 'b';
obj.BraggEdgeColor = {'r', 'r', 'b', 'k'};
obj.BraggEdgeColor = [0 1 0];
obj.BraggEdgeColor = {[0 1 0] [1 0 0] [0 1 1] [1 1 0]};

obj.BraggFaceColor = 'r';
obj.BraggFaceColor = {'r', 'r', 'b', 'k'};
obj.BraggFaceColor = [1 0 0];
obj.BraggFaceColor = {[0 1 0] [1 0 0] [0 1 1] [1 1 0]};

obj.BraggMarker = '+';
obj.BraggMarker = {'o', '<', '>', 'p'};

obj.DSVisible = 'on';
obj.DSVisible = 'off';
obj.DSVisible = {'on' 'off' 'on' 'on'};

obj.DSLineWidth = 2;
obj.DSLineWidth = [1 2 3 4];

obj.DSLineStyle = '--';
obj.DSLineStyle = {'--', ':', '-', '-.'};

obj.DSColor = 'r';
obj.DSColor = {'r', 'r', 'b', 'k'};
obj.DSColor = [1 0 0];
obj.DSColor = {[0 1 0] [1 0 0] [0 1 1] [1 1 0]};

obj.LabelVisible = {'on' 'off' 'off' 'on'};
obj.LabelVisible = 'on';

obj.LabelForeColor = 'r';
obj.LabelForeColor = {'r', 'r', 'b', 'k'};
obj.LabelForeColor = {[0 1 0] [1 0 0] [0 1 1] [1 1 0]};
obj.LabelForeColor = [0 0 0];

obj.LabelBackgroundColor = 'r';
obj.LabelBackgroundColor = {'r', 'r', 'b', 'k'};
obj.LabelBackgroundColor = [0.3 0.3 0.3];
obj.LabelBackgroundColor = {[0 1 0] [1 0 0] [0 1 1] [1 1 0]};

obj.LabelFontSize = [5 7 9 11];
obj.LabelFontSize = 8;

obj.LabelFontWeight = 'bold';
obj.LabelFontWeight = {'normal', 'bold', 'normal' 'bold'};

obj.LabelFontAngle = 'italic';
obj.LabelFontAngle = {'italic' 'normal' 'normal' 'italic'};

obj.LabelFontName = 'FixedWidth';
obj.LabelFontName = {'FixedWidth' 'Arial' 'Arial' 'Arial'};

obj.SFVisible = 'on';
obj.SFVisible = 'off';
obj.SFVisible = {'on' 'on' 'off' 'on'};

obj.SFLineWidth = 2;
obj.SFLineWidth = [1 2 3 4];

obj.SFLineStyle = '-';
obj.SFLineStyle = {'--', ':', '-', '-.'};

obj.SFColor = 'r';
obj.SFColor = {'r', 'r', 'b', 'k'};
obj.SFColor = [0 0 1];
obj.SFColor = {[1 1 0] [1 0 0] [0 1 1] [0 1 0]};

obj.BraggPeaksMirrored = true;

obj.OverallVisibility = 'off';
obj.OverallVisibility = 'on';

obj.LabelOffset = [1 1];

obj.LabelLocation = 'north';
obj.LabelLocation = {'north' 'southeast' 'west' 'east'};

obj.Name = 'New Name';

obj.IncludeSampleRotation = true;
obj.IncludeSampleRotation = false;

Props = properties(obj);
for iP = 1:numel(Props)
    obj.(Props{iP});
end
    
%% Loading Test
p = mfilename('fullpath');
directory = fileparts(p);

f = figure;
ax1 = axes('Parent', f);
load(fullfile(directory, 'Cu(OH)2.mat'));
DataView(SaveCrystal{1})
DataView(SaveCrystal{2})

f = figure;
ax2 = axes('Parent', f);
load(fullfile(directory, 'Default Crystal.mat'));
DataView(SaveCrystal{1})
DataView(SaveCrystal{2})

f = figure;
ax3 = axes('Parent', f);
load(fullfile(directory, 'KAKSUL.mat'));
DataView(SaveCrystal{1})
DataView(SaveCrystal{2})

obj = SaveCrystal{2};

ChangeCrystal(obj, GIDVis_Crystal('TestStructure.res'));

ChangeHKL(obj, [1 2 3; -1 1 1; 0 -1 0; 3 -3 4]);

ChangeOrientation(obj, [1 0 0]);

ChangePlusMinusqxy(obj, -1);

ChangeSFMultiplier(obj, 0.02)

ChangeSFProportionality(obj, 'radius')

ChangeSpace(obj, 'polar')
ChangeSpace(obj, 'q')

xlim(ax1, [-10 10])
ylim(ax1, [-10 10])

f = figure;
ax2 = axes('Parent', f);
title(ax2, 'New Figure');
ChangeParent(obj, ax2)
xlim(ax2, [-10, 10])
ylim(ax2, [-10, 10])
axis(ax2, 'equal')

% properties
obj.BraggVisible = 'off';
obj.BraggVisible = 'on';
obj.BraggVisible = {'on', 'off', 'on', 'on'};

obj.BraggSize = [1 2 3 4];
obj.BraggSize = 7;

obj.BraggEdgeColor = 'b';
obj.BraggEdgeColor = {'r', 'r', 'b', 'k'};
obj.BraggEdgeColor = [0 1 0];
obj.BraggEdgeColor = {[0 1 0] [1 0 0] [0 1 1] [1 1 0]};

obj.BraggFaceColor = 'r';
obj.BraggFaceColor = {'r', 'r', 'b', 'k'};
obj.BraggFaceColor = [1 0 0];
obj.BraggFaceColor = {[0 1 0] [1 0 0] [0 1 1] [1 1 0]};

obj.BraggMarker = '+';
obj.BraggMarker = {'o', '<', '>', 'p'};

obj.DSVisible = 'on';
obj.DSVisible = 'off';
obj.DSVisible = {'on' 'off' 'on' 'on'};

obj.DSLineWidth = 2;
obj.DSLineWidth = [1 2 3 4];

obj.DSLineStyle = '--';
obj.DSLineStyle = {'--', ':', '-', '-.'};

obj.DSColor = 'r';
obj.DSColor = {'r', 'r', 'b', 'k'};
obj.DSColor = [1 0 0];
obj.DSColor = {[0 1 0] [1 0 0] [0 1 1] [1 1 0]};

obj.LabelVisible = {'on' 'off' 'off' 'on'};
obj.LabelVisible = 'on';

obj.LabelForeColor = 'r';
obj.LabelForeColor = {'r', 'r', 'b', 'k'};
obj.LabelForeColor = {[0 1 0] [1 0 0] [0 1 1] [1 1 0]};
obj.LabelForeColor = [0 0 0];

obj.LabelBackgroundColor = 'r';
obj.LabelBackgroundColor = {'r', 'r', 'b', 'k'};
obj.LabelBackgroundColor = [0.3 0.3 0.3];
obj.LabelBackgroundColor = {[0 1 0] [1 0 0] [0 1 1] [1 1 0]};

obj.LabelFontSize = [5 7 9 11];
obj.LabelFontSize = 8;

obj.LabelFontWeight = 'bold';
obj.LabelFontWeight = {'normal', 'bold', 'normal' 'bold'};

obj.LabelFontAngle = 'italic';
obj.LabelFontAngle = {'italic' 'normal' 'normal' 'italic'};

obj.LabelFontName = 'FixedWidth';
obj.LabelFontName = {'FixedWidth' 'Arial' 'Arial' 'Arial'};

obj.SFVisible = 'on';
obj.SFVisible = 'off';
obj.SFVisible = {'on' 'on' 'off' 'on'};

obj.SFLineWidth = 2;
obj.SFLineWidth = [1 2 3 4];

obj.SFLineStyle = '-';
obj.SFLineStyle = {'--', ':', '-', '-.'};

obj.SFColor = 'r';
obj.SFColor = {'r', 'r', 'b', 'k'};
obj.SFColor = [0 0 1];
obj.SFColor = {[1 1 0] [1 0 0] [0 1 1] [0 1 0]};

obj.BraggPeaksMirrored = true;

obj.OverallVisibility = 'off';
obj.OverallVisibility = 'on';

obj.LabelOffset = [1 1];

obj.LabelLocation = 'north';
obj.LabelLocation = {'north' 'southeast' 'west' 'east'};

obj.Name = 'New Name';

obj.IncludeSampleRotation = true;
obj.IncludeSampleRotation = false;

Props = properties(obj);
for iP = 1:numel(Props)
    obj.(Props{iP});
end