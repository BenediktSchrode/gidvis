% testing of the ExperimentalSetUp class
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2018 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Test 1: getDetectorPixel
% simple test for a detector 1 pixel high, 1 pixel wide at 0 sdd
Exp = ExperimentalSetUp('Test', 0, ... %sdd
    1, 1, ... % psx psz
    1, 1, ... % detlenx detlenz
    1, 1, ... % cpx cpz
    1, ... % lambda
    0, 0, 0, ... % rx ry rz
    0); % outer_offset
DetPixelCalc = getDetectorPixel(Exp);
DetPixelExp = [0 0 0];
assert(all(DetPixelCalc(:) == DetPixelExp(:)))

% simple test for a detector 1 pixel high, 1 pixel wide at 1 sdd
Exp = ExperimentalSetUp('Test', 1, ... %sdd
    1, 1, ... % psx psz
    1, 1, ... % detlenx detlenz
    1, 1, ... % cpx cpz
    1, ... % lambda
    0, 0, 0, ... % rx ry rz
    0); % outer_offset
DetPixelCalc = getDetectorPixel(Exp);
DetPixelExp = [0 1 0];
assert(all(DetPixelCalc(:) == DetPixelExp(:)))

% simple test for a detector 1 pixel high, 1 pixel wide at 1 sdd, rx = ry =
% rz = 1
Exp = ExperimentalSetUp('Test', 1, ... %sdd
    1, 1, ... % psx psz
    1, 1, ... % detlenx detlenz
    1, 1, ... % cpx cpz
    1, ... % lambda
    1, 1, 1, ... % rx ry rz
    0); % outer_offset
DetPixelCalc = getDetectorPixel(Exp);
DetPixelExp = [0 1 0];
assert(all(DetPixelCalc(:) == DetPixelExp(:)))

% simple test for a detector 1 pixel high, 1 pixel wide at 1 sdd, rz = 1,
% cpx = 0
Exp = ExperimentalSetUp('Test', 1, ... %sdd
    1, 1, ... % psx psz
    1, 1, ... % detlenx detlenz
    0, 1, ... % cpx cpz
    1, ... % lambda
    0, 0, 1, ... % rx ry rz
    0); % outer_offset
DetPixelCalc = getDetectorPixel(Exp);
% rotate the pixel [1 0 0] around the z axis by 1 degree, add sdd in y
% direction
DetPixelExp = [cosd(1) -sind(1) 0; sind(1) cosd(1) 0; 0 0 1]*[1 0 0]' + [0 1 0]';
assert(all(DetPixelCalc(:) == DetPixelExp(:)))

% simple test for a detector 1 pixel high, 1 pixel wide at 1 sdd, ry = 1,
% cpx = 0
Exp = ExperimentalSetUp('Test', 1, ... %sdd
    1, 1, ... % psx psz
    1, 1, ... % detlenx detlenz
    0, 1, ... % cpx cpz
    1, ... % lambda
    0, 1, 0, ... % rx ry rz
    0); % outer_offset
DetPixelCalc = getDetectorPixel(Exp);
% rotate the pixel [1 0 0] around the y axis by 1 degree, add sdd in y
% direction
DetPixelExp = [cosd(1) 0 sind(1); 0 1 0; -sind(1) 0 cosd(1)]*[1 0 0]' + [0 1 0]';
assert(all(DetPixelCalc(:) == DetPixelExp(:)))

% simple test for a detector 1 pixel high, 1 pixel wide at 1 sdd, rx = 1,
% cpx = 0
Exp = ExperimentalSetUp('Test', 1, ... %sdd
    1, 1, ... % psx psz
    1, 1, ... % detlenx detlenz
    0, 1, ... % cpx cpz
    1, ... % lambda
    1, 0, 0, ... % rx ry rz
    0); % outer_offset
DetPixelCalc = getDetectorPixel(Exp);
% rotate the pixel [1 0 0] around the x axis by 1 degree, add sdd in y
% direction
DetPixelExp = [1 0 0; 0 cosd(1) -sind(1); 0 sind(1) cosd(1)]*[1 0 0]' + [0 1 0]';
assert(all(DetPixelCalc(:) == DetPixelExp(:)))

% Test for sdd of 1, different pixel sizes in x and z direction, detlenx =
% detlenz = 1, centerpixel = [0 0], rotation 1 deg in every direction
Exp = ExperimentalSetUp('Test', 1, ... %sdd
    1, 2, ... % psx psz
    1, 1, ... % detlenx detlenz
    0, 0, ... % cpx cpz
    1, ... % lambda
    1, 1, 1, ... % rx ry rz
    0); % outer_offset
DetPixelCalc = getDetectorPixel(Exp);
% rotate the pixel [1 0 -1], add sdd in y direction
DetPixelExp = [cosd(1) -sind(1) 0; sind(1) cosd(1) 0; 0 0 1]*...
    [1 0 0; 0 cosd(1) -sind(1); 0 sind(1) cosd(1)]*...
    [cosd(1) 0 sind(1); 0 1 0; -sind(1) 0 cosd(1)]*[1 0 -2]' + [0 1 0]';
assert(all(DetPixelCalc(:) == DetPixelExp(:)))

% Test for sdd of 1, different pixel sizes in x and z direction, detlenx =
% detlenz = 1, centerpixel = [0 0], rotation 1 deg in every direction
% gamma (in-plane rotation) set to 10 degree, delta (out-of-plane rotation
% set to 5 degree
Exp = ExperimentalSetUp('Test', 1, ... %sdd
    1, 2, ... % psx psz
    1, 1, ... % detlenx detlenz
    0, 0, ... % cpx cpz
    1, ... % lambda
    1, 1, 1, ... % rx ry rz
    0); % outer_offset
Exp.Geometry = 'GammaDelta';
Exp.delta = 5;
Exp.gamma = 10;
DetPixelCalc = getDetectorPixel(Exp);
% rotate the pixel [1 0 -1], add sdd in y direction
DetPixelExp = [cosd(1) -sind(1) 0; sind(1) cosd(1) 0; 0 0 1]*...
    [1 0 0; 0 cosd(1) -sind(1); 0 sind(1) cosd(1)]*...
    [cosd(1) 0 sind(1); 0 1 0; -sind(1) 0 cosd(1)]*[1 0 -2]' + [0 1 0]';
% perform a rotation of 10 degree around the z axes (gamma) and a rotation
% of 5 degree around the x axes (delta)
DetPixelExp = [cosd(10) -sind(10) 0; sind(10) cosd(10) 0; 0 0 1]*...
    [1 0 0; 0 cosd(5) -sind(5); 0 sind(5) cosd(5)]*DetPixelExp;
assert(all(DetPixelCalc(:) == DetPixelExp(:)))

%% Test 2: getQ
% simple Test: center pixel should become [0 0 0] in q, independent of all
% the other variables
Exp = ExperimentalSetUp('Test', 7.6, ... %sdd
    3.1, 2.4, ... % psx psz
    1, 1, ... % detlenx detlenz
    1.25, 0.4, ... % cpx cpz
    1.4, ... % lambda
    1.2, 1.8, 1.4, ... % rx ry rz
    3.2); % outer_offset
% get the outgoing k vector
% kOut = getDetectorPixel(Exp, 0, 0);
% calculate the q value corresponding to the center pixel
[qxy, qz, qx, qy, q] = CalculateQ(Exp, 0.43, 0.12, 1.25, 0.4);
assert(all([qx qy qz qxy q] == zeros(1, 5)));

