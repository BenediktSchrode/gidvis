% testing of FittingWithCustomBorders
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Test 1: Constructor test
x = linspace(-3, 3, 500);
a = 3;
b = 0.4;
c = .55;
y = a/(sqrt(2*pi*c^2)).*exp(-(x-b).^2./(2.*c.^2));

figure
lh = plot(x, y);
F = FittingWithCustomBorders(lh, [], @OneDimGaussianFit);
assert(all(F.Color == [1 0 0]))
assert(all(F.Limits == [-3 3]))
assert(abs(F.FitResult.A - a) < 1e-8)
assert(abs(F.FitResult.d) < 1e-8)
assert(abs(F.FitResult.k) < 1e-8)
assert(abs(F.FitResult.mu - b) < 1e-8)
assert(abs(F.FitResult.sigma - c) < 1e-8)

figure
lh = plot(x, y);
F = FittingWithCustomBorders(lh, 'k', @OneDimGaussianFit);
assert(all(F.Color == [0 0 0]))
assert(all(F.Limits == [-3 3]))
assert(abs(F.FitResult.A - a) < 1e-8)
assert(abs(F.FitResult.d) < 1e-8)
assert(abs(F.FitResult.k) < 1e-8)
assert(abs(F.FitResult.mu - b) < 1e-8)
assert(abs(F.FitResult.sigma - c) < 1e-8)

figure
lh = plot(x, y);
F = FittingWithCustomBorders(lh, [.5 .3 .7], @OneDimGaussianFit);
assert(all(F.Color == [.5 .3 .7]))
assert(all(F.Limits == [-3 3]))
assert(abs(F.FitResult.A - a) < 1e-8)
assert(abs(F.FitResult.d) < 1e-8)
assert(abs(F.FitResult.k) < 1e-8)
assert(abs(F.FitResult.mu - b) < 1e-8)
assert(abs(F.FitResult.sigma - c) < 1e-8)

x = -3:0.01:3;
a = 3;
b = 0.4;
c = .55;
y = a/(sqrt(2*pi*c^2)).*exp(-(x-b).^2./(2.*c.^2));

figure
lh = plot(x, y);
F = FittingWithCustomBorders(lh, [], @OneDimGaussianFit, [-2 2]);
assert(all(F.Color == [1 0 0]))
assert(all(F.Limits == [-2 2]))
assert(abs(F.FitResult.A - a) < 1e-8)
assert(abs(F.FitResult.d) < 1e-8)
assert(abs(F.FitResult.k) < 1e-8)
assert(abs(F.FitResult.mu - b) < 1e-8)
assert(abs(F.FitResult.sigma - c) < 1e-8)

figure
lh = plot(x, y);
F = FittingWithCustomBorders(lh, [], @OneDimGaussianFit, [-2.005 2.005]);
assert(all(F.Color == [1 0 0]))
assert(all(F.Limits == [-2.01 2]))
assert(abs(F.FitResult.A - a) < 1e-8)
assert(abs(F.FitResult.d) < 1e-8)
assert(abs(F.FitResult.k) < 1e-8)
assert(abs(F.FitResult.mu - b) < 1e-8)
assert(abs(F.FitResult.sigma - c) < 1e-8)

%% Test 2: Adapting data after creation
x = linspace(-3, 3, 500);
a = 3;
b = 0.4;
c = .55;
y = a/(sqrt(2*pi*c^2)).*exp(-(x-b).^2./(2.*c.^2));

figure
lh = plot(x, y);
F = FittingWithCustomBorders(lh, [], @OneDimGaussianFit);
set(lh, 'YData', y./2)
assert(all(F.Color == [1 0 0]))
assert(all(F.Limits == [-3 3]))
assert(abs(F.FitResult.A - a/2) < 1e-8)
assert(abs(F.FitResult.d) < 1e-8)
assert(abs(F.FitResult.k) < 1e-8)
assert(abs(F.FitResult.mu - b) < 1e-8)
assert(abs(F.FitResult.sigma - c) < 1e-8)

figure
lh = plot(x, y);
F = FittingWithCustomBorders(lh, [], @OneDimGaussianFit);
set(lh, 'XData', linspace(-13, -7, 500));
assert(all(F.Color == [1 0 0]))
assert(all(F.Limits == [-3 3]))
assert(isempty(F.FitResult))

figure
lh = plot(x, y);
F = FittingWithCustomBorders(lh, [], @OneDimGaussianFit);
assert(all(F.Color == [1 0 0]))
assert(all(F.Limits == [-3 3]))
newX = linspace(-6, 0, 500);
set(lh, 'XData', newX);
ind1 = find(newX >= -3, 1);
ind2 = find(newX <= 3, 1, 'last');
assert(all(F.Limits == [newX(ind1) newX(ind2)]))
assert(abs(F.FitResult.A - a) < 1e-8)
assert(abs(F.FitResult.d) < 1e-8)
assert(abs(F.FitResult.k) < 1e-8)
assert(abs(F.FitResult.mu - (b-3)) < 1e-8)
assert(abs(F.FitResult.sigma - c) < 1e-8)

figure
lh = plot(x, y);
F = FittingWithCustomBorders(lh, [], @OneDimGaussianFit);
assert(all(F.Color == [1 0 0]))
assert(all(F.Limits == [-3 3]))
newY = y;
newY(230) = NaN;
set(lh, 'YData', newY);
assert(abs(F.FitResult.A - a) < 1e-8)
assert(abs(F.FitResult.d) < 1e-8)
assert(abs(F.FitResult.k) < 1e-8)
assert(abs(F.FitResult.mu - b) < 1e-8)
assert(abs(F.FitResult.sigma - c) < 1e-8)

figure
lh = plot(x, y);
F = FittingWithCustomBorders(lh, [], @OneDimGaussianFit);
assert(all(F.Color == [1 0 0]))
assert(all(F.Limits == [-3 3]))
newY = y;
newY(230) = inf;
set(lh, 'YData', newY);
assert(abs(F.FitResult.A - a) < 1e-8)
assert(abs(F.FitResult.d) < 1e-8)
assert(abs(F.FitResult.k) < 1e-8)
assert(abs(F.FitResult.mu - b) < 1e-8)
assert(abs(F.FitResult.sigma - c) < 1e-8)

%% Test 3: NaN/inf data
x = linspace(-3, 3, 500);
y = NaN(size(x));

figure
lh = plot(x, y);
F = FittingWithCustomBorders(lh, [], @OneDimGaussianFit);
assert(all(F.Color == [1 0 0]))
assert(all(F.Limits == [-3 3]))
assert(isempty(F.FitResult))

a = 3;
b = 0.4;
c = .55;
y = a/(sqrt(2*pi*c^2)).*exp(-(x-b).^2./(2.*c.^2));

set(lh, 'YData', y);
assert(all(F.Color == [1 0 0]))
assert(all(F.Limits == [-3 3]))
assert(abs(F.FitResult.A - a) < 1e-8)
assert(abs(F.FitResult.d) < 1e-8)
assert(abs(F.FitResult.k) < 1e-8)
assert(abs(F.FitResult.mu - b) < 1e-8)
assert(abs(F.FitResult.sigma - c) < 1e-8)


x = linspace(-3, 3, 500);
y = inf(size(x));

figure
lh = plot(x, y);
F = FittingWithCustomBorders(lh, [], @OneDimGaussianFit);
assert(all(F.Color == [1 0 0]))
assert(all(F.Limits == [-3 3]))
assert(isempty(F.FitResult))

a = 3;
b = 0.4;
c = .55;
y = a/(sqrt(2*pi*c^2)).*exp(-(x-b).^2./(2.*c.^2));

set(lh, 'YData', y);
assert(all(F.Color == [1 0 0]))
assert(all(F.Limits == [-3 3]))
assert(abs(F.FitResult.A - a) < 1e-8)
assert(abs(F.FitResult.d) < 1e-8)
assert(abs(F.FitResult.k) < 1e-8)
assert(abs(F.FitResult.mu - b) < 1e-8)
assert(abs(F.FitResult.sigma - c) < 1e-8)

%% Test 4: Methods test
x = linspace(-3, 3, 500);
a = 0.75;
b = -.2;
c = .3;
y = a/(sqrt(2*pi*c^2)).*exp(-(x-b).^2./(2.*c.^2));

f = figure;
lh = plot(x, y);
F = FittingWithCustomBorders(lh, [], @OneDimGaussianFit);
assert(all(F.Color == [1 0 0]))
assert(all(F.Limits == [-3 3]))
assert(abs(F.FitResult.A - a) < 1e-8)
assert(abs(F.FitResult.d) < 1e-8)
assert(abs(F.FitResult.k) < 1e-8)
assert(abs(F.FitResult.mu - b) < 1e-8)
assert(abs(F.FitResult.sigma - c) < 1e-8)

CenterResult(F)
F.CenterResult

delete(F)

close(f)

%% Test 5: Other fitting models
x = linspace(0, 10, 100);
L = 3;
k = 16;
x0 = 3;
d = 5;
y = L./(1+exp(-k.*(x-x0))) + d;

figure
lh = plot(x, y);
F = FittingWithCustomBorders(lh, [], @SigmoidFit);
assert(abs(F.FitResult.L - L) < 1e-8)
assert(abs(F.FitResult.k - k) < 1e-8)
assert(abs(F.FitResult.x0 - x0) < 1e-8)
assert(abs(F.FitResult.d - d) < 1e-8)


x = linspace(0, 10, 100);
L = 3;
k = -16;
x0 = 3;
d = 5;
y = L./(1+exp(-k.*(x-x0))) + d;

figure
lh = plot(x, y);
F = FittingWithCustomBorders(lh, [], @SigmoidFit);
assert(abs(F.FitResult.L - L) < 1e-8)
assert(abs(F.FitResult.k - k) < 1e-8)
assert(abs(F.FitResult.x0 - x0) < 1e-8)
assert(abs(F.FitResult.d - d) < 1e-8)


x = linspace(0, 3, 100);
p = [1 2];
y = polyval(p, x);

figure
lh = plot(x, y);
F = FittingWithCustomBorders(lh);
assert(abs(F.FitResult.p1 - p(1)) < 1e-8)
assert(abs(F.FitResult.p2 - p(2)) < 1e-8)

p = [1 2 3];
y = polyval(p, x);

figure
lh = plot(x, y);
F = FittingWithCustomBorders(lh, [], fittype('poly2'));
assert(abs(F.FitResult.p1 - p(1)) < 1e-8)
assert(abs(F.FitResult.p2 - p(2)) < 1e-8)
assert(abs(F.FitResult.p3 - p(3)) < 1e-8)

figure
lh = plot(x, y);
F = FittingWithCustomBorders(lh);
F.FitFunction = fittype('poly2');
assert(abs(F.FitResult.p1 - p(1)) < 1e-8)
assert(abs(F.FitResult.p2 - p(2)) < 1e-8)
assert(abs(F.FitResult.p3 - p(3)) < 1e-8)

%% Test 6: Adapting limits after creation
x = -3:0.01:3;
a = 3;
b = 0.4;
c = .55;
y = a/(sqrt(2*pi*c^2)).*exp(-(x-b).^2./(2.*c.^2));

figure
lh = plot(x, y);
F = FittingWithCustomBorders(lh, [], @OneDimGaussianFit, [-2 2]);
assert(all(F.Color == [1 0 0]))
assert(all(F.Limits == [-2 2]))
assert(abs(F.FitResult.A - a) < 1e-8)
assert(abs(F.FitResult.d) < 1e-8)
assert(abs(F.FitResult.k) < 1e-8)
assert(abs(F.FitResult.mu - b) < 1e-8)
assert(abs(F.FitResult.sigma - c) < 1e-8)

F.Limits = [-2.5 2.5];
assert(all(F.Limits == [-2.5 2.5]))