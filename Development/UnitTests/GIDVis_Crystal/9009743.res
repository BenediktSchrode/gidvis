TITL 9009743
CELL 0.71073 4.115 4.115 4.115 90 90 90
ZERR 48 0.0000 0.0000 0.0000 0 0 0
LATT 1
SYMM -y,z,-x
SYMM -z,x,-y
SYMM -x,y,-z
SYMM -z,y,x
SYMM -x,z,y
SYMM -y,x,z
SYMM -z,-y,-x
SYMM -x,-z,-y
SYMM -y,-x,-z
SYMM -y,-z,x
SYMM -z,-x,y
SYMM -x,-y,z
SYMM y,-z,-x
SYMM z,-x,-y
SYMM x,-y,-z
SYMM z,-y,x
SYMM x,-z,y
SYMM y,-x,z
SYMM z,y,-x
SYMM x,z,-y
SYMM y,x,-z
SYMM y,z,x
SYMM z,x,y
SFAC Cl Cs
UNIT 1 1
FVAR 1.00
Cs      2  0.000000  0.000000  0.000000  0.020833 0.05000
Cl      1  0.500000  0.500000  0.500000  0.020833 0.05000
END
