TITL 9005769
CELL 0.71073 4.2696 4.2696 4.2696 90 90 90
ZERR 24 0.0000 0.0000 0.0000 0 0 0
LATT -1
SYMM 1/2+z,1/2+x,1/2-y
SYMM 1/2+z,1/2-x,1/2+y
SYMM 1/2-z,1/2+x,1/2+y
SYMM 1/2-z,1/2-x,1/2-y
SYMM y,-z,-x
SYMM -y,-z,x
SYMM -y,z,-x
SYMM y,z,x
SYMM 1/2+x,1/2-y,1/2+z
SYMM 1/2-x,1/2+y,1/2+z
SYMM 1/2+x,1/2+y,1/2-z
SYMM 1/2-x,1/2-y,1/2-z
SYMM -z,-x,y
SYMM -z,x,-y
SYMM z,-x,-y
SYMM z,x,y
SYMM 1/2-y,1/2+z,1/2+x
SYMM 1/2+y,1/2+z,1/2-x
SYMM 1/2+y,1/2-z,1/2+x
SYMM 1/2-y,1/2-z,1/2-x
SYMM -x,y,-z
SYMM x,-y,-z
SYMM -x,-y,z
SFAC O Cu
UNIT 2 4
FVAR 1.00
Cu      2  0.250000  0.250000  0.250000  0.166667 0.05000
O       1  0.000000  0.000000  0.000000  0.083333 0.05000
END
