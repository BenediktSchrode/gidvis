TITL LaB6_660a
CELL 0.71073 4.15692 4.15692 4.15692 90 90 90
ZERR 48 0.00000 0.00000 0.00000 0.0 0.0 0.0
LATT 1
SYMM -x,-y,z
SYMM -x,y,-z
SYMM x,-y,-z
SYMM z,x,y
SYMM z,-x,-y
SYMM -z,-x,y
SYMM -z,x,-y
SYMM y,z,x
SYMM -y,z,-x
SYMM y,-z,-x
SYMM -y,-z,x
SYMM y,x,-z
SYMM -y,-x,-z
SYMM y,-x,z
SYMM -y,x,z
SYMM x,z,-y
SYMM -x,z,y
SYMM -x,-z,-y
SYMM x,-z,y
SYMM z,y,-x
SYMM z,-y,x
SYMM -z,y,x
SYMM -z,-y,-x
SFAC B La
UNIT 6 1
FVAR 1.00
LA1     2  0.000000  0.000000  0.000000  0.020833 0.004500
B       1  0.198000  0.500000  0.500000  0.125000 0.003500
END
