classdef Test_GIDVis_Crystal < matlab.unittest.TestCase
    % Testing of GIDVis_Crystal.m
    %
    % This file is part of GIDVis.
    %
    % see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    properties (TestParameter)
        % to run this test, the following *.res files have to be on the
        % MATLAB search path. Additionally, the *.HKL files as exported
        % from PowderCell have to be on the MATLAB search path.
        % Of course, you can also use your own test files instead. Enter
        % their names in the File cell.
        File = {'1000003', '1000007', '1011258', '1507774', '2000018', ...
                '2000503', '2000581', '2001614', '2001914', '2004506', ...
                '2020192', '2300116', '4109834', '4109837', '4300483', ...
                '4300501', '4312876', '4500011', '4505180', '4505197', ...
                '9007849', '9009743', '9012188', 'CaffeineAlpha_NIWFEE02', ...
                'CaffeineBeta_NIWFEE03', 'CaffeineMonohydrate_CAFINE01', ...
                'Cu(OH)2', 'jb_away_cr781', 'LaB6_660a', ...
                'P2O_PENTQU01', 'P4O_YOFROB', 'P4O_YOFROB01', ...
                'SiC-6H-alpha', 'SINDIG_1259571', 'SINDIG02_1259573', ...
                'LaB6_NISTSRM_660a', 'AB_hP8_186_ab_ab', 'SiC4H', ...
                'CuO_1011148', 'Cu2O_9005769', 'CuO_ICSD67850', ...
                'CopperHydroxide_ICSD068459', 'CuBDC_ZUBKEO'};
    end
    
    methods (Test)
        function ConstructorTest(testCase)
            % This function tests the various constructors GIDVis_Crystal
            % can be used with. It should run without showing any message
            % boxes etc.
            
            % no input
            Cr = GIDVis_Crystal();
            
            % a, b, c, alpha, beta, gamma, name
            Cr = GIDVis_Crystal(3, 4, 5, 85, 90, 95, 'Test');
            
            % full property list 1
            Cr2 = GIDVis_Crystal(Cr.a, Cr.b, Cr.c, Cr.alpha, Cr.beta, ...
                Cr.gamma, Cr.Name, Cr.SpacegroupNumber, Cr.UnitCell, ...
                Cr.Setting, {'C' 'H' 'S' 'Na' 'O' 'Li'}, [1 1 1 1 1 1], ...
                Cr.Symm);
            
            % file path
            Cr = GIDVis_Crystal('TestStructure.res');
            
            % full property list 2
            Cr2 = GIDVis_Crystal(Cr.a, Cr.b, Cr.c, Cr.alpha, Cr.beta, ...
                Cr.gamma, Cr.Name, Cr.SpacegroupNumber, Cr.UnitCell, ...
                Cr.Setting, {'C' 'H' 'S' 'Na' 'O' 'Li'}, [1 1 1 1 1 1], ...
                Cr.Symm);
            
            % GIDVis_Crystal as input
            Cr3 = GIDVis_Crystal(Cr);
        end
        
        function MethodsTest(testCase)
            % This function tests the different methods available for
            % GIDVis_Crystal which are not tested in any of the other
            % tests.
            
            Cr = GIDVis_Crystal('TestStructure.res');
            
            %% Conversion fractional/cartesian coordinates
            FrC = [.3 .2 .1; 0 0 0; 1 1 1; .5 .1 .36; -0.3, -0.2 -0.1];
            CaC = CalculateCartesianCoordinates(Cr, FrC);
            FrC2 = CalculateFractionalCoordinates(Cr, CaC);
            assert(all(abs(FrC(:) - FrC2(:)) < 1e-8))
            
            %% General tests
            [Passed, errMsg] = CheckUnitCellConsistency(Cr);
            assert(Passed == true)
            assert(isempty(errMsg))
            
            f = DataView(Cr);
            assert(isa(f, 'matlab.ui.Figure'))
            assert(strcmp(get(f, 'Visible'), 'on'))
            
            f = EditUnitCell(Cr);
            assert(isa(f, 'matlab.ui.Figure'))
            assert(strcmp(get(f, 'Visible'), 'on'))
            
            Atoms1 = IncreaseUnitCell(Cr);
            assert(isa(Atoms1, 'GIDVis_Crystal_Atom'))
            Atoms2 = IncreaseUnitCell(Cr, [0 1], [0 1], [0 1]);
            assert(isa(Atoms2, 'GIDVis_Crystal_Atom'))
            assert(numel(Atoms1) == numel(Atoms2))
            
            nv = NormalVector(Cr, [1 1 1; 1 0 0]);
            assert(all(size(nv) == [2 3]))
            
            [psi, phi] = PeakPositionsA(Cr, [1 1 1; 1 0 0; -3 2 -1; 1 4 0], [1 -2 3]);
            assert(all(size(psi) == size(phi)))
            
            q = PeakPositionsQ(Cr, [1 1 1; 1 0 0; -3 2 -1; 1 4 0], [1 -2 3]);
            assert(all(size(q) == [4 3]))
            
            R = RForDefault(Cr);
            assert(all(size(R) == [3 3]))
            assert(all(all(R == eye(3))))
            
            Rotate(Cr, [1 0 0; 0 0 -1; 0 1 0]*[0 0 -1; 0 1 0; 1 0 0])
            nv = NormalVector(Cr, [0 0 1]);
            assert(all(nv == [-1 0 0]))
            
            R = RForDefault(Cr);
            Rotate(Cr, R);
            nv = NormalVector(Cr, [0 0 1]);
            assert(all(nv == [0 0 1]))
            
            [Vertices, Faces] = hklPlane(Cr, [1 2 3]);
            assert(all(size(Vertices) == [3 3]))
            assert(all(size(Faces) == [1 3]))
            [Vertices, Faces] = hklPlane(Cr, [0 0 1]);
            assert(all(size(Vertices) == [4 3]))
            assert(all(size(Faces) == [1 4]))
            
            Dirs = uvwDirection(Cr, [1 2 3]);
            assert(all(size(Dirs) == [1 3]));
            
            Dirs = uvwDirection(Cr, [1 2 3], 1);
            assert(all(size(Dirs) == [1 3]));
            assert(abs(sqrt(sum(Dirs.^2)) - 1) < 1e-8)
            
            Dirs = uvwDirection(Cr, [1 2 3], 3);
            assert(all(size(Dirs) == [1 3]));
            assert(abs(sqrt(sum(Dirs.^2)) - 3) < 1e-8)
            
            Dirs = uvwDirection(Cr, [1 2 3; 5 3 1; -3 2 5; 6 2 0]);
            assert(all(size(Dirs) == [4 3]));
            
            Dirs = uvwDirection(Cr, [1 2 3; 5 3 1; -3 2 5; 6 2 0], 1);
            assert(all(size(Dirs) == [4 3]));
            assert(all(abs(sqrt(sum(Dirs.^2, 2)) - 1) < 1e-8))
            
            Dirs = uvwDirection(Cr, [1 2 3; 5 3 1; -3 2 5; 6 2 0], 1.3);
            assert(all(size(Dirs) == [4 3]));
            assert(all(abs(sqrt(sum(Dirs.^2, 2)) - 1.3) < 1e-8))
            
            HKL = RemoveBraggPeakDuplicates(Cr, [1 1 1; 1 0 0; -1 0 0; 1 1 1], [0 0 1]);
            assert(all(size(HKL) == [3 3]))
            assert(all(HKL(:)' == [1 1 -1 1 0 0 1 0 0]))
            
            HKL = RemoveBraggPeakDuplicates(Cr, [1 1 1; 1 0 0; -1 0 0; 1 1 1]);
            assert(all(size(HKL) == [2 3]))
            assert(all(HKL(:)' == [1 1 1 0 1 0]))
            
            Cr.Orient([0 2 0]);
            assert(Cr.a_vec(3) < 1e-8)
            assert(Cr.c_vec(3) < 1e-8)
        end
        
        function PlotTest(testCase)
            % This function tests the plotting of GIDVis_Crystal
            
            Cr = GIDVis_Crystal('TestStructure.res');
            
            %% Testing plot
            plot(Cr);
            
            %% Testing plotAtoms
            hg = plotAtoms(Cr, Cr.UnitCell, 'BondColor', 'r', 'BondWidth', 3, ...
                'BondLength', 1.8, 'AtomStyle', '3D', 'Translation', [1 2 3], ...
                'AtomColor', [0 1 0]);
            assert(isa(hg, 'matlab.graphics.primitive.Group'))
            Ch = get(hg, 'Children');
            assert(numel(findobj(Ch, 'Type', 'patch')) == numel(Cr.UnitCell))
            assert(any(cellfun(@isempty, get(Ch, 'Tag'))) == 0)
            
            % testing nx3 matrix for atom color
            hg = plotAtoms(Cr, Cr.UnitCell, 'AtomColor', rand(numel(Cr.UnitCell), 3));
            assert(isa(hg, 'matlab.graphics.primitive.Group'))
            Ch = get(hg, 'Children');
            assert(numel(findobj(Ch, 'Type', 'patch')) == numel(Cr.UnitCell))
            assert(any(cellfun(@isempty, get(Ch, 'Tag'))) == 0)
            
            % testing 3xn matrix for atom color
            hg = plotAtoms(Cr, Cr.UnitCell, 'AtomColor', rand(3, numel(Cr.UnitCell)));
            assert(isa(hg, 'matlab.graphics.primitive.Group'))
            Ch = get(hg, 'Children');
            assert(numel(findobj(Ch, 'Type', 'patch')) == numel(Cr.UnitCell))
            assert(any(cellfun(@isempty, get(Ch, 'Tag'))) == 0)
            
            % testing function handle with three inputs for atom color
            hg = plotAtoms(Cr, Cr.UnitCell, 'AtomColor', @(x, y, z) x.^2 + y.^2);
            assert(isa(hg, 'matlab.graphics.primitive.Group'))
            Ch = get(hg, 'Children');
            assert(numel(findobj(Ch, 'Type', 'patch')) == numel(Cr.UnitCell))
            assert(any(cellfun(@isempty, get(Ch, 'Tag'))) == 0)
            
            % testing function handle with two inputs for atom color
            hg = plotAtoms(Cr, Cr.UnitCell, 'AtomColor', @(Cr, A) Beq(A, Cr));
            assert(isa(hg, 'matlab.graphics.primitive.Group'))
            Ch = get(hg, 'Children');
            assert(numel(findobj(Ch, 'Type', 'patch')) == numel(Cr.UnitCell))
            assert(any(cellfun(@isempty, get(Ch, 'Tag'))) == 0)
            
            % testing single char color as atom color
            hg = plotAtoms(Cr, Cr.UnitCell, 'AtomColor', 'r');
            assert(isa(hg, 'matlab.graphics.primitive.Group'))
            Ch = get(hg, 'Children');
            assert(numel(findobj(Ch, 'Type', 'patch')) == numel(Cr.UnitCell))
            assert(any(cellfun(@isempty, get(Ch, 'Tag'))) == 0)
            
            hg = plotAtoms(Cr, Cr.UnitCell, 'AtomColor', 'magenta');
            assert(isa(hg, 'matlab.graphics.primitive.Group'))
            Ch = get(hg, 'Children');
            assert(numel(findobj(Ch, 'Type', 'patch')) == numel(Cr.UnitCell))
            assert(any(cellfun(@isempty, get(Ch, 'Tag'))) == 0)
            
            % testing cell of char colors as atom color
            PosCol = {'red', 'r', 'green', 'g', ...
                'blue', 'b', 'yellow', 'y', 'magenta', 'm', 'cyan', 'c', ...
                'white', 'w', 'black', 'k'};
            ColorInds = randi(numel(PosCol), 1, numel(Cr.UnitCell));
            Colors = PosCol(ColorInds);
            
            hg = plotAtoms(Cr, Cr.UnitCell, 'AtomColor', Colors);
            assert(isa(hg, 'matlab.graphics.primitive.Group'))
            Ch = get(hg, 'Children');
            assert(numel(findobj(Ch, 'Type', 'patch')) == numel(Cr.UnitCell))
            assert(any(cellfun(@isempty, get(Ch, 'Tag'))) == 0)
            
            hg = plotAtoms(Cr, Cr.UnitCell, 'AtomColor', Colors');
            assert(isa(hg, 'matlab.graphics.primitive.Group'))
            Ch = get(hg, 'Children');
            assert(numel(findobj(Ch, 'Type', 'patch')) == numel(Cr.UnitCell))
            assert(any(cellfun(@isempty, get(Ch, 'Tag'))) == 0)
            
            %% Testing lightsource
            f = figure;
            ax = axes(f);
            plotAtoms(Cr, Cr.UnitCell, 'Parent', ax);
            LS = findall(ax, 'Type', 'light');
            assert(numel(LS) == 1)
            view(ax, 35, 27)
            LS = findall(ax, 'Type', 'light');
            assert(numel(LS) == 1)
            
            f = figure;
            ax = subplot(1, 2, 1);
            plotAtoms(Cr, Cr.UnitCell, 'Parent', ax);
            LSf = findall(f, 'Type', 'light');
            assert(numel(LSf) == 1)
            LSa = findall(ax, 'Type', 'light');
            assert(numel(LSa) == 1)
            ax = subplot(1, 2, 2);
            plotAtoms(Cr, Cr.UnitCell, 'Parent', ax);
            LSf = findall(f, 'Type', 'light');
            assert(numel(LSf) == 2)
            LSa = findall(ax, 'Type', 'light');
            assert(numel(LSa) == 1)
            view(ax, 35, 27)
            LSf = findall(f, 'Type', 'light');
            assert(numel(LSf) == 2)
            LSa = findall(ax, 'Type', 'light');
            assert(numel(LSa) == 1)
            
            %% Testing plotInPlaneAtoms
            hg = plotInPlaneAtoms(Cr, [1 1 1]);
            assert(isa(hg, 'matlab.graphics.primitive.Group'))
            assert(numel(get(hg, 'Children')) == 78 + 1) % atoms plus line for atom centers
            assert(any(cellfun(@isempty, get(get(hg, 'Children'), 'Tag'))) == 0)
            
            hg = plotInPlaneAtoms(Cr, [1 1 1], 'AtomColor', [0 0 1], 'Translation', [1 2 3]);
            assert(isa(hg, 'matlab.graphics.primitive.Group'))
            assert(numel(get(hg, 'Children')) == 78 + 1) % atoms plus line for atom centers
            assert(any(cellfun(@isempty, get(get(hg, 'Children'), 'Tag'))) == 0)
            
            
            hg = plotInPlaneAtoms(Cr, [1 1 1], 0.8);
            assert(isa(hg, 'matlab.graphics.primitive.Group'))
            assert(numel(get(hg, 'Children')) == 78 + 1) % atoms plus line for atom centers
            assert(any(cellfun(@isempty, get(get(hg, 'Children'), 'Tag'))) == 0)
            
            hg = plotInPlaneAtoms(Cr, [1 1 1], 0.8, 'AtomColor', [0 0 1], 'Translation', [1 2 3]);
            assert(isa(hg, 'matlab.graphics.primitive.Group'))
            assert(numel(get(hg, 'Children')) == 78 + 1) % atoms plus line for atom centers
            assert(any(cellfun(@isempty, get(get(hg, 'Children'), 'Tag'))) == 0)
            
            
            hg = plotInPlaneAtoms(Cr, [1 1 1], 0.8, [-2 2]);
            assert(isa(hg, 'matlab.graphics.primitive.Group'))
            assert(numel(get(hg, 'Children')) == 78 + 1) % atoms plus line for atom centers
            assert(any(cellfun(@isempty, get(get(hg, 'Children'), 'Tag'))) == 0)
            
            hg = plotInPlaneAtoms(Cr, [1 1 1], 0.8, [-2 2], 'AtomColor', [0 0 1], 'Translation', [1 2 3]);
            assert(isa(hg, 'matlab.graphics.primitive.Group'))
            assert(numel(get(hg, 'Children')) == 78 + 1) % atoms plus line for atom centers
            assert(any(cellfun(@isempty, get(get(hg, 'Children'), 'Tag'))) == 0)
            
            
            hg = plotInPlaneAtoms(Cr, [1 1 1], 0.8, [-2 2]);
            assert(isa(hg, 'matlab.graphics.primitive.Group'))
            assert(numel(get(hg, 'Children')) == 78 + 1) % atoms plus line for atom centers
            assert(any(cellfun(@isempty, get(get(hg, 'Children'), 'Tag'))) == 0)
            
            hg = plotInPlaneAtoms(Cr, [1 1 1], 0.8, [-2 2], 'AtomColor', [0 0 1], 'Translation', [1 2 3]);
            assert(isa(hg, 'matlab.graphics.primitive.Group'))
            assert(numel(get(hg, 'Children')) == 78 + 1) % atoms plus line for atom centers
            assert(any(cellfun(@isempty, get(get(hg, 'Children'), 'Tag'))) == 0)
            
            
            hg = plotInPlaneAtoms(Cr, [1 1 1], 0.8, [-2 2], [-2 2]);
            assert(isa(hg, 'matlab.graphics.primitive.Group'))
            assert(numel(get(hg, 'Children')) == 78 + 1) % atoms plus line for atom centers
            assert(any(cellfun(@isempty, get(get(hg, 'Children'), 'Tag'))) == 0)
            
            hg = plotInPlaneAtoms(Cr, [1 1 1], 0.8, [-2 2], [-2 2], 'AtomColor', [0 0 1], 'Translation', [1 2 3]);
            assert(isa(hg, 'matlab.graphics.primitive.Group'))
            assert(numel(get(hg, 'Children')) == 78 + 1) % atoms plus line for atom centers
            assert(any(cellfun(@isempty, get(get(hg, 'Children'), 'Tag'))) == 0)
            
            
            hg = plotInPlaneAtoms(Cr, [1 1 1], 0.8, [-2 2], [-2 2], [-2 2]);
            assert(isa(hg, 'matlab.graphics.primitive.Group'))
            assert(numel(get(hg, 'Children')) == 78 + 1) % atoms plus line for atom centers
            assert(any(cellfun(@isempty, get(get(hg, 'Children'), 'Tag'))) == 0)
            
            hg = plotInPlaneAtoms(Cr, [1 1 1], 0.8, [-2 2], [-2 2], [-2 2], 'AtomColor', [0 0 1], 'Translation', [1 2 3]);
            assert(isa(hg, 'matlab.graphics.primitive.Group'))
            assert(numel(get(hg, 'Children')) == 78 + 1) % atoms plus line for atom centers
            assert(any(cellfun(@isempty, get(get(hg, 'Children'), 'Tag'))) == 0)
            
            %% Testing plotPlane
            
            hg = plotPlane(Cr, [1 1 1]);
            assert(isa(hg, 'matlab.graphics.primitive.Patch'))
            assert(isempty(get(hg, 'Tag')) == 0)
            
            hg = plotPlane(Cr, [1 1 1], 'FaceColor', 'g');
            assert(isa(hg, 'matlab.graphics.primitive.Patch'))
            assert(isempty(get(hg, 'Tag')) == 0)
            
            hg = plotPlane(Cr, [1 1 1], 'Translation', [3 1 2]);
            assert(isa(hg, 'matlab.graphics.primitive.Patch'))
            assert(isempty(get(hg, 'Tag')) == 0)
            
            hg = plotPlane(Cr, [1 1 1], 'Translation', -[3 1 2], ...
                'FaceColor', 'r');
            assert(isa(hg, 'matlab.graphics.primitive.Patch'))
            assert(isempty(get(hg, 'Tag')) == 0)
            
            %% Testing plotUnitCell
            hg = plotUnitCell(Cr);
            assert(isa(hg, 'matlab.graphics.primitive.Group'))
            assert(numel(get(hg, 'Children')) == 7)
            assert(any(cellfun(@isempty, get(get(hg, 'Children'), 'Tag'))) == 0)
            
            %% Testing plotDirection
            hg = plotDirection(Cr, [1 3 -4; 0 0 1], 3, 'Color', 'r');
            assert(isa(hg, 'matlab.graphics.chart.primitive.Line'))
            assert(all(size(hg) == [2 1]))
            assert(any(cellfun(@isempty, get(hg, 'Tag'))) == 0)
            
            hg = plotDirection(Cr, [1 3 -4; 0 0 1]);
            assert(isa(hg, 'matlab.graphics.chart.primitive.Line'))
            assert(all(size(hg) == [2 1]))
            assert(any(cellfun(@isempty, get(hg, 'Tag'))) == 0)
            
            hg = plotDirection(Cr, [1 3 -4; 0 0 1], 0);
            assert(isa(hg, 'matlab.graphics.chart.primitive.Line'))
            assert(all(size(hg) == [2 1]))
            assert(any(cellfun(@isempty, get(hg, 'Tag'))) == 0)
            
            hg = plotDirection(Cr, [1 3 -4; 0 0 1], 0, 'Translation', [3 5 1]);
            assert(isa(hg, 'matlab.graphics.chart.primitive.Line'))
            assert(all(size(hg) == [2 1]))
            assert(any(cellfun(@isempty, get(hg, 'Tag'))) == 0)
            
            hg = plotDirection(Cr, [1 3 -4; 0 0 1], 0, 'Translation', [3 5 1], ...
                'Label', 'on');
            assert(isa(hg(1:2), 'matlab.graphics.chart.primitive.Line'))
            assert(isa(hg(3:4), 'matlab.graphics.primitive.Text'))
            assert(all(size(hg) == [4 1]))
            assert(any(cellfun(@isempty, get(hg, 'Tag'))) == 0)
            
        end
        
        function ResImportTest(testCase)
            % This function tests the *.res file import using the file
            % TestStructure.res which contains some difficult cases for the
            % import.
            Cr = GIDVis_Crystal('TestStructure.res');
            
            % check general properties
            assert(strcmp(Cr.Name, 'Testing'));
            assert(all(abs([Cr.a Cr.b Cr.c Cr.alpha Cr.beta Cr.gamma] - ...
                [5.037 3.518 12.49 90.83 98.7 89.01]) < 1e-8));
            assert(all(abs([Cr.Delta_a Cr.Delta_b Cr.Delta_c Cr.Delta_alpha Cr.Delta_beta Cr.Delta_gamma] - ...
                [0.001 0.005 0.03 0.08 0.1 0.02]) < 1e-8));
            assert(abs(Cr.V - 218.7298) < 1e-4)
            % check asymmetric unit
            assert(numel(Cr.AsymmetricUnit) == 6);
            
            assert(all([ ...
                strcmp(Cr.AsymmetricUnit(1).Element, 'C'), ...
                abs(Cr.AsymmetricUnit(1).xAccuracy - 1e-6) < 1e-8, ...
                abs(Cr.AsymmetricUnit(1).yAccuracy - 1e-6) < 1e-8, ...
                abs(Cr.AsymmetricUnit(1).zAccuracy - 1e-4) < 1e-8, ...
                abs(Cr.AsymmetricUnit(1).SOF - 1) < 1e-8, ...
                all(abs(Cr.AsymmetricUnit(1).U - (0.01:0.01:0.06)) < 1e-8), ...
                abs(Cr.AsymmetricUnit(1).x - 0.333333) < Cr.AsymmetricUnit(1).xAccuracy, ...
                abs(Cr.AsymmetricUnit(1).y - 0.666667) < Cr.AsymmetricUnit(1).yAccuracy, ...
                abs(Cr.AsymmetricUnit(1).z - 0.0000) < Cr.AsymmetricUnit(1).zAccuracy ...
                ]));
            
            assert(all([ ...
                strcmp(Cr.AsymmetricUnit(2).Element, 'H'), ...
                abs(Cr.AsymmetricUnit(2).xAccuracy - 1e-4) < 1e-8, ...
                abs(Cr.AsymmetricUnit(2).yAccuracy - 1e-4) < 1e-8, ...
                abs(Cr.AsymmetricUnit(2).zAccuracy - 1e-4) < 1e-8, ...
                abs(Cr.AsymmetricUnit(2).SOF - 1) < 1e-8, ...
                abs(Cr.AsymmetricUnit(2).U - 0.05) < 1e-8, ...
                abs(Cr.AsymmetricUnit(2).x - 0.666700) < Cr.AsymmetricUnit(2).xAccuracy, ...
                abs(Cr.AsymmetricUnit(2).y - 0.333300) < Cr.AsymmetricUnit(2).yAccuracy, ...
                abs(Cr.AsymmetricUnit(2).z - 0.0000) < Cr.AsymmetricUnit(2).zAccuracy ...
                ]));
            
            assert(all([ ...
                strcmp(Cr.AsymmetricUnit(3).Element, 'S'), ...
                abs(Cr.AsymmetricUnit(3).xAccuracy - 1e-3) < 1e-8, ...
                abs(Cr.AsymmetricUnit(3).yAccuracy - 1e-3) < 1e-8, ...
                abs(Cr.AsymmetricUnit(3).zAccuracy - 1e-3) < 1e-8, ...
                abs(Cr.AsymmetricUnit(3).SOF - 1) < 1e-8, ...
                abs(Cr.AsymmetricUnit(3).U - 0.05) < 1e-8, ...
                abs(Cr.AsymmetricUnit(3).x - 0.333) < Cr.AsymmetricUnit(3).xAccuracy, ...
                abs(Cr.AsymmetricUnit(3).y - 0.000) < Cr.AsymmetricUnit(3).yAccuracy, ...
                abs(Cr.AsymmetricUnit(3).z - 0.666) < Cr.AsymmetricUnit(3).zAccuracy ...
                ]));
            
            assert(all([ ...
                strcmp(Cr.AsymmetricUnit(4).Element, 'Na'), ...
                abs(Cr.AsymmetricUnit(4).xAccuracy - 1e-2) < 1e-8, ...
                abs(Cr.AsymmetricUnit(4).yAccuracy - 1e-5) < 1e-8, ...
                abs(Cr.AsymmetricUnit(4).zAccuracy - 1e-5) < 1e-8, ...
                abs(Cr.AsymmetricUnit(4).SOF - 1) < 1e-8, ...
                abs(Cr.AsymmetricUnit(4).U - 0.05) < 1e-8, ...
                abs(Cr.AsymmetricUnit(4).x - 0.00) < Cr.AsymmetricUnit(4).xAccuracy, ...
                abs(Cr.AsymmetricUnit(4).y - 0.6666600) < Cr.AsymmetricUnit(4).yAccuracy, ...
                abs(Cr.AsymmetricUnit(4).z - 0.3333300) < Cr.AsymmetricUnit(4).zAccuracy ...
                ]));
            
            assert(all([ ...
                strcmp(Cr.AsymmetricUnit(5).Element, 'O'), ...
                abs(Cr.AsymmetricUnit(5).xAccuracy - 1e-7) < 1e-8, ...
                abs(Cr.AsymmetricUnit(5).yAccuracy - 1e-3) < 1e-8, ...
                abs(Cr.AsymmetricUnit(5).zAccuracy - 1e-6) < 1e-8, ...
                abs(Cr.AsymmetricUnit(5).SOF - 1) < 1e-8, ...
                abs(Cr.AsymmetricUnit(5).U - 0.05) < 1e-8, ...
                abs(Cr.AsymmetricUnit(5).x - 0.6606600) < Cr.AsymmetricUnit(5).xAccuracy, ...
                abs(Cr.AsymmetricUnit(5).y - 0.000) < Cr.AsymmetricUnit(5).yAccuracy, ...
                abs(Cr.AsymmetricUnit(5).z - 0.333030) < Cr.AsymmetricUnit(5).zAccuracy ...
                ]));
            
            assert(all([ ...
                strcmp(Cr.AsymmetricUnit(6).Element, 'Li'), ...
                abs(Cr.AsymmetricUnit(6).xAccuracy - 1e-4) < 1e-8, ...
                abs(Cr.AsymmetricUnit(6).yAccuracy - 1e-6) < 1e-8, ...
                abs(Cr.AsymmetricUnit(6).zAccuracy - 1e-6) < 1e-8, ...
                abs(Cr.AsymmetricUnit(6).SOF - 1) < 1e-8, ...
                abs(Cr.AsymmetricUnit(6).U - 0.05) < 1e-8, ...
                abs(Cr.AsymmetricUnit(6).x - 0.0000) < Cr.AsymmetricUnit(6).xAccuracy, ...
                abs(Cr.AsymmetricUnit(6).y - 0.330300) < Cr.AsymmetricUnit(6).yAccuracy, ...
                abs(Cr.AsymmetricUnit(6).z - 0.606700) < Cr.AsymmetricUnit(6).zAccuracy ...
                ]));
            
            
            % check unit cell
            assert(numel(Cr.UnitCell) == 6);
            
            for iA = 1:numel(Cr.AsymmetricUnit)
                assert(strcmp(Cr.AsymmetricUnit(iA).Name, Cr.UnitCell(iA).Name))
                assert(abs(Cr.AsymmetricUnit(iA).x - Cr.UnitCell(iA).x) < 1e-8)
                assert(abs(Cr.AsymmetricUnit(iA).y - Cr.UnitCell(iA).y) < 1e-8)
                assert(abs(Cr.AsymmetricUnit(iA).z - Cr.UnitCell(iA).z) < 1e-8)
                assert(strcmp(Cr.AsymmetricUnit(iA).Element.Name, Cr.UnitCell(iA).Element.Name))
                assert(abs(Cr.AsymmetricUnit(iA).SOF - Cr.UnitCell(iA).SOF) < 1e-8)
                assert(all(abs(Cr.AsymmetricUnit(iA).U - Cr.UnitCell(iA).U) < 1e-8))
                assert(all(Cr.UnitCell(iA).SymmOp(:) == [1 0 0 0 1 0 0 0 1 0 0 0]'))
                assert(abs(Cr.AsymmetricUnit(iA).xAccuracy - Cr.UnitCell(iA).xAccuracy) < 1e-8)
                assert(abs(Cr.AsymmetricUnit(iA).yAccuracy - Cr.UnitCell(iA).yAccuracy) < 1e-8)
                assert(abs(Cr.AsymmetricUnit(iA).zAccuracy - Cr.UnitCell(iA).zAccuracy) < 1e-8)
            end
        end
        
        function MainTest(testCase, File)
            FN = File;
            ResExists = (exist([FN, '.res'], 'file') == 2);
            assert(ResExists, sprintf('DID NOT PASS StructureFactorTest FOR FILE %s.res: *.res FILE NOT FOUND', FN));
            HKLExists = (exist([FN, '.HKL'], 'file') == 2);
            assert(HKLExists, sprintf('DID NOT PASS StructureFactorTest FOR FILE %s.res: *.HKL FILE NOT FOUND', FN));
            Cr = GIDVis_Crystal([FN, '.res']);

            % read corresponding HKL file
            IM = importdata([FN, '.HKL'], ' ', 10);
            M = IM.data;
            HKL = M(:, 1:3);
            SF_PC = M(:, 7);
            TwoTheta_PC = M(:, 4);

            % get the wavelength
            StrC = strsplit(IM.textdata{2});
            lambda = str2double(StrC{end});

            % calculate the q vector of the HKLs in any orientation
            qvec = PeakPositionsQ(Cr, HKL, [1 0 0]);
            % calculate q length
            q = sqrt(sum(qvec.^2, 2));
            % calculate the 2Theta angle
            TwoTheta = 2.*asind(lambda.*q./(4*pi));

            % compare the 2Theta angle (3 decimal places)
            L = abs(TwoTheta - TwoTheta_PC) > 0.001;
            assert(~any(L), sprintf('DID NOT PASS 2Theta Test FOR FILE %s.res: %d/%d deviating >= 0.01.\n', FN, sum(L), numel(L)));

            % in case of anisotropic U factors, we change their order so
            % that they are ordered as in PowderCell.
            % According to the SHELXL instructions
            % (http://shelx.uni-ac.gwdg.de/SHELX/shelxl_html.php), the
            % order of Uij is U11 U22 U33 U23 U13 U12, PowderCell uses U11
            % U22 U33 U12 U13 U23 instead.
            % GIDVis implements the U11 U22 U33 U23 U13 U12 ordering. To
            % compare the results with results from GIDVis, we will reorder
            % them here.
            for iUC = 1:numel(Cr.UnitCell)
                if numel(Cr.UnitCell(iUC).U) > 1
                    Cr.UnitCell(iUC).U(4:6) = fliplr(Cr.UnitCell(iUC).U(4:6));
                end
            end

            % calculate structure factors
            SF = abs(Cr.GetStructureFactors(HKL));

            if isempty(SF)
                assert(~isempty(SF), sprintf('DID NOT PASS StructureFactorTest FOR FILE %s.res: NO STRUCTURE FACTORS CALCULATED.\n', FN));
            end

            % PowderCell gives structure factors only with 2 decimal
            % places, check for identity only for up to two decimal places
            Deviation = SF_PC-SF;
            L = abs(Deviation) >= 0.01;
            assert(~any(L), sprintf('DID NOT PASS StructureFactorTest FOR FILE %s.res: %d/%d deviating >= 0.01.\n', FN, sum(L), numel(L)));
        end
        
        function RotationTest(testCase)
            % Tests the structure factor calculation including rotation
            
            % create a tetragonal unit cell with an atom in the unit cell -
            % which atom and which position does not matter
            Cr = GIDVis_Crystal(5, 5, 10, 90, 90, 90, 'Test');
            Cr.UnitCell = GIDVis_Crystal_Atom;
            Cr.UnitCell.Name = 'Cu';
            Cr.UnitCell.x = 0;
            Cr.UnitCell.y = 0;
            Cr.UnitCell.z = 0;
            Cr.UnitCell.Element = ChEl('Cu');
            Cr.UnitCell.SOF = 1;
            Cr.UnitCell.U = 0.05;
            
            % Orientation so that long unit cell axis is parallel to
            % substrate
            Orientation = [1 0 0];
            
            % Since the c-axis is twice as long as the b-axis, the peaks
            % 002 and 010 should have the same equivalent peaks and the
            % same summed intensity values
            hkl = [0 0 2; 0 1 0];
            
            % calculate the summed |SF|^2 and the equivalent peaks
            [SFSummedFull, EqFull] = GetStructureFactors2Rot(Cr, hkl, Orientation);
            
            % the |SF|^2 have to be the same
            assert(abs(SFSummedFull(1) - SFSummedFull(2)) < 1e-8);
            
            % the equivalent peaks have to be the same
            assert(isempty(setdiff(EqFull{1}, EqFull{2})));
            
            % repeat the test with the c-axis being three times the b-axis
            Cr.c = 15;
            
            % now the 003 and 010 should have the same equivalent peaks and
            % the same summed intensity values
            hkl = [0 0 3; 0 1 0];
            
            % calculate the summed |SF|^2 and the equivalent peaks
            [SFSummedFull, EqFull] = GetStructureFactors2Rot(Cr, hkl, Orientation);
            
            % the |SF|^2 have to be the same
            assert(abs(SFSummedFull(1) - SFSummedFull(2)) < 1e-8);
            
            % the equivalent peaks have to be the same
            assert(isempty(setdiff(EqFull{1}, EqFull{2})));
            
            % in case of a hexagonal unit cell in [0 0 1] orientation, the
            % 101 peak appears 4 times (-101 0-11 011 101). However, the
            % -111 and 1-11 peaks appear also at the position of the 101
            % peak
            Cr.gamma = 120;
            Orientation = [0 0 1];
            
            % create hkl values
            hkl = [1 0 1; -1 1 1];
            
            % calculate the summed |SF|^2 and the equivalent peaks
            [SFSummedFull, EqFull] = GetStructureFactors2Rot(Cr, hkl, Orientation);
            
            % the |SF|^2 have to be the same
            assert(abs(SFSummedFull(1) - SFSummedFull(2)) < 1e-8);
            
            % the equivalent peaks have to be the same
            assert(isempty(setdiff(EqFull{1}, EqFull{2})));
        end
        
        function OrientationTest(testCase)
            % Testing of orientation and peak positions. The peaks
            % calculated should not vary with the orientation or general
            % rotation (except in phi)
            
            % Create a crystal
            Cr = GIDVis_Crystal(5.067, 8.064, 8.884, 91.64, 93.3, 94.01, '');
            
            % define an orientation
            Orientation = [1 4 3];
            
            % create some hkl indices
            hkl = Cr.CreateHKLCombinations(-15:15);
            
            % calculate the peak positions
            q1 = PeakPositionsQ(Cr, hkl, Orientation);
            [psi1, phi1] = PeakPositionsA(Cr, hkl, Orientation);
            
            % Orient the crystal
            Cr.Orient(Orientation);
            
            % calculate the peak positions
            q2 = PeakPositionsQ(Cr, hkl, Orientation);
            [psi2, phi2] = PeakPositionsA(Cr, hkl, Orientation);
            
            % check identity of q values and inclination
            assert(all(abs(q1(:) - q2(:)) < 1e-8))
            assert(all(abs(psi1(:) - psi2(:)) < 1e-8))
            
            % due to rounding errors, the phi values calculated from the
            % unrotated crystal will not be the same for the rotated
            % crystal when the hkl is a multiple of the orientation.
            % Exclude these edge cases from the comparison.
            L = all(mod(hkl, Orientation) == 0, 2);
            assert(all(abs(phi1(~L) - phi2(~L)) < 1e-8))
            
            % general rotations
            Cr = GIDVis_Crystal(5.067, 8.064, 8.884, 91.64, 93.3, 94.01, '');
            
            % create some hkl indices
            hkl = Cr.CreateHKLCombinations(-15:15);
            
            Orientation = [-1 4 -3];
            
            % calculate the peak positions
            q1 = PeakPositionsQ(Cr, hkl, Orientation);
            psi1 = PeakPositionsA(Cr, hkl, Orientation);
            
            % create a rotation matrix
            R = Cr.RotMat([3.4 1.6 -2.3], [0.3 8.4 -3.7]);
            
            % rotate the crystal
            Cr.Rotate(R);
            
            % calculate the peak positions
            q2 = PeakPositionsQ(Cr, hkl, Orientation);
            psi2 = PeakPositionsA(Cr, hkl, Orientation);
            
            % the phi values will not be the same, i.e. the qx and qy
            % values will not be the same. Check qxy^2 and qz instead.
            assert(all(abs((q1(:, 1).^2 + q1(:, 2).^2) - (q2(:, 1).^2 + q2(:, 2).^2)) < 1e-8))
            assert(all(abs(q1(:, 3) - q2(:, 3)) < 1e-8))
            assert(all(abs(psi1(:) - psi2(:)) < 1e-8))
        end
    end
end