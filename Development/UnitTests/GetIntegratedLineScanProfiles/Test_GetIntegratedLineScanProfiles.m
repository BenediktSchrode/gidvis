% Unit tests for the function GetIntegratedLineScanProfiles
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2018 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Test 1: horizontal and vertical scan
x = linspace(0, 2, 100);
y = linspace(0, 2, 100);
A = 1;
x0 = 0.5;
y0 = 0.7;
sigmax = 0.4;
sigmay = 0.7;

[X, Y] = meshgrid(x, y);

Z = A.*exp(-((X-x0).^2./(2*sigmax^2)+(Y-y0).^2./(2*sigmay^2)));

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 2 2]);
assert(~isempty(cfun1) && ~isempty(DeltaFit1) && ~isempty(ResString1))
assert(~isempty(cfun2) && ~isempty(DeltaFit2) && ~isempty(ResString2))

assert(all(abs([x0 sigmax] - [cfun1.mu cfun1.sigma]) < 1e-8))
assert(all(abs([y0 sigmay] - [cfun2.mu cfun2.sigma]) < 1e-8))

%% Test 2: NaN/inf
x = linspace(0, 2, 100);
y = linspace(0, 2, 100);
A = 1;
x0 = 0.5;
y0 = 0.7;
sigmax = 0.4;
sigmay = 0.7;

[X, Y] = meshgrid(x, y);
Pos = [1, 1; numel(x) numel(y); 5 7; 7 5; 33 56];

for iP = 1:size(Pos, 1)
    Z = A.*exp(-((X-x0).^2./(2*sigmax^2)+(Y-y0).^2./(2*sigmay^2)));
    Z(Pos(iP, 1), Pos(iP, 2)) = NaN;
    [xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 2 2]);
    assert(~isempty(cfun1) && ~isempty(DeltaFit1) && ~isempty(ResString1))
    assert(~isempty(cfun2) && ~isempty(DeltaFit2) && ~isempty(ResString2))
    assert(numel(xc) == numel(zc1))
    assert(numel(yc) == numel(zc2))
    
    Z = A.*exp(-((X-x0).^2./(2*sigmax^2)+(Y-y0).^2./(2*sigmay^2)));
    Z(Pos(iP, 1), :) = NaN;
    [xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 2 2]);
    assert(~isempty(cfun1) && ~isempty(DeltaFit1) && ~isempty(ResString1))
    assert(~isempty(cfun2) && ~isempty(DeltaFit2) && ~isempty(ResString2))
    assert(numel(xc) == numel(zc1))
    assert(numel(yc) == numel(zc2))
    
    Z = A.*exp(-((X-x0).^2./(2*sigmax^2)+(Y-y0).^2./(2*sigmay^2)));
    Z(:, Pos(iP, 2)) = NaN;
    [xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 2 2]);
    assert(~isempty(cfun1) && ~isempty(DeltaFit1) && ~isempty(ResString1))
    assert(~isempty(cfun2) && ~isempty(DeltaFit2) && ~isempty(ResString2))
    assert(numel(xc) == numel(zc1))
    assert(numel(yc) == numel(zc2))
end

for iP = 1:size(Pos, 1)
    Z = A.*exp(-((X-x0).^2./(2*sigmax^2)+(Y-y0).^2./(2*sigmay^2)));
    Z(Pos(iP, 1), Pos(iP, 2)) = inf;
    [xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 2 2]);
    assert(~isempty(cfun1) && ~isempty(DeltaFit1) && ~isempty(ResString1))
    assert(~isempty(cfun2) && ~isempty(DeltaFit2) && ~isempty(ResString2))
    assert(numel(xc) == numel(zc1))
    assert(numel(yc) == numel(zc2))
    
    Z = A.*exp(-((X-x0).^2./(2*sigmax^2)+(Y-y0).^2./(2*sigmay^2)));
    Z(Pos(iP, 1), :) = inf;
    [xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 2 2]);
    assert(isempty(cfun1) && isempty(DeltaFit1) && isempty(ResString1))
    assert(~isempty(cfun2) && ~isempty(DeltaFit2) && ~isempty(ResString2))
    assert(numel(xc) == numel(zc1))
    assert(numel(yc) == numel(zc2))
    
    Z = A.*exp(-((X-x0).^2./(2*sigmax^2)+(Y-y0).^2./(2*sigmay^2)));
    Z(:, Pos(iP, 2)) = inf;
    [xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 2 2]);
    assert(~isempty(cfun1) && ~isempty(DeltaFit1) && ~isempty(ResString1))
    assert(isempty(cfun2) && isempty(DeltaFit2) && isempty(ResString2))
    assert(numel(xc) == numel(zc1))
    assert(numel(yc) == numel(zc2))
end

Z = NaN(size(X));
[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 2 2]);
assert(isempty(cfun1) && isempty(DeltaFit1) && isempty(ResString1))
assert(isempty(cfun2) && isempty(DeltaFit2) && isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

Z = inf(size(X));
[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 2 2]);
assert(isempty(cfun1) && isempty(DeltaFit1) && isempty(ResString1))
assert(isempty(cfun2) && isempty(DeltaFit2) && isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

%% Test 3: Small regions
x = linspace(0, 2, 100);
y = linspace(0, 2, 100);
A = 1;
x0 = 0.5;
y0 = 0.7;
sigmax = 0.4;
sigmay = 0.7;

[X, Y] = meshgrid(x, y);

Z = A.*exp(-((X-x0).^2./(2*sigmax^2)+(Y-y0).^2./(2*sigmay^2)));

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 0.03 2]);
assert(isempty(cfun1) && isempty(DeltaFit1) && isempty(ResString1))
assert(~isempty(cfun2) && ~isempty(DeltaFit2) && ~isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 2 0.03]);
assert(~isempty(cfun1) && ~isempty(DeltaFit1) && ~isempty(ResString1))
assert(isempty(cfun2) && isempty(DeltaFit2) && isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 0.002 2]);
assert(isempty(cfun1) && isempty(DeltaFit1) && isempty(ResString1))
assert(~isempty(cfun2) && ~isempty(DeltaFit2) && ~isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 2 0.002]);
assert(~isempty(cfun1) && ~isempty(DeltaFit1) && ~isempty(ResString1))
assert(isempty(cfun2) && isempty(DeltaFit2) && isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 0.03 0.03]);
assert(isempty(cfun1) && isempty(DeltaFit1) && isempty(ResString1))
assert(isempty(cfun2) && isempty(DeltaFit2) && isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 0.02 0.02]);
assert(isempty(cfun1) && isempty(DeltaFit1) && isempty(ResString1))
assert(isempty(cfun2) && isempty(DeltaFit2) && isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 0 2]);
assert(isempty(cfun1) && isempty(DeltaFit1) && isempty(ResString1))
assert(~isempty(cfun2) && ~isempty(DeltaFit2) && ~isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 2 0]);
assert(~isempty(cfun1) && ~isempty(DeltaFit1) && ~isempty(ResString1))
assert(isempty(cfun2) && isempty(DeltaFit2) && isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

%% Test 4: Small regions in combination with inf
x = linspace(0, 2, 100);
y = linspace(0, 2, 100);
A = 1;
x0 = 0.5;
y0 = 0.7;
sigmax = 0.4;
sigmay = 0.7;

[X, Y] = meshgrid(x, y);

Z = A.*exp(-((X-x0).^2./(2*sigmax^2)+(Y-y0).^2./(2*sigmay^2)));
Z(1, 1) = inf;

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 0.03 2]);
assert(isempty(cfun1) && isempty(DeltaFit1) && isempty(ResString1))
assert(~isempty(cfun2) && ~isempty(DeltaFit2) && ~isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 2 0.03]);
assert(~isempty(cfun1) && ~isempty(DeltaFit1) && ~isempty(ResString1))
assert(isempty(cfun2) && isempty(DeltaFit2) && isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 0.002 2]);
assert(isempty(cfun1) && isempty(DeltaFit1) && isempty(ResString1))
assert(~isempty(cfun2) && ~isempty(DeltaFit2) && ~isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 2 0.002]);
assert(~isempty(cfun1) && ~isempty(DeltaFit1) && ~isempty(ResString1))
assert(isempty(cfun2) && isempty(DeltaFit2) && isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 0.03 0.03]);
assert(isempty(cfun1) && isempty(DeltaFit1) && isempty(ResString1))
assert(isempty(cfun2) && isempty(DeltaFit2) && isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 0.02 0.02]);
assert(isempty(cfun1) && isempty(DeltaFit1) && isempty(ResString1))
assert(isempty(cfun2) && isempty(DeltaFit2) && isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 0 2]);
assert(isempty(cfun1) && isempty(DeltaFit1) && isempty(ResString1))
assert(~isempty(cfun2) && ~isempty(DeltaFit2) && ~isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 2 0]);
assert(~isempty(cfun1) && ~isempty(DeltaFit1) && ~isempty(ResString1))
assert(isempty(cfun2) && isempty(DeltaFit2) && isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))


Z = A.*exp(-((X-x0).^2./(2*sigmax^2)+(Y-y0).^2./(2*sigmay^2)));
Z(15, :) = inf;

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 0.03 2]);
assert(isempty(cfun1) && isempty(DeltaFit1) && isempty(ResString1))
assert(~isempty(cfun2) && ~isempty(DeltaFit2) && ~isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 2 0.03]);
assert(~isempty(cfun1) && ~isempty(DeltaFit1) && ~isempty(ResString1))
assert(isempty(cfun2) && isempty(DeltaFit2) && isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 0.002 2]);
assert(isempty(cfun1) && isempty(DeltaFit1) && isempty(ResString1))
assert(~isempty(cfun2) && ~isempty(DeltaFit2) && ~isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 2 0.002]);
assert(~isempty(cfun1) && ~isempty(DeltaFit1) && ~isempty(ResString1))
assert(isempty(cfun2) && isempty(DeltaFit2) && isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 0.03 0.03]);
assert(isempty(cfun1) && isempty(DeltaFit1) && isempty(ResString1))
assert(isempty(cfun2) && isempty(DeltaFit2) && isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 0.02 0.02]);
assert(isempty(cfun1) && isempty(DeltaFit1) && isempty(ResString1))
assert(isempty(cfun2) && isempty(DeltaFit2) && isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 0 2]);
assert(isempty(cfun1) && isempty(DeltaFit1) && isempty(ResString1))
assert(~isempty(cfun2) && ~isempty(DeltaFit2) && ~isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 2 0]);
assert(~isempty(cfun1) && ~isempty(DeltaFit1) && ~isempty(ResString1))
assert(isempty(cfun2) && isempty(DeltaFit2) && isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))


Z = A.*exp(-((X-x0).^2./(2*sigmax^2)+(Y-y0).^2./(2*sigmay^2)));
Z(:, 15) = inf;

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 0.03 2]);
assert(isempty(cfun1) && isempty(DeltaFit1) && isempty(ResString1))
assert(~isempty(cfun2) && ~isempty(DeltaFit2) && ~isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 2 0.03]);
assert(~isempty(cfun1) && ~isempty(DeltaFit1) && ~isempty(ResString1))
assert(isempty(cfun2) && isempty(DeltaFit2) && isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 0.002 2]);
assert(isempty(cfun1) && isempty(DeltaFit1) && isempty(ResString1))
assert(~isempty(cfun2) && ~isempty(DeltaFit2) && ~isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 2 0.002]);
assert(~isempty(cfun1) && ~isempty(DeltaFit1) && ~isempty(ResString1))
assert(isempty(cfun2) && isempty(DeltaFit2) && isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 0.03 0.03]);
assert(isempty(cfun1) && isempty(DeltaFit1) && isempty(ResString1))
assert(isempty(cfun2) && isempty(DeltaFit2) && isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 0.02 0.02]);
assert(isempty(cfun1) && isempty(DeltaFit1) && isempty(ResString1))
assert(isempty(cfun2) && isempty(DeltaFit2) && isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 0 2]);
assert(isempty(cfun1) && isempty(DeltaFit1) && isempty(ResString1))
assert(~isempty(cfun2) && ~isempty(DeltaFit2) && ~isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 2 0]);
assert(~isempty(cfun1) && ~isempty(DeltaFit1) && ~isempty(ResString1))
assert(isempty(cfun2) && isempty(DeltaFit2) && isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))


%% Test 5: Small regions in combination with NaN
x = linspace(0, 2, 100);
y = linspace(0, 2, 100);
A = 1;
x0 = 0.5;
y0 = 0.7;
sigmax = 0.4;
sigmay = 0.7;

[X, Y] = meshgrid(x, y);

Z = A.*exp(-((X-x0).^2./(2*sigmax^2)+(Y-y0).^2./(2*sigmay^2)));
Z(1, 1) = NaN;

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 0.03 2]);
assert(isempty(cfun1) && isempty(DeltaFit1) && isempty(ResString1))
assert(~isempty(cfun2) && ~isempty(DeltaFit2) && ~isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 2 0.03]);
assert(~isempty(cfun1) && ~isempty(DeltaFit1) && ~isempty(ResString1))
assert(isempty(cfun2) && isempty(DeltaFit2) && isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 0.002 2]);
assert(isempty(cfun1) && isempty(DeltaFit1) && isempty(ResString1))
assert(~isempty(cfun2) && ~isempty(DeltaFit2) && ~isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 2 0.002]);
assert(~isempty(cfun1) && ~isempty(DeltaFit1) && ~isempty(ResString1))
assert(isempty(cfun2) && isempty(DeltaFit2) && isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 0.03 0.03]);
assert(isempty(cfun1) && isempty(DeltaFit1) && isempty(ResString1))
assert(isempty(cfun2) && isempty(DeltaFit2) && isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 0.02 0.02]);
assert(isempty(cfun1) && isempty(DeltaFit1) && isempty(ResString1))
assert(isempty(cfun2) && isempty(DeltaFit2) && isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 0 2]);
assert(isempty(cfun1) && isempty(DeltaFit1) && isempty(ResString1))
assert(~isempty(cfun2) && ~isempty(DeltaFit2) && ~isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 2 0]);
assert(~isempty(cfun1) && ~isempty(DeltaFit1) && ~isempty(ResString1))
assert(isempty(cfun2) && isempty(DeltaFit2) && isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))


Z = A.*exp(-((X-x0).^2./(2*sigmax^2)+(Y-y0).^2./(2*sigmay^2)));
Z(15, :) = NaN;

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 0.03 2]);
assert(isempty(cfun1) && isempty(DeltaFit1) && isempty(ResString1))
assert(~isempty(cfun2) && ~isempty(DeltaFit2) && ~isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 2 0.03]);
assert(~isempty(cfun1) && ~isempty(DeltaFit1) && ~isempty(ResString1))
assert(isempty(cfun2) && isempty(DeltaFit2) && isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 0.002 2]);
assert(isempty(cfun1) && isempty(DeltaFit1) && isempty(ResString1))
assert(~isempty(cfun2) && ~isempty(DeltaFit2) && ~isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 2 0.002]);
assert(~isempty(cfun1) && ~isempty(DeltaFit1) && ~isempty(ResString1))
assert(isempty(cfun2) && isempty(DeltaFit2) && isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 0.03 0.03]);
assert(isempty(cfun1) && isempty(DeltaFit1) && isempty(ResString1))
assert(isempty(cfun2) && isempty(DeltaFit2) && isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 0.02 0.02]);
assert(isempty(cfun1) && isempty(DeltaFit1) && isempty(ResString1))
assert(isempty(cfun2) && isempty(DeltaFit2) && isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 0 2]);
assert(isempty(cfun1) && isempty(DeltaFit1) && isempty(ResString1))
assert(~isempty(cfun2) && ~isempty(DeltaFit2) && ~isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 2 0]);
assert(~isempty(cfun1) && ~isempty(DeltaFit1) && ~isempty(ResString1))
assert(isempty(cfun2) && isempty(DeltaFit2) && isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))


Z = A.*exp(-((X-x0).^2./(2*sigmax^2)+(Y-y0).^2./(2*sigmay^2)));
Z(:, 15) = NaN;

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 0.03 2]);
assert(isempty(cfun1) && isempty(DeltaFit1) && isempty(ResString1))
assert(~isempty(cfun2) && ~isempty(DeltaFit2) && ~isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 2 0.03]);
assert(~isempty(cfun1) && ~isempty(DeltaFit1) && ~isempty(ResString1))
assert(isempty(cfun2) && isempty(DeltaFit2) && isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 0.002 2]);
assert(isempty(cfun1) && isempty(DeltaFit1) && isempty(ResString1))
assert(~isempty(cfun2) && ~isempty(DeltaFit2) && ~isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 2 0.002]);
assert(~isempty(cfun1) && ~isempty(DeltaFit1) && ~isempty(ResString1))
assert(isempty(cfun2) && isempty(DeltaFit2) && isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 0.03 0.03]);
assert(isempty(cfun1) && isempty(DeltaFit1) && isempty(ResString1))
assert(isempty(cfun2) && isempty(DeltaFit2) && isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 0.02 0.02]);
assert(isempty(cfun1) && isempty(DeltaFit1) && isempty(ResString1))
assert(isempty(cfun2) && isempty(DeltaFit2) && isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 0 2]);
assert(isempty(cfun1) && isempty(DeltaFit1) && isempty(ResString1))
assert(~isempty(cfun2) && ~isempty(DeltaFit2) && ~isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))

[xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(x, y, Z, [0 0 2 0]);
assert(~isempty(cfun1) && ~isempty(DeltaFit1) && ~isempty(ResString1))
assert(isempty(cfun2) && isempty(DeltaFit2) && isempty(ResString2))
assert(numel(xc) == numel(zc1))
assert(numel(yc) == numel(zc2))