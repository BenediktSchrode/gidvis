% Unit tests for the function GetLineProfile
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2018 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Test 1: horizontal and vertical scan
x = linspace(0, 2, 100);
y = linspace(0, 2, 100);
A = 1;
x0 = 0.5;
y0 = 0.5;
sigmax = 0.4;
sigmay = 0.6;

[X, Y] = meshgrid(x, y);

Z = A.*exp(-((X-x0).^2./(2*sigmax^2)+(Y-y0).^2./(2*sigmay^2)));

[xc, zc, cfun, deltaCoeff, ResString] = GetLineProfile(x, y, Z, [0 y0], [2 y0]);

assert(~isempty(cfun))
assert(all(abs([x0 sigmax] - [cfun.mu cfun.sigma]) < 1e-8));

[xc, zc, cfun, deltaCoeff, ResString] = GetLineProfile(x, y, Z, [x0 0], [x0 2]);

assert(~isempty(cfun))
assert(all(abs([y0 sigmay] - [cfun.mu cfun.sigma]) < 1e-8));

%% Test 2: diagonal scan
x = linspace(0, 2, 100);
y = linspace(0, 2, 100);
A = 1;
x0 = 0.5;
y0 = 0.5;
sigmax = 0.4;
sigmay = 0.4;

[X, Y] = meshgrid(x, y);

Z = A.*exp(-((X-x0).^2./(2*sigmax^2)+(Y-y0).^2./(2*sigmay^2)));

[xc, zc, cfun, deltaCoeff, ResString] = GetLineProfile(x, y, Z, [0 0], [2 2]);

assert(~isempty(cfun));
assert(all(abs([sqrt(x0^2+y0^2) sigmax sigmay] == [cfun.mu cfun.sigma cfun.sigma]) < 1e-8));
y = linspace(0, 2, 100);