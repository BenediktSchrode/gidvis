% testing of the function IlluminatedArea.m
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

BL = ExperimentalSetUp;
alpha_i = 2;
SampleWidth = 20;
SampleLength = 20;
phi = 0:2:20;

FPC = IlluminatedArea(alpha_i, SampleWidth, SampleLength, phi, BL.BeamHeight, BL.BeamWidth);
assert(all(abs(FPC - 1.14614833) < 1e-8))

alpha_i = 0.2;
phi = 0:90:360;
FPC = IlluminatedArea(alpha_i, SampleWidth, SampleLength, phi, BL.BeamHeight, BL.BeamWidth);
assert(all(abs(FPC - 4) < 1e-8))

alpha_i = 0.2;
phi = 0:90:360;
FPC = IlluminatedArea(alpha_i, SampleWidth, SampleLength*2, phi, BL.BeamHeight, BL.BeamWidth);
assert(all(abs(FPC - [8 4 8 4 8]) < 1e-8))

alpha_i = 0.2;
phi = 0:90:360;
FPC = IlluminatedArea(alpha_i, SampleWidth*2, SampleLength, phi, BL.BeamHeight, BL.BeamWidth);
assert(all(abs(FPC - [4 8 4 8 4]) < 1e-8))

alpha_i = 0.45;
phi = [0 15:2:35 45 90];
BL.BeamWidth = 10;
FPC = IlluminatedArea(alpha_i, SampleWidth, SampleLength, phi, BL.BeamHeight, BL.BeamWidth);
Expected = [200, 207.05523608, 209.13835130, 211.52413624, 214.22899873, 217.12074102, 219.81687043, 222.21208385, 224.05592454, 225.46328493, 226.53334122, 227.33957893, 228.86871585, 200];
assert(all(abs(FPC - Expected) < 1e-8))