% testing of the ImASF function and the AtomicWeight data
%
% This file is part of GIDVis.
%
% To run this test, refrac by Zhang Jiang
% (https://de.mathworks.com/matlabcentral/fileexchange/6026-toolbox--x-ray-refraction-of-matter)
% has to be on the MATLAB search path.
% 
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Input tests
Formula = 'H2ONVRhCs3Ag56O12FEr';
Energy = 8000;
Method = 'spline';

% should work with two input arguments
S1 = ImASF(Formula, Energy);
% should work with three input arguments
S2 = ImASF(Formula, Energy, 'spline');

% check if the third input argument was correctly defaulted to 'spline'
for iS = 1:numel(S1)
    assert(S1(iS).f2 == S2(iS).f2)
    assert(S1(iS).N == S2(iS).N)
    assert(S1(iS).AtomicWeight == S2(iS).AtomicWeight)
    assert(strcmp(S1(iS).Element, S2(iS).Element))
end

%% Method input test
% not testing cubic because this will change in a future MATLAB release.
% not testing v5cubic because it requires the grid to have uniform spacing.
Methods = {'linear', 'nearest', 'next', 'previous', 'pchip', ...
    'makima', 'spline'};
Formula = 'H2ONVRhCs3Ag56O12FEr';
Energy = 8000;

for iM = 1:numel(Methods)
    ImASF(Formula, Energy, Methods{iM});
end

%% Value tests

% make sure that refrac is on the MATLAB path
try
    S = refrac('SiO2', 8, 1);
catch ex
    error('GIDVis:Test_ImASF', ex.message);
end

% make some test energies (also including energies outside valid range of
% 10 - 30000 eV)
Energies= [32000 1 8047.78 3000 30 500 15000 25000 1700 12000];
% make some test formulas and add all chemical elements available in
% Henke's scattering factors collection
Formulas = {'H2O'; 'CO2'; 'Co2'; 'N78O21Ar1'; 'Si'; 'Ag'; 'CuO2H2'; ...
    'SiO2'; 'C22H12O2'; 'H'; 'He'; 'Li'; 'Be'; 'B'; 'C'; 'N'; ...
    'O'; 'F'; 'Ne'; 'Na'; 'Mg'; 'Al'; 'Si'; 'P'; 'S'; 'Cl'; ...
    'Ar'; 'K'; 'Ca'; 'Sc'; 'Ti'; 'V'; 'Cr'; 'Mn'; 'Fe'; 'Co'; ...
    'Ni'; 'Cu'; 'Zn'; 'Ga'; 'Ge'; 'As'; 'Se'; 'Br'; 'Kr'; 'Rb'; ...
    'Sr'; 'Y'; 'Zr'; 'Nb'; 'Mo'; 'Tc'; 'Ru'; 'Rh'; 'Pd'; 'Ag'; ...
    'Cd'; 'In'; 'Sn'; 'Sb'; 'Te'; 'I'; 'Xe'; 'Cs'; 'Ba'; 'La'; ...
    'Ce'; 'Pr'; 'Nd'; 'Pm'; 'Sm'; 'Eu'; 'Gd'; 'Tb'; 'Dy'; 'Ho'; ...
    'Er'; 'Tm'; 'Yb'; 'Lu'; 'Hf'; 'Ta'; 'W'; 'Re'; 'Os'; 'Ir'; ...
    'Pt'; 'Au'; 'Hg'; 'Tl'; 'Pb'; 'Bi'; 'Po'; 'At'; 'Rn'; 'Fr'; ...
    'Ra'; 'Ac'; 'Th'; 'Pa'; 'U'};

% test will run on all possible combinations of energies and formulas
[mgE, mgF] = meshgrid(1:numel(Energies), 1:numel(Formulas));

for ind = 1:numel(mgE)
    % reset the error variable
    ImASFError = false;
    % in case the energy is outside the range, there should be an error
    try
        % calculate ImASF properties using pchip method to be comparable to
        % refrac
        S = ImASF(Formulas{mgF(ind)}, Energies(mgE(ind)), 'pchip');
    catch
        ImASFError = true;
    end

    % reset the error variable
    refracError = false;
    % in case the energy is outside the range, there should be an error
    try
        % calculate the refraction properties using refrac
        ref = refrac(Formulas{mgF(ind)}, Energies(mgE(ind))/1000, 1);
    catch
        refracError = true;
    end
    
    % check if the error status is the same
    assert(ImASFError == refracError, 'Failed using energy %g, Formula %s: One test threw an error, the other one not.', Energies(mgE(ind)), Formulas{mgF(ind)});
    % if there was no error, calculate and compare the final f2 value
    if ~ImASFError
        f2 = sum([S.f2].*[S.N]);
        assert(abs(f2-ref.f2) < 1e-8, 'Failed f2 using energy %g, Formula %s: %g', Energies(mgE(ind)), Formulas{mgF(ind)}, abs(f2-ref.f2))
        FullWeight = sum([S.AtomicWeight].*[S.N]);
        % the 0.004 is kind of arbitrary to pass the test with the refrac
        % data. It should be good enough to rule out large errors.
        assert(abs(1-FullWeight/ref.molecularWeight) < 0.004, 'Failed atomic weight for formula %s: %g', Formulas{mgF(ind)}, abs(1-FullWeight/ref.molecularWeight))
    end
end