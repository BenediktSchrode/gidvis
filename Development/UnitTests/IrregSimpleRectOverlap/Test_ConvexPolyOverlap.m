% testing of the ConvexPolyOverlap function
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2018 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

P1 = [1 -1; 1 1; -1 1; -1 -1];
P1Closed = [P1(end, :); P1];
R = @(alpha) [cosd(alpha) -sind(alpha); sind(alpha) cosd(alpha)];

%% Test 1: Simple Rectangles
M = ConvexPolyOverlap(P1, P1.*2);
assert(all(M(:) == P1Closed(:)));

M = ConvexPolyOverlap(P1.*2, P1);
assert(all(M(:) == P1Closed(:)));

M = ConvexPolyOverlap(P1, P1);
assert(all(M(:) == P1Closed(:)));

M = ConvexPolyOverlap([0 0; 0 2; -1 2; -1 0], [1 -1; 1 1; 0 1; 0 -1]);
assert(all(M(:) == [0 0 0 0 1 0]'))

M = ConvexPolyOverlap(P1, P1+2);
assert(all(M(:) == [1 1]'))

M = ConvexPolyOverlap(P1, P1+3);
assert(isempty(M))

%% Test 2: Rotated Rectangles
M = ConvexPolyOverlap(P1, P1*R(45).*sqrt(2));
assert(all(M(:) == P1Closed(:)))

M = ConvexPolyOverlap(P1, P1*R(45).*sqrt(2)+1);
assert(all(abs(M(:) - [1 1 -1 1 -1 1 1 -1]') < eps))

%% Test 3: Irregular Rectangles
M = ConvexPolyOverlap([0.5 -0.4; 0.6 0.3; -0.2 0.1; -1 -0.2], ...
    [0 0; 0.2 0.4; -0.4 0.4; -0.6 -0.3]);
M2 = [-0.58715596	-0.25504587; ...
    -0.52631579	-0.26315789; ...
    0 0; ...
    0.08571429	0.17142857; ...
    -0.2 0.1; ...
    -0.52000000	-0.02000000; ...
    -0.58715596	-0.25504587];
all(abs(M(:)-M2(:)) < 1e-6);
% M = IrregRectOverlap(P1.*0.4, P2);