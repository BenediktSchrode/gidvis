% testing of PeakFind.m
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2018 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

f = figure('Visible', 'off');
ax = axes('Parent', f);

%% Test 1: Regular data
x = linspace(0, 2, 100);
y = linspace(0, 2, 100);
A = 37;
x0 = 0.75;
y0 = 0.4;
sigmax = 0.4;
sigmay = 0.6;

[X, Y] = meshgrid(x, y);

Z = A.*exp(-((X-x0).^2./(2*sigmax^2)+(Y-y0).^2./(2*sigmay^2)));

PF = PeakFind(X(:), Y(:), Z(:), 1, 1, 2, 2, ax);
PF.PerformFit();

assert(~isempty(PF.FitResult))
M1 = [PF.FitResult.A PF.FitResult.sigmax PF.FitResult.sigmay ...
    PF.FitResult.x0 PF.FitResult.y0];
M2 = [A sigmax sigmay x0 y0];

assert(all(abs(M1 - M2) < 1e-6));

%% Test 2: low scattered data
x = logspace(0, 1, 100)./5;
y = linspace(0, 2, 100);
A = 37;
x0 = 0.75;
y0 = 0.4;
sigmax = 0.4;
sigmay = 0.6;

[X, Y] = meshgrid(x, y);

Z = A.*exp(-((X-x0).^2./(2*sigmax^2)+(Y-y0).^2./(2*sigmay^2)));

PF = PeakFind(X(:), Y(:), Z(:), 1, 1, 2, 2, ax);
PF.PerformFit();

assert(~isempty(PF.FitResult))
M1 = [PF.FitResult.A PF.FitResult.sigmax PF.FitResult.sigmay ...
    PF.FitResult.x0 PF.FitResult.y0];
M2 = [A sigmax sigmay x0 y0];

assert(all(abs(M1 - M2) < 1e-6));

%% Test 3: highly scattered data
A = 37;
x0 = 0.75;
y0 = 0.4;
sigmax = 0.4;
sigmay = 0.6;

X = rand(10000, 1)*2;
Y = rand(10000, 1)*2;

Z = A.*exp(-((X-x0).^2./(2*sigmax^2)+(Y-y0).^2./(2*sigmay^2)));

PF = PeakFind(X(:), Y(:), Z(:), 1, 1, 2, 2, ax);
PF.PerformFit();

assert(~isempty(PF.FitResult))
M1 = [PF.FitResult.A PF.FitResult.sigmax PF.FitResult.sigmay ...
    PF.FitResult.x0 PF.FitResult.y0];
M2 = [A sigmax sigmay x0 y0];

assert(all(abs(M1 - M2) < 1e-6));

%% Test 4: Data Dimensions
x = linspace(0, 2, 100);
y = linspace(0, 2, 100);
A = 37;
x0 = 0.75;
y0 = 0.4;
sigmax = 0.4;
sigmay = 0.6;

[X, Y] = meshgrid(x, y);

Z = A.*exp(-((X-x0).^2./(2*sigmax^2)+(Y-y0).^2./(2*sigmay^2)));

PF = PeakFind(X, Y, Z, 1, 1, 2, 2, ax);

try
    PF = PeakFind(X(:), Y, Z, 1, 1, 2, 2, ax);
catch ex
    assert(strcmp(ex.message, 'X, Y and Z data have to be of the same size.'))
end

try
    PF = PeakFind(X, Y(:), Z, 1, 1, 2, 2, ax);
catch ex2
    assert(strcmp(ex2.message, 'X, Y and Z data have to be of the same size.'))
end

try
    PF = PeakFind(X, Y, Z(:), 1, 1, 2, 2, ax);
catch ex3
    assert(strcmp(ex3.message, 'X, Y and Z data have to be of the same size.'))
end

try
    PF = PeakFind(X(:)', Y(:), Z(:), 1, 1, 2, 2, ax);
catch ex4
    assert(strcmp(ex4.message, 'X, Y and Z data have to be of the same size.'))
end

try
    PF = PeakFind(X(:), Y(:)', Z(:), 1, 1, 2, 2, ax);
catch ex5
    assert(strcmp(ex5.message, 'X, Y and Z data have to be of the same size.'))
end

try
    PF = PeakFind(X(:), Y(:), Z(:)', 1, 1, 2, 2, ax);
catch ex6
    assert(strcmp(ex6.message, 'X, Y and Z data have to be of the same size.'))
end

close(f)