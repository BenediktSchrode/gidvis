% Testing of the function ReadDataFromFile.m
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2020 Benedikt Schrode                                     %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Create a cell containing the file names, the class of the data and the
% size of the data. We're dealing with formats which have one measurement
% per file here (others below).
FullInput = {'T1.edf' 'T2.edf' 'int32' [619 487]; ...
    'T1.tif' 'T2.tif' 'int32' [1679 1475]; ...
    'T1.img' 'T2.img' 'uint32' [385 775]; ...
    'T1.mccd' 'T2.mccd' 'uint16' [2048 2048]; ...
    'T1.cbf' 'T2.cbf' 'int32' [1679 1475]; ...
    'T1.000' 'T2.001' 'int32' [1024 1024]; ...
    'T1.tiff' 'T2.tiff' 'int32' [1679 1475]; ...
    'T1.mat', 'T2.mat' 'double' [1679 1475]};

% get path to the test data directory
TestDir = fullfile(fileparts(mfilename('fullpath')), 'TestData');

% create two LFMElement containing the reference to data in Eiger format
% (possibility of multiple measurements per data file)
Eiger1 = LFMElement;
Eiger1.Path = fullfile(TestDir, 'h5File_data_000001.h5');
Eiger1.h5Location = '/entry/data/data';
Eiger1.h5ChunkNumber = 1;
Eiger1.h5Master = fullfile(TestDir, 'h5File_master.h5');
Eiger1.h5ImageNumber = 1;

Eiger2 = LFMElement;
Eiger2.Path = fullfile(TestDir, 'TestSeries_data_000001.h5');
Eiger2.h5Location = '/entry/data/data';
Eiger2.h5ChunkNumber = 1;
Eiger2.h5Master = fullfile(TestDir, 'TestSeries_master.h5');
Eiger2.h5ImageNumber = 1;

% concatenate the Eiger input with the other input
FullInput = [FullInput; ...
    {Eiger1 Eiger2 'int32' [1679 1475]}];

%% no changes necessary below this line

for iFF = 1:size(FullInput, 1)
    % create an experimental setup to test the data cropping
    ExpectedDataSize = FullInput{iFF, 4};
    ExpSetup = ExperimentalSetUp;
    ExpSetup.detlenx = round(ExpectedDataSize(2)*0.9);
    ExpSetup.detlenz = round(ExpectedDataSize(1)*0.8);

    % create different input forms
    if ischar(FullInput{iFF, 1}) % one measurement per file
        % get file extension
        [~, ~, Ext] = fileparts(FullInput{iFF, 1});
        AssertSupportedFiles(Ext);
        
        % create forms of input
        P1 = fullfile(TestDir, FullInput{iFF, 1});
        P2 = fullfile(TestDir, FullInput{iFF, 2});
        LFM1 = LFMElement;
        LFM1.Path = P1;
        LFM2 = LFMElement;
        LFM2.Path = P2;

        % combine input forms together
        Input = cell(8, 1);
        Input{1} = P1;
        Input{2} = P2;
        Input{3} = LFM1;
        Input{4} = LFM2;
        Input{5} = {P1, P2};
        Input{6} = {P1; P2};
        Input{7} = [LFM1, LFM2];
        Input{8} = [LFM1; LFM2];
    else % possibility of multiple measurements per file
        % get file extension
        Ext = FullInput{iFF, 1}.Ext;
        % check if it is contained
        AssertSupportedFiles(Ext, true);
        % create some inputs
        Input = cell(4, 1);
        Input{1} = FullInput{iFF, 1};
        Input{2} = FullInput{iFF, 2};
        Input{3} = [FullInput{iFF, 1}, FullInput{iFF, 2}];
        Input{4} = [FullInput{iFF, 1}; FullInput{iFF, 2}];
    end
    
    % read each input form using different calls to the ReadDataFromFile
    % function
    for iI = 1:numel(Input)
        % print status message
        fprintf('Reading file format #%g, input format #%g.\n', iFF, iI);
        %% one input
        M = ReadDataFromFile(Input{iI});
        % check the returned outputs
        AssertDataType(M, 'double', Ext)
        % check the data size
        AssertDataSize(M, [FullInput{iFF, 4}])
        % check the non-identity
        AssertNonIdentity(M)
        
        %% two inputs
        M = ReadDataFromFile(Input{iI}, ExpSetup);
        % check the returned outputs
        AssertDataType(M, 'double', Ext)
        % check the data size
        AssertDataSize(M, [ExpSetup.detlenz ExpSetup.detlenx])
        % check the non-identity
        AssertNonIdentity(M)
        
        M = ReadDataFromFile(Input{iI}, []);
        % check the returned outputs
        AssertDataType(M, 'double', Ext)
        % check the data size
        AssertDataSize(M, [FullInput{iFF, 4}])
        % check the non-identity
        AssertNonIdentity(M)
        
        %% three inputs
        M = ReadDataFromFile(Input{iI}, ExpSetup, 1);
        % check the returned outputs
        AssertDataType(M, FullInput{iFF, 3}, Ext)
        % check the data size
        AssertDataSize(M, [ExpSetup.detlenz ExpSetup.detlenx])
        % check the non-identity
        AssertNonIdentity(M)
        
        M = ReadDataFromFile(Input{iI}, ExpSetup, 0);
        % check the returned outputs
        AssertDataType(M, 'double', Ext)
        % check the data size
        AssertDataSize(M, [ExpSetup.detlenz ExpSetup.detlenx])
        % check the non-identity
        AssertNonIdentity(M)
        
        M = ReadDataFromFile(Input{iI}, ExpSetup, []);
        % check the returned outputs
        AssertDataType(M, 'double', Ext)
        % check the data size
        AssertDataSize(M, [ExpSetup.detlenz ExpSetup.detlenx])
        % check the non-identity
        AssertNonIdentity(M)
        
        %% four inputs
        f = figure('Visible', 'off');
        T = uicontrol(f, 'Style', 'text', 'Units', 'normalized', ...
            'Position', [0.1 0.45 0.8 0.1], 'String', 'Blub', ...
            'Background', 'w', 'FontSize', 20);
        
        M = ReadDataFromFile(Input{iI}, ExpSetup, 1, T);
        % check the returned outputs
        AssertDataType(M, FullInput{iFF, 3}, Ext)
        % check the data size
        AssertDataSize(M, [ExpSetup.detlenz ExpSetup.detlenx])
        % check the non-identity
        AssertNonIdentity(M)
        
        M = ReadDataFromFile(Input{iI}, ExpSetup, 0, T);
        % check the returned outputs
        AssertDataType(M, 'double', Ext)
        % check the data size
        AssertDataSize(M, [ExpSetup.detlenz ExpSetup.detlenx])
        % check the non-identity
        AssertNonIdentity(M)
        
        M = ReadDataFromFile(Input{iI}, ExpSetup, [], []);
        % check the returned outputs
        AssertDataType(M, 'double', Ext)
        % check the data size
        AssertDataSize(M, [ExpSetup.detlenz ExpSetup.detlenx])
        % check the non-identity
        AssertNonIdentity(M)
        
        close(f)
        
        
        %% one input
        [M, H] = ReadDataFromFile(Input{iI});
        % check the returned outputs
        AssertDataType(M, 'double', Ext)
        % check the data size
        AssertDataSize(M, [FullInput{iFF, 4}])
        % check the non-identity
        AssertNonIdentity(M, H)
        
        % check header
        assert(iscell(H))
        assert(numel(H) == size(M, 3))
        for iH = 1:numel(H)
            AssertNumHeader(H{iH})
        end
        
        %% two inputs
        [M, H] = ReadDataFromFile(Input{iI}, ExpSetup);
        % check the returned outputs
        AssertDataType(M, 'double', Ext)
        % check the data size
        AssertDataSize(M, [ExpSetup.detlenz ExpSetup.detlenx])
        % check the non-identity
        AssertNonIdentity(M, H)
        
        % check header
        assert(iscell(H))
        assert(numel(H) == size(M, 3))
        for iH = 1:numel(H)
            AssertNumHeader(H{iH})
        end
        
        [M, H] = ReadDataFromFile(Input{iI}, []);
        % check the returned outputs
        AssertDataType(M, 'double', Ext)
        % check the data size
        AssertDataSize(M, [FullInput{iFF, 4}])
        % check the non-identity
        AssertNonIdentity(M, H)
        
        % check header
        assert(iscell(H))
        assert(numel(H) == size(M, 3))
        for iH = 1:numel(H)
            AssertNumHeader(H{iH})
        end
        
        %% three inputs
        [M, H] = ReadDataFromFile(Input{iI}, ExpSetup, 1);
        % check the returned outputs
        AssertDataType(M, FullInput{iFF, 3}, Ext)
        % check the data size
        AssertDataSize(M, [ExpSetup.detlenz ExpSetup.detlenx])
        % check the non-identity
        AssertNonIdentity(M, H)
        
        % check header
        assert(iscell(H))
        assert(numel(H) == size(M, 3))
        for iH = 1:numel(H)
            AssertNumHeader(H{iH})
        end
        
        [M, H] = ReadDataFromFile(Input{iI}, ExpSetup, 0);
        % check the returned outputs
        AssertDataType(M, 'double', Ext)
        % check the data size
        AssertDataSize(M, [ExpSetup.detlenz ExpSetup.detlenx])
        % check the non-identity
        AssertNonIdentity(M, H)
        
        % check header
        assert(iscell(H))
        assert(numel(H) == size(M, 3))
        for iH = 1:numel(H)
            AssertNumHeader(H{iH})
        end
        
        [M, H] = ReadDataFromFile(Input{iI}, ExpSetup, []);
        % check the returned outputs
        AssertDataType(M, 'double', Ext)
        % check the data size
        AssertDataSize(M, [ExpSetup.detlenz ExpSetup.detlenx])
        % check the non-identity
        AssertNonIdentity(M, H)
        
        % check header
        assert(iscell(H))
        assert(numel(H) == size(M, 3))
        for iH = 1:numel(H)
            AssertNumHeader(H{iH})
        end
        
        %% four inputs
        f = figure('Visible', 'off');
        T = uicontrol(f, 'Style', 'text', 'Units', 'normalized', ...
            'Position', [0.1 0.45 0.8 0.1], 'String', 'Blub', ...
            'Background', 'w', 'FontSize', 20);
        
        [M, H] = ReadDataFromFile(Input{iI}, ExpSetup, 1, T);
        % check the returned outputs
        AssertDataType(M, FullInput{iFF, 3}, Ext)
        % check the data size
        AssertDataSize(M, [ExpSetup.detlenz ExpSetup.detlenx])
        % check the non-identity
        AssertNonIdentity(M, H)
        
        % check header
        assert(iscell(H))
        assert(numel(H) == size(M, 3))
        for iH = 1:numel(H)
            AssertNumHeader(H{iH})
        end
        
        [M, H] = ReadDataFromFile(Input{iI}, ExpSetup, 0, T);
        % check the returned outputs
        AssertDataType(M, 'double', Ext)
        % check the data size
        AssertDataSize(M, [ExpSetup.detlenz ExpSetup.detlenx])
        % check the non-identity
        AssertNonIdentity(M, H)
        
        % check header
        assert(iscell(H))
        assert(numel(H) == size(M, 3))
        for iH = 1:numel(H)
            AssertNumHeader(H{iH})
        end
        
        [M, H] = ReadDataFromFile(Input{iI}, ExpSetup, [], []);
        % check the returned outputs
        AssertDataType(M, 'double', Ext)
        % check the data size
        AssertDataSize(M, [ExpSetup.detlenz ExpSetup.detlenx])
        % check the non-identity
        AssertNonIdentity(M, H)
        
        % check header
        assert(iscell(H))
        assert(numel(H) == size(M, 3))
        for iH = 1:numel(H)
            AssertNumHeader(H{iH})
        end
        
        close(f)
        
        % testing with 'cmd' as status
        [M, H] = ReadDataFromFile(Input{iI}, ExpSetup, 1, 'cmd');
        % check the returned outputs
        AssertDataType(M, FullInput{iFF, 3}, Ext)
        % check the data size
        AssertDataSize(M, [ExpSetup.detlenz ExpSetup.detlenx])
        % check the non-identity
        AssertNonIdentity(M, H)
        
        % check header
        assert(iscell(H))
        assert(numel(H) == size(M, 3))
        for iH = 1:numel(H)
            AssertNumHeader(H{iH})
        end
        
        [M, H] = ReadDataFromFile(Input{iI}, ExpSetup, 0, 'cmd');
        % check the returned outputs
        AssertDataType(M, 'double', Ext)
        % check the data size
        AssertDataSize(M, [ExpSetup.detlenz ExpSetup.detlenx])
        % check the non-identity
        AssertNonIdentity(M, H)
        
        % check header
        assert(iscell(H))
        assert(numel(H) == size(M, 3))
        for iH = 1:numel(H)
            AssertNumHeader(H{iH})
        end
        
        [M, H] = ReadDataFromFile(Input{iI}, ExpSetup, [], []);
        % check the returned outputs
        AssertDataType(M, 'double', Ext)
        % check the data size
        AssertDataSize(M, [ExpSetup.detlenz ExpSetup.detlenx])
        % check the non-identity
        AssertNonIdentity(M, H)
        
        % check header
        assert(iscell(H))
        assert(numel(H) == size(M, 3))
        for iH = 1:numel(H)
            AssertNumHeader(H{iH})
        end
    end
end

%% helper functions
function AssertDataType(M, ExpClass, Ext)
    % checks the data type
    assert(isa(M, ExpClass), 'While testing extension ''%s''.\nData should be returned as %s.', Ext, ExpClass);
end

function AssertDataSize(M, ExpSize)
    % checks the data size
    sz = size(M);
    if numel(sz) == 2
        assert(all(size(M) == ExpSize))
    else
        assert(all(size(M) == [ExpSize 2]))
    end
end

function AssertNonIdentity(M, H)
if nargin < 2 || isempty(H); H = []; end
if numel(size(M)) == 2; return; end
L = M(:, :, 1) == M(:, :, 2);
assert(~all(L(:)), 'Data returned for file 1 is exactly the same as data returned for file 2.')

if isempty(H); return; end
H1 = H{1};
H2 = H{2};
if isempty(H1) && isempty(H2); return; end
L = isequal(H1, H2);
assert(~all(L(:)), 'Header returned for file 1 is exactly the same as header returned for file 2.');

end

function AssertNumHeader(H)
    % checks if all numerical values in header are returned as numerical
    % values, not as char etc.
    for iR = 1:size(H, 1)
        if ~ischar(H{iR, 2}); continue; end
        Val = str2num(H{iR, 2});
        assert(isempty(Val), sprintf('Any numerical values in the header should be converted to numbers:\nValue of element %s is %s and should be converted to a number.', ...
            H{iR, 1}, H{iR, 2}))
    end
end

function AssertSupportedFiles(Ext, ExcludeFull)
% checks if the extension is found in the list of supported files

% default input arguments
if nargin < 2 || isempty(ExcludeFull)
    ExcludeFull = false;
end

% get cell of supported file extensions
C = SupportedFileFormats;

% check if file extension is contained
assert(any(contains(C, Ext)), 'Could not find the file extension ''%s'' in the output of the function SupportedFileFormats().', Ext)

if ~ExcludeFull
    % get cell of supported file extensions in type 'full'
    C = SupportedFileFormats('full');

    % file extension should be twice in the first column
    L = contains(C{1, 1}, ['*' Ext]);
    assert(L, 'Could not find the file extension ''*%s'' in the first element of SupportedFileFormats(''full'').', Ext);
    L = contains(C(2:end, 1), ['*' Ext]);
    assert(any(L), 'Could not find the file extension ''%s'' in the lines 2:end of SupportedFileFormats(''full'').', Ext);
end
end