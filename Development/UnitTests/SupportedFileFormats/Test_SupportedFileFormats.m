% Testing of the function SupportedFileFormats.m
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2020 Benedikt Schrode                                     %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

C = SupportedFileFormats('full');

[~, ind] = sort(C(2:end, 2));
Delta = diff(ind);

assert(all(Delta == ones(size(Delta))), 'Elements in rows 2:end of column 2 in SupportedFileFormats(''full'') have to be sorted alphabetically.')

Str = lower(C{1, 2});
ind = strfind(Str, '(');
Str = Str(ind+1:end-1);
StrC = strsplit(Str, ', ');
[~, ind] = sort(StrC);
Delta = diff(ind);

assert(all(Delta == ones(size(Delta))), 'Elements in first row, second column of SupportedFileFormats(''full'') have to be sorted alphabetically.')
