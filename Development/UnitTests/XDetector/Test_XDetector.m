% testing of the function XDetector.m
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% Constructor Test
D = XDetector('A', 10, 20, 1., .05, [], [], 'rectangular', '', [], []);
M = Pixels(D);

f = figure;
imagesc(M)

D = XDetector('A', 10, 20, 3, 4, 2, 2, 'round', 'Si', 0.32, 2.33);
M = Pixels(D);
imagesc(M)

Dets = {'Eiger 500K', 'Eiger 1M', 'Eiger 4M', 'Eiger 9M', 'Eiger 16M', ...
    'Pilatus 100K', 'Pilatus 300K', 'Pilatus 300K-W', 'Pilatus 1M', ...
    'Pilatus 2M', 'Rayonix SX165 2x2', 'Rayonix SX165 4x4', ...
    'Rayonix SX165 8x8', 'Vantec 2000', 'Pilatus 300KW', 'Rayonix SX165'};


for iD = 1:numel(Dets)
    D = XDetector(Dets{iD});
    D = XDetector(lower(Dets{iD}));
    D = XDetector(upper(Dets{iD}));
end
