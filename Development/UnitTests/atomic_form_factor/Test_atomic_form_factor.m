% testing of the atomic_form_factor function
%
% This file is part of GIDVis.
% 
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Dimension test
q = linspace(0, 1, 13);

aff = atomic_form_factor(q, 'H');
assert(all(size(aff) == [13 1]))

aff = atomic_form_factor(q', 'H');
assert(all(size(aff) == [13 1]))

aff = atomic_form_factor(q, {'H'});
assert(all(size(aff) == [13 1]))

aff = atomic_form_factor(q', {'H'});
assert(all(size(aff) == [13 1]))

aff = atomic_form_factor(q, {'H', 'He', 'Li', 'Be'});
assert(all(size(aff) == [13 4]))

aff = atomic_form_factor(q, {'H', 'He', 'Li', 'Be'}');
assert(all(size(aff) == [13 4]))

aff = atomic_form_factor(q', {'H', 'He', 'Li', 'Be'});
assert(all(size(aff) == [13 4]))

aff = atomic_form_factor(q', {'H', 'He', 'Li', 'Be'}');
assert(all(size(aff) == [13 4]))


%% Value test
% load the expected result
S = load('Expectedaff.mat');
ExpectedResult = S.ExpectedResult;

for ind = 1:size(ExpectedResult, 1)
    % create the element
    Element = ChEl(ExpectedResult{ind, 1});
    % create a q vector
    q = linspace(0, 3, 300);
    % get the atomic form factor
    aff = Element.aff(q);
    % compare
    assert(all(abs(aff-[ExpectedResult{ind, 2:end}]') < 1e-8), 'Failed atomic form factor of element %s.', ExpectedResult{ind, 1})
end