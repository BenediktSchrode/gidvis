% testing of the function regrid.m
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2018 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Test 1a: one dimensional
M = ones(10, 1);
[R, x, y] = regrid(1:10, 1:10, M, [1 1]);
assert(R == 1)
assert(all(x == [1 10]))
assert(all(y == [1 10]))

[R, x, y] = regrid(1:10, 11:20, M, [1 1]);
assert(R == 1)
assert(all(x == [1 10]))
assert(all(y == [11 20]))

[R, x, y] = regrid(11:20, 1:10, M, [1 1]);
assert(R == 1)
assert(all(x == [11 20]))
assert(all(y == [1 10]))

%% Test 1b: one dimensional

M = ones(10, 1);
[R, x, y] = regrid(1:10, 1:10, M, size(M));
assert(all(R == M))
assert(all(x == [1 10]))
assert(all(y == [1 10]))

[R, x, y] = regrid(1:10, 11:20, M, size(M));
assert(all(R == M))
assert(all(x == [1 10]))
assert(all(y == [11 20]))

[R, x, y] = regrid(11:20, 1:10, M, size(M));
assert(all(R == M))
assert(all(x == [11 20]))
assert(all(y == [1 10]))

%% Test 1c: one dimensional

M = ones(10, 1);
[R, x, y] = regrid(1:10, 1:10, M, [1 2]);
assert(all(R == [1 1]))
assert(all(x == [1 10]))
assert(all(y == [1 10]))

[R, x, y] = regrid(1:10, 11:20, M, [1 2]);
assert(all(R == [1 1]))
assert(all(x == [1 10]))
assert(all(y == [11 20]))

[R, x, y] = regrid(11:20, 1:10, M, [1 2]);
assert(all(R == [1 1]))
assert(all(x == [11 20]))
assert(all(y == [1 10]))

%% Test 1d: one dimensional

M = ones(10, 1);
[R, x, y] = regrid(1:10, 1:10, M, [2 1]);
assert(all(R == [1; 1]))
assert(all(x == [1 10]))
assert(all(y == [1 10]))

[R, x, y] = regrid(1:10, 11:20, M, [2 1]);
assert(all(R == [1; 1]))
assert(all(x == [1 10]))
assert(all(y == [11 20]))

[R, x, y] = regrid(11:20, 1:10, M, [2 1]);
assert(all(R == [1; 1]))
assert(all(x == [11 20]))
assert(all(y == [1 10]))

%% Test 1e: one dimensional

M = 1:10;
[R, x, y] = regrid(1:10, 1:10, M, [1 1]);
assert(R == mean(1:10))
assert(all(x == [1 10]))
assert(all(y == [1 10]))

[R, x, y] = regrid(1:10, 11:20, M, [1 1]);
assert(R == mean(1:10))
assert(all(x == [1 10]))
assert(all(y == [11 20]))

[R, x, y] = regrid(11:20, 1:10, M, [1 1]);
assert(R == mean(1:10))
assert(all(x == [11 20]))
assert(all(y == [1 10]))

%% Test 1f: one dimensional

M = 1:10;
[R, x, y] = regrid(1:10, 1:10, M, size(M));
assert(all(R == M))
assert(all(x == [1 10]))
assert(all(y == [1 10]))

[R, x, y] = regrid(1:10, 11:20, M, size(M));
assert(all(R == M))
assert(all(x == [1 10]))
assert(all(y == [11 20]))

[R, x, y] = regrid(11:20, 1:10, M, size(M));
assert(all(R == M))
assert(all(x == [11 20]))
assert(all(y == [1 10]))

%% Test 1g: one dimensional

M = 1:10;
[R, x, y] = regrid(1:10, 1:10, M, [2 1]);
assert(all(R == [mean(1:5); mean(6:10)]))
assert(all(x == [1 10]))
assert(all(y == [1 10]))

[R, x, y] = regrid(1:10, 11:20, M, [2 1]);
assert(all(R == [mean(1:5); mean(6:10)]))
assert(all(x == [1 10]))
assert(all(y == [11 20]))

[R, x, y] = regrid(11:20, 1:10, M, [2 1]);
assert(all(R == [mean(1:5); mean(6:10)]))
assert(all(x == [11 20]))
assert(all(y == [1 10]))

%% Test 1h: one dimensional

M = 1:12;
[R, x, y] = regrid(1:12, 1:12, M, [1, 4]);
assert(all(R == [mean(1:2) mean(3:6) mean(7:10) mean(11:12)]));
assert(all(x == [1 12]))
assert(all(y == [1 12]))

[R, x, y] = regrid(1:12, 11:22, M, [1, 4]);
assert(all(R == [mean(1:2) mean(3:6) mean(7:10) mean(11:12)]));
assert(all(x == [1 12]))
assert(all(y == [11 22]))

[R, x, y] = regrid(11:22, 1:12, M, [1, 4]);
assert(all(R == [mean(1:2) mean(3:6) mean(7:10) mean(11:12)]));
assert(all(x == [11 22]))
assert(all(y == [1 12]))

%% Test 1i: one dimensional, check correct removal of NaN values

M = 1:10;
M(3) = NaN;
[R, x, y] = regrid(1:10, 1:10, M, [2 1]);
LR = isnan(R);
assert(all(LR == [0; 0]))
assert(all(R == [mean([1 2 4 5]) mean(6:10)]'))
assert(all(x == [1 10]))
assert(all(y == [1 10]))

[R, x, y] = regrid(1:10, 11:20, M, [2 1]);
LR = isnan(R);
assert(all(LR == [0; 0]))
assert(all(R == [mean([1 2 4 5]) mean(6:10)]'))
assert(all(x == [1 10]))
assert(all(y == [11 20]))

[R, x, y] = regrid(11:20, 1:10, M, [2 1]);
LR = isnan(R);
assert(all(LR == [0; 0]))
assert(all(R == [mean([1 2 4 5]) mean(6:10)]'))
assert(all(x == [11 20]))
assert(all(y == [1 10]))

%% Test 2: two dimensional
M = ones(10, 10);
[X, Y] = meshgrid(1:10, 1:10);
[R, x, y] = regrid(X(:), Y(:), M(:), [1 1]);
assert(R == 1)

M = ones(10, 10);
[X, Y] = meshgrid(1:10, 1:10);
R = regrid(X(:), Y(:), M(:), size(M));
assert(all(R(:) == M(:)))

M = ones(10, 10);
[X, Y] = meshgrid(1:10, 1:10);
R = regrid(X(:), Y(:), M(:), [1 2]);
assert(all(R == [1 1]))

M = ones(10, 10);
[X, Y] = meshgrid(1:10, 1:10);
R = regrid(X(:), Y(:), M(:), [2 1]);
assert(all(R == [1; 1]))

M = 1:10;
[X, Y] = meshgrid(1:10, 1:10);
M = repmat(M, 10, 1);
R = regrid(X(:), Y(:), M(:), [2 1]);
assert(all(R == mean(1:10)))

M = 1:10;
[X, Y] = meshgrid(1:10, 1:10);
M = repmat(M, 10, 1);
R = regrid(X(:), Y(:), M(:), size(M));
assert(all(R(:) == M(:)))

M = 1:10;
[X, Y] = meshgrid(1:10, 1:10);
M = repmat(M, 10, 1);
R = regrid(X(:), Y(:), M(:), [1 2]);
assert(all(R == [mean(1:5) mean(6:10)]))

M = 1:10;
[X, Y] = meshgrid(1:10, 1:10);
M = repmat(M, 10, 1);
R = regrid(X(:), Y(:), M(:), [2 1]);
assert(all(R == [mean(1:10); mean(1:10)]))

M = reshape(1:24, 2, 12);
[X, Y] = meshgrid(1:12, 1:2);
R = regrid(X(:), Y(:), M(:), size(M));
assert(all(R(:) == M(:)));

% scattered data
XY = rand(100, 2)*0.1-0.05;
XY = [XY; 1 0; 1 1; 0 1];
M = ones(size(XY, 1), 1);
R = regrid(XY(:, 1), XY(:, 2), M(:), [2 2]);
assert(all(R(:) == [1 1 1 1]'));

M = reshape(1:24, 2, 12);
[X, Y] = meshgrid(1:12, 1:2);
R = regrid(X(:), Y(:), M(:), [2 4]);
assert(all(R(:) == [mean(1:2:3) mean(2:2:4) mean(5:2:11) mean(6:2:12) ...
    mean(13:2:19) mean(14:2:20) mean(21:2:23) mean(22:2:24)]'));

M = 1:10;
[X, Y] = meshgrid(1:10, 1:10);
M = repmat(M, 10, 1);
R = regrid(X(:), Y(:), M(:), [5 5]);
RExp = repmat([mean([1 2]) mean([3 4]) mean([5 6]) mean([7 8]) mean([9 10])], 5, 1);
assert(all(R(:) == RExp(:)));

% removal of NaNs
M = 1:10;
[X, Y] = meshgrid(1:10, 1:10);
M = repmat(M, 10, 1);
M(1:2:end) = NaN;
R = regrid(X(:), Y(:), M(:), [5 5]);
M2 = (1:2:9)+0.5;
M2 = repmat(M2, 5, 1);
assert(all(R(:) == M2(:)));

