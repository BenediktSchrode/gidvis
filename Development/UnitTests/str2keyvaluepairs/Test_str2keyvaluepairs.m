% Test of str2keyvaluepairs(Name)
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2020 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% separated by underscore
Str = 'Sample13_sdd150_detz0.tif';
R = str2keyvaluepairs(Str);
S = struct('Sample', 13, 'sdd', 150, 'detz', 0);
assert(isequal(R, S))

% leading underscore
Str = '_Sample13_sdd150_detz0.tif';
R = str2keyvaluepairs(Str);
S = struct('Sample', 13, 'sdd', 150, 'detz', 0);
assert(isequal(R, S))

% leading underscores
Str = '____Sample13_sdd150_detz0.tif';
R = str2keyvaluepairs(Str);
S = struct('Sample', 13, 'sdd', 150, 'detz', 0);
assert(isequal(R, S))

% separated by space
Str = 'Sample13 sdd150 detz0.tif';
R = str2keyvaluepairs(Str);
S = struct('Sample', 13, 'sdd', 150, 'detz', 0);
assert(isequal(R, S))

% leading space
Str = ' Sample13 sdd150 detz0.tif';
R = str2keyvaluepairs(Str);
S = struct('Sample', 13, 'sdd', 150, 'detz', 0);
assert(isequal(R, S))

% leading spaces
Str = '    Sample13 sdd150 detz0.tif';
R = str2keyvaluepairs(Str);
S = struct('Sample', 13, 'sdd', 150, 'detz', 0);
assert(isequal(R, S))

% separated by multiple spaces
Str = 'Sample13 sdd150  detz0.tif';
R = str2keyvaluepairs(Str);
S = struct('Sample', 13, 'sdd', 150, 'detz', 0);
assert(isequal(R, S))

% same keyword multiple times
Str = 'Sample13 sdd150  detz0 sdd250.tif';
R = str2keyvaluepairs(Str);
S = struct('Sample', 13, 'sdd1', 150, 'sdd2', 250, 'detz', 0);
assert(isequal(R, S))

% no separation between value of former keyword and current keyword
Str = 'Sample13 sdd150detz0.tif';
R = str2keyvaluepairs(Str);
% S = struct('Sample', 13, 'sdd', 150);
S = struct('Sample', 13, 'sdd', 150, 'detz', 0);
assert(isequal(R, S))

% using d as decimal mark with one decimal
Str = 'Sample13 sdd150 detz6d9.tif';
R = str2keyvaluepairs(Str);
S = struct('Sample', 13, 'sdd', 150, 'detz', 6.9);
assert(isequal(R, S))

% using capital D as decimal mark with one decimal
Str = 'Sample13 sdd150 detz6D9.tif';
R = str2keyvaluepairs(Str);
S = struct('Sample', 13, 'sdd', 150, 'detz', 6.9);
assert(isequal(R, S))

% using period as decimal mark with one decimal
Str = 'Sample13 sdd150 detz6.9.tif';
R = str2keyvaluepairs(Str);
S = struct('Sample', 13, 'sdd', 150, 'detz', 6.9);
assert(isequal(R, S))

% using d as decimal mark with multiple decimals 
Str = 'Sample13 sdd150 detz6d952.tif';
R = str2keyvaluepairs(Str);
S = struct('Sample', 13, 'sdd', 150, 'detz', 6.952);
assert(isequal(R, S))

% using capital D as decimal mark with multiple decimals 
Str = 'Sample13 sdd150 detz6D952.tif';
R = str2keyvaluepairs(Str);
S = struct('Sample', 13, 'sdd', 150, 'detz', 6.952);
assert(isequal(R, S))

% using d as decimal mark with one decimal, separated by space
Str = 'Sample13 sdd150 detz 6d9.tif';
R = str2keyvaluepairs(Str);
S = struct('Sample', 13, 'sdd', 150, 'detz', 6.9);
assert(isequal(R, S))

% using capital D as decimal mark with one decimal, separated by space
Str = 'Sample13 sdd150 detz 6D9.tif';
R = str2keyvaluepairs(Str);
S = struct('Sample', 13, 'sdd', 150, 'detz', 6.9);
assert(isequal(R, S))

% using d as decimal mark with one decimal, separated by underscore
Str = 'Sample13 sdd150 detz_6d9.tif';
R = str2keyvaluepairs(Str);
S = struct('Sample', 13, 'sdd', 150, 'detz', 6.9);
assert(isequal(R, S))

% using capital D as decimal mark with one decimal, separated by underscore
Str = 'Sample13 sdd150 detz_6D9.tif';
R = str2keyvaluepairs(Str);
S = struct('Sample', 13, 'sdd', 150, 'detz', 6.9);
assert(isequal(R, S))

% using period as decimal mark with multiple decimals, separated by space
Str = 'Sample13 sdd150 detz 6.952.tif';
R = str2keyvaluepairs(Str);
S = struct('Sample', 13, 'sdd', 150, 'detz', 6.952);
assert(isequal(R, S))

% using a string without number at the beginning of the file name
Str = 'Sample sdd150 detz6d9.tif';
R = str2keyvaluepairs(Str);
S = struct('sdd', 150, 'detz', 6.9);
assert(isequal(R, S))

% using a string without number in the file name
Str = 'sdd150 Sample detz6d9.tif';
R = str2keyvaluepairs(Str);
S = struct('sdd', 150, 'detz', 6.9);
assert(isequal(R, S))

% using a name at the end of the file name
Str = 'sdd150 detz6d9 Sample.tif';
R = str2keyvaluepairs(Str);
S = struct('sdd', 150, 'detz', 6.9);
assert(isequal(R, S))

% using underscores
Str = 'Sample_Dep13 sdd150 detz6d9.tif';
R = str2keyvaluepairs(Str);
S = struct('Dep', 13, 'sdd', 150, 'detz', 6.9);
assert(isequal(R, S))

% using d as a key
Str = 'Sample 15 d27.tif';
R = str2keyvaluepairs(Str);
S = struct('Sample', 15, 'd', 27);
assert(isequal(R, S))

% using d as a key plus a decimal number
Str = 'Sample 15 d27d45.tif';
R = str2keyvaluepairs(Str);
S = struct('Sample', 15, 'd', 27.45);
assert(isequal(R, S))

% using d as a key plus period for decimal number
Str = 'Sample 15 d27.45.tif';
R = str2keyvaluepairs(Str);
S = struct('Sample', 15, 'd', 27.45);
assert(isequal(R, S))

% using minus as delimiter
Str = 'Sample_Name-15 sdd27d45.tif';
R = str2keyvaluepairs(Str);
S = struct('Name', 15, 'sdd', 27.45);
assert(isequal(R, S))