function varargout = GIDVis(varargin)
% This file, together with GIDVis.fig, is the main file of GIDVis. Run
% GIDVis by running this file.
% 
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information.
%
% Usage:
%     varargout = GIDVis(varargin)
%
% Input:
%     varargin ... default input arguments (can be empty)
%
% Output:
%     varargout ... default GUIDE output
%
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Edit the above text to modify the response to help GIDVis

% Last Modified by GUIDE v2.5 26-Mar-2020 14:59:43

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @GIDVis_OpeningFcn, ...
                   'gui_OutputFcn',  @GIDVis_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before GIDVis is made visible.
function GIDVis_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to GIDVis (see VARARGIN)

% Choose default command line output for GIDVis
handles.output = hObject;

% check the MATLAB version in use when not compiled
if ~isdeployed
    v = version('-release');
    if ~strcmp(v, '2017b')
        warning('GIDVis:UntestesMATLABVersion', ...
            'GIDVis is running in the untested MATLAB version %s.\n', v);
    end
end

% find figure toolbar
FigTB = findall(hObject, 'tag', 'FigureToolBar');
if ~verLessThan('MATLAB', '9.5')
    addToolbarExplorationButtons(hObject);
end
% remove some of the toolbar buttons (including separators if necessary)
set(findall(FigTB, 'Tag', 'Standard.EditPlot'), 'Separator', 'off');
set(findall(FigTB, 'Tag', 'Standard.NewFigure'), 'Visible', 'off');
set(findall(FigTB, 'Tag', 'Standard.FileOpen'), 'Visible', 'off');
set(findall(FigTB, 'Tag', 'Exploration.Rotate'), 'Visible', 'off');
set(findall(FigTB, 'Tag', 'Exploration.Brushing'), 'Visible', 'off');
set(findall(FigTB, 'Tag', 'DataManager.Linking'), 'Visible', 'off', 'Separator', 'off');
set(findall(FigTB, 'Tag', 'Standard.PrintFigure'), 'Visible', 'off');
set(findall(FigTB, 'Tag', 'Standard.SaveFigure'), 'Visible', 'off');
set(findall(FigTB, 'Tag', 'Plottools.PlottoolsOn'), 'Visible', 'off');
set(findall(FigTB, 'Tag', 'Plottools.PlottoolsOff'), 'Visible', 'off', 'Separator', 'off');
set(findall(FigTB, 'Tag', 'Annotation.InsertColorbar'), 'Visible', 'off', 'Separator', 'off');
set(findall(FigTB, 'Tag', 'Standard.OpenInspector'), 'Visible', 'off'); % new toolbar button in R2018b

% remove more separators
set(findall(FigTB, 'Tag', 'Exploration.ZoomIn'), 'Separator', 'off');

if ~isdeployed
    % get directory path of GIDVis.m
    handles.DirectoryPath = fileparts(mfilename('fullpath'));
    
    % add modules folders and subfolders to Matlab's current path
    addpath(genpath(fullfile(handles.DirectoryPath)));
else
    handles.DirectoryPath = ctfroot;
end

% get all the paths in the javaclasspath
C = javaclasspath('-all');
% get the path to the ImportCBF.jar file
CBFPath = fullfile(handles.DirectoryPath, 'functions', 'ExternalFunctions', 'jcbf', 'ImportCBF.jar');
% add the path to the ImportCBF.jar file to the java path if it is not
% there yet
if ~ismember(CBFPath, C)
    javaaddpath(CBFPath);
end

% the settings for this module are stored in directory 
% handles.DirectoryPath. Store its path in the handles structure to reuse 
% it.
handles.SettingsPath = fullfile(handles.DirectoryPath, 'Settings.mat');

% open the load file module and add Beamtime Folder as argument
BeamtimePath = fullfile(handles.DirectoryPath, 'Beamtimes');
handles.BeamtimePath = BeamtimePath;
handles.LoadFileModule = LoadFileModule(handles.MainAxes,BeamtimePath);

% open the toolbox module
ColormapsPath = fullfile(handles.DirectoryPath, 'Colormaps');
handles.ToolboxModule = ToolboxModule(handles.MainAxes, ColormapsPath);

% set the callback for the Save Colormap menu item
set(handles.Menu_SaveColormap, 'Callback', ...
    @(src, evt) SaveColormap(src, ColormapsPath));

% set the callback for the Manipulate Colormap menu item
set(handles.Menu_ManipulateColormap, 'Callback', ...
    @(src, evt) colormapeditor);

% initialize the annotation module
InitializeAnnotationModule(FigTB, handles.Figure_GIDVis)

% initialize the detector module
InitializeDetectorModule()

% activate the AddRingsModule
AddRingModule(hObject, FigTB, handles.MainAxes, 'on');

% activate the CrosshairModule
CrosshairModule(FigTB, hObject, handles.MainAxes, 'off');

% activate the GridModule
GridModule(FigTB, handles.MainAxes, 'on');

% activate the Distance and Angle Module
DistanceAndAngleModule(FigTB, handles.MainAxes, 'on');

% activate the LineProfileModule
LineProfileModule(FigTB, handles.MainAxes, 'off');

% activate the IntegratedLineScanModule
IntegratedLineScan(FigTB, handles.MainAxes, 'off');

% activate the PowderPlotModule
PowderPlot(hObject, FigTB, handles.MainAxes, handles.LoadFileModule, handles.ToolboxModule, 'off');

% activate the MapVisibilityModule
MapVisibilityModule(FigTB, handles.MainAxes, 'on');

% overwrite the data cursor to display correct values, even if the data
% plotted is sqrt(data) or log(data)
dcm_obj = datacursormode(hObject);
set(dcm_obj, 'UpdateFcn', {@DataCursorUpdateFunction, handles.ToolboxModule, dcm_obj});

% Update handles structure
guidata(hObject, handles);

imshow(imread('GIDVis_gray.png'), 'Parent', handles.MainAxes);

% UIWAIT makes GIDVis wait for user response (see UIRESUME)
% uiwait(handles.Figure_GIDVis);

% --- Outputs from this function are returned to the command line.
function varargout = GIDVis_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% read out settings if available
if exist(handles.SettingsPath, 'file') == 2
    load(handles.SettingsPath);
    % store the settings in the handles structure
    handles.Settings = SettingsMainWindow;
    % restore the window position
    if isfield(handles.Settings, 'WindowPosition')
        RestorePositionOrDefault(hObject, handles.Settings.WindowPosition, ...
            'west');
    end
    % restore axes position
    if isfield(handles.Settings, 'MainAxesPosition')
        set(handles.MainAxes, 'Position', handles.Settings.MainAxesPosition);
    end
    % restore axes font properties
    if isfield(handles.Settings, 'MainAxesFontName')
        set(handles.MainAxes, 'FontName', handles.Settings.MainAxesFontName);
    end
    if isfield(handles.Settings, 'MainAxesFontWeight')
        set(handles.MainAxes, 'FontWeight', handles.Settings.MainAxesFontWeight);
    end
    if isfield(handles.Settings, 'MainAxesFontAngle')
        set(handles.MainAxes, 'FontAngle', handles.Settings.MainAxesFontAngle);
    end
    if isfield(handles.Settings, 'MainAxesFontUnits')
        set(handles.MainAxes, 'FontUnits', handles.Settings.MainAxesFontUnits);
    end
    if isfield(handles.Settings, 'MainAxesFontSize')
        set(handles.MainAxes, 'FontSize', handles.Settings.MainAxesFontSize);
    end
else
    AlignWindows(handles);
    button = questdlg(sprintf(['This is the first time you''re starting GIDVis. You can find help and tutorials in the manual (Help Menu - Manual).\n\n', ...
        'Do you want to open the manual now?']), ...
        'Open Manual?', 'Yes', 'No', 'Yes');
    if strcmpi(button, 'yes')
        Menu_Manual_Callback(handles.Menu_Manual, [], handles);
    end
end
% the windows were set to Visible 'off' in guide, make them visible. This
% is needed to avoid showing the window in the not restored position before
% the output_fcn had the chance to restore the window position
set(handles.LoadFileModule, 'Visible', 'on');
set(handles.ToolboxModule, 'Visible', 'on');
set(hObject, 'Visible', 'on');

% check for write access in DirectoryPath
[~, values] = fileattrib(handles.DirectoryPath);
if ~values.UserWrite
    msgbox(sprintf(['GIDVis needs write access in the directory it is run from to work properly. ', ...
        'The current directory (%s) does not allow user write access.\n', ...
        'You can continue using GIDVis from this directory, but useability is severly limited.\n\n', ...
        'Place GIDVis in an other directory for proper use.'], P), 'No Write Access', 'warn', 'modal');
end

% Get default command line output from handles structure
varargout{1} = handles.output;

% --------------------------------------------------------------------
function FileMenu_Callback(hObject, eventdata, handles)
% hObject    handle to FileMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Menu_SelectFolder_Callback(hObject, eventdata, handles)
% hObject    handle to Menu_SelectFolder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.LoadFileModule, 'Visible', 'on');
figure(handles.LoadFileModule); % bring figure to front


% --------------------------------------------------------------------
function Menu_Toolbox_Callback(hObject, eventdata, handles)
% hObject    handle to Menu_Toolbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.ToolboxModule, 'Visible', 'on')
figure(handles.ToolboxModule); % bring figure to front


% --- Executes when user attempts to close Figure_GIDVis.
function Figure_GIDVis_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to Figure_GIDVis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% store the settings
SettingsMainWindow.WindowPosition = get(hObject, 'Position');
SettingsMainWindow.MainAxesPosition = get(handles.MainAxes, 'Position');
SettingsMainWindow.MainAxesFontName = get(handles.MainAxes, 'FontName');
SettingsMainWindow.MainAxesFontWeight = get(handles.MainAxes, 'FontWeight');
SettingsMainWindow.MainAxesFontFontAngle = get(handles.MainAxes, 'FontAngle');
SettingsMainWindow.MainAxesFontUnits = get(handles.MainAxes, 'FontUnits');
SettingsMainWindow.MainAxesFontSize = get(handles.MainAxes, 'FontSize');
% in case of no write access, avoid throwing error by try
try; save(handles.SettingsPath, 'SettingsMainWindow'); end

% Hint: delete(hObject) closes the figure
delete(hObject);


% --------------------------------------------------------------------
function BeamlineSetupModule_Callback(hObject, eventdata, handles)
% hObject    handle to BeamlineSetupModule (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

BeamlineSetupModule(handles.MainAxes,  handles.BeamtimePath, ...
    handles.LoadFileModule);


% --------------------------------------------------------------------
function WindowsMenu_Callback(hObject, eventdata, handles)
% hObject    handle to WindowsMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Menu_AlignWindows_Callback(hObject, eventdata, handles)
% hObject    handle to Menu_AlignWindows (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function PeakPositionsMenu_Callback(hObject, eventdata, handles)
% hObject    handle to PeakPositionsMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Menu_PeakIndexation_Callback(hObject, eventdata, handles)
% hObject    handle to Menu_PeakIndexation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.Figure_GIDVis, 'Pointer', 'watch');
drawnow
CrystalModule(handles.MainAxes, fullfile(handles.DirectoryPath, 'Crystals'), ...
    hObject, handles.ToolboxModule);


% --------------------------------------------------------------------
function Menu_MultipleScans_Callback(hObject, eventdata, handles)
% hObject    handle to Menu_MultipleScans (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
MultipleScansModule(handles.MainAxes, handles.LoadFileModule, handles.ToolboxModule);


% --------------------------------------------------------------------
function ToolsMenu_Callback(hObject, eventdata, handles)
% hObject    handle to ToolsMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Menu_MainAxesSettings_Callback(hObject, eventdata, handles)
% hObject    handle to Menu_MainAxesSettings (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
MainAxesSettingsModule(handles.MainAxes, handles.ToolboxModule)


% --------------------------------------------------------------------
function Menu_AlignWindowsLayout1_Callback(hObject, eventdata, handles)
% hObject    handle to Menu_AlignWindowsLayout1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
AlignWindows(handles, 1);


% --------------------------------------------------------------------
function Menu_AlignWindowsLayout2_Callback(hObject, eventdata, handles)
% hObject    handle to Menu_AlignWindowsLayout2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
AlignWindows(handles, 2);


function HelpMenu_Callback(hObject, eventdata, handles)
% hObject    handle to HelpMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Menu_About_Callback(hObject, eventdata, handles)
% hObject    handle to Menu_About (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
AboutWindowModule([], fullfile(handles.DirectoryPath, 'functions', 'ExternalFunctions'));


% --------------------------------------------------------------------
function Menu_Annotations_Callback(hObject, eventdata, handles)
% hObject    handle to Menu_Annotations (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
AnnotationsPath = fullfile(fileparts(mfilename('fullpath')), 'Annotations');
RunAnnotationModule(handles.MainAxes, AnnotationsPath, hObject);


% --------------------------------------------------------------------
function Menu_SaveProject_Callback(hObject, eventdata, handles)
% hObject    handle to Menu_SaveProject (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ProjectModule(handles.LoadFileModule, handles.ToolboxModule, handles.DirectoryPath, handles.MainAxes);


% --------------------------------------------------------------------
function Menu_LoadProject_Callback(hObject, eventdata, handles)
% hObject    handle to Menu_LoadProject (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
LoadProject(handles.DirectoryPath, handles.ToolboxModule, handles.MainAxes, handles.LoadFileModule);


% --------------------------------------------------------------------
function Menu_CorrectionFactors_Callback(hObject, eventdata, handles)
% hObject    handle to Menu_CorrectionFactors (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
CorrectionFactorsModule(handles.MainAxes, handles.LoadFileModule, handles.ToolboxModule)


% --------------------------------------------------------------------
function Menu_BeamFootprint_Callback(hObject, eventdata, handles)
% hObject    handle to Menu_BeamFootprint (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
FootprintModule(handles.MainAxes);


% --------------------------------------------------------------------
function Menu_PeakFinder_Callback(hObject, eventdata, handles)
% hObject    handle to Menu_PeakFinder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
PeakFindingModule(handles.MainAxes);


% --------------------------------------------------------------------
function Menu_CalibrationWizard_Callback(hObject, eventdata, handles)
% hObject    handle to Menu_CalibrationWizard (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
CalibrationWizard(handles.MainAxes, handles.LoadFileModule);


% --------------------------------------------------------------------
function Menu_GIDVisRoot_Callback(hObject, eventdata, handles)
% hObject    handle to Menu_GIDVisRoot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ispc % check if user is running Windows
    winopen(handles.DirectoryPath);
else
    msgbox(sprintf('Could not open directory.\n\nThe directory path is:\n%s', ...
        handles.DirectoryPath));
end


% --------------------------------------------------------------------
function Menu_ExportMap_Callback(hObject, eventdata, handles)
% hObject    handle to Menu_ExportMap (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Menu_ExportRegriddedData_Callback(hObject, eventdata, handles)
% hObject    handle to Menu_ExportRegriddedData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ExportData(handles.MainAxes, true);

% --------------------------------------------------------------------
function Menu_ExportNonRegriddedData_Callback(hObject, eventdata, handles)
% hObject    handle to Menu_ExportNonRegriddedData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ExportData(handles.MainAxes, false);


% --------------------------------------------------------------------
function Menu_GenerateCode_Callback(hObject, eventdata, handles)
% hObject    handle to Menu_GenerateCode (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
GenerateCode(handles.MainAxes);


% --------------------------------------------------------------------
function Menu_GIDVisWebpage_Callback(hObject, eventdata, handles)
% hObject    handle to Menu_GIDVisWebpage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
web('http://www.if.tugraz.at/amd/GIDVis/', '-browser')


% --------------------------------------------------------------------
function Menu_Manual_Callback(hObject, eventdata, handles)
% hObject    handle to Menu_Manual (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% create path to the help file
P = fullfile(handles.DirectoryPath, 'Manual', 'Manual.pdf');
if ~isdeployed && ispc % check for isdeployed, because open() cannot be
	open(P)            % used in deployed applications
else
	stat = web(['file:///', P], '-browser');
    if stat ~= 0
        msgbox(sprintf('Could not open help file. You can find it at\n%s', ...
            P));
    end
end


% --- Executes on key press with focus on Figure_GIDVis or any of its controls.
function Figure_GIDVis_WindowKeyPressFcn(hObject, eventdata, handles)
% hObject    handle to Figure_GIDVis (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.FIGURE)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
if isempty(eventdata.Modifier) && strcmpi(eventdata.Key, 'f1')
    Menu_Manual_Callback(handles.Menu_Manual, [], handles);
end


function Menu_SaveImage_Callback(hObject, eventdata, handles)
% hObject    handle to Menu_SaveImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
SavePlotModule(handles.MainAxes);


% --------------------------------------------------------------------
function Menu_Detectors_Callback(hObject, eventdata, handles)
% hObject    handle to Menu_Detectors (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
DetectorModule(handles.MainAxes);


% --------------------------------------------------------------------
function Menu_SetupSimulation_Callback(hObject, eventdata, handles)
% hObject    handle to Menu_SetupSimulation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
SetupSimulationModule(handles.MainAxes, handles.ToolboxModule, handles.LoadFileModule);


% --------------------------------------------------------------------
function Menu_InitializeEiger_Callback(hObject, eventdata, handles)
% hObject    handle to Menu_InitializeEiger (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(hObject, 'Enable', 'off')
drawnow();
PluginPath = fullfile(handles.DirectoryPath, 'functions', ...
    'ExternalFunctions', 'hdf5plugin');
InitializeEiger(PluginPath);
set(hObject, 'Enable', 'on')
