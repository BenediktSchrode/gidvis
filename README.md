# [GIDVis](https://www.if.tugraz.at/amd/GIDVis/) - a modular MATLAB program to analyze grazing incidence X-ray diffraction (GIXD) images.

## Features
- Conversion from pixel space to reciprocal space on the fly
- Conversion to polar space on the fly
- Variety of supported input data formats (e.g. `*.cbf`, `*.edf`, `*.mccd`, `*.tif`, Bruker Format (`*.000` - `*.999`), ...)
- Crystal phase analysis
- Detector calibration
- Pole figure calculation
- Analysis of image series
- Line scans
- Integrated line scans
- Powder plot
- Peak fitting
- Merging/stitching of multiple files
- Projects
- ...

## Availability
GIDVis is released under the terms of the GNU General Public License, either version 3 of the License, or (at your opinion) any later version. You can find the full license text [here](https://bitbucket.org/BenediktSchrode/gidvis/raw/master/GIDVisLicense.txt).

Visit the [Downloads Page](https://bitbucket.org/BenediktSchrode/gidvis/wiki/Downloads) to download the source code or compiled executable versions of GIDVis.

## Citation
If you use GIDVis for your work, please cite:
> Schrode, B.; Pachmajer, S.; Dohr, M.; Röthel, C.; Domke, J.; Fritz, T.; Resel, R.; Werzer, O. GIDVis : A Comprehensive Software Tool for Geometry-Independent Grazing-Incidence X-Ray Diffraction Data Analysis and Pole-Figure Calculations. J Appl Crystallogr 2019, 52 (3). https://doi.org/10.1107/S1600576719004485

## Help
Please see the [Manual (PDF, 11 MB)](https://bitbucket.org/BenediktSchrode/gidvis/raw/master/Manual/Manual.pdf) for general help on using GIDVis and extended tutorials.

## Bugs and Missing Features
If you find a bug or have a feature request, please file an [issue](https://bitbucket.org/BenediktSchrode/gidvis/issues/new).

## Contribute
To contribute, check out the Wiki page on [Contribution](https://bitbucket.org/BenediktSchrode/gidvis/wiki/Contribute).

![alt text](GIDVis_white.png "GIDVis")