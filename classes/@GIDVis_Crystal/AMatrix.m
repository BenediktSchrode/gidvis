function A = AMatrix(obj, TargetOrientation)
    % Calculates the matrix A used to simulate the reflexes of a crystal
    % which does not have a (001) orientation.
    %
    % Usage:
    %     A = AMatrix(obj, TargetOrientation);
    %
    % Input:
    %     obj ... GIDVis_Crystal object
    %     TargetOrientation ... the orientation of the crystal
    %
    % Output:
    %     A ... matrix A
    %
    % Sources:
    %     [1] Moser, A., 2012. Crystal Structure Solution Based on Grazing 
    %         Incidence X-ray Diffraction: Software Development and 
    %         Application to Organic Films (phd thesis). Graz University of 
    %         Technology.
    %     [2] Shmueli, U. (Ed.), 2008. International tables for 
    %         crystallography. Vol. B: Reciprocal space, 3. ed. ed. 
    %         Springer, Dordrecht.
    %
    % This file is part of GIDVis.
    %
    % see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % create a matrix of a_vec, b_vec and c_vec
    M = [obj.a_vec obj.b_vec obj.c_vec];
    % The algorithm below requires that a_vec and b_vec lie in the
    % x-y-plane. So we have to perform a rotation.
    % get the normal vector of a_vec and b_vec
    n = cross(M(:, 1), M(:, 2));
    % normalize it
    n = n./norm(n);
    % get the rotation matrix
    R = obj.RotMat(n, [0 0 1]);
    
    % Rotate the matrix containing the reciprocal space vectors
    A = R*[obj.a_star_vec, obj.b_star_vec, obj.c_star_vec];
    
    if dot(TargetOrientation./norm(TargetOrientation), [0 0 1]) == 1
        return;
    end
    if dot(TargetOrientation./norm(TargetOrientation), [0 0 1]) == -1
        A(:, end) = -1*A(:, end);
        return;
    end
    o1 = A*[0; 0; 1];
    o2 = A*TargetOrientation(:);
    n = cross(o1, o2)./abs(norm(cross(o1, o2)));
    cos_phi = dot(o1, o2)/(abs(norm(o1))*abs(norm(o2)));
    t = 1-cos_phi;
    s = sqrt(1-cos_phi^2);
    R = ones(3, 3).*t;
    R = R.*[n(1)*n(1), n(2)*n(1), n(3)*n(1); ...
        n(1)*n(2), n(2)*n(2), n(3)*n(2); ...
        n(1)*n(3), n(2)*n(3), n(3)*n(3)];
    R = R+eye(3).*cos_phi;
    R = R + s.*[0, n(3), -n(2); ...
        -n(3), 0, n(1); ...
        n(2), -n(1), 0];
    A = R*A;
end