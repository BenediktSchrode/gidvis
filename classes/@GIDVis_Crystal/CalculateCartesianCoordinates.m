function Cart = CalculateCartesianCoordinates(obj, Fracs)
    % Calculates cartesian coordinates for the fractional coordinates Fracs = [Fracx_1, Fracy_1, Fracz_1; Fracx_2, Fracy_2, Fracz_2; ...; Fracx_n, Fracy_n, Fracz_n]
    %
    % Usage:
    %     Cart = CalculateCartesianCoordinates(obj, Fracs);
    %
    % Input:
    %     obj ... GIDVis_Crystal object
    %     Fracs ... n x 3 matrix of fractional coordinates to transform
    %
    % Output:
    %     Cart ... n x 3 matrix of cartesian coordinates
    %
    %
    % source: https://en.wikipedia.org/wiki/Fractional_coordinates
    %
    % This file is part of GIDVis.
    %
    % see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    ConversionMatrix = [obj.a_vec obj.b_vec obj.c_vec];
    Cart = (ConversionMatrix*Fracs.').';
end