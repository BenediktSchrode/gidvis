function Fracs = CalculateFractionalCoordinates(obj, Cart)
    % Calculates fractional coordinates for the cartesian coordinates
    %
    % Usage:
    %     Fracs = CalculateFractionalCoordinates(obj, Cart);
    %
    % Input:
    %     obj ... GIDVis_Crystal object
    %     Cart ... n x 3 matrix of cartesian coordinates to transform
    %
    % Output:
    %     Fracs ... n x 3 matrix of fractional coordinates
    %
    %
    % source: https://en.wikipedia.org/wiki/Fractional_coordinates
    %
    % This file is part of GIDVis.
    %
    % see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    ConversionMatrix = [obj.a_vec obj.b_vec obj.c_vec];
    Fracs = (ConversionMatrix\Cart.').';
end