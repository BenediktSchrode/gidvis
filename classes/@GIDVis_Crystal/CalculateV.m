function V = CalculateV(a, b, c, alpha, beta, gamma)
    % Calculates the volume of a parallelepiped.
    %
    % Usage:
    %     V = CalculateV(a, b, c, alpha, beta, gamma);
    %
    % Input:
    %     a ... length a of the parallelepiped
    %     b ... length b of the parallelepiped
    %     c ... length c of the parallelepiped
    %     alpha ... angle alpha of the parallelepiped
    %     beta ... angle beta of the parallelepiped
    %     gamma ... angle gamma of the parallelepiped
    %
    % Output:
    %     V ... volume of the parallelepiped
    %
    % This file is part of GIDVis.
    %
    % see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    V = a*b*c*...
         sqrt(1-cosd(alpha)^2-cosd(beta)^2-cosd(gamma)^2 + ...
         2*cosd(alpha)*cosd(beta)*cosd(gamma));
end