function [Passed, errMsg] = CheckUnitCellConsistency(obj)
	% Checks unit cell contents if they fit parameters from the *.res file
    %
    % This function checks whether the atoms in the unit cell fulfil two
    % criteria:
    %     (1) The number of atoms in the unit cell has to match the sum of
    %         the numbers after the UNIT keyword in the *.res file.
    %     (2) The number of atoms of element E has to appear as many times
    %         as the corresponding number at the UNIT keyword from the res
    %         file
    %
    % Usage:
    %     Passed = CheckUnitCellConsistency(obj);
    %
    % Input:
    %     obj ... GIDVis_Crystal object
    %
    % Output:
    %     Passed ... boolean if test is passed or not. If the UnitCell 
    %                or the UNIT property of the GIDVis_Crystal object is 
    %                empty Passed will be empty.
    %
    % This file is part of GIDVis.
    %
    % see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    errMsg = '';

    if isempty(obj.UnitCell)
        Passed = [];
        errMsg = 'No atoms in the unit cell.';
        return;
    end

    if isempty(obj.UNIT)
        Passed = [];
        errMsg = 'UNIT property empty.';
        return;
    end

    Passed = true;
    % The number of atoms in the unit cell should match sum(UNIT)
    if numel(obj.UnitCell) ~= sum(obj.UNIT)
        Passed = false;
        % do not return here to get a better error message
    end
    
    % Element SFAC{i} has to be UNIT{i} times in the unit cell
    for iEl = 1:numel(obj.SFAC)
        L = cellfun(@(x) strcmp(x, obj.SFAC{iEl}), {obj.UnitCell.Element});
        if sum(L) ~= obj.UNIT(iEl)
            Passed = false;
            errMsg = sprintf('Element %s should appear %g times in the unit cell; but it appears %g times.', obj.SFAC{iEl}, obj.UNIT(iEl), sum(L));
            break
        end
    end
end