function hkl = CreateHKLCombinations(h, k, l)
    % Calculates all possible combinations of h k l values. 
    %
    % Usage:
    %     hkl = CreateHKLCombinations(h, k, l);
    %
    % Input:
    %     h ... vector of h values
    %     k ... vector of k values
    %     l ... vector of l values
    %
    % Output:
    %     hkl ... n x 3 matrix containing all possible combinations
    %
    % This file is part of GIDVis.
    %
    % see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    if nargin == 1
        k = h;
        l = h;
    elseif nargin == 3
    else
        error('GIDVis_Crystal:CreateHKLCombinations:InputArguments', 'Provide either one or three input arguments.');
    end

    [H, K, L] = ndgrid(h, k, l);
    hkl = [H(:) K(:) L(:)];
    % find h == k == l == 0
    ind = find(all(bsxfun(@eq, hkl, [0 0 0]), 2));
    hkl(ind, :) = [];
end