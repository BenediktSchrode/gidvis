function [UnitCell, Spacegr, ErrorMessages] = CreateUnitCell(Cr)
    % Creates the unit cell for crystal Cr from its asymmetric unit.
    %
    % This function creates the unit cell of the crystal Cr, starting from
    % its asymmetric unit Cr.AsymmetricUnit. For this process, the space
    % group has to be determined. This is done by comparing the symmetry
    % operations given by Cr.Symm with the symmetry operations of the space
    % groups. Then, the symmetry operations are applied to the atoms of the
    % asymmetric unit (including additional translations if necessary for
    % the determined space group). Then, equivalent atoms are removed from
    % the created atoms. Finally, the unit cell consistency is checked.
    %
    % If multiple space groups are found with the symmetry operations
    % Cr.Symm, above mentioned process is performed beginning with the
    % first match. If the unit cell consistency check is not passed, the
    % next space group is tried as long as the consistency check is passed.
    %
    %
    % Usage:
    %     [UnitCell, Spacegr, ErrorMessages] = CreateUnitCell(Cr);
    %
    % Input:
    %     Cr ... GIDVis_Crystal object
    %
    % Output:
    %    UnitCell ... vector of atoms in the unit cell
    %    Spgrnr ... spacegroup
    %    CrSetting ... additional information about the space group
    %
    % This file is part of GIDVis.
    %    
    % see also: GIDVisLicense, SpacegroupSuggestions, RemoveDuplicateAtoms,
    % CheckUnitCellConsistency

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % if asymmetric unit is empty, no unit cell can be constructed
    if isempty(Cr.AsymmetricUnit)
        UnitCell = [];
        Spacegr = [];
        ErrorMessages = 'Asymmetric unit empty. Cannot create unit cell.';
        return
    end
    % in case unit cell is already filled, do not repeat process
    if ~isempty(Cr.UnitCell)
        return;
    end

    % get the symmetry operations
    TempSymm = Cr.Symm;
    % if identity operation is not given, add it
    if ~any(strcmp(TempSymm, 'x,y,z'))
        TempSymm = [{'x,y,z'}; TempSymm];
    end

    % transform string representation of symmetry operations to
    % matrix form
    SymMats = cell(numel(TempSymm), 1);
    for iS = 1:numel(TempSymm)
        SymMats{iS} = CreateSymmetryMatrix(TempSymm{iS});
    end

    % get the spacegroup
    Spgrs = SpacegroupSuggestions(Cr);
    if isempty(Spgrs)
        msgbox('Could not determine spacegroup.');
        Spacegr = [];
        return;
    end

    % in case of centro symmetry, double the number of the symmetry
    % matrizes by applying the inverse to each of them
    if Cr.Latt > 0
        SymMats = [SymMats; cellfun(@(x) -1.*x, SymMats, 'UniformOutput', false)];
    end

    ASUContent = Cr.AsymmetricUnit;
    
    ErrorMessages = cell(size(Spgrs));
    % loop over all suggested spacegroups
    for iSpgrs = 1:numel(Spgrs)
        Spgr = Spgrs(iSpgrs);

        % Spgr.Coordinates holds the additional translations, in
        % case none is given, it should be [0 0 0]
        if isempty(Spgr.Coordinates); Spgr.Coordinates = [0 0 0]; end

        UnitCell = GIDVis_Crystal_Atom;
        % loop over all atoms of the asymmetric unit
        for iASU = 1:numel(Cr.AsymmetricUnit)
            % get the atom
            ASU = ASUContent(iASU);
            % get the position
            Pos = [ASU.x; ASU.y; ASU.z];
            % get the accuracy of the position
            if ~isempty(ASU.xAccuracy)
                ASUAcc = [ASU.xAccuracy ASU.yAccuracy ASU.zAccuracy];
            else
                ASUAcc = 10.^(-[ASU.Lx ASU.Ly ASU.Lz]);
            end

            % number of atoms which will result after applying the
            % symmetry operations
            NumGenPos = numel(SymMats);

            % "pre-allocate"
            NewAtoms = GIDVis_Crystal_Atom.empty(1, 0);

            cnt = 1;
            for iS = 1:numel(SymMats)
                % current symmetry operation
                M = SymMats{iS};
                for iC = 1:size(Spgr.Coordinates, 1)
                    % create new atom
                    NewAtoms(cnt) = GIDVis_Crystal_Atom;
                    % store basic information
                    NewAtoms(cnt).SymmOp = [M(:, 1:3) M(:, 4) + Spgr.Coordinates(iC, :)'];
                    NewAtoms(cnt).Name = ASU.Name;
                    NewAtoms(cnt).Element = ASU.Element;
                    NewAtoms(cnt).U = ASU.U;

                    % get the accuracies involved with the current
                    % symmetry operation
                    NewAtoms(cnt).xAccuracy = max(ASUAcc(M(1, 1:3)~=0)); %10.^(-min(delta(M(1, 1:3)~=0)));
                    NewAtoms(cnt).yAccuracy = max(ASUAcc(M(2, 1:3)~=0)); %10.^(-min(delta(M(2, 1:3)~=0)));
                    NewAtoms(cnt).zAccuracy = max(ASUAcc(M(3, 1:3)~=0)); %10.^(-min(delta(M(3, 1:3)~=0)));

                    % calculate the atoms position
                    NewPos(1:3) = M(1:3, 1:3)*Pos + M(1:3, 4);
                    % Perform additional translation
                    NewPos = NewPos + Spgr.Coordinates(iC, :);

                    % make all atoms fall in the unit cell (i.e.
                    % fractional coordinates between 0 and 1).
                    while any(NewPos(:) >= 1)
                        NewPos(NewPos(:) >= 1) = NewPos(NewPos(:) >= 1) - 1;
                    end
                    while any(NewPos(:) < 0)
                        NewPos(NewPos(:) < 0) = NewPos(NewPos(:) < 0) + 1;
                    end

                    % store position in atom
                    NewAtoms(cnt).x = NewPos(1);
                    NewAtoms(cnt).y = NewPos(2);
                    NewAtoms(cnt).z = NewPos(3);

                    cnt = cnt+1;
                end
            end

            NewAtoms = Cr.RemoveDuplicateAtoms(NewAtoms);

            NewSOF = ASU.SOF*NumGenPos*size(Spgr.Coordinates, 1)/numel(NewAtoms);
            for iNA = 1:numel(NewAtoms)
                NewAtoms(iNA).SOF = NewSOF;
            end

            % add the final positions to the unit cell
            UnitCell(end+1:end+numel(NewAtoms), 1) = NewAtoms;
        end
        % remove first entry from unit cell (which is empty)
        UnitCell = UnitCell(2:end);
        % assign unit cell to crystal
        Cr.UnitCell = UnitCell;

        % check unit cell: in case there is more than one
        % spacegroup suggestion, if the currently selected one
        % already passes the check, the others are not tried
        % anymore.
        [Passed, errmsg] = CheckUnitCellConsistency(Cr);
        if isempty(Passed) || Passed
            break;
        end
        if isempty(Spgr.SettingDescription)
            ErrorMessages{iSpgrs} = sprintf('Failed testing spacegroup #%g. Reason: %s', Spgr.Number, errmsg);
        else
            ErrorMessages{iSpgrs} = sprintf('Failed testing spacegroup #%g (Setting: %s). Reason: %s', Spgr.Number, Spgr.SettingDescription, errmsg);
        end
    end
    % assign results to variables
%     CrSetting = Spgr.SettingDescription;
%     Spgrnr = [num2str(Spgr.Number), ' (', Spgr.FullName, ')'];
    Spacegr = Spgr;
    
    if ~Passed
        UnitCell = [];
        Spgr = [];
    end
end