function f = DataView(Cr)
    % Creates a figure showing the properties of the crystal.
    %
    %
    % Usage:
    %     f = DataView(Cr)
    %
    % Input:
    %     Cr ... GIDVis_Crystal object
    %
    % Output:
    %    f ... handle to the figure displaying the information
    %
    % This file is part of GIDVis.
    %    
    % see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fInt = figure('Name', ['Crystal Data for ', Cr.Name], 'MenuBar', 'none', ...
    'NumberTitle', 'off');
tabgp = uitabgroup(fInt, 'Units', 'normalized', 'Position', [0 0 1 1]);

% create a tab for the basic data
t = uitab(tabgp, 'Title', 'Basic Data');
txt = uicontrol('Style', 'edit', 'Parent', t, 'Units', 'normalized', ...
    'Position', [0 0 1 1], 'FontSize', 10, 'HorizontalAlignment', 'center', ...
    'Max', 100);

% Prepare a string for the number of atoms in the asymmetric unit and the
% number of atoms in the unit cell. It should look like: N0 (N1 E1, N2 E2,
% N3 E3) where N0 is the number of atoms in the asymmetric unit/unit cell
% and Ni are the numbers of atoms of element Ei.
if numel(Cr.UnitCell) > 0
    % get the unique elements
    Els = [Cr.UnitCell.Element];
    UnElements = unique({Els.Name});
    UCElN = {Els.Name};
    if isempty(Cr.AsymmetricUnit)
        ASUElN = {};
    else
        Els = [Cr.AsymmetricUnit.Element];
        ASUElN = {Els.Name};
    end
    % initialize result cell
    ASUElements = cell(size(UnElements));
    UCElements = ASUElements;
    % loop over the unique elements
    for iEl = 1:numel(UnElements)
        % we have to make a check here in case the crystal contains atoms
        % in the unit cell but not in the asymmetric unit
        if numel(Cr.AsymmetricUnit) > 0
            % make logic vector containing 1s where the element UnElements{iEL} is
            % found in the asymmetric unit
            L = cellfun(@(x) strcmp(x, UnElements{iEl}), ASUElN);
            % prepare the result string for the asymmetric unit
            ASUElements{iEl} = [num2str(sum(L)) ' ', UnElements{iEl}, ', '];
        end
        % make logic vector containing 1s where the element UnElements{iEL} is
        % found in the unit cell
        L = cellfun(@(x) strcmp(x, UnElements{iEl}), UCElN);
        % prepare the result string for the unit cell
        UCElements{iEl} = [num2str(sum(L)) ' ', UnElements{iEl}, ', '];
    end
end

% count number of atoms in the asymmetric unit
ASUElementsStr = num2str(numel(Cr.AsymmetricUnit));
% if there are some atoms in the asymmetric unit, make a detailed list how
% many of each element
if numel(Cr.AsymmetricUnit) ~= 0
    WStr = [ASUElements{:}];
    ASUElementsStr = [ASUElementsStr, ' (', WStr(1:end-2), ')'];
end
% count number of atoms in the unit cell
UCElementsStr = num2str(numel(Cr.UnitCell));
% if there are some atoms in the unit cell, make a detailed list how many
% of each element
if numel(Cr.UnitCell) ~= 0
    WStr = [UCElements{:}];
    UCElementsStr = [UCElementsStr, ' (', WStr(1:end-2), ')'];
end

ASign = char(197);
DegSign = char(176);

% prepare a huge string for the basic crystal data
Str = sprintf(['Name: %s\n', ...
    '-----------------------------------------\n', ...
    'a = %g %s\n', ...
    'b = %g %s\n', ...
    'c = %g %s\n', ...
    'alpha = %g%s\n', ...
    'beta = %g%s\n', ...
    'gamma = %g%s\n', ...
    '-----------------------------------------\n', ...
    'a* = %g 1/%s\n', ...
    'b* = %g 1/%s\n', ...
    'c* = %g 1/%s\n', ...
    'alpha* = %g%s\n', ...
    'beta* = %g%s\n', ...
    'gamma* = %g%s\n', ...
    '-----------------------------------------\n', ...
    'V = %g %s^3\n', ...
    'Lattice Type: %s\n'], ...
    Cr.Name, Cr.a, ASign, Cr.b, ASign, Cr.c, ASign, ...
    Cr.alpha, DegSign, Cr.beta, DegSign, Cr.gamma, DegSign, ...
    Cr.a_star, ASign, Cr.b_star, ASign, Cr.c_star, ASign, ...
    Cr.alpha_star, DegSign, Cr.beta_star, DegSign, Cr.gamma_star, DegSign, ...
    Cr.V, ASign, Cr.LatticeType);

if ~isempty(Cr.CentroSymmetric) && Cr.CentroSymmetric
    Str = sprintf('%sCentro Symmetric: yes\n', Str);
elseif ~isempty(Cr.CentroSymmetric) && ~Cr.CentroSymmetric
    Str = sprintf('%sCentro Symmetric: no\n', Str);
end

Str = [Str, sprintf([ ...
    '-----------------------------------------\n', ...
    'a_vector = [%g %g %g]\n', ...
    'b_vector = [%g %g %g]\n', ...
    'c_vector = [%g %g %g]\n', ...
    '-----------------------------------------\n', ...
    'a*_vector = [%g %g %g]\n', ...
    'b*_vector = [%g %g %g]\n', ...
    'c*_vector = [%g %g %g]\n', ...
    '-----------------------------------------\n', ...
    'Number of atoms in the asymmetric unit: %s\n', ...
    'Number of atoms in the unit cell: %s\n', ...
    ], ...
    Cr.a_vec, Cr.b_vec, Cr.c_vec, ...
    Cr.a_star_vec, Cr.b_star_vec, Cr.c_star_vec, ...
    ASUElementsStr, UCElementsStr)];

% use this strange strsplit construct to avoid problems with the text not
% being correctly centered in the uicontrol
set(txt, 'String', strsplit(Str, '\n'));

if ~isempty(Cr.AsymmetricUnit)
    % if there are atoms in the asymmetric unit, create a cell containing
    % the information of all the atoms in the asymmetric unit
    Els = [Cr.AsymmetricUnit.Element];
    M = { ...
        Cr.AsymmetricUnit.Name; ...
        Els.Abbreviation};
    Cart = CalculateCartesianCoordinates(Cr, [[Cr.AsymmetricUnit.x]', [Cr.AsymmetricUnit.y]', [Cr.AsymmetricUnit.z]']);
    M = [M; ...
        num2cell([Cr.AsymmetricUnit.x]); ...
        num2cell([Cr.AsymmetricUnit.y]); ...
        num2cell([Cr.AsymmetricUnit.z]); ...
        num2cell([Cr.AsymmetricUnit.xAccuracy]); ...
        num2cell([Cr.AsymmetricUnit.yAccuracy]); ...
        num2cell([Cr.AsymmetricUnit.zAccuracy]); ...
        num2cell(Cart(:, 1)'); ...
        num2cell(Cart(:, 2)'); ...
        num2cell(Cart(:, 3)'); ...
        num2cell([Cr.AsymmetricUnit.SOF]); ...
        num2cell(Cr.AsymmetricUnit.Beq(Cr)'); ...
        num2cell(Cr.AsymmetricUnit.Beq(Cr)'./(8*pi^2))];
    
    % create a tab for the asymmetric unit information, put a table into it
    % and put the information in there
    AUTab = uitab('Parent', tabgp, 'Title', 'Asymmetric Unit');
    Tab = uitable(AUTab);
    set(Tab, 'Data', M');
    set(Tab, 'Units', 'normalized');
    set(Tab, 'Position', [0 0 1 0.9]);
    set(Tab, 'FontSize', 10);
    ColNames = {'Name', 'Element', 'x_frac', 'y_frac', 'z_frac', ...
        'Delta x_frac', 'Delta y_frac', 'Delta z_frac', ...
        'x', 'y', 'z', 'SOF', 'Beq', 'U'};
    set(Tab, 'ColumnName', ColNames);
    T = uicontrol(AUTab, 'Style', 'text', 'String', 'Use Ctrl + A then Ctrl + C in the table below to copy the data to the clipboard.', 'Units', 'normalized', 'Position', [0 0.92 0.84 0.06], 'FontSize', 10);
    set(fInt, 'Color', get(T, 'BackgroundColor'));
    uicontrol(AUTab, 'Style', 'pushbutton', 'String', 'Export to file', 'Units', 'normalized', 'Position', [0.84 0.92 0.15 0.06], 'FontSize', 10, 'Callback', {@WriteValuesToFile, Cr, Tab});
end

if ~isempty(Cr.UnitCell)
    % if there are atoms in the unit cell, create a cell containing the
    % information of all the atoms in the unit cell
    Els = [Cr.UnitCell.Element];
    M = { ...
        Cr.UnitCell.Name; ...
        Els.Abbreviation};
    Cart = CalculateCartesianCoordinates(Cr, [[Cr.UnitCell.x]', [Cr.UnitCell.y]', [Cr.UnitCell.z]']);
    M = [M; ...
        num2cell([Cr.UnitCell.x]); ...
        num2cell([Cr.UnitCell.y]); ...
        num2cell([Cr.UnitCell.z]); ...
        num2cell([Cr.UnitCell.xAccuracy]); ...
        num2cell([Cr.UnitCell.yAccuracy]); ...
        num2cell([Cr.UnitCell.zAccuracy]); ...
        num2cell(Cart(:, 1)'); ...
        num2cell(Cart(:, 2)'); ...
        num2cell(Cart(:, 3)'); ...
        num2cell([Cr.UnitCell.SOF]); ...
        num2cell(Cr.UnitCell.Beq(Cr)'); ...
        num2cell(Cr.UnitCell.Beq(Cr)'./(8*pi^2)); ...
        CreateSymmetryString({Cr.UnitCell.SymmOp})];
    
    % create a tab for the unit cell information, put a table into it and
    % put the information in there
    UCTab = uitab('Parent', tabgp, 'Title', 'Unit Cell');
    Tab = uitable(UCTab);
    set(Tab, 'Data', M');
    set(Tab, 'Units', 'normalized');
    set(Tab, 'Position', [0 0 1 0.9]);
    set(Tab, 'FontSize', 10);
    ColNames = {'Name', 'Element', 'x_frac', 'y_frac', 'z_frac', ...
        'Delta x_frac', 'Delta y_frac', 'Delta z_frac', ...
        'x', 'y', 'z', 'SOF', 'Beq', 'U', 'Symmetry Op.'};
    set(Tab, 'ColumnName', ColNames);
    T = uicontrol(UCTab, 'Style', 'text', 'String', 'Use Ctrl + A then Ctrl + C in the table below to copy the data to the clipboard.', 'Units', 'normalized', 'Position', [0 0.92 0.84 0.06], 'FontSize', 10);
    set(fInt, 'Color', get(T, 'BackgroundColor'));
    uicontrol(UCTab, 'Style', 'pushbutton', 'String', 'Export to file', 'Units', 'normalized', 'Position', [0.84 0.92 0.15 0.06], 'FontSize', 10, 'Callback', {@WriteValuesToFile, Cr, Tab}); 
end

% here we create a tab for the plot of the crystal structure. To avoid
% a delay in the start-up, the plotting is only executed when the tab
% gets selected and the structure wasn't already plotted
FigTab = uitab('Parent', tabgp, 'Title', 'Structure View', ...
    'BackgroundColor', 'w', 'Tag', 'Tab_StructureView');
% add an axes system to the tab
StructureView_ax = axes('Parent', FigTab, 'Units', 'normalized', ...
    'Position', [0.1300 0.1100 0.7750 0.8150], 'Color', 'w', ...
    'Visible', 'off');
% add some controls for defining the number of repeat units of the unit
% cell which should be plotted
uicontrol('Style', 'text', 'Parent', FigTab, 'Units', ...
    'normalized', 'Position', [0.03 0.03 0.17 0.038], ...
    'FontSize', 10, 'String', 'Repeat Units: ', ...
    'BackgroundColor', 'w', 'HorizontalAlignment', 'right');
RepUx = uicontrol('Style', 'popupmenu', 'FontSize', 10, ...
    'Units', 'normalized', 'Position', [0.2 0.03 0.1 0.05], ...
    'String', {'1', '2', '3'}, 'Parent', FigTab);
RepUy = uicontrol('Style', 'popupmenu', 'FontSize', 10, ...
    'Units', 'normalized', 'Position', [0.31 0.03 0.1 0.05], ...
    'String', {'1', '2', '3'}, 'Parent', FigTab);
RepUz = uicontrol('Style', 'popupmenu', 'FontSize', 10, ...
    'Units', 'normalized', 'Position', [0.42 0.03 0.1 0.05], ...
    'String', {'1', '2', '3'}, 'Parent', FigTab);
% set the callback of the popup menus to draw the new plot
set([RepUx, RepUy, RepUz], 'Callback', @RepeatUnitsChanged);
% add a button to show the atom color code
uicontrol('Style', 'pushbutton', 'FontSize', 10, ...
    'Units', 'normalized', 'Position', [0.82 0.01465 0.15 0.06995],  ...
    'String', 'Color Code', 'Parent', FigTab, ...
    'Callback', @(src, evt) ColorCode);
% add a text label which will be shown as long the plot is updated
PlottingLabel = uicontrol('Style', 'text', 'Parent', FigTab, ...
    'FontSize', 10, 'String', 'Plotting ...', 'Units', 'normalized', ...
    'Position', [0 0 1 0.7], 'HorizontalAlignment', 'center', ...
    'BackgroundColor', 'w');
% set the SelectionChangedFcn of the tabgroup. In the callback, the
% crystal structure is plotted if not done already.
set(tabgp, 'SelectionChangedFcn', @SelectionChanged);

if nargout > 0
    f = fInt;
end

function SelectionChanged(~, evt)
    % executed when the selected tab changes
    
    % check if the tab with the crystal structure plot is selected
    if strcmp(get(evt.NewValue, 'Tag'), 'Tab_StructureView')
        if isempty(get(StructureView_ax, 'Children'))
            % if there are no children in the axes system, the crystal
            % structure has not been yet plotted, so plot it now.
            PlotCrystalStructure();
        end
        % turn on the rotate3d mode of the figure
        rotate3d(StructureView_ax, 'on');
    end
end

function RepeatUnitsChanged(varargin)
    % executed when the number of repeat units is changed via the popup
    % menus
    
    % clear the axes system
    cla(StructureView_ax);
    % plot the crystal structure (with the new repeat unit values)
    PlotCrystalStructure();
end

function PlotCrystalStructure
    % function to plot the crystal structure
    
    % show the label telling the user that plotting process is running
    set(PlottingLabel, 'Visible', 'on');
    drawnow();
    
    % plot unit cell
    plotUnitCell(Cr, 'Parent', StructureView_ax);
    hold(StructureView_ax, 'on')
    % to plot the atoms, we have to get a list of atoms according to the
    % repeat unit values selected
    A = IncreaseUnitCell(Cr, [0 get(RepUx, 'Value')], ...
        [0 get(RepUy, 'Value')], [0 get(RepUz, 'Value')]);
    % to avoid drawing a lot of atoms in 3d style (which is slower than 2d
    % style), check the number of atoms
    if numel(A) < 500
        % plot atoms using 3d style (default in plotAtoms)
        plotAtoms(Cr, A, 'Parent', StructureView_ax);
        % there are not so many atoms, so the user can select 1 to 3 repeat
        % units
        set([RepUx, RepUy, RepUz], 'String', {'1', '2', '3'});
    else
        % plot atoms using the 2d style
        plotAtoms(Cr, A, 'Parent', StructureView_ax, 'AtomStyle', '2d');
        % There are already a lot of atoms. To avoid a too large plot,
        % limit the choices for the repeat units to 1 to the currently
        % selected value
        set(RepUx, 'String', strsplit(num2str(1:get(RepUx, 'Value'))));
        set(RepUy, 'String', strsplit(num2str(1:get(RepUy, 'Value'))));
        set(RepUz, 'String', strsplit(num2str(1:get(RepUz, 'Value'))));
    end
    % turn off the axis system
    axis(StructureView_ax, 'off')
    % switch the axis mode to 'equal'
    axis(StructureView_ax, 'equal')

    % hide the label telling the user that plotting process is running
    set(PlottingLabel, 'Visible', 'off');
end
end