function f = EditUnitCell(obj)
    % Function for interactive editing of the unit cell and its contents.
    %
    % Usage:
    %     f = EditUnitCell(obj)
    %
    % Input:
    %     obj ... GIDVis_Crystal object
    %
    % Output:
    %     f ... figure handle to the window where the editing is performed
    %
    % This file is part of GIDVis.
    %
    % see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% create a figure window but keep it invisible
fInt = figure('NumberTitle', 'off', ...
    'Name', ['Edit Unit Cell (', obj.Name, ')'], ...
    'Units', 'pixels', 'Toolbar', 'none', 'MenuBar', 'none', ...
    'Visible', 'off');

% create a table for the unit cell parameters
UIT1 = uitable('Parent', fInt, 'Units', 'pixels', ...
    'Position', [6 372 288 122]);
% create a table for the unit cell atoms
UIT2 = uitable('Parent', fInt, 'Units', 'pixels', ...
    'Position', [6 48 288 276], 'FontSize', 10); 
% get allowed elements from the ChEl enumeration
[~, s] = enumeration('ChEl');
s = sort(s);
% Set the column format of the table for the unit cell atoms. Use the
% AllowedElements from the DummyAtom to make user choose the element from a
% dropdown menu
set(UIT2, 'ColumnFormat', {s', 'numeric', ...
    'numeric', 'numeric', 'numeric', 'numeric'});
% create buttons to add and remove atoms
Btn_AddAtom = uicontrol('Style', 'pushbutton', 'Units', 'pixels', ...
    'Position', [6 330 150 26], 'String', 'Add Atom', ...
    'FontSize', 10, 'Callback', @(src, evt) Btn_AddAtom_Callback(UIT2));
Btn_CopyAtom = uicontrol('Style', 'pushbutton', 'Units', 'pixels', ...
    'Position', [162 330 150 26], 'String', 'Copy Atom', ...
    'FontSize', 10, 'Callback', @(src, evt) Btn_CopyAtom_Callback(UIT2));
Btn_RemoveAtom = uicontrol('Style', 'pushbutton', 'Units', 'pixels', ...
    'Position', [318 330 150 26], 'String', 'Remove Atom', ...
    'FontSize', 10, 'Callback', @(src, evt) Btn_RemoveAtom_Callback(UIT2));
% create the OK button
uicontrol('Style', 'pushbutton', 'Units', 'pixels', ...
    'Position', [6 6 150 26], 'String', 'OK', ...
    'FontSize', 10, 'Callback', @(src, evt) Btn_OK_Callback(obj, UIT2, UIT1));

% get the data for the first table (unit cell parameters)
D1 = {obj.a; obj.b; obj.c; obj.alpha; obj.beta; obj.gamma};
% set properties of the unit cell parameters table
set(UIT1, 'ColumnName', {}, 'Data', D1, 'FontSize', 10, ...
    'RowName', {'a (Angstroem)', 'b (Angstroem)', 'c (Angstroem)', ...
    'alpha (degree)', 'beta (degree)', 'gamma (degree)'}, ...
    'ColumnEditable', true, 'ColumnFormat', {'numeric'});

% set the column names of the unit cell atoms table
set(UIT2, 'ColumnName', {'Element', 'x', 'y', 'z', 'SOF', 'B'}, ...
    'ColumnEditable', true);
% if there are already atoms in the unit cell, set them as the data of the
% table
if ~isempty(obj.UnitCell)
    Els = [obj.UnitCell.Element];
    D2 = {Els.Abbreviation; obj.UnitCell.x; obj.UnitCell.y; obj.UnitCell.z; obj.UnitCell.SOF}';
    D2(:, end+1) = num2cell(obj.UnitCell.Beq(obj)); 
    set(UIT2, 'Data', D2);
end

% in this callback, the index of the selected row is saved. It is used when
% the user wants to remove an atom
set(UIT2, 'CellSelectionCallback', @UIT_CellSelection_Callback);

% set SizeChanged_Callback function and execute it once
set(fInt, 'SizeChangedFcn', @(src, evt) Figure_SizeChanged_Callback(src, UIT1, UIT2, Btn_AddAtom, Btn_RemoveAtom, Btn_CopyAtom));
Figure_SizeChanged_Callback(fInt, UIT1, UIT2, Btn_AddAtom, Btn_RemoveAtom, Btn_CopyAtom);

% make figure visible
set(fInt, 'Visible', 'on');

if nargout > 0
    f = fInt;
end

function UIT_CellSelection_Callback(src, evt)
    % store index of selected row (needed for deleting an atom)
    set(src, 'UserData', evt.Indices(:, 1));
end

function Figure_SizeChanged_Callback(src, UIT1, UIT2, Btn_AddAtom, Btn_RemoveAtom, Btn_CopyAtom)
    % adapt the size of the UI elements: only thing which should become
    % larger/smaller is the table of the atoms, other elements should keep
    % their size
    
    set(src, 'Units', 'pixels');
    WinPos = get(src, 'Position');

    UIT1Pos = get(UIT1, 'Position');
    set(UIT1, 'Position', [UIT1Pos(1) WinPos(4)-128 WinPos(3)-12 UIT1Pos(4)]);

    UIT2Pos = get(UIT2, 'Position');
    NewHeight = WinPos(4)-224;
    if NewHeight >= 50
        set(UIT2, 'Position', [UIT2Pos(1) 48 WinPos(3)-12 WinPos(4)-224]);
    end

    Btn_AddPos = get(Btn_AddAtom, 'Position');
    set(Btn_AddAtom, 'Position', [Btn_AddPos(1) WinPos(4)-170 Btn_AddPos(3) Btn_AddPos(4)]);

    Btn_RemovePos = get(Btn_RemoveAtom, 'Position');
    set(Btn_RemoveAtom, 'Position', [Btn_RemovePos(1) WinPos(4)-170 Btn_RemovePos(3) Btn_RemovePos(4)]);
    
    Btn_CopyPos = get(Btn_CopyAtom, 'Position');
    set(Btn_CopyAtom, 'Position', [Btn_CopyPos(1) WinPos(4)-170 Btn_CopyPos(3) Btn_CopyPos(4)]);
end

function Btn_OK_Callback(obj, TableHandle, ParamTable)
    % Callback for the OK button. Save the entered unit cell values.
    
    % get the data of the unit cell parameters
    Data = get(ParamTable, 'Data');
    % convert the data to numbers
    Params = cell2mat(Data);
    if any(isnan(Params)) % check for not numeric values
        msgbox('Not numeric values entered for the unit cell parameters. Please check.', 'modal');
        return;
    end
    % assign the unit cell parameters
    obj.a = Params(1);
    obj.b = Params(2);
    obj.c = Params(3);
    obj.alpha = Params(4);
    obj.beta = Params(5);
    obj.gamma = Params(6);

    % get the data of the atoms table
    Data = get(TableHandle, 'Data');
    % convert the data to numbers
    Coordinates = cell2mat(Data(:, 2:end));
    NaNs = isnan(Coordinates);
    if any(NaNs(:)) % check for not numeric values
        msgbox('Not numeric values entered for the coordinates and/or SOF and/or B. Please check.', 'modal');
        return;
    end
    
    % reset the UnitCell property of the crystal
    obj.UnitCell = GIDVis_Crystal_Atom.empty();
    % loop over the atoms table's data and add the atoms to the UnitCell
    for iR = 1:size(Coordinates, 1)
        obj.UnitCell(iR, 1) = GIDVis_Crystal_Atom;
        obj.UnitCell(iR, 1).Name = Data{iR, 1};
        obj.UnitCell(iR, 1).x = Coordinates(iR, 1);
        obj.UnitCell(iR, 1).y = Coordinates(iR, 2);
        obj.UnitCell(iR, 1).z = Coordinates(iR, 3);
        obj.UnitCell(iR, 1).Element = Data{iR, 1};
        obj.UnitCell(iR, 1).SOF = Coordinates(iR, 4);
        obj.UnitCell(iR, 1).U = Coordinates(iR, 5)/(8*pi^2);
        % Assign a NaN accuracy value
        obj.UnitCell(iR, 1).xAccuracy = NaN;
        obj.UnitCell(iR, 1).yAccuracy = NaN;
        obj.UnitCell(iR, 1).zAccuracy = NaN;
        % the symmetry operation which created the atoms is always the
        % identy without additional translation
        obj.UnitCell(iR, 1).SymmOp = [eye(3) zeros(3, 1)];
    end

    % assign additional crystal properties
    obj.Delta_a = [];
    obj.Delta_b = [];
    obj.Delta_c = [];
    obj.Delta_alpha = [];
    obj.Delta_beta = [];
    obj.Delta_gamma = [];
    obj.Latt = [];
    obj.AsymmetricUnit = []; % remove the atoms which might be in the 
    % asymmetric unit. Otherwise, the atoms in the asymmetric unit and the
    % unit cell might disagree with each other
    obj.Symm = [];
    obj.Setting = [];
    obj.SFAC = [];
    obj.UNIT = [];

    % close the figure window
    close(ancestor(TableHandle, 'figure'));
end

function Btn_RemoveAtom_Callback(TableHandle)
    % remove an atom
    
    % get the index of the last selected row
    UD = get(TableHandle, 'UserData');
    % get the atom data
    Data = get(TableHandle, 'Data');
    % remove the corresponding row
    Data(UD, :) = [];
    % reassign the data to the table
    set(TableHandle, 'Data', Data);
end

function Btn_AddAtom_Callback(TableHandle)
    % add an atom
    
    % get the old data
    OldRows = get(TableHandle, 'Data');
    % create a new dummy row with some default values
    NewRow = {'Ac', 0, 0, 0, 1, 8*pi^2*0.05}; % 8*pi^2*0.05 is the default also used in Mercury/PowderCell
    % concatenate the old and new data
    NewRows = [OldRows; NewRow];
    % reassign the data to the table
    set(TableHandle, 'Data', NewRows);
end

function Btn_CopyAtom_Callback(TableHandle)
    % copy the currently selected atom
    
    % get the index of the last selected row
    UD = get(TableHandle, 'UserData');
    % if the index is empty, nothing is selected
    if isempty(UD); return; end
    
    % get the old data
    OldRows = get(TableHandle, 'Data');
    
    % add the copied row below the selected row
    if UD == size(OldRows, 1)
        NewRows = [OldRows; OldRows(UD, :)];
    else
        NewRows = [OldRows(1:UD, :); OldRows(UD, :); OldRows(UD+1, :)];
    end
    
    % reassign the data to the table
    set(TableHandle, 'Data', NewRows);
end
end