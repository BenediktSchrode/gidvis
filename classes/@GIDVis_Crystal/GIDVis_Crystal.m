classdef GIDVis_Crystal < handle
% Class representing a crystal.
% 
% This class contains:
% - calculation of basic properties of the crystal unit cell in real and
%   reciprocal space (e.g. unit cell vectors, volume, etc.)
% - import from *.res file
% - calculation of expected peak positions
% - calculation of structure factors
% - conversion between fractional and cartesian coordinates and v.v.
% - calculation of hkl planes and their normal vectors
% - plotting (atoms, unit cell, hkl planes and atoms lying in a certain hkl
%   plane)
% - rotation of crystal: A rotation of the crystal rotates the real space
%   lattice vectors a_vec, b_vec and c_vec, so all real space quantities
%   are influenced by a rotation (e.g. the vertices of an hkl plane
%   obtained by hklPlane() or the position of the atoms in real space
%   obtained by CalculateCartesianCoordinates(Cr, Fracs)).
%
% This file requires the following additional files included in GIDVis to
% run:
% - ChEl.m
% - ColorCode.m
% - CreateSymmetryMatrix.m
% - CreateSymmetryString.m
% - GIDVis_Crystal_Atom.m
%
%
% This file is part of GIDVis.
%
% see also: GIDVis_Crystal_Demo, GIDVis_Crystal_Atom, ChEl, 
% CreateSymmetryMatrix, ColorCode, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    properties
        Name % Name of the crystal.
        Spacegroup % space group of the crystal.
        Delta_a % standard deviation of real space unit cell parameter a. Angstr�m units.
        Delta_b % standard deviation of real space unit cell parameter b. Angstr�m units.
        Delta_c % standard deviation of real space unit cell parameter c. Angstr�m units.
        Delta_alpha % standard deviation of real space unit cell parameter alpha. Degree units.
        Delta_beta % standard deviation of real space unit cell parameter beta. Degree units.
        Delta_gamma % standard deviation of real space unit cell parameter gamma. Degree units.
        % Array containing GIDVis_Crystal_Atoms.
        %
        % Will be filled when importing from *.res file.
        % see also: GIDVis_Crystal_Atom
        UnitCell
    end % properties
    
    properties (Dependent)
        a_star % Reciprocal space unit cell parameter a*.
        b_star % Reciprocal space unit cell parameter b*.
        c_star % Reciprocal space unit cell parameter c*.
        alpha_star % Reciprocal space unit cell parameter alpha*.
        beta_star % Reciprocal space unit cell parameter beta*.
        gamma_star % Reciprocal space unit cell parameter gamma*.
        V % Volume of the unit cell.
        a % Real space unit cell parameter a. Angstr�m units.
        b % Real space unit cell parameter b. Angstr�m units.
        c % Real space unit cell parameter c. Angstr�m units.
        alpha % Real space unit cell parameter alpha. Degree units.
        beta % Real space unit cell parameter beta. Degree units.
        gamma % Real space unit cell parameter gamma. Degree units.
        a_star_vec % Reciprocal space unit cell vector a_vec*
        b_star_vec % Reciprocal space unit cell vector b_vec*
        c_star_vec % Reciprocal space unit cell vector c_vec*
        CentroSymmetric % Is crystal centro symmetric or not
        LatticeType % Lattice type converted to a string
    end % properties (Dependent)
    
    properties (SetAccess = protected)
        a_vec % Real space unit cell vector a_vec
        b_vec % Real space unit cell vector b_vec
        c_vec % Real space unit cell vector c_vec
        Latt % Lattice type as given in the SHELXL help (http://shelx.uni-ac.gwdg.de/SHELX/shelxl_html.php#LATT)
        % Array containing GIDVis_Crystal_Atoms.
        %
        % Will be filled when importing from *.res file.
        % see also: GIDVis_Crystal_Atom
        AsymmetricUnit
        Symm % Cell containing the symmetry elements given in the *.res file.
        Setting % If more than one setting is available for a spacegroup, setting tells which one is chosen.
        SpacegroupNumber % Spacegroup number as given in the International Tables for Crystallography.
    end
    
    properties (Access = protected)
        SFAC % value of the SHELX field SFAC
        UNIT % value of the SHELX field UNIT
    end
    
    properties (Constant = true, Access = protected)
        % constant value for the drawing of the atom spheres
        sx = [0.894427 0.723607 0.276393 -0.276393 -0.723607 -0.894427 -0.723607 -0.276393 0.276393 0.723607 0 0 0.817452 0.884119 0.924213 0.924213 0.884119 0.817452 0.786507 0.898715 0.959200 0.982247 0.959200 0.898715 0.833155 0.951057 0.994186 0.994186 0.951057 0.850651 0.964275 0.984525 0.964275 0.833155 0.941231 0.941231 0.786507 0.412378 0.533632 0.649427 0.741022 0.796498 0.381433 0.555436 0.684413 0.794654 0.867604 0.489876 0.688191 0.810995 0.902591 0.587785 0.792636 0.896903 0.661515 0.860216 0.705492 0.412378 0.391857 0.353018 0.296409 0.229397 0.162029 0.250349 0.555436 0.536209 0.491123 0.422991 0.343279 0.212158 0.688191 0.662791 0.606182 0.525731 0.162460 0.792636 0.755128 0.686557 0.106079 0.860216 0.810146 0.050070 0.796498 0.741022 0.649427 0.533632 0.705492 0.867604 0.794654 0.684413 0.661515 0.902591 0.810995 0.587785 0.896903 0.489876 0.381433 0.229397 0.296409 0.353018 0.391857 0.050070 0.422991 0.491123 0.536209 0.106079 0.606182 0.662791 0.162460 0.755128 0.212158 0.250349 0.304235 0.204814 0.091595 -0.020685 -0.119265 0.200279 0.409627 0.303531 0.183191 0.065560 0.106079 0.501223 0.388004 0.262866 0 0.567101 0.449358 -0.106079 0.605352 -0.200279 -0.074838 -0.021623 0.034986 0.087622 0.131084 -0.250349 0.126582 0.187592 0.239800 0.277718 -0.212158 0.331395 0.388004 0.425325 -0.162460 0.512947 0.555436 -0.106079 0.655422 -0.050070 0.283550 0.444613 0.592818 0.708876 0.131084 0.444613 0.607062 0.741022 0.277718 0.592818 0.741022 0.425325 0.708876 0.555436 0.655422 -0.605352 -0.567101 -0.501223 -0.409627 -0.304235 -0.705492 -0.449358 -0.388004 -0.303531 -0.204814 -0.661515 -0.262866 -0.183191 -0.091595 -0.587785 -0.065560 0.020685 -0.489876 0.119265 -0.381433 -0.087622 -0.239800 -0.388004 -0.512947 -0.131084 -0.034986 -0.187592 -0.331395 -0.277718 0.021623 -0.126582 -0.425325 0.074838 -0.555436 -0.655422 -0.796498 -0.867604 -0.902591 -0.896903 -0.860216 -0.817452 -0.741022 -0.794654 -0.810995 -0.792636 -0.898715 -0.649427 -0.684413 -0.688191 -0.951057 -0.533632 -0.555436 -0.964275 -0.412378 -0.941231 -0.755128 -0.606182 -0.422991 -0.229397 -0.810146 -0.662791 -0.491123 -0.296409 -0.686557 -0.536209 -0.353018 -0.525731 -0.391857 -0.343279 -0.162029 -0.786507 -0.884119 -0.959200 -0.994186 -0.984525 -0.817452 -0.833155 -0.924213 -0.982247 -0.994186 -0.898715 -0.850651 -0.924213 -0.959200 -0.951057 -0.833155 -0.884119 -0.964275 -0.786507 -0.941231 -0.283550 -0.444613 -0.592818 -0.708876 -0.131084 -0.444613 -0.607062 -0.741022 -0.277718 -0.592818 -0.741022 -0.425325 -0.708876 -0.555436 -0.655422 -0.796498 -0.741022 -0.649427 -0.533632 -0.412378 -0.705492 -0.867604 -0.794654 -0.684413 -0.555436 -0.661515 -0.902591 -0.810995 -0.688191 -0.587785 -0.896903 -0.792636 -0.489876 -0.860216 -0.381433 -0.391857 -0.353018 -0.296409 -0.229397 -0.250349 -0.536209 -0.491123 -0.422991 -0.212158 -0.662791 -0.606182 -0.162460 -0.755128 -0.106079 -0.050070 0.119265 0.020685 -0.091595 -0.204814 -0.304235 0.200279 -0.065560 -0.183191 -0.303531 -0.409627 0.106079 -0.262866 -0.388004 -0.501223 -0 -0.449358 -0.567101 -0.106079 -0.605352 -0.200279 -0.087622 -0.034986 0.021623 0.074838 -0.239800 -0.187592 -0.126582 -0.388004 -0.331395 -0.512947 0.304235 0.409627 0.501223 0.567101 0.605352 0.204814 0.303531 0.388004 0.449358 0.091595 0.183191 0.262866 -0.020685 0.065560 -0.119265 0.512947 0.388004 0.239800 0.087622 0.331395 0.187592 0.034986 0.126582 -0.021623 -0.074838]';
        % constant value for the drawing of the atom spheres
        sy = [0 0.525731 0.850651 0.850651 0.525731 0 -0.525731 -0.850651 -0.850651 -0.525731 0 0 0.476192 0.309017 0.107677 -0.107677 -0.309017 -0.476192 0.380954 0.403548 0.215354 -0 -0.215354 -0.403548 0.201774 0.309017 0.107677 -0.107677 -0.309017 -0 0.201774 0 -0.201774 -0.201774 0.095238 -0.095238 -0.380954 0.770495 0.769672 0.738028 0.671480 0.578689 0.865734 0.652955 0.630351 0.577350 0.497255 0.854729 0.500000 0.456127 0.389579 0.809017 0.326477 0.269672 0.730026 0.154099 0.630291 -0.770495 -0.666667 -0.522674 -0.348450 -0.166667 0 -0.770495 -0.652955 -0.522674 -0.356822 -0.174225 -0 -0.652955 -0.500000 -0.348450 -0.174225 0 -0.500000 -0.326477 -0.166667 -0 -0.326477 -0.154099 0 -0.154099 -0.578689 -0.671480 -0.738028 -0.769672 -0.630291 -0.497255 -0.577350 -0.630351 -0.730026 -0.389579 -0.456127 -0.809017 -0.269672 -0.854729 -0.865734 0.166667 0.348450 0.522674 0.666667 0.154099 0.174225 0.356822 0.522674 0.326477 0.174225 0.348450 0.500000 0.166667 0.652955 0.770495 0.936339 0.978801 0.978801 0.936339 0.865734 0.924594 0.912253 0.934172 0.912253 0.854729 0.979432 0.845705 0.845705 0.809017 1.000000 0.745356 0.730026 0.979432 0.630291 0.924594 0.769672 0.630351 0.456127 0.269672 0.095238 0.770495 0.738028 0.577350 0.389579 0.201774 0.652955 0.671480 0.497255 0.309017 0.500000 0.578689 0.403548 0.326477 0.476192 0.154099 -0 -0.107677 -0.215354 -0.309017 -0.095238 0.107677 -0 -0.107677 -0.201774 0.215354 0.107677 -0.309017 0.309017 -0.403548 -0.476192 0.630291 0.745356 0.845705 0.912253 0.936339 0.630291 0.730026 0.845705 0.934172 0.978801 0.730026 0.809017 0.912253 0.978801 0.809017 0.854729 0.936339 0.854729 0.865734 0.865734 0.269672 0.389579 0.497255 0.578689 0.095238 0.456127 0.577350 0.671480 0.201774 0.630351 0.738028 0.309017 0.769672 0.403548 0.476192 0.578689 0.497255 0.389579 0.269672 0.154099 0.476192 0.671480 0.577350 0.456127 0.326477 0.403548 0.738028 0.630351 0.500000 0.309017 0.769672 0.652955 0.201774 0.770495 0.095238 0.166667 0.174225 0.174225 0.166667 0 0.348450 0.356822 0.348450 0 0.522674 0.522674 0 0.666667 0 0 -0.380954 -0.309017 -0.215354 -0.107677 0 -0.476192 -0.201774 -0.107677 0 0.107677 -0.403548 0 0.107677 0.215354 -0.309017 0.201774 0.309017 -0.201774 0.380954 -0.095238 0 -0.107677 -0.215354 -0.309017 -0.095238 0.107677 0 -0.107677 -0.201774 0.215354 0.107677 -0.309017 0.309017 -0.403548 -0.476192 -0.578689 -0.671480 -0.738028 -0.769672 -0.770495 -0.630291 -0.497255 -0.577350 -0.630351 -0.652955 -0.730026 -0.389579 -0.456127 -0.500000 -0.809017 -0.269672 -0.326477 -0.854729 -0.154099 -0.865734 -0.666667 -0.522674 -0.348450 -0.166667 -0.770495 -0.522674 -0.356822 -0.174225 -0.652955 -0.348450 -0.174225 -0.500000 -0.166667 -0.326477 -0.154099 -0.865734 -0.936339 -0.978801 -0.978801 -0.936339 -0.924594 -0.854729 -0.912253 -0.934172 -0.912253 -0.979432 -0.809017 -0.845705 -0.845705 -1.000000 -0.730026 -0.745356 -0.979432 -0.630291 -0.924594 -0.269672 -0.456127 -0.630351 -0.769672 -0.389579 -0.577350 -0.738028 -0.497255 -0.671480 -0.578689 -0.936339 -0.912253 -0.845705 -0.745356 -0.630291 -0.978801 -0.934172 -0.845705 -0.730026 -0.978801 -0.912253 -0.809017 -0.936339 -0.854729 -0.865734 -0.578689 -0.497255 -0.389579 -0.269672 -0.671480 -0.577350 -0.456127 -0.738028 -0.630351 -0.769672]';
        % constant value for the drawing of the atom spheres
        sz = [-0.447214 0.447214 -0.447214 0.447214 -0.447214 0.447214 -0.447214 0.447214 -0.447214 0.447214 -1.000000 1.000000 0.324059 0.350487 0.366382 0.366382 0.350487 0.324059 0.486088 0.171639 0.183191 0.187592 0.183191 0.171639 0.514918 0 0 0 0 0.525731 -0.171639 -0.175244 -0.171639 0.514918 -0.324059 -0.324059 0.486088 -0.486088 -0.350487 -0.183191 0 0.175244 -0.324059 -0.514918 -0.366382 -0.187592 0 -0.171639 -0.525731 -0.366382 -0.183191 0 -0.514918 -0.350487 0.171639 -0.486088 0.324059 -0.486088 -0.634038 -0.776009 -0.889227 -0.958957 -0.986786 -0.586227 -0.514918 -0.662791 -0.794654 -0.889227 -0.939234 -0.727076 -0.525731 -0.662791 -0.776009 -0.850651 -0.850651 -0.514918 -0.634038 -0.727076 -0.939234 -0.486088 -0.586227 -0.986786 0.175244 0 -0.183191 -0.350487 0.324059 0 -0.187592 -0.366382 0.171639 -0.183191 -0.366382 0 -0.350487 -0.171639 -0.324059 -0.958957 -0.889227 -0.776009 -0.634038 -0.986786 -0.889227 -0.794654 -0.662791 -0.939234 -0.776009 -0.662791 -0.850651 -0.634038 -0.727076 -0.586227 -0.175244 -0 0.183191 0.350487 0.486088 -0.324059 -0 0.187592 0.366382 0.514918 -0.171639 0.183191 0.366382 0.525731 0 0.350487 0.514918 0.171639 0.486088 0.324059 0.634038 0.776009 0.889227 0.958957 0.986786 0.586227 0.662791 0.794654 0.889227 0.939234 0.727076 0.662791 0.776009 0.850651 0.850651 0.634038 0.727076 0.939234 0.586227 0.986786 0.958957 0.889227 0.776009 0.634038 0.986786 0.889227 0.794654 0.662791 0.939234 0.776009 0.662791 0.850651 0.634038 0.727076 0.586227 -0.486088 -0.350487 -0.183191 0 0.175244 -0.324059 -0.514918 -0.366382 -0.187592 0 -0.171639 -0.525731 -0.366382 -0.183191 0 -0.514918 -0.350487 0.171639 -0.486088 0.324059 -0.958957 -0.889227 -0.776009 -0.634038 -0.986786 -0.889227 -0.794654 -0.662791 -0.939234 -0.776009 -0.662791 -0.850651 -0.634038 -0.727076 -0.586227 -0.175244 -0 0.183191 0.350487 0.486088 -0.324059 -0 0.187592 0.366382 0.514918 -0.171639 0.183191 0.366382 0.525731 0 0.350487 0.514918 0.171639 0.486088 0.324059 0.634038 0.776009 0.889227 0.958957 0.586227 0.662791 0.794654 0.889227 0.727076 0.662791 0.776009 0.850651 0.634038 0.939234 0.986786 -0.486088 -0.350487 -0.183191 0 0.175244 -0.324059 -0.514918 -0.366382 -0.187592 0 -0.171639 -0.525731 -0.366382 -0.183191 0 -0.514918 -0.350487 0.171639 -0.486088 0.324059 -0.958957 -0.889227 -0.776009 -0.634038 -0.986786 -0.889227 -0.794654 -0.662791 -0.939234 -0.776009 -0.662791 -0.850651 -0.634038 -0.727076 -0.586227 -0.175244 -0 0.183191 0.350487 0.486088 -0.324059 -0 0.187592 0.366382 0.514918 -0.171639 0.183191 0.366382 0.525731 0 0.350487 0.514918 0.171639 0.486088 0.324059 0.634038 0.776009 0.889227 0.958957 0.586227 0.662791 0.794654 0.889227 0.727076 0.662791 0.776009 0.850651 0.634038 0.939234 0.986786 -0.486088 -0.350487 -0.183191 0 0.175244 -0.324059 -0.514918 -0.366382 -0.187592 0 -0.171639 -0.525731 -0.366382 -0.183191 0 -0.514918 -0.350487 0.171639 -0.486088 0.324059 -0.958957 -0.889227 -0.776009 -0.634038 -0.889227 -0.794654 -0.662791 -0.776009 -0.662791 -0.634038 -0.175244 -0 0.183191 0.350487 0.486088 -0 0.187592 0.366382 0.514918 0.183191 0.366382 0.525731 0.350487 0.514918 0.486088 0.634038 0.776009 0.889227 0.958957 0.662791 0.794654 0.889227 0.662791 0.776009 0.634038]';
        % constant value for the drawing of the atom spheres
        sf = [1 35 36; 1 36 80; 1 56 35; 1 80 81; 1 81 56; 2 13 57; 2 19 13; 2 57 131; 2 131 151; 2 151 19; 3 38 112; 3 43 38; 3 112 186; 3 118 43; 3 186 118; 4 117 132; 4 132 187; 4 138 117; 4 187 221; 4 221 138; 5 168 202; 5 173 168; 5 202 256; 5 208 173; 5 256 208; 6 207 222; 6 222 257; 6 227 207; 6 257 291; 6 291 227; 7 238 272; 7 243 238; 7 272 326; 7 278 243; 7 326 278; 8 277 292; 8 292 327; 8 297 277; 8 327 352; 8 352 297; 9 58 97; 9 64 58; 9 97 313; 9 308 64; 9 313 308; 10 18 37; 10 37 167; 10 87 18; 10 167 342; 10 342 87; 11 63 82; 11 82 262; 11 102 63; 11 192 102; 11 262 192; 12 137 152; 12 152 237; 12 157 137; 12 237 307; 12 307 157; 13 14 20; 13 19 14; 13 20 42; 13 42 57; 14 15 21; 14 19 25; 14 21 20; 14 25 15; 15 16 22; 15 22 21; 15 25 30; 15 30 16; 16 17 23; 16 23 22; 16 30 34; 16 34 17; 17 18 24; 17 24 23; 17 34 37; 17 37 18; 18 83 24; 18 87 83; 19 151 165; 19 165 25; 20 21 26; 20 26 47; 20 47 42; 21 22 27; 21 27 26; 22 23 28; 22 28 27; 23 24 29; 23 29 28; 24 83 88; 24 88 29; 25 163 30; 25 165 163; 26 27 31; 26 31 51; 26 51 47; 27 28 32; 27 32 31; 28 29 33; 28 33 32; 29 88 92; 29 92 33; 30 160 34; 30 163 160; 31 32 35; 31 35 54; 31 54 51; 32 33 36; 32 36 35; 33 92 95; 33 95 36; 34 156 37; 34 160 156; 35 56 54; 36 95 80; 37 156 167; 38 39 44; 38 43 39; 38 44 101; 38 101 112; 39 40 45; 39 43 48; 39 45 44; 39 48 40; 40 41 46; 40 46 45; 40 48 52; 40 52 41; 41 42 47; 41 47 46; 41 52 55; 41 55 42; 42 55 57; 43 113 48; 43 118 113; 44 45 49; 44 49 105; 44 105 101; 45 46 50; 45 50 49; 46 47 51; 46 51 50; 48 113 119; 48 119 52; 49 50 53; 49 53 108; 49 108 105; 50 51 54; 50 54 53; 52 119 124; 52 124 55; 53 54 56; 53 56 110; 53 110 108; 55 124 128; 55 128 57; 56 81 110; 57 128 131; 58 59 65; 58 64 59; 58 65 86; 58 86 97; 59 60 66; 59 64 70; 59 66 65; 59 70 60; 60 61 67; 60 67 66; 60 70 75; 60 75 61; 61 62 68; 61 68 67; 61 75 79; 61 79 62; 62 63 69; 62 69 68; 62 79 82; 62 82 63; 63 98 69; 63 102 98; 64 308 331; 64 331 70; 65 66 71; 65 71 90; 65 90 86; 66 67 72; 66 72 71; 67 68 73; 67 73 72; 68 69 74; 68 74 73; 69 98 103; 69 103 74; 70 330 75; 70 331 330; 71 72 76; 71 76 93; 71 93 90; 72 73 77; 72 77 76; 73 74 78; 73 78 77; 74 103 107; 74 107 78; 75 329 79; 75 330 329; 76 77 80; 76 80 95; 76 95 93; 77 78 81; 77 81 80; 78 107 110; 78 110 81; 79 328 82; 79 329 328; 82 328 262; 83 84 88; 83 87 91; 83 91 84; 84 85 89; 84 89 88; 84 91 94; 84 94 85; 85 86 90; 85 90 89; 85 94 96; 85 96 86; 86 96 97; 87 341 91; 87 342 341; 88 89 92; 89 90 93; 89 93 92; 91 340 94; 91 341 340; 92 93 95; 94 339 96; 94 340 339; 96 338 97; 96 339 338; 97 338 313; 98 99 103; 98 102 106; 98 106 99; 99 100 104; 99 104 103; 99 106 109; 99 109 100; 100 101 105; 100 105 104; 100 109 111; 100 111 101; 101 111 112; 102 188 106; 102 192 188; 103 104 107; 104 105 108; 104 108 107; 106 188 193; 106 193 109; 107 108 110; 109 193 197; 109 197 111; 111 197 200; 111 200 112; 112 200 186; 113 114 119; 113 118 123; 113 123 114; 114 115 120; 114 120 119; 114 123 127; 114 127 115; 115 116 121; 115 121 120; 115 127 130; 115 130 116; 116 117 122; 116 122 121; 116 130 132; 116 132 117; 117 133 122; 117 138 133; 118 184 123; 118 186 184; 119 120 124; 120 121 125; 120 125 124; 121 122 126; 121 126 125; 122 133 139; 122 139 126; 123 181 127; 123 184 181; 124 125 128; 125 126 129; 125 129 128; 126 139 144; 126 144 129; 127 177 130; 127 181 177; 128 129 131; 129 144 148; 129 148 131; 130 172 132; 130 177 172; 131 148 151; 132 172 187; 133 134 139; 133 138 143; 133 143 134; 134 135 140; 134 140 139; 134 143 147; 134 147 135; 135 136 141; 135 141 140; 135 147 150; 135 150 136; 136 137 142; 136 142 141; 136 150 152; 136 152 137; 137 153 142; 137 157 153; 138 221 235; 138 235 143; 139 140 144; 140 141 145; 140 145 144; 141 142 146; 141 146 145; 142 153 158; 142 158 146; 143 233 147; 143 235 233; 144 145 148; 145 146 149; 145 149 148; 146 158 162; 146 162 149; 147 230 150; 147 233 230; 148 149 151; 149 162 165; 149 165 151; 150 226 152; 150 230 226; 152 226 237; 153 154 158; 153 157 161; 153 161 154; 154 155 159; 154 159 158; 154 161 164; 154 164 155; 155 156 160; 155 160 159; 155 164 166; 155 166 156; 156 166 167; 157 307 356; 157 356 161; 158 159 162; 159 160 163; 159 163 162; 161 355 164; 161 356 355; 162 163 165; 164 354 166; 164 355 354; 166 353 167; 166 354 353; 167 353 342; 168 169 174; 168 173 169; 168 174 191; 168 191 202; 169 170 175; 169 173 178; 169 175 174; 169 178 170; 170 171 176; 170 176 175; 170 178 182; 170 182 171; 171 172 177; 171 177 176; 171 182 185; 171 185 172; 172 185 187; 173 203 178; 173 208 203; 174 175 179; 174 179 195; 174 195 191; 175 176 180; 175 180 179; 176 177 181; 176 181 180; 178 203 209; 178 209 182; 179 180 183; 179 183 198; 179 198 195; 180 181 184; 180 184 183; 182 209 214; 182 214 185; 183 184 186; 183 186 200; 183 200 198; 185 214 218; 185 218 187; 187 218 221; 188 189 193; 188 192 196; 188 196 189; 189 190 194; 189 194 193; 189 196 199; 189 199 190; 190 191 195; 190 195 194; 190 199 201; 190 201 191; 191 201 202; 192 258 196; 192 262 258; 193 194 197; 194 195 198; 194 198 197; 196 258 263; 196 263 199; 197 198 200; 199 263 267; 199 267 201; 201 267 270; 201 270 202; 202 270 256; 203 204 209; 203 208 213; 203 213 204; 204 205 210; 204 210 209; 204 213 217; 204 217 205; 205 206 211; 205 211 210; 205 217 220; 205 220 206; 206 207 212; 206 212 211; 206 220 222; 206 222 207; 207 223 212; 207 227 223; 208 254 213; 208 256 254; 209 210 214; 210 211 215; 210 215 214; 211 212 216; 211 216 215; 212 223 228; 212 228 216; 213 251 217; 213 254 251; 214 215 218; 215 216 219; 215 219 218; 216 228 232; 216 232 219; 217 247 220; 217 251 247; 218 219 221; 219 232 235; 219 235 221; 220 242 222; 220 247 242; 222 242 257; 223 224 228; 223 227 231; 223 231 224; 224 225 229; 224 229 228; 224 231 234; 224 234 225; 225 226 230; 225 230 229; 225 234 236; 225 236 226; 226 236 237; 227 291 305; 227 305 231; 228 229 232; 229 230 233; 229 233 232; 231 303 234; 231 305 303; 232 233 235; 234 300 236; 234 303 300; 236 296 237; 236 300 296; 237 296 307; 238 239 244; 238 243 239; 238 244 261; 238 261 272; 239 240 245; 239 243 248; 239 245 244; 239 248 240; 240 241 246; 240 246 245; 240 248 252; 240 252 241; 241 242 247; 241 247 246; 241 252 255; 241 255 242; 242 255 257; 243 273 248; 243 278 273; 244 245 249; 244 249 265; 244 265 261; 245 246 250; 245 250 249; 246 247 251; 246 251 250; 248 273 279; 248 279 252; 249 250 253; 249 253 268; 249 268 265; 250 251 254; 250 254 253; 252 279 284; 252 284 255; 253 254 256; 253 256 270; 253 270 268; 255 284 288; 255 288 257; 257 288 291; 258 259 263; 258 262 266; 258 266 259; 259 260 264; 259 264 263; 259 266 269; 259 269 260; 260 261 265; 260 265 264; 260 269 271; 260 271 261; 261 271 272; 262 328 266; 263 264 267; 264 265 268; 264 268 267; 266 328 332; 266 332 269; 267 268 270; 269 332 335; 269 335 271; 271 335 337; 271 337 272; 272 337 326; 273 274 279; 273 278 283; 273 283 274; 274 275 280; 274 280 279; 274 283 287; 274 287 275; 275 276 281; 275 281 280; 275 287 290; 275 290 276; 276 277 282; 276 282 281; 276 290 292; 276 292 277; 277 293 282; 277 297 293; 278 324 283; 278 326 324; 279 280 284; 280 281 285; 280 285 284; 281 282 286; 281 286 285; 282 293 298; 282 298 286; 283 321 287; 283 324 321; 284 285 288; 285 286 289; 285 289 288; 286 298 302; 286 302 289; 287 317 290; 287 321 317; 288 289 291; 289 302 305; 289 305 291; 290 312 292; 290 317 312; 292 312 327; 293 294 298; 293 297 301; 293 301 294; 294 295 299; 294 299 298; 294 301 304; 294 304 295; 295 296 300; 295 300 299; 295 304 306; 295 306 296; 296 306 307; 297 352 362; 297 362 301; 298 299 302; 299 300 303; 299 303 302; 301 361 304; 301 362 361; 302 303 305; 304 359 306; 304 361 359; 306 356 307; 306 359 356; 308 309 314; 308 313 309; 308 314 331; 309 310 315; 309 313 318; 309 315 314; 309 318 310; 310 311 316; 310 316 315; 310 318 322; 310 322 311; 311 312 317; 311 317 316; 311 322 325; 311 325 312; 312 325 327; 313 338 318; 314 315 319; 314 319 334; 314 334 331; 315 316 320; 315 320 319; 316 317 321; 316 321 320; 318 338 343; 318 343 322; 319 320 323; 319 323 336; 319 336 334; 320 321 324; 320 324 323; 322 343 347; 322 347 325; 323 324 326; 323 326 337; 323 337 336; 325 347 350; 325 350 327; 327 350 352; 328 329 332; 329 330 333; 329 333 332; 330 331 334; 330 334 333; 332 333 335; 333 334 336; 333 336 335; 335 336 337; 338 339 343; 339 340 344; 339 344 343; 340 341 345; 340 345 344; 341 342 346; 341 346 345; 342 353 346; 343 344 347; 344 345 348; 344 348 347; 345 346 349; 345 349 348; 346 353 357; 346 357 349; 347 348 350; 348 349 351; 348 351 350; 349 357 360; 349 360 351; 350 351 352; 351 360 362; 351 362 352; 353 354 357; 354 355 358; 354 358 357; 355 356 359; 355 359 358; 357 358 360; 358 359 361; 358 361 360; 360 361 362];
    end
    
    methods
        function Str = get.Setting(obj)
            if isempty(obj.Setting)
                if ~isempty(obj.Spacegroup)
                    Str = obj.Spacegroup.SettingDescription;
                else
                    Str = '';
                end
            else
                Str = obj.Setting;
            end
        end
        
        function Str = get.SpacegroupNumber(obj)
            if isempty(obj.Spacegroup)
                Str = '';
                return;
            end
            Str = [num2str(obj.Spacegroup.Number), ' (', obj.Spacegroup.FullName, ')'];
        end
        
        function set.a(obj, a)
            % set the a_vec to the length given by a
            e = obj.a_vec./obj.a;
            obj.a_vec = a.*e;
        end
        function a = get.a(obj)
            % length of a_vec
            a = norm(obj.a_vec);
        end
        function set.b(obj, b)
            % set the b_vec to the length given by b
            e = obj.b_vec./obj.b;
            obj.b_vec = b.*e;
        end
        function b = get.b(obj)
            % length of b_vec
            b = norm(obj.b_vec);
        end
        function set.c(obj, c)
            % set the c_vec to the length given by c
            e = obj.c_vec./obj.c;
            obj.c_vec = c.*e;
        end
        function c = get.c(obj)
            % length of c_vec
            c = norm(obj.c_vec);
        end
        
        function set.alpha(obj, alpha)
            % set the alpha angle (between b_vec and c_vec)
            
            % get the rotation matrix to rotate the crystal into default
            % orientation (i.e. a_vec in direction of the x axis, a_vec and
            % b_vec in the x-y-plane)
            RDef = RForDefault(obj);
            % rotate the crystal to default orientation
            Rotate(obj, RDef);
            
            % store the lattice parameters
            aC = obj.a;
            bC = obj.b;
            cC = obj.c;
            betaC = obj.beta;
            gammaC = obj.gamma;
            
            % calculate the volumne
            VT = obj.CalculateV(aC, bC, cC, alpha, betaC, gammaC);
            
            % update the c_vec
            obj.c_vec = [cC*cosd(betaC); ...
                cC*(cosd(alpha)-cosd(betaC)*cosd(gammaC))/sind(gammaC); ...
                VT/(aC*bC*sind(gammaC))];
            % make matrix of a_vec, b_vec and c_vec
            M = [obj.a_vec obj.b_vec obj.c_vec];
            % rotate back a_vec, b_vec and c_vec
            M = RDef\M;
            % assign values
            obj.a_vec = M(:, 1);
            obj.b_vec = M(:, 2);
            obj.c_vec = M(:, 3);
        end
        function alpha = get.alpha(obj)
            % angle between b_vec and c_vec
            alpha = acosd(dot(obj.b_vec, obj.c_vec)/(obj.b*obj.c));
        end
        function set.beta(obj, beta)
            % see set.alpha
            RDef = RForDefault(obj);
            Rotate(obj, RDef);
            
            aC = obj.a;
            bC = obj.b;
            cC = obj.c;
            alphaC = obj.alpha;
            gammaC = obj.gamma;
            
            VT = obj.CalculateV(aC, bC, cC, alphaC, beta, gammaC);
            
            obj.c_vec = [cC*cosd(beta); ...
                cC*(cosd(alphaC)-cosd(beta)*cosd(gammaC))/sind(gammaC); ...
                VT/(aC*bC*sind(gammaC))];
            M = [obj.a_vec obj.b_vec obj.c_vec];
            M = RDef\M;
            obj.a_vec = M(:, 1);
            obj.b_vec = M(:, 2);
            obj.c_vec = M(:, 3);
        end
        function beta = get.beta(obj)
            % angle between a_vec and c_vec
            beta = acosd(dot(obj.a_vec, obj.c_vec)/(obj.a*obj.c));
        end
        function set.gamma(obj, gamma)
            % see set.alpha
            RDef = RForDefault(obj);
            Rotate(obj, RDef);
            
            aC = obj.a;
            bC = obj.b;
            cC = obj.c;
            alphaC = obj.alpha;
            betaC = obj.beta;
            
            VT = obj.CalculateV(aC, bC, cC, alphaC, betaC, gamma);
            % changing gamma will also change b_vec, not only c_vec
            obj.b_vec = [bC*cosd(gamma); bC*sind(gamma); 0];
            obj.c_vec = [cC*cosd(betaC); ...
                cC*(cosd(alphaC)-cosd(betaC)*cosd(gamma))/sind(gamma); ...
                VT/(aC*bC*sind(gamma))];
            M = [obj.a_vec obj.b_vec obj.c_vec];
            M = RDef\M;
            obj.a_vec = M(:, 1);
            obj.b_vec = M(:, 2);
            obj.c_vec = M(:, 3);
        end
        function gamma = get.gamma(obj)
            % angle between a_vec and b_vec
            gamma = acosd(dot(obj.a_vec, obj.b_vec)/(obj.a*obj.b));
        end
        
        function LT = get.LatticeType(obj)
            % get the string representation of the lattice type as
            % determined by the SHELX LATT parameter
            if isempty(obj.Latt)
                LT = 'unknown';
            else
                switch abs(obj.Latt)
                    case 1
                        LT = 'P';
                    case 2
                        LT = 'I';
                    case 3
                        LT = 'rhombohedral obverse on hexagonal axes';
                    case 4
                        LT = 'F';
                    case 5
                        LT = 'A';
                    case 6
                        LT = 'B';
                    case 7
                        LT = 'C';
                    otherwise
                        LT = 'unknown';
                end
            end
        end
        
        function CS = get.CentroSymmetric(obj)
            % is crystal centro symmetric or not
            if isempty(obj.Latt)
                CS = [];
            elseif obj.Latt > 0
                CS = true;
            else
                CS = false;
            end
        end
        function V = get.V(obj)
            % calculate the volumne of the unit cell
             V = obj.CalculateV(obj.a, obj.b, obj.c, obj.alpha, obj.beta, obj.gamma);
        end
        function a_star = get.a_star(obj)
            % get reciprocal space length a*
             a_star = 2*pi*obj.b*obj.c*sind(obj.alpha)/obj.V;
        end
        function b_star = get.b_star(obj)
            % get reciprocal space length b*
             b_star = 2*pi*obj.c*obj.a*sind(obj.beta)/obj.V;
        end
        function c_star = get.c_star(obj)
            % get reciprocal space length c*
             c_star = 2*pi*obj.b*obj.a*sind(obj.gamma)/obj.V;
        end
        function alpha_star = get.alpha_star(obj)
            % get reciprocal space angle alpha*
            alpha_star = acosd((cosd(obj.beta)*cosd(obj.gamma)-cosd(obj.alpha))/(sind(obj.beta)*sind(obj.gamma)));
        end
        function beta_star = get.beta_star(obj)
            % get reciprocal space angle beta*
            beta_star = acosd((cosd(obj.gamma)*cosd(obj.alpha)-cosd(obj.beta))/(sind(obj.gamma)*sind(obj.alpha)));
        end
        function gamma_star = get.gamma_star(obj)
            % get reciprocal space angle gamma*
            gamma_star = acosd((cosd(obj.alpha)*cosd(obj.beta)-cosd(obj.gamma))/(sind(obj.alpha)*sind(obj.beta)));
        end
        
        function a_star_vec = get.a_star_vec(obj)
            % get reciprocal space vector a_vec*
            a_star_vec = 2*pi*cross(obj.b_vec, obj.c_vec)./obj.V;
        end
        function b_star_vec = get.b_star_vec(obj)
            % get reciprocal space vector b_vec*
            b_star_vec = 2*pi*cross(obj.c_vec, obj.a_vec)./obj.V;
        end
        function c_star_vec = get.c_star_vec(obj)
            % get reciprocal space vector c_vec*
            c_star_vec = 2*pi*cross(obj.a_vec, obj.b_vec)./obj.V;
        end
        
        function obj = GIDVis_Crystal(varargin)
            % Create a GIDVis_Crystal.
            %
            % Usage:
            % GIDVis_Crystal creates a default crystal.
            % GIDVis_Crystal(a, b, c, alpha, beta, gamma, name) creates a
            % crystal using lattice parameters a, b, c, alpha, beta and
            % gamma and the name.
            % GIDVis_Crystal(a, b, c, alpha, beta, gamma, name, SpacegroupNumber, UnitCell, Setting, SFAC, UNIT, Symm) creates a
            % crystal using lattice parameters a, b, c, alpha, beta and
            % gamma, the name, the SpacegroupNumber, the AsymmetricUnit, SFAC, UNIT and Symm.
            % GIDVis_Crystal(FilePath) reads crystal parameters from the
            % *.res file FilePath and returns a crystal.
            % GIDVis_Crystal(Cr) creates the crystal from GIDVis_Crystal Cr.
            if nargin == 0
                obj.a_vec = [5; 0; 0];
                obj.b_vec = [0; 5; 0];
                obj.c_vec = [0; 0; 5];
                obj.Name = 'Default Crystal';
            elseif nargin == 1 % import from *.res file or copying GIDVis_Crystal
                if ischar(varargin{1})
                    obj = obj.ReadResFile(varargin{1});
                elseif isa(varargin{1}, 'GIDVis_Crystal')
                    Cr = varargin{1};
                    obj.Name = Cr.Name;
                    obj.a_vec = Cr.a_vec;
                    obj.b_vec = Cr.b_vec;
                    obj.c_vec = Cr.c_vec;
                    obj.Latt = Cr.Latt;
                    obj.UnitCell = Cr.UnitCell;
                    obj.AsymmetricUnit = Cr.AsymmetricUnit;
                    obj.Symm = Cr.Symm;
                    obj.Delta_a = Cr.Delta_a;
                    obj.Delta_b = Cr.Delta_b;
                    obj.Delta_c = Cr.Delta_c;
                    obj.Delta_alpha = Cr.Delta_alpha;
                    obj.Delta_beta = Cr.Delta_beta;
                    obj.Delta_gamma = Cr.Delta_gamma;
                    obj.Latt = Cr.Latt;
                elseif iscell(varargin) && isstruct(varargin{1})
                    S = varargin{1};
                    if isfield(S, 'a')
                        a = S.a;
                        b = S.b;
                        c = S.c;
                        alpha = S.alpha;
                        beta = S.beta;
                        gamma = S.gamma;
                        V = obj.CalculateV(a, b, c, alpha, beta, gamma);

                        % calculate a_vec, b_vec and c_vec
                        obj.a_vec = [a; 0; 0];
                        obj.b_vec = [b*cosd(gamma); b*sind(gamma); 0];
                        obj.c_vec = [c*cosd(beta); c*(cosd(alpha)-cosd(beta)*cosd(gamma))/sind(gamma); V/(a*b*sind(gamma))];
                    else
                        obj.a_vec = S.a_vec;
                        obj.b_vec = S.b_vec;
                        obj.c_vec = S.c_vec;
                    end
            
                    obj.Name = S.Name;
                    obj.Delta_a = S.Delta_a;
                    obj.Delta_b = S.Delta_b;
                    obj.Delta_c = S.Delta_c;
                    obj.Delta_alpha = S.Delta_alpha;
                    obj.Delta_beta = S.Delta_beta;
                    obj.Delta_gamma = S.Delta_gamma;
                    obj.Latt = S.Latt;
                    obj.UnitCell = S.UnitCell;
                    obj.AsymmetricUnit = S.AsymmetricUnit;
                    obj.Symm = S.Symm;
                end
            elseif nargin == 7
                % get out lattice parameters
                a = varargin{1};
                b = varargin{2};
                c = varargin{3};
                alpha = varargin{4};
                beta = varargin{5};
                gamma = varargin{6};
                
                % calculate volumne of unit cell
                V = obj.CalculateV(a, b, c, alpha, beta, gamma);
        
                % calculate a_vec, b_vec and c_vec
                obj.a_vec = [a; 0; 0];
                obj.b_vec = [b*cosd(gamma); b*sind(gamma); 0];
                obj.c_vec = [c*cosd(beta); c*(cosd(alpha)-cosd(beta)*cosd(gamma))/sind(gamma); V/(a*b*sind(gamma))];

                obj.Name = varargin{7};
            elseif nargin == 13
                % get lattice parameters
                a = varargin{1};
                b = varargin{2};
                c = varargin{3};
                alpha = varargin{4};
                beta = varargin{5};
                gamma = varargin{6};
                
                % calculate volumne
                V = obj.CalculateV(a, b, c, alpha, beta, gamma);
        
                % calculate a_vec, b_vec, c_vec
                obj.a_vec = [a; 0; 0];
                obj.b_vec = [b*cosd(gamma); b*sind(gamma); 0];
                obj.c_vec = [c*cosd(beta); c*(cosd(alpha)-cosd(beta)*cosd(gamma))/sind(gamma); V/(a*b*sind(gamma))];
                
                % assign other properties
                obj.Name = varargin{7};
                obj.AsymmetricUnit = varargin{9};
                obj.Setting = varargin{10};
                obj.SFAC = varargin{11};
                obj.UNIT = varargin{12};
                obj.Symm = varargin{13};
                [obj.UnitCell, obj.Spacegroup, ErrorMessages] = CreateUnitCell(obj);
                Passed = obj.CheckUnitCellConsistency;
                if ~isempty(Passed) && ~Passed
                    obj.UnitCell = [];
                    obj.AsymmetricUnit = [];
                    obj.UnitCellConsistencyMessage(Passed, ErrorMessages);
                end
            else
                error('Provide either 0, 1, 7 or 13 input arguments.');
            end
        end
        
        [means, sigmas, q, Rands] = DeltaQ(obj, hkl, Orientation, N);
        Orient(obj, hkl, Dir);
    end
    
    methods (Static)
        hkl = CreateHKLCombinations(h, k, l);
        NewAtoms = RemoveDuplicateAtoms(NewAtoms);
        R = RotMat(f, t);
    end
    
    methods (Access = private, Static = true)
        V = CalculateV(a, b, c, alpha, beta, gamma);
    end
    
    methods (Access=private)        
        obj = ReadResFile(obj, FilePath);
        A = AMatrix(obj, TargetOrientation);
        SpgrArr = SpacegroupSuggestions(Cr)
%         [UnitCell, Spgrnr, CrSetting, ErrorMsg] = CreateUnitCell(Cr);
        [UnitCell, Spacegr, ErrorMessages] = CreateUnitCell(Cr);
        UnitCellConsistencyMessage(~, Passed, ErrorMsg);
        
    end % methods (Access=private)
end % classdef Crystal