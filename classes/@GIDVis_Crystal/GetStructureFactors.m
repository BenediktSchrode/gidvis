function F = GetStructureFactors(obj, HKL)
    % Calculates the structure factors of given HKL values.
    %
    % Usage:
    %     F = GetStructureFactors(obj, HKL)
    % 
    % Input:
    %     obj ... GIDVis_Crystal object
    %     HKL ... hkl indices of the peaks to calculate the structure
    %             factors for as a n x 3 matrix.
    %
    % Output:
    %     F ... n x 1 vector containing the structure factors
    %
    % This file is part of GIDVis.
    %
    % see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if numel(obj.UnitCell) == 0
        F = [];
        return;
    end

    q_vek = obj.PeakPositionsQ(HKL, [0 1 1]); % orientation is not important, only magnitude of q vector is important
    q = sqrt(sum(q_vek.^2, 2)); % normalize q
    affValues = aff([obj.UnitCell.Element], q);
    F = zeros(size(HKL, 1), 1);
    Beq = obj.UnitCell.Beq(obj);
    for iHKL = 1:size(HKL, 1)
        for j = 1:numel(obj.UnitCell)
            F(iHKL, 1) = F(iHKL, 1) + affValues(iHKL, j)*...
                exp(2*pi*-1i*(HKL(iHKL, 1)*obj.UnitCell(j).x+HKL(iHKL, 2)*obj.UnitCell(j).y+HKL(iHKL, 3)*obj.UnitCell(j).z))*...
                exp(-Beq(j)*(q(iHKL)/(4*pi))^2)*...
                obj.UnitCell(j).SOF;
        end
    end
end