function [SFSummedFull, EqFull] = GetStructureFactors2Rot(obj, hkl, Orientation, Tolerance)
    % Calculates the square of the absolute value of the structure factors
    % including a full crystal rotation around the axis perpendicular to
    % the orientation plane by summing up the |SF|^2 contributions of peaks
    % lying at the same qxy and qz positions.
    %
    % Usage:
    %     F = GetStructureFactors2Rot(obj, HKL, Orientation, Tolerance)
    % 
    % Input:
    %     obj ... GIDVis_Crystal object
    %     hkl ... hkl indices of the peaks to calculate the |SF|^2 for as 
    %             n x 3 matrix.
    %     Orientation ... Orientation of the crystal.
    %     Tolerance ... tolerance for determination of uniqueness. Default 
    %                   1e-8, if not given.
    %
    % Output:
    %     F ... n x 1 vector containing the summed |SF|^2 values
    %
    % This file is part of GIDVis.
    %
    % see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    if isempty(obj.UnitCell) % no structure factors can be calculated
        SFSummedFull = [];
        EqFull = cell.empty(1, 0);
        return;
    end

    % assign default if necessary
    if nargin < 4 || isempty(Tolerance); Tolerance = 1e-8; end
    
    % calculate the peak positions (qx, qy, qz)
    qxyz = PeakPositionsQ(obj, hkl, Orientation);

    % calculate in-plane and out-of-plane component of each peak
    qxyOriginal = sqrt(qxyz(:, 1).^2 + qxyz(:, 2).^2);
    qzOriginal = qxyz(:, 3);
    % calculate length of the scattering vector for each peak
    qOriginal = sqrt(qxyOriginal.^2 + qzOriginal.^2);
    
    % initialize the output variables
    SFSummedFull = NaN(size(qzOriginal));    
    EqFull = cell(size(SFSummedFull));

    for ihkl = 1:size(hkl, 1)
        % calculate the plane spacing
        d = 2*pi/qOriginal(ihkl);
        % determine the maximum h, k and l values which should be checked
        % for "equivalent" peaks. Add 1 for safety.
        maxh = ceil(obj.a/d) + 1;
        maxk = ceil(obj.b/d) + 1;
        maxl = ceil(obj.c/d) + 1;

        % Create all hkl combinations with the determined maximum h, k and
        % l values. They are the candidates
        Fullhkl = obj.CreateHKLCombinations(-maxh:maxh, -maxk:maxk, -maxl:maxl);

        % calculate the peak positions of the candidates
        qxyz = PeakPositionsQ(obj, Fullhkl, Orientation);
        qxy = sqrt(qxyz(:, 1).^2 + qxyz(:, 2).^2);
        qz = qxyz(:, 3);
        q = sqrt(qxy.^2 + qz.^2);
        
        % remove candidates which have very different q values from the one
        % we're investigating
        L = q < qOriginal(ihkl) + 0.1 & q > qOriginal(ihkl) - 0.1;
        Fullhkl = Fullhkl(L, :);
        qxy = qxy(L);
        qz = qz(L);

        % initialize a vector holding the equivalent peaks
        Eq = false(size(qxy));

        for iC = 1:size(Fullhkl, 1)
            % compare the hkl peak with all the other combinations
            if abs(qxyOriginal(ihkl) - qxy(iC)) < Tolerance && ...
                    abs(qzOriginal(ihkl) - qz(iC)) < Tolerance
                Eq(iC) = true;
            end
        end
        
        % store all equivalent peaks in the cell
        EqFull{ihkl} = Fullhkl(Eq, :);
        % calculate the absolute values of the structure factors of the
        % equivalent peaks
        SF = abs(GetStructureFactors(obj, EqFull{ihkl}));
        % calculate the sum of the absolute values of the structure factors
        SFSummedFull(ihkl) = sum(SF.^2);
    end
end