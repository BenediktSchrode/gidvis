function Atoms = InPlaneAtoms(obj, hkl, Delta, alims, blims, clims)
    % Returns atoms which are lying within +/- Delta of the plane given by
    % hkl.
    % alims, blims and clims determines how large the unit cell is expanded
    % before slicing.
    % alims, blims and clims are defaulted to [-2 2] if not given.
    %
    % Usage: 
    %     InPlaneAtoms(obj, hkl, Delta, alims, blims, clims)
    %
    % Input:
    %     obj ... GIDVis_Crystal object
    %     hkl ... three-element vector of Miller indices of the plane
    %     Delta ... limit how close an atom has to be to the plane to be
    %               considered
    %     alims ... limits in the direction of obj.a_vec. Default [-2 2].
    %     blims ... limits in the direction of obj.b_vec. Default [-2 2].
    %     clims ... limits in the direction of obj.c_vec. Default [-2 2].
    %
    % Output:
    %     Atoms ... vector of GIDVis_Crystal_Atom
    %
    % This file is part of GIDVis.
    %
    % see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    if isempty(obj.UnitCell); Atoms = []; return; end

    % make default inputs if necessary
    if nargin < 4 || isempty(alims); alims = [-2 2]; end
    if nargin < 5 || isempty(blims); blims = [-2 2]; end
    if nargin < 6 || isempty(clims); clims = [-2 2]; end
    
    % increase the unit cell
    Atoms = IncreaseUnitCell(obj, alims, blims, clims);
    % calculate cartesian coordinates from the fractional
    % coordinates of the atoms
    Pos = CalculateCartesianCoordinates(obj, [[Atoms.x]' [Atoms.y]' [Atoms.z]']);
    % calculate the vertices of the hkl plane
    Vertices = hklPlane(obj, hkl);
    % calculate the normal vector of the hkl plane
    n = NormalVector(obj, hkl);
    % initialize vector for the distance between each atom and the
    % plane
    D = NaN(numel(Atoms), 1);
    % calculate the distance between each atom and the plane
    for iD = 1:numel(D)
        D(iD) = dot(n, (Pos(iD, :)-Vertices(1, :)));
    end

    % remove atoms which are further away from the plane than SDeltaD
    L = abs(D) <= Delta;
    Atoms = Atoms(L);