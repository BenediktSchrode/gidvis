function Atoms = IncreaseUnitCell(obj, alims, blims, clims)
    % Returns atoms which are inside the volume given after
    % translating the unit cell in the direction of a_vec (alims),
    % b_vec (blims) and c_vec (clims).
    % E.g. alims = [0 2] means that the unit cell is translated 0
    % times, once and twice in the a_vec direction and all atoms
    % inside the resulting volume are returned.
    %
    % Usage:
    %     Atoms = IncreaseUnitCell(obj, alims, blims, clims)
    %
    % Input:
    %     obj ... GIDVis_Crystal object
    %     alims ... limits in the direction of obj.a_vec. Default [0 1].
    %     blims ... limits in the direction of obj.b_vec. Default [0 1].
    %     clims ... limits in the direction of obj.c_vec. Default [0 1].
    %
    % Output:
    %     Atoms ... vector of GIDVis_Crystal_Atom
    %
    % This file is part of GIDVis.
    %
    % see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % check if there are atoms in the unit cell
    if isempty(obj.UnitCell)
        warning('GIDVis_Crystal:NoAtoms', 'No atoms are available in the unit cell. Import the crystal from a *.res file instead.');
        Atoms = GIDVis_Crystal_Atom.empty(0);
        return;
    end

    % check input arguments
    if nargin < 2 || isempty(alims); alims = [0 1]; end
    if nargin < 3 || isempty(blims); blims = [0 1]; end
    if nargin < 4 || isempty(clims); clims = [0 1]; end

    % we have to repeat the unit cell on each corner of a unit cube,
    % which can be done by shifting the unit cell in x, y, and z
    % directions
    [Sx, Sy, Sz] = ndgrid(floor(alims(1)):ceil(alims(2)), ...
        floor(blims(1)):ceil(blims(2)), ...
        floor(clims(1)):ceil(clims(2)));
    Shifts = [Sx(:) Sy(:) Sz(:)];
    Shifts = unique(Shifts, 'rows');

    % positions of the unit cell atoms (fractional coordinates)
    FPos = [[obj.UnitCell.x]' [obj.UnitCell.y]' [obj.UnitCell.z]'];

    % calculate the shifted positions
    Atoms = repmat(obj.UnitCell(:), size(Shifts, 1), 1);
    for i = 1:size(Shifts, 1)
        ShiftedPos = num2cell(FPos + repmat(Shifts(i, :), numel(obj.UnitCell), 1));
        [Atoms((i-1)*numel(obj.UnitCell)+1:i*numel(obj.UnitCell), :).x] = ...
            ShiftedPos{:, 1};
        [Atoms((i-1)*numel(obj.UnitCell)+1:i*numel(obj.UnitCell), :).y] = ...
            ShiftedPos{:, 2};
        [Atoms((i-1)*numel(obj.UnitCell)+1:i*numel(obj.UnitCell), :).z] = ...
            ShiftedPos{:, 3};
    end

    Pos = [[Atoms.x]' [Atoms.y]' [Atoms.z]'];

    % remove atoms lying outside the unit cell
    L = Pos(:, 1) < alims(1) | Pos(:, 1) > alims(2) | ...
        Pos(:, 2) < blims(1) | Pos(:, 2) > blims(2) | ...
        Pos(:, 3) < clims(1) | Pos(:, 3) > clims(2);
    Atoms(L) = [];
end