function nFull = NormalVector(obj, hklFull)
    % Returns the normalized vector perpendicular to the plane
    % given by hkl.
    %
    % Usage:
    %     n = NormalVector(obj, hkl);
    %
    % Input:
    %     obj ... GIDVis_Crystal object
    %     hklFull ... n x 3 matrix of hkl values
    %
    % Output:
    %     nFull ... n x 3 matrix of normal vectors
    %
    % This file is part of GIDVis.
    %
    % see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % same calculation as in the method AMatrix
    A = [obj.a_star_vec, obj.b_star_vec, obj.c_star_vec];
    nFull = (A*hklFull')';
    L = sqrt(sum(nFull.^2, 2));
    nFull = nFull./repmat(L, 1, 3);
end