function Orient(obj, hkl, Dir)
    % Rotates the crystal so that the normal vector of the crystallographic
    % plane given by hkl is parallel to the direction given by Dir.
    %
    % Usage:
    %     Orient(obj, hkl, Dir)
    %
    % Input:
    %     obj ... GIDVis_Crystal object
    %     hkl ... Miller indices of the plane
    %     Dir ... real space direction to which the crystallographic plane
    %             should be perpendicular
    %
    %
    % This file is part of GIDVis.
    %
    % see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2019 Benedikt Schrode                                     %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % In case Dir is not given, it should be [0 0 1]
    if nargin < 3 || isempty(Dir); Dir = [0 0 1]; end
    
    % normalize Dir
    Dir = Dir./sqrt(sum(Dir.^2));
    
    % get the normal vector
    NV = NormalVector(obj, hkl);
    
    % calculate the rotation matrix
    R = obj.RotMat(NV, Dir);
    
    % finally, rotate the crystal using the rotation matrix R
    Rotate(obj, R);
end