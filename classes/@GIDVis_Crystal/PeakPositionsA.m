function [psi, phi] = PeakPositionsA(obj, hkl, TargetOrientation)
    % Calculate the peak positions in angular space, i.e. as psi
    % (inclination) and phi (azimuth).
    %
    % Usage:
    %     [psi, phi] = PeakPositionsA(obj, hkl, TargetOrientation)
    %
    % Input:
    %     obj ... GIDVis_Crystal object
    %     hkl ... n x 3 matrix of hkl values
    %     TargetOrientation ... Orientation of the crystal (plane parallel
    %                           to the substrate surface)
    %
    % Output:
    %     psi ... n x 1 vector of psi values (inclination)
    %     phi ... n x 1 vector of phi values (azimuth)
    %
    % This file is part of GIDVis.
    %
    % see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % get the peak positions in reciprocal space
    q = PeakPositionsQ(obj, hkl, TargetOrientation);
    % extract single values
    qx = q(:, 1);
    qy = q(:, 2);
    qz = q(:, 3);
    qxy = sqrt(qx.^2 + qy.^2);
    % calculate the psi and phi components
    psi = atan2(qxy, qz)*180/pi;
    psi = mod(psi, 180); % makes 180 to 0
    phi = atan2(qy, qx)*180/pi;
end