function R = RForDefault(obj)
    % Returns the rotation matrix R which rotates the crystal into
    % default orientation, i.e. with a_vec being in the x
    % direction, and a_vec and b_vec in the x-y-plane.
    %
    % Usage:
    %     R = RForDefault(obj)
    %
    % Input:
    %     obj ... GIDVis_Crystal object
    %
    % Output:
    %     R ... 3 x 3 rotation matrix
    %
    % This method does not rotate the crystal; to directly rotate
    % the crystal to the default orientation use
    %     Rotate(obj, RForDefault(obj))
    %
    %
    % This file is part of GIDVis.
    %
    % see also: Rotate, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % create a matrix of a_vec, b_vec and c_vec
    M = [obj.a_vec obj.b_vec obj.c_vec];
    % first we want to rotate a_vec and b_vec into the x-y-plane,
    % i.e. the normal vector of the plane spanned by a_vec and
    % b_vec has to be into the z direction.
    % get the normal vector
    n = cross(M(:, 1), M(:, 2));
    n = n./norm(n);
    % get the rotation matrix
    R = obj.RotMat(n, [0 0 1]);
    % perform rotation
    M = R*M;
    % finally, get the rotation matrix to rotate the a_vec into the
    % x direction
    R = obj.RotMat(M(:, 1)'./obj.a, [1 0 0])*R;
end