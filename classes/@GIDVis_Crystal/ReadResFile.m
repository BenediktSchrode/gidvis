function obj = ReadResFile(obj, FilePath)
    % Creates a GIDVis_Crystal from a res file.
    %
    % Usage:
    %     obj = ReadResFile(obj, FilePath);
    %
    % Input:
    %     obj ... GIDVis_Crystal object
    %     FilePath ... path to the *.res file
    %
    % Output:
    %     obj ... GIDVis_Crystal object
    %
    % This file is part of GIDVis.
    %
    % see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    [~, ~, ext] = fileparts(FilePath);
    % check the extension
    if ~strcmpi(ext, '.res')
        error('The provided file (%s) is not a *.res file.', FilePath)
    end
    [fid, message] = fopen(FilePath);
    if fid == -1
        error('Could not load file %s.\n\nReason: %s.', FilePath, message);
    end

    AtomCounter = 1;
    MovedAtoms = false;
    OmittedAtoms = false;

    ResFileReserved = {'ABIN', 'ACTA', 'AFIX', 'ANIS', 'ANSC', 'ANSR', 'BASF', 'BIND', 'BLOC', 'BOND', 'BUMP', 'CELL', 'CGLS', 'CHIV', 'CONF', 'CONN', 'DAMP', 'DANG', 'DEFS', 'DELU', 'DFIX', 'DISP', 'EADP', 'END', 'EQIV', 'EXTI', 'EXYZ', 'FEND', 'FLAT', 'FMAP', 'FRAG', 'FREE', 'FVAR', 'GRID', 'HFIX', 'HKLF', 'HTAB', 'ISOR', 'LATT', 'LAUE', 'LIST', 'L.S.', 'MERG', 'MORE', 'MOVE', 'MPLA', 'NCSY', 'NEUT', 'OMIT', 'PART', 'PLAN', 'PRIG', 'REM', 'RESI', 'RIGU', 'RTAB', 'SADI', 'SAME', 'SFAC', 'SHEL', 'SIMU', 'SIZE', 'SPEC', 'STIR', 'SUMP', 'SWAT', 'SYMM', 'TEMP', 'TITL', 'TWIN', 'TWST', 'UNIT', 'WGHT', 'WIGL', 'WPDB', 'XNPD', 'ZERR'};

    tline = fgetl(fid);            
    while ischar(tline)
        % comments in *.res file are started with a !
        % remove everything after this
        k = strfind(tline, '!');
        tline(k:end) = [];
        tline = strtrim(tline);
        if strncmpi(tline, 'CELL', 4) % read out unit cell parameters
            Params = str2num(tline(5:end)); %#ok BS
            aC = Params(2);
            bC = Params(3);
            cC = Params(4);
            alphaC = Params(5);
            betaC = Params(6);
            gammaC = Params(7);
            VC = aC*bC*cC*...
                sqrt(1-cosd(alphaC)^2-cosd(betaC)^2-cosd(gammaC)^2 + ...
                2*cosd(alphaC)*cosd(betaC)*cosd(gammaC));

            obj.a_vec = [aC; 0; 0];
            obj.b_vec = [bC*cosd(gammaC); bC*sind(gammaC); 0];
            obj.c_vec = [cC*cosd(betaC); cC*(cosd(alphaC)-cosd(betaC)*cosd(gammaC))/sind(gammaC); VC/(aC*bC*sind(gammaC))];
        elseif strncmpi(tline, 'TITL', 4) % read out title
            obj.Name = strtrim(tline(5:end));
        elseif strncmpi(tline, 'SYMM', 4)
            obj.Symm{end+1, 1} = strrep(tline(5:end), ' ', '');
        elseif strncmpi(tline, 'SFAC', 4) % read out SFAC line
            TempSFAC = strtrim(tline(5:end));
            while ~isempty(strfind(TempSFAC, '  ')) %#ok BS. Backwards compatibility
                TempSFAC = strrep(TempSFAC, '  ', ' ');
            end
            obj.SFAC = regexp(strrep(TempSFAC, '$', ''), ' ', 'split');
        elseif strncmpi(tline, 'UNIT', 4)
            obj.UNIT = str2num(strtrim(tline(5:end))); %#ok BS
        elseif strncmpi(tline, 'MOVE', 4)
            MovedAtoms = true;
        elseif strncmpi(tline, 'OMIT', 4)
            OmittedAtoms = true;
        elseif strncmpi(tline, 'LATT', 4)
            obj.Latt = str2double(tline(5:end));
        elseif strncmpi(tline, 'ZERR', 4)
            Params = str2num(tline(5:end)); %#ok BS
            obj.Delta_a = Params(2);
            obj.Delta_b = Params(3);
            obj.Delta_c = Params(4);
            obj.Delta_alpha = Params(5);
            obj.Delta_beta = Params(6);
            obj.Delta_gamma = Params(7);
        else
            % check if the line can be imported as
            % Atomname SF x y z SOF U
            % or
            % Name SF x y z SOF U11 U22 U33 U23 U13 U12

            % remove leading and trailing spaces
            Str = strtrim(tline);
            Str = strtrim(Str);
            % replace double spaces by single spaces
            while ~isempty(strfind(Str, '  ')) %#ok BS. Backwards compatibility
                Str = strrep(Str, '  ', ' ');
            end
            % split the string at spaces
            splitted = regexp(Str, ' ', 'split');
            % Check resulting array according to the following
            % rules (from SHELXL instructions):
            % splitted needs to have 5, 6, 7 or 12 elements
            % splitted{1} should not match a reserved word
            % splitted{1} has a maximum length of 4 --> removed this criterion to make import more robust <BS>
            % splitted{1} is not a member of the reserved words list ResFileReserved
            % the first char of splitted{1} has to be a letter, not a number
            if (numel(splitted) == 5 || numel(splitted) == 6 || numel(splitted) == 7 || numel(splitted) == 12) && ~ismember(splitted{1}, ResFileReserved) && isnan(str2double(splitted{1}(1)))
                % convert the string representation to numbers
                Res = cellfun(@str2double, splitted(2:end), 'UniformOutput', false);
                % first atom for the asymmetric unit, we have to
                % initialize the field
                if isempty(obj.AsymmetricUnit)
                    obj.AsymmetricUnit = GIDVis_Crystal_Atom;
                else
                    % add atom to the asymmetric unit
                    obj.AsymmetricUnit(AtomCounter, 1) = GIDVis_Crystal_Atom;
                end
                % check if all numbers are converted correctly
                if ~any(cellfun(@isnan, Res))
                    % set name of atom
                    obj.AsymmetricUnit(AtomCounter, 1).Name = splitted{1};
                    if isempty(obj.SFAC)
                        % in case no SFAC information was read, try
                        % to extract chemical element from name
                        [s1, s2] = regexp(obj.AsymmetricUnit(AtomCounter, 1).Name, '[A-Z][A-Z]?[a-z]?(\d[+-]+)?');
                        obj.AsymmetricUnit(AtomCounter, 1).Element = obj.AsymmetricUnit(AtomCounter, 1).Name(s1:s2);
                    else
                        % use chemical element from SFAC list
                        obj.AsymmetricUnit(AtomCounter, 1).Element = obj.SFAC{Res{1}};
                    end
                    % assign properties
                    obj.AsymmetricUnit(AtomCounter, 1).x = Res{2};
                    obj.AsymmetricUnit(AtomCounter, 1).y = Res{3};
                    obj.AsymmetricUnit(AtomCounter, 1).z = Res{4};
                    % as the accuracy we use the number of digits
                    % after the decimal place, i.e. for 0.1234 it
                    % will be 4.

                    % Mercury unfortunately exports a coordinate
                    % such as 0.6667 to 0.666700, although the
                    % accuracy should still be 4. A similar problem
                    % occurs with 1/3, i.e. 0.3333. So for these
                    % special cases (a decimal point followed by 1
                    % or more 6 followed by a zero or one 7 or a
                    % decimal point followed by one or more 3),
                    % remove the trailing zeros, in all other cases
                    % not.
                    Px = splitted{3};
                    while ~isempty(regexp(Px, '(\.6+7?|\.3+)0+$', 'once'))
                        Px = Px(1:end-1);
                    end
                    % "approximate" the position of the atom by the number
                    % of digits
                    obj.AsymmetricUnit(AtomCounter, 1).xAccuracy = 10^(-(numel(Px) - strfind(Px, '.')));

                    Py = splitted{4};
                    while ~isempty(regexp(Py, '(\.6+7?|\.3+)0+$', 'once'))
                        Py = Py(1:end-1);
                    end
                    % "approximate" the position of the atom by the number
                    % of digits
                    obj.AsymmetricUnit(AtomCounter, 1).yAccuracy = 10^(-(numel(Py) - strfind(Py, '.')));


                    Pz = splitted{5};
                    while ~isempty(regexp(Pz, '(\.6+7?|\.3+)0+$', 'once'))
                        Pz = Pz(1:end-1);
                    end
                    % "approximate" the position of the atom by the number
                    % of digits
                    obj.AsymmetricUnit(AtomCounter, 1).zAccuracy = 10^(-(numel(Pz) - strfind(Pz, '.')));

                    % get the site occupancy factor
                    if numel(Res) >= 5
                        obj.AsymmetricUnit(AtomCounter, 1).SOF = Res{5};
                    else
                        obj.AsymmetricUnit(AtomCounter, 1).SOF = 1;
                    end

                    % get the atomic displacement parameter
                    if numel(Res) >= 6
                        obj.AsymmetricUnit(AtomCounter, 1).U = [Res{6:end}];
                    else
                        obj.AsymmetricUnit(AtomCounter, 1).U = 0;
                    end
                    AtomCounter = AtomCounter + 1;
                end
            end
        end
        tline = fgetl(fid);
        % lines ending with a = are splitted over multiple lines,
        % if the line ends with '=', read next line as well and
        % concat them
        while ~isempty(strfind(tline, '=')) %#ok BS. Backwards compatibility
            tline = strrep(tline, '=', '');
            tline = [tline, fgetl(fid)]; %#ok
        end
    end
    % close file
    fclose(fid);

    % *.res files with MOVE command are not supported.
    if MovedAtoms
        msgbox('The file you''re trying to load uses the ''MOVE'' command, which is not implemented in GIDVis. Aborting reading in the atomic positions! Please contact the creator of this module.', 'modal');
        obj.AsymmetricUnit = [];
    end

    % *.res files with OMIT command are not supported.
    if OmittedAtoms
        msgbox('The file you''re trying to load uses the ''OMIT'' command, which is not implemented in GIDVis. Aborting reading in the atomic positions! Please contact the creator of this module.', 'modal');
        obj.AsymmetricUnit = [];
    end

    if ~isempty(obj.AsymmetricUnit)
        % construct unit cell from asymmetric unit
        [obj.UnitCell, obj.Spacegroup, Errors] = CreateUnitCell(obj);
        % in case no unit cell could be created, obj.UnitCell is empty
        if isempty(obj.UnitCell)
            obj.AsymmetricUnit = [];
            obj.UnitCellConsistencyMessage(false, Errors);
        end
    end
end