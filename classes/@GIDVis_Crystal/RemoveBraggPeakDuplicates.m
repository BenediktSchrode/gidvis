function HKL = RemoveBraggPeakDuplicates(obj, HKL, Orientation, Tolerance)
    % Makes a list of hkl values unique according to Bragg peak
    % position.           
    % 
    %
    % Usage:
    %     HKL = RemoveBraggPeakDuplicates(obj, HKL, Orientation, Tolerance)
    %
    % Input:
    %     - obj ... GIDVis_Crystal
    %     - HKL ... matrix of hkl values (n x 3)
    %     - Orientation ... Orientation of the crystal.
    %     - Tolerance ... tolerance for determination of
    %                     uniqueness. Default 1e-8.
    %           
    % If Orientation is empty, the uniqueness determination follows
    % the powder method, otherwise the RSM method: For both, the
    % expected Bragg peak positions in terms of qx, qy and qz are
    % calculated.
    %
    % In case of method 'Powder', the magnitude of the scattering
    % vector is then calculated: qmag = sqrt(qx^2+qy+^2+qz^2). If
    % two of these values fullfil the following condition, they are
    % considered to be equal: abs(qmag_1 - qmag_2) < Tolerance.
    %
    % In case of the method 'RSM', the in-plane and out-of-plane
    % component of the scattering vector is calculated: qxy =
    % sqrt(qx^2+qy^2), qz = qz. If two of these values fulfill the
    % following condition, they are considered to be equal:
    % abs(qxy_1 - qxy_2) < Tolerance && abs(qz_1 - qz_2) <
    % Tolerance, where && is the conditional AND.
    %
    %
    % Output:
    %     HKL ... n x 3 matrix of hkl values describing unique Bragg peak
    %             positions
    %
    %
    % This file is part of GIDVis.
    %
    % see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if nargin < 3; Orientation = []; end
    if nargin < 4 || isempty(Tolerance); Tolerance = 1e-8; end

    if isempty(Orientation)
        qvec = PeakPositionsQ(obj, HKL, [1 0 0]);
        % calculate the length of the scattering vector
        q = sqrt(sum(qvec.^2, 2));
        % concatenate the length of the scattering vector and
        % the HKL matrix
        M = [q HKL];
        % To remove duplicates, the idea is to sort the M
        % matrix and then check for each element if the
        % previous element is the same.
        % For the same q value, the HKL with the higher h
        % should survive, e.g. for 100 and 010 the 010 should
        % be removed. So sort the rows descending, first by
        % column 1 (i.e. q), then by column 2 (i.e. h), then by
        % column 3( i.e. k), then by column 4 (i.e. l).
        [~, idx] = sortrows(M, 'descend');
        % sort the corresponding q and hkl list
        q = q(idx);
        HKL = HKL(idx, :);
        nq = numel(q);
        % loop over all entries of the q vector, starting from
        % the back (looping from back is required to allow
        % removal of the elements and to remove the elements
        % with the low l and k values first).
        for iZ = nq:-1:2
            % if the difference between the current element and
            % the element above is smaller than the tolerance,
            % remove the current element from q and HKL
            if abs(q(iZ)-q(iZ-1)) < Tolerance
                q(iZ) = [];
                HKL(iZ, :) = [];
            end
        end
    else
        % calculate Bragg peak positions in reciprocal space for
        % requested orientation
        qvec = PeakPositionsQ(obj, HKL, Orientation);
        % calculate in- and out-of-plane component of the
        % scattering vector
        qxy = sqrt(sum(qvec(:, 1:2).^2, 2));
        qz = qvec(:, 3);
        % sort the HKL values descending
        [HKL, idx] = sortrows(HKL, 'descend');
        % also sort qxy and qz
        qxy = qxy(idx);
        qz = qz(idx);
        % the idea is to loop over all elements, then compare
        % each other element to this element and check for
        % uniqueness. In case of the same qxy and qz value, set
        % the ToDelete to true. Start the second loop from the
        % back to remove the element with the lower h and lower
        % k values first.
        ToDelete = false(size(qxy));
        for iOne = 1:numel(qxy)
            for iTwo = numel(qxy):-1:iOne
                if iOne == iTwo; continue; end
                if ToDelete(iTwo); continue; end
                if abs(qxy(iOne)-qxy(iTwo)) < Tolerance && ...
                    abs(qz(iOne)-qz(iTwo)) < Tolerance
                    ToDelete(iTwo) = true;
                end
            end
        end
        % remove the elements from HKL which have to be deleted
        HKL(ToDelete, :) = [];
   end
end