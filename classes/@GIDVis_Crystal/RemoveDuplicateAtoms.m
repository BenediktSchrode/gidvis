function NewAtoms = RemoveDuplicateAtoms(NewAtoms)
    % Removes duplicate atoms.
    %
    % This function can be used to remove duplicate atoms from a vector of
    % GIDVis_Crystal_Atom. Duplicate in this context means that an atom is
    % on the same xyz position and is the same chemical element. This
    % function is for example performed after applying all symmetry
    % operations to the atoms of the asymmetric unit which might result in
    % multiple atoms at the same position.
    %
    % Usage:
    %     NewAtoms = RemoveDuplicateAtoms(NewAtoms);
    % 
    % Input:
    %     NewAtoms ... Vector of GIDVis_Crystal_Atom from which duplicates
    %                  should be removed
    %
    % Output:
    %     NewAtoms ... Vector of GIDVis_Crystal_Atom without duplicates
    %
    %
    % This file is part of GIDVis.
    %
    % see also: GIDVis_Crystal_Atom, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % prepare for checking identity of atom positions           
    % initialize vector holding trues where an identical atom was
    % found
    Identical = false(size(NewAtoms));

    % compare each atom position with all others
    for iP = 1:numel(NewAtoms)
        if Identical(iP)
            continue;
        end
        for jP = 1:numel(NewAtoms)
            if iP == jP || Identical(jP)
                continue;
            end
            % check identity of chemical element            
            % if they are not identical, the atoms are not considered to be
            % duplicate no matter the position
            if isa(NewAtoms(iP).Element, 'ChEl')
                if ~strcmp(NewAtoms(iP).Element.Name, NewAtoms(jP).Element.Name)
                    continue;
                end
            else
                if ~strcmp(NewAtoms(iP).Element, NewAtoms(jP).Element)
                    continue;
                end
            end
            
            % get the positions of the ith and jth atom
            PosiP = [NewAtoms(iP).x NewAtoms(iP).y NewAtoms(iP).z];
            PosjP = [NewAtoms(jP).x NewAtoms(jP).y NewAtoms(jP).z];

            % get the accuracies of the ith and jth atom
            AcciP = [NewAtoms(iP).xAccuracy; NewAtoms(iP).yAccuracy; NewAtoms(iP).zAccuracy];
            AccjP = [NewAtoms(jP).xAccuracy; NewAtoms(jP).yAccuracy; NewAtoms(jP).zAccuracy];

            % if the atom position is lying at fractional
            % coordinate 1 considering the accuracy,
            % subtract 1.
            PosiP(1-PosiP(:) < AcciP(:)) = PosiP(1-PosiP(:) < AcciP(:)) - 1;
            PosjP(1-PosjP(:) < AccjP(:)) = PosjP(1-PosjP(:) < AccjP(:)) - 1;

            % calculate the difference between the
            % positions
            Delta = PosiP - PosjP;
            % the tolerance for comparison has to be the
            % maximum of the accuracy of the contributing
            % positions
            MyTol = max([[NewAtoms([iP jP]).xAccuracy]; [NewAtoms([iP jP]).yAccuracy]; [NewAtoms([iP jP]).zAccuracy]], [], 2);
            % check for identical positions
            L = abs(Delta) - MyTol' <= 1e-12;
            % set the Identical value of the atom to true
            if all(L)
                Identical(jP) = true;
            end
        end
    end

    % remove identical atoms
    NewAtoms = NewAtoms(~Identical);
end