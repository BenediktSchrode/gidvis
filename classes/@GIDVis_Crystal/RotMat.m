function R = RotMat(f, t)
    % Calculates the rotation matrix R so that the vector f is being
    % rotated onto the vector t.
    %
    % Usage:
    %     R = RotMat(f, t);
    %
    % Input:
    %     f ... Vector f.
    %     t ... Vector t.
    %
    % Output:
    %     R ... Rotation matrix R to rotate vector f onto vector t.
    %
    %
    % Calculation of the rotation matrix is performed following the
    % algorithm by Moeller and Hughes:   
    % Tomas Moeller & John F. Hughes (1999): Efficiently Building a Matrix
    % to Rotate One Vector to Another, Journal of Graphics Tools, 4:4, 1-4
    %
    %
    % This file is part of GIDVis.
    %
    % see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2019 Benedikt Schrode                                     %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
    % normalize f and t
    f = f./sqrt(sum(f.^2));
    t = t./sqrt(sum(t.^2));
    f = f(:);
    t = t(:);
    
    % Below is the implementation of calculating the rotation matrix
    % following Moeller and Hughes
    c = dot(f, t);
    
    if abs(c) <= 0.99 % the vectors are not parallel
        v = cross(f, t);
        h = (1-c)/(1-c^2);
        R = [c + h*v(1)^2 h*v(1)*v(2)-v(3) h*v(1)*v(3)+v(2); ...
            h*v(1)*v(2)+v(3) c+h*v(2)^2 h*v(2)*v(3)-v(1); ...
            h*v(1)*v(3)-v(2) h*v(2)*v(3)+v(1) c+h*v(3)^2];
    else % special case of vectors being nearly parallel
        % Different than the original source, I've added a <= in every row
        % so that every component with every other component has the <=
        % once
        if abs(f(1)) <= abs(f(2)) && abs(f(1)) < abs(f(3))
            p = [1 0 0]';
        elseif abs(f(2)) < abs(f(1)) && abs(f(2)) <= abs(f(3))
            p = [0 1 0]';
        elseif abs(f(3)) <= abs(f(1)) && abs(f(3)) < abs(f(2))
            p = [0 0 1]';
        end
        u = p-f;
        v = p-t;
        duu = dot(u, u);
        dvv = dot(v, v);
        duv = dot(u, v);
        
        T1 = 2/duu;
		T2 = 2/dvv;
		T3 = 4*duv/(duu*dvv);
		R = [1 - T1*u(1)*u(1) - T2*v(1)*v(1) + T3*v(1)*u(1), ...
               - T1*u(1)*u(2) - T2*v(1)*v(2) + T3*v(1)*u(2), ...
               - T1*u(1)*u(3) - T2*v(1)*v(3) + T3*v(1)*u(3); ...
               - T1*u(2)*u(1) - T2*v(2)*v(1) + T3*v(2)*u(1), ...
             1 - T1*u(2)*u(2) - T2*v(2)*v(2) + T3*v(2)*u(2), ...
               - T1*u(2)*u(3) - T2*v(2)*v(3) + T3*v(2)*u(3); ...
               - T1*u(3)*u(1) - T2*v(3)*v(1) + T3*v(3)*u(1), ...
               - T1*u(3)*u(2) - T2*v(3)*v(2) + T3*v(3)*u(2), ...
             1 - T1*u(3)*u(3) - T2*v(3)*v(3) + T3*v(3)*u(3)];
    end
end