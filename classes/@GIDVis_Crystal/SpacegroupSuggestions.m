function SpgrArr = SpacegroupSuggestions(Cr)
    % SpacegroupSuggestions finds the possible spacegroups by
    % comparison of the symmetry elements Cr.Symm to the known
    % symmetry elements of each spacegroup.
    %
    % Usage:
    %     SpgrArr = SpacegroupSuggestions(Cr)
    %
    % Input:
    %     Cr ... GIDVis_Crystal object
    %
    % Output:
    %     SpgrArr ... vector of Spacegroup elements
    %
    %
    % This file is part of GIDVis.
    %
    % see also: Spacegroup, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % load spacegroups
    load Spacegroups.mat Spgrs;

    % get symmetry elements
    TempSymm = Cr.Symm;
    % if identity operation is not given, add it
    if ~any(strcmp(TempSymm, 'x,y,z'))
        TempSymm{end+1} = 'x,y,z';
    end

    % transform the string representation of the symmetry
    % operations to matrix form
    CMat = cell(size(TempSymm));
    for iS = 1:numel(CMat)
        CMat{iS} = CreateSymmetryMatrix(TempSymm{iS});
    end

    % when crystal is centro symmetric, the additional symmetry
    % operations are not given explicitly, calculate them from the
    % already obtained symmetry matrices
    if Cr.CentroSymmetric
        CMatNew = repmat(CMat, 2, 1);
        for iC = numel(CMatNew)/2+1:numel(CMatNew)
            M = CMatNew{iC};
            M(1:3, 1:3) = -1*M(1:3, 1:3);
            L = M(1:3, 4) > 0;
            M(L, 4) = 1-M(L, 4);
            CMatNew{iC} = M;
        end
        CMat = CMatNew;
    end

    Candidates = false(size(Spgrs));
    % loop over all possible space groups
    for iSpgr = 1:numel(Spgrs)
        Spgr = Spgrs{iSpgr};
        SMat = Spgr.WyckoffPositions{1}.Symmetries;
        if isempty(Spgr.Coordinates); Spgr.Coordinates = zeros(1, 3); end

        % check symmetry matrices for equality
        if numel(CMat) == numel(SMat)
            L = NaN(size(CMat));
            for iC = 1:size(Spgr.Coordinates, 1)
                for iCMat = 1:numel(CMat)
                    M = CMat{iCMat};
                    M(1:3, 4) = M(1:3, 4) + Spgr.Coordinates(iC, :)';
                    while M(1, 4) >= 1; M(1, 4) = M(1, 4)-1; end
                    while M(2, 4) >= 1; M(2, 4) = M(2, 4)-1; end
                    while M(3, 4) >= 1; M(3, 4) = M(3, 4)-1; end
                    for iSMat = 1:numel(SMat)
                        M2 = SMat{iSMat};
                        if isequal(M(1:3, 1:3), M2(1:3, 1:3)) && isequal(M(1:3, 4), M2(1:3, 4))
                            L(iCMat) = 1;
                            break;
                        end
                    end
                end
            end
            if all(L == 1)
                Candidates(iSpgr) = true;
            end
        end
    end

    SpgrArr = [Spgrs{Candidates}];
end