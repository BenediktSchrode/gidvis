function [a, b, gamma, hkl1, hkl2, avec, bvec] = SurfaceUC(obj, hklPlane, hklminmax)
    % Tries to find a surface unit cell of the plane given by hklPlane.
    % Since there are several possible surface unit cells, the following
    % rules are applied to select the "correct" one:
    %     - minimum length a1
    %     - minimum length a2
    %     - angle gamma as close to 90 degree as possible
    %     - prefer angles above 90 degree to angles below 90 degree
    %     - the more positive hkl values, the better
    % During the algorithm, all possible hkl combinations (in a certain
    % range) are tested, wheter they can be used to describe a surface unit
    % cell lying in the plane given by the hkl values in hklPlane.
    %
    %
    % Usage:
    %     [a, b, gamma, hkl1, hkl2, avec, bvec] = SurfaceUC(obj, hklPlane)
    %     [a, b, gamma, hkl1, hkl2, avec, bvec] = SurfaceUC(obj, hklPlane, hklminmax)
    %
    % Input:
    %     obj ... GIDVis_Crystal object
    %     hklPlane ... hkl indices of the plane in which the surface unit
    %                  cell should lie
    %     hklminmax ... vector of hkl indices which should be checked. To
    %                   check all hkl indices between -5 and 7, use -5:7.
    %                   Defaulted to -max(abs(hklPlane)):max(abs(hklPlane))
    %                   if not given.
    %
    % Output:
    %     a ... length 1 of the surface unit cell
    %     b ... length 2 of the surface unit cell
    %     gamma ... angle between a1 and a2
    %     hkl1 ... hkl indices of the vector describing a1
    %     hkl2 ... hkl indices of the vector describing a2
    %     avec ... a as vector
    %     bvec ... b as vector
    % If no surface unit cell was found, the output arguments are returned
    % empty. In this case, you can try to increase the range hklminmax.
    %
    %
    % This file is part of GIDVis.
    %
    % see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % assign default value to hklminmax if not given
    if nargin < 3 || isempty(hklminmax)
        hklminmax = -max(abs(hklPlane)):max(abs(hklPlane));
    end

    % create the hkl combinations
    hkl = obj.CreateHKLCombinations(hklminmax);

    % find directions which are lying in the hklPlane: if h1*h2+k1*k2+l1*l2
    % = 0 is fulfilled, the direction h1, k1, l1 lies in the plane h2, k2,
    % l2.
    L = hklPlane(:)'*hkl' == 0;
    % only keep hkl values where the corresponding direction is in the
    % plane
    hkl = hkl(L, :);

    % calculate the directions
    Dirs = obj.uvwDirection(hkl);
    % calculate the length of the directions
    L = sqrt(sum(Dirs.^2, 2));

    % "initialize"
    SUC = NaN(0, 11);

    % try every hkl with every other hkl
    for iD1 = 1:size(Dirs, 1)
        for iD2 = 1:size(Dirs, 1)
            % do not use the same hkl values
            if iD1 == iD2; continue; end
            D1 = Dirs(iD1, :);
            D2 = Dirs(iD2, :);
            
            % check if rules are fulfilled
            if dot(D1, D1) > dot(D2, D2); continue; end
            if (abs(dot(D1, D2)) - dot(D1, D1)/2) > 1e-8; continue; end
            if dot(D1, D2) > 0; continue; end
            
            % calculate the normalized direction vectors
            N1 = Dirs(iD1, :)./L(iD1);
            N2 = Dirs(iD2, :)./L(iD2);

            % check for parallel and anti-parallel vectors
            if all(abs(N1 - N2) < 1e-12); continue; end
            if all(abs(N1 + N2) < 1e-12); continue; end

            % add the current values to the SUC result variable
            SUC(end+1, 1:3) = hkl(iD1, :); % h1 k1 l1
            SUC(end, 4:6) = hkl(iD2, :); % h2 k2 l2
            SUC(end, 7) = L(iD1); % length 1
            SUC(end, 8) = L(iD2); % length 2
            SUC(end, 9) = sum(SUC(end, 1:6) < 0); % number of h, k, l indices smaller than 0
        end
    end

    % to find the "correct" SUC, sort the results in ascending order by
    % length 1, length 2 and the number of h, k, l indices smaller than 0
    % (this prefers hkl indices > 0, e.g. [010] & [2 0 -1] is preferred
    % over [0 -1 0] & [-2 0 1]).
    SUC = sortrows(SUC, [7 8 9]);
    if isempty(SUC)
        a = [];
        b = [];
        gamma = [];
        hkl1 = [];
        hkl2 = [];
        avec = [];
        bvec = [];
    else
        hkl1 = SUC(1, 1:3);
        hkl2 = SUC(1, 4:6);
        avec = obj.uvwDirection(hkl1);
        bvec = obj.uvwDirection(hkl2);
        a = sqrt(sum(avec.^2));
        b = sqrt(sum(bvec.^2));
        gamma = acosd(dot(avec, bvec)/(a*b));
    end
end