function UnitCellConsistencyMessage(~, Passed, Errors)
    % Returns the message telling the user that the import failed.
    %
    % Usage:
    %     UnitCellConsistencyMessage(obj, Passed)
    %
    % Input:
    %     obj ... GIDVis_Crystal object (not used)
    %     Passed ... Boolean whether test passed or not
    %
    % This file is part of GIDVis.
    %
    % see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if ~Passed
        if nargin < 2 || isempty(Errors) 
            Errors = {''};
        end
        msg = 'An error occured during the import of your res file.\n\nIf this error persists, please file a bug report. If possible, include the res file.\n\n';
        msg2 = sprintf('%s\n', Errors{:});
        msgbox(sprintf([msg msg2]), 'modal');
    end
end