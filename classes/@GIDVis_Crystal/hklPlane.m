function [Vertices, Faces] = hklPlane(obj, hkl)
    % Returns the Vertices and Faces of the plane given by hkl.
    %
    % Usage:
    %     [V, F] = hklPlane(obj, hkl);
    % 
    % Input:
    %     obj ... GIDVis_Crystal object
    %     hkl ... hkl indices of the plane as a vector with three elements
    %
    % Output:
    %     V ... Vertices of the plane
    %     F ... Faces of the plane
    %
    % The output can be easily plotted using
    %     p = patch('Vertices', V, 'Faces', F);
    %
    %
    % This file is part of GIDVis.
    %
    % see also: patch, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % check if plane is [0 0 0]
    if all(hkl == 0)
        error('GIDVis:GIDVis_Crystal:ZeroPeak', 'hkl 000 not valid.');
    end
    
    % check for three-element hkl vector
    if numel(hkl) ~= 3
        error('GIDVis:GIDVis_Crystal:DimensionMismatch', ...
            'hkl has to be a three-element vector.');
    end
    
    % get the axes intersections
    Ints = 1./hkl;

    % create the vertice points of the plane using the axes
    % intersections and the unit cell vectors
    Vertices = [Ints(1)*obj.a_vec Ints(2)*obj.b_vec Ints(3)*obj.c_vec];
    % define the faces
    Faces = [1 2 3];
    % for Miller indices 0, the intersections become infinite
    L = isinf(Ints);
    % matrix containing the unit cell vectors
    M = [obj.a_vec obj.b_vec obj.c_vec];

    % special treatment for one or two Miller indices being 0
    if sum(L) == 1 % one Miller index is 0 (requested plane parallel to one axis)
        % remove vertices of the Miller index 0
        Vertices = Vertices(:, ~L);
        % create two new vertices points by simply adding the unit
        % cell vector which the plane is parallel to to the already
        % existing vertices
        Vertices(:, 3) = Vertices(:, 1) + M(:, L);
        Vertices(:, 4) = Vertices(:, 2) + M(:, L);
        % define faces
        Faces = [1 2 4 3];
    elseif sum(L) == 2
        % remove vertices of the Miller index 0
        Vertices = Vertices(:, ~L);
        % take only the unit cell vectors where the Miller index is
        % 0
        M = M(:, L);
        % Create three new vertices: one by adding the first, one
        % by adding the second, one by adding the sum of the first
        % and second unit cell vector to the already existing
        % vertex
        Vertices(:, 2) = Vertices(:, 1) + M(:, 1);
        Vertices(:, 3) = Vertices(:, 1) + sum(M, 2);
        Vertices(:, 4) = Vertices(:, 1) + M(:, 2);
        % define faces
        Faces = [1 2 3 4];    
    end

    % in order to easily plot the plane using the patch command,
    % transpose the vertices
    Vertices = Vertices';
end