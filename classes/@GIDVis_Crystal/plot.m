function fOut = plot(obj)
    % Function to plot a crystal.
    %
    % Usage:
    %     plot(obj)
    %
    % Input:
    %     obj ... GIDVis_Crystal object
    %
    % Output:
    %     fOut ... handle of the figure window
    %
    %
    % Use plotUnitCell, plotAtoms, plotInPlaneAtoms, plotPlane for more
    % control over the plotting process and properties.
    %
    %
    % This file is part of GIDVis.
    %
    % see also: GIDVisLicense, plotUnitCell, plotAtoms, plotInPlaneAtoms, 
    % plotPlane

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2020 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    f = figure('Color', 'w');
    ax = axes('Parent', f);
    plotUnitCell(obj, 'Parent', ax);
    plotAtoms(obj, obj.UnitCell, 'Parent', ax);
    axis(ax, 'equal');
    axis(ax, 'off');
    rotate3d(ax, 'on');
    uimenu(f, 'Label', 'Color Code', 'Callback', ...
        @(src, evt) ColorCode());
    
    if nargout > 0
        fOut = f;
    end
end