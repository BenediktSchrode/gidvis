function hg = plotAtoms(obj, Atoms, varargin)
    % Plots the atoms.
    %
    % Usage:
    %     hg = plotAtoms(obj, Atoms, varargin)
    %
    % Input:
    %     obj ... GIDVis_Crystal object
    %     Atoms ... vector of GIDVis_Crystal_Atom
    %
    % List of name-value-pairs:
    %     - BondColor: Color of bond lines. Default black.
    %     - BondWidth: Line width of bond lines. Default 1.5.
    %     - BondLength: if the distance between two atoms is lower
    %       than this value, a bond is drawn. Set this value to 0
    %       to not plot any bonds at all. Default: 1.95.
    %     - AtomStyle: Describes how to plot atoms. '3D' plots
    %       atoms as spheres with the radius being the atomic
    %       radius and the color depending on the element. '2D' 
    %       plots the atoms as circles with the color depending on
    %       the element. 'off' does not plot any atoms. Default
    %       '3D'.
    %     - Parent: parent axis of the plot. Default gca.
    %     - Translation: three element vector describing translation of the 
    %       atoms. Default [0 0 0].
    %     - AtomColor: Color of the atoms. Either a three-element vector of
    %       rgb values (between 0 and 1), an n x 3 or 3 x n matrix of rgb
    %       values where n is the number of atoms, the string 'element', or
    %       a function handle @(x, y, z) or @(Crystal, Atoms). In case of
    %       'element', a color scheme mostly following CPK is used. If a
    %       function handle @(x, y, z) is given the color is calculated
    %       according to the result of the function handle with the
    %       cartesian coordinates of the atom position used as input. For
    %       example, use @(x, y, z) z as function handle to make the color
    %       depending on the z coordinate of the atom. If a function handle
    %       @(Crystal, Atom) is given the color is calculated according to
    %       the result of the function handle with the Crystal and the list
    %       of Atoms as input. For example, use @(Cr, A) Beq(A, Cr) to make
    %       the color depending on the equivalent Debye-Waller factor.
    %       Default 'element'.
    %     - Colormap: If AtomColor is a function handle, the colormap is
    %       used to map the atom color. Specify the colormap as a function
    %       handle, e.g. @bone to use MATLAB's bone colormap. Default:
    %       @parula.
    %
    %
    % Output:
    %     hggroup of all elements plotted.
    %
    %
    % This file is part of GIDVis.
    %
    % see also: ChEl, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2020 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    if isempty(Atoms)
        if nargout > 0
            hg = [];
        end
        return;
    end

    % create a validation function
    ParentValidation = @(x) assert(strcmp(get(x, 'Type'), 'axes'), ...
        'It has to be of type ''axes''.');

    p = inputParser;
    addRequired(p, 'obj');
    addRequired(p, 'Atoms');
    addParameter(p, 'BondColor', 'k');
    addParameter(p, 'BondWidth', 1.5);
    addParameter(p, 'BondLength', 1.95);
    addParameter(p, 'AtomStyle', '3D'); % 3D, 2D or off (case-insensitive)
    addParameter(p, 'Parent', gca, ParentValidation);
    addParameter(p, 'Translation', [0 0 0]);
    addParameter(p, 'AtomColor', 'element');
    addParameter(p, 'Colormap', @parula);
    parse(p, obj, Atoms, varargin{:});

    % extract atom positions (they are in fractional coordinates)
    Pos = [[Atoms.x]' [Atoms.y]' [Atoms.z]'];
    % transform fractional to cartesian coordinates
    Cart = CalculateCartesianCoordinates(obj, Pos);
    % apply translations
    Cart(:, 1) = Cart(:, 1) + p.Results.Translation(1);
    Cart(:, 2) = Cart(:, 2) + p.Results.Translation(2);
    Cart(:, 3) = Cart(:, 3) + p.Results.Translation(3);
    % get elements
    Elements = [Atoms.Element];
    if isa(p.Results.AtomColor, 'char') && ...
            strcmpi(p.Results.AtomColor, 'element')
        % get colors depending on element
        Colors = vertcat(Elements.Color);
    elseif isa(p.Results.AtomColor, 'function_handle')
        % get colors depending on function handle
        if nargin(p.Results.AtomColor) == 3
            % use cartesian coordinates as input
            H = p.Results.AtomColor(Cart(:, 1), Cart(:, 2), Cart(:, 3));
        elseif nargin(p.Results.AtomColor) == 2
            % use GIDVis_Crystal and the atoms as input
            H = p.Results.AtomColor(obj, Atoms);
        else
            error('GIDVis_Crystal:plotAtoms:IncorrectInput', ...
                'The function handle for ''AtomColor'' should allow two or three inputs.');
        end
        % assign colors to the atoms according to the value of H
        minz = min(H);
        maxz = max(H);
        CM = p.Results.Colormap(256);
        Cx = linspace(minz, maxz, 256);
        
        [Z, CX] = meshgrid(H, Cx);        
        [~, ind] = min(abs(Z - CX));        
        Colors = CM(ind, :);
        
        % set the color limits of the axis if the lower and upper limits
        % are not the same
        if minz ~= maxz
            caxis(ancestor(p.Results.Parent, 'axes'), [minz maxz]);
        end
    elseif isnumeric(p.Results.AtomColor) || ...
            (ischar(p.Results.AtomColor) && ~isempty(str2col(p.Results.AtomColor))) || ...
            (iscell(p.Results.AtomColor) && ~isempty(str2col(p.Results.AtomColor)))
        if isnumeric(p.Results.AtomColor)
            Colors = p.Results.AtomColor;
        else
            Colors = str2col(p.Results.AtomColor);
        end
        if numel(Colors) ~= 3 % several colors are given for the atoms
            % transpose if necessary
            if size(Colors, 2) ~= 3
                Colors = Colors.';
            end
        elseif numel(Colors) == 3 && strcmpi(p.Results.AtomStyle, '3d')
            % create a matrix containing the same color for each atom
            Colors = repmat(Colors(:), 1, numel(Atoms))';
        end
    else
        error('GIDVis_Crystal:plotAtoms:InvalidAtomColor', ...
            'One or multiple of the AtomColor provided is not valid.');
    end
    
    if size(Colors, 1) ~= numel(Atoms)
        error('GIDVis_Crystal:plotAtoms:IncorrectColorNumber', ...
            ['The number of colors does not match the number of atoms. ', ...
            'Please give a single color or as many colors as there are atoms.'])
    end
    
    % get the atomic radii
    Radii = [Elements.Radius]./100;

    % create hggroup
    hgInt = hggroup('Parent', p.Results.Parent);
    % store the old view
    [OldViewAz, OldViewEl] = view(p.Results.Parent);

    % if bonds have to be plotted
    if p.Results.BondLength
        % calculate distance between all the atoms
        D = squareform(pdist(Cart));
        % find atoms which are closer to each other than BondLength
        L = D < p.Results.BondLength;
        % set the diagonal of L to zero since this corresponds to the
        % distance between an atom and the same atom
        L(1:numel(Atoms)+1:end) = 0;
        % get upper triangular matrix (in the lower triangular matrix there
        % are the opposite directions of the bonds, we don't need them)
        TriUL = triu(L);
        NumBonds = sum(TriUL(:));
        % initialize vectors for the x, y and z components of the bond to
        % plot and the tag list.
        PBx = NaN(2, NumBonds);
        PBy = PBx;
        PBz = PBx;
        TagList = cell(NumBonds, 1);

        % get the row and column indices of the upper triangular matrix.
        % They determine which atom has a bond to which other atom.
        [RowInd, ColInd] = find(TriUL);
        % loop over the atoms having a bond
        for iN = 1:numel(RowInd)
            % set the x, y and z coordinate of the first atom
            PBx(1, iN) = Cart(RowInd(iN), 1);
            PBy(1, iN) = Cart(RowInd(iN), 2);
            PBz(1, iN) = Cart(RowInd(iN), 3);
            % set the x, y and z coordinate of the second atom
            PBx(2, iN) = Cart(ColInd(iN), 1);
            PBy(2, iN) = Cart(ColInd(iN), 2);
            PBz(2, iN) = Cart(ColInd(iN), 3);
            % create a tag showing which atom is bonded to which
            TagList{iN} = [Atoms(RowInd(iN)).Name ' -- ' Atoms(ColInd(iN)).Name];
        end
        
        % plot the bonds
        lhBonds = plot3(PBx, PBy, PBz, 'Color', p.Results.BondColor, ...
            'LineWidth', p.Results.BondWidth, 'Parent', hgInt);
        % set the tag of all the lines simultaneously
        set(lhBonds, {'Tag'}, TagList(:));
    end

    if strcmpi(p.Results.AtomStyle, '3d')        
        % loop over all atoms
        for iE = 1:numel(Atoms)
            % plot the atom spheres with the constant values which are
            % available in GIDVis_Crystal and describe a sphere.
            % FaceLighting is required to get 3d-effect later on in
            % combination with a light source.
            % PickableParts is none so that the datacursor tool cannot be
            % used on the outer surface of the sphere
            patch('Faces', obj.sf, 'Vertices', [obj.sx.*Radii(iE) + Cart(iE, 1) ...
                obj.sy.*Radii(iE) + Cart(iE, 2) obj.sz.*Radii(iE) + Cart(iE, 3)], ...
                'Parent', hgInt, 'Tag', Atoms(iE).Name, 'FaceColor', ...
                Colors(iE, :), 'FaceLighting', 'gouraud', 'EdgeColor', 'none', ...
                'PickableParts', 'none');
        end
        % make the spheres shiny
        material(hgInt, 'shiny');
        % plot a marker at the center of every atom. This together with the
        % patch with PickableParts set to 'none' makes the data cursor show
        % the atom position correctly. Using marker 'o' to make the
        % clickable area a bit larger compared to marker '.'
        line('Parent', hgInt, 'XData', Cart(:, 1), 'YData', Cart(:, 2), ...
            'ZData', Cart(:, 3), 'LineStyle', 'none', 'Marker', 'o', ...
            'Color', 'w', 'Tag', 'AtomCenters');
        % reset the view
        view(OldViewAz, OldViewEl);
        % set the x axis, y axis and z axis scaling equal
        daspect([1 1 1])
        % find light sources in the parent axes
        LS = findall(ancestor(hgInt, 'axes'), 'Type', 'light');
        % Add a new light source only if there is none defined yet
        % in the parent axes. Using a light gives a 3d impression
        if isempty(LS); LS = camlight('right', 'infinite'); end
        % Add a listener to the View property of the axes. In the listener,
        % the camera position is updated so that the atoms are always
        % illuminated (also when looking at "the back" of them)
        addlistener(p.Results.Parent, 'View', 'PostSet', ...
            @(src, evt) CameraViewChanged_Callback(LS));
    elseif strcmpi(p.Results.AtomStyle, '2d')
        % plot atoms using scatter
        h = scatter3(Cart(:, 1), Cart(:, 2), Cart(:, 3), 'filled', ...
            'Parent', hgInt, 'Tag', 'Atoms');
        set(h, 'CData', Colors); % set the colors of the scatter points
        % restore view
        view(OldViewAz, OldViewEl);
    elseif strcmpi(p.Results.AtomStyle, 'off')
        % don't plot anything
    else
        error('GIDVis_Crystal:plotAtoms:UnknownAtomStyle', ...
            'Unknown atom style %s.', p.Results.AtomStyle);
    end
    
    if nargout > 0
        hg = hgInt;
    end
end

function CameraViewChanged_Callback(LS)
if isgraphics(LS)
    camlight(LS, 'right')
end
end

function Colors = str2col(Str)
% Converts a MATLAB color char representation into the corresponding rgb
% vector.
%
% Usage:
%     Colors = str2col(Str)
%
% Input:
%     Str ... char or cell of char
%
% Output:
%     Colors ... nx3 matrix of rgb values

C = {'red', 'r', 'green', 'g', ...
        'blue', 'b', 'yellow', 'y', 'magenta', 'm', 'cyan', 'c', ...
        'white', 'w', 'black', 'k'};

M = [1 0 0; 0 1 0; 0 0 1; 1 1 0; 1 0 1; 0 1 1; 1 1 1; 0 0 0];

[Found, ind] = ismember(Str, C);
if all(Found)
    Colors = M(ceil(ind/2), :);
else
    Colors = [];
end
end