function hg = plotDirection(obj, uvw, varargin)
    % Plots the atoms.
    %
    % Usage:
    %     plotDirection(obj, uvw)
    %     plotDirection(obj, uvw, Normalize)
    %     plotDirection(___, Name, Value) % Use this option with any of the
    %         input argument combination in the previous syntaxes
    %
    % Input:
    %     obj ... GIDVis_Crystal object
    %     uvw ... Direction to plot. The end point to draw is evaluated by
    %             u*obj.a_vec + v*obj.b_vec + w*obj.c_vec
    %     Normalize ... If 0, no normalization takes place. If larger than
    %                   0 the plotted direction is normalized to this
    %                   length. Default 0, i.e. no normalization.
    %     Name, Value ... All name-value pairs except 'Translation' and
    %                     'Label' are directly passed to the plot3 call
    %                     used to plot the directions, i.e. you can use all
    %                     name-value pairs available for plot3.
    %                     The name-value pair 'Translation' translates the
    %                     direction vector. Its value should be a three-
    %                     element vector. Default [0 0 0].
    %                     The name-value pair 'Label' determines if the
    %                     directions should be labelled. Its value should
    %                     be 'on' or 'off'. Default 'off'.    
    %
    % Output:
    %     handles of all elements plotted.
    %
    %
    % This file is part of GIDVis.
    %
    % see also: uvwDirection, plot3, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2020 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    p = inputParser;
    p.KeepUnmatched = true;
    addRequired(p, 'obj', @(obj) isscalar(obj) && isa(obj, 'GIDVis_Crystal'));
    addRequired(p, 'uvw', @(uvw) isreal(uvw) && any(size(uvw) == 3));
    addOptional(p, 'Normalize', 0, @(Normalize) isscalar(Normalize) && isreal(Normalize));
    addParameter(p, 'Translation', [0 0 0]);
    addParameter(p, 'Label', 'off')
    
    parse(p, obj, uvw, varargin{:});
    
    % get the names of the unmatched inputs and their values and combine
    % them in a cell
    Unmatched = [fieldnames(p.Unmatched) struct2cell(p.Unmatched)]';

    % create the matrix with the vectors describing the unit cell
    Dirs = uvwDirection(p.Results.obj, p.Results.uvw, p.Results.Normalize);
    % create matrices which are then plotted (each column one direction)
    X = [zeros(1, size(uvw, 1)); Dirs(:, 1)'];
    Y = [zeros(1, size(uvw, 1)); Dirs(:, 2)'];
    Z = [zeros(1, size(uvw, 1)); Dirs(:, 3)'];
    % apply the translation
    X = X + p.Results.Translation(1);
    Y = Y + p.Results.Translation(2);
    Z = Z + p.Results.Translation(3);
    
    % plot the whole thing
    hgInt = plot3(X, Y, Z, Unmatched{:});
    % create the tag
    Str = sprintf('[%g %g %g]\n', uvw');
    TagStr = strsplit(Str, '\n');
    % set the tag to the plotted directions
    set(hgInt, {'Tag'}, TagStr(1:end-1)');
    
    % plot labels if requested
    if strcmpi(p.Results.Label, 'on')
        th = text(X(end, :), Y(end, :), Z(end, :), TagStr(1:end-1));
        
        L = X(2, :) < X(1, :);
        HA = repmat({'left'}, size(X, 2), 1);
        HA(L) = {'right'};
        L = abs(X(2, :)) < 1e-8;
        HA(L) = {'center'};
        
        L = Y(2, :) < Y(1, :);
        VA = repmat({'bottom'}, size(Y, 2), 1);
        VA(L) = {'top'};
        L = abs(Y(2, :)) < 1e-8;
        VA(L) = {'middle'};
        
        set(th, {'Tag'}, TagStr(1:end-1)', {'HorizontalAlignment'}, HA, ...
            {'VerticalAlignment'}, VA)
        hgInt = [hgInt; th];
    end
    
    if nargout > 0
        hg = hgInt;
    end
end