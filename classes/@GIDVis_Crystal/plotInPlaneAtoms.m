function hg = plotInPlaneAtoms(obj, hkl, varargin)
    % Plots atoms lying in a crystallographic plane.
    %
    % Usage: 
    %     plotInPlaneAtoms(obj, hkl)
    %     plotInPlaneAtoms(obj, hkl, Delta)
    %     plotInPlaneAtoms(obj, hkl, Delta, alims)
    %     plotInPlaneAtoms(obj, hkl, Delta, alims, blims)
    %     plotInPlaneAtoms(obj, hkl, Delta, alims, blims, clims)
    %     plotInPlaneAtoms(___, Name, Value) % Use this option with any of
    %         the input argument combination in the previous syntaxes
    %
    % Input:
    %     obj ... GIDVis_Crystal object.
    %     hkl ... three-element vector of Miller indices of the plane.
    %     Delta ... limit how close an atom has to be to the plane to be
    %               plotted. Default 1.
    %     alims ... limits in the direction of obj.a_vec. Default [-2 2].
    %     blims ... limits in the direction of obj.b_vec. Default [-2 2].
    %     clims ... limits in the direction of obj.c_vec. Default [-2 2].
    %     Name, Value ... Name-Value pairs to adapt the plotting process.
    %                     For the list of supported name-value pairs see
    %                     plotAtoms.
    %
    % Output:
    %     hggroup containing all elements plotted
    % 
    %
    % This file is part of GIDVis.
    %
    % see also: IncreaseUnitCell, plotAtoms, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2020 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% obj, hkl, Delta, alims, blims, clims, varargin
    p = inputParser;
    p.KeepUnmatched = true;
    addRequired(p, 'obj', @(x) isscalar(x) && isa(x, 'GIDVis_Crystal'));
    addRequired(p, 'hkl', @(x) isreal(x) && numel(x) == 3);
    addOptional(p, 'Delta', 1, @(x) isreal(x) && isscalar(x));
    addOptional(p, 'alims', [-2 2], @(x) isreal(x) && numel(x) == 2);
    addOptional(p, 'blims', [-2 2], @(x) isreal(x) && numel(x) == 2);
    addOptional(p, 'clims', [-2 2], @(x) isreal(x) && numel(x) == 2);
    parse(p, obj, hkl, varargin{:});
    
    % get the atoms in the hkl plane
    Atoms = InPlaneAtoms(p.Results.obj, p.Results.hkl, p.Results.Delta, ...
        p.Results.alims, p.Results.blims, p.Results.clims);

    % get the names of the unmatched inputs and their values and combine
    % them in a cell
    Unmatched = [fieldnames(p.Unmatched) struct2cell(p.Unmatched)]';
    
    % plot the atoms passing all the unmatched inputs
    hgInt = plotAtoms(obj, Atoms, Unmatched{:});
    
    if nargout > 0
        hg = hgInt;
    end
end