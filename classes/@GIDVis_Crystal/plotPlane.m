function p = plotPlane(obj, hkl, varargin)
    % Plots the plane given by hkl.
    %
    % Usage:
    %     plotPlane(obj, hkl)
    %     plotPlane(obj, hkl, Name, Value)
    %
    % Input:
    %     obj ... GIDVis_Crystal object.
    %     hkl ... hkl indices of the plane to plot as a three-element
    %             vector.
    %     Name, Value ... additional name-value pairs.
    %
    % All name-value pairs except for 'Translation' are directly passed to
    % the patch call used to plot the plane, i.e. you can use all name-
    % value pairs available for the patch command. 
    % 'Translation' is a 3-element vector [Tx, Ty, Tz]. Default [0 0 0].
    %
    % Output:
    %     handle of the patch of the plane
    %
    % This file is part of GIDVis.
    %
    % see also: hklPlane, patch, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2020 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    p = inputParser;
    p.KeepUnmatched = true;
    addRequired(p, 'obj', @(obj) isscalar(obj) && isa(obj, 'GIDVis_Crystal'));
    addRequired(p, 'uvw', @(hkl) isreal(hkl) && any(size(hkl) == 3));
    addParameter(p, 'Translation', [0 0 0], ...
        @(Translation) isreal(Translation) && numel(Translation) == 3)
    parse(p, obj, hkl, varargin{:});
    
    % get the names of the unmatched inputs and their values and combine
    % them in a cell
    Unmatched = [fieldnames(p.Unmatched) struct2cell(p.Unmatched)]';

    % calculate vertices and faces of the hkl plane
    [Vertices, Faces] = hklPlane(obj, hkl);

    % apply translation
    Vertices(:, 1) = Vertices(:, 1) + p.Results.Translation(1);
    Vertices(:, 2) = Vertices(:, 2) + p.Results.Translation(2);
    Vertices(:, 3) = Vertices(:, 3) + p.Results.Translation(3);
    
    % create the tags
    TagStr = ['(' num2str(hkl(1)) ' ' num2str(hkl(2)) ' ' num2str(hkl(3)) ')'];
    % plot the plane passing all unmatched name-value pairs to the patch
    % call
    pInt = patch('Vertices', Vertices, 'Faces', Faces, Unmatched{:}, ...
        'Tag', TagStr);
    
    if nargout > 0
        p = pInt;
    end
end