function hg = plotUnitCell(obj, varargin)
    % Plot the unit cell.
    %
    % Usage:
    %     hg = plotUnitCell(obj, varargin);
    %
    % Input:
    %     obj ... GIDVis_Crystal object
    %     varargin ... name - value pairs:
    %     Allowed name-value pairs:
    %         - LineColor: color of the parallelepiped lines
    %         - LineWidth: width of the parallelepiped lines
    %         - Parent: parent axis
    %         - aColor: color of the lattice vector a
    %         - bColor: color of the lattice vector b
    %         - cColor: color of the lattice vector c
    %         - FaceColor: face color of the parallelepiped
    %         - FaceAlpha: transparency of the parallelepiped faces
    %         - Translation: 3-element vector describing a translation
    %           of the crystal in x, y and z direction
    %         - Labels: 'on' or 'off' determining whether to show the
    %           labels a, b and c at the tips of the unit-cell vectors
    %         - FontSize: font size of the labels a, b and c
    %     All unmatched name-value pairs will be directly passed to the
    %     patch command used to plot the unit cell faces.
    %
    % Output:
    %     hggroup containing all the elements plotted
    %
    % This file is part of GIDVis.
    %
    % see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2020 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    p = inputParser;
    p.KeepUnmatched = 1;
    addRequired(p, 'obj');
    addParameter(p, 'LineColor', 'k');
    addParameter(p, 'LineWidth', 2);
    addParameter(p, 'Parent', gca);
    addParameter(p, 'aColor', 'r');
    addParameter(p, 'bColor', 'g');
    addParameter(p, 'cColor', 'b');
    addParameter(p, 'FaceColor', 'none');
    addParameter(p, 'FaceAlpha', 1);
    addParameter(p, 'Translation', [0 0 0]);
    addParameter(p, 'Labels', 'on');
    addParameter(p, 'FontSize', 10);
    parse(p, obj, varargin{:});
    
    % get the unmatched name-value pairs in a cell
    UnMatched = [fieldnames(p.Unmatched) struct2cell(p.Unmatched)]';
    
    % assign the unit cell vectors to variables
    Ca = obj.a_vec;
    Cb = obj.b_vec;
    Cc = obj.c_vec;

    % calculate the vertices of the unit cell from the unit cell
    % vectors
    Vertices = [zeros(3, 1) Ca Ca+Cb Cb ...
    Cc Ca+Cc Ca+Cb+Cc Cb+Cc]';
    Vertices(:, 1) = Vertices(:, 1) + p.Results.Translation(1);
    Vertices(:, 2) = Vertices(:, 2) + p.Results.Translation(2);
    Vertices(:, 3) = Vertices(:, 3) + p.Results.Translation(3);
    % define the faces of the unit cell
    Faces = [1 2 3 4; 1 2 6 5; 2 3 7 6; 3 4 8 7; 4 1 5 8; 5 6 7 8];

    hgInt = hggroup('Parent', p.Results.Parent);

    % plot the unit cell using patch
    patch('Vertices', Vertices, 'Faces', Faces, ...
        'FaceColor', p.Results.FaceColor, ...
        'LineWidth', p.Results.LineWidth, ...
        'FaceAlpha', p.Results.FaceAlpha, 'Parent', hgInt, ...
        'EdgeColor', p.Results.LineColor, UnMatched{:}, 'Tag', 'UnitCell');
    % plot the unit cell vectors
    plotDirection(obj, [1 0 0], 0, 'Translation', ...
        p.Results.Translation, 'Color', p.Results.aColor, 'LineWidth', ...
        p.Results.LineWidth, 'Parent', hgInt, 'Tag', 'a_vec');
    plotDirection(obj, [0 1 0], 0, 'Translation', ...
        p.Results.Translation, 'Color', p.Results.bColor, 'LineWidth', ...
        p.Results.LineWidth, 'Parent', hgInt, 'Tag', 'b_vec');
    plotDirection(obj, [0 0 1], 0, 'Translation', ...
        p.Results.Translation, 'Color', p.Results.cColor, 'LineWidth', ...
        p.Results.LineWidth, 'Parent', hgInt, 'Tag', 'c_vec');
    % create the text labels
    if ~strcmpi(p.Results.Labels, 'off')
        text(hgInt, obj.a_vec(1)+p.Results.Translation(1), ...
            obj.a_vec(2)+p.Results.Translation(2), ...
            obj.a_vec(3)+p.Results.Translation(3), 'a', ...
            'Tag', 'aLabel', ...
            'FontSize', p.Results.FontSize)
        text(hgInt, obj.b_vec(1)+p.Results.Translation(1), ...
            obj.b_vec(2)+p.Results.Translation(2), ...
            obj.b_vec(3)+p.Results.Translation(3), 'b', ...
            'Tag', 'bLabel', ...
            'FontSize', p.Results.FontSize)
        text(hgInt, obj.c_vec(1)+p.Results.Translation(1), ...
            obj.c_vec(2)+p.Results.Translation(2), ...
            obj.c_vec(3)+p.Results.Translation(3), 'c', ...
            'Tag', 'cLabel', ...
            'FontSize', p.Results.FontSize)
    end
    
    % make the axes lengths equal in all three directions
    axis(p.Results.Parent, 'equal')
    
    if nargout > 0
        hg = hgInt;
    end
end