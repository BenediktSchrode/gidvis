function Dirs = uvwDirection(obj, uvw, Normalize)
    % Returns the direction given by uvw.
    %
    % Usage:
    %     Dirs = uvwDirection(obj, uvw, Normalize);
    % 
    % Input:
    %     obj ... GIDVis_Crystal object
    %     uvw ... uvw indices of the direction as an N x 3 matrix
    %     Normalize ... If 0, no normalization takes place. If larger than
    %                   0 the direction is normalized to this length.
    %
    % Output:
    %     Dirs ... N x 3 matrix of the directions
    %
    %
    % This file is part of GIDVis.
    %
    % see also: plotDirection, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    if nargin < 3 || isempty(Normalize)
        Normalize = 0;
    end

    % create the matrix with the vectors describing the unit cell
    M = [obj.a_vec obj.b_vec obj.c_vec]';
    % multiply by uvw to get the end points of the vectors
    Dirs = uvw*M;
    % normalize if required
    if Normalize
        L = sqrt(sum(Dirs.^2, 2));
        Dirs = Dirs./repmat(L, 1, 3);
        Dirs = Dirs.*Normalize;
    end
end