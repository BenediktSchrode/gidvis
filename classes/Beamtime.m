classdef Beamtime < handle
    % Represents a beamtime. Stores the used experimental set ups.
    %
    % This file is part of GIDVis.
    %
    % see also: ExperimentalSetUp, GIDVisLicense

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %                                                                         %
    %                       This file is part of GIDVis                       %
    %                                                                         %
    % Copyright (C) 2017-2018 Benedikt Schrode, Christian Roethel, Stefan     %
    % Pachmajer                                                               %
    %                                                                         %
    % GIDVis is free software: you can redistribute it and/or modify it under %
    % the terms of the GNU General Public License as published by the Free    %
    % Software Foundation, either version 3 of the License, or (at your       %
    % option) any later version.                                              %
    %                                                                         %
    % GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
    % ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
    % FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
    % for more details.                                                       %
    %                                                                         %
    % You should have received a copy of the GNU General Public License along %
    % with GIDVis, you can find it in GIDVisLicense.m.                        %
    % If not, see <http://www.gnu.org/licenses/>.                             %
    %                                                                         %
    %                                                                         %
    % Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
    %                                                                         %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    properties
        % Name of the beamtime.
        % Shown in the LoadFileModule window
        Name
        % Array of the single experimental set ups.
        SetUps = ExperimentalSetUp.empty
    end
    
    methods
        function obj = Beamtime(varargin)
            % Constructur.
            %
            % Usage:
            %     B = Beamtime();
            %     B = Beamtime('Name')
            %     B = Beamtime('Name', ArrayOfExperimentalSetUps)
            if nargin == 0
                obj.Name = 'Default Beamtime Name';
            elseif nargin == 1
                obj.Name = varargin{1};
            elseif nargin == 2
                obj.Name = varargin{1};
                obj.SetUps = varargin{2};
            end
        end
        
        function AddSetUp(obj, SetUp)
            % Adds an experimental to the end of the SetUps array.
            %
            % Usage: 
            %     Beamtime.AddSetUp(SetUp)
            %     AddSetUp(Beamtime, SetUp)
            if ~isa(SetUp, 'ExperimentalSetUp')
                error('GIDVis:Beamtime:WrongType', 'Only ExperimentalSetUp objects are supported.');
            end
            obj.SetUps(end+1) = SetUp;
        end
        
        function RemoveSetUp(obj, SetUpName)
            % Removes the experimental set up with a given name from the experimental set up array.
            %
            % Usage:
            %     Beamtime.RemoveSetUp('Name')
            %     RemoveSetUp(Beamtime, 'Name')
            if ~ischar(SetUpName)
                error('GIDVis:Beamtime:WrongType', 'Only ExperimentalSetUp objects are supported.');
            end
            L = strcmp({obj.SetUps.name}, SetUpName);
            if sum(L) == 0
                warning('GIDVis:Beamtime:NotFound', 'Could not find ExperimentalSetUp with name ''%s''.', SetUpName)
            else
                obj.SetUps(L) = [];
            end
        end
        
        function RemoveSetUpAt(obj, Index)
            % Removes the experimental set up at position Index from the array.
            %
            % Usage: 
            %     Beamtime.RemoveSetUpAt(Index)
            %     RemoveSetUpAt(Beamtime, Index)
            if Index > numel(obj.SetUps)
                warning('GIDVis:Beamtime:IndexOutOfRange', 'Cannot delete ExperimentalSetup. Index out of range.');
            else
                obj.SetUps(Index) = [];
            end
        end
    end
end