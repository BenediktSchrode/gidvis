classdef ExperimentalSetUp
    % Class containing the experimental parameters of the measurement
    %
    % This file is part of GIDVis.
    %
    % see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2018 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    properties
        % Name for this set up.
        % Shown, e.g., in the LoadFileModule.
        name = 'Default name'
        % Sample detector distance in mm.
        sdd = 150
        % Horizontal pixel size of the detector in micrometers.
        psx = 0.172
        % Vertical pixel size of the detector in micrometers.
        psz = 0.172
        % Number of pixels of the detector in horizontal direction.
        detlenx = 1475
        % Number of pixels of the detector in vertical direction.
        detlenz = 840
        % Beam center in horizontal direction (in pixels).
        % Can be double.
        cpx = 800
        % Beam center in vertical direction (in pixels).
        % Can be double.
        cpz = 826
        % Wavelength used during the measurement in Angstroems.
        lambda = 1.4
        % Rotation of the detector around the x axes.
        rx = 0
        % Rotation of the detector around the y axes.
        ry = 0
        % Rotation of the detector around the z axes.
        rz = 0
        % correction for gamma if outer detector arm moves in a different
        % plane than expected
        outer_offset = 0
        % Geometry of the set-up (see the property PossibleGeometries for a
        % list of supported geometries).
        Geometry = 'none';
        % angle of the in-plane detector rotation
        gamma = 0;
        % angle of the out-of-plane detector rotation
        delta = 0;
        % Fraction of the horizontally polarized light (between 0 and 1)
        HFraction = 0.99;
        % Detector material properties
        % Structure with fields Formula, Density and Thickness (in mm).
        % Default values DetectorMaterial.Formula = Si,
        % DetectorMaterial.Density = 2.33, DetectorMaterial.Thickness =
        % 0.32.
        DetectorMaterial = struct('Formula', '', 'Density', [], 'Thickness', []);
        % Properties of the medium between sample and detector
        % Structure with fields Formula and Density. Default values are
        % air, i. e. Medium.Formula = 'N78O21Ar', Medium.Density =
        % 0.0011839.
        Medium = struct('Formula', 'N78O21Ar', 'Density', 0.0011839);
        % Beam height (mm) usually defined by primary side slits
        BeamHeight = 0.2;
        % Beam width (mm) usually defined by primary side slits
        BeamWidth = 0.2;
        % Flat field image data
        FFImageData = [];
    end
    
    properties (SetAccess = private)
        % List of possible geometries. 
        % GammaDelta: means that the outer rotation angle is gamma
        % (rotation around the z axis), the inner rotation angle is delta
        % (rotation around the rotated x axis).
        PossibleGeometries = {'none', 'GammaDelta'};
    end
    
    methods        
        function obj = set.gamma(obj, value)
            if strcmp(obj.Geometry, 'none') && value ~= 0
                warning('GIDVis:ExperimentalSetUp:Geometry', ...
                    'Setting geometry to ''GammaDelta''.')
                obj.Geometry = 'GammaDelta';
            end
            obj.gamma = value;
        end
        
        function obj = set.delta(obj, value)
            if strcmp(obj.Geometry, 'none') && value ~= 0
                warning('GIDVis:ExperimentalSetUp:Geometry', ...
                    'Setting geometry to ''GammaDelta''.')
                obj.Geometry = 'GammaDelta';
            end
            obj.delta = value;
        end
        
        function obj = ExperimentalSetUp(varargin)
            % Constructor
            %
            % Usage: 
            % obj = ExperimentalSetUp();
            % obj = ExperimentalSetUp(name);
            % obj = ExperimentalSetUp(name, sdd, psx, psz, detlenx, detlenz, ...
            %     cpx, cpz, lambda, rx, ry, rz, outer_offset);
            
            if nargin == 0
            elseif nargin == 1
                obj.name = varargin{1};
            elseif nargin == 13
                obj.name = varargin{1};
                obj.sdd = varargin{2};
                obj.psx = varargin{3};
                obj.psz = varargin{4};
                obj.detlenx = varargin{5};
                obj.detlenz = varargin{6};
                obj.cpx = varargin{7};
                obj.cpz = varargin{8};
                obj.lambda = varargin{9};
                obj.rx = varargin{10};
                obj.ry = varargin{11};
                obj.rz = varargin{12};
                obj.outer_offset = varargin{13};
            end
        end
        
        function obj = struct2cell(ExperimentalSetUp)
            obj = cell(numel(properties(ExperimentalSetUp)),1);
            prop = properties(ExperimentalSetUp);
            for i = 1:numel(prop)
                obj{i} = ExperimentalSetUp.(prop{i});
            end
        end
        
        function [R, phi] = CalculatePolar(obj, omega, chi, x, z)
            % Calculates the polar space data.
            %
            % Usage:
            %     [R, phi] = CalculatePolar(obj, omega, chi)
            %     [R, phi] = CalculatePolar(obj, omega, chi, x, z)
            %
            % Input:
            %     obj ... ExperimentalSetUp
            %     omega ... omega angle
            %     chi ... chi angle
            %     x ... x pixel values to convert. Defaulted to full
            %           detector area if not given.
            %     z ... z pixel values to convert. Defaulted to full
            %           detector area if not given.
            %
            % Output:
            %     R ... radial part
            %     phi ... angular part
            if nargin == 3
                [q_xy, q_z, ~, ~, R] = CalculateQ(obj, omega, chi);
            elseif nargin == 5
                [q_xy, q_z, ~, ~, R] = CalculateQ(obj, omega, chi, x, z);
            end
            phi = atan2(q_xy, q_z)*180/pi;
        end
        
        function [R, phi, Int] = CalculatePolarRegridded(obj, omega, chi, PixelIntensity, RegridResolution)
            % Calculates the regridded polar space data.
            %
            % Usage:
            %     [R, phi, Int] = CalculatePolarRegridded(obj, omega, chi, PixelIntensity, RegridResolution)
            %
            % Input:
            %     obj ... ExperimentalSetUp
            %     omega ... omega angle
            %     chi ... chi angle
            %     PixelIntensity ... intensity values
            %     RegridResolution ... two-element vector with number of
            %                          boxes to use for the regridding
            %                          process
            %
            % Output:
            %     R ... two-element vector giving the lower and upper bound
            %           of the radial part
            %     phi ... two-element vector giving the lower and upper
            %             bound of the angular part
            %     Int ... intensity matrix of the size as given by the
            %             regridding resolution.
            %
            %
            % You can easily plot the output using imagesc(R, phi, Int).
            % If the full R or phi vector is needed, use linspace.            
            [R, phi] = CalculatePolar(obj, omega, chi);
            [Int, R, phi] = regrid(R, phi, PixelIntensity(:), RegridResolution);
        end
        
        function [q_xy, q_z, q_x, q_y, q] = CalculateQ(obj, omega, chi, x, z)
            % Calculates the reciprocal scattering vector components.
            %
            % Usage:
            %     [q_xy, q_z, q_x, q_y, q] = CalculateQ(obj, omega, chi)
            %     [q_xy, q_z, q_x, q_y, q] = CalculateQ(obj, omega, chi, x, z)
            %
            % Input:
            %     obj ... ExperimentalSetUp
            %     omega ... omega angle
            %     chi ... chi angle
            %     x ... x pixel values to convert. Defaulted to full
            %           detector area if not given.
            %     z ... z pixel values to convert. Defaulted to full
            %           detector area if not given.
            %
            % Output:
            %     q_xy ... in-plane component of the scattering vector
            %     q_z ... out-of-plane component of the scattering vector
            %     q_x ... x-component of the scattering vector
            %     q_y ... y-component of the scattering vector
            %     q ... length of the scattering vector
            if nargin < 4
                kout = getDetectorPixel(obj);
            else
                kout = getDetectorPixel(obj, x, z);
            end
            [q_xy, q_z, q_x, q_y, q] = getQ(obj, kout, omega, chi);
        end
        
        function [qxy, qz, Int] = CalculateQRegridded(obj, omega, chi, PixelIntensity, RegridResolution)
            % Calculates the regridded reciprocal space values.
            %
            % Usage:
            %     [qxy, qz, Int] = CalculateQRegridded(obj, omega, chi, PixelIntensity, RegridResolution)
            %
            % Input:
            %     obj ... ExperimentalSetUp
            %     omega ... omega angle
            %     chi ... chi angle
            %     PixelIntensity ... intensity values
            %     RegridResolution ... two-element vector with number of
            %                          boxes to use for the regridding
            %                          process
            %
            % Output:
            %     qxy ... two-element vector giving the lower and upper
            %             bound of the in-plane component of the scattering
            %             vector
            %     qz ... two-element vector giving the lower and upper
            %            bound of the out-of-plane component of the
            %            scattering vector
            %     Int ... intensity matrix of the size as given by the
            %             regridding resolution.
            %
            %
            % You can easily plot the output using imagesc(qxy, qz, Int).
            % If the full qxy or qz vector is needed, use linspace.
            [q_xy, q_z] = CalculateQ(obj, omega, chi);
            [Int, qxy, qz] = regrid(q_xy, q_z, PixelIntensity(:), RegridResolution);
        end
    end
    
    methods
        function [qxy, qz, Int] = CalculateQSpace(obj, omega, chi, PixelIntensity, RegridResolution)
            % Calculates the regridded qxy, qz and intensity values
            %
            % Usage of this function is discouraged. Use
            % CalculateQRegridded instead.
            % 
            % Usage:
            %     [qxy, qz, Int] = CalculateQSpace(ExperimentalSetUp, omega, chi, PixelIntensity, RegridResolution)
            
            kout = getDetectorPixel(obj);
            [qxy, qz] = getQ(obj, kout, omega, chi);
            [Int, qxy, qz] = regrid(qxy, qz, PixelIntensity(:), RegridResolution);
        end
        
        function [v] = getDetectorPixel(varargin)
            % Calculates k_out for the set up.
            % Usage: 
            % v = getDetectorPixel(ExperimentalSetUp);
            %     Calculates k_out for all pixels of the detector, i.e.
            %     pixels 1 to detlenx in horizontal direction and 1 to
            %     detlenz in vertical direction.
            %
            % v = getDetectorPixel(ExperimentalSetUp, x, z);
            %     Calculates k_out for the pixels given by x and z.

            if nargin == 1
                beamline = varargin{1};

                [xm, zm] = meshgrid(1:beamline.detlenx, 1:beamline.detlenz);
                x = xm(:);
                z = zm(:);

                xpx = beamline.cpx;
                zpx = beamline.cpz;

                pxsize_x = beamline.psx;
                pxsize_z = beamline.psz;

            elseif nargin == 3
                beamline = varargin{1};
                x = varargin{2};
                z = varargin{3};
                x = x(:);
                z = z(:);
                pxsize_x = beamline.psx;
                pxsize_z = beamline.psz;
                xpx = beamline.cpx;
                zpx = beamline.cpz;
            else
                error('GIDVis:getDetectorPixel', 'Incorrect number of input arguments.')
            end

            Rx = @(ai) [1 0 0; 0 cosd(ai) -sind(ai); 0 sind(ai) cosd(ai)];
            Ry = @(ai) [cosd(ai) 0 sind(ai); 0 1 0; -sind(ai) 0 cosd(ai)]; 
            Rz = @(ai) [cosd(ai) -sind(ai) 0; sind(ai) cosd(ai) 0; 0 0 1];

            x1 =(x-xpx)*pxsize_x;
            y1 = 0*x1;

            z1 = (zpx-z)*pxsize_z;
            y2 = 0*z1;

            v = [x1 y1+y2 z1];

            %tilt detector in detector coordinate systems
            if beamline.rx ~= 0 || beamline.rz ~= 0 || beamline.ry ~= 0
                rot_x = Rx(beamline.rx);
                rot_z = Rz(beamline.rz);
                rot_y = Ry(beamline.ry);
                v = (rot_z*rot_x*rot_y*v')';
            end
            
            %move according to sdd
            v(:,2) = v(:,2)+beamline.sdd;

            % perform goniometer rotations around center of sample
            switch beamline.Geometry
                case 'GammaDelta'
                    if beamline.gamma ~= 0 || beamline.delta ~= 0
                        gam = Rz(beamline.gamma+beamline.outer_offset);
                        del = Rx(beamline.delta);
                        v = (gam*del*v')';
                    end
                otherwise
                    
            end
        end
        
        function [q_xy, q_z, q_x, q_y, q] = getQ(beamline, kout, omega, chi)
            % For given k_out, calculates the corresponding q vectors.
            % Output:
            %     q_xy ... in-plane component of the scattering vector.
            %     q_z ... out-of-plane component of the scattering vector.
            %     q_x ... x component of the scattering vector.
            %     q_y ... y component of the scattering vector.
            %     q ... absolute value of the scattering vector.
            %
            % Usage: 
            % [qxy, qz, q_x, q_y, q] = getQ(beamline, kout, omega, chi);
            
            Rx = @(ai) [1 0 0; 0 cosd(ai) -sind(ai); 0 sind(ai) cosd(ai)];
            Ry = @(ai) [cosd(ai) 0 sind(ai); 0 1 0; -sind(ai) 0 cosd(ai)]; 
            Rz = @(ai) [cosd(ai) -sind(ai) 0; sind(ai) cosd(ai) 0; 0 0 1];

            kin = [0;1;0];

            %converting pixels to q space:
            %normalisation (to get direction of kout) using a matrix
            kout_norm = sqrt(kout(:,1).^2+kout(:,2).^2+kout(:,3).^2);
            kout = kout./repmat(kout_norm, 1,3); 

            q_vec = (kout-repmat(kin',size(kout,1), 1)) .* 2*pi/beamline.lambda;

            %transform q vectors into sample coordinate system
            %using rotation around omega (incidence angle) and 
            %chi (rotation along the beam direction)
            q_vec = (Ry(chi)*Rx(-omega)*q_vec')';

            q_xy = sqrt(q_vec(:,1).^2+q_vec(:,2).^2).*sign(q_vec(:,1));
            q_z = q_vec(:,3);

            if nargout > 2
                q_x = q_vec(:,1);
                q_y = q_vec(:,2);
                q = sqrt(q_x.^2+q_y.^2+q_z.^2);
            end
        end
    end
end