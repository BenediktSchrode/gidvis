classdef GIDVis_Crystal_Atom
% Class representing an atom.
%
% This file is part of GIDVis.
%
% see also: ChEl, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    properties
        Name = '' % Name of the atom in the *.res file, e.g. C5 or H62A.
        x = 0 % fractional coordinate x of the atom in the unit cell.
        y = 0 % fractional coordinate y of the atom in the unit cell.
        z = 0 % fractional coordinate z of the atom in the unit cell.
        % (deprecated) number of places after comma of x
        % Use the xAccuracy property instead.
        Lx = 0
        % (deprecated) number of places after comma of y
        % Use the yAccuracy property instead.
        Ly = 0
        % (deprecated) number of places after comma of z
        % Use the zAccuracy property instead.
        Lz = 0
        Element ChEl = ChEl('C') % Chemical element.
        SOF = 1 % site occupancy factor.
        U = 0 % displacement parameter. Scalar or [U11 U22 U33 U23 U13 U12]
        % (deprecated) the Wyckoff letter of the atomic position
        % This property was filled during *.res file import in former
        % versions of GIDVis_Crystal, in current versions it is not.
        WyckoffLetter
        % (deprecated) the multiplicity of the position
        % This property was filled during *.res file import in former
        % versions of GIDVis_Crystal, in current versions it is not.
        Multiplicity
        SymmOp % symmetry operation by which the atom was created
        xAccuracy % accuracy of x
        yAccuracy % accuracy of y
        zAccuracy % accuracy of z
    end
    
    methods
        function obj = GIDVis_Crystal_Atom(varargin)
            % GIDVis_Crystal_Atom Constructor
            %
            % Usage:
            %     A = GIDVis_Crystal_Atom();
            %     A = GIDVis_Crystal_Atom(x, y, z, El);
            %     A = GIDVis_Crystal_Atom(x, y, z, El, Name);
            %
            % Input:
            %     x ... fractional coordinate x
            %     y ... fractional coordinate y
            %     z ... fractional coordinate z
            %     El ... ChEl of the element
            %     Name ... name of the element
            %
            % Output:
            %     A ... GIDVis_Crystal_Atom
            %           Use the properties of A to set the other properties
            
            if nargin == 4 || nargin == 5
                obj = GIDVis_Crystal_Atom();
                obj.x = varargin{1};
                obj.y = varargin{2};
                obj.z = varargin{3};
                obj.Element = varargin{4};
                if nargin == 5
                    obj.Name = varargin{5};
                end
            end
        end
        
        function Val = get.xAccuracy(obj)
            if ~isempty(obj.xAccuracy)
                Val = obj.xAccuracy;
            elseif ~isempty(obj.Lx)
                Val = 10^(-obj.Lx);
            else
                Val = [];
            end
        end
        
        function Val = get.yAccuracy(obj)
            if ~isempty(obj.yAccuracy)
                Val = obj.yAccuracy;
            elseif ~isempty(obj.Ly)
                Val = 10^(-obj.Ly);
            else
                Val = [];
            end
        end
        
        function Val = get.zAccuracy(obj)
            if ~isempty(obj.zAccuracy)
                Val = obj.zAccuracy;
            elseif ~isempty(obj.Lz)
                Val = 10^(-obj.Lz);
            else
                Val = [];
            end
        end
        
        function Beq = Beq(obj, Cr)
            % Calculate the equivalent Debye-Waller Factor from the
            % displacement parameter.
            %
            % source:
            % Rietveld Analysis Program BGMN. Manual.
            % Joerg Bergmann
            Beq = NaN(size(obj));
            for iO = 1:numel(obj)
                if numel(obj(iO).U) == 1
                    Beq(iO) = 8*pi^2*obj(iO).U;
                else
                    Beq(iO) = 8*pi^2/3 * ...
                        (obj(iO).U(1)*sind(Cr.alpha)^2+obj(iO).U(2)*sind(Cr.beta)^2+obj(iO).U(3)*sind(Cr.gamma)^2 + ...
                        2*obj(iO).U(6)*sind(Cr.alpha)*sind(Cr.beta)*cosd(Cr.gamma)+...
                        2*obj(iO).U(5)*sind(Cr.alpha)*sind(Cr.gamma)*cosd(Cr.beta)+...
                        2*obj(iO).U(4)*sind(Cr.beta)*sind(Cr.gamma)*cosd(Cr.alpha))/...
                        (1+2*cosd(Cr.alpha)*cosd(Cr.beta)*cosd(Cr.gamma)-cosd(Cr.alpha)^2-cosd(Cr.beta)^2-cosd(Cr.gamma)^2);
                end
            end
        end
        
        function obj = set.Element(obj, NewElement)
            if ischar(NewElement)
                El = ChEl(NewElement);
                obj.Element = El;
            else
                obj.Element = NewElement;
            end
        end
    end
end % classdef