%% Demo file for GIDVis_Crystal.m
% This file gives a short overview of possibilites of GIDVis_Crystal.
% To run completely, it requires Gold.res (*.res file of the crystal
% structure of gold) and P2O_1230895.res (*.res file of the crystal
% structure of P2O solved by Dzyabchenko et al.). They can be found e.g. at
% http://www.crystallography.net/cod/9008463.html?cif=9008463 and 
% https://www.ccdc.cam.ac.uk/structures/Search?Ccdcid=1230895
%
% This file is part of GIDVis.
%
% see also: GIDVis_Crystal, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Standard Properties
% Create a default crystal:
Cr = GIDVis_Crystal;

% Look at some of its real space properties:
[Cr.a Cr.b Cr.c Cr.alpha Cr.beta Cr.gamma Cr.V]

%%
% Look at some of its reciprocal space properties:
[Cr.a_star Cr.b_star Cr.c_star Cr.alpha_star Cr.beta_star Cr.gamma_star]

%%
% You can also obtain the unit cell vectors in real space (each column is
% one vector):
[Cr.a_vec Cr.b_vec Cr.c_vec]

%%
% and in reciprocal space (each column is one vector):
[Cr.a_star_vec Cr.b_star_vec Cr.c_star_vec]

%% Diffraction
% Plot the expected peak positions of gold in (001) orientation.

%%
% Create the gold crystal:
Au = GIDVis_Crystal(4.07825, 4.07825, 4.07825, 90, 90, 90, 'Gold');

%%
% Create all possible hkl combinations with h, k, and l between -2 and 2:
HKL = Cr.CreateHKLCombinations(-2:2, -2:2, -2:2);

%%
% calculate the peak positions for these hkl values in (001) orientation:
PP = PeakPositionsQ(Au, HKL, [0 0 1]);

%% 
% and plot them in a reciprocal space map (i.e. q space)
figure
plot(sqrt(PP(:, 1).^2+PP(:, 2).^2), PP(:, 3), 'ok')
xlabel('q_{xy} (A^{-1})')
ylabel('q_z (A^{-1})')
title({'Calculated peak positions for Au(001)', ...
    'with hkl values between -2 and 2'})

%%
% Please note that a calculated peak does not mean that this peak can be
% observed experimentally, since this is influenced by the structure
% factor.

%%
% Load the gold crystal from a *.res file:
AuRes = GIDVis_Crystal('Gold.res');

% calculate the structure factors for the peaks obtained above
SF = GetStructureFactors(AuRes, HKL);

%%
% Experimentally measured intensity is proportional to the square of the
% absolute value of SF:
I = abs(SF).^2;

%%
% Plot all peaks with an expected intensity larger than 0.1:
L = I > 0.1;

figure
plot(sqrt(PP(L, 1).^2+PP(L, 2).^2), PP(L, 3), 'ok')
xlabel('q_{xy} (A^{-1})')
ylabel('q_z (A^{-1})')
title({'Calculated peak positions for Au(001) with hkl values', ...
    'between -2 and 2 and |SF|^2 larger than 0.1'})

%% Standard Plotting
% Plot the unit cell of the default crystal and the gold crystal. To make
% them distinguishable, set the line color of the gold crystal to orange.
% Additionally, plot the gold atoms in 2-d style.
figure
plotUnitCell(Cr)
hold on
plotUnitCell(AuRes, 'LineColor', [255 165 0]./255)
plotAtoms(AuRes, IncreaseUnitCell(AuRes), 'AtomStyle', '2d')
xlabel('x (A)')
ylabel('y (A)')
zlabel('z (A)')
axis equal
title('Default crystal and gold unit cell')

%%
% Plot the gold crystal repeated twice in each direction with the atoms in
% 2-d style.
figure
plotUnitCell(AuRes)
view(3)
plotAtoms(AuRes, IncreaseUnitCell(AuRes, [0 2], [0 2], [0 2]), ...
    'AtomStyle', '2d')
xlabel('x (A)')
ylabel('y (A)')
zlabel('z (A)')
axis equal
view(3)
title('Gold crystal of 2 x 2 x 2 the size of the unit cell.')

%%
% Plot the (111) and (100) plane of the gold crystal together with the unit
% cell.
figure
plotUnitCell(AuRes)
hold on
plotPlane(AuRes, [1 1 1], 'FaceColor', 'r', 'FaceAlpha', 0.5, ...
    'DisplayName', '(111)')
plotPlane(AuRes, [1 0 0], 'FaceColor', 'g', 'FaceAlpha', 0.5, ...
    'DisplayName', '(100)')
xlabel('x (A)')
ylabel('y (A)')
zlabel('z (A)')
axis equal
legend show
title('(111) and (100) plane of the gold crystal')

%%
% Plot the atoms lying within +/- 1 of the 111 plane of the gold crystal:
figure
view([0 0 1])
plotInPlaneAtoms(AuRes, [1 1 1], 1);
xlabel('x (A)')
ylabel('y (A)')
zlabel('z (A)')
axis equal
title('Gold atoms lying in the (111) plane')

%%
% Plot the gold unit cell, the (111) plane and plot the high-symmetry
% directions of this plane.
figure
plotUnitCell(AuRes)
hold on
plotPlane(AuRes, [1 1 1], 'FaceColor', 'k', 'FaceAlpha', 0.5, ...
    'EdgeColor', 'none')
plotDirection(AuRes, [0 1 -1; 0 -1 1; 1 0 -1; -1 0 1; 1 -1 0; -1 1 0], ...
    'Translation', [AuRes.a AuRes.b AuRes.c]./3, 'Label', 'on', ...
    'LineWidth', 2, 'Color', 'm')
xlabel('x (A)')
ylabel('y (A)')
zlabel('z (A)')
axis equal
title('High symmetry directions of the (111) plane')

%%
% To change the direction the camera is pointing to, you can use MATLAB's
% view function. For example, to look perpendicular to the (111) plane, use
view(AuRes.NormalVector([1 1 1]))

%%
% You could have also used view(AuRes.uvwDirection([1 1 1])) since for this
% cubic crystal the normal vector of the (111) plane is the same as the
% direction [111].

%% Advanced Plotting: Rotations of the Crystal
% Rotate the gold crystal so that the (111) plane is parallel to the
% x-y-plane and plot the atoms within +/- 1 of the (111) plane.

%%
% Orient the gold crystal so that the 111 plane is parallel to the
% x-y-plane. I.e. the normal vector of the 111 plane should be parallel to
% the z-axis.
Orient(AuRes, [1 1 1], [0 0 1])

%%
% Plot the atoms of the (111) plane with the plane and the unit cell:
figure
subplot(1, 2, 1)
plotInPlaneAtoms(AuRes, [1 1 1], 1, 'AtomStyle', '2d')
hold on
plotUnitCell(AuRes)
plotPlane(AuRes, [1 1 1], 'FaceColor', 'r', 'FaceAlpha', 0.5)
xlabel('x (A)')
ylabel('y (A)')
zlabel('z (A)')
axis equal
title({'Gold crystal rotated so that the', ...
    '(111) plane is parallel to the x-y-plane:', 'Top View'})
subplot(1, 2, 2)
plotInPlaneAtoms(AuRes, [1 1 1], 'AtomStyle', '2d')
hold on
plotUnitCell(AuRes)
plotPlane(AuRes, [1 1 1], 'FaceColor', 'r', 'FaceAlpha', 0.5)
xlabel('x (A)')
ylabel('y (A)')
zlabel('z (A)')
axis equal
view([0 1 0])
title({'Gold crystal rotated so that the', ...
    '(111) plane is parallel to the x-y-plane:', 'Side View'})

%%
% Now compare the unit cell of the unrotated gold crystal (Au) to the unit
% cell of the rotated gold crystal (AuRes). Translate the rotated unit cell
% a bit to move them apart.
figure
subplot(2, 2, 1)
plotUnitCell(Au)
hold on
plotPlane(Au, [1 1 1], 'FaceColor', 'r', 'FaceAlpha', 0.5, ...
    'EdgeColor', 'none')
plotUnitCell(AuRes, 'Translation', [-8 0 0])
plotPlane(AuRes, [1 1 1], 'Translation', [-8 0 0], 'FaceColor', 'r', ...
    'FaceAlpha', 0.5, 'EdgeColor', 'none')
xlabel('x (A)')
ylabel('y (A)')
zlabel('z (A)')
title('Default View')
axis equal
subplot(2, 2, 2)
plotUnitCell(Au)
hold on
plotPlane(Au, [1 1 1], 'FaceColor', 'r', 'FaceAlpha', 0.5, ...
    'EdgeColor', 'none')
plotUnitCell(AuRes, 'Translation', [-8 0 0])
plotPlane(AuRes, [1 1 1], 'Translation', [-8 0 0], 'FaceColor', 'r', ...
    'FaceAlpha', 0.5, 'EdgeColor', 'none')
xlabel('x (A)')
ylabel('y (A)')
zlabel('z (A)')
title('X-Y-View')
axis equal
view([0 0 1])
subplot(2, 2, 3)
plotUnitCell(Au)
hold on
plotPlane(Au, [1 1 1], 'FaceColor', 'r', 'FaceAlpha', 0.5, ...
    'EdgeColor', 'none')
plotUnitCell(AuRes, 'Translation', [-8 0 0])
plotPlane(AuRes, [1 1 1], 'Translation', [-8 0 0], 'FaceColor', 'r', ...
    'FaceAlpha', 0.5, 'EdgeColor', 'none')
xlabel('x (A)')
ylabel('y (A)')
zlabel('z (A)')
title('X-Z-View')
axis equal
view([0 -1 0])
subplot(2, 2, 4)
plotUnitCell(Au);
hold on
plotPlane(Au, [1 1 1], 'FaceColor', 'r', 'FaceAlpha', 0.5, ...
    'EdgeColor', 'none')
plotUnitCell(AuRes, 'Translation', [-8 0 0]);
plotPlane(AuRes, [1 1 1], 'Translation', [-8 0 0], 'FaceColor', 'r', ...
    'FaceAlpha', 0.5, 'EdgeColor', 'none')
xlabel('x (A)')
ylabel('y (A)')
zlabel('z (A)')
title('Y-Z-View')
axis equal
view([1 0 0])

%%
% Compare the unit cell vectors of the unrotated and the rotated gold
% crystal
fprintf('Unrotated:\n')
[Au.a_vec Au.b_vec Au.c_vec]
fprintf('Rotated:\n')
[AuRes.a_vec AuRes.b_vec AuRes.c_vec]

%%
% They are different, but the unit cell dimensions are the same:
fprintf('Unrotated:\n')
[Au.a Au.b Au.c Au.alpha Au.beta Au.gamma]
fprintf('Rotated:\n')
[AuRes.a AuRes.b AuRes.c AuRes.alpha AuRes.beta AuRes.gamma]

%% Advanced Plotting 2: Surface Unit Cells
% Load the gold crystal:
AuRes = GIDVis_Crystal('Gold.res');

%%
% Calculate the surface unit cell of the Gold (111) plane:
[a1, a2, gamma, hkl1, hkl2, a1vec, a2vec] = SurfaceUC(AuRes, [1 1 1]);

%%
% Rotate the gold crystal so that the (111) plane is parallel to the
% x-y-plane:
Orient(AuRes, [1 1 1], [0 0 1]);

%%
% Rotate the gold crystal around the z-axis so that the [10-1] direction is
% pointing in the positive x-direction.
% To do this, first, calculate the roation angle:
Angle = acosd(dot(AuRes.uvwDirection([1 0 -1], 1), [1 0 0]));

%%
% Now, create the rotation matrix:
R = [cosd(Angle) -sind(Angle) 0; sind(Angle) cosd(Angle) 0; 0 0 1];

%%
% Rotate the crystal:
Rotate(AuRes, R);

%%
% Plot the gold atoms lying in the (111) plane. Translate them a bit to
% have a gold atom in the origin. Additionally, plot the surface unit cell.
%
% Calculate the data points first:
P = [0 0 0; ...
    uvwDirection(AuRes, hkl1); ...
    uvwDirection(AuRes, hkl1) + uvwDirection(AuRes, hkl2); ...
    uvwDirection(AuRes, hkl2)];

figure
plotInPlaneAtoms(AuRes, [1 1 1], 'Translation', [1.665 0 0])
hold on
patch(P(:, 1), P(:, 2), 5*ones(size(P, 1), 1), 'w', 'FaceAlpha', 0.5, ...
    'LineWidth', 2)

%%
% As z-component of the patch 5s are used to make sure that the surface
% unit cell is lying above the gold atoms.

%% Advanced Plotting 3: Multiple Crystals
% Load the gold crystal and the 1230985 structure of P2O:
AuRes = GIDVis_Crystal('Gold.res');
P2O = GIDVis_Crystal('P2O_1230895.res');

%%
% Rotate the gold crystal so that the (111) plane is parallel to the
% x-y-plane:
Orient(AuRes, [1 1 1], [0 0 1]);

%%
% Rotate the P2O crystal so that the (140) plane is parallel to the
% x-y-plane:
Orient(P2O, [1 4 0], [0 0 1]);

%% 
% Now plot the atoms in the (140) plane of the P2O crystal over the atoms
% of the (111) plane of the gold crystal
figure
plotInPlaneAtoms(AuRes, [1 1 1], 1, [-6 6], [-6 6], [-6 6])
hold on
plotInPlaneAtoms(P2O, [1 4 0], 1, [-4 4], [-4 4], [-4 4], ...
    'Translation', [0 0 5])
xlim([-15 15])
ylim([-15 15])
xlabel('x (A)')
ylabel('y (A)')
zlabel('z (A)')
axis equal
title('P2O molecules on a gold (111) surface')

%%
% To understand which atoms have which color, plot the colored periodic
% table of the elements
ColorCode()