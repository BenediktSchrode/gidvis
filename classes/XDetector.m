classdef XDetector
    % Class representing an X-ray detector
    %
    %
    % This file is part of GIDVis.
    %
    % see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    properties
        Name {mustBeChar} = 'Pilatus 2M'; % Name of the detector
        detlenx {mustBeNumeric, mustBePositive, mustBeNonNan, mustBeInteger, mustBeNonempty} = 1475; % Number of pixels parallel to the bottom of the detector.
        detlenz {mustBeNumeric, mustBePositive, mustBeNonNan, mustBeInteger, mustBeNonempty} = 1679; % Number of pixels perpendicular to the bottom of the detector.
        psx {mustBeNumeric, mustBeReal, mustBePositive, mustBeNonNan, mustBeFinite, mustBeNonempty} = 0.172; % Pixel size (mm) in horizontal direction.
        psz {mustBeNumeric, mustBeReal, mustBePositive, mustBeNonNan, mustBeFinite, mustBeNonempty} = 0.172; % Pixel size (mm) in vertical direction.
        InactiveRows {mustBeNumeric, mustBeReal, mustBePositive, mustBeNonNan, mustBeFinite} = [196:212 408:424 620:636 832:848 1044:1060 1256:1272 1468:1484]; % Indices of inactive rows.
        InactiveColumns {mustBeNumeric, mustBeReal, mustBePositive, mustBeNonNan, mustBeFinite} = [488:494 982:988]; % Indices of inactive columns.
        Shape {mustBeMember(Shape, {'rectangular', 'round'})} = 'rectangular'; % Shape of the active area of the detector. 'rectangular' or 'round'.
        DetectorMaterial {mustBeChar} = 'Si'; % Chemical formula of the detector material.
        DetectorMaterialThickness {mustBeNumeric, mustBeReal, mustBeNonNan, mustBeFinite} = 0.32; % Thickness of the detector material (mm).
        DetectorMaterialDensity {mustBeNumeric, mustBeReal, mustBeNonNan, mustBeFinite} = 2.33; % Density of the detector material (g/cm^3).
    end
    
    methods
        function obj = XDetector(varargin)
            % XDetector creates an X-ray detector.
            %
            % Usage:
            %     D = XDetector(Name)
            %       Creates the detector with Name. Name should be 'Eiger
            %       500K', 'Eiger 1M', 'Eiger 4M', 'Eiger 16M', 'Pilatus
            %       100K', 'Pilatus 300K', 'Pilatus 300K-W', 'Pilatus 
            %       300KW', 'Pilatus 1M', 'Pilatus 2M', 'Rayonix SX165', 
            %       'Rayonix SX165 2x2', 'Rayonix SX165 4x4', 'Rayonix 
            %       SX165 8x8' or 'Vantec 2000'. Name is case insensitive.
            %
            %     D = XDetector(Name, detlenx, detlenz, psx, psz, ...
            %         InactiveRows, InactiveColumns, Shape, ...
            %         DetectorMaterial, DetectorMaterialThickness, ...
            %         DetectorMaterialDensity)
            %       Creates the detector with the given properties.
            if nargin == 0
                obj = XDetector('Default Detector', 1475, 1679, 0.172, 0.172, [], [], 'rectangular', '', [], []);
            elseif nargin == 1
                switch lower(varargin{1})
                    case 'eiger 500k'
                        obj = XDetector('Eiger 500K', 1030, 514, 0.075, 0.075, [], [], 'rectangular', '', [], []); %Eiger 500K
                    case 'eiger 1m'
                        obj = XDetector('Eiger 1M', 1030, 1065, 0.075, 0.075, 515:551, [], 'rectangular', '', [], []); %Eiger 1M
                    case 'eiger 4m'
                        obj = XDetector('Eiger 4M', 2070, 2167, 0.075, 0.075, [515:551 1066:1102 1617:1653], 1031:1040, 'rectangular', '', [], []); %Eiger 4M
                    case 'eiger 9m'
                        obj = XDetector('Eiger 9M', 3110, 3269, 0.075, 0.075, [515:551 1066:1102 1617:1653 2168:2204 2719:2755], [1031:1040 2071:2080], 'rectangular', '', [], []); %Eiger 9M
                    case 'eiger 16m'
                        obj = XDetector('Eiger 16M', 4150, 4371, 0.075, 0.075, [515:551 1066:1102 1617:1653 2168:2204 2719:2755 3270:3306 3821:3857], [1031:1040 2071:2080 3110:3120], 'rectangular', '', [], []); %Eiger 16M
                    case 'pilatus 100k'
                        obj = XDetector('Pilatus 100K', 487, 195, 0.172, 0.172, [], [], 'rectangular', 'Si', 0.32, 2.33); %Pilatus 100K
                    case 'pilatus 300k'
                        obj = XDetector('Pilatus 300K', 487, 619, 0.172, 0.172, [196:212 408:424], [], 'rectangular', 'Si', 0.32, 2.33); %Pilatus 300K
                    case {'pilatus 300k-w', 'pilatus 300kw'}
                        obj = XDetector('Pilatus 300K-W', 1475, 195, 0.172, 0.172, [], [488:494 982:988], 'rectangular', 'Si', 0.32, 2.33); %Pilatus 300K-W
                    case 'pilatus 1m'
                        obj = XDetector('Pilatus 1M', 981, 1043, 0.172, 0.172, [196:212 408:424 620:636 832:848], 488:494, 'rectangular', 'Si', 0.32, 2.33); %Pilatus 1M
                    case 'pilatus 2m'
                        obj = XDetector('Pilatus 2M', 1475, 1679, 0.172, 0.172, [196:212 408:424 620:636 832:848 1044:1060 1256:1272 1468:1484], [488:494 982:988], 'rectangular', 'Si', 0.32, 2.33); %Pilatus 2M
                    case {'rayonix sx165 2x2', 'rayonix sx165'}
                        obj = XDetector('Rayonix SX165 2x2', 2048, 2048, 0.08, 0.08, [], [], 'round', '', [], []); %Rayonix SX165 2x2
                    case 'rayonix sx165 4x4'
                        obj = XDetector('Rayonix SX165 4x4', 1024, 1024, 0.16, 0.16, [], [], 'round', '', [], []); %Rayonix SX165 4x4
                    case 'rayonix sx165 8x8'
                        obj = XDetector('Rayonix SX165 8x8', 512, 512, 0.32, 0.32, [], [], 'round', '', [], []); %Rayonix SX165 8x8
                    case 'vantec 2000'
                        obj = XDetector('Vantec 2000', 2048, 2048, 0.068, 0.068, [], [], 'rectangular', '', [], []); %Vantec 2000
                    otherwise
                        error('XDetector:UnknownDetectorName', ...
                            'The detector name ''%s'' is not known.', ...
                            varargin{1});
                end
            elseif nargin == 11
                obj.Name = varargin{1};
                obj.detlenx = varargin{2};
                obj.detlenz = varargin{3};
                obj.psx = varargin{4};
                obj.psz = varargin{5};
                obj.InactiveRows = varargin{6};
                obj.InactiveColumns = varargin{7};
                obj.Shape = varargin{8};
                obj.DetectorMaterial = varargin{9};
                obj.DetectorMaterialThickness = varargin{10};
                obj.DetectorMaterialDensity = varargin{11};
            else
                error('XDetector:NotEnoughInputArguments', ...
                    'Provide one or 11 input arguments.');
            end
        end
        
        function M = Pixels(obj, InactVal)
            % Returns a pixel representation of the detector.
            %
            % Usage:
            %     M = obj.Pixels
            %     M = obj.Pixels(InactVal)
            % 
            % Input:
            %     InactVal ... Value of the inactive area
            %
            % Output:
            %     M ... matrix representing the pixels of the detector
            
            if nargin < 2 || isempty(InactVal); InactVal = NaN; end
            
            % create matrix of the size of the detector
            M = ones(obj.detlenz, obj.detlenx);
            % set the inactive rows and columns to InactVal
            M(obj.InactiveRows, :) = InactVal;
            M(:, obj.InactiveColumns) = InactVal;
            
            if strcmp(obj.Shape, 'round') % handle the round detector
                % calculate the center of the detector
                C = [size(M, 2) size(M, 1)]./2;
                % create the x and z coordinates of the pixels
                [detx, detz] = meshgrid(1:obj.detlenx, 1:obj.detlenz);
                % calculate the distance of each pixel to the center
                PD = sqrt((detx-C(1)).^2 + (detz-C(2)).^2);
                % if the distance is larger than half the lower detector
                % size, the point is outside the active area
                L = PD > min(size(M)/2);
                % set the inactive pixels to InactVal
                M(L) = InactVal;
            elseif strcmp(obj.Shape, 'rectangular')
            else
                error('XDetector:UnknownShape', 'Unknown detector shape.')
            end
        end
    end
end

function mustBeChar(a)
    if ~ischar(a)
        error('Value assigned to DetectorMaterial has to be of type char.')
    end
end