classdef listbox < hgsetget %#ok. Backwards compatibility.
    % Extends the default uicontrol of style 'listbox' to handle
    % double-clicks. Create listeners to the events 'SingleClick' and
    % 'DoubleClick' to react to the corresponding actions.
    %   
    % You can set all properties of a standard uicontrol during the
    % listbox() call using parameter/value pairs. After object creation, a
    % limited subset of these properties is available. If you need to
    % change any of these properties after object creation, add them to the
    % dependent properties and add the set and get methods here in this
    % file.
    %
    % source: https://stackoverflow.com/questions/15298859/extending-matlab-uicontrol
    %
    % This file is part of GIDVis.
    %
    % see also: addlistener, uicontrol, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2018 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    properties (Access = private)
        Control
    end

    properties (Dependent = true)
        % Make any properties of uicontrol for which you want to still
        % allow access as dependent properties. Define set and get methods
        % for these properties below.
        FontSize
        Position
        String
        Units
        Value
        Visible
    end
    
    properties (Access = private)
        Callback
        LastClick = clock;
        LastIndex = 0;
    end
    
    events
        SingleClick
        DoubleClick
    end

    methods
        function obj = listbox(varargin)
            obj.Control = uicontrol(varargin{:}, 'Style', 'listbox');
            % make sure to delete this object if you delete the uicontrol
            set(obj.Control, 'DeleteFcn', {@(source, eventData) delete(obj)})
            
            set(obj.Control, 'Callback', @(src, evt) obj.CallbackFun);
        end
        
        function CallbackFun(obj)
            % get the current time
            CurrentClick = clock;
            % get the index of the clicked element
            CurrentIndex = obj.Value;
            if isempty(CurrentIndex); return; end
            % calculate the elapsed time between two clicks
            Delta = etime(CurrentClick, obj.LastClick);
            % timespan for which two clicks count as one double-click
            DeltaTDoubleClick = 0.5;
            % check for double click: (1) by time and (2) if same element
            % was clicked
            if Delta < DeltaTDoubleClick && CurrentIndex == obj.LastIndex
                notify(obj, 'DoubleClick'); % raise DoubleClick event
            else
                notify(obj, 'SingleClick'); % raise SingleClick event
            end
            % store the last click time
            obj.LastClick = CurrentClick;
            % store the last clicked index
            obj.LastIndex = CurrentIndex;
        end

        % Define set and get methods for any uicontrol properties you wish
        % to retain. These methods will simply redirect calls to the
        % uicontrol object.
        function units = get.Units(obj)
            units = get(obj.Control, 'Units');
        end
        function set.Units(obj, newUnits)
            set(obj.Control, 'Units', newUnits);
        end
        
        function vis = get.Visible(obj)
            vis = get(obj.Control, 'Visible');
        end
        function set.Visible(obj, newVis)
            set(obj.Control, 'Visible', newVis);
        end
        
        function size = get.FontSize(obj)
            size = get(obj.Control, 'FontSize');
        end
        function set.FontSize(obj, newSize)
            set(obj.Control, 'FontSize', newSize);
        end
        
        function Pos = get.Position(obj)
            Pos = get(obj.Control, 'Position');
        end
        function set.Position(obj, newPos)
            set(obj.Control, 'Position', newPos);
        end

        function Value = get.Value(obj)
            Value = get(obj.Control, 'Value');
        end
        function set.Value(obj, Value)
            set(obj.Control, 'Value', Value);
        end

        function items = get.String(obj)
            items = get(obj.Control, 'String');
        end
        function set.String(obj, String)
            set(obj.Control, 'String', String);
        end
    end
end