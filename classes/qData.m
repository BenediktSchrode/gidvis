classdef qData
    % Used to store pixel data together with the corresponding experimental
    % set ups.
    %
    % This file is part of GIDVis.
    %
    % see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2018 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    properties
        Beamtime % beamtime structure
        % Assignment of the raw data files to the beamtime setting.
        % If file i in RawDataFileNames is recorded with beamtime setting
        % j, then Assignment(i) = j.
        Assignment
    end
    
    properties (SetAccess = protected)
        % Cell containing the file names of the original files.
        RawDataFileNames
        % Cell containing the original raw pixel data of the files given in RawDataFileNames.
        RawPixelData
    end
    
    properties (Access = private)
        % Additional pixels (DEPRECATED).
        % AdditionalPixels [l, t, r, b] tells the algorithm how many pixels 
        % to ignore left/above/right/below a column/row/column/row full of 
        % NaNs (NaNs are representing blind areas of the detector).
        %
        % This property is deprecated since the same functionality can be
        % achieved by using a flat field correction image in the
        % experimental setup. Keeping property private to ensure that old
        % data files saved with this property do not change when displayed.
        AdditionalPixels = NaN(1, 4);
    end
    
    methods
        function [q_xy, q_z, q_x, q_y, q] = getQ(obj, omega, chi)
            q_xy = [];
            q_x = [];
            q_y = [];
            q_z = [];
            q = [];
            for iFiles = 1:numel(obj.RawPixelData)
                % calculate the q values
                [q_xy0, q_z0, q_x0, q_y0, q0] = obj.Beamtime.SetUps(obj.Assignment(iFiles)).CalculateQ(omega, chi);

                % append the new q values
                q_xy = [q_xy; q_xy0];
                q_x = [q_x; q_x0];
                q_y = [q_y; q_y0];
                q_z = [q_z; q_z0];
                q = [q; q0];
            end
        end
        
        function [qxyR, qzR, IntR] = RegriddedValues(obj, omega, chi, RegridResolution)
            [q_xy, q_z] = getQ(obj, omega, chi);
            Int = obj.IntensityCropped;
            % remove intensity values < 0 and the corresponding q values
            L = Int < 0;
            Int(L) = [];
            q_xy(L) = [];
            q_z(L) = [];
            % regrid everything
            [IntR, qxyR, qzR] = regrid(q_xy, q_z, Int, RegridResolution);
        end
        
        function [rho, Psi] = getPolar(obj, omega, chi)
            [q_xy, q_z, ~, ~, rho] = getQ(obj, omega, chi);
            Psi = atan2(q_xy, q_z)*180/pi;
        end
        
        function [rhoR, PsiR, IntR] = RegriddedValuesPolar(obj, omega, chi, RegridResolution)
            [rho, Psi] = getPolar(obj, omega, chi);
            Int = obj.IntensityCropped;
            % remove intensity values < 0 and the corresponding values in
            % rho and Psi
            L = Int < 0;
            Int(L) = [];
            rho(L) = [];
            Psi(L) = [];
            % regrid everything
            [IntR, rhoR, PsiR] = regrid(rho, Psi, Int, RegridResolution);
        end
        
        function Int = IntensityCropped(obj, Val)
            if nargin < 2 || isempty(Val)
                StartInd = 1;
                EndInd = numel(obj.RawPixelData);
            else
                StartInd = Val;
                EndInd = Val;
            end
            Int = [];
            for iFiles = StartInd:EndInd
                Int0 = obj.RawPixelData{iFiles};
                Int0 = obj.RemoveDetectorGaps(Int0);
                Int0 = Int0(1:obj.Beamtime.SetUps(obj.Assignment(iFiles)).detlenz, ...
                    1:obj.Beamtime.SetUps(obj.Assignment(iFiles)).detlenx);
                Int = [Int; Int0(:)];
            end
        end
        
        function Int = Intensity(obj)
            Int = [];
            for iFiles = 1:numel(obj.RawPixelData)
                Int0 = obj.RawPixelData{iFiles};
                Int0 = obj.RemoveDetectorGaps(Int0);
                Int = [Int; Int0(:)];
            end
        end
        
        function obj = qData(varargin)
            % Creates a qData object.
            %
            % Usage:
            %
            %     qData(FilePathsRawData, Beamtime, Assignment)
            %     Creates a qData with the specified parameters.
            %
            %     qData(Path)
            %     Loads the data file from Path, where Path is a string.
            
            if nargin == 1
                FilePath = varargin{1};
                if ~ischar(FilePath)
                    error('GIDVis:qData', 'When using one input argument, it has to be of type string.')
                end
                if exist(FilePath, 'file') ~= 2
                    error('GIDVis:qData', 'Unable to read file %s: No such file or directory.', FilePath);
                end
                [~, ~, Ext] = fileparts(FilePath);
                if ~strcmpi(Ext, '.GIDDat')
                    error('GIDVis:qData', 'Unable to read file %s: Unsupported Extension.', FilePath);
                end
                load(FilePath, '-mat', 'obj');
                if iscell(obj.RawDataFileNames)
                    FilePathsRawData = LFMElement.empty(0, 1);
                    for iF = 1:numel(obj.RawDataFileNames)
                        FilePathsRawData(iF) = LFMElement;
                        FilePathsRawData(iF).Path = obj.RawDataFileNames{iF};
                    end
                    obj.RawDataFileNames = FilePathsRawData;
                end
            elseif nargin == 3
                % extract the input values from varargin
                if isa(varargin{1}, 'cell')
                    FilePathsRawData = LFMElement.empty(0, 1);
                    In1 = varargin{1};
                    for iF = 1:numel(varargin{1})
                        FilePathsRawData(iF) = LFMElement;
                        FilePathsRawData(iF).Path = In1{iF};
                    end
                elseif isa(varargin{1}, 'LFMElement')
                    FilePathsRawData = varargin{1};
                end
                Beamtime = varargin{2};
                Assignment = varargin{3};
                
                obj.RawDataFileNames = FilePathsRawData;
                obj.RawPixelData = cell(1, numel(FilePathsRawData));
                for iF = 1:numel(FilePathsRawData)
                    obj.RawPixelData{iF} = ReadDataFromFile(FilePathsRawData(iF));
                end
                
                % assign other properties to obj
                obj.Assignment = Assignment;
                obj.Beamtime = Beamtime;
            else
                error('GIDVis:qData', 'Number of input arguments not correct.');
            end
        end
        
        function Path = SaveqData(obj, Path) %#ok Schrode
            % Saves the qData object in the file given by path.
            % If the extension for the file is different from .GIDDat, it
            % gets set to .GIDDat issuing a warning.
            % The output argument returns the final path.
            %
            % Usage:
            % 
            % SaveqData(obj, Path)
            % obj.SaveqData(Path)
            [Dir, Name, Ext] = fileparts(Path);
            if ~strcmp(Ext, '.GIDDat')
                warning('GIDVis:qData', 'Setting file extension to ''GIDDat''.');
            end
            Path = fullfile(Dir, [Name, '.GIDDat']);
            save(Path, 'obj');
        end
        
        function Path = SaveqDataWithUI(obj, DefaultPath)
            % Saves the qData object in a user selected file.
            % The output argument returns the path to the file saved.
            %
            % Usage:
            % 
            % SaveqDataWithUI(obj)
            % obj.SaveqDataWithUI()
            % SaveqDataWithUI(obj, DefaultPath)
            % obj.SaveqDataWithUI(DefaultPath)
            
            if nargin < 2 || isempty(DefaultPath)
                DefaultPath = '';
            else
                [Dir, Name, Ext] = fileparts(DefaultPath);
                if ~strcmp(Ext, '.GIDDat')
                    warning('GIDVis:qData', 'Setting file extension to ''GIDDat''.');
                    DefaultPath = fullfile(Dir, [Name, '.GIDDat']);
                end
            end
            [FileName, PathName] = uiputfile({'*.GIDDat', 'GIDVis Data File (*.GIDDat)'}, 'Save as', DefaultPath);
            if isequal(FileName, 0) || isequal(PathName, 0)
                Path = '';
                return;
            end
            Path = fullfile(PathName, FileName);
            SaveqData(obj, Path);
        end
        
        function Str = DataInformation(obj)
            % Creates a string containing information about the qData.
            % object.
            %
            % Usage:
            %
            % DataInformation(obj)
            % obj.DataInformation()
            S = obj.Beamtime.SetUps(obj.Assignment);
            NamesOrdered = {S.name};

            Str = sprintf('This file''s data has been created from the following files:\n%s\n\n', [sprintf('%s\n', obj.RawDataFileNames(1:end-1).DisplayName), obj.RawDataFileNames(end).DisplayName]);
            Str = [Str, sprintf('The beamtime used during creation is: %s.\n', obj.Beamtime.Name)];
            Str = [Str, sprintf('The experimental settings used during creation are, in the order of the above mentioned files:\n%s', [sprintf('%s\n', NamesOrdered{1:end-1}), NamesOrdered{end}])];
            if ~all(isnan(obj.AdditionalPixels)) && ~all(obj.AdditionalPixels == 0)
                Str = [Str, sprintf('\n\nFurther parameters:\nAdditionalPixels (l, t, r, b) = [%g, %g, %g, %g].', obj.AdditionalPixels)];
            end
        end
    end
    
    methods (Access = private, Static)
        function Data = RemoveDetectorGaps(Data)
            % find columns and rows full of intensity values < 0
            L1 = all(Data <= 0, 1);
            L2 = all(Data <= 0, 2);
            % set these to NaN
            Data(:, L1) = NaN;
            Data(L2, :) = NaN;
        end
    end
end