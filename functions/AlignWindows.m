function AlignWindows(handles, LayoutNumber)
% This function aligns the windows one of the following manners, depending 
% on the second input argument to this function (either 1 or 2), or, if 
% this is empty or invalid, based on the screen heihgt:
% 
% Case 1 (large screen height):
% +-----------------+-------+
% |                 | Tool- |
% |                 | box   |
% |  Main Window    |-------|
% |                 | Load  |
% |                 | Files |
% +-----------------+-------+
%
% Case 2 (lower screen height):
% +-----------------+-------+-------+
% |                 |       |       |
% |                 | Load  | Tool- |
% |  Main Window    | Files | box   |
% |                 |       |       |
% |                 |       |       |
% +-----------------+-------+-------+
%
% Case 1 (large screen height):
% The width of the Load File Module window stays the same. The Toolbox
% Module window gets the same width as the Load File Module window. The
% height of the Load Files Window is adapted so that it fills the remaining
% height of the screen. The Main Windows height and width is changed so
% that it fills the rest of the screen.
%
% Case 2 (lower screen height):
% The widths of the Load File Module and Toolbox Module window stay the
% same. The heights of the Load File Module and Toolbox Module window are
% adapted so that they fill the height of the screen. The Main Windows
% height and width is changed so that it fills the rest of the screen.
%
% Usage:
%     AlignWindows(handles, LayoutNumber)
%
% Input:
%     handles ... handles of the main GIDVis window
%     LayoutNumber ... Number defining the layout (1 or 2). If not given,
%                      it is defaulted depending on the screen height.
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2018 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

HeightForCase2 = 1150;

% get the screen size in pixels. Get and restore the previous units used.
Old0Units = get(0, 'Units');
set(0, 'Units', 'pixels');
ScreenSize = get(0, 'ScreenSize');
set(0, 'Units', Old0Units);

% decide which layout to use
if nargin < 2 || isempty(LayoutNumber) || ~any([1 2] == LayoutNumber)
    if ScreenSize(4) > HeightForCase2
        LayoutNumber = 1;
    else
        LayoutNumber = 2;
    end
end

% Store the old units of the main window and set the units to pixels.
OldMainWinUnits = get(handles.Figure_GIDVis, 'Units');
set(handles.Figure_GIDVis, 'Units', 'pixels');

% store old units of the toolbox window and set the units to pixels.
OldToolboxUnits = get(handles.ToolboxModule, 'Units');
set(handles.ToolboxModule, 'Units', 'pixels');

% store the old units of the load file module and set the new units to
% pixels
OldLoadFileModuleUnits = get(handles.LoadFileModule, 'Units');
set(handles.LoadFileModule, 'Units', 'pixels');

TaskbarHeight = 40; % on Windows 7
% get the position and size of the load file and toolbox window
LoadFileOuterPos = get(handles.LoadFileModule, 'OuterPosition');
ToolboxOuterPos = get(handles.ToolboxModule, 'OuterPosition');
    
if LayoutNumber == 1
    % set the width of the toolbox window to the width of the load file
    % module window
    ToolboxOuterPos(3) = LoadFileOuterPos(3);
    % set the Toolbox module's new position
    set(handles.ToolboxModule, 'OuterPosition', ToolboxOuterPos);
    % move the toolbox module window to the upper right corner
    movegui(handles.ToolboxModule, 'northeast');
    % get the updated outer position of the toolbox module
    ToolboxOuterPos = get(handles.ToolboxModule, 'OuterPosition');
    % set the outer position of the load file module window
    set(handles.LoadFileModule, 'OuterPosition', [ToolboxOuterPos(1) TaskbarHeight ToolboxOuterPos(3) ScreenSize(4)-TaskbarHeight-ToolboxOuterPos(4)]);
    % set the OuterPosition of the Main Window
    set(handles.Figure_GIDVis, 'OuterPosition', [0 TaskbarHeight ScreenSize(3)-ToolboxOuterPos(3) ScreenSize(4)-TaskbarHeight]);
elseif LayoutNumber == 2
    % reset the width of the toolbox
    ToolboxOuterPos(3) = 246;
    set(handles.ToolboxModule, 'OuterPosition', ToolboxOuterPos);
    % move the toolbox module window to the upper right corner
    movegui(handles.ToolboxModule, 'northeast');
    % get the updated position and size of the toolbox module window
    ToolboxOuterPos = get(handles.ToolboxModule, 'OuterPosition');
    % set the outer position of the load file module window
    set(handles.LoadFileModule, 'OuterPosition', [ToolboxOuterPos(1)-LoadFileOuterPos(3) TaskbarHeight LoadFileOuterPos(3) ScreenSize(4)-TaskbarHeight]);
    % set the OuterPosition of the Main Window
    set(handles.Figure_GIDVis, 'OuterPosition', [0 TaskbarHeight ScreenSize(3)-ToolboxOuterPos(3)-LoadFileOuterPos(3) ScreenSize(4)-TaskbarHeight]);
end

% restore the units of the windows
set(handles.Figure_GIDVis, 'Units', OldMainWinUnits);
set(handles.LoadFileModule, 'Units', OldLoadFileModuleUnits);
set(handles.ToolboxModule, 'Units', OldToolboxUnits);
% make each window the current figure once, ending with the main window.
% This brings the windows to the front.
figure(handles.ToolboxModule);
figure(handles.LoadFileModule);
figure(handles.Figure_GIDVis);

% call the resize function of the Load File Module window
LoadFileModule('Fig_LoadFileModule_ResizeFcn', handles.LoadFileModule, [], guidata(handles.LoadFileModule));