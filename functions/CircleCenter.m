function [C, r] = CircleCenter(x, y)
% Calculates for at least three points the circle center and the radius.
%
% Usage:
%     [C, r] = CircleCenter(x, y)
%
% Input:
%     x ... x coordinates
%     y ... y coordinates
%
% Output:
%     C ... 1 x 2 matrix with the center of the circle
%     r ... radius of the circle
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if numel(x) ~= numel(y)
    error('x and y need to have the same number of elements.')
end

if numel(x) < 3
    error('x and y have to contain at least three data points.')
end

P = [x(:) y(:)];

d = -sum(P.^2, 2);

M = [ones(size(P, 1), 1) -P];

S = M\d;

C = [S(2)/2 S(3)/2];
r = sqrt(sum(C.^2) - S(1));