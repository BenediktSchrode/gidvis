function [Center, Radius] = CircleSelection(PixelIntensity)
% Interactive selection of points on a circle. Returns the center of the
% circle.
%
% Usage:
%     Center = CircleSelection(Intensity)
%
% Input:
%     Intensity ... Matrix
%
% Output:
%     Center ... Center of the circle.
%     Radius ... Radius of the circle.
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2018 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% create a figure with an axes where the intensity is plotted
f = figure('WindowStyle', 'modal');
ax1 = subplot(1, 2, 1);
imagesc(ax1, PixelIntensity)
caxis(ax1, getColorBarLimits(PixelIntensity))
title(ax1, {'Click on at least three points of one diffraction ring and', 'close the window when you''re done.'})
% create a second axes to show a magnified portion of the image
ax2 = subplot(1, 2, 2);
imagesc(ax2, PixelIntensity);
caxis(ax2, getColorBarLimits(PixelIntensity))
set(ax2, 'XTick', [], 'YTick', [])
% set the positions of the axes
set(ax1, 'Position', [0.13 0.11 0.775 0.80]);
set(ax2, 'Position', [0.7 0.66 0.25 0.25]);
% initialize a line for the magnified axes to show the center
Cross = line('XData', [], 'YData', [], 'Parent', ax2);
% initialize lines where the clicked positions will be plotted
DataPoints1 = line('XData', [], 'YData', [], 'Parent', ax1, ...
    'Marker', 'x', 'LineStyle', 'none', 'MarkerSize', 10);
DataPoints2 = line('XData', [], 'YData', [], 'Parent', ax2, ...
    'Marker', 'x', 'LineStyle', 'none', 'MarkerSize', 10);
% initialize a line for the circle plot
lhCircle1 = line('XData', [], 'YData', [], 'Color', 'w', ...
    'HitTest', 'off', 'Parent', ax1);
lhCircle2 = line('XData', [], 'YData', [], 'Color', 'k', 'LineStyle', '--', ...
    'HitTest', 'off', 'Parent', ax1);

% set the callbacks
set(f, 'WindowButtonMotionFcn', @(src, evt) MotionFcn_Callback(src, evt, ax1, ax2, Cross, PixelIntensity))
set(f, 'WindowButtonDownFcn', @(src, evt) ButtonDownFcn_Callback(src, evt, DataPoints1, DataPoints2, ax1, lhCircle1, lhCircle2));
set(f, 'CloseRequestFcn', @(src, evt) FigureCloseRequest_Callback(src, evt, DataPoints1))

% block code execution until this window is closed. Required to return the
% output argument at the correct time (when closing window).
uiwait(f);

function FigureCloseRequest_Callback(src, evt, DP1)
% evaluated when user tries to close window.
X = get(DP1, 'XData');
% check if at least three data points where selected
if numel(X) < 3
    msgbox('Please select at least three points.', 'modal')
    return;
end
% calculate center of circle with x and y data of clicked points
Y = get(DP1, 'YData');
[Center, Radius] = CircleCenter(X, Y);
% close figure. This resumes code execution blocked by uiwait before.
delete(src);
end

function ButtonDownFcn_Callback(src, evt, DP1, DP2, ax1, lhCircle1, lhCircle2)
% user clicked in the axes.
% get the CurrentPoint, i.e. the position the user clicked onto
P = get(ax1, 'CurrentPoint');
% get old data points
XData = get(DP1, 'XData');
YData = get(DP1, 'YData');
% concatenate the old data points and the new one
XData = [XData, P(1, 1)];
YData = [YData, P(1, 2)];
% update the line
set([DP1 DP2], 'XData', XData, 'YData', YData);

% plot circle if there are enough data points clicked already
if numel(XData) >= 3
    phi = linspace(0, 2*pi, 360);
    [C, r] = CircleCenter(XData, YData);
    x = r.*cos(phi) + C(1);
    y = r.*sin(phi) + C(2);
    set([lhCircle1, lhCircle2], 'XData', x, 'YData', y);
end
end

function MotionFcn_Callback(src, evt, ax1, ax2, Cross, PixelIntensity)
% user is moving the mouse over the figure. Update the zoomed portion of
% the image

% get current point
C = get(ax1, 'CurrentPoint');
% get axes limits
XL1 = xlim(ax1);
YL1 = ylim(ax1);
% check if mouse is in axes system
if C(1, 1) < XL1(1) || C(1, 1) > XL1(2) || C(1, 2) < YL1(1) || C(1, 2) > YL1(2)
    return;
end
% factor: the x and y axis limits of the zoomed image will be 2*F percent
% of the axes limits of the unzoomed image
F = 0.05;
% calculate new axes limits
XL2 = sort([C(1, 1)-F*(XL1(2)-XL1(1)) C(1, 1)+F*(XL1(2)-XL1(1))]);
YL2 = sort([C(1, 2)-F*(YL1(2)-YL1(1)) C(1, 2)+F*(YL1(2)-YL1(1))]);
% set axes limits
xlim(ax2, XL2);
ylim(ax2, YL2);
% update the crosshair in the zoomed plot
LX = [diff(XL2)/2+XL2(1) diff(XL2)/2+XL2(1) NaN XL2];
LY = [YL2 NaN diff(YL2)/2+YL2(1) diff(YL2)/2+YL2(1)];
set(Cross, 'XData', LX, 'YData', LY);
% get the data inside the zoomed axes
try
D = PixelIntensity( ...
    max([floor(YL2(1)) 1]):min([ceil(YL2(2)) size(PixelIntensity, 1)]), ...
    max([floor(XL2(1)) 1]):min([ceil(XL2(2)) size(PixelIntensity, 2)]));
catch 
    a = 5;
end
% set the color limits accordingly
caxis(ax2, getColorBarLimits(D));
end
end