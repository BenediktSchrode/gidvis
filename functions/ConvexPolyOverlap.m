function Out = ConvexPolyOverlap(Vertices1, Vertices2, Visualize)
% ConvexPolyOverlap returns the intersection points of two convex polygons.
%
% Usage:
%     Out = ConvexPolyOverlap(Vertices1, Vertices2, Visualize);
%
% Input:
%     Vertices1 ... the n vertex points of the first polygon as a n x 2 
%                   matrix (the polygon should not be closed, i.e. the last 
%                   data point should not be the same as the first)
%     Vertices2 ... the m vertex points of the first polygon as a m x 2 
%                   matrix (the polygon should not be closed, i.e. the last 
%                   data point should not be the same as the first)
%     Visualize ... Boolean whether the input polygons and the output
%                   should be visualized in a figure
%
% Output:
%     Out ... the s intersection points of the two polygons as s x 2
%             matrix. Empty if there are no intersections.
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin < 3 || isempty(Visualize); Visualize = true; end
% check for identical Polygons
if all(size(Vertices1) == size(Vertices2)) && all(Vertices1(:) == Vertices2(:))
    if Visualize; PlotIntersection([Vertices1; Vertices1(1, :)], Vertices1, Vertices2); end
    Out = SortPolyPoints(Vertices1);
    Out = [Out; Out(1, :)];
    return;
end

% check for points of Vertices1 lying in Vertices2
IsInPolygon1 = inpolygon(Vertices1(:, 1), Vertices1(:, 2), Vertices2(:, 1), Vertices2(:, 2));

% check for points of Vertices2 lying in Vertices1
IsInPolygon2 = inpolygon(Vertices2(:, 1), Vertices2(:, 2), Vertices1(:, 1), Vertices1(:, 2));

if all(IsInPolygon2)
    if Visualize; PlotIntersection([Vertices2; Vertices2(1, :)], Vertices1, Vertices2); end
    Out = SortPolyPoints(Vertices2);
    Out = [Out; Out(1, :)];
    return;
end

% close polygons by adding first point at end of vertices vector
Vertices1C = [Vertices1; Vertices1(1, :)];
Vertices2C = [Vertices2; Vertices2(1, :)];

% find intersection points of the polygons
[xi, yi] = intersections(Vertices1C(:, 1), Vertices1C(:, 2), ...
    Vertices2C(:, 1), Vertices2C(:, 2), true, false);
Points = [xi(:), yi(:)];

% add vertices which are contained in one polygon
Points = [Points; Vertices1(IsInPolygon1, :); Vertices2(IsInPolygon2, :)];

% remove duplicates
Points = unique(Points, 'rows');

if ~isempty(Points)
    FinalPoints = SortPolyPoints(Points);
else
    FinalPoints = Points;
end

if ~isempty(FinalPoints) && (~all(FinalPoints(1, :) == FinalPoints(end, :)))
    FinalPoints = [FinalPoints; FinalPoints(1, :)];
end

if Visualize; PlotIntersection(FinalPoints, Vertices1, Vertices2); end

Out = FinalPoints;

function FinalPoints = SortPolyPoints(Points)
CenterX = mean(Points(:, 1));
CenterY = mean(Points(:, 2));
Angles = atan2(Points(:, 2)-CenterY, Points(:, 1)-CenterX);
[~, ind] = sort(Angles);
FinalPoints = Points(ind, :);

function PlotIntersection(Points, Polygon1, Polygon2)

figure
fill(Points(:, 1), Points(:, 2), 'y')
hold on
plot([Polygon1(:, 1); Polygon1(1, 1)], [Polygon1(:, 2); Polygon1(1, 2)], 'r');
plot([Polygon2(:, 1); Polygon2(1, 1)], [Polygon2(:, 2); Polygon2(1, 2)], 'g');
plot(Points(:, 1), Points(:, 2), '*')
if ~isempty(Points)
    text(Points(1, 1), Points(1, 2), ['1, ', num2str(size(Points, 1))]);
    for iP = 2:size(Points, 1)-1
        text(Points(iP, 1), Points(iP, 2), num2str(iP));
    end
    if size(Points, 1) > 1
        plot(mean(Points(1:end-1, 1)), mean(Points(1:end-1, 2)), 'bo', 'MarkerFaceColor', 'b')
    else
        plot(Points(1), Points(2), 'bo', 'MarkerFaceColor', 'b')
    end
end
if isempty(Points)
    legend('Polygon 1', 'Polygon 2');
else
    legend('Overlap', 'Polygon 1', 'Polygon 2', 'Bounding Points', 'Mean')
end