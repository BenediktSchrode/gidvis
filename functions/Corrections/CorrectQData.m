function [DataCorrected, LC, PC, SAC, SDDC, DEC] = CorrectQData(ToolboxElement, QData)
% This function corrects the QData according to the intensity correction
% factors calculated by CorrectionsFactors.m.
%
% The corrections to apply and other necessary parameters such as the
% incident angle and the sample tilt are directly obtained from the toolbox
% module.
%
% Usage:
%     [DataCorrected, LC, PC, SAC, SDDC, DEC] = CorrectQData(ToolboxElement, QData)
%
% Input:
%     ToolboxElement ... Any gui element of the toolbox module.
%     QData ... The QData object to apply the corrections to.
%
% Output:
%     DataCorrected ... the corrected data.
%     LC, PC, SAC, SDDC, DEC ... the correction factors.
%
% This file is part of GIDVis.
%
% see also: CorrectionFactors, qData, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2018 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% initialize the correction factor matrizes
LC = 1;
PC = 1;
SAC = 1;
SDDC = 1;
DEC = 1;

% get the handles structure of the toolbox module
TBx = guidata(ToolboxElement);

% get omega
omega = ToolboxModule('GetOmega', ToolboxElement);

% get chi
chi = str2double(get(TBx.TB_chi, 'String'));

% get the value of the check boxes
Lorentz = get(TBx.CB_LorentzCorrection, 'Value');
Polarization = get(TBx.CB_PolarizationCorrection, 'Value');
SolidAngle = get(TBx.CB_SolidAngleCorrection, 'Value');
PixelDistance = get(TBx.CB_PixelDistanceCorrection, 'Value');
DetectorEff = get(TBx.CB_DetectorEfficiencyCorrection, 'Value');
FlatField = get(TBx.CB_FlatFieldCorrection, 'Value');

% no corrections selected, corrected data is raw data
if ~any([Lorentz, Polarization, SolidAngle, PixelDistance, DetectorEff, FlatField])
    DataCorrected = QData.IntensityCropped;
    return;
end

DataCorrected = [];

% calculate the correction factors
if any([Lorentz, Polarization, SolidAngle, PixelDistance, DetectorEff, FlatField])
    for iF = 1:numel(QData.RawPixelData)
        DataCorrected0 = QData.IntensityCropped(iF);
        ExpSetUp = QData.Beamtime.SetUps(QData.Assignment(iF));
        if any([Lorentz, Polarization, SolidAngle, PixelDistance, DetectorEff])
            [LC, PC, SAC, SDDC, DEC] = CorrectionFactors(ExpSetUp, omega, ...
                chi);
            % apply the corrections depending on the checkbox values
            if Lorentz; DataCorrected0 = DataCorrected0./LC(:); end
            if Polarization; DataCorrected0 = DataCorrected0./PC(:); end
            if SolidAngle; DataCorrected0 = DataCorrected0.*SAC(:); end
            if PixelDistance; DataCorrected0 = DataCorrected0.*SDDC(:); end
            if DetectorEff; DataCorrected0 = DataCorrected0.*DEC(:); end
        end
        if FlatField
            if isempty(ExpSetUp.FFImageData)
                warning('GIDVis:CorrectPixelData', ['No flat field image for experimental set up ', ExpSetUp.name, '.']);
            else
                FFImg = ExpSetUp.FFImageData(1:ExpSetUp.detlenz, 1:ExpSetUp.detlenx);
                DataCorrected0 = DataCorrected0.*FFImg(:);
            end
        end
        DataCorrected = [DataCorrected; DataCorrected0];
    end
end