function [LC, PC, SAC, SDDC, DEC] = CorrectionFactors(BL, omega, chi, x, y)
% CORRECTIONFACTORS calculates correction factors for grazing incidence
% diffraction using an area detector.
%
% Usage:
% [LC, PC, SAC, SDDC, DEC] = CorrectionFactors(BL, omega, chi)
%
% Input:
% BL ... ExperimentalSetUp
% omega ... rotation around omega (incident angle) (degree)
% chi ... sample tilt around beam axis (degree)
% x ... pixel position x (optional)
% y ... pixel position y (optional)
%
% Output:
% LC ... Lorentz correction. Measured intensity has to be divided by LC.
% PC ... Polarization correction. Measured intensity has to be divided by
% PC.
% SAC ... Solid angle correction. Measured intensity has to be multiplied
% with SAC. See Jiang, J. Appl. Cryst. (2015). 48, 917-926, for a
% description.
% SDDC ... Correction factor for different sample-detector distances for
% each individual pixel and therefore weakened intensity for pixels further
% from the center pixel. Measured intensity has to be multiplied with SDDC.
% See Jiang, J. Appl. Cryst. (2015). 48, 917-926, for a more detailed
% description.
% DEC ... Correction factor for different detector efficiencies resulting
% in a longer path length for the beam in pixels further away from the
% center pixel and therefore a larger probability of absorption and
% detection. Measured intensity has to be multiplied with DEC.
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2018 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% calculate the outgoing wave vectors kout
if nargin < 4
    kout = getDetectorPixel(BL);
else
    kout = getDetectorPixel(BL, x, y);
end

% calculate the q vectors from kout
[q_xy, q_z] = getQ(BL, kout, omega, chi);

% transform omega to radians
omega = omega*pi/180;

% normalize kout
koutLength = sqrt(kout(:, 1).^2+kout(:, 2).^2+kout(:, 3).^2);
koutnormalized = (kout./repmat(koutLength, 1, 3))';

% define some constants
r_e = 2.8179403227e-15; % classical electron radius
N_A = 6.022140857e23; % Avogadro constant
PlanckConstant = 6.62607004e-34;
ElementaryCharge = 1.6021766208e-19;
SpeedOfLight = 299792458;

% calculate the energy corresponding to the wavelength
Energy = PlanckConstant*SpeedOfLight/(BL.lambda*10^(-10))/ElementaryCharge;

% from the definition of q_z (Pichler, eq. 1), the angle alpha_f can be
% calculated:
alpha_f = asin(q_z.*BL.lambda./(2*pi) - sin(omega));

% from q_xy = sqrt(q_x^2 + q_y^2) (Pichler, eq. 2), using the definitions
% for q_x and q_y (Pichler eq. 1) and the calculated value of alpha_f, the
% angle theta_f can be calculated:
theta_f = acos((cos(alpha_f).^2 + cos(omega).^2 - BL.lambda^2.*q_xy.^2./(4*pi^2))./(2.*cos(alpha_f).*cos(omega)));

% for q_xy/q_z values which are not accessible in GIXD (cf. Moser, Fig.
% 2.11), theta_f becomes complex. Set these values to NaN.
alpha_f(imag(theta_f) ~= 0) = NaN;
theta_f(imag(theta_f) ~= 0) = NaN;

%% Calculation of the Lorentz factor (Moser, eq. 2.34)
LC = 1./(cos(omega).*sin(theta_f).*cos(alpha_f));
if nargin < 4
    LC = reshape(LC, BL.detlenz, BL.detlenx);
end

%% Calculation of the Polarization factor (Smilgies, eq. 6)
% horizontal polarization, i. e. the vector of the polarization is pointing
% in the x direction
HorizPol = [1 0 0]';
% evaluate the cross product of the polarization vector and kout
PCK = cross(repmat(HorizPol, 1, size(koutnormalized, 2)), koutnormalized);
% calculate the squared norm of the cross product
HPC = sum(PCK.^2)';

% vertical polarization, i. e. the vector of the polarization is pointing
% in the z direction
VertPol = [0 0 1]';
% evaluate the cross product of the polarization vector and kout
PCK = cross(repmat(VertPol, 1, size(koutnormalized, 2)), koutnormalized);
% calculate the squared norm of the cross product
VPC = sum(PCK.^2)';

% calculate total correction factor by linear combination of the horizontal
% and the vertical contribution
PC = BL.HFraction.*HPC + (1-BL.HFraction).*VPC;

if nargin < 4
    PC = reshape(PC, BL.detlenz, BL.detlenx);
end

%% Calculation of the correction factor for different sample-pixel distances (GIXSGui doc)

if isempty(BL.Medium.Formula) || isempty(BL.Medium.Density)
    SDDC = [];
else
    % calculate the absorption properties of the path medium
    P = ImASF(BL.Medium.Formula, Energy);

    if isempty(fields(P))
        SDDC = [];
    else
        % calculate the attenuation
        A = (BL.lambda*10^(-10))^2/2/pi*r_e*N_A*BL.Medium.Density/(sum([P.AtomicWeight].*[P.N])).*[P.N]*[P.f2]'*1e6;
        % calculate the attenuation length
        AttL = (BL.lambda*10^(-10))./A/(4*pi)*100;
        % convert from cm to mm
        AttL = AttL*10;
        % calculate the correction factor
        SDDC = 1./(exp(-koutLength./AttL));
        if nargin < 4
            SDDC = reshape(SDDC, BL.detlenz, BL.detlenx);
        end
    end
end

%% Calculation of the Detector Efficiency Correction Factor

Rx = @(ai) [1 0 0; 0 cosd(ai) -sind(ai); 0 sind(ai) cosd(ai)];
Ry = @(ai) [cosd(ai) 0 sind(ai); 0 1 0; -sind(ai) 0 cosd(ai)]; 
Rz = @(ai) [cosd(ai) -sind(ai) 0; sind(ai) cosd(ai) 0; 0 0 1];

% unit vector of the detector normal
n = [0 1 0];

% rotate the unit vector of the detector normal around the x, y, and z axes
% by the angles from the ExperimentalSetUp
rot_x = Rx(BL.rx);
rot_y = Ry(BL.ry);
rot_z = Rz(BL.rz);
n = (rot_z*rot_x*rot_y*n');

M = kout';

% calculate the angles between the rotated unit vector of the detector and
% the kout vectors. They have to be used to calculate the different path
% lengths in the detector instead of simply the two theta angle.
delta = acosd(dot(repmat(n, 1, size(M, 2)), M)./koutLength')';

if isempty(BL.DetectorMaterial.Formula) || ...
        isempty(BL.DetectorMaterial.Density) || ...
        isempty(BL.DetectorMaterial.Thickness)
    DEC = [];
else    % calculate absorption properties of the detector material
    P = ImASF(BL.DetectorMaterial.Formula, Energy);

    if isempty(fields(P))
        DEC = [];
    else
        % calculate attenuation
        A = (BL.lambda*10^(-10))^2/2/pi*r_e*N_A*BL.DetectorMaterial.Density/(sum([P.AtomicWeight].*[P.N])).*[P.N]*[P.f2]'*1e6;
        % calculate attenuation length
        AttL = (BL.lambda*10^(-10))./A/(4*pi)*100;
        % convert from cm to mm
        AttL = AttL*10;
        % calculate the correction factor
        DEC = 1./(1-exp(-BL.DetectorMaterial.Thickness./(cosd(delta))./AttL));
        if nargin < 4
            DEC = reshape(DEC, BL.detlenz, BL.detlenx);
        end
    end
end

%% Calculation of the solid angle correction factor (GIXSGUI doc)
% calculate the solid angle with the real angle between the detector and
% the kout vectors
DeltaOmegai = BL.psx*BL.psz*cosd(delta)./koutLength.^2;
% normalize it to the maximum
DeltaOmega0 = max(DeltaOmegai(:));

% calculate correction factor (has to be multiplied)
SAC = DeltaOmega0./DeltaOmegai;
if nargin < 4
    SAC = reshape(SAC, BL.detlenz, BL.detlenx);
end