function FPC = IlluminatedArea(alpha_i, SampleWidth, SampleLength, phi, BH, BW)
% Calculation of the illuminated area of the sample
%
% Usage:
%     FPC = IlluminatedArea(alpha_i, SampleWidth, SampleLength, phi, BL, BW)
%
% Input:
%     alpha_i ... incident angle
%     SampleWidth ... size of the sample perpendicular to the beam for phi
%     = 0 degree
%     SampleLength ... size of the sample parallel to the beam for phi = 0
%                      degree
%     phi ... rotation angle around the surface normal. Can be a vector.
%     BH ... Beam height
%     BW ... Beam width
%
% Output:
%     FPC ... illuminated area. If phi is a vector, FPC will be a vector as
%             well
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2020 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Size of the footprint of the incoming beam (Moser, eq. 2.36)
BeamFP_L = BH/sind(alpha_i); % length
BeamFP_W = BW; % width

% rectangle defining the beam footprint
RBeam = [-BeamFP_W/2 -BeamFP_L/2 BeamFP_W BeamFP_L];

Beamxy = [RBeam(1) RBeam(2); ...
    RBeam(1)+RBeam(3) RBeam(2); ...
    RBeam(1)+RBeam(3) RBeam(2)+RBeam(4); ...
    RBeam(1) RBeam(2)+RBeam(4)];

% lines defining the sample
xS = [-SampleWidth/2 SampleWidth/2; SampleWidth/2 SampleWidth/2; ...
    SampleWidth/2 -SampleWidth/2; -SampleWidth/2 -SampleWidth/2];
yS = [-SampleLength/2 -SampleLength/2; -SampleLength/2 SampleLength/2; ...
    SampleLength/2 SampleLength/2; SampleLength/2 -SampleLength/2];

% 2d rotation matrix
Rot = @(alpha) [cosd(alpha) -sind(alpha); sind(alpha) cosd(alpha)];

% initialize result
FPC = NaN(size(phi));
% loop over all requested phi values
for iPhi = 1:numel(phi)
    % vertices of the sample
    xy = [xS(:) yS(:)];
    % rotate the sample around phi
    xyR = Rot(phi(iPhi))*xy';
    % reshape the result
    xR = reshape(xyR(1, :), 4, 2);
    yR = reshape(xyR(2, :), 4, 2);
    % check if vertices of sample are in the beam
    L1 = (xR(:) >= RBeam(1) & xR(:) <= RBeam(1)+RBeam(3)) & ...
        (yR(:) >= RBeam(2) & yR(:) <= RBeam(2)+RBeam(4));
    % check if vertices of the beam are in the sample
    L2 = inpolygon(Beamxy(:, 1), Beamxy(:, 2), [xyR(1, :) xyR(1, 1)], [xyR(2, :) xyR(2, 1)]);
    if all(L1) % sample is completely in the beam
        FPC(iPhi) = SampleW*SampleL; % footprint is the sample size
    elseif all(L2) % beam is completely in the sample
        FPC(iPhi) = BeamFP_L*BeamFP_W; % footprint is the beam size
    else % parts of the sample are outside the beam
        % calculate intersection points of the sample lines and the beam
        % rectangle
        [xn, yn] = LineClipping_LB(RBeam, xR, yR);
        % since we only checked if all the beam vertices are in the sample,
        % and did not cover the case that only two are in, add beam
        % vertices which are in the sample to the xn, yn
        xn = [xn; Beamxy(L2, 1)];
        yn = [yn; Beamxy(L2, 2)];
        % calculate the azimuthal angle of the intersection points: since
        % we don't know the exact order of the vertex points (the polygon
        % described by the xn, yn currently might be self-intersecting), we
        % have to sort them. Do a sorting by angle.
        Angles = atan2(yn, xn);
        [~, ind] = sort(Angles);
        xn = xn(ind);
        yn = yn(ind);
        % calculate the polygon area (size of the footprint on the sample)
        FPC(iPhi) = polygonArea([xn yn]);
    end
end