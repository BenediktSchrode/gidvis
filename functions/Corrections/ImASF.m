function Props = ImASF(Formula, Energy, Method)
% Calculates the refraction properties of a material when interacting with
% an X-ray beam of Energy.
%
% Code is based on the MATLAB Toolbox: X-ray Refraction of Matter, function
% refrac by Zhang Jiang (https://de.mathworks.com/matlabcentral/fileexchange/6026-toolbox--x-ray-refraction-of-matter)
% but is heavily adapted for the needs of GIDVis.
% Please see the license text of the refrac software package in
% /functions/ExternalFunctions/refrac/license.txt
%
% The imaginary part of the atomic scattering factors is taken from
% http://henke.lbl.gov/optical_constants/asf.html
%
%
% Usage:
%     Props = ImASF(Formula, Energy, Method)
%
% Input:
%     Formula ... sum formula of the material to calculate, e.g. 'H2O'
%     Energy ... energy of the X-ray beam in eV
%     Method ... method to use to extract the f2 value for an arbitrary
%                energy value from discrete data. Can be any of the methods
%                of MATLAB's interp1 methods.
%
% Output:
%     Props ... struct containg refraction properties
%
% This file is part of GIDVis.
%
% see also: interp1, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if Energy < 10 || Energy > 30000
    error('Energy out of range. It should be between 10 eV and 30000 eV.');
end

if nargin < 3 || isempty(Method)
    Method = 'spline';
end

% extract the elements and the amount of each atom
% tok is a cell of size (1xNumElements), each of this cell contains in {1}
% the element and in {2} the number (number is still a string)
tok = regexp(Formula, '([A-Z][a-z]?)(\d*)', 'tokens');

% initialize result
Props = struct;

% loop over all elements
for n = 1:numel(tok)
    % get the nth element
    C = tok{n};
    % get the number of this element
    num = str2double(C{2});
    % in case there is no number, it should be one (e.g. for the C in CO2)
    if isnan(num); num = 1; end
    % create the element
    El = ChEl(C{1});
    % get f2
    f2Interp = El.f2(Energy, Method);
    % assign to result structure
    Props(n).f2 = f2Interp;
    Props(n).Element = C{1};
    Props(n).N = num;
    Props(n).AtomicWeight = El.Weight;
end
