function [xn, yn, ind] = LineClipping_LB(R, xL, yL)
% calculates intersection points of the lines given by xL and yL with the
% rectangle R defined as (x, y, width, height) using the Liang-Barsky line
% clipping algorithm.
% xL and yL have to be of size (n, 2).
%
% see: https://en.wikipedia.org/wiki/Liang%E2%80%93Barsky_algorithm
%
% Usage:
%     [xn, yn, ind] = LineClipping_LB(R, xL, yL)
% 
% Input:
%     R ... rectangle defined by the four-element vector 
%           [x, y, width, height]
%     xL ... x values of the n lines. Has to be of size n x 2
%     yL ... y values of the n lines. Has to be of size n x 2
%     
% Output:
%     xn ... x values of the intersection points
%     yn ... y values of the intersection points
%     ind ... indices determining from which line the intersection point
%             results
%
% Example:
%     R = [0 0 1 1];
%     x = [-1 2; -2 4];
%     y = [0.5 0.5; 0.3 0.3];
%     [xn, yn, ind] = LineClipping_LB(R, x, y);
%     
%     figure
%     rectangle('Position', R)
%     hold on
%     plot(x', y', 'r')
%     plot(xn, yn, 'ob')
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

p(1, :) = -(xL(:, 2)-xL(:, 1));
p(2, :) = -p(1, :);
p(3, :) = -(yL(:, 2)-yL(:, 1));
p(4, :) = -p(3, :);

q(1, :) = xL(:, 1)-R(1);
q(2, :) = R(1)+R(3)-xL(:, 1);
q(3, :) = yL(:, 1)-R(2);
q(4, :) = R(2)+R(4)-yL(:, 1);

ind = 1:size(q, 2);

% lines which are parallel to one of the boundaries will have p == 0. If
% for this p(i), the corresponding q(i) < 0, the line lies outside of the
% rectangle
LPO = any(p == 0 & q < 0);

% remove lines which are parallel and outside the rectangle
xL(LPO, :) = [];
yL(LPO, :) = [];
p(:, LPO) = [];
q(:, LPO) = [];
ind(:, LPO) = [];

% depending if p is < 0 or larger 0, this means that the line is either
% going from the outside to the inside of the rectangle or from the inside
% to the outside of the rectangle
OutToIn = p < 0;
InToOut = p > 0;

% calculate the intersection point (for parametric form)
u = q./p;

% the intersections have to be divided in the groups going out of and going
% into the rectangle
M = zeros(size(u));
M(OutToIn) = u(OutToIn);
u1 = max(M);
M = ones(size(u));
M(InToOut) = u(InToOut);
u2 = min(M);

% if u1 > u2, the whole line lies outside the rectangle, so no intersection
% points
LO = u1 > u2;

ind(LO) = [];

% calculate the intersection points using parametric form of line
xn1 = xL(~LO, 1)' + p(2, ~LO).*u1(~LO);
xn2 = xL(~LO, 1)' + p(2, ~LO).*u2(~LO);
xn = [xn1; xn2]';

yn1 = yL(~LO, 1)' + p(4, ~LO).*u1(~LO);
yn2 = yL(~LO, 1)' + p(4, ~LO).*u2(~LO);
yn = [yn1; yn2]';

ind = repmat(ind(:), 1, 2);
xn = xn(:);
yn = yn(:);
ind = ind(:);