function area = polygonArea(V)
% polygonArea calculates the area of a non intersecting polygon described
% by its vertices V applying the shoelace formula.
% 
% Usage:
%     area = polygonArea(V)
%
% Input:
%     V ... the vertices of the polygon. X values in first column, y values in
%           second
%
% Output:
%     area ... area of the polygon
%
%
% Example:
% V = [0 2 1 1 0; 0 0 1 3 2]';
% polygonArea(V)
% fill(V(:, 1), V(:, 2), 'r');
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if ~all(V(1, :) == V(end, :))
    V = [V; V(1, :)];
end

area = 0.5*abs(sum(V(1:end-1, 1).*V(2:end, 2)) - sum(V(1:end-1, 2).*V(2:end, 1)));