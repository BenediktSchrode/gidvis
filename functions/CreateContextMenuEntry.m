function h = CreateContextMenuEntry(Parent, Object, Property, DisplayName, Val, varargin)
    % This function creates an uimenu entry in a UIContextMenu defined by
    % Parent. This can be used to change Property of Object, e.g. the color
    % of a line. The DisplayName will be shown as the label in the uimenu
    % entry.
    %
    % Usage:
    %     h = CreateContextMenuEntry(Parent, Object, Property, ...
    %         DisplayName, Val, varargin)
    %
    % Input:
    %     Parent ... the UIContextMenu the uimenu entries should be added
    %                to.
    %     Object ... the element of which a certain property should be
    %                changed
    %     Property ... defines which properties of the object can be
    %                  changed with uimenu entries
    %     DisplayName ... string shown as the uimenu entry's label
    %     Val ... type of the value to change. Has to be 'numeric', 
    %             'color', 'font', 'char', 'onoff' or a cell. 
    %             In case of 'numeric', an inputdlg will be used to ask for 
    %             a new numeric value.
    %             In case of 'color', uisetcolor will be used to define a 
    %             new color. In case of 'font', uisetfont will be used to 
    %             define new font properties.
    %             In case of 'char', an inputdlg will be used to get a new 
    %             string.
    %             If Val is a cell, every row of Val will give a child 
    %             element of the uimenu, i.e. you can give selectable 
    %             choices using a cell for Val.
    %     varargin ... will be passed to the uimenu creation function
    %
    % Output:
    %     h ... handle to the created uimenu
    %
    % Example:
    % This example creates a line l and a uicontextmenu associated with it.
    % The elements of the context menu are created using
    % CreateContextMenuEntry.
    % The first will give an entry to change the color property of the
    % line l.
    % The second give will give an entry to change the LineWidth property
    % of the line l.
    % The third one will give an entry to change the LineStyle property of
    % the line l, using the options -, --, and :. They are shown as Solid,
    % Dashed and Dotted in the context menu.
    %
    % l = line;
    % uim = uicontextmenu;
    % CreateContextMenuEntry(uim, l, 'Color', 'Color...', 'color');
    % CreateContextMenuEntry(uim, l, 'LineWidth', 'Line Width...', 'numeric');
    % CreateContextMenuEntry(uim, l, 'LineStyle', 'Line Style', {'-', 'Solid'; '--', 'Dashed'; ':', 'Dotted'});
    % set(l, 'UIContextMenu', uim);
    %
    %
    % This file is part of GIDVis.
    %
    % see also: uimenu, uicontextmenu, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    if ischar(Val) && strcmp(Val, 'numeric')
        h = uimenu(Parent, 'Label', DisplayName, varargin{:}, ...
            'Callback', @(src, evt) SetNumericValue(Object, Property, DisplayName), ...
            'Tag', ['UIM_', Property]);
    elseif ischar(Val) && strcmp(Val, 'color')
        h = uimenu(Parent, 'Label', DisplayName, varargin{:}, ...
            'Callback', @(src, evt) SetColor(Object, Property, DisplayName), ...
            'Tag', ['UIM_', Property]);
    elseif ischar(Val) && strcmp(Val, 'font')
        h = uimenu(Parent, 'Label', DisplayName, varargin{:}, ...
            'Callback', @(src, evt) SetFont(Object, Property, DisplayName), ...
            'Tag', ['UIM_', Property]);
    elseif ischar(Val) && strcmp(Val, 'char')
        h = uimenu(Parent, 'Label', DisplayName, varargin{:}, ...
            'Callback', @(src, evt) SetString(Object, Property, DisplayName), ...
            'Tag', ['UIM_', Property]);
    elseif ischar(Val) && strcmp(Val, 'onoff')
        h = uimenu(Parent, 'Label', DisplayName, varargin{:}, ...
            'Callback', @(src, evt) SetOnOff(src, Object, Property, DisplayName), ...
            'Tag', ['UIM_', Property]);
    elseif iscell(Val)
        h = uimenu(Parent, 'Label', DisplayName, varargin{:}, ...
            'Callback', {@ParentCntxt_Callback, Object, Property}, ...
            'Tag', ['UIM_', Property]);
        for iV = 1:size(Val, 1)
            uimenu(h, 'Label', Val{iV, 2}, 'Tag', Val{iV, 1}, ...
                'Callback', @(src, evt) SetProperty(Object, Property, Val{iV, 1}));
        end
    end
end

function SetOnOff(src, Object, Property, DisplayName)
    if strcmp(get(src, 'Checked'), 'on')
        set(Object, Property, 'off')
        set(src, 'Checked', 'off')
    else
        set(Object, Property, 'on')
        set(src, 'Checked', 'on')
    end
end

function SetString(Object, Property, DisplayName)
    defAns = get(Object, Property);
    if ~iscell(defAns); defAns = {defAns}; end
    answer = inputdlg(sprintf('Enter new %s:', lower(DisplayName)), ...
        sprintf('New %s', DisplayName), 1, defAns);
    if isempty(answer)
        return;
    end
    set(Object, Property, answer{1});
end

function SetFont(Object, Property, DisplayName)
    NewFont = uisetfont(Object, ['New ', DisplayName]);
    if isequal(NewFont, 0)
        return;
    end
    set(Object, 'Property', NewFont);
end

function SetProperty(Object, Property, Value)
    if iscell(Property) && ~iscell(Value)
        Value = repmat({Value}, size(Property, 1), size(Property, 2));
    end
    set(Object, Property, Value);
end

function ParentCntxt_Callback(src, evt, Object, Property)
    Val = get(Object, Property);
    Ch = get(src, 'Children');
    set(Ch, 'Checked', 'off');
    if iscell(Val); Val = Val{1}; end
    set(findall(Ch, 'Tag', Val), 'Checked', 'on');
end

function SetColor(Object, Property, DisplayName)
    Col = get(Object, Property);
    if ischar(Col)
        Col = [];
    end
    NewCol = uisetcolor(Col, ['New ', DisplayName]);
    if numel(NewCol) == 1 && NewCol == 0
        return;
    end
    set(Object, Property, NewCol);
end

function SetNumericValue(Object, Property, DisplayName)
    if nargin < 3 || isempty(DisplayName)
        if iscell(Property)
            DisplayName = Property{1}; 
        else
            DisplayName = Property;
        end
    end
    Val = get(Object, Property);
    if ~iscell(Val); Val = {Val}; end
    answer = inputdlg(sprintf('Enter new %s:', lower(DisplayName)), ...
        sprintf('New %s', DisplayName), 1, {num2str(Val{1})});
    if isempty(answer) || isnan(str2double(answer{1}))
        return;
    end
    if iscell(Property)
        set(Object, Property, num2cell(repmat(str2double(answer{1}), size(Property, 1), size(Property, 2))));
    else
        set(Object, Property, str2double(answer{1}));
    end
end