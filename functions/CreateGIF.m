function CreateGIF(FilePaths, MainAxesHandle)
% This function creates a GIF animation file.
%
% Usage:
%     CreateGIF(FilePaths, MainAxesHandle)
%
% Input:
%     FilePaths ... cell containing the full paths (i.e. including the 
%                   directory) of the files which should be included in the 
%                   GIF file
%     MainAxesHandle ... handle of the main GIDVis axes handle
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% get file name and directory to save GIF to
[FileName, PathName] = uiputfile({'*.gif', 'GIF File'}, 'Save GIF', fileparts(FilePaths(1).Path));
if isequal(FileName, 0) || isequal(PathName, 0)
    return;
end
SaveFileName = fullfile(PathName, FileName);

prompt = {'Enter the loop count:', 'Enter the delay time:'};
dlg_title = 'Input';
num_lines = 1;
defaultans = {'inf', '0.5'};
answer = inputdlg(prompt, dlg_title, num_lines, defaultans);

if isempty(answer)
    return;
end

LoopCount = str2double(answer{1});
DelayTime = str2double(answer{2});
if isnan(LoopCount) || isnan(DelayTime)
    return;
end

StatusWindow = figure('NumberTitle', 'off', 'Toolbar', 'none', 'MenuBar', 'none', ...
    'Name', 'Creating GIF', 'DockControls', 'off', 'WindowStyle', 'modal', ...
    'Resize', 'off', 'Units', 'pixels', 'Position', [0 0 350 100], 'Visible', 'off', ...
    'CloseRequestFcn', @StatusWindow_CloseRequest, 'UserData', false);
movegui(StatusWindow, 'center');
StatusTxt = uicontrol(StatusWindow, 'Style', 'text', 'Units', 'normalized', ...
    'Position', [0.05 0.55 0.9 0.4], 'String', 'Preparing for GIF creation.', ...
    'FontSize', 10);
CancelBtn = uicontrol(StatusWindow, 'Style', 'pushbutton', 'FontSize', 10, ...
    'Units', 'normalized', 'Position', [0.6 0.2 .35 .25], 'String', 'Cancel', ...
    'Callback', @CancelBtn_Callback, 'UserData', false);
set(StatusWindow, 'Visible', 'on');

MainGIDVisHandle = guidata(MainAxesHandle);

ToolboxHandle = MainGIDVisHandle.ToolboxModule;
GD_TBx = guidata(ToolboxHandle);
LoadFileModuleHandle = MainGIDVisHandle.LoadFileModule;

beamline = LoadFileModule('GetBeamline', LoadFileModuleHandle);
f = ancestor(MainAxesHandle, 'figure');
OldColor = get(f, 'Color');
set(f, 'Color', 'w')

% Calculate the correction factors. They do not change depending on the
% file because the beamline is fixed. Use NaN for the data because we don't
% need it. After loading a file, the correction factors are used on this
% raw intensity data
[~, LC, PC, SAC, SDDC, DEC] = CorrectPixelData(ToolboxHandle, ...
    LoadFileModule('GetBeamline', LoadFileModuleHandle), NaN(beamline.detlenz, beamline.detlenx));

% get the value of the correction factor check boxes
Lorentz = get(GD_TBx.CB_LorentzCorrection, 'Value');
Polarization = get(GD_TBx.CB_PolarizationCorrection, 'Value');
SolidAngle = get(GD_TBx.CB_SolidAngleCorrection, 'Value');
PixelDistance = get(GD_TBx.CB_PixelDistanceCorrection, 'Value');
DetectorEff = get(GD_TBx.CB_DetectorEfficiencyCorrection, 'Value');
FlatField = get(GD_TBx.CB_FlatFieldCorrection, 'Value');

Error = false;

for iF = 1:numel(FilePaths)
    set(StatusTxt, 'String', sprintf('Working on file %d of %d.', iF, numel(FilePaths)));
    if get(CancelBtn, 'UserData')
        break;
    end
    
    try
        data = ReadDataFromFile(FilePaths(iF));
        
        % crop the data if any of the corrections have to be applied
        if any([numel(LC) numel(PC) numel(SAC) numel(SDDC) numel(DEC)] > 1)
            data = data(1:beamline.detlenz, 1:beamline.detlenx);
        end

        % apply the corrections depending on the checkbox values
        if Lorentz; data = data./LC; end
        if Polarization; data = data./PC; end
        if SolidAngle; data = data.*SAC; end
        if PixelDistance; data = data.*SDDC; end
        if DetectorEff; data = data.*DEC; end
        % apply the flat field correction
        if FlatField
            if isempty(ExpSetUp.FFImageData)
                warning('GIDVis:CorrectPixelData', ['No flat field image for experimental set up ', ExpSetUp.name, '.']);
            else
                DataCorrected = DataCorrected.*ExpSetUp.FFImageData(1:ExpSetUp.detlenz, 1:ExpSetUp.detlenx);
            end
        end
        
        PlotImageData(MainAxesHandle, ToolboxHandle, data, LoadFileModuleHandle);
        title(MainAxesHandle, num2str(iF));
        drawnow

        frame = getframe(f);
        im = frame2im(frame);
        [A, map] = rgb2ind(im, 256);
        if iF == 1
            imwrite(A, map, SaveFileName, 'gif', 'LoopCount', LoopCount, 'DelayTime', DelayTime);
        else
            imwrite(A, map, SaveFileName, 'gif', 'WriteMode', 'append', 'DelayTime', DelayTime);
        end
    catch ex
        [~, FileName, Ext] = fileparts(FilePaths{iF});
        set(StatusTxt, 'Position', [.05 .05 .9 .9], 'String', sprintf('Unknown error during GIF creation while handling file %s%s:\n%s\n\nAborting.', FileName, Ext, ex.message));
        Error = true;
        break;
    end
end

title(MainAxesHandle, '');
set(f, 'Color', OldColor);

set(StatusWindow, 'UserData', true);
if ~get(CancelBtn, 'UserData')
    if ~Error
        set(StatusTxt, 'Position', [.05 .05 .9 .9], 'String', sprintf('GIF creation finished.\nSaved to %s.', SaveFileName));
        set(CancelBtn, 'Visible', 'off');
    else
        set(CancelBtn, 'Visible', 'off');
        delete(SaveFileName);
    end
else
    if exist(SaveFileName, 'file')
        delete(SaveFileName);
    end
    delete(StatusWindow);
end

end

function StatusWindow_CloseRequest(src, evt)
UD = get(src, 'UserData');
if ~UD % creation not finished yet
    msgbox('Cannot close this window during GIF creation. Cancel the GIF creation first.', 'modal');
else
    delete(src);
end

end

function CancelBtn_Callback(src, evt)
set(src, 'UserData', true);
end