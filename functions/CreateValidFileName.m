function [Str, Defaulted] = CreateValidFileName(FileName, DefaultName)
% This function changes a given file name to a file name which is valid on
% Windows. 
%
% Usage:
%     [Str, Defaulted] = CreateValidFileName(FileName, DefaultName)
%
% Input:
%     FileName ... File name which should be changed to a valid one
%     DefaultName ... If the resulting file name after removal of invalid
%     characters is empty, the DefaultName will be returned. If this input
%     argument is not given, it is defaulted to the current date and time
%     in the format yyyy-mm-dd_HH-MM-SS.
%
% Output:
%     Str ... The valid file name.
%     Defaulted ... boolean whether the default name was returned or not.
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2018 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if nargin < 2 || isempty(DefaultName)
    DefaultName = datestr(clock, 'yyyy-mm-dd_HH-MM-SS');
end
Defaulted = false;
% See Naming Conventions for Windows:
% https://msdn.microsoft.com/en-us/library/aa365247

% divide FileName into parts
[pathstr, FileName, ext] = fileparts(FileName);

% Replace not allowed characters in Windows filenames (< > : " / \ | ? *)
% by empty string:
FileName = regexprep(FileName, ...
    {'<' '>' ':' '"' '/' '\' '|' '?' '*'}, ...
    '');

% replace german umlaute and �
FileName = regexprep(FileName, ...
    {'�' '�' '�' '�' '�' '�' '�'}, ...
    {'ae' 'oe' 'ue' 'Ae' 'Oe' 'Ue' 'ss'});

% not allowed file names in Windows CON, PRN, AUX, NUL, COM1, COM2, COM3,
% COM4, COM5, COM6, COM7, COM8, COM9, LPT1, LPT2, LPT3, LPT4, LPT5, LPT6,
% LPT7, LPT8, and LPT9
NotAllowed = {'CON', 'PRN', 'AUX', 'NUL', 'COM1', 'COM2', 'COM3', ...
    'COM4', 'COM5', 'COM6', 'COM7', 'COM8', 'COM9', 'LPT1', 'LPT2', ...
    'LPT3', 'LPT4', 'LPT5', 'LPT6', 'LPT7', 'LPT8', 'LPT9'};
if any(strcmpi(NotAllowed, FileName))
    FileName = DefaultName;
    Defaulted = true;
end

if numel(FileName) == 0
	FileName = DefaultName;
	Defaulted = true;
end

Str = fullfile(pathstr, [FileName, ext]);