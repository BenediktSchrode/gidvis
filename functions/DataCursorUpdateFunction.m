function txt = DataCursorUpdateFunction(empt, obj, ToolboxModuleHandle, dcm)
% This function is used to overwrite the text displayed by the data cursor.
% This data uses the input arguments to format the text displayed by the
% data cursor according to the settings in the Toolbox Module. So if the 
% data displayed is actually e.g. the sqrt of the data, it will square the 
% data and so display the correct intensity value. Additionally, the
% datacursor string is formatted according to the selected space.
%
% Usage:
%     txt = DataCursorUpdateFunction(empt, obj, ToolboxModuleHandle, dcm)
%
% Input:
%     empt ... empty in older MATLAB versions. Might contain information in
%              newer MATLAB releases. Is not used in this file. However, do
%              not remove this input argument from the function definition
%              as it is automatically provided by the callback.
%     obj ... handle to event object. Automatically provided by the
%             callback.
%     ToolboxModuleHandle ... handle of the toolbox module
%     dcm ... the type of this variable depends on the MATLAB version. In
%             R2017b, it is of class
%             'matlab.graphics.shape.internal.DataCursorManager' whereas in
%             older versions it is of class graphics.datacursormanager.
%
% Output:
%     txt ... the text to display in the data cursor
%
% Example:
%     dcm_obj = datacursormode(hObject);
%     set(dcm_obj, 'UpdateFcn', {@DataCursorUpdateFunction, ...
%         handles.ToolboxModule, dcm_obj});
%
%
% This file is part of GIDVis.
%
% see also: datacursormode, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% set the font size of the data cursor to 10 (only working for old Matlab
% versions where dcm is of class graphics.datacursormanager)
if isa(dcm, 'graphics.datacursormanager')
    set(dcm.CurrentDataCursor, 'FontSize', 10)
end

% get the currently selected scaling
Scaling = ToolboxModule('GetSelectedScaling', ToolboxModuleHandle);
% get the currently selected space
Space = ToolboxModule('GetSelectedSpace', ToolboxModuleHandle);
% get the position of the data cursor
pos = obj.Position;
% get the target of the data cursor (usually of type 'image' or 'line')
Tar = obj.Target;
if isfield(get(Tar), 'CData') % we're dealing with an image (e.g. user clicked map)
    CDat = get(Tar, 'CData'); % get the cdata (e.g. intensity of map)
    Ind = get(obj, 'DataIndex'); % get the indices from the data cursor
    
    % in newer Matlab version, Ind is a linear index. Transform it to
    % row/column index
    if numel(Ind) == 1
        [I1, I2] = ind2sub(size(CDat), Ind);
        Ind = [I2, I1];
    end
    
    % correct the intensity value, according to the selected scaling
    switch lower(Scaling)
        case 'sqrt'
            CorrectedData = CDat(Ind(2), Ind(1))^2;
        case 'log'
            CorrectedData = exp(CDat(Ind(2), Ind(1)));
        case 'linear'
            CorrectedData = CDat(Ind(2), Ind(1));
    end
    % create a string, displaying the data (cell used to display line
    % breaks) according to selected space
    if strcmpi(Space, 'q')
        if verLessThan('MATLAB', '9.5')
            txt = {sprintf('q_xy: %g', pos(1)), ...
                  sprintf('q_z: %g', pos(2)), ...
                  sprintf('q: %g', sqrt(pos(1)^2+pos(2)^2)), ...
                  sprintf('2 Theta (lambda_Cu): %g�', 2*asind(1.54*(sqrt(pos(1)^2+pos(2)^2))/4/pi)), ...
                  sprintf('Int: %g', CorrectedData)};
        else % newer versions support subscripts in the tooltip
            txt = {sprintf('q_{xy}: %g', pos(1)), ...
                  sprintf('q_z: %g', pos(2)), ...
                  sprintf('q: %g', sqrt(pos(1)^2+pos(2)^2)), ...
                  sprintf('2 Theta (lambda_{Cu}): %g�', 2*asind(1.54*(sqrt(pos(1)^2+pos(2)^2))/4/pi)), ...
                  sprintf('Int: %g', CorrectedData)};
            
        end
    elseif strcmpi(Space, 'pixel')
        txt = {sprintf('pixel 1: %g', pos(1)), ...
              sprintf('pixel 2: %g', pos(2)), ...
              sprintf('Int: %g', CorrectedData)};
    elseif strcmpi(Space, 'polar')
        txt = {sprintf('Radius: %g', pos(1)), ...
              sprintf('Angle: %g�', pos(2)), ...
              sprintf('Int: %g', CorrectedData)};
    end
else % we're dealing with something else than an image (e.g. user clicked on a line of the Add Ring Module or Crystal Module)
    % create string according to selected space
    if strcmpi(Space, 'q')
        txt = {sprintf('q_xy: %g', pos(1)), ...
              sprintf('q_z: %g', pos(2)), ...
              sprintf('q: %g', sqrt(pos(1)^2+pos(2)^2)), ...
              sprintf('2 Theta (lambda_Cu): %g�', 2*asind(1.54*(sqrt(pos(1)^2+pos(2)^2))/4/pi))};
    elseif strcmpi(Space, 'pixel')
        txt = {sprintf('pixel 1: %g', pos(1)), ...
              sprintf('pixel 2: %g', pos(2))};
    elseif strcmpi(Space, 'polar')
        txt = {sprintf('Radius: %g', pos(1)), ...
              sprintf('Angle: %g�', pos(2))};
    end
end