function R = AtomicRadii(Elements)
% List of the radii of the elements.
% 
% sources:
%     Clementi, E., Raimondi, D. L. & Reinhardt, W. P. (1967). The Journal of Chemical Physics. 47, 1300-1307.
%     Mohr, P. J., Newell, D. B. & Taylor, B. N. (2016). Journal of Physical and Chemical Reference Data. 45, 043102.

warning('AtomicRadii:UsageDiscouraged', ...
    'The usage of ''AtomicRadii'' is discouraged. Use ''ChEl'' instead.');

if ~iscell(Elements); Elements = {Elements}; end

R = NaN(numel(Elements), 1);
for iE = 1:numel(Elements)
    Str = Elements{iE};
    % create the element
    Element = ChEl(Str);
    % get and store the radius
    R(iE) = Element.Radius;
end