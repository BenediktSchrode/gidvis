function ROINumericPosition(ROI,src,evt) %#ok<INUSD>
%ROINUMERICPOSITION Use a GUI to set the numeric boundaries of a RoI
%   How to call:
%       -ROINumericPosition(ROI,src,evt)
%
%   The only real used variable is the first argument (ROI), that's the
%   handle to the RoI object you want to modify.
%   For compatibility reasons, also the source of the call (src, second
%   input) and the event (evt, third input) are kept as inputs, but they
%   are never used.
%   Actually it supports:
%       -imrect
%       -imline
%   and extensions (sub-classes)
%   
%   Copyright (C) Jacopo Remondina, University of Milano - Bicocca
%   mail to j.remondina@campus.unimib.it
%
%   Code distributed under the GNU GPLv3 license:
%   This program is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
% 
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
% 
%   You should have received a copy of the GNU General Public License
%   along with this program.  If not, see <http://www.gnu.org/licenses/>.


% check if I was fed with some proper stuff
if ~isa(ROI,'imroi')
    warning('ROI is not an handle to the ancestor class "imroi". Cannot continue.')
    return
end

% check if there is another window
if isa(ROI,'imCustomRect') || isa(ROI,'imCustomLine')
    if isempty(ROI.UserData)
        ROI.UserData=struct('hasNumericGUIControl',false...
            ,'NumericGUIControlHandles',struct());
    elseif isstruct(ROI.UserData)
        if isfield(ROI.UserData,'hasNumericGUIControl')
            if ROI.UserData.hasNumericGUIControl
                warning('Control already present; showing it.')
                ROI.UserData.NumericGUIControlHandles.common.figure.Visible='off';
                ROI.UserData.NumericGUIControlHandles.common.figure.Visible='on';
                return
            end
        else
            ROI.UserData.hasNumericGUIControl=false;
            ROI.UserData.NumericGUIControlHandles=struct();
        end
    else
        warning('Storing old RoI userData as a field (oldUserData) in a struct');
        ROI.UserData=struct('hasNumericGUIControl',false...
            ,'NumericGUIControlHandles',struct()...
            ,'oldUserData','ROI.UserData');
    end
else
    % cannot store nothing in built-in RoI handles, so I cannot check if
    % they already have such control
end


startingPos=round(getPosition(ROI),5); % using round to not have 1463 digits

positionCallbackID=ROI.addNewPositionCallback(@ROIPosListener);

% configuration for different kinds of RoI
totEntries=4;


% try to understand how it was called and save it in 'shape'
% 'shape' is used in the GUIInit and the setCallback functions

% labels is a cell array containing "totEntries" char array, each asking form a number
% startingValues is a numeric array containing the initial value to assign
% at each editField
if isa(ROI,'imrect')
    shape='rect';
    labels={'X starting position','Y starting position','X end position','Y end position'};
    % imrect want the position as [x0, y0, width, height]
    startingValues=[startingPos(1),startingPos(2),startingPos(1)+startingPos(3),startingPos(2)+startingPos(4)];
elseif isa(ROI,'imline')
    shape='line';
    labels={'X start position','Y start position','X end position','Y end position'};
    % imline want the position as [Xstart, Ystart; Xend, Yend]
    startingValues=[startingPos(1,1),startingPos(1,2),startingPos(2,1),startingPos(2,2)];
else
    warning('ROI %s not recognaized. Assuming a 2D rectangualr shape.',class(ROI))
    shape='rect';
    labels={'X starting position','X extension','Y starting position','Y extension'};
    % imrect want the position as [x0, y0, width, height]
    startingValues=[startingPos(1),startingPos(3),startingPos(2),startingPos(4)];
end
GUIObjs=struct();

%create the GUI
GUICreation();

% attach the result to the UserData (if available)
if isa(ROI,'imCustomRect') || isa(ROI,'imCustomLine')
    ROI.UserData.hasNumericGUIControl=true;
    ROI.UserData.NumericGUIControlHandles=GUIObjs;
end

%initialize it with proper labels and values
GUIInit();

% listener for RoI being deleted
addlistener(ROI,'ObjectBeingDestroyed',@closeWindow);

% GUI function
    function GUICreation()
        % get the monitor resolution
        set(groot,'Unit','pixel');
        screenSize=get(groot,'ScreenSize');
        
        %set the single elements Horizontal and Vertical size (default in pixel)
        elementH=120;
        elementV=30;
        Vspacing=elementV;
        Hspacing=30;
        
        % common GUI elements
        GUIObjs.common.figure = uifigure('Name',sprintf('Numerically set %s dimensions',class(ROI))...
            ,'Resize','off'...
            ,'CloseRequestFcn',@closeWindow...
            );
        windowH=elementH*3 ...label+uieditfield
            +50*2 ...margins
            +2*Hspacing;
        windowV=elementV*totEntries ...%4 entries
            +Vspacing*totEntries ...%space above each of them
            +elementV*3 ...space+button+space
            ;
        GUIObjs.common.figure.Position(1)=(screenSize(3)-windowH)/2;
        GUIObjs.common.figure.Position(2)=(screenSize(4)-windowV)/2;
        GUIObjs.common.figure.Position(3)=windowH;
        GUIObjs.common.figure.Position(4)=windowV;
        
        % edit field GUI elements
        GUIObjs.fields=struct('label',gobjects(totEntries,1)...
            ,'editField',gobjects(totEntries,1)...
            ,'actualValue',gobjects(totEntries,1)...
            ); %initialize the struct of objects
        entryH=(windowH-3*elementH-2*Hspacing)/2;
        for i=1:4
            % rows: x and y
                % columns: anchor point and size
                % label
                entryV=windowV-i*elementV-i*Vspacing;
                GUIObjs.fields.label(i)=uilabel(GUIObjs.common.figure...
                    ,'HorizontalAlignment','right'...
                    ,'VerticalAlignment','center'...
                    ,'Position',[entryH,entryV,elementH,elementV]...
                    );
                % numeric edit field
                GUIObjs.fields.editField(i)=uieditfield(GUIObjs.common.figure, 'numeric'...
                    ,'Position',[entryH+elementH+Hspacing,entryV,elementH,elementV]...
                    );
                % actual value
                GUIObjs.fields.actualValue(i)=uilabel(GUIObjs.common.figure...
                    ,'HorizontalAlignment','left'...
                    ,'VerticalAlignment','center'...
                    ,'Position',[entryH+2*elementH+2*Hspacing,entryV,elementH,elementV]...
                    );
        end
        % Create Buttons
        entryV=(windowV-(1+totEntries)*elementV-totEntries*Vspacing)/2;
        entryH=entryH+elementH+Hspacing;
        GUIObjs.commands.setButton = uibutton(GUIObjs.common.figure, 'push'...
            ,'Text','Set position'...
            ,'ButtonPushedFcn',@pushButtonsCallback...
            ,'Tag','commands:set'...% used to identify the source of a certain action, in this case the 'ButtonPushed' callback'
            ,'Position',[entryH,entryV,elementH,elementV]...
            );
        GUIObjs.commands.copyButton = uibutton(GUIObjs.common.figure, 'push'...
            ,'Text','<-copy values'...
            ,'ButtonPushedFcn',@pushButtonsCallback...
            ,'Tag','commands:copy'...% used to identify the source of a certain action, in this case the 'ButtonPushed' callback'
            ,'Position',[entryH+elementH+Hspacing,entryV,elementH,elementV]...
            );
    end

    function GUIInit()
        %common
        for i=1:totEntries
            GUIObjs.fields.label(i).Text=labels{i};
            GUIObjs.fields.editField(i).Value=startingValues(i);
            GUIObjs.fields.actualValue(i).Text=sprintf('Actual: %5.4g',startingValues(i));
            GUIObjs.fields.actualValue(i).UserData=startingValues(i);
        end
        % shape-specific restrictions/options
        switch shape
            case 'rect'
                % negative values for width and height are handled in
                % the setCallback function. No need to check for them here
                
                % GUIObjs.fields.editField(i)
                % no restriction on any textField
            case 'line'
                % GUIObjs.fields.editField(i)
                % no restriction on any textField
        end
    end

% callbacks functions
    function pushButtonsCallback(src,evt) %#ok<INUSD>
        tags=strsplit(string(src.Tag),':');
        switch tags{2}
            case 'set'
                setCallback();
            case 'copy'
                copyCallback();
            otherwise
                warning('Don''t know any element with tag "%s";\rAborting operation.',src.Tag);
                return
        end
    end

    function closeWindow(src,evt) %#ok<INUSD>
        % remove the position listener
        ROI.removeNewPositionCallback(positionCallbackID);
        % reset the flag
        if isa(ROI,'imCustomRect') || isa(ROI,'imCustomLine')
            ROI.UserData.hasNumericGUIControl=false;
        end
        % delete the GUI
        delete(GUIObjs.common.figure);
    end

    function setCallback()
        outPos=startingPos;
        insertedValues=outPos;
        for i=1:totEntries
                insertedValues(i)=GUIObjs.fields.editField(i).Value;
        end
        switch shape
            case 'rect'
                outPos(1)=min(insertedValues(1),insertedValues(3));
                outPos(2)=min(insertedValues(2),insertedValues(4));
                outPos(3)=abs(insertedValues(1)-insertedValues(3));
                outPos(4)=abs(insertedValues(2)-insertedValues(4));
            case 'line'
                outPos(1,1)=insertedValues(1);
                outPos(1,2)=insertedValues(2);
                outPos(2,1)=insertedValues(3);
                outPos(2,2)=insertedValues(4);
        end
        setPosition(ROI,outPos);
    end

    function copyCallback()
        for i=1:totEntries
            GUIObjs.fields.editField(i).Value=GUIObjs.fields.actualValue(i).UserData;
        end
    end

% listeners
    function ROIPosListener(newPos)
        %newPos=round(getPosition(ROI),5);
        newPos=round(newPos,5); % using round to not have 1463 digits
        if isa(ROI,'imrect')
            actualValues=[newPos(1),newPos(2),newPos(1)+newPos(3),newPos(2)+newPos(4)];
        elseif isa(ROI,'imline')
            actualValues=[newPos(1,1),newPos(1,2),newPos(2,1),newPos(2,2)];
        else
            warning('ROI %s not recognaized. Assuming a 2D shape.',class(ROI))
            shape='rect';
        end
        for i=1:totEntries
            GUIObjs.fields.actualValue(i).Text=sprintf('Actual: %5.4g',actualValues(i));
            GUIObjs.fields.actualValue(i).UserData=actualValues(i);
        end
    end
end
