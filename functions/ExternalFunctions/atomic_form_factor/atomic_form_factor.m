function aff = atomic_form_factor(q, El)
% Calculates the atomic form factor of element El for given q values. 
%
% Input: 
%     q ... Vector of q elements to calculate atomic form factors for.
%     El ... String or cell containing strings for the elements to
%            calculate the atomic form factors for
% Output:
%     aff ... Matrix containing the atomic form factors. Has as many rows
%             as q elements, as many columns as El elements.
%
%
% Example:
%     figure
%     plot(linspace(0, 25, 100), atomic_form_factor(linspace(0, 25, 100), {'C', 'Cval', 'O1-'}))
%     legend('C', 'Cval', 'O1-')
%
%
%
% Source and further information:
%  - P. J. Brown, A. G. Fox, E. N. Maslen, M. A. O'Keefe, B. T. M. Willis,
%    in Int. Tables Crystallogr. (Ed.: E. Prince), International Union Of
%    Crystallography, Chester, England, 2006, pp. 554-595.
%  - http://lamp.tu-graz.ac.at/~hadley/ss1/crystaldiffraction/atomicformfactors/formfactors.php

% added Si to the list of coefficients using a, b, c of Siv. BS. Oct 2017

warning('atomic_form_factor:UsageDiscouraged', ...
    'The usage of ''atomic_form_factor'' is discouraged. Use ''ChEl'' instead.');

if ~iscell(El)
    El = {El};
end

aff = NaN(numel(q), numel(El));

for iEl = 1:numel(El)
    Element = ChEl(El{iEl});
    aff(:, iEl) = Element.aff(q);
end
end