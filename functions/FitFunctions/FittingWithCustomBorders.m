classdef FittingWithCustomBorders < handle
% FITTINGWITHCUSTOMBORDERS can be used to fit data shown in a line plot. By
% dragging markers, the user can interactively select the range of data
% used for the fitting process. This helps e.g. when peaks are close to
% each other. Additionally, the user can interactively select the model
% used during the fitting procedure.
%
% FITTINGWITHCUSTOMBORDERS requires the curve fitting toolbox to perform
% the fit.
%
% Usage:
%     FittingWithCustomBorders(LH)
%     FittingWithCustomBorders(LH, Color)
%     FittingWithCustomBorders(LH, Color, FitFunction)
%     FittingWithCustomBorders(LH, Color, fittypeObj)
%     FittingWithCustomBorders(LH, Color, FitFunction, Limits)
%     FittingWithCustomBorders(LH, Color, fittypeObj, Limits)
%
% Input:
%     - LH: line handle to the line which will be fitted
%     - Color: Color for the line and the textbox boundary
%     - FitFunction: function to fit to the data. FitFunction should be a
%       function accepting two input arguments xdata and ydata and
%       returning three output arguments: (1) cfit, returning the result of
%       the fitting process, (2) gof, the goodness of fit, (3) output, the
%       output structure. Defaulted to fittype('poly1') if not given.
%     - Limits: Two-element vector for the x-limits of the fit. Minimum and
%       maxium values of the x-data if not given.
%
% Example:
%     % This example shows some use cases of FittingWithCustomBorders:
%     % subplot 1: Simplest call where default color and fittype is used.
%     % subplot 2: To exclude data from the fit, the user can either drag
%     %            the rectangular markers or the limits can be adapted by
%     %            code (shown in example).
%     % subplot 3: Advanced example showing how to create a custom linear
%     %            model (a1*sin(x) + a2*x + a3) as fit function
%     % subplot 4: Using a function defined in a file as fit function.
%
%     x = linspace(-3*pi, 3*pi, 100);
%
%     figure
%     subplot(2, 2, 1)
%     lh = plot(x, 3.*x + 5, '.');
%     F = FittingWithCustomBorders(lh); % same as FittingWithCustomBorders(lh, 'r', fittype('poly1'));
%     title('Simplest Call')
% 
%     subplot(2, 2, 2)
%     y = cos(x);
%     y(1:20) = zeros(1, 20);
%     y(end-19:end) = ones(1, 20);
%     lh = plot(x, y, '.');
%     F = FittingWithCustomBorders(lh, 'r', fittype('sin1'));
%     F.Limits = [-5.7 5.7];
%     title('Setting Limits')
% 
%     subplot(2, 2, 3)
%     lh = plot(x, 8*sin(x)+x+3, '.');
%     F = FittingWithCustomBorders(lh, 'k', fittype({'sin(x)', 'x', '1'}, ...
%         'coefficients', {'a_1', 'a_2', 'a_3'}));
%     title('Custom Linear Model')
% 
%     subplot(2, 2, 4)
%     lh = plot(x, exp(-x.^2) + x/10 + 5, '.');
%     F = FittingWithCustomBorders(lh, 'k', @OneDimGaussianFit);
%     title('Fit Function from File')
%
%
% This file is part of GIDVis.
%
% see also: fittype, cfit, fit, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2018 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

properties (Access = protected)
    hg
    LHFitExtended
    LHFit
    LHPoints
    TResult
    FigureHandle
    AxesHandle
    OrigLH
    Listener
    Cnt_FunctionChange
end

properties (Dependent = true)
    Limits
    Color
    ResultPosition
    Visible
    UIFunctionChange = 'on';
end

properties (SetAccess = protected)
    FitResult
    FitGOF
    FitOutput
end

properties
    FitFunction
end

methods
    function obj = FittingWithCustomBorders(LH, Col, FitFun, Bounds)
        % Constructor
        
        % check for availability of the curve fitting toolbox
        [status, ~] = license('checkout', 'Curve_Fitting_Toolbox');
        if ~status
            warning('GIDVis:FittingWithCustomBorders:CurveFittingToolboxRequired', ...
                'To perform curve fitting, the Curve Fitting Toolbox is required.');
            obj = FittingWithCustomBorders.empty();
            return;
        end

        % set default color if not given
        if nargin < 2 || isempty(Col); Col = 'r'; end
        % set default fit function if not given
        if nargin < 3 || isempty(FitFun)
            obj.FitFunction = fittype('poly1');
        else
            if isa(FitFun, 'function_handle') || isa(FitFun, 'fittype')
                obj.FitFunction = FitFun;
            else
                error('FittingWithCustomBorders:FitFunTypeMismatch', ...
                    'FitFun should be a function handle or a fittype, instead it was a %s.', ...
                    class(FitFun));
            end
        end
        
        obj.OrigLH = LH;

        % read out the data and the axes
        XData = get(LH, 'XData');
        YData = get(LH, 'YData');
        obj.AxesHandle = ancestor(LH, 'axes');
        obj.FigureHandle = ancestor(obj.AxesHandle, 'figure');
        if isempty(obj.AxesHandle)
            error('FittingWithCustomBorders:NoAxes', 'No axes available.');
        end
        if isempty(XData) || isempty(YData)
            warning('FittingWithCustomBorders:NoData', 'No data available.')
        end

        if nargin < 4 || isempty(Bounds)
            % use the min and max x values as start boundaries
            [minX, indMin] = min(XData);
            [maxX, indMax] = max(XData);
        else
            % use bounds which are given as input
            [~, indMin] = min(abs(XData - Bounds(1)));
            minX = XData(indMin);
            [~, indMax] = min(abs(XData - Bounds(2)));
            maxX = XData(indMax);
        end
        XL = xlim(obj.AxesHandle);
        YL = ylim(obj.AxesHandle);

        % plot the things into a hggroup
        obj.hg = hggroup('Parent', obj.AxesHandle, 'DisplayName', 'Fit');
        obj.LHFitExtended = line('XData', NaN, 'YData', NaN, 'Color', Col, 'LineStyle', ':', 'Parent', obj.hg, 'HandleVisibility', 'off', 'XLimInclude', 'off', 'YLimInclude', 'off'); % line handle to curve showing fit result outside of bounds
        obj.LHFit = line('XData', NaN, 'YData', NaN, 'Color', Col, 'Parent', obj.hg, 'DisplayName', 'Fit'); % line handle to curve showing fit result
        obj.LHPoints = line('XData', [minX maxX], 'YData', [YData(indMin), YData(indMax)], 'Color', 'k', 'Marker', 's', 'MarkerSize', 10, 'MarkerFaceColor', Col, 'LineStyle', 'none', 'Parent', obj.hg, 'Clipping', 'off', 'HandleVisibility', 'off'); % line handle to curve for draggable boundary points
        obj.TResult = text(mean(XL), mean(YL), '', 'HorizontalAlignment', 'left', 'VerticalAlignment', 'middle', 'Parent', obj.hg); % handle to textbox displaying fit result

        % create function to drag boundary points
        set(obj.LHPoints, 'ButtonDownFcn', @(src, evt) obj.MouseButtonClickOnLine);
        % function to drag result textbox
        set(obj.TResult, 'ButtonDownFcn', @(src, evt) obj.MouseButtonClickOnText, 'BackgroundColor', 'w', 'EdgeColor', Col);

        % create context menu
        hcmenu = uicontextmenu('Parent', obj.FigureHandle);
        
        % create context menu entry to change fit function
        UIFM = uimenu(hcmenu, 'Label', 'Fit Model');
        
        % check for fit functions on the path and add them to the context
        % menu if available
        Sep = 'off'; % separator in context menu required or not
        if ~isempty(which('OneDimGaussianFitCBG.m'))
            uimenu(UIFM, 'Label', 'Gaussian (constant background)', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, @OneDimGaussianFitCBG));
            Sep = 'on';
        end
        if ~isempty(which('OneDimGaussianFit.m'))
            uimenu(UIFM, 'Label', 'Gaussian (linear background)', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, @OneDimGaussianFit));
            Sep = 'on';
        end
        if ~isempty(which('SigmoidFit.m'))
            uimenu(UIFM, 'Label', 'Sigmoid', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, @SigmoidFit));
            Sep = 'on';
        end
        
        % create context menu entries for the different MATLAB default fit
        % functions
        uim = uimenu(UIFM, 'Label', 'Polynomial Model', 'Separator', Sep);
        uimenu(uim, 'Label', '1st Degree', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('poly1')))
        uimenu(uim, 'Label', '2nd Degree', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('poly2')))
        uimenu(uim, 'Label', '3rd Degree', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('poly3')))
        uimenu(uim, 'Label', '4th Degree', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('poly4')))
        uimenu(uim, 'Label', '5th Degree', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('poly5')))
        uimenu(uim, 'Label', '6th Degree', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('poly6')))
        uimenu(uim, 'Label', '7th Degree', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('poly7')))
        uimenu(uim, 'Label', '8th Degree', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('poly8')))
        uimenu(uim, 'Label', '9th Degree', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('poly9')))
        
        uim = uimenu(UIFM, 'Label', 'Exponential Model');
        uimenu(uim, 'Label', '1 Term', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('exp1')))
        uimenu(uim, 'Label', '2 Terms', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('exp2')))
        
        uim = uimenu(UIFM, 'Label', 'Fourier Model');
        uimenu(uim, 'Label', '1 Term', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('fourier1')))
        uimenu(uim, 'Label', '2 Terms', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('fourier2')))
        uimenu(uim, 'Label', '3 Terms', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('fourier3')))
        uimenu(uim, 'Label', '4 Terms', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('fourier4')))
        uimenu(uim, 'Label', '5 Terms', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('fourier5')))
        uimenu(uim, 'Label', '6 Terms', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('fourier6')))
        uimenu(uim, 'Label', '7 Terms', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('fourier7')))
        uimenu(uim, 'Label', '8 Terms', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('fourier8')))
        
        uim = uimenu(UIFM, 'Label', 'Gaussian Model');
        uimenu(uim, 'Label', '1 Term', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('gauss1')))
        uimenu(uim, 'Label', '2 Terms', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('gauss2')))
        uimenu(uim, 'Label', '3 Terms', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('gauss3')))
        uimenu(uim, 'Label', '4 Terms', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('gauss4')))
        uimenu(uim, 'Label', '5 Terms', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('gauss5')))
        uimenu(uim, 'Label', '6 Terms', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('gauss6')))
        uimenu(uim, 'Label', '7 Terms', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('gauss7')))
        uimenu(uim, 'Label', '8 Terms', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('gauss8')))
        
        uim = uimenu(UIFM, 'Label', 'Power Model');
        uimenu(uim, 'Label', 'a*x^b', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('power1')))
        uimenu(uim, 'Label', 'a*x^b+c', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('power2')))
        
        uim = uimenu(UIFM, 'Label', 'Sume of Sine Model');
        uimenu(uim, 'Label', '1 Term', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('sin1')))
        uimenu(uim, 'Label', '2 Terms', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('sin2')))
        uimenu(uim, 'Label', '3 Terms', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('sin3')))
        uimenu(uim, 'Label', '4 Terms', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('sin4')))
        uimenu(uim, 'Label', '5 Terms', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('sin5')))
        uimenu(uim, 'Label', '6 Terms', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('sin6')))
        uimenu(uim, 'Label', '7 Terms', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('sin7')))
        uimenu(uim, 'Label', '8 Terms', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('sin8')))
        
        uim = uimenu(UIFM, 'Label', 'Fourier Series Model');
        uimenu(uim, 'Label', '1 Terms', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('fourier1')))
        uimenu(uim, 'Label', '2 Terms', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('fourier2')))
        uimenu(uim, 'Label', '3 Terms', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('fourier3')))
        uimenu(uim, 'Label', '4 Terms', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('fourier4')))
        uimenu(uim, 'Label', '5 Terms', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('fourier5')))
        uimenu(uim, 'Label', '6 Terms', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('fourier6')))
        uimenu(uim, 'Label', '7 Terms', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('fourier7')))
        uimenu(uim, 'Label', '8 Terms', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('fourier8')))
        
        uim = uimenu(UIFM, 'Label', 'Splines');
        uimenu(uim, 'Label', 'cubic', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('cubicspline')))
        uimenu(uim, 'Label', 'smoothing', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('smoothingspline')))
        
        uim = uimenu(UIFM, 'Label', 'Interpolants');
        uimenu(uim, 'Label', 'Linear', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('linearinterp')))
        uimenu(uim, 'Label', 'Nearest Neighbor', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('nearestinterp')))
        uimenu(uim, 'Label', 'Cubic', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('cubicinterp')))
        uimenu(uim, 'Label', 'Shape-Preserving (PCHIP)', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('pchipinterp')))
        
        uimenu(UIFM, 'Label', 'Weibull', 'Callback', @(src, evt) Cnt_SetFitModel_Callback(obj, fittype('weibull')))
        
        obj.Cnt_FunctionChange = UIFM;
        
        % create context menu entry to center result
        uimenu(hcmenu, 'Label', 'Bring Result to Center', 'Callback', @(src, evt) obj.CenterResult);
        
        % create context menu to export fit result to workspace
        if ~isdeployed
            uimenu(hcmenu, 'Label', 'Export Fit to Workspace', ...
                'Callback', @(src, evt) obj.SaveResultToWorkspace);
        end

        set([obj.LHFit, obj.LHPoints, obj.TResult, obj.LHFitExtended], 'UIContextMenu', hcmenu);

        % call UpdateFit function once to update the display
        obj.UpdateFit();
        % in case the x or y data in the plot changes, we have to update the fit
        obj.Listener = addlistener(LH, {'XData', 'YData'}, 'PostSet', @(src, evt) obj.UpdateFit);
    end
    
    function set.FitFunction(obj, val)
        obj.FitFunction = val;
        obj.UpdateFit();
    end
    
    function set.UIFunctionChange(obj, val)
        if ~ismember(val, {'on', 'off'})
            error('FittingWithCustomBorders:InvalidValue', ...
                'The provided value is not valid. Use either ''on'' or ''off''.');
        end
        set(obj.Cnt_FunctionChange, 'Visible', val);
    end
    
    function val = get.UIFunctionChange(obj)
        val = get(obj.Cnt_FunctionChange, 'Visible');
    end
    
    function val = get.Visible(obj)
        val = get(obj.hg, 'Visible');
    end
    
    function set.Visible(obj, val)
        set(obj.hg, 'Visible', val);
    end
    
    function val = get.Limits(obj)
        val = get(obj.LHPoints, 'XData');
    end
    
    function set.Limits(obj, val)
        set(obj.LHPoints, 'XData', val);
        obj.UpdateFit;
    end
    
    function val = get.Color(obj)
        val = get(obj.LHFit, 'Color');
    end
    
    function set.Color(obj, val)
        set([obj.LHFitExtended, obj.LHFit], 'Color', val);
        set(obj.LHPoints, 'MarkerFaceColor', val);
        set(obj.TResult, 'EdgeColor', val);
    end
    
    function val = get.ResultPosition(obj)
        val = get(obj.TResult, 'Position');
    end
    
    function delete(obj)
        delete(obj.Listener);
        delete(obj.hg);
    end

    function CenterResult(obj)
        % sometimes the data to fit changes in order of magnitudes, so that the fit 
        % result textbox is not visible in the axes system anymore. In this case, 
        % move it in again
        XL = xlim(get(obj.OrigLH, 'Parent'));
        YL = ylim(get(obj.OrigLH, 'Parent'));

        Ext = get(obj.TResult, 'Extent');

        set(obj.TResult, 'Position', [mean(XL)-Ext(3)/2, mean(YL)]);
    end
end

methods (Access = private)
    function SaveResultToWorkspace(obj)
        if isempty(obj.FitResult); return; end
        assignin('base', 'fitobject', obj.FitResult);
        assignin('base', 'gof', obj.FitGOF);
        assignin('base', 'output', obj.FitOutput);
    end
    
    function MouseButtonClickOnLine(obj)
        % executed when user clicks (and holds) on draggable boundary point
        
        % if user clicked with right mouse button, nothing should happen
        if ~strcmp(get(obj.FigureHandle, 'SelectionType'), 'normal'); return; end
        % read out current point of mouse
        CP = get(obj.AxesHandle, 'CurrentPoint');
        % get the x and y data of the boundary points
        x_line = get(obj.LHPoints, 'XData');
        % find the data point which is dragged (could be the left or the
        % right one) by comparing the x position of the draggable points to
        % the cursor position
        CPX = repmat(CP(1, 1), 2, 1);
        LineX = x_line;
        dist = sqrt(sum((LineX(:)-CPX(:)).^2, 2));
        [~, ind] = min(dist);

        % set the WindowButtonMotionFcn to react on changes of the cursor position
        set(obj.FigureHandle, 'WindowButtonMotionFcn', @(src, evt) obj.MouseMoveOnLine(ind));
        % in mouse release fcn we know that dragging stopped
        set(obj.FigureHandle, 'WindowButtonUpFcn', @(src, evt) obj.MouseRelease);
    end

    function MouseMoveOnLine(obj, ind)
        % executed when user drags a boundary point

        % get x and y data of the original data (the one we want to fit)
        XData = get(obj.OrigLH, 'XData');
        YData = get(obj.OrigLH, 'YData');
        % get the current cursor position
        CP = get(obj.AxesHandle, 'CurrentPoint');
        % get the x and y values of the boundary points
        x = get(obj.LHPoints, 'XData');
        y = get(obj.LHPoints, 'YData');
        % set the boundary points x and y data to the value given by the x
        % coordinate of the closest data point and its y coordinate
        [~, indmin] = min(abs(XData - CP(1, 1)));
        x(ind) = XData(indmin);
        y(ind) = YData(indmin);
        set(obj.LHPoints, 'XData', x, 'YData', y);
    end

    function MouseButtonClickOnText(obj)
        % if user clicked with right mouse button, nothing should happen
        if ~strcmp(get(obj.FigureHandle, 'SelectionType'), 'normal'); return; end
        % get the current mouse position
        CP = get(obj.AxesHandle, 'CurrentPoint');
        % set WindowButtonMotionFcn to react on dragging the result textbox
        set(obj.FigureHandle, 'WindowButtonMotionFcn', @(src, evt) obj.MouseMoveOnText(CP));
    end

    function MouseMoveOnText(obj, LastCP)
        % the user drags the result textbox
        CP = get(obj.AxesHandle, 'CurrentPoint');
        % get the old position of the textbox
        OldPos = get(obj.TResult, 'Position');
        % set the position of the box according to the movement difference between
        % the Last mouse position and the current mouse position
        set(obj.TResult, 'Position', [OldPos(1) + (CP(1, 1) - LastCP(1, 1)), OldPos(2) + (CP(1, 2) - LastCP(1, 2))]);
        set(obj.FigureHandle, 'WindowButtonUpFcn', @(src, evt) obj.MouseReleaseFromText);
        % set the WindowButtonMotionFcn with the updated CP
        set(obj.FigureHandle, 'WindowButtonMotionFcn', @(src, evt) obj.MouseMoveOnText(CP));
    end

    function MouseReleaseFromText(obj)
        set(obj.FigureHandle, 'WindowButtonMotionFcn', '');
    end

    function MouseRelease(obj)
        % reset the WindowButtonMotionFcn and the MouseReleaseFcn
        set(obj.FigureHandle, 'WindowButtonMotionFcn', '');
        set(obj.FigureHandle, 'WindowButtonUpFcn', '');
        % and update fit
        obj.UpdateFit;
    end

    function UpdateFit(obj)
        if ~ishandle(obj.OrigLH)
            obj.FitResult = [];
            obj.FitGOF = [];
            obj.FitOutput = [];
            return
        end
        % read out data
        XData = get(obj.OrigLH, 'XData');
        YData = get(obj.OrigLH, 'YData');
        if numel(XData) ~= numel(YData) || isempty(XData)
            set(obj.hg, 'Visible', 'off');
            set(obj.LHFit, 'Visible', 'off');
            set(obj.LHFitExtended, 'Visible', 'off');
            set(obj.LHPoints, 'Visible', 'off');
            obj.FitResult = [];
            obj.FitGOF = [];
            obj.FitOutput = [];
            return; 
        end
        % make sure x is ascending
        x = sort(get(obj.LHPoints, 'XData'));
        if isempty(x) || (numel(x) == 2 && x(1) == x(2))
            x = [XData(1) XData(end)];
        end
        % get x values between the boundary points
        L = XData >= x(1) & XData <= x(2);
        % perform fit
        if isa(obj.FitFunction, 'function_handle')
            try
                [cfun, gof, output] = obj.FitFunction(XData(L), YData(L));
            catch exception
                msgbox(sprintf('Error in fit process:\n\n%s', ...
                    exception.message), 'Error', 'error', 'modal');
                return;
            end
        elseif isa(obj.FitFunction, 'fittype')
            xtf = XData(L);
            ytf = YData(L);
            LNaN = isnan(ytf);
            xtf(LNaN) = [];
            ytf(LNaN) = [];
            Linf = isinf(ytf);
            xtf(Linf) = [];
            ytf(Linf) = [];
            try
                [cfun, gof, output] = fit(xtf(:), ytf(:), obj.FitFunction);
            catch exception
                msgbox(sprintf('Error in fit process:\n\n%s', ...
                    exception.message), 'Error', 'error', 'modal');
                return;
            end
        end
        if isempty(cfun) % in some cases, fit is not possible
            set(obj.TResult, 'Visible', 'off');
            set(obj.LHFit, 'Visible', 'off');
            set(obj.LHFitExtended, 'Visible', 'off');
            obj.FitResult = [];
            obj.FitGOF = [];
            obj.FitOutput = [];
            return;
        end
        % assign fit results to obj
        obj.FitResult = cfun;
        obj.FitGOF = gof;
        obj.FitOutput = output;

        % get minimum/maximum of data and axis limits
        minX = min(XData(L));
        maxX = max(XData(L));
        xLimits = xlim(obj.AxesHandle);

        % create variables for plotting fit result
        Range = maxX - minX;
        x_fit = linspace(minX, maxX, 300);
        y_fit = feval(cfun, x_fit);
        x_fitExtended = [linspace(min([xLimits(1) minX - Range]), minX, 100) NaN linspace(maxX, max([xLimits(2) maxX + Range]), 100)];
        y_fitExtended = feval(cfun, x_fitExtended);
        % set the data of the fit line handle
        set(obj.LHFit, 'XData', x_fit, 'YData', y_fit, 'Visible', 'on');
        % set the data of the extended fit line handle
        set(obj.LHFitExtended, 'XData', x_fitExtended, 'YData', y_fitExtended, 'Visible', 'on');
        % set the boundary points to the first and last data point of the fit line
        set(obj.LHPoints, 'XData', [x_fit(1), x_fit(end)], 'YData', [y_fit(1), y_fit(end)]);
        % prepare string for the textbox displaying the result
        Names = coeffnames(cfun);
        Values = coeffvalues(cfun);
        depend = dependnames(cfun);
        indep = indepnames(cfun);
        if strcmp(category(cfun), 'interpolant') || strcmp(category(cfun), 'spline')
            Str = sprintf('Model: %s(%s) = %s (%s)', depend{1}, indep{1}, ...
                formula(cfun), type(cfun));
        else
            Str = sprintf('Model: %s(%s) = %s', depend{1}, indep{1}, formula(cfun));
            interval = confint(cfun);
            deltaCoeff = abs(coeffvalues(cfun) - interval(1, :));
            for iN = 1:numel(Names)
                Str = sprintf('%s\n%s = %g \\pm %g', Str, Names{iN}, Values(iN), deltaCoeff(iN));
            end
        end
        % add goodness of fit values to display
        Str = sprintf('%s\n\nSSE: %g\nR-square: %g\nAdj. R-square: %g\nRMSE: %g', ...
            Str, obj.FitGOF.sse, obj.FitGOF.rsquare, ...
            obj.FitGOF.adjrsquare, obj.FitGOF.rmse);
        % update textbox of fit result
        set(obj.TResult, 'String', Str, 'Visible', 'on');
        % show the draggable points
        set(obj.LHPoints, 'Visible', 'on');
        % show hg group
        set(obj.hg, 'Visible', 'on');
    end
    
    function Cnt_SetFitModel_Callback(obj, NewModel)
        obj.FitFunction = NewModel;
    end
end
end