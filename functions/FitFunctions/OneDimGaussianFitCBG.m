function [cfun, gof, output, deltaCoeff] = OneDimGaussianFitCBG(xdata, ydata, Starts)
% OneDimGaussianFitCBG fits a one dimensional Gaussian curve including a
% constant background to xdata/ydata. Start parameters of the fit will be
% estimated from the data if not given.
%
% Usage:
%     [cfun, gof, output, deltaCoeff] = OneDimGaussianFitCBG(xdata, ydata, Starts)
%
% Input:
%     xdata ... x values of the data
%     ydata ... y values of the data
%     Starts ... initial guesses for the fit parameters. Will be estimated
%                from the data if not given.
%
% Output:
%    cfun ... cfit object of the fit
%    gof ... goodness-of-fit structure
%    output ... fit output structure
%    deltaCoeff ... the 95% confidence deviation of the fitted coefficients
%
% This file is part of GIDVis.
%
% see also: fit, cfit, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2019 Benedikt Schrode                                     %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% function to fit
ft = fittype('A/(sqrt(2*pi*sigma^2))*exp(-0.5*((x-mu)/sigma)^2) + d', 'independent', 'x', 'dependent', 'y');

%% fit options
opts = fitoptions('Method', 'NonlinearLeastSquares');
opts.Display = 'off';

%% remove NaNs from the data
foundNaN = false;
Lx = isnan(xdata);
if any(Lx)
    foundNaN = true;
    xdata(Lx) = [];
    ydata(Lx) = [];
end
Ly = isnan(ydata);
if any(Ly)
    foundNaN = true;
    xdata(Ly) = [];
    ydata(Ly) = [];
end
if foundNaN
    warning('OneDimGaussianFitCBG:NaNFound', 'NaNs were found in the data. Removing them before fitting.');
end
foundInf = false;
Lx = isinf(xdata);
if any(Lx)
    foundInf = true;
    xdata(Lx) = [];
    ydata(Lx) = [];
end
Ly = isinf(ydata);
if any(Ly)
    foundInf = true;
    xdata(Ly) = [];
    ydata(Ly) = [];
end
if foundInf
    warning('OneDimGaussianFitCBG:InfFound', 'Inf were found in the data. Removing them before fitting.');
end

if numel(xdata) ~= numel(ydata)
    error('OneDimGaussianFitCBG:SizeMismatch', 'X and Y data should have the same number of elements.');
elseif numel(xdata) <= 4
    warning('OneDimGaussianFitCBG:NotEnoughObservations', 'The number of elements in x and y data is not large enough to fit.');
    cfun = [];
    gof = [];
    output = [];
    deltaCoeff = [];
    return;
end

%% make sure xdata is increasing
[xdata, indx] = sort(xdata);
ydata = ydata(indx);

%% find start points for fit, if they are not given
if nargin < 3 || isempty(Starts)
    [maxy, indmax] = max(ydata); % value and position of maximum
    [miny, ~] = min(ydata); % value of minimum
    % find FWHM
    y_zero = ydata - miny;
    maxyz = max(y_zero);
    indright = find(y_zero >= maxyz/2, 1, 'last'); % index of datapoint close to right width
    indleft = find(y_zero >= maxyz/2, 1, 'first'); % index of datapoint close to left width

    if indleft == indright
        if indleft == 1
            indleft = 2;
            indright = 3;
        end
        if indright == numel(y_zero)
            indright = numel(y_zero)-1;
            indleft = indright-1;
        end
        k = (y_zero(indleft)-y_zero(indleft-1))/(xdata(indleft)-xdata(indleft-1));
        d = y_zero(indleft) - k*xdata(indleft);
        LeftX = (maxyz/2-d)/k;
        
        k = (y_zero(indright)-y_zero(indright+1))/(xdata(indright)-xdata(indright+1));
        d = y_zero(indright) - k*xdata(indright);
        RightX = (maxyz/2-d)/k;
        
        if ~isinf(LeftX) && ~isinf(RightX)
            FWHM = RightX - LeftX;
        elseif isinf(LeftX)
            FWHM = abs(RightX)/2;
        elseif isinf(RightX)
            FWHM = abs(LeftX)/2;
        end
    else
        FWHM = xdata(indright) - xdata(indleft);    
    end
    
    sigma_start = FWHM/2.3548; % calculate start value for sigma from FWHM
    
    Starts = [(maxy-miny)*sqrt(2*pi*sigma_start^2), miny, ...
        xdata(indmax), sigma_start];
end

%% set start points, toleranes and upper/lower limits
opts.StartPoint = Starts;
opts.TolFun = 1e-8;
opts.TolX = 1e-8;
opts.Lower = [-Inf -Inf -Inf 0];
opts.Upper = Inf(1, 4);

%% perform fit
[cfun, gof, output] = fit(xdata(:), ydata(:), ft, opts);

%% calculate 95% confidence intervals
interval = confint(cfun);
deltaCoeff = abs(coeffvalues(cfun) - interval(1, :));