function [cfun, gof, output, deltaCoeff] = SigmoidFit(xdata, ydata, Starts)
% SigmoidFit fits a one Sigmoid curve including a background to
% xdata/ydata. Start parameters of the fit will be estimated from the data
% if not given.
%
% Usage:
%     [cfun, gof, output, deltaCoeff] = SigmoidFit(xdata, ydata, Starts)
%
% Input:
%     xdata ... x values of the data
%     ydata ... y values of the data
%     Starts ... initial guesses for the fit parameters. Will be estimated
%                from the data if not given.
%
% Output:
%    cfun ... cfit object of the fit
%    gof ... goodness-of-fit structure
%    output ... fit output structure
%    deltaCoeff ... the 95% confidence deviation of the fitted coefficients
%
% This file is part of GIDVis.
%
% see also: fit, cfit, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% function to fit
ft = fittype('L/(1+exp(-k*(x-x0))) + d', 'independent', 'x', 'dependent', 'y');

%% fit options
opts = fitoptions('Method', 'NonlinearLeastSquares');
opts.Display = 'off';

%% remove NaNs from the data
foundNaN = false;
Lx = isnan(xdata);
if any(Lx)
    foundNaN = true;
    xdata(Lx) = [];
    ydata(Lx) = [];
end
Ly = isnan(ydata);
if any(Ly)
    foundNaN = true;
    xdata(Ly) = [];
    ydata(Ly) = [];
end
if foundNaN
    warning('SigmoidFit:NaNFound', 'NaNs were found in the data. Removing them before fitting.');
end
foundInf = false;
Lx = isinf(xdata);
if any(Lx)
    foundInf = true;
    xdata(Lx) = [];
    ydata(Lx) = [];
end
Ly = isinf(ydata);
if any(Ly)
    foundInf = true;
    xdata(Ly) = [];
    ydata(Ly) = [];
end
if foundInf
    warning('SigmoidFit:InfFound', 'Inf were found in the data. Removing them before fitting.');
end

if numel(xdata) ~= numel(ydata)
    error('SigmoidFit:SizeMismatch', 'X and Y data should have the same number of elements.');
elseif numel(xdata) <= 5
    warning('SigmoidFit:NotEnoughObservations', 'The number of elements in x and y data is not large enough to fit.');
    cfun = [];
    gof = [];
    output = [];
    deltaCoeff = [];
    return;
end

if numel(xdata) < 5
    warning('SigmoidFit:NotEnoughData', 'No fit possible, since too little data points.')
    cfun = [];
    gof = [];
    output = [];
    deltaCoeff = [];
    return;
end
%% make sure xdata is increasing
[xdata, indx] = sort(xdata);
ydata = ydata(indx);

%% find start points for fit, if they are not given
if nargin < 3 || isempty(Starts)
    LStart = max(ydata) - min(ydata);
    dStart = min(ydata);
    % look at data points between 10 and 90%
    dy = diff(ydata)./diff(xdata);
    [~, indmaxdy] = max(abs(dy));
    kStart = dy(indmaxdy)/max(ydata);
    x0 = xdata(indmaxdy);
    Starts = [LStart, dStart, kStart, x0];
end

%% set start points, toleranes and upper/lower limits
opts.StartPoint = Starts;
opts.TolFun = 1e-8;
opts.TolX = 1e-8;
opts.Lower = [0 -Inf -Inf -Inf];
opts.Upper = Inf(1, 4);

%% perform fit
[cfun, gof, output] = fit(xdata(:), ydata(:), ft, opts);

%% calculate 95% confidence intervals
interval = confint(cfun);
deltaCoeff = abs(coeffvalues(cfun) - interval(1, :));