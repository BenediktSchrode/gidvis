function [cfun, deltaCoeff, gof, output] = TwoDimGaussianFit(xdata, ydata, zdata, Starts)
% TwoDimGaussianFit fits a two dimensional gaussian to xdata/ydata/zdata.
% Start parameters of the fit will be estimated from the data if not given.
%
% Usage:
%     [cfun, deltaCoeff, gof, output] = TwoDimGaussianFit(xdata, ydata, zdata, Starts)
%
% Input:
%     xdata ... the x values of the data
%     ydata ... the y values of the data
%     zdata ... the z values of the data
%     Starts ... initial guesses for the fit parameters. Will be estimated
%                from the data if not given.
%
% Output:
%    cfun ... cfit object of the fit
%    deltaCoeff ... the 95% confidence deviation of the fitted coefficients
%    gof ... structure containing values on the goodness of fit
%    output ... general information about the fitting process/algorithm
%
% This file is part of GIDVis.
%
% see also: fit, cfit, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% function to fit
ft = fittype('A*exp(-((x-x0)^2/(2*sigmax^2)+(y-y0)^2/(2*sigmay^2)))+kxy*x*y+kx*x+ky*y+d', ...
                'independent', {'x', 'y'}, 'dependent', 'z');

%% fit options
opts = fitoptions('Method', 'NonlinearLeastSquares');
opts.Display = 'off';

%% make sure data is correct size
xdata = xdata(:);
ydata = ydata(:);
zdata = zdata(:);

%% remove NaNs from the data
found = false;
Lx = isnan(xdata);
if any(Lx)
    found = true;
    xdata(Lx) = [];
    ydata(Lx) = [];
    zdata(Lx) = [];
end
Ly = isnan(ydata);
if any(Ly)
    found = true;
    xdata(Ly) = [];
    ydata(Ly) = [];
    zdata(Ly) = [];
end
Lz = isnan(zdata);
if any(Lz)
    found = true;
    xdata(Lz) = [];
    ydata(Lz) = [];
    zdata(Lz) = [];
end
if found
    warning('TwoDimGaussianFit:NaNFound', 'NaNs were found in the data. Removing them before fitting.');
end

if numel(xdata) ~= numel(ydata) || numel(xdata) ~= numel(zdata)
    error('TwoDimGaussianFit:SizeMismatch', 'X, Y and Z data should have the same number of elements.');
end
if numel(xdata) <= 9
    warning('TwoDimGaussianFit:NotEnoughObservations', 'The number of elements in x, y and z data is not large enough to fit.');
    cfun = [];
    deltaCoeff = [];
    return;
end

% find start values if they are not given
if nargin < 4 || isempty(Starts)
    xmin = min(xdata);
    ymin = min(ydata);
    xmax = max(xdata);
    ymax = max(ydata);
    % kx, ky, d
    d = sqrt((xdata-xmin).^2+(ydata-ymin).^2);
    [~, ind] = min(d);
    P = [xdata(ind), ydata(ind), zdata(ind)];
    d = sqrt((xdata-xmax).^2+(ydata-ymin).^2);
    [~, ind] = min(d);
    Q = [xdata(ind), ydata(ind), zdata(ind)];
    d = sqrt((xdata-xmin).^2+(ydata-ymax).^2);
    [~, ind] = min(d);
    R = [xdata(ind), ydata(ind), zdata(ind)];
    M = [P; Q; R];
    EG = M\ones(3, 1);
    EG = EG./EG(3);
    kxStart = -EG(1);
    kyStart = -EG(2);
    if isinf(kxStart) || isnan(kxStart)
        kxStart = 0; 
    end
    if isinf(kyStart) || isnan(kyStart)
        kyStart = 0; 
    end
    dStart = P(3)-kxStart*P(1)-kyStart*P(2);
    % A
    ZZeroed = zdata-kxStart.*xdata-kyStart.*ydata-dStart;
    [MaxZ, Ind] = max(ZZeroed);
    % sigmax
    Deltay1 = (max(ydata)-min(ydata))/100;
    L = ydata >= ydata(Ind) - Deltay1/2 & ydata <= ydata(Ind) + Deltay1/2;
    XSig = xdata(L);
    [XSig, SortInd] = sort(XSig);
    ZSig = ZZeroed(L);
    ZSig = ZSig(SortInd);
    indright = find(ZSig >= MaxZ/2, 1, 'last'); % index of datapoint close to right width
    indleft = find(ZSig >= MaxZ/2, 1, 'first'); % index of datapoint close to left width
    if indleft == indright
        if indright == numel(ZSig)
            indright = numel(ZSig)-1;
            indleft = indright-1;
        end
        if indleft == 1
            indleft = 2;
            indright = 3;
        end
        if indright ~= 0 && indleft ~= 0 && indright == numel(ZSig)
            k = (ZSig(indleft)-ZSig(indleft-1))/(XSig(indleft)-XSig(indleft-1));
            d = ZSig(indleft) - k*XSig(indleft);
            LeftX = (max(ZSig)/2-d)/k;

            try
                k = (ZSig(indright)-ZSig(indright+1))/(XSig(indright)-XSig(indright+1));
            catch
                k = 0;
            end
            d = ZSig(indright) - k*XSig(indright);
            RightX = (max(ZSig)/2-d)/k;

            if ~isinf(LeftX) && ~isinf(RightX)
                FWHMX = RightX - LeftX;
            elseif isinf(LeftX)
                FWHMX = abs(RightX)/2;
            elseif isinf(RightX)
                FWHMX = abs(LeftX)/2;
            end
        else
            FWHMX = 0.23548;
        end
    else
        FWHMX = XSig(indright) - XSig(indleft);
    end
    sigmaxStart = FWHMX/2.3548;
    % sigmay
    Deltax1 = (max(xdata)-min(xdata))/100;
    L = xdata > xdata(Ind) - Deltax1/2 & xdata < xdata(Ind) + Deltax1/2;
    YSig = ydata(L);
    [YSig, SortInd] = sort(YSig);
    ZSig = ZZeroed(L);
    ZSig = ZSig(SortInd);
    indright = find(ZSig >= MaxZ/2, 1, 'last');
    indleft = find(ZSig >= MaxZ/2, 1, 'first');
    if indright == indleft
        if indright == numel(ZSig)
            indright = numel(ZSig)-1;
            indleft = indright-1;
        end
        if indleft == 1
            indleft = 2;
            indright = 3;
        end
        if indright ~= 0 && indleft ~= 0 && indright ~= numel(ZSig)
            k = (ZSig(indleft)-ZSig(indleft-1))/(YSig(indleft)-YSig(indleft-1));
            d = ZSig(indleft) - k*YSig(indleft);
            LeftX = (max(ZSig)/2-d)/k;

            try
                k = (ZSig(indright)-ZSig(indright+1))/(YSig(indright)-YSig(indright+1));
                d = ZSig(indright) - k*YSig(indright);
                RightX = (max(ZSig)/2-d)/k;
            catch
                RightX = max(ZSig)/2;
            end

            if ~isinf(LeftX) && ~isinf(RightX)
                FWHMY = RightX - LeftX;
            elseif isinf(LeftX)
                FWHMY = abs(RightX)/2;
            elseif isinf(RightX)
                FWHMY = abs(LeftX)/2;
            end
        else
            FWHMY = 0.23548;
        end
    else
        FWHMY = YSig(indright) - YSig(indleft);
    end
    sigmayStart = FWHMY/2.3548;

    if sigmayStart == 0; sigmayStart = 0.1; end
    if sigmaxStart == 0; sigmaxStart = 0.1; end
    Starts = [zdata(Ind), dStart, kxStart, 0, kyStart, ...
        sigmaxStart, sigmayStart, xdata(Ind), ydata(Ind)];
end

%% set start points, toleranes and upper/lower limits
opts.StartPoint = Starts;
opts.TolFun = 1e-8;
opts.TolX = 1e-8;
opts.Lower = [0 -Inf -Inf -Inf -Inf 0 0 -Inf -Inf];
opts.Upper = Inf(1, 9);

%% perform fit
[cfun, gof, output] = fit([xdata(:), ydata(:)], zdata(:), ft, opts);

%% calculate 95% confidence intervals
interval = confint(cfun);
deltaCoeff = abs(coeffvalues(cfun) - interval(1, :));