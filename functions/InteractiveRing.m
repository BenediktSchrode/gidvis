classdef InteractiveRing < handle & hgsetget %#ok BS. hgsetget due to backwards compatibility.
    % InteractiveRing adds a draggable ring to a plot by interactive placement.
    %
    % This file is part of GIDVis.
    %
    % see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2018 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % inherit from hgsetget to enable things like get(obj, 'LineColor'). 
    % We inherit from hgsetget instead of matlab.mixin.SetGet to obtain
    % backwards compatibility.
    properties (SetAccess = private)
        Radius = 0; % radius of the ring
        UnderConstruction = false; % bool if InteractivePlacement is running
        CreationSelectionType = ''; % string showing the figure's SelectionType which was used during ring creation
    end
    
    properties (Constant = true, Hidden = true)
        sinphi = sin(linspace(-pi, pi, 500)); % for calculation of ring coordinates
        cosphi = cos(linspace(-pi, pi, 500)); % for calculation of ring coordinates
    end
    
    properties (Dependent = true, SetObservable = true)
        LineStyle % line style of the ring
        LineWidth % line width of the ring
        LineColor % line color of the ring
        LabelVisible % bool if label visible
    end
    
    properties (Access = protected, Hidden = true)
        HGGroup % parent to store all graphic elements in
        LabelHandle % handle to the label
        LineHandle % handle to the line
        
        WindowButtonDownFcnID
        WindowButtonMotionFcnID
        WindowButtonUpFcnID
        
        AxesHandle % handle to the axes the ring is in
        
        ClickObj % the objects on which a ButtonDownFcn will be placed for interactive placement of ring, i.e. the children of the axes
    end
    
    properties (Dependent = true, Access = protected)
        FigureHandle % handle to the figure the ring is in
    end
    
    properties (SetAccess = private)
        ContextMenu % the context menu associated with the ring. Private so that it cannot be deleted. Elements can be added by uimenu(obj.ContextMenu, ...)
    end
    
    properties
        LabelUpdateFunction = @(obj) sprintf('%g', obj.Radius); % function called for update of label text
        Center = [0 0]; % center point of the ring
    end
    
    methods
        % Constructor.
        % Usage: InteractiveRing(Ax, LStyle, LWidth, LColor, LVis)
        function obj = InteractiveRing(Ax, LStyle, LWidth, LColor, LVis)
            % initialize missing values
            if nargin < 1 || isempty(Ax); Ax = gca; end
            if nargin < 2 || isempty(LStyle); LStyle = '-'; end
            if nargin < 3 || isempty(LWidth); LWidth = 1; end
            if nargin < 4 || isempty(LColor); LColor = 'k'; end
            if nargin < 5 || isempty(LVis); LVis = 'on'; end
                        
            % store the axes handle
            obj.AxesHandle = Ax;
            % get the children in the axes
            obj.ClickObj = [obj.AxesHandle; get(obj.AxesHandle, 'Children')];
            
            % create the hggroup
            obj.HGGroup = hggroup('Parent', obj.AxesHandle);
            % initialize the line handle
            obj.LineHandle = line('XData', [], 'YData', [], ...
                'Parent', obj.HGGroup, 'Tag', 'RingLine', ...
                'XLimInclude', 'off', 'YLimInclude', 'off');
            % set the line properties according to input arguments
            set(obj.LineHandle, 'LineStyle', LStyle, 'LineWidth', LWidth, ...
                'Color', LColor);
            % initialize the label handle
            obj.LabelHandle = text(0, 0, '', 'Parent', obj.HGGroup, ...
                'BackgroundColor', 'w', 'Visible', LVis, ...
                'Tag', 'RingLabel');
            
            % set the ButtonDownFcn of the line --> used for dragging ring
            set(obj.LineHandle, 'ButtonDownFcn', @(src, evt) obj.Line_ButtonDownFcn);
            
            % create the context menu
            obj.ContextMenu = uicontextmenu('Parent', obj.FigureHandle);
            % and it's entries
            uimenu(obj.ContextMenu, 'Label', 'Delete Ring', ...
                'Callback', @(src, evt) obj.delete, 'Tag', 'DeleteRing');
            CreateContextMenuEntry(obj.ContextMenu, obj, 'LineColor', 'Line Color...', 'color');
            CreateContextMenuEntry(obj.ContextMenu, obj, 'LineWidth', 'Line Width...', 'numeric');
            CreateContextMenuEntry(obj.ContextMenu, obj, 'LineStyle', 'Line Style', {'-', 'Solid'; '--', 'Dashed'; ':', 'Dotted'; '-.', 'Dash-Dotted'});
            CreateContextMenuEntry(obj.ContextMenu, obj, 'LabelVisible', 'Label Visibility', 'onoff', 'Checked', LVis);
            uimenu(obj.ContextMenu, 'Label', 'Bring to Front', 'Separator', 'on', ...
                'Callback', @(src, evt) obj.BringToFront);
            
            % set the context menu of the line and the label
            set(obj.LineHandle, 'UIContextMenu', obj.ContextMenu);
            set(obj.LabelHandle, 'UIContextMenu', obj.ContextMenu);
        end
        
        % start the interactive placement (i. e. let user click where ring
        % should go through)
        function StartInteractivePlacement(obj)
            set(obj.ClickObj, 'ButtonDownFcn', @(src, evt) obj.ButtonDownFcn);
            set(obj.FigureHandle, 'Pointer', 'crosshair')
            obj.UnderConstruction = true;
            uiwait(obj.FigureHandle); % block code execution
        end
        
        % Cancels the interactive placement
        function CancelInteractivePlacement(obj)
            set(obj.ClickObj, 'ButtonDownFcn', '');
            set(obj.FigureHandle, 'Pointer', 'arrow')
            obj.UnderConstruction = false;
            uiresume(obj.FigureHandle); % continue code execution
        end
        
        % set/get functions
        function set.LineStyle(obj, Val)
            set(obj.LineHandle, 'LineStyle', Val);
        end
        function Val = get.LineStyle(obj)
            Val = get(obj.LineHandle, 'LineStyle');
        end
        
        function set.LineWidth(obj, Val)
            set(obj.LineHandle, 'LineWidth', Val);
        end
        function Val = get.LineWidth(obj)
            Val = get(obj.LineHandle, 'LineWidth');
        end
        
        function set.LineColor(obj, Val)
            set(obj.LineHandle, 'Color', Val);
        end
        function Val = get.LineColor(obj)
            Val = get(obj.LineHandle, 'Color');
        end
        
        function set.LabelVisible(obj, Val)
            set(obj.LabelHandle, 'Visible', Val);
        end
        function Val = get.LabelVisible(obj)
            Val = get(obj.LabelHandle, 'Visible');
        end
        
        function Val = get.FigureHandle(obj)
            Val = ancestor(obj.HGGroup, 'figure');
        end
        
        function set.Center(obj, Val)
            Delta = obj.Center(:) - Val(:);
            OldPos = get(obj.LabelHandle, 'Position');
            NewPos = OldPos(:) - [Delta; 0];
            obj.Center = Val;
            
            obj.UpdateRing(NewPos);
        end
        
        % delete object, just need to delete the hggroup
        function delete(obj)
            delete(obj.HGGroup);
        end
    end
    
    methods (Access = protected)
        % bring the ring system to the front
        function BringToFront(obj)
            uistack(obj.HGGroup, 'top');
        end
        
        % update the plotted elements
        function UpdateRing(obj, LabelPos)
            % calculate coordinates for the circle
            x = obj.Radius.*obj.cosphi + obj.Center(1);
            y = obj.Radius.*obj.sinphi + obj.Center(2);
            
            % set data for ring
            set(obj.LineHandle, 'XData', x, 'YData', y);
            
            % set label position
            set(obj.LabelHandle, 'Position', LabelPos);
            % set label string according to the LabelUpdateFunction
            try
                set(obj.LabelHandle, 'String', obj.LabelUpdateFunction(obj));
            catch ex
                set(obj.LabelHandle, 'String', sprintf('Error in custom LabelUpdateFunction.\n%s', ex.message));
            end
            % get the quadrant and set the labels alignment properties
            % accordingly
            alpha = atan2(LabelPos(2)-obj.Center(2), LabelPos(1)-obj.Center(1));
            if alpha < 0; alpha = alpha + 2*pi; end
            Q = floor(alpha/(pi/2));
            switch Q
                case 0
                    set(obj.LabelHandle, 'HorizontalAlignment', 'left', ...
                        'VerticalAlignment', 'bottom');
                case 1
                    set(obj.LabelHandle, 'HorizontalAlignment', 'right', ...
                        'VerticalAlignment', 'bottom');
                case 2
                    set(obj.LabelHandle, 'HorizontalAlignment', 'right', ...
                        'VerticalAlignment', 'top');
                case 3
                    set(obj.LabelHandle, 'HorizontalAlignment', 'left', ...
                        'VerticalAlignment', 'top');
            end
        end
        
        % update plotted elements after ring has been dragged
        function UpdateRingByDrag(obj)
            % get mouse position
            CurPoint = get(obj.AxesHandle, 'CurrentPoint');
            
            % Calculate Radius
            obj.Radius = sqrt((obj.Center(1) - CurPoint(1, 1)).^2 + ...
                (obj.Center(2) - CurPoint(1, 2)).^2);
            
            % calculate label position
            LabelPos = [CurPoint(1, 1) CurPoint(1, 2)];
            
            % update the ring
            UpdateRing(obj, LabelPos);
        end
        
        % function executed during InteractivePlacement
        function ButtonDownFcn(obj)
            set(obj.ClickObj, 'ButtonDownFcn', '');
            set(obj.FigureHandle, 'Pointer', 'arrow')
            UpdateRingByDrag(obj);
            obj.UnderConstruction = false;
            obj.CreationSelectionType = get(obj.FigureHandle, 'SelectionType');
            uiresume();
        end
        
        % when the user clicks the ring
        function Line_ButtonDownFcn(obj)
            if ~strcmp(get(obj.FigureHandle, 'SelectionType'), 'normal'); return; end
            UpdateRingByDrag(obj);
            set(obj.FigureHandle, 'WindowButtonMotionFcn', @(src, evt) obj.WindowButtonMotionFcn);
            set(obj.FigureHandle, 'WindowButtonUpFcn', @(src, evt) obj.WindowButtonUpFcn);
        end
        
        % when the ring gets dragged
        function WindowButtonMotionFcn(obj)
            UpdateRingByDrag(obj);
        end
        
        % mouse gets released after ring drag
        function WindowButtonUpFcn(obj)
            set(obj.FigureHandle, 'WindowButtonMotionFcn', '');
            set(obj.FigureHandle, 'WindowButtonUpFcn', '');
        end
    end
end