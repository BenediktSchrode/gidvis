function Choices = MultiSelectionDialog(Text, Options, varargin)
% This functions provides a user interface to select multiple items from a
% list of items.
%
% Input:
%     Text ... Title above the selection area
%     Options ... Cell of strings to choose from
%     varargin ... all other input arguments are passed directly to the
%                  call to the MATLAB function dialog()
% 
% Output:
%     Choices ... Cell of strings of selected items
%
% Usage:
%     Choices = MultiSelectionDialog(Text, Options, varargin)
%
% Run MultiSelectionDialog without input for a demo.
%
% This file is part of GIDVis.
%
% see also: dialog, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2018 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if nargin == 0
        Text = 'Select the options:';
        Options = 'a|b|c|d|e|f|g|h|i|j|k|l';
    end

    d = dialog('Position', [300 300 400 400], varargin{:});
    uicontrol('Parent', d, 'Style', 'text', ...
        'Units', 'normalized', 'Position', [0.05 0.9 .9 .07], ...
        'String', Text, ...
        'HorizontalAlignment', 'Left', 'FontSize', 10);
    LBFrom = listbox('Parent', d, 'Units', 'normalized', ...
        'Position', [0.05 0.13 .395 .78], 'String', Options, ...
        'FontSize', 10);
    LBTo = listbox('Parent', d, 'Units', 'normalized', ...
        'Position', [0.555 0.13 .395 .78], 'FontSize', 10);
    BtnAdd = uicontrol('Parent', d, 'Units', 'normalized', ...
        'Position', [0.465 .555 0.07 0.07], 'String', '>>', ...
        'FontSize', 10);
    BtnRemove = uicontrol('Parent', d, 'Units', 'normalized', ...
        'Position', [0.465 .465 0.07 0.07], 'String', '<<', ...
        'FontSize', 10, 'Callback', {@BtnRemove_Callback});
    uicontrol('Parent', d, 'Units', 'normalized', ...
        'Position', [0.05 0.03 .9 .07], 'String', 'Ok', ...
        'FontSize', 10, 'Callback', {@BtnOK_Callback, LBTo});
    set(BtnAdd, 'Callback', @(src, evt) BtnAdd_Callback(LBFrom, LBTo));
    set(BtnRemove, 'Callback', @(src, evt) BtnRemove_Callback(LBFrom, LBTo));
    
    Choices = {};
    
    addlistener(LBFrom, 'DoubleClick', @(src, evt) BtnAdd_Callback(LBFrom, LBTo));
    addlistener(LBTo, 'DoubleClick', @(src, evt) BtnRemove_Callback(LBFrom, LBTo));
    
    % Wait for d to close before running to completion
    uiwait(d);
    
    function BtnAdd_Callback(LBFrom, LBTo)
        idx = get(LBFrom, 'Value');
        if isempty(idx); return; end
        itemsFrom = get(LBFrom, 'String');
        if isempty(itemsFrom); return; end
        itemsTo = get(LBTo, 'String');
        itemsTo = sortrows([itemsTo; itemsFrom(idx)]);
        itemsFrom(idx) = [];
        while numel(itemsFrom) < idx && idx > 1
            idx = idx - 1;
        end
        set(LBFrom, 'Value', idx, 'String', itemsFrom);
        if numel(itemsTo) == 1
            set(LBTo, 'Value', 1);
        end
        set(LBTo, 'String', itemsTo);
    end
   
    function BtnRemove_Callback(LBFrom, LBTo)
        idx = get(LBTo, 'Value');
        if isempty(idx); return; end
        itemsFrom = get(LBFrom, 'String');
        itemsTo = get(LBTo, 'String');
        if isempty(itemsTo); return; end
        itemsFrom = sortrows([itemsFrom; itemsTo(idx)]);
        itemsTo(idx) = [];
        while numel(itemsTo) < idx && idx > 1
            idx = idx - 1;
        end
        set(LBFrom, 'String', itemsFrom);
        set(LBTo, 'Value', idx, 'String', itemsTo);
    end

    function BtnOK_Callback(src, evt, LBTo)
        Choices = get(LBTo, 'String');
        delete(ancestor(src, 'figure'));
    end
end