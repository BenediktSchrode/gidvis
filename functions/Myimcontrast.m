function h = Myimcontrast(varargin)
% Myimcontrast opens the imcontrast window but with the apply button
% removed and the text adapted.
% To give the user the full control over the image's color display,
% imcontrast can be used. However, this includes a button to apply the
% changes to the image data. Although this does not alter the image file
% itself (and the effect is gone after reloading), it might be source of
% confusion. So this function removes the possibility to apply the changes
% to the data.
%
% For a description on the usage of the Adjust Contrast window, please
% refer to the imcontrast help.
%
% Usage:
%     h = Myimcontrast(varargin)
%
% Input:
%     varargin ... all arguments are directly passed to the imcontrast call
%
% Output:
%     h ... Handle to Adjust Contrast tool figure
%
%
% This file is part of GIDVis.
%
% see also: imcontrast, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% check if there is a license for the image toolbox, which is necessary for
% the imcontrast call below
[stat, msg] = license('checkout', 'Image_Toolbox');
if ~stat
    msgbox(msg, 'License Checkout Failed', 'modal');
    h = [];
    return;
end

h = imcontrast(varargin{:});
AdjustDataButton = findobj(h, 'Tag', 'adjust data button');
try %#ok
% Using of try ok here, because in other MATLAB releases AdjustDataButton
% might not be found (e.g. because the tag changed).
set(AdjustDataButton, 'Visible', 'off');
end
TextLabel = findobj(h, 'Tag', 'status text');
try %#ok
% Using of try ok here, because in other MATLAB releases TextLabel might
% not be found (e.g. because the tag changed).
    Text = get(TextLabel, 'String');
    set(TextLabel, 'String', Text(1, :));
end