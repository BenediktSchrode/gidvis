function [x, y] = NormalizedUnitsFromDataUnits(AxesHandle, xToTransform, yToTransform)
    % Use this function to calculate normalized units from data units.
    %
    % Usage:
    %     [x, y] = NormalizedUnitsFromDataUnits(AxesHandle, ...
    %         xToTransform, yToTransform);
    %
    % Input:
    %     AxesHandle ... handle of the axes for which the units should be
    %                    converted
    %     xToTransform ... the x component in data units which should be 
    %                      transformed
    %     yToTransform ... the y component in data units which should be 
    %                      transformed
    %
    % Output:
    %     x ... the x component in normalized units
    %     y ... the y component in normalized units
    %
    % This file is part of GIDVis.
    %
    % see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % store the old axes units
    OldAxUnits = get(AxesHandle, 'Units');
    % set the axes units to normlaized
    set(AxesHandle, 'Units', 'normalized');
    % get the position of the axes
    AxPos = plotboxpos(AxesHandle);
    % get the x and y lims of the axes (is in data units)
    AxLims = axis(AxesHandle);
    % restore the unit of the axes
    set(AxesHandle, 'Units', OldAxUnits);
    
    % calculate the size of the axes
    AxWidth = diff(AxLims(1:2));
    AxHeight = diff(AxLims(3:4));

    % calculate the values, taking reversed axes into account if neccessary
    if strcmpi(get(AxesHandle, 'XDir'), 'reverse')
        x = 1-((xToTransform-AxLims(1))*AxPos(3)/AxWidth + (1-AxPos(1)-AxPos(3)));
    else
        x = (xToTransform-AxLims(1))*AxPos(3)/AxWidth + AxPos(1);
    end
    if strcmpi(get(AxesHandle, 'YDir'), 'reverse')
        y = 1-((yToTransform-AxLims(3))*AxPos(4)/AxHeight + (1-AxPos(2)-AxPos(4)));
    else
        y = (yToTransform-AxLims(3))*AxPos(4)/AxHeight + AxPos(2);
    end
end