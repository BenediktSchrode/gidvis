classdef PeakFind < handle
    % PEAKFIND fits a two dimensional Gaussian function to data which can
    % be selected interactively by the user.
    %
    % XData, YData and ZData can be scattered data.
    % XData, YData and ZData can be changed after the PEAKFIND creation.
    % However, to update the fit, the PerformFit method has to be called.
    %
    % Usage:
    %     PeakFind(X, Y, Z, x0, y0, W, H);
    %         Creates PEAKFIND with center x0, y0, width W, height H in
    %         current axis. The data is given by X, Y, Z.
    %     PeakFind(X, Y, Z, x0, y0, W, H, ax);
    %         Creates PEAKFIND with center x0, y0, width W, height H in
    %         axis ax. The data is given by X, Y, Z.
    %
    % Example:
    %     [x, y, z] = peaks(50);
    %     imagesc(x(1, :), y(:, 1), z)
    %     PF = PeakFind(x, y, z, 0.10, 1.7, 2.3, 1.1);
    %     PF.PerformFit();
    %     plot(PF);
    %
    % Now the user can interact with the rectangle (resizing, dragging).
    % The fit will be updated when interaction is complete.
    %
    %
    % This file is part of GIDVis.
    %
    % see also: TwoDimGaussianFit, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2018 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    properties
        % x data connected to the PeakFind.
        XData
        % y data connected to the PeakFind.
        YData
        % z data connected to the PeakFind.
        ZData
    end
    
    properties
        % Can be used to store user data.
        UserData
        % If given, the point given by x0_Expected and y0_Expected is
        % marked.
        x0_Expected
        % If given, the point given by x0_Expected and y0_Expected is
        % marked.
        y0_Expected
    end
    
    properties (SetAccess = protected)
        % Result of the fit (cfit object).
        FitResult
        % The width of the data box.
        Width
        % The height of the data box.
        Height
        % Horizontal center of the data box.
        x0
        % Vertical center of the data box.
        y0
    end
    
    properties (Access = protected)
        % A hggroup all the graphics elements are plotted to.
        HGGroup
        % Handle of the textbox showing the Fitting text.
        FittingTB_Handle
        % Handle of the rectangle of the box.
        Rectangle_Handle
        % Handle of the marker showing the fit center.
        FitCenter_Handle
        % Handle of the marker showing the rectangle center.
        RectangleCenter_Handle
        % Handle of the markers showing the rectangle corners and lines.
        RectangleCorners_Handle
        % Handle to the marker showing the expected position.
        Expected_Handle
        % Handle of the line connecting the center of the recangle with the
        % fit result.
        Connector_Handle
        % Stores the value of the Fitting property.
        FittingSave = false;
    end
    
    properties (SetAccess = protected, Dependent = true)
        % Boolean whether fit process is currently running or not.
        Fitting = false;
    end
    
    events
        % Fired whenever the fitting process finishes.
        FinishedFitting
    end
    
    methods        
        function set.Fitting(obj, value)
            % Function for setting the Fitting property.
            
            % check if the fitting textbox was already created
            if ~isempty(obj.FittingTB_Handle)
                % turn visibility on/off depending on new Fitting value.
                if value
                    set(obj.FittingTB_Handle, 'Visible', 'on');
                else
                    set(obj.FittingTB_Handle, 'Visible', 'off');
                end
            end
            % store the new value.
            obj.FittingSave = value;
        end
        
        function val = get.Fitting(obj)
            % Function for getting the Fitting property.
            val = obj.FittingSave;
        end
        
        function obj = PeakFind(X, Y, Z, x0, y0, W, H, ax)
            % Constructor of the PEAKFIND class.
            %
            % Usage:
            %     PeakFind(X, Y, Z, x0, y0, W, H);
            %         Creates PeakFind with center x0, y0, width W, height
            %         H in current axis. The data is given by X, Y, Z.
            %     PeakFind(X, Y, Z, x0, y0, W, H, ax);
            %         Creates PeakFind with center x0, y0, width W, height
            %         H in axis ax. The data is given by X, Y, Z.
            
            % check number of input arguments and set default if necessary
            if nargin < 7; error('Not enough inputs.'); end
            if nargin < 8 || isempty(ax); ax = gca; end
            
            % check that the data is of same size
            if ~all(size(X) == size(Y)) || ~all(size(X) == size(Z))
                error('X, Y and Z data have to be of the same size.')
            end
            
            % store values in class
            obj.x0 = x0;
            obj.y0 = y0;
            obj.Width = W;
            obj.Height = H;
            
            % for the fit process, the data should have one column
            obj.XData = X(:);
            obj.YData = Y(:);
            obj.ZData = Z(:);

            % initialize all the graphic elements.
            obj.HGGroup = hggroup('Parent', ax);
            obj.Rectangle_Handle = rectangle('Position', [0 0 0 0], ...
                'Parent', obj.HGGroup, 'FaceColor', [1 1 1 .3]);
            obj.Connector_Handle = line('XData', [], 'YData', [], ...
                'Parent', obj.HGGroup);
            obj.Expected_Handle = line('XData', [], 'YData', [], ...
                'Parent', obj.HGGroup, 'Color', 'y');
            obj.FitCenter_Handle = line('XData', [], 'YData', [], ...
                'Color', 'r', 'Marker', 'o', 'Parent', obj.HGGroup);
            obj.RectangleCenter_Handle = line('XData', [], 'YData', [], ...
                'Color', 'k', 'Marker', 's', 'Parent', obj.HGGroup, ...
                'MarkerSize', 8);
            obj.RectangleCorners_Handle = line('XData', [], 'YData', [], ...
                'Color', 'k', 'Marker', 's', 'Parent', obj.HGGroup, ...
                'LineStyle', 'none', 'MarkerSize', 8);
            obj.FittingTB_Handle = text(NaN, NaN, 'Fitting ...', 'Parent', obj.HGGroup, ...
                'HorizontalAlignment', 'center', 'String', 'Fitting', ...
                'VerticalAlignment', 'middle', 'FontSize', 10, ...
                'BackgroundColor', 'w', 'Visible', 'off');
            
            % set the ButtonDownFcn of the rectangle center (used to drag
            % the rectangle)
            set(obj.RectangleCenter_Handle, ...
                'ButtonDownFcn', @(src, evt) obj.Rectangle_BtnDown);
            % set the ButtonDownFcn of the outer rectangle markers (used to
            % resize the rectangle)
            set(obj.RectangleCorners_Handle, ...
                'ButtonDownFcn', @(src, evt) obj.RectangleSize_Callback);
            
            % create a context menu (used to show the fit result in 3D and
            % the fit result in a table)
            UIC = uicontextmenu(ancestor(obj.HGGroup, 'figure'));
            uimenu(UIC, 'Label', 'Show 3D', ...
                'Callback', @(src, evt) plot3(obj));
            uimenu(UIC, 'Label', 'Show Fit Result', ...
                'Callback', @(src, evt) ShowFitResult(obj));
            
            % set the context menu to all the children of the HGGroup.
            Ch = get(obj.HGGroup, 'Children');
            for iCh = 1:numel(Ch)
                set(Ch(iCh), 'UIContextMenu', UIC);
            end
            
            % call the plot method to update the plot
            plot(obj);
        end
        
        function delete(obj)
            % destructor
            
            % only the hggroup has to be deleted, because all graphics
            % elements have this as their parent.
            delete(obj.HGGroup);
        end
        
        function plot(obj)
            % plots the PEAKFIND.
            
            % update the data of all the graphics elements:
            
            % rectangle center
            set(obj.RectangleCenter_Handle, 'XData', obj.x0, ...
                'YData', obj.y0);
            
            % marker showing the expected x0, y0
            set(obj.Expected_Handle, 'XData', [obj.x0, obj.x0_Expected], ...
                'YData', [obj.y0, obj.y0_Expected]);
            
            % if fit result is not available
            if isempty(obj.FitResult)
                % reset data of the fit center handle and the connector
                % handle
                set(obj.FitCenter_Handle, 'XData', [], 'YData', []);
                set(obj.Connector_Handle, 'XData', [], 'YData', []);
                % make the edge color of the rectangle red, in order to
                % visualize not successful fitting process
                set(obj.Rectangle_Handle, 'EdgeColor', 'r');
            else
                % fitting successful --> edge color of rectangle should be
                % green
                set(obj.Rectangle_Handle, 'EdgeColor', 'g');
                % set the fit center to the x0 and y0 obtained from the fit
                set(obj.FitCenter_Handle, 'XData', obj.FitResult.x0, ...
                    'YData', obj.FitResult.y0);
                % set the data of the line connecting the rectangle center
                % to the fit result x0/y0.
                set(obj.Connector_Handle, ...
                    'XData', [obj.x0 obj.FitResult.x0], ...
                    'YData', [obj.y0 obj.FitResult.y0]);
            end
            % update rectangle position
            set(obj.Rectangle_Handle, 'Position', [obj.x0-obj.Width/2, ...
                obj.y0-obj.Height/2, obj.Width, obj.Height]);
            % update the corner and side markers of the rectangle
            set(obj.RectangleCorners_Handle, ...
                'XData', [obj.x0-obj.Width/2 obj.x0 ...
                obj.x0+obj.Width/2 obj.x0+obj.Width/2 ...
                obj.x0+obj.Width/2 obj.x0 ...
                obj.x0-obj.Width/2 obj.x0-obj.Width/2], ...
                'YData', [obj.y0-obj.Height/2 obj.y0-obj.Height/2 ...
                obj.y0-obj.Height/2 obj.y0 ...
                obj.y0+obj.Height/2 obj.y0+obj.Height/2 ...
                obj.y0+obj.Height/2 obj.y0]);
            % update the position of the fitting textbox.
            set(obj.FittingTB_Handle, 'Position', [obj.x0, obj.y0]);
        end
        
        function plot3(obj)
            % plots the fit result including the data in a 3-d plot.
            
            % extract the data points which are lying inside the rectangle
            L = obj.XData(:) > obj.x0-obj.Width/2 & ...
                obj.XData(:) < obj.x0+obj.Width/2 & ...
                obj.YData(:) > obj.y0-obj.Height/2 & ...
                obj.YData(:) < obj.y0+obj.Height/2;
            
            if sum(L) == 0
                msgbox('Plotting not possible.', 'modal');
                return;
            end
            
            % find the minimum and maximum x and y values
            XMin = min(obj.XData(L));
            XMax = max(obj.XData(L));
            YMin = min(obj.YData(L));
            YMax = max(obj.YData(L));
            
            % create x/y grid where the fit should be evaluated
            xFit = linspace(XMin, XMax, 100);
            yFit = linspace(YMin, YMax, 100);
            [XFit, YFit] = meshgrid(xFit, yFit);
            
            % create figure
            f = figure;
            % create axis
            ax = axes('Parent', f);
            % plot the data points inside the rectangle
            plot3(obj.XData(L), obj.YData(L), obj.ZData(L), 'k.', ...
                'Parent', ax);
            % if fit result is available
            if ~isempty(obj.FitResult)
                hold(ax, 'on');
                % plot the surface of the fit result
                Z = obj.FitResult(XFit, YFit);
                surf(xFit, yFit, Z, 'EdgeColor', 'none', 'Parent', ax, ...
                    'FaceAlpha', 0.8);
                hold(ax, 'off');
            end
        end
        
        function PerformFit(obj)
            % Performs the fitting process on the PEAKFind.
            
            % check for availability of the curve fitting toolbox
            [stat, ~] = license('checkout', 'Curve_Fitting_Toolbox');
            if ~stat
                msgbox('Fitting requires the Curve Fitting Toolbox, which is not available.', 'modal')
                return;
            end
            
            % set the Fitting property to true
            obj.Fitting = true;
            
            % find data inside the selection rectangle
            L = obj.XData > obj.x0-obj.Width/2 & ...
                obj.XData < obj.x0+obj.Width/2 & ...
                obj.YData > obj.y0-obj.Height/2 & ...
                obj.YData < obj.y0+obj.Height/2;

            % extract data inside selection rectangle
            X_ = obj.XData(L);
            Y_ = obj.YData(L);
            Z_ = obj.ZData(L);            
            
            % perform the actual fit
            obj.FitResult = TwoDimGaussianFit(X_, Y_, Z_);
            
            if isempty(obj.FitResult)
                % fit failed, we don't have to do anything
            elseif obj.FitResult.x0 < obj.x0-obj.Width/2 || ...
                    obj.FitResult.x0 > obj.x0+obj.Width/2 || ...
                    obj.FitResult.y0 < obj.y0-obj.Height/2 || ...
                    obj.FitResult.y0 > obj.y0+obj.Height/2
                % checking if peak position determined by fit is outside
                % the selection rectangle --> if yes, throw away fit result
                obj.FitResult = [];
            end
            
            % finished fitting, set Fitting property to false
            obj.Fitting = false;
            % raise FinishedFitting event
            notify(obj, 'FinishedFitting');
        end
    end
    
    methods (Access = protected)
        function ShowFitResult(obj)
            % function showing the fit result in a table
            
            % check if fit result is available
            if isempty(obj.FitResult)
                msgbox('No fitting result available.', 'modal')
                return;
            end
            
            % read out all the results
            DN = dependnames(obj.FitResult);
            IDN = indepnames(obj.FitResult);
            CN = coeffnames(obj.FitResult);
            CV = coeffvalues(obj.FitResult);
            CI = confint(obj.FitResult);
            C = cell(size(CN, 1), 5);
            for iA = 1:size(CN, 1)
                C{iA, 1} = CN{iA};
                C{iA, 2} = CV(iA);
                C{iA, 3} = (CI(2, iA)-CI(1, iA))/2;
                C{iA, 4} = CI(1, iA);
                C{iA, 5} = CI(2, iA);
            end
            
            % create figure with the result displayed
            f = figure('WindowStyle', 'modal', 'Toolbar', 'none', ...
                'Name', 'Fit Result');
            uicontrol('Style', 'text', 'Units', 'normalized', ...
                'Position', [0.05 0.66 0.9 0.32], 'String', ...
                sprintf('Model:\n\t%s(%s, %s) = %s\n\nBounding Box:\n[%g, %g, %g, %g]\n\nCoefficients (with 95%% confidence bounds):', ...
                DN{1}, IDN{1}, IDN{2}, formula(obj.FitResult), ...
                obj.x0-obj.Width/2, obj.y0-obj.Height/2, obj.Width, ...
                obj.Height), ...
                'Parent', f, 'HorizontalAlignment', 'left', ...
                'FontSize', 10);
            uitable(f, 'Units', 'normalized', 'FontSize', 10, ...
                'Position', [0.05 0.05 0.9 0.66], 'Data', C, 'ColumnName', ...
                {'Parameter', 'Value', 'Plus/Minus', 'Lower Bound', 'Upper Bound'}); 
            
        end
        
        function RectangleSize_Callback(obj)
            % function called when user clicks on a corner or edge marker
            
            % do not update position when fit process is running
            if obj.Fitting; return; end
            
            % find the figure
            f = ancestor(obj.HGGroup, 'figure');
            % check which mouse button was used for dragging. If not left
            % mouse button, leave callback function.
            if ~strcmp(get(f, 'SelectionType'), 'normal'); return; end
            % get the axes of the figure
            ax = get(f, 'CurrentAxes');
            % read out cursor position
            P = get(ax, 'CurrentPoint');
            % find corner user drags by finding the corner with the minimum
            % distance to the cursor position.
            xData = get(obj.RectangleCorners_Handle, 'XData');
            yData = get(obj.RectangleCorners_Handle, 'YData');
            d = sqrt((xData-P(1, 1)).^2+(yData-P(1, 2)).^2);
            [~, ind] = min(d);
            % set the WindowButtonMotionFcn (executed when user drags)
            set(f, 'WindowButtonMotionFcn', @(src, evt) obj.RectangleSizeChange_Callback(ind));
            % set the WindowButtonUpFcn (executed when user releases mouse
            % button)
            set(f, 'WindowButtonUpFcn', @(src, evt) obj.WindowButtonUpFcn_Callback);
        end
        
        function Rectangle_BtnDown(obj)
            % callback executed when user clicks on marker for moving the
            % rectangle position.
            
            % do not react if fit process is running
            if obj.Fitting; return; end
            % find figure
            f = ancestor(obj.HGGroup, 'figure');
            % check which mouse button was used. If not left, leave the
            % callback.
            if ~strcmp(get(f, 'SelectionType'), 'normal'); return; end
            % set the WindowButtonMotionFcn (executed when user drags)
            set(f, 'WindowButtonMotionFcn', @(src, evt) obj.WindowButtonMotionFcn_Callback);
            % set the WindowButtonUpFcn (executed when user releases mouse
            % button)
            set(f, 'WindowButtonUpFcn', @(src, evt) obj.WindowButtonUpFcn_Callback);
        end
        
        function WindowButtonMotionFcn_Callback(obj)
            % function executed when user drags the center marker, on of
            % the corner markers or one of the edge markers.
            
            % get figure
            f = ancestor(obj.HGGroup, 'figure');
            % get current axes
            ax = get(f, 'CurrentAxes');
            % get cursor position
            P = get(ax, 'CurrentPoint');
            % set the x0 and y0 property of the PeakFind
            obj.x0 = P(1, 1);
            obj.y0 = P(1, 2);
            % call the plot method to update all the graphics elements
            plot(obj);
        end
        
        function RectangleSizeChange_Callback(obj, ind)
            % function executing when user drags a corner or an edge marker
            % of the rectangle.
            % ind defines which of the 8 markers is dragged:
            % 7 ----- 6 ----- 5
            % |               |
            % 8               4
            % |               |
            % 1 ----- 2 ----- 3
            
            % get figure
            f = ancestor(obj.HGGroup, 'figure');
            % get axes
            ax = get(f, 'CurrentAxes');
            % get cursor position
            P = get(ax, 'CurrentPoint');
            % get the x and y data of the corner and edge markers
            xData = get(obj.RectangleCorners_Handle, 'XData');
            yData = get(obj.RectangleCorners_Handle, 'YData');
            % calculate the change, i.e. the amount the user dragged the
            % marker
            DeltaX = P(1, 1) - xData(ind);
            DeltaY = P(1, 2) - yData(ind);
            
            % depending on the marker dragged, change the center, width and
            % height of the rectangle
            if ind == 1 || ind == 7 || ind == 8
                if ~(obj.Width-DeltaX < 0)
                    obj.x0 = obj.x0 + DeltaX/2;
                    obj.Width = obj.Width - DeltaX;
                end
            elseif ind == 3 || ind == 4 || ind == 5
                if ~(obj.Width+DeltaX < 0)
                    obj.x0 = obj.x0 + DeltaX/2;
                    obj.Width = obj.Width + DeltaX;
                end
            end
            if ind == 1 || ind == 2 || ind == 3
                obj.y0 = obj.y0 + DeltaY/2;
                obj.Height = obj.Height - DeltaY;
            elseif ind == 5 || ind == 6 || ind == 7
                obj.y0 = obj.y0 + DeltaY/2;
                obj.Height = obj.Height + DeltaY;
            end
            
            % calling the plot method updates the plot
            plot(obj);
        end
        
        function WindowButtonUpFcn_Callback(obj)
            % called when the user releases the mouse button after clicking
            % an element
            
            % get figure
            f = ancestor(obj.HGGroup, 'figure');
            % reset WindowButtonMotionFcn
            set(f, 'WindowButtonMotionFcn', '');
            % reset WindowButtonUpFcn
            set(f, 'WindowButtonUpFcn', '');
            % update the fit
            PerformFit(obj);
            % update the plot
            plot(obj);
        end
    end
end