function InitializeEiger(GIDVisPluginPath)
% To read the Eiger files which are compressed with external filters the
% directory of the plugin files has to be set to the environment variable
% 'HDF5_PLUGIN_PATH'. InitializeEiger performs this task.
% On Windows, the environment variable is set by running the setx command.
% This was the only reliable way to set the variable.
% On Linux, the user has to set the environment variable by himself. I
% could not find a way to do this from MATLAB so that reading Eiger files
% works.
%
% Refer also to the section "Importing HDF5 Files" in the MATLAB
% documentation and to the 'HDF5 Dynamically Loaded Filters - The HDF
% Group' pdf available online (https://support.hdfgroup.org/HDF5/doc/Advanced/DynamicallyLoadedFilters/HDF5DynamicallyLoadedFilters.pdf).
%
% Usage:
%     InitializeEiger(GIDVisPluginPath)
% 
% Input:
%     GIDVisPluginPath ... path to the directory containing the plugins
%
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2019 Benedikt Schrode                                     %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

cmdout = '';

if ispc
    % check if there is already a environment variable called HDF5_PLUGIN_PATH
    OldValue = getenv('HDF5_PLUGIN_PATH');
    if isempty(OldValue)
        % just set the environment variable
        command = sprintf('setx HDF5_PLUGIN_PATH "%s"', GIDVisPluginPath);
        [status, cmdout] = system(command);
    else
        % separate the old values at the semicolon
        OldValues = strsplit(OldValue, ';');
        % append the path to the already existing environment variable
        if ~any(ismember(GIDVisPluginPath, OldValues))
            OldValues{end+1} = GIDVisPluginPath;
            % to work, the GIDVisPluginPath has to be the first one in the
            % environment variable
            OldValues = fliplr(OldValues);
            command = sprintf('setx HDF5_PLUGIN_PATH "%s"', strjoin(OldValues, ';'));
            [status, cmdout] = system(command);
        else
            status = inf; % the environment variable already exists. Set status to inf
        end
    end
    
    if status == 0
        msgbox(sprintf('Successfully set the environment variable ''HDF5_PLUGIN_PATH'' to ''%s''.\n\nRestart GIDVis and MATLAB to make changes effective.', GIDVisPluginPath), 'modal')
    elseif isinf(status)
        msgbox(sprintf('The environment variable ''HDF5_PLUGIN_PATH'' already exists with the path ''%s''.\n\n No changes applied.', GIDVisPluginPath), 'modal');
    else
        msgbox(sprintf('Setting the environment variable ''HDF5_PLUGIN_PATH'' failed with the following message:\n\n%s', cmdout), 'modal');
    end
else
    command = sprintf('export HDF5_PLUGIN_PATH="%s"', GIDVisPluginPath);
    CopiedMessage = 'It was tried to copy the above mentioned command to the clipboard.';
    try % try to copy the command to the clipboard
        clipboard('copy', command)
    catch % if copy process failed, reset CopiedMessage
        CopiedMessage = '';
    end
    if isdeployed
        msgbox(sprintf(['Please set the environment variable ''HDF5_PLUGIN_PATH'' to the following value before starting GIDVis: %s\n\n', ...
            'You can do this by adding the following command to the run_GIDVis_xxxx_xx_xx.sh file:\n\n%s\n\nThen, start GIDVis by running run_GIDVis_xxxx_xx_xx.sh.\n\n', ...
            CopiedMessage], ...
            GIDVisPluginPath, command), 'modal')
    else
        msgbox(sprintf(['Please set the environment variable ''HDF5_PLUGIN_PATH'' to the following value before starting MATLAB: %s\n\n', ...
            'You can do this by e.g. running the following command in the terminal:\n\n%s\n\nThen, start MATLAB from the same terminal.\n\n', ...
            'Please refer to general help to create a persistent environment variable in your system.', ...
            CopiedMessage], ...
            GIDVisPluginPath, command), 'modal')
    end
end