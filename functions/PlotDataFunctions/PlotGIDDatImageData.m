function PlotGIDDatImageData(AxesHandle, ToolboxHandle, qDataObject, CorrectedData)
% This function plots the map of the data from the qData object. In it, it
% takes care of the conversion to various spaces, the color limits, the
% axes limits, the intensity scaling (e.g. linear, sqrt, log) and the
% correct y-axis direction.
% This function does not apply intensity corrections to the data!
%
% Usage:
%     PlotGIDDatImageData(AxesHandle, ToolboxHandle, qDataObject, CorrectedData)
%
% Input:
%     AxesHandle ... handle of the axes to plot into
%     ToolboxHandle ... handle of an UI element of the toolbox module
%                       window
%     qDataObject ... the qData element to plot
%     CorrectedData ... the intensity data as n x 1 vector, with intensity
%                       corrections applied if necessary
%
%
% This file is part of GIDVis.
%
% see also: qData, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ToolboxData = guidata(ToolboxHandle);
contents = get(ToolboxData.PM_ColorMap, 'String');
ReqColormap = contents{get(ToolboxData.PM_ColorMap, 'Value')};
contents = get(ToolboxData.PM_Space, 'String');
Space = contents{get(ToolboxData.PM_Space, 'Value')};
RegridResolution = [str2double(get(ToolboxData.TB_RegridResolution1, 'String')), ...
    str2double(get(ToolboxData.TB_RegridResolution2, 'String'))];
omega = ToolboxModule('GetOmega', ToolboxHandle);
chi = str2double(get(ToolboxData.TB_chi, 'String'));

if ~(strcmpi(Space, 'q') || strcmpi(Space, 'polar'))
    msgbox(['Cannot use Space ', Space, ' with qData files.'], 'modal');
    return;
end

contents = cellstr(get(ToolboxData.PM_AxesFormat, 'String'));
AxesFormat = contents{get(ToolboxData.PM_AxesFormat, 'Value')};
LockColorbar = get(ToolboxData.CB_LockColorScaling, 'Value');
contents = get(ToolboxData.PM_Scaling, 'String');
Scaling = contents{get(ToolboxData.PM_Scaling, 'Value')};

[qxy, qz, ~, ~, rho] = qDataObject.getQ(omega, chi);
% remove intensity values < 0 and the corresponding q values
L = CorrectedData < 0;
CorrectedData(L) = [];
qxy(L) = [];
qz(L) = [];
rho(L) = [];

if strcmpi(Space, 'polar')
    % transform to polar coordinates
    yData = atan2(qxy, qz)*180/pi;
    xData = rho;

    % regrid everything
    [CorrectedData, xToPlot, yToPlot] = regrid(xData, yData, CorrectedData, RegridResolution);
elseif strcmpi(Space, 'q')
    [CorrectedData, xToPlot, yToPlot] = regrid(qxy, qz, CorrectedData, RegridResolution);
end

if strcmpi(Scaling, 'log')
    CorrectedData = log(CorrectedData);
elseif strcmpi(Scaling, 'sqrt')
    CorrectedData = sqrt(CorrectedData);
end

% Update the axes labels according to the space selected.
% We have to do this before updating the data because the integrated line
% scan module will update its data as soon as it gets set. However, it will
% also read the axis labels. So to make sure to display the correct axis
% labels in the ILS, first update the labels, then the data
if strcmpi(Space, 'q')
    [Horiz, Vert] = MainAxesSettingsModule('getQAxesLabels');
elseif strcmpi(Space, 'pixel')
    [Horiz, Vert] = MainAxesSettingsModule('getPixelAxesLabels');
elseif strcmpi(Space, 'polar')
    [Horiz, Vert] = MainAxesSettingsModule('getPolarAxesLabels');
end
xlabel(AxesHandle, Horiz);
ylabel(AxesHandle, Vert);

h = findobj(AxesHandle, 'Tag', 'MainImage');
if isempty(h)
    imagesc(xToPlot, yToPlot, CorrectedData, 'Parent', AxesHandle, 'Tag', 'MainImage');
    % using imagesc deletes the axis labels, so set them again
    xlabel(AxesHandle, Horiz);
    ylabel(AxesHandle, Vert);
    ToolboxModule('Btn_AxesLimitsFull_Callback', ...
        ToolboxData.Btn_AxesLimitsFull, [], ToolboxData);
else
    set(h, 'XData', xToPlot, 'YData', yToPlot, 'CData', CorrectedData);
end

% set the colormap
colormap(AxesHandle, eval([ReqColormap, '(256)']));
if ~LockColorbar
    ColorLims = getColorBarLimits(CorrectedData);
    set(AxesHandle, 'Clim', ColorLims);
    set(ToolboxData.TB_ColorLimMin, 'String', ColorLims(1));
    set(ToolboxData.TB_ColorLimMax, 'String', ColorLims(2));
end

set(AxesHandle, 'YDir', 'normal');

axis(AxesHandle, AxesFormat);