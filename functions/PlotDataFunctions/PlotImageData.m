function PlotImageData(AxesHandle, ToolboxHandle, data, LoadFileModuleHandle)
% This function plots the map given by data. This function takes care of
% the conversion to various spaces, the color limits, the axes limits, the
% intensity scaling (e.g. linear, sqrt, log) and the correct y-axis
% direction.
% This function does not apply intensity corrections to the data!
%
% Usage:
%     PlotImageData(AxesHandle, ToolboxHandle, data, LoadFileModuleHandle)
%
% Input:
%     AxesHandle ... handle of the axes to plot into
%     ToolboxHandle ... handle of an UI element of the toolbox module
%                       window
%     data ... the intensity data as n x 1 vector, with intensity
%              corrections applied if necessary
%     LoadFileModuleHandle ... handle of an UI element of the Load File 
%                              module window
%
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Retrieve data from the toolbox
ToolboxData = guidata(ToolboxHandle);

contents = get(ToolboxData.PM_Scaling, 'String');
Scaling = contents{get(ToolboxData.PM_Scaling, 'Value')};

contents = get(ToolboxData.PM_Space, 'String');
Space = contents{get(ToolboxData.PM_Space, 'Value')};

omega = ToolboxModule('GetOmega', ToolboxHandle);

chi = str2double(get(ToolboxData.TB_chi, 'String'));

RegridResolution = [str2double(get(ToolboxData.TB_RegridResolution1, 'String')), ...
    str2double(get(ToolboxData.TB_RegridResolution2, 'String'))];

LockColorbar = get(ToolboxData.CB_LockColorScaling, 'Value');

contents = cellstr(get(ToolboxData.PM_AxesFormat, 'String'));
AxesFormat = contents{get(ToolboxData.PM_AxesFormat, 'Value')};

beamline = LoadFileModule('GetBeamline', LoadFileModuleHandle);

contents = get(ToolboxData.PM_ColorMap, 'String');
ReqColormap = contents{get(ToolboxData.PM_ColorMap, 'Value')};

data(data < 0) = NaN;

% apply the scaling to the data
if strcmpi(Scaling, 'log')
    data = log(data);
elseif strcmpi(Scaling, 'sqrt')
    data = sqrt(data);
end

% if the selected space is 'q', we have to perform the conversion from
% pixel to q
if strcmpi(Space, 'q')
    if any(size(data) < [beamline.detlenz beamline.detlenx])
        msgbox(['The size of the data read from the image is smaller than detlenx/detlenz from the beamline.', char(10), ...
            'Most likely reasons: You have loaded a scaled image and/or selected the wrong beamline.', char(10), ...
            'Use original measurement files and make sure you have selected the correct beamline and their parameters are correct.'], 'modal');
        return;
    end
    data = data(1:beamline.detlenz, 1:beamline.detlenx);
    [xToPlot, yToPlot, data] = CalculateQSpace(beamline, omega, chi, data, RegridResolution);
elseif strcmpi(Space, 'pixel')
    sz = size(data);
    xToPlot = 1:sz(2);
    yToPlot = 1:sz(1);
    
    set(get(AxesHandle, 'XLabel'), 'String', 'pixel')
    set(get(AxesHandle, 'YLabel'), 'String', 'pixel')
elseif strcmpi(Space, 'polar')
    if any(size(data) < [beamline.detlenz beamline.detlenx])
        msgbox(['The size of the data read from the image is smaller than detlenx/detlenz from the beamline.', char(10), ...
            'Most likely reasons: You have loaded a scaled image and/or selected the wrong beamline.', char(10), ...
            'Use original measurement files and make sure you have selected the correct beamline and their parameters are correct.'], 'modal');
        return;
    end
    % crop data
    data = data(1:beamline.detlenz, 1:beamline.detlenx);
    % calculate the regridded data in polar space
    [xToPlot, yToPlot, data] = CalculatePolarRegridded(beamline, omega, chi, data, RegridResolution);
end

% Update the axes labels according to the space selected.
% We have to do this before updating the data because the integrated line
% scan module will update its data as soon as it gets set. However, it will
% also read the axis labels. So to make sure to display the correct axis
% labels in the ILS, first update the labels, then the data
if strcmpi(Space, 'q')
    [Horiz, Vert] = MainAxesSettingsModule('getQAxesLabels');
elseif strcmpi(Space, 'pixel')
    [Horiz, Vert] = MainAxesSettingsModule('getPixelAxesLabels');
elseif strcmpi(Space, 'polar')
    [Horiz, Vert] = MainAxesSettingsModule('getPolarAxesLabels');
end
xlabel(AxesHandle, Horiz);
ylabel(AxesHandle, Vert);

% save the current axes limits
xL = xlim(AxesHandle);
yL = ylim(AxesHandle);

% find the main image
h = findobj(AxesHandle, 'Tag', 'MainImage');
FirstTime = isempty(h);
if FirstTime % it's the first time plotting a map, so use imagesc
    imagesc(xToPlot, yToPlot, data, 'Parent', AxesHandle, 'Tag', 'MainImage');
    % using imagesc deletes the axis labels, so set them again
    xlabel(AxesHandle, Horiz);
    ylabel(AxesHandle, Vert);
    % change the axis limits to view the full image
    ToolboxModule('Btn_AxesLimitsFull_Callback', ...
        ToolboxData.Btn_AxesLimitsFull, [], ToolboxData);
else % only update the data
    set(h, 'XData', xToPlot, 'YData', yToPlot, 'CData', data);
end

% set the colormap
colormap(AxesHandle, eval([ReqColormap, '(256)']));
if ~LockColorbar % if colorbar is not locked, ...
    ColorLims = getColorBarLimits(data); % we have to calculate new limits, ...
    set(AxesHandle, 'Clim', ColorLims); % set them for the plot, ...
    set(ToolboxData.TB_ColorLimMin, 'String', ColorLims(1)); % and in the corresponding
    set(ToolboxData.TB_ColorLimMax, 'String', ColorLims(2)); % textboxes
end

% we have to reverse the y axes in pixel space, but not in q or polar space
if strcmpi(Space, 'q') || strcmpi(Space, 'polar')
    set(AxesHandle, 'YDir', 'normal');
elseif strcmpi(Space, 'pixel')
    set(AxesHandle, 'YDir', 'reverse');
end

% update the axis format (this usually changes the axes limits)
axis(AxesHandle, AxesFormat);
if ~FirstTime
    % restore the old axes limits
    xlim(AxesHandle, xL);
    ylim(AxesHandle, yL);
end