function [data, FullHeader] = ReadDataFromBrukerFrame(PathElement, ExpSetup, intSwitch, StatusOut)
%READ_BRUKER_FRAME reads bruker frame according to the manual: 
% http://www.esrf.eu/files/live/sites/www/files/UsersAndScience/Experiments/MX/About_our_beamlines/BM29/Users/biosaxs-detectors/Vantec/gaddsref.pdf
%
% Usage:
%     data = ReadDataFromBrukerFrame(PathElement, ExpSetup, intSwitch, StatusOut)
%
% Input:
%     PathElement ... char array of a path or cell of paths or LFMElement
%                     or array of LFMElement
%     ExpSetup ... ExperimentalSetUp (see information about crop below)
%     intSwitch ... if 1, data is returned in its native class, e.g. int32. 
%                   Otherwise data is returned as double.
%     StatusOut ... handle of a uicontrol where status messages are written
%                   to by setting the String property of StatusOut or the
%                   string 'cmd' which writes status messages to the MATLAB
%                   command window.
%
% Output:
%     data ... m x n x k matrix, where m and n are the dimensions of the
%              images or the size of the image after cropping due to the
%              experimental setup and k the number of files given via 
%              PathElement
%     FullHeader ... k x 1 cell containing the header information
%
% If the data has large enough dimensions and an experimental setup is
% given, it gets cropped to the size given by detlenx and detlenz in
% ExpSetup. Otherwise, a warning will be printed.
%
% The reason why the image size and the experimental setup detector lengths
% do not fit together, can be: selected the wrong experimental setup (the
% detector in the incorrectly selected experimental setup is larger than
% the one used to measure file PathElement) or the image was scaled before
% loading it into GIDVis (often done by e.g. ImageJ).
%
% This file is part of GIDVis.
%
% see also: LFMElement, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2020 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if isa(PathElement, 'LFMElement')
    PathElement = {PathElement.Path};
end

if ~iscell(PathElement)
    PathElement = {PathElement};
end

if nargin < 2
    ExpSetup = [];
end

if nargin < 3 || isempty(intSwitch)
	intSwitch = false;
end

if nargin < 4
    StatusOut = [];
end

% initialize header cell
if nargout > 1
    FullHeader = cell(numel(PathElement), 1);
end

% loop over file paths
for iF = 1:numel(PathElement)
    % update status label
    if nargin > 3 && ~isempty(StatusOut)
        Message = sprintf('Reading file %d/%d', iF, numel(PathElement));
        if isa(StatusOut, 'matlab.ui.control.UIControl')
            set(StatusOut, 'String', Message);
            drawnow;
        elseif ischar(StatusOut) && strcmpi(StatusOut, 'cmd')
            fprintf([Message '\n'])
        end
    end
    
    % open file
    file = fopen(PathElement{iF});
    
    % read the header information if necessary
    if nargout > 1
        FullHeader{iF} = GetBrukerHeader(file);
    end
    
    %offset are calculated according to bruker gadds manual
    %the offset is calculated by the offset_value*80 (80 char per line)

    fseek(file, 0*80, 'bof');
    tline = fread(file, [1,80], '*char');
    tokens = regexp(tline, ':','split'); 
    format = str2double(tokens{2});

    if format ~= 86
        fprintf('ERROR: FORMAT IS %d. FILE INVALID!\n', format)
        data = nan;
        return
    end


    %move file pointer to HDRBLKS position
    fseek(file, 2*80, 'bof');
    tline = fread(file, [1,80], '*char');
    tokens = regexp(tline, ':','split');  
    hdrblks = str2double(tokens{2});


    %move file pointer to NOVERFL position to check for saturated pixel
    %overflow pixel are not supported at the moment but so far it has not been
    %an issue. only a warning is shown
    fseek(file, 20*80, 'bof');
    tline = fread(file, [1,80], '*char');
    tokens = regexp(tline, ':','split');  
    noverfl = str2double(tokens{2});

    %get bytes per pixel PIXELB
    fseek(file, 39*80, 'bof');
    tline = fread(file, [1,80], '*char');
    tokens = regexp(tline, ':','split');  
    pixelb = str2double(tokens{2});

    %get dimension of the detector with NROWS and NCOLS
    fseek(file, 40*80, 'bof');
    tline = fread(file, [1,80], '*char');
    tokens = regexp(tline, ':','split');  
    nrows = str2double(tokens{2});
    tline = fread(file, [1,80], '*char');
    tokens = regexp(tline, ':','split');  
    ncols = str2double(tokens{2});

    if iF == 1
        data = NaN(nrows, ncols, numel(PathElement));
    end

    %caluclate header size using hdrblocks
    header_size = 512*hdrblks;


    %set correct data format depending on pixelb
    data_format = '';

    switch pixelb
        case 1
            data_format = 'uint8';
        case 2
            data_format = 'uint16';
    end

    %skip header and move to beginning of data block
    fseek(file, header_size, 'bof');

    %read binary detector intensity, for nrows x nocols

    dataCur = fread(file, [nrows,ncols], data_format);

    %read ASCII overflow table
    %each table entry is 16 char for each pixel long: first 9 are the intensity and last 7 the
    %pixel number 2024*j + k with zero based indices according to bruker manual

    if noverfl > 0
        fprintf('NOVERLF = %d. CHECKING OVERFLOW TABLE!\n', noverfl)
        overflow_entry = fread(file, [1,noverfl*16], '*char');
        overflow_table = zeros(noverfl,2);
        for k = 1:noverfl
            %correct intensity for pixel
            overflow_table(k,2) = str2double(overflow_entry((k-1)*16+1:(k-1)*16+9));
            %pixel identifier
            overflow_table(k,1) = str2double(overflow_entry((k-1)*16+10:k*16));
        end

        %find indices of oversaturated pixel (depending on byte per pixel)
        %pixel_index = find(data == (2*pixelb)^8-1);

        %add +1 to pixel indices since bruker uses zero based indices
        dataCur(overflow_table(:,1)+1) = overflow_table(:,2);


    end

    %flipping data seems to be necessesary to make sense, not sure why but
    %otherwise it does not work
    dataCur = flipud(dataCur);

    % store current data in data matrix
    data(:, :, iF) = dataCur;
    
    fclose(file);
end

% cropping data if ExpSetup is input argument
if nargin > 1 && ~isempty(ExpSetup)
    res1 = ExpSetup.detlenx; % number of columns
    res2 = ExpSetup.detlenz; % number of rows
    datasz = size(data);
    if datasz(1) >= res2 && datasz(2) >= res1
        data = data(1:res2, 1:res1, :);
    else
        warning('GIDVis:ImportedDataDimensionMismatch', ...
            'Data dimension in selected file is lower than the size of the currently selected detector. Scaling image. Trying to convert to q space will give an error.');
    end
end

%output as int32 or as double
if intSwitch
	data = int32(data);
end

end

function Header = GetBrukerHeader(fid)
    % reads, interprets and converts (if necessary) the header information
    % from the Bruker frame format. Version 1 of the header is implemented
    % according to the table in the above mentioned manual in Appendix A
    % (pdf page 457).
    
    % Cell describing the header content. Column 1 gives the offset, column
    % 2 the display name and column 3 the format of the value (either num
    % for numeric values or char for character values).
    % In case the information is split over multiple blocks (e.g. as for
    % the unit cell parameters, offset 48 and 49), the first column of this
    % cell contains a vector with all the offset values (e.g. [48 49] in
    % case of the unit cell parameters).   
    % In case the value is a vector (type 6R or 9R in the manual table),
    % the display name should be splitted by the pipe symbol (|). See e.g.
    % the diffractometer angles, offset 39, for an example. This makes sure
    % that the values and display names are splitted into single values for
    % complete property access.
    HeaderItems = {0, 'Frame format', 'num'; ...
        1, 'Header version', 'num'; ...
        2, 'Header size (512-byte blocks)', 'num'; ...
        3, 'Kind of data', 'char'; ...
        4, 'Site name', 'char'; ...
        5, 'Diffractometer model', 'char'; ...
        6, 'User name', 'char'; ...
        7, 'Sample ID', 'char'; ...
        8, 'Basic data set name', 'char'; ...
        9, 'Run number within data set', 'num'; ...
        10, 'Specimen number within data set', 'num'; ...
        11, 'User comment #1', 'char'; ...
        12, 'User comment #2', 'char'; ...
        13, 'User comment #3', 'char'; ...
        14, 'User comment #4', 'char'; ...
        15, 'User comment #5', 'char'; ...
        16, 'User comment #6', 'char'; ...
        17, 'User comment #7', 'char'; ...
        18, 'User comment #8', 'char'; ...
        19, 'Total frame counts', 'num'; ...
        20, 'Number of overflows', 'num'; ...
        21, 'Minimum counts in a pixel', 'num'; ...
        22, 'Maximum counts in a pixel', 'num'; ...
        23, 'Number of on-time events', 'num'; ...
        24, 'Number of late events', 'num'; ...
        25, '(Original) frame filename', 'char'; ...
        26, 'Date and time of creation', 'date'; ...
        27, 'Accumulated exposure time (s)', 'num'; ...
        28, 'Requested time for last exposure (s)', 'num'; ...
        29, 'Actual time for last exposure (s)', 'num'; ...
        30, 'Nonzero if acquired by oscillation', 'num'; ...
        31, '# steps or oscillations in this frame', 'num'; ...
        32, 'Scan range (degrees)', 'num'; ...
        33, 'Starting scan angle (degrees)', 'num'; ...
        34, 'Scan angle increment between frames', 'num'; ...
        35, 'Sequence number of this frame in series (@0)', 'num'; ...
        36, 'Total number of frames in the series', 'num'; ...
        37, 'Diffractometer angle 2T|Diffractometer angle OM|Diffractometer angle PH|Diffractometer angle CH', 'num'; ...
        38, 'Number of pixels > 64K', 'num'; ...
        39, 'Number of bytes/pixel', 'num'; ...
        40, 'Number of rasters in frame', 'num'; ...
        41, 'Number of pixels/raster', 'num'; ...
        42, 'Order of bytes in word', 'num'; ...
        43, 'Order of words in a longword', 'num'; ...
        44, 'X-ray target material', 'char'; ...
        45, 'X-ray source (kV)', 'num'; ...
        46, 'X-ray source (mA)', 'num'; ...
        47, 'Filter/monochromator setting', 'char'; ...
        [48 49], 'Unit cell A|Unit cell B|Unit cell C|Unit cell ALPHA|Unit cell BETA|Unit cell GAMMA', 'num'; ...
        [50 51], 'Orientation matrix (P3 conventions)', 'num'; ...
        52, 'Low temp flag', 'num'; ...
        53, 'Zoom: Xc|Zoom: Yc|Zoom: Mag', 'num'; ...
        54, 'X of direct beam at 2-theta=0|Y of direct beam at 2-theta=0', 'num'; ...
        55, 'Sample-detector distance (cm)', 'num'; ...
        56, 'Byte pointer to trailer info', 'num'; ...
        57, 'Compression scheme ID, if any', 'char'; ...
        58, 'Linear scale, offset for pixel values', 'num'; ...
        59, 'Discriminator: Pulse height setting 1|Discriminator: Pulse height setting 2', 'num'; ...
        60, 'Preamp gain setting', 'num'; ...
        61, 'Flood table correction filename', 'char'; ...
        62, 'Brass plate correction filename', 'char'; ...
        63, 'Wavelength average|Wavelength a1|Wavelength a2', 'num'; ...
        64, 'X pixel # of maximum counts|Y pixel # of maximum counts', 'num'; ...
        65, 'Scan axis', 'num'; ...
        66, 'Two theta angle at end of frame|omega angle at end of frame|phi angle at end of frame|chi angle at end of frame', 'num'; ...
        [67 68], 'Detector position correction dX|Detector position correction dY|Detector position correction dDist|Detector position correction Pitch|Detector position correction Roll|Detector position correction Yaw', 'num'; ...
        69, 'Recommended display lookup table', 'char'; ...
        70, 'Recommended display limit 1|Recommended display limit 2', 'num'; ...
        71, 'Name and version of program writing frame', 'char'; ...
        72, 'Nonzero if acquired by rotation of phi during scan', 'num'; ...
        73, 'File name of active pixel mask associated thi this frame', 'char'; ...
        [74 75], 'Octagon mask parameter 1|Octagon mask parameter 2|Octagon mask parameter 3|Octagon mask parameter 4|Octagon mask parameter 5|Octagon mask parameter 6|Octagon mask parameter 7|Octagon mask parameter 8', 'num'; ...
        [76 77], 'Unit cell parameter A standard deviation|Unit cell parameter B standard deviation|Unit cell parameter C standard deviation|Unit cell parameter ALPHA standard deviation|Unit cell parameter BETA standard deviation|Unit cell parameter GAMMA standard deviation', 'num'; ...
        78, 'Detector type', 'char'; ...
        79, 'Number of exposures', 'num'; ...
        80, 'CCD parameter readnoise|CCD parameter e/ADU|CCD parameter e/photon|CCD parameter bias|CCD parameter full scale', 'num'; ...
        81, 'Chemical formula in CIFTAB string', 'char'; ...
        82, 'Crystal morphology in CIFTAB string', 'char'; ...
        83, 'Crystal color in CIFTAB string', 'char'; ...
        84, 'Crystal dimension (3 ea) in CIFTAB string', 'char'; ...
        85, 'Density measurement method in CIFTAB string', 'char'; ...
        86, 'Name of dark current correction', 'char'; ...
        87, 'Auto-ranging gain|Auto-ranging high-speed time|Auto-ranging scale|Auto-ranging offset|Auto-ranging full scale', 'num'; ...
        88, 'Goniometer zero correction 1|Goniometer zero correction 2|Goniometer zero correction 3|Goniometer zero correction 4', 'num'; ...
        89, 'Crystal X translation|Crystal Y translation|Crystal Z translation', 'num'; ...
        90, 'H for reciprocal space scan|K for reciprocal space scan|L for reciprocal space scan|X for reciprocal space scan|Y for reciprocal space scan', 'num'; ...
        91, 'Diffractometer setting linear axis X|Diffractometer setting linear axis Y|Diffractometer setting linear axis Z|Diffractometer setting linear axis Aux', 'num'; ...
        92, 'Actual goniometer linear axis X at end of frame|Actual goniometer linear axis Y at end of frame|Actual goniometer linear axis Z at end of frame|Actual goniometer linear axis Aux at end of frame', 'num'};

    % initialize a cell to store the header information. Column 1 will hold
    % the display name, column 2 the value
    Header = cell(0, 2);

    % loop over the header items
    for iH = 1:size(HeaderItems, 1)
        % get the current header item
        HeaderItem = HeaderItems(iH, :);
        % check if information is split over multiple blocks
        if numel(HeaderItem{1}) > 1
            % read the first block
            Offset = HeaderItem{1};
            fseek(fid, Offset(1)*80, 'bof');
            tline = fread(fid, [1, 80], '*char');
            for iN = 2:numel(HeaderItem{1})
                % read the following block
                cline = fread(fid, [1, 80], '*char');
                % split at the : character (delimiter of header keyword and
                % value)
                tokens = regexp(cline, ':', 'split', 'once');
                % append the additional information to tline (but do not
                % append the keyword a second time)
                tline = [tline, ' ', tokens{2}];
            end
        else
            % just read one block
            fseek(fid, HeaderItem{1}*80, 'bof');
            tline = fread(fid, [1, 80], '*char');
        end
        % split the string at the : delimiter
        tokens = regexp(tline, ':', 'split', 'once');
        % numeric values have to be converted to a number
        if strcmp(HeaderItem{3}, 'num')
            % convert value to a number. Use str2num here to make
            % conversion of multiple numbers to a vector possible
            val = str2num(tokens{2});
            % split the display name at the pipe character
            Str = strsplit(HeaderItem{2}, '|');
            % loop over the elements
            for iv = 1:min([numel(val) numel(Str)])
                % append to the Header cell
                Header{end+1, 1} = Str{iv};
                Header{end, 2} = val(iv);
            end
        elseif strcmp(HeaderItem{3}, 'date')
            % try to convert to datetime
            try
                Value = datetime(strtrim(tokens{2}));
            catch
                Value = strtrim(tokens{2});
            end
            Header{end+1, 1} = HeaderItem{1};
            Header{end, 2} = Value;
        elseif strcmp(HeaderItem{3}, 'char')
            % simply append to the header cell
            Header{end+1, 1} = HeaderItem{2};
            Header{end, 2} = strtrim(tokens{2});
        end
    end
end