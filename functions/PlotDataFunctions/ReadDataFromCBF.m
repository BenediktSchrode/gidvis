function [data, header] = ReadDataFromCBF(PathElement, ExpSetup, intSwitch, StatusOut)
% ReadDataFromCBF Reads data from the cbf file given by PathElement.
%
% During the cbf file import, (adapted) source code written by J. Lewis
% Muir for the software library Jcbf is used. Please see the license text
% of the Jcbf software package in
% /functions/ExternalFunctions/jcbf/LICENSE.txt
%
% Usage:
%     [data, header] = ReadDataFromCBF(PathElement, ExpSetup, intSwitch, StatusOut)
% 
% Input:
%     PathElement ... char array of a path or cell of paths or LFMElement 
%                     or array of LFMElement
%     ExpSetup ... ExperimentalSetUp (see information about crop below)
%     intSwitch ... if 1, data is returned as int32. Otherwise data is
%                   returned as double.
%     StatusOut ... handle of a uicontrol where status messages are written
%                   to by setting the String property of StatusOut or the
%                   string 'cmd' which writes status messages to the MATLAB
%                   command window.
%
% Output:
%     data ... m x n x k matrix, where m and n are the dimensions of the
%              images or the size of the image after cropping due to the
%              experimental setup and k the number of elements given via 
%              PathElement
%     header ... k x 1 cell containing the header information
%
% If the data has large enough dimensions and an experimental setup is
% given, it gets cropped to the size given by detlenx and detlenz in
% ExpSetup. Otherwise, a warning will be printed.
%
% The reason why the image size and the experimental setup detector lengths
% do not fit together, can be: selected the wrong experimental setup (the
% detector in the incorrectly selected experimental setup is larger than
% the one used to measure file PathElement) or the image was scaled before
% loading it into GIDVis (often done by e.g. ImageJ).
%
% This file is part of GIDVis.
%
% see also: LFMElement, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2020 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if isa(PathElement, 'LFMElement')
    PathElement = {PathElement.Path};
end
if ~iscell(PathElement)
    PathElement = {PathElement};
end
if nargin < 2
    ExpSetup = [];
end
if nargin < 3 || isempty(intSwitch)
    intSwitch = false;
end
if nargin < 4
    StatusOut = [];
end

header = cell(numel(PathElement), 1);

data = [];

% loop over file paths
for iF = 1:numel(PathElement)
    % update status label
    if nargin > 3 && ~isempty(StatusOut)
        Message = sprintf('Reading file %d/%d', iF, numel(PathElement));
        if isa(StatusOut, 'matlab.ui.control.UIControl')
            set(StatusOut, 'String', Message);
            drawnow;
        elseif ischar(StatusOut) && strcmpi(StatusOut, 'cmd')
            fprintf([Message '\n'])
        end
    end
    
    ROData = ImportCBF.ReadOutCBFPixels(PathElement{iF}); % ROData contains the pixel data and the size of the array in the last two values
    if isempty(ROData)
        msgbox(['Could not read file ', PathElement{iF}, '.'], 'modal');
        continue;
    end
    % get size of image
    header{iF} = [ROData(end-1) ROData(end)];
    % initialize result array
    if iF == 1
        data = NaN(ROData(end), ROData(end-1), numel(PathElement));
    end
    % store image data
    data(:, :, iF) = reshape(ROData(1:end-2), ROData(end-1), ROData(end))';
end

% cropping data if ExpSetup is input argument
if nargin > 1 && ~isempty(ExpSetup)
    res1 = ExpSetup.detlenx; % number of columns
    res2 = ExpSetup.detlenz; % number of rows
    datasz = size(data);
    if datasz(1) >= res2 && datasz(2) >= res1
        data = data(1:res2, 1:res1, :);
    else
        warning('GIDVis:ImportedDataDimensionMismatch', ...
            'Data dimension in selected file is lower than the size of the currently selected detector. Scaling image. Trying to convert to q space will give an error.');
    end
end

%output as int32 or as double
if intSwitch
	data = int32(data);
end