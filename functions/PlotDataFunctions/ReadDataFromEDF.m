function [data, FullHeader] = ReadDataFromEDF(PathElement, ExpSetup, intSwitch, StatusOut)
% ReadDataFromEDF Reads data from the edf file given by PathElement. 
% 
% Usage:
%     [data, FullHeader] = ReadDataFromEDF(PathElement, ExpSetup, intSwitch, StatusOut)
%
% Input:
%     PathElement ... char array of a path or cell of paths or LFMElement 
%                     or array of LFMElement
%     ExpSetup ... ExperimentalSetUp (see information about crop below)
%     intSwitch ... if 1, data is returned as int32. Otherwise data is
%                   returned as double.
%     StatusOut ... handle of a uicontrol where status messages are written
%                   to by setting the String property of StatusOut or the
%                   string 'cmd' which writes status messages to the MATLAB
%                   command window.
%
% Output:
%     data ... m x n x k matrix, where m and n are the dimensions of the
%              images or the size of the image after cropping due to the
%              experimental setup and k the number of files given via 
%              PathElement
%     FullHeader ... k x 1 cell containing the header information
%
% If the data has large enough dimensions and an experimental setup is
% given, it gets cropped to the size given by detlenx and detlenz in
% ExpSetup. Otherwise, a warning will be printed.
%
% The reason why the image size and the experimental setup detector lengths
% do not fit together, can be: selected the wrong experimental setup (the
% detector in the incorrectly selected experimental setup is larger than
% the one used to measure file PathElement) or the image was scaled before
% loading it into GIDVis (often done by e.g. ImageJ).
%
% This file is part of GIDVis.
%
% see also: LFMElement, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2020 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if isa(PathElement, 'LFMElement')
    PathElement = {PathElement.Path};
end

if ~iscell(PathElement)
    PathElement = {PathElement};
end

if nargin < 3 || isempty(intSwitch)
    intSwitch = false;
end

% initialize header cell
if nargout > 1
    FullHeader = cell(numel(PathElement), 1);
end

LessThan83 = verLessThan('matlab', '8.3');

% initialize a data variable
data = [];

% loop over file paths
for iF = 1:numel(PathElement)
    % update status label
    if nargin > 3 && ~isempty(StatusOut)
        Message = sprintf('Reading file %d/%d', iF, numel(PathElement));
        if isa(StatusOut, 'matlab.ui.control.UIControl')
            set(StatusOut, 'String', Message);
            drawnow;
        elseif ischar(StatusOut) && strcmpi(StatusOut, 'cmd')
            fprintf([Message '\n'])
        end
    end
    
    header{3} = struct;
    
    % initialize
    motors = containers.Map;
    counters = containers.Map;
    edf_file = fopen(PathElement{iF}, 'r');

    motor_pos = '';
    motor_mne = '';
    counter_pos = '';
    counter_mne = '';

    dim1 = 0;
    dim2 = 0;

    % get the first line to check for valid edf file: an edf file should
    % start with an open curly brace { in the first line
    tline = fgets(edf_file);
    if numel(tline) < 1 || ~strcmp(tline(1), '{')
        warning('GIDVis:ReadDataFromEDF', 'Invalid edf file %s.', PathElement{iF});
        continue;
    end
    while 1
        tline = fgets(edf_file);
        %stop at end of header
        if strcmp(strtrim(tline), '}')
            break;
        end

        %split lines into names and values
        tokens = regexp(tline, '=','split');    

        %motor names and positions
        if strcmp(strtrim(tokens{1}), 'motor_mne')
            motor_mne = strtrim(tokens{2});
            continue;
        end

        if strcmp(strtrim(tokens{1}), 'motor_pos')
            motor_pos = strtrim(tokens{2});
            continue;
        end

        %counter names and positions
        if strcmp(strtrim(tokens{1}), 'counter_mne')
            counter_mne = strtrim(tokens{2});
            continue;
        end

        if strcmp(strtrim(tokens{1}), 'counter_pos')
            counter_pos = strtrim(tokens{2});
            continue;
        end

        %get detector size
        if strcmp(strtrim(tokens{1}), 'Dim_1')
            dim1 = strtrim(tokens{2});
            continue;
        end

        if strcmp(strtrim(tokens{1}), 'Dim_2')
            dim2 = strtrim(tokens{2});
            continue;
        end
        
        % get all other information
        if numel(tokens) == 2
            Value = strtrim(strrep(tokens{2}, ';', ''));
            NumValue = str2num(Value);
            if ~isempty(NumValue)
                Value = NumValue;
            end
            if LessThan83
                header{3}.(genvarname(strtrim(tokens{1}))) = Value;
            else
                header{3}.(matlab.lang.makeValidName(strtrim(tokens{1}))) = Value;
            end
        end
    end

    %split the lines into single names and positions
    motor_pos = strread(motor_pos,'%s','delimiter',' ');
    motor_mne = strread(motor_mne,'%s','delimiter',' ');

    %split the lines into single names and positions
    counter_pos = strread(counter_pos,'%s','delimiter',' ');
    counter_mne = strread(counter_mne,'%s','delimiter',' ');

    %create dictionary for easy use of motor positions
    for k = 1:numel(motor_pos)-1
        motors(char(motor_mne(k))) = str2double(motor_pos(k));
    end

    %create dictionary for easy use of counter positions
    for k = 1:numel(counter_pos)-1
        counters(char(counter_mne(k))) = str2double(counter_pos(k));
    end

    %convert to double and remove ; at the end
    dim1 = str2double(dim1(1:end-2));
    dim2 = str2double(dim2(1:end-2));
    
    if numel(data) == 0 && isempty(data)
        data = zeros(dim2, dim1, numel(PathElement), 'int32');
    end

    % save motors and counters in header cell
    header{1} = motors;
    header{2} = counters;

    %read binary data block as defined by dim1 and dim2
    dataCur = fread(edf_file, [1,dim1*dim2], '*int32');

    % close data file
    fclose(edf_file);

    %check data integrity, because sometimes a few bytes are missing
    %copy data in a nan array, missing data at the end will be nan's
    %it seems at only files are affected if the first line of the binary block
    %is CR, so the files should by ok just some strange behaviour while reading
    %the data, maybe a linux windows issue of CR
    if numel(dataCur) ~= dim1*dim2
        read_bytes = numel(dataCur);
        dataCur = nan(1,dim1*dim2);   
        fprintf('WARNING: ONLY %d BYTES READ OF %d, DATA SET TO NAN\n', read_bytes, numel(dataCur));    
    end


    dataCur = transpose(reshape(dataCur, dim1, dim2));
    
    data(:, :, iF) = dataCur;
    
    if nargout > 1
        % convert the header
        FullHeader{iF} = [map2cell([header{1}; header{2}]); ...
            [fieldnames(header{3}) struct2cell(header{3})]];
    end
end

% cropping data if ExpSetup is input argument
if nargin > 1 && ~isempty(ExpSetup)
    res1 = ExpSetup.detlenx; % number of columns
    res2 = ExpSetup.detlenz; % number of rows
    datasz = size(data);
    if datasz(1) >= res2 && datasz(2) >= res1
        data = data(1:res2, 1:res1, :);
    else
        warning('GIDVis:ImportedDataDimensionMismatch', ...
            'Data dimension in selected file is lower than the size of the currently selected detector. Scaling image. Trying to convert to q space will give an error.');
    end
end

%output as int32 or as double
if ~intSwitch
	data = double(data);
end