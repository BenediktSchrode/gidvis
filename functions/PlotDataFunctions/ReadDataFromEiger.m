function [data, header] = ReadDataFromEiger(PathElement, ExpSetup, intSwitch, StatusOut)
% ReadDataFromEiger Reads data from the Eiger master file given by PathElement. 
% 
% Usage:
%     [data, header] = ReadDataFromEiger(PathElement, ExpSetup, intSwitch, StatusOut)
%
% Input:
%     FileElement ... LFMElement or array of LFMElement
%     ExpSetup ... ExperimentalSetUp (see information about crop below)
%     intSwitch ... if 1, data is returned in its native format, e.g. as 
%                   int32. Otherwise data is returned as double.
%     StatusOut ... handle of a uicontrol where status messages are written
%                   to by setting the String property of StatusOut or the
%                   string 'cmd' which writes status messages to the MATLAB
%                   command window.
%
% Output:
%     data ... m x n x k matrix, where m and n are the dimensions of the
%              images or the size of the image after cropping due to the
%              experimental setup and k the number of elements given via 
%              PathElement
%     header ... k x 1 cell containing the header information
%
% If the data has large enough dimensions and an experimental setup is
% given, it gets cropped to the size given by detlenx and detlenz in
% ExpSetup. Otherwise, a warning will be printed.
%
% The reason why the image size and the experimental setup detector lengths
% do not fit together, can be: selected the wrong experimental setup (the
% detector in the incorrectly selected experimental setup is larger than
% the one used to measure file PathElement) or the image was scaled before
% loading it into GIDVis (often done by e.g. ImageJ).
%
% This file is part of GIDVis.
%
% see also: LFMElement, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2019-2020 Benedikt Schrode                                %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% useful information on the file format and test data
% http://download.nexusformat.org/doc/html/classes/applications/NXmx.html#nxmx
% https://www.iucr.org/__data/assets/pdf_file/0009/114120/06_Foerster.pdf
% https://github.com/nexusformat/exampledata
% https://www.dectris.com/files/content/images/products/Features%20Bilder/EIGER%20HDF5%20File-Architecture_large.png
% https://zenodo.org/record/2529979
% https://github.com/silx-kit/hdf5plugin/raw/master/hdf5plugin/VS2015/x64/H5bshuf.dll

% set some default values if not given
if nargin < 3 || isempty(intSwitch)
	intSwitch = false;
end

% get the file info of the first file
S = h5info(PathElement(1).Path, PathElement(1).h5Location);
% get the chunk size of the first file
ChunkSize = S.ChunkSize;

% read the data of the first file: the class of data1 depends on the class
% used to store the data, it can be eg. unsigned/signed 8/16/32 bit etc.
data1 = h5read(PathElement(1).Path, PathElement(1).h5Location, ...
    [1 1 PathElement(1).h5ChunkNumber], ChunkSize)';

% try to get the pixel mask (this contains information about detector gaps,
% dead pixels, etc)
try
    PixelMask = h5read(PathElement(1).h5Master, '/entry/instrument/detector/detectorSpecific/pixel_mask')';
    PixelMaskL = PixelMask ~= 0;
catch
    PixelMaskL = [];
end

% apply the pixel mask
if ~isempty(PixelMaskL)
    data1(PixelMaskL) = -1;
end

% the detector gaps in the eiger format sometimes have an intensity value
% corresponding to the maximum integer value of the used class. Get this
% value.
GapValue = intmax(class(data1));

% find rows and columns full of the GapValue
L1 = all(data1 == GapValue, 1);
L2 = all(data1 == GapValue, 2);
% replace intensity values in the gap by -1
data1(:, L1) = -1;
data1(L2, :) = -1;

if numel(PathElement) > 1
    % initialize result array
    sz = size(data1);
    % use the class of data1
    data = zeros(sz(1), sz(2), numel(PathElement), class(data1));
    data(:, :, 1) = data1;
else
    data = data1;
end

% read header of first file if requested
if nargout > 1 && ~isempty(PathElement(1).h5Master)
    % initialize header cell
    header = cell(numel(PathElement), 1);
    
    % read header
    header{1} = GetHeaderInformation(PathElement(1));
end

for iF = 2:numel(PathElement)
    % update status
    if nargin > 3 && ~isempty(StatusOut)
        Message = sprintf('Reading file %d/%d', iF, numel(PathElement));
        if isa(StatusOut, 'matlab.ui.control.UIControl')
            set(StatusOut, 'String', Message);
            drawnow;
        elseif ischar(StatusOut) && strcmpi(StatusOut, 'cmd')
            fprintf([Message '\n'])
        end
    end
    
    % get the file info of the file
    S = h5info(PathElement(iF).Path, PathElement(iF).h5Location);
    % get the chunk size of the first file
    ChunkSize = S.ChunkSize;
    % read the data of the file
    filedata = h5read(PathElement(iF).Path, PathElement(iF).h5Location, ...
        [1 1 PathElement(iF).h5ChunkNumber], ChunkSize)';
    
    % apply the pixel mask
    if ~isempty(PixelMaskL)
        data1(PixelMaskL) = -1;
    end
    
    % find rows and columns full of the GapValue
    L1 = all(filedata == GapValue, 1);
    L2 = all(filedata == GapValue, 2);
    % replace intensity values in the gap by -1
    filedata(:, L1) = -1;
    filedata(L2, :) = -1;
    
    % store data in large matrix
    data(:, :, iF) = filedata;    
    
    if nargout > 1
        % read header
        header{iF} = GetHeaderInformation(PathElement(iF));
    end
end

% cropping data if experimental setup is input argument
if nargin > 1 && ~isempty(ExpSetup)
    res1 = ExpSetup.detlenx; % number of columns
    res2 = ExpSetup.detlenz; % number of rows
    datasz = size(data);
    if datasz(1) >= res2 && datasz(2) >= res1
        data = data(1:res2, 1:res1, :);
    else
        warning('GIDVis:ImportedDataDimensionMismatch', ...
            'Data dimension in selected file is lower than the size of the currently selected detector. Scaling image. Trying to convert to q space will give an error.');
    end
end

%output as int (which means the format used to save the file) or convert
%the data to double
if ~intSwitch
	data = double(data);
end

function CHeader = GetHeaderInformation(p)
% the important header information is stored in the master file, so we have
% to read this data
CHeader = ReadDataFromGroup(p.h5Master, '/entry');


function H = ReadDataFromGroup(Path, GroupPath, Recursive)
if nargin < 3 || isempty(Recursive)
    Recursive = true;
end
S = h5info(Path, GroupPath);
H = cell(0, 2);
for iDS = 1:numel(S.Datasets)
    H{end+1, 1} = [GroupPath, '/', S.Datasets(iDS).Name];
    H{end, 2} = h5read(Path, H{end, 1});
    if strcmp(S.Datasets(iDS).Datatype.Class, 'H5T_STRING') && numel(S.Datasets(iDS).FillValue) >= 1
        H{end, 2} = strrep(H{end, 2}, S.Datasets(iDS).FillValue(1), '');
    end
    for iA = 1:numel(S.Datasets(iDS).Attributes)
        AName = S.Datasets(iDS).Attributes(iA).Name;
        AValue = S.Datasets(iDS).Attributes(iA).Value;
        H{end+1, 1} = [H{end, 1}, ' - ', AName];
        H{end, 2} = AValue;
    end
end
if Recursive
    for iG = 1:numel(S.Groups)
        H = [H; ReadDataFromGroup(Path, S.Groups(iG).Name)];
    end
end