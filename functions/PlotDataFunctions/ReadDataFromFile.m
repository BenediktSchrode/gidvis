function [data, header] = ReadDataFromFile(PathElement, ExpSetup, intSwitch, StatusOut)
% This function reads data from file(s) depending on its/their extension
%
% Usage:
%     [data, header] = ReadDataFromFile(PathElement, ExpSetup, intSwitch, StatusOut)
%
% Input:
%     PathElement ... char array of a path or cell of paths or LFMElement
%                  or array of LFMElement. If PathElement contains
%                  references to several files/elements, the extension of
%                  the first file/element is the one from which it is
%                  decided which ReadDataFrom... function to use.
%     ExpSetup ... ExperimentalSetUp (see information about crop below)
%     intSwitch ... if 1, data is returned in its native class, e.g. int32.
%                   Otherwise data is returned as double.
%     StatusOut ... handle of a uicontrol where status messages are written
%                   to by setting the String property of StatusOut or the
%                   string 'cmd' which writes status messages to the MATLAB
%                   command window.
%
% Output:
%     data ... m x n x k matrix, where m and n are the dimensions of the
%              images or the size of the image after cropping due to the
%              experimental setup and k the number of elements given via 
%              PathElement
%     header ... k x 1 cell containing the header information
%
% If the data has large enough dimensions and an experimental setup is
% given, it gets cropped to the size given by detlenx and detlenz in
% ExpSetup. Otherwise, a warning will be printed.
%
% The reason why the image size and the experimental setup detector lengths
% do not fit together, can be: selected the wrong experimental setup (the
% detector in the incorrectly selected experimental setup is larger than
% the one used to measure file PathElement) or the image was scaled before
% loading it into GIDVis (often done by e.g. ImageJ).
%
%
% This file is part of GIDVis.
%
% see also: ReadDataFromBrukerFrame, ReadDataFromCBF, ReadDataFromEDF, 
% ReadDataFromMat, ReadDataFromMccd, ReadDataFromRigakuIMG, 
% ReadDataFromTif, LFMElement, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2020 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin < 2
    ExpSetup = [];
end
% set some default values if not given
if nargin < 3 || isempty(intSwitch)
	intSwitch = false;
end
if nargin < 4
    StatusOut = [];
end

if ischar(PathElement) && ~iscell(PathElement)
    PathElement = {PathElement};
end
if iscell(PathElement)
    [~, ~, Ext] = fileparts(PathElement{1});
else
    Ext = PathElement(1).Ext;
end

if nargout == 1
    switch lower(Ext)
        case '.edf'
            data = ReadDataFromEDF(PathElement, ExpSetup, intSwitch, StatusOut);
        case {'.tif', '.tiff'}
            data = ReadDataFromTif(PathElement, ExpSetup, intSwitch, StatusOut);
        case '.giddat'
            data = cell(numel(PathElement), 1);
            for iF = 1:numel(PathElement)
                data{iF} = qData(PathElement(iF).Path);
            end
            if numel(PathElement) == 1
                data = data{1};
            end
        case '.cbf'
            data = ReadDataFromCBF(PathElement, ExpSetup, intSwitch, StatusOut);
        case '.mat'
            data = ReadDataFromMat(PathElement, ExpSetup, intSwitch, StatusOut);
        case '.mccd'
            data = ReadDataFromMccd(PathElement, ExpSetup, intSwitch, StatusOut);
        case '.img'
            data = ReadDataFromRigakuIMG(PathElement, ExpSetup, intSwitch, StatusOut);
        case '.h5'
            try
                data = ReadDataFromEiger(PathElement, ExpSetup, intSwitch, StatusOut);
            catch ex
                if strcmpi(ex.identifier, 'MATLAB:imagesci:h5read:libraryError')
                    h = msgbox({'To read this type of file format an environment variable has to be set.' 'Use ''Tools - Set Eiger Environment Variable'' in the GIDVis main window to set it.'}, 'modal');
                    uiwait(h);
                end
                data = [];
            end
        otherwise
        	% check if the extension is a BrukerFrame
            if ~isempty(regexp(Ext, '\.\d{3}$', 'once', 'ignorecase'))
                data = ReadDataFromBrukerFrame(PathElement, ExpSetup, intSwitch, StatusOut);
            else
                error('GIDVis:ReadDataFromFile', 'Unknown file format.');
            end
    end
else
    switch lower(Ext)
        case '.edf'
            [data, header] = ReadDataFromEDF(PathElement, ExpSetup, intSwitch, StatusOut);
        case {'.tif', '.tiff'}
            [data, header] = ReadDataFromTif(PathElement, ExpSetup, intSwitch, StatusOut);
        case '.giddat'
            data = cell(numel(PathElement), 1);
            for iF = 1:numel(PathElement)
                data{iF} = qData(PathElement(iF).Path);
            end
            if numel(PathElement) == 1
                data = data{1};
            end
            header = cell(numel(PathElement), 1);
        case '.cbf'
            data = ReadDataFromCBF(PathElement, ExpSetup, intSwitch, StatusOut);
            header = cell(numel(PathElement), 1);
        case '.mat'
            [data, header] = ReadDataFromMat(PathElement, ExpSetup, intSwitch, StatusOut);
        case '.mccd'
            [data, header] = ReadDataFromMccd(PathElement, ExpSetup, intSwitch, StatusOut);
        case '.img'
            [data, header] = ReadDataFromRigakuIMG(PathElement, ExpSetup, intSwitch, StatusOut);
        case '.h5'
            [data, header] = ReadDataFromEiger(PathElement, ExpSetup, intSwitch, StatusOut);
        otherwise
        	% check if the extension is a BrukerFrame
            if ~isempty(regexp(Ext, '\.\d{3}$', 'once', 'ignorecase'))
                [data, header] = ReadDataFromBrukerFrame(PathElement, ExpSetup, intSwitch, StatusOut);
            else
                error('GIDVis:ReadDataFromFile', 'Unknown file format.');
            end
    end
end

if ~strcmpi(Ext, '.GIDDat')
    L1 = all(data <= 0, 1);
    L2 = all(data <= 0, 2);

    for pInd = 1:size(data, 3)
        data(:, L1(:, :, pInd), pInd) = -1;
        data(L2(:, :, pInd), :, pInd) = -1;
    end
end