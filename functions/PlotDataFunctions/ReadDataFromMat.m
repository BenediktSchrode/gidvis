function [data, FullHeader] = ReadDataFromMat(PathElement, ExpSetup, intSwitch, StatusOut)
% ReadDataFromMat Reads data from the mat file given by PathElement. 
% 
% Usage:
%     [data, FullHeader] = ReadDataFromMat(PathElement, ExpSetup, intSwitch, StatusOut)
%
% Input:
%     PathElement ... char array of a path or cell of paths or LFMElement 
%                     or array of LFMElement
%     ExpSetup ... ExperimentalSetUp (see information about crop below)
%     intSwitch ... if 1, data is returned in its native class, e.g. int32. 
%                   Otherwise data is returned as double.
%     StatusOut ... handle of a uicontrol where status messages are written
%                   to by setting the String property of StatusOut or the
%                   string 'cmd' which writes status messages to the MATLAB
%                   command window.
%
% Output:
%     data ... m x n x k matrix, where m and n are the dimensions of the
%              images or the size of the image after cropping due to the
%              experimental setup and k the number of elements given via 
%              PathElement
%     header ... k x 1 cell containing the header information
%
% If the data has large enough dimensions and an experimental setup is
% given, it gets cropped to the size given by detlenx and detlenz in
% ExpSetup. Otherwise, a warning will be printed.
%
% The reason why the image size and the experimental setup detector lengths
% do not fit together, can be: selected the wrong experimental setup (the
% detector in the incorrectly selected experimental setup is larger than
% the one used to measure file PathElement) or the image was scaled before
% loading it into GIDVis (often done by e.g. ImageJ).
%
% This file is part of GIDVis.
%
% see also: LFMElement, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if isa(PathElement, 'LFMElement')
    PathElement = {PathElement.Path};
end

if ~iscell(PathElement)
    PathElement = {PathElement};
end

if nargin < 3 || isempty(intSwitch)
	intSwitch = false;
end

% initialize header cell
if nargout > 1
    FullHeader = cell(numel(PathElement), 1);
end

% read data from first file name
data1 = load(PathElement{1});
if ~isfield(data1, 'Intensity')
    data = [];
    return;
end

% read file info
if nargout > 1
    FullHeader{1} = data1.FileInfo;
    if ~iscell(FullHeader{1})
        FullHeader{1} = [{'File Information'}, FullHeader(1)];
    end
end

if numel(PathElement) > 1
    % initialize result array
    sz = size(data1.Intensity);
    data = NaN(sz(1), sz(2), numel(PathElement));
    data(:, :, 1) = data1.Intensity;
else
    data = data1.Intensity;
end

% loop over file paths
for iF = 2:numel(PathElement)
    if nargin > 3 && ~isempty(StatusOut)
        Message = sprintf('Reading file %d/%d', iF, numel(PathElement));
        if isa(StatusOut, 'matlab.ui.control.UIControl')
            set(StatusOut, 'String', Message);
            drawnow;
        elseif ischar(StatusOut) && strcmpi(StatusOut, 'cmd')
            fprintf([Message '\n'])
        end
    end
    
    dataN = load(PathElement{iF});
    if ~isfield(dataN, 'Intensity')
        data = [];
        return;
    end
    data(:, :, iF) = dataN.Intensity;
    if nargout > 1
        FullHeader{iF} = dataN.FileInfo;
        if ~iscell(FullHeader{iF})
            FullHeader{iF} = [{'File Information'}, FullHeader(iF)];
        end
    end
end

% cropping data if beamline is input argument
if nargin > 1 && ~isempty(ExpSetup)
    res1 = ExpSetup.detlenx; % number of columns
    res2 = ExpSetup.detlenz; % number of rows
    datasz = size(data);
    if datasz(1) >= res2 && datasz(2) >= res1
        data = data(1:res2, 1:res1, :);
    else
        warning('GIDVis:ImportedDataDimensionMismatch', ...
            'Data dimension in selected file is lower than the size of the currently selected detector. Scaling image. Trying to convert to q space will give an error.');
    end
end

%output as int32 or as double
if ~intSwitch
	data = double(data);
end