function [data,headerFull]= ReadDataFromRigakuIMG (PathElement, ExpSetup, intSwitch, StatusOut)
% ReadDataFromRigakuIMG Reads data from the img file given by PathElement. 
% 
% Usage:
%     [data, header] = ReadDataFromRigakuIMG(PathElement, ExpSetup, intSwitch, StatusOut)
%
% Input:
%     PathElement ... char array of a path or cell of paths or LFMElement 
%                     or array of LFMElement
%     ExpSetup ... ExperimentalSetUp (see information about crop below)
%     intSwitch ... if 1, data is returned in its native class, e.g. int32. 
%                   Otherwise data is returned as double.
%     StatusOut ... handle of a uicontrol where status messages are written
%                   to by setting the String property of StatusOut or the
%                   string 'cmd' which writes status messages to the MATLAB
%                   command window.
%
% Output:
%     data ... m x n x k matrix, where m and n are the dimensions of the
%              images or the size of the image after cropping due to the
%              experimental setup and k the number of elements given via 
%              PathElement
%     header ... k x 1 cell containing the header information
%
% If the data has large enough dimensions and an experimental setup is
% given, it gets cropped to the size given by detlenx and detlenz in
% ExpSetup. Otherwise, a warning will be printed.
%
% The reason why the image size and the experimental setup detector lengths
% do not fit together, can be: selected the wrong experimental setup (the
% detector in the incorrectly selected experimental setup is larger than
% the one used to measure file PathElement) or the image was scaled before
% loading it into GIDVis (often done by e.g. ImageJ).
%
% This file is part of GIDVis.
%
% see also: LFMElement, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2019-2020 Sebastian Hofer                                 %
% Copyright (C) 2020 Benedikt Schrode                                     %
%                                                                         %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if isa(PathElement, 'LFMElement')
    PathElement = {PathElement.Path};
end
if ~iscell(PathElement)
    PathElement = {PathElement};
end

if nargin < 3 || isempty(intSwitch)
    intSwitch = false;
end

headerFull = cell(numel(PathElement), 1);

for iF = 1:numel(PathElement)
    if nargin > 3 && ~isempty(StatusOut)
        Message = sprintf('Reading file %d/%d', iF, numel(PathElement));
        if isa(StatusOut, 'matlab.ui.control.UIControl')
            set(StatusOut, 'String', Message);
            drawnow;
        elseif ischar(StatusOut) && strcmpi(StatusOut, 'cmd')
            fprintf([Message '\n'])
        end
    end
    
    fID = fopen(PathElement{iF});

    header=fread(fID, 'uint8');
    header_bytes=header(3:21);
    header=header(3:end);
    header_bytes=char(header_bytes)';
    header_bytes=strsplit(header_bytes, {';','='}, 'CollapseDelimiters', false);  
    header_bytes=str2double(header_bytes{2});


    header_all=char(header(1:header_bytes))';
    header_1=strsplit(header_all, {';\n', '='}, 'CollapseDelimiters', false);
    header_1=header_1(1:end-1);
    numbers = cellfun(@str2num, header_1, 'UniformOutput', false);
    l1=cellfun(@isempty, numbers);
    numbers(l1)=header_1(l1);
    header_cell=reshape(numbers, 2, size(header_1,2)/2)';
    headerFull{iF}=header_cell;

    l_size1=strcmp(header_cell,'SIZE1');
    l_size2=strcmp(header_cell,'SIZE2');
    nrows=header_cell{l_size1(:,1), 2};
    ncols=header_cell{l_size2(:,1), 2};

    l_data_type=strcmp(header_cell, 'Data_type');
    data_type_string=header_cell{l_data_type(:,1), 2};

    switch data_type_string
        case 'unsigned long int'
            data_type='uint32';
            bytes_per_data=4;
        case 'signed char'
            data_type='int8';
            bytes_per_data=1;
        case 'unsigned char'
            data_type='uint8';  
            bytes_per_data=1;
        case 'short int'
            data_type='int16'; 
            bytes_per_data=2;
        case 'long int'
            data_type='int32'; 
            bytes_per_data=4;
        case 'unsigned short int'
            data_type='uint16';
            bytes_per_data=2;
        case 'float IEEE'
            data_type='double';
            bytes_per_data=8;
    end

    l_byte_order=strcmp(header_cell, 'BYTE_ORDER');
    byte_order_string=header_cell{l_byte_order(:,1), 2};

    switch byte_order_string
        case 'big_endian'
            byte_order='ieee-be';
        case 'little_endian'
            byte_order='ieee-le';
    end

    fseek(fID,0,'bof');
    data_raw=fread(fID, inf, ['*' data_type], byte_order);
    data_raw=data_raw(header_bytes/bytes_per_data+1:end);
    dataCur=reshape(data_raw, nrows, ncols);
    fclose(fID);

    if iF == 1
        data = -ones(ncols, nrows, numel(PathElement), data_type);
    end

    data(:, :, iF) = dataCur';
end
 
% cropping data if ExpSetup is input argument
if nargin > 1 && ~isempty(ExpSetup)
    res1 = ExpSetup.detlenx; % number of columns
    res2 = ExpSetup.detlenz; % number of rows
    datasz = size(data);
    if datasz(1) >= res2 && datasz(2) >= res1
        data = data(1:res2, 1:res1, :);
    else
        warning('GIDVis:ImportedDataDimensionMismatch', ...
            'Data dimension in selected file is lower than the size of the currently selected detector. Scaling image. Trying to convert to q space will give an error.');
    end
end

if ~intSwitch
	data=double(data);
end

