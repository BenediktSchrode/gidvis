function [data, header] = ReadDataFromTif(PathElement, ExpSetup, intSwitch, StatusOut)
% ReadDataFromTif Reads data from the tif file given by PathElement. 
% 
% Usage:
%     [data, header] = ReadDataFromTif(PathElement, ExpSetup, intSwitch, StatusOut)
%
% Input:
%     PathElement ... char array of a path or cell of paths or LFMElement
%                     or array of LFMElement
%     ExpSetup ... ExperimentalSetUp (see information about crop below)
%     intSwitch ... if 1, data is returned as int32. Otherwise data is
%                   returned as double.
%     StatusOut ... handle of a uicontrol where status messages are written
%                   to by setting the String property of StatusOut or the
%                   string 'cmd' which writes status messages to the MATLAB
%                   command window.
%
% Output:
%     data ... m x n x k matrix, where m and n are the dimensions of the
%              images or the size of the image after cropping due to the
%              experimental setup and k the number of elements given via 
%              PathElement
%     header ... k x 1 cell containing the header information
%
% If the data has large enough dimensions and an experimental setup is
% given, it gets cropped to the size given by detlenx and detlenz in
% ExpSetup. Otherwise, a warning will be printed.
%
% The reason why the image size and the experimental setup detector lengths
% do not fit together, can be: selected the wrong experimental setup (the
% detector in the incorrectly selected experimental setup is larger than
% the one used to measure file PathElement) or the image was scaled before
% loading it into GIDVis (often done by e.g. ImageJ).
%
% This file is part of GIDVis.
%
% see also: LFMElement, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2020 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% tif files contain one measurement per data file

% if the first input argument is of class LFMElement, we only extract the
% path to the tif file from the first input argument
if isa(PathElement, 'LFMElement')
    PathElement = {PathElement.Path};
end

% if PathElement is not a cell, convert it to a cell. This is necessary
% since we will loop over the elements of PathElement below. If PathElement
% is e.g. a char array, we would loop over the single letters.
if ~iscell(PathElement)
    PathElement = {PathElement};
end

% set some default values if not given
if nargin < 3 || isempty(intSwitch)
	intSwitch = false;
end

% loop over the elements
for iF = 1:numel(PathElement)
    % update status message
    if nargin > 3 && ~isempty(StatusOut)
        Message = sprintf('Reading file %d/%d', iF, numel(PathElement));
        if isa(StatusOut, 'matlab.ui.control.UIControl')
            set(StatusOut, 'String', Message);
            drawnow;
        elseif ischar(StatusOut) && strcmpi(StatusOut, 'cmd')
            fprintf([Message '\n'])
        end
    end
    
    % read header if wanted
    if nargout > 1
        if iF == 1
            % initialize cell
            header = cell(numel(PathElement), 1);
        end
        % read out image header
        header{iF} = ReadHeader(PathElement{iF});
    end
    
    if iF == 1
        % read data from first file
        data1 = imread(PathElement{iF});
        
        if numel(PathElement) > 1
            % initialize result array
            sz = size(data1);
            data = zeros(sz(1), sz(2), numel(PathElement), 'int32');
            data(:, :, 1) = data1;
        else
            data = data1;
        end
    else
        data(:, :, iF) = imread(PathElement{iF});
    end
end

% cropping data if beamline is input argument
if nargin > 1 && ~isempty(ExpSetup)
    res1 = ExpSetup.detlenx; % number of columns
    res2 = ExpSetup.detlenz; % number of rows
    datasz = size(data);
    if datasz(1) >= res2 && datasz(2) >= res1
        data = data(1:res2, 1:res1, :);
    else
        warning('GIDVis:ImportedDataDimensionMismatch', ...
            'Data dimension in selected file is lower than the size of the currently selected detector. Scaling image. Trying to convert to q space will give an error.');
    end
end

%output as int32 or as double
if ~intSwitch
	data = double(data);
end

function H = ReadHeader(PathElement)
% reads the header from a tif file
info = imfinfo(PathElement);
if isfield(info, 'DateTime')
    try
        info.DateTime = datetime(info.DateTime, ...
            'InputFormat', 'yyyy:MM:dd HH:mm:ss');
    catch
        info.DateTime = '';
    end
end
% convert to cell
H = [fieldnames(info) struct2cell(info)];
% we remove the Filename and the FileModDate from the header, this
% information is not needed because it is added later
L = strcmp(H(:, 1), 'Filename') | strcmp(H(:, 1), 'FileModDate');
H = H(~L, :);
% For XRD1 tif files, a lot of information is stored in the
% ImageDescription. Try to extract this information
if sum(ismember(H(:, 1), 'ImageDescription'))
    H = AddImageDescriptionToHeader(H);
end

function header = AddImageDescriptionToHeader(header)
% extracts the information given in header.ImageDescription and stores it
% directly back in header
LessThan83 = verLessThan('matlab', '8.3');

% find the ImageDescription
ind = ismember(header(:, 1), 'ImageDescription');

% different informations are delimited by \r\n, split at these positions
StrS = strsplit(header{ind, 2}, '\r\n');
% loop over all lines
for iS = 1:numel(StrS)
    % regular expression to filter the string as written by XRD1
    T = regexp(StrS{iS}, '#\s([a-zA-z\s,=:_2]+)\s+([0-9\+.e-]*)\s*([a-zA-Z\/\.])*$', 'tokens');
    if isempty(T); continue; end
    T = T{1};
    % put the information into the header cell
    if isempty(T{3})
        header{end+1, 1} = strtrim(T{1});
    else
        header{end+1, 1} = [strtrim(T{1}), ' (', T{3}, ')'];
    end
    header{end, 2} = str2double(T{2});
end
