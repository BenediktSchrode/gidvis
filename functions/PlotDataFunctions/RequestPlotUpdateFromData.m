function RequestPlotUpdateFromData(HandleGUIElement)
% This function can be called to force an update of the plot. It will
% retrieve already loaded data from the handles structure, other 
% parameters from the ToolboxModule. 
%
% Usage:
%     RequestPlotUpdateFromData(HandleGUIElement)
%
% Input:
%     HandleGUIElement ... handle to ANY gui element in the main
%                          window, e.g. the main axes.
% 
% Call this function from outside the GIDVis.m file as follows:
% GIDVis('RequestPlotUpdateFromData', HandleGUIElement);
%
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% the handles structure of the main window can be loaded by:
handles = guidata(HandleGUIElement);

% extract data from handles structure
if ~isfield(handles, 'RawData') % if not available, plot the data of the selected file
    RequestPlotUpdateFromFilePath(HandleGUIElement);
    return;
end
data = handles.RawData;

% plot the data, depending on the type
if isa(data, 'qData')
    % apply corrections to the data
    handles.CorrectedData = CorrectQData(handles.ToolboxModule, data);
        
    PlotGIDDatImageData(handles.MainAxes, handles.ToolboxModule, data, handles.CorrectedData);
else
    % apply the corrections to the data
    CorrectedData = CorrectPixelData(handles.ToolboxModule, ...
        LoadFileModule('GetBeamline', handles.LoadFileModule), data);

    % store corrected data in handles structure
    handles.CorrectedData = CorrectedData;
    
    % plot the data
    PlotImageData(handles.MainAxes, handles.ToolboxModule, CorrectedData, ...
        handles.LoadFileModule);
end

% Update handles structure
guidata(HandleGUIElement, handles);