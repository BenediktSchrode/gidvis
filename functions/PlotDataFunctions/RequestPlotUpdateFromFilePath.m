function Success = RequestPlotUpdateFromFilePath(HandleGUIElement)
% This function should only be called if the filepath of the image to 
% display has changed! Do not call this function if only the scaling, the 
% space, ... changes. In this case, use RequestPlotUpdateFromData!
%
% This function can be called to force an update of the plot. It will
% retrieve the file path from the LoadFileModule.
% 
% Usage:
%     Success = RequestPlotUpdateFromFilePath(HandleGUIElement)
%
% Input:
%     HandleGUIElement ... handle to ANY gui element in the main
%                          window, e.g. the main axes. 
%
% Output:
%     Success ... bool if data was successfully plotted or not
%
%
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% The handles structure of the main window can be loaded by:
handles = guidata(HandleGUIElement);

% get the selected elements
Element = LoadFileModule('GetSelectedFilePath', handles.LoadFileModule);
Element = Element(1);
if isempty(Element); Success = false; return; end

Ext = Element(1).Ext;

% read out data depending on the extension of the file
switch lower(Ext)
    case '.giddat' % reading qData
        % read out the data
        Q = qData(Element.Path);
        
        % get the selected space
        ToolboxData = guidata(handles.ToolboxModule);
        contents = get(ToolboxData.PM_Space, 'String');
        SelectedSpace = contents{get(ToolboxData.PM_Space, 'Value')};
        
        % we have to remove the possibility to set the beamtime and the
        % beamline
        UpdateToolboxGUIElements(handles, Q);
        
        % save the raw data in the handles structure
        handles.RawData = Q;
        
        % apply corrections to the data
        handles.CorrectedData = CorrectQData(handles.ToolboxModule, Q);
        
        % plot the data
        PlotGIDDatImageData(handles.MainAxes, handles.ToolboxModule, Q, handles.CorrectedData);
        
        % in case the previously selected space was pixel, automatically
        % update the axes limits to avoid confusion
        if strcmpi(SelectedSpace, 'pixel')
            % execute the Btn_AxesLimitsFull_Callback from the
            % ToolboxModule to automatically set the axes limits
            ToolboxModule('Btn_AxesLimitsFull_Callback', ToolboxData.Btn_AxesLimitsFull, [], ToolboxData);
        end
    case ''  % when no map was loaded and the user changes toolbox values
             % an Extension '' is delivered to this function. To avoid
             % throwing an error, "handle" this case here, i.e. do nothing
    otherwise % reading pixel data
        % we have to update the GUI elements in the load file module
        % (selection of beamtime and beamline)
        UpdateToolboxGUIElements(handles);

        % read data
        data = ReadDataFromFile(Element);
        
        % check if data is empty
        if isempty(data)
            Success = false;
            msgbox('Could not load data from selected file.', 'modal');
            return
        end

        % save the raw data in the handles structure
        handles.RawData = data;
        
        % apply the corrections to the data
        CorrectedData = CorrectPixelData(handles.ToolboxModule, ...
            LoadFileModule('GetBeamline', handles.LoadFileModule), data);

        % store the corrected data in the handles structure
        handles.CorrectedData = CorrectedData;
        
        % plot the corrected data
        PlotImageData(handles.MainAxes, handles.ToolboxModule, ...
            CorrectedData, handles.LoadFileModule);
end

% Update handles structure
guidata(HandleGUIElement, handles);

Success = true;