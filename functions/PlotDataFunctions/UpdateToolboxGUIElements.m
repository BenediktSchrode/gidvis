function UpdateToolboxGUIElements(varargin)
% This function updates the visibility and the enabled state of the GUI
% elements of the Toolbox and the Load File Module depending on the file
% type which is loaded. E.g. in case of the *.GIDDat file, the experimental
% set up cannot be selected anymore.
%
% Usage:
%     UpdateToolboxGUIElements(MainGIDVisHandle)
%     UpdateToolboxGUIElements(MainGIDVisHandle, qData)
%
% Input:
%     MainGIDVisHandle ... handles structure of the main GIDVis window
%     qData ... if a qData object has to be plotted, the qData object has
%               to be given to UpdateToolboxGUIElements, so that the values
%               can be set accordingly.
%
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin == 0
    error('GIDVis:UpdateToolboxGUIElements', 'At least one input required.')
end

MainHandles = varargin{1};
ToolboxData = guidata(MainHandles.ToolboxModule);
LFMData = guidata(MainHandles.LoadFileModule);

if nargin == 2 && isa(varargin{2}, 'qData') % loading qData
    Q = varargin{2};
    OnOffStr = 'off';
    contents = get(ToolboxData.PM_Space, 'String');
    Value = get(ToolboxData.PM_Space, 'Value');
    if ~strcmpi(contents{Value}, 'q') && ~strcmpi(contents{Value}, 'polar')
        set(ToolboxData.PM_Space, 'Value', find(strcmp('q', contents)));
    end
elseif nargin == 1 % loading pixel format
    OnOffStr = 'on';
    if isfield(ToolboxData.Settings, 'PM_Space_Value')
        set(ToolboxData.PM_Space, 'Value', ToolboxData.Settings.PM_Space_Value);
    end
    if isfield(ToolboxData.Settings, 'TB_omega_String')
        set(ToolboxData.TB_omega, 'String', ToolboxData.Settings.TB_omega_String);
    end
    if isfield(ToolboxData.Settings, 'TB_chi_String')
        set(ToolboxData.TB_chi, 'String', ToolboxData.Settings.TB_chi_String);
    end
    if isfield(ToolboxData.Settings, 'TB_RegridResolution1_String')
        set(ToolboxData.TB_RegridResolution1, 'String', ToolboxData.Settings.TB_RegridResolution1_String);
    end
    if isfield(ToolboxData.Settings, 'TB_RegridResolution2_String')
        set(ToolboxData.TB_RegridResolution2, 'String', ToolboxData.Settings.TB_RegridResolution2_String);
    end
end

set(LFMData.BeamtimeSelection, 'Visible', OnOffStr);
set(LFMData.BeamlineSelection, 'Visible', OnOffStr);
set(LFMData.CB_AutoDetectBeamline, 'Visible', OnOffStr);