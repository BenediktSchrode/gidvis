function limits = getColorBarLimits(data)
% GETCOLORBARLIMITS returns colorbar limits based on the data given,
% putting priority on visibility of all features.
%
% Usage:
%     limits = getColorBarLimits(data)
%
% Input:
%     data ... matrix or vector containing the data values
%
% Output:
%     limits ... two-element vector of colorbar limits
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    if isempty(data)
        limits = [0 1];
        return;
    end

    if ~isa(data, 'double'); data = double(data); end
    
    [histb, hista] = hist(data(:),1000);
    quantil = cumsum(histb)./nansum(histb);
    upperlim_colorbar = hista(find(quantil >= 0.999, 1));
    lowerlim_colorbar = hista(find(quantil >= 0.10, 1));
    
    if isempty(lowerlim_colorbar) || lowerlim_colorbar >= upperlim_colorbar || isnan(lowerlim_colorbar)
        lowerlim_colorbar = min(data(:));
    end
    if isempty(upperlim_colorbar) || isnan(upperlim_colorbar)
        upperlim_colorbar = max(data(:));
    end
    
    if (isempty(lowerlim_colorbar) && isempty(upperlim_colorbar)) || ...
            (isnan(lowerlim_colorbar) && isnan(upperlim_colorbar)) || ...
            lowerlim_colorbar == upperlim_colorbar
        lowerlim_colorbar = 0;
        upperlim_colorbar = 1;
    end
    
    limits = [lowerlim_colorbar upperlim_colorbar];
    
    if limits(2) < limits(1)
        limits = fliplr(limits);
    end
end

