function [map, qxy_lim, qz_lim] = regrid(qxy, qz, intensity_flat, targetSize)
%REGRID Bins intensity values into a new grid.
% This function bins the intensity values intensity_flat, which are lying
% at the positions qxy, qz into targetSize(1)*targetSize(2) boxes.
% targetSize(1) is the number of boxes in the vertical direction,
% targetSize(2) is the number of boxes in the horizontal direction.
%
% The first box is centered around the data point with the lowest qxy and
% lowest qz value. The spacing between the center adjacent boxes is given
% by 
%     (max(qxy)-min(qxy))/(targetSize(2)-1)
%     (max(qz)-min(qz))/(targetSize(1)-1)
%
% Boxes where no data point is in get NaN in the output argument map.
%
% NaN values in the intensity get removed during the regridding process,
% otherwise a whole box becomes NaN just because of one data point in it
% which has NaN intensity, regardless of all the other data points in the
% box.
%
% Usage:
%     [map, qxy_lim, qz_lim] = regrid(qxy, qz, intensity_flat, targetSize)
%
% Input:
%     qxy ... the qxy values
%     qz ... the qz values
%     intensity_flat ... the intensity for each data point
%     targetSize ... two-element vector with the number of boxes in the
%                    vertical and horizontal direction
%
% Output:
%     map ... intensity matrix with targetSize(1) rows and targetSize(2)
%             columns
%     qxy_lim ... the minimum and maximum qxy value
%     qz_lim ... the minimum and maximum qz value
%
% Visualize the regridded data by
%
%     imagesc(qxy_lim, qz_lim, map)
%
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% assign variables
xx = qz;
yy = qxy;
zz = intensity_flat;

% find the minimum and maximum data values
minX = min(xx);
maxX = max(xx);
minY = min(yy);
maxY = max(yy);

% calculate the indices of the boxes the data points fall into
xxBin = round( (xx-minX)/(maxX-minX)*(targetSize(1)-1) ) +1;
yyBin = round( (yy-minY)/(maxY-minY)*(targetSize(2)-1) ) +1;

% remove NaN values
L = isnan(zz);
zz(L) = [];
xxBin(L) = [];
yyBin(L) = [];

if numel(xxBin) == numel(zz)
    % map by using accumarray, take the mean of each bin
    map = accumarray([xxBin(:),yyBin(:)],zz,targetSize)./accumarray([xxBin(:),yyBin(:)],1,targetSize);
else
    msgbox(['Regridding (during the conversion from pixel to q space) not possible.', char(10), 'Most likely reasons: You have loaded a scaled image and/or selected the wrong beamline.', char(10), 'Use original measurement files and make sure you have selected the correct beamline.'], 'Regridding not possible', 'modal');
end


qxy_lim = [minY maxY];
qz_lim = [minX maxX];


end

