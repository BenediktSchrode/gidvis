function S = RecursiveDir(Path)
% Recursively visits directories and subdirectories and returns files and
% directories.
%
% Usage:
%     S = RecursiveDir(Path)
%
% Input:
%     Path ... the directory to go through
%
% Output:
%     S ... n x 1 struct containing information about the n files and
%           directories.
%
% This file is part of GIDVis.
%
% see also: dir, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

S = dir(Path);
for iFD = 1:numel(S)
    if strcmp(S(iFD).name, '.') || strcmp(S(iFD).name, '..')
        continue;
    end
    if S(iFD).isdir
        S = [S; RecursiveDir(fullfile(S(iFD).folder, S(iFD).name))];
    end
end
end