function Defaulted = RestorePositionOrDefault(hFig, Position, DefaultPosition, Units)
% This window can be used to place the window at a stored position or, if
% this is off-screen, at a default position.
%
% Usage:
%     RestorePositionOrDefault(hFig, Position)
%     RestorePositionOrDefault(hFig, Position, DefaultPosition)
%     RestorePositionOrDefault(hFig, Position, DefaultPosition, Units)
%
% Input:
%     hFig ... handle of the figure to position
%     Position ... position vector [x y width height] of the new position
%                  in pixel units
%     DefaultPosition ... position vector to place the window at if it is
%                         off-screen when placing at the given position or
%                         a string input for the movegui position. If not
%                         given, this defaults to 'onscreen'.
%     Units ... Units to use when applying the position to the window.
%               Defaults to the units of hFig.
%
% Output:
%     Defaulted ... boolean whether the default position was used or not
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2020 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin < 3 || isempty(DefaultPosition)
    DefaultPosition = 'onscreen';
end
if nargin < 4 || isempty(Units)
    Units = get(hFig, 'Units');
end

% make sure the figure is fully drawn
drawnow;

% get screensize
OldUnits = get(0, 'Units');
set(0, 'Units', Units);
Scr = get(0, 'MonitorPositions');
set(0, 'Units', OldUnits);

% get the difference in window position and window outer position
OldUnits = get(hFig, 'Units');
set(hFig, 'Units', Units);
FP = get(hFig, 'Position');
OP = get(hFig, 'OuterPosition');
DeltaX = OP(1) - FP(1);
DeltaY = OP(2) - FP(2);
DeltaW = OP(3) - FP(3);
DeltaH = OP(4) - FP(4);

% calculate the lower left and upper right corner of the window if it would
% be placed at the desired position
WindowVertices = [Position(1) Position(2); ...
    Position(1)+Position(3)+DeltaW+DeltaX-1 Position(2)+Position(4)+DeltaH+DeltaY-1];

InOrOn = false;

for iScr = 1:size(Scr, 1)
    ScreenVertices = [Scr(iScr, 1:2); ...
        Scr(iScr, 1)+Scr(iScr, 3) Scr(iScr, 2); ...
        Scr(iScr, 1)+Scr(iScr, 3) Scr(iScr, 2)+Scr(iScr, 4); ...
        Scr(iScr, 1) Scr(iScr, 2)+Scr(iScr, 4)];
    
    [In, On] = inpolygon(WindowVertices(:, 1), WindowVertices(:, 2), ...
        ScreenVertices(:, 1), ScreenVertices(:, 2));
    
    if all(In | On)
        InOrOn = true;
        break;
    end
end

if InOrOn
    set(hFig, 'Position', Position);
    Defaulted = false;
else
    if ischar(DefaultPosition)
        movegui(hFig, DefaultPosition)
    else
        set(hFig, 'Position', DefaultPosition)
    end
    Defaulted = true;
end

set(hFig, 'Units', OldUnits);