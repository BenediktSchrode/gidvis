function SaveDataToMat(Path, Intensity, FileInfo)
% this function saves the data given by Data in a mat file located at Path.
% Path should include the extension .mat.
%
% Additionally, you can enter a string which is then saved together with
% the Intensity matrix in the mat file.
%
% Usage:
%     SaveDataToMat(Path, Intensity, FileInfo)
% 
% Input:
%     Path ... filename to save to
%     Intensity ... intensity matrix to save
%     FileInfo ... n x 2 cell containing additional file information. In 
%                  the first column, keywords are given, in the second
%                  column the associated values are given.
%                  In a previous version of this function, FileInfo could 
%                  be a single string. To preserve backwards compatibility, 
%                  giving a string here will automatically convert it to a 
%                  cell with the keyword 'File Information' and the value
%                  given by the single string.
%
% Examples:
%     SaveDataToMat('FileName.mat', [1 2; 3 4])
%     SaveDataToMat('FileName.mat', [1 2; 3 4], 'This is some information')
%
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin < 3 || isempty(FileInfo)
    FileInfo = {};
end

if ~iscell(FileInfo)
    FileInfo = [{'File Information'} {FileInfo}];
end

% Check if intensity matrix can be represented as integer
if all(Intensity(:) == round(Intensity(:)))
    MinInt = min(Intensity(:));
    MaxInt = max(Intensity(:));
    if MaxInt < intmax('uint32') && MinInt > intmin('uint32')
        Intensity = uint32(Intensity);
    elseif MaxInt < intmax('int32') && MinInt > intmin('int32')
        Intensity = int32(Intensity);
    end
end

save(Path, 'Intensity', 'FileInfo');