function MaxInd = ScoreStruct(SingleStruct, StructCell)
% This function compares the structure SingleStruct with the cell of
% structures StructCell and returns the index of the structure which has
% the highest agreement with SingleStruct.
%
% The scoring process is as follow:
% Every field of the structs is compared with each other. If they are the
% same, the score gets increased by 1, if not, nothing happens. After
% comparison of all fields, the highest score is searched and its index
% returned. In case that two Structs of StructCell reach the same score,
% the first one is returned.
%
% Usage:
%     Ind = ScoreStruct(SingleStruct, StructCell)
%
% Input:
%     SingleStruct ... the single structure with fields and values which
%                      will be compared
%     StructCell ... cell of structures to which the single structure will
%                    be compared to
%
% Output:
%     Ind ... Index of the best matching structure of the cell of
%             structures
%
% Example:
%     SingleStruct = struct('A', 1, 'B', 2);
%     StructCell = {struct('A', 1, 'B', 0), struct('A', 2, 'B', 1), ...
%         struct('A', 1, 'B', 2)};
%     Ind = ScoreStruct(SingleStruct, StructCell);
% 
% returns Ind = 3 since for the third struct in StructCell, A and B is the
% same as for the SingleStruct.
%
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fieldsSingle = fields(SingleStruct);

Scores = zeros(size(StructCell));
for iF = 1:numel(fieldsSingle)
    Scores = Scores + ...
        cellfun(@(x) isfield(x, (fieldsSingle{iF})) && ...
        (x.(fieldsSingle{iF}) == SingleStruct.(fieldsSingle{iF})), StructCell);
end

[~, MaxInd] = max(Scores);