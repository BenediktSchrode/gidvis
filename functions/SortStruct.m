function SortedStructs = SortStruct(Structs, n)
% sorts the structure Struct by using the data value of the n-th field. 
% n = 1 if not given.
%
% Usage:
%     SortedStruct = SortStruct(Structs, n)
%
% Input:
%     Structs ... struct array which should be sorted according to one of
%                 the field's values
%     n ... for sorting, the n-th data value is used. Defaulted to n = 1 if
%           not given.
%
% Output:
%     SortedStructs ... struct array which are sorted by the n-th field's
%                       value
%
% source/idea: 
% https://blogs.mathworks.com/pick/2010/09/17/sorting-structure-arrays-based-on-fields/
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if nargin < 2 || isempty(n); n = 1; end

SFields = fieldnames(Structs);
SCell = struct2cell(Structs);
sz = size(SCell);
SCell = reshape(SCell, sz(1), []);
SCell = SCell';
SCell = sortrows(SCell, n);
SCell = reshape(SCell', sz);
SortedStructs = cell2struct(SCell, SFields, 1);