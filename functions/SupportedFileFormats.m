function SupportedFileTypes = SupportedFileFormats(Style)
% This function returns the supported file formats.
%
% Usage:
%     SupportedFileTypes = SupportedFileFormats
%     SupportedFileTypes = SupportedFileFormats(Style)
%
% Input:
%     Style ... Output style. 
%               If 'full', then the output value is a n x 2 cell,
%               containing the file extensions of supported files in the
%               first column and a description in the second column. This
%               cell can be used directly as FilterSpec in the MATLAB
%               uigetfile function (see example).
%               If no input argument or 'minimal' is given as Style, the
%               function returns an n x 1 cell containing only the 
%               supported file extensions.
%
% Output:
%     SupportedFileTypes ... n x 1 cell of supported file extensions
%                           ('minimal' Style or without input argument) or
%                           n x 2 cell with file extensions and description
%                           ('full' Style).
%
% Example:
%     % open a file selection dialog where all supported file types can be
%     % selected
%     uigetfile(SupportedFileFormats('full'))
%
%
% This file is part of GIDVis.
%
% see also: uigetfile, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (nargin < 1 || isempty(Style)) || (nargin == 1 && strcmpi(Style, 'minimal'))
    % Deal with the Bruker numbering as file extensions
    BrukerExts = strsplit(sprintf('.%03u\n', 0:999));
    % concatenate with the other supported file types
    SupportedFileTypes = {'.tif', '.tiff', '.GIDDat', '.edf', '.cbf', ...
        '.mat', '.mccd', BrukerExts{1:end-1}, '.img', '.h5'};
elseif strcmpi(Style, 'full')
    BrukerExts = sprintf('*.%03u;', 0:999);
    SupportedFileTypes = {[BrukerExts '*.tif;*.tiff;*.edf;*.cbf;*.mat;*.mccd;*.img'], ...
        'Supported File Types (*.000 - *.999, *.cbf, *.edf, *.img, *.mat, *.mccd, *.tif, *.tiff)'; ...
        BrukerExts, 'Bruker Raw Frame Format (*.000 - *.999)'; ...
        '*.cbf', 'Cbf Files (*.cbf)'; ...
        '*.edf', 'Edf Files (*.edf)'; ...
        '*.mccd', 'MarCCD Files (*.mccd)'; ...
        '*.mat', 'Mat Files (*.mat)'; ...
        '*.img', 'Rigaku IMG File (*.img)'; ...
        '*.tif;*.tiff', 'Tif(f) Files (*.tif, *.tiff)'};
else
    error('GIDVis:SupportedFileFormats', 'Unknown Style input %s.', Style);
end