function SwitchOffToolstripButtons(FigureHandle, src)
% This function sets the state of all the toolstripbuttons to 'off'. It
% should be used when a new toolstripbutton's state is turned to 'on', so
% that not two or more tools are activated at the same time.
%
% Usage:
%     SwitchOffToolstripButtons(FigureHandle, src)
%
% Input:
%     FigureHandle ... parent figure handle of the figure containing the
%                      toolbar
%     src ... the toolstrip button which was clicked when this function is
%             called. All other toolstrip buttons' state will be turned to
%             'off', except for the toolstrip button src.
%
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

FigTB = findall(FigureHandle, 'tag', 'FigureToolBar');

Exceptions = {'ToggleMainAxesMajorGrid', 'ToggleMainAxesMinorGrid'};

C = get(FigTB, 'Children');
for iC = 1:numel(C)
    if strcmp(get(C(iC), 'Type'), 'uitoggletool') && ...
            C(iC) ~= src && ...
            strcmp(get(C(iC), 'State'), 'on') && ...
            ~any(ismember(Exceptions, get(C(iC), 'Tag')))
        set(C(iC), 'State', 'off');
    end
end

% the standard plot tools do not show up in the childrens collection of the
% figure toolbar bar. Deal with them.
pan(FigureHandle, 'off')
zoom(FigureHandle, 'off')
datacursormode(FigureHandle, 'off')
plotedit(FigureHandle, 'off')