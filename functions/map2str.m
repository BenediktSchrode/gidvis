function Str = map2str(M)
% This function converts a containers.Map into a string according to the
% following scheme:
% 
% key1: value1
% key2: value2
% key3: value3
%
% Usage:
%     Str = map2str(M)
%
% Input:
%     M ... the containers.Map which should be converted to a string
%           representation
%
% Output:
%     Str ... the string representation of the containers.Map
%
% This file is part of GIDVis.
%
% see also: containers.Map, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Str = '';
k = keys(M);
val = values(M);
for ik = 1:numel(k)
    if ischar(val{ik})
        Str = sprintf('%s%s: %s\n', Str, k{ik}, val{ik});
    elseif isnumeric(val{ik})
        Str = sprintf('%s%s: %g\n', Str, k{ik}, val{ik});
    end
end