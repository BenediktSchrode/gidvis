function FullStr = props2str(obj, Props)
% This function creates a char cell out of the properties of an object.
%
% Usage:
%     FullStr = props2str(obj);
%     FullStr = props2str(obj, Props);
%
% Input:
%     obj ... An object with properties
%     Props ... cell of properties to display. If not given, this will be a
%               sorted cell of properties(obj).
%
% Output:
%     FullStr ... Char cell containing the information in the following
%                 form:
%                                    a: 5
%                                    b: 10
%                   Long property name: 'abc'
%                 i.e. the property name and value are separated by a
%                 colon and aligned at this colon.
%
%
% This file is part of GIDVis.
%
% see also: properties, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% in case no properties cell is given, create one
if nargin < 2 || isempty(Props)
    % get the properties associated with the object
    Props = properties(obj);
    % turn all properties to lower case
    PropsLower = cellfun(@lower, Props, 'UniformOutput', false);
    % sort the lower case properties
    [~, idx] = sort(PropsLower);
    % sort the properties not in lower case
    Props = Props(idx);
end

% get the length of each property name
L = cellfun(@numel, Props);
% add two for display as for default MATLAB disp
Lmax = max(L)+2;

% initialize cell for the result
FullStr = cell(numel(Props), 1);

for iP = 1:numel(Props)
    % get the value of the property
    val = obj.(Props{iP});
    % enter the name of the property into the cell (padded with spaces)
    FullStr{iP} = sprintf(['%' num2str(Lmax) 's: '], Props{iP});
    
    % format value according to its class/type. It is tried to mimic the
    % default MATLAB disp style.
    if isnumeric(val) || islogical(val) % numeric
        if isempty(val)
            FullStr{iP} = [FullStr{iP}, sprintf('[]')];
        elseif numel(val) == 1
            FullStr{iP} = [FullStr{iP}, sprintf('%g', val)];
        elseif size(val, 1) == 1
            Str = sprintf(repmat('%g ', 1, numel(val)), val);
            FullStr{iP} = [FullStr{iP}, sprintf('[%s]', Str(1:end-1))];
        else
            Str = sprintf(repmat('%.0fx', 1, numel(size(val))), size(val));
            FullStr{iP} = [FullStr{iP}, sprintf('[%s %s]', Str(1:end-1), class(val))];
        end
    elseif ischar(val)
        if isempty(val)
            FullStr{iP} = [FullStr{iP}, sprintf('''''')];
        elseif size(val, 1) == 1
            FullStr{iP} = [FullStr{iP}, sprintf('''%s''', val)];
        else
            Str = sprintf(repmat('%.0fx', 1, numel(size(val))), size(val));
            FullStr{iP} = [FullStr{iP}, sprintf('[%s %s]', Str(1:end-1), class(val))];
        end
    elseif iscell(val)
        if isempty(val)
            FullStr{iP} = [FullStr{iP}, sprintf('{}')];
        elseif size(val, 1) == 1

        elseif size(val, 2) == 1
            Str = sprintf(repmat('%.0fx', 1, numel(size(val))), size(val));
            FullStr{iP} = [FullStr{iP}, sprintf('{%s %s}', Str(1:end-1), class(val))];
        end
    else
        Str = sprintf(repmat('%.0fx', 1, numel(size(val))), size(val));
        FullStr{iP} = [FullStr{iP}, sprintf('[%s %s]', Str(1:end-1), class(val))];
    end
    % add a line break at the end of each property value pair
    FullStr{iP} = [FullStr{iP}, sprintf('\n')];
end