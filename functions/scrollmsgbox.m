function dOut = scrollmsgbox(Message, Title, varargin)
% This function creates a msgbox where the message can be scrolled.
%
% Usage:
%     d = scrollmsgbox(Message, Title, varargin)
%
% Input:
%     Message ... Message shown in the scrollable part.
%     Title ... Window title for the dialog. '' if not given.
%     varargin ... all varargin arguments except for Style are directly
%                  passed to the dialog() call. 
%                  Style can be 'normal' or 'html'. In case it is 'html',
%                  the Message is interpreted as html string.
%                  The 'html' style requires Yair Altman's findjobj utility
%                  [1].
%
% [1] Yair Altman (2019). findjobj - find java handles of Matlab graphic
%     objects
%     (https://www.mathworks.com/matlabcentral/fileexchange/14317-findjobj-find-java-handles-of-matlab-graphic-objects),
%     MATLAB Central File Exchange. Retrieved October 29, 2019.
%
% Please see also Undocumented Matlab for more information on the html
% capabilites:
%     http://undocumentedmatlab.com/blog/rich-matlab-editbox-contents
%
% Output:
%     d ... handle to the dialog
%
% Example:
%     Message = '<div style="font-family:sans-serif"><p style="color:blue">This is some blue text!</p><p><u>underline</u></p><p>Please note that setting the style to font-family:sans-serif makes sure that the same font is used as in the standard dialog.</p></div>';
%     scrollmsgbox(Message, 'HTML Text', 'Style', 'html')
%
% This file is part of GIDVis.
%
% see also: dialog, findjobj, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin < 2 || isempty(Title)
    Title = '';
end

% all name-value pairs are passed directly to the dialog call except Style.
% Find Style if it is in varargin.
StyleInd = find(ismember(varargin(1:2:end), 'Style'));
if ~isempty(StyleInd) % Style is given
    % extract the Style parameter
    StyleInd = 2*StyleInd-1;
    Style = varargin{StyleInd+1};
    % remove the Style parameter from the varargin cell
    varargin([StyleInd StyleInd+1]) = [];
else
    Style = 'normal';
end

d = dialog('Name', Title, varargin{:});


Pos = get(d, 'Position');
TextField = uicontrol('Parent', d, 'Style', 'edit', ...
    'Position', [10 45 Pos(3)-20 Pos(4)-45-10], ...
    'HorizontalAlignment', 'left', 'Max', inf, 'Min', 1, ...
    'enable', 'inactive', 'FontSize', 10);
CloseBtn = uicontrol('Parent', d, 'Position', [(Pos(3)-70)/2 10 70 25], ...
    'String', 'Close', 'Callback', 'delete(gcf)', 'FontSize', 10);

set(d, 'WindowStyle', 'normal')
if strcmpi(Style, 'html')
    try
        jScrollPane = findjobj_fast(TextField);
        jViewPort = jScrollPane.getViewport;
        jEditbox = jViewPort.getComponent(0);
        jEditbox.setEditorKit(javax.swing.text.html.HTMLEditorKit);
        jEditbox.setText(Message);
    catch ex
        warning('scrollmsgbox:HTMLNotPossible', 'Falling back to default style ''normal''.\n%s', ex.message)
        Message = strsplit(Message, '\n', 'CollapseDelimiters', false);
        set(TextField, 'String', Message);
    end
elseif strcmpi(Style, 'normal')
    Message = strsplit(Message, '\n', 'CollapseDelimiters', false);
    set(TextField, 'String', Message);
end

set(d, 'Resize', 'on')
set(d, 'ResizeFcn', @DialogResizeFcn)

if nargout > 0
    dOut = d;
end

function DialogResizeFcn(~, ~)
NewPos = get(d, 'Position');
if NewPos(3) < 50 || NewPos(4) < 80; return; end
set(TextField, 'Position', [10 45 NewPos(3)-20 NewPos(4)-45-10]);
set(CloseBtn, 'Position', [(NewPos(3)-70)/2 10 70 25]);
end
end