function Out = str2keyvaluepairs(Name)
% This function extracts numbers after keywords from a string and stores
% them in a structure. Keywords are searched automatically from the string
% by the following criteria: (i) they are either at the beginning at the
% string, delimited by an underscore or a whitespace character; (ii) they
% are followed by a digit.
%
% E.g. from the string 'key1_id17 test54 Experiment' the keywords key, id
% and test are extracted. There is no keyword Experiment because it is not
% followed by a digit. The corresponding numbers for the struct are then 1,
% 17 and 54. If decimal places in the number are needed, the decimal
% separator can also be the letter d instead of the decimal point. If the
% number cannot be converted from the string, it will be assigned NaN.
%
%
% Usage:
%     Out = BeamlineParametersFromString(String)
%
% Input:
%     String ... string to extract the parameters from
%
% Output:
%     Out ... Structure containing the determined keywords as fields and
%             their values
%
%
% Example:
% BeamlineParametersFromString('sdd150_detz6d9_delta13_Setting1 Example.tif')
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% initialize result structure
Out = struct;

% try to extract keywords:
% assume that they are either at the beginning of the string, delimited by
% an underscore _, or delimited by a whitespace character followed by a
% digit
% keys = regexp(Name, '(^|_|\s)([a-zA-Z]+)\d', 'tokens');
keys = regexp(Name, '([a-zA-Z]+)[^a-z|^A-Z|^\d]*(\d+[dD\.]\d+|\d+)', 'tokens');

for iK = 1:numel(keys)
    % get the cell of the regexp result
    keyC = keys{iK};
    % the key word is in the first cell
    key = keyC{1};
    % try to extract a number after the keyword. We have to do this here
    % again to check for the same keyword appearing multiple times
    Toks = regexp(Name, [key, '[^a-z|^A-Z|^\d]*(\d+[dD\.]\d+|\d+)'], 'tokens');
    % make sure to have a valid key for the structure
%     StructKey = matlab.lang.makeValidName(key);
    if numel(Toks) == 1 % the keyword is only appearing once in the input
        % add the keyword and the value to the struct
        Out.(key) = str2double(strrep(lower(Toks{1}), 'd', '.'));
    else % the keyword is multiple times in the input
        % loop over the appearances of the keyword and add a number to the
        % keyword in the output struct
        for iT = 1:numel(Toks)
            Out.([key, num2str(iT)]) = str2double(strrep(Toks{iT}, 'd', '.'));
        end
    end
end