function varargout = AboutWindowModule(varargin)
% This, together with AboutWindowModule.fig, is the AboutWindowModule of
% GIDVis.
%
% Usage:
%     varargout = AboutWindowModule([], ExtFPath)
%
% Input:
%     [] ... empty input argument
%     ExtFPath ... the path to the directory containing the external
%                  functions
% The reason why there is an empty input argument at the first place is
% that otherwise MATLAB would try to interpret the string ExtFPath as a
% function defined in AboutWindowModule. I could not find another solution
% than passing first an empty argument, then the string. <BS>
%
% Output:
%     varargout ... default GUIDE output
%
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Last Modified by GUIDE v2.5 03-Apr-2019 11:22:42

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @AboutWindowModule_OpeningFcn, ...
                   'gui_OutputFcn',  @AboutWindowModule_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before AboutWindowModule is made visible.
function AboutWindowModule_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to AboutWindowModule (see VARARGIN)

% Choose default command line output for AboutWindowModule
handles.output = hObject;

set(hObject, 'WindowStyle', 'modal');

% store the path to the external functions directory in the handles
% structure
handles.ExtFunPath = varargin{2};

% search for external functions
dirs = dir(handles.ExtFunPath);
% remove . and .. dir from listing
dirs = dirs(3:end);

% set the listbox string to the directory names
set(handles.LB_ExternalFunctions, 'String', {dirs.name});

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes AboutWindowModule wait for user response (see UIRESUME)
% uiwait(handles.Fig_AboutWindowModule);


% --- Outputs from this function are returned to the command line.
function varargout = AboutWindowModule_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in Btn_OpenGIDVisWebpage.
function Btn_OpenGIDVisWebpage_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_OpenGIDVisWebpage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
web('http://www.if.tugraz.at/amd/GIDVis/', '-browser')


% --- Executes on selection change in LB_ExternalFunctions.
function LB_ExternalFunctions_Callback(hObject, eventdata, handles)
% hObject    handle to LB_ExternalFunctions (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns LB_ExternalFunctions contents as cell array
%        contents{get(hObject,'Value')} returns selected item from LB_ExternalFunctions
contents = cellstr(get(hObject, 'String'));
SelEl = contents{get(hObject, 'Value')};
S = dir(fullfile(handles.ExtFunPath, SelEl));
FileNames = {S.name};
L = strcmpi(FileNames, 'license.txt');
Txt = fileread(fullfile(handles.ExtFunPath, SelEl, FileNames{L}));
scrollmsgbox(Txt, 'License', 'WindowStyle', 'modal');

% --- Executes during object creation, after setting all properties.
function LB_ExternalFunctions_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LB_ExternalFunctions (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Btn_ShowLicense.
function Btn_ShowLicense_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_ShowLicense (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Txt = fileread('GIDVisLicense.txt');
scrollmsgbox(Txt, 'License', 'WindowStyle', 'modal');
