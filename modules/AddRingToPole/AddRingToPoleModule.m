function AddRingToPoleModule(Figure, Toolbar, AxesHandle, SeparatorOnOff)
% This is the code file for the Add Ring To Pole Module. Here, all the
% programming concerning for the Add Ring To Pole Module is done, starting
% from reading/saving settings, adding the icon to the toolbar and handling
% the user interaction with the toolbar button.
%
% Usage:
%     AddRingToPoleModule(Figure, Toolbar, AxesHandle, SeparatorOnOff)
%
% Input:
%     Figure ... the handle of the figure window
%     Toolbar ... the handle of the figure's toolbar
%     AxesHandle ... the axes handle
%     SeparatorOnOff ... string ('on' or 'off') defining whether a
%                        separator should be drawn left of the toggle tool
%                        or not
%     
%
% This file is part of GIDVis.
%
% see also: uitoggletool, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % the settings for this module are stored in the same directory as the file
    % AddRingModule.m
    SettingsPath = fullfile(fileparts(mfilename('fullpath')), 'Settings.mat');

    % initialize the LastProperties struct with some default values
    LastProperties.LineStyle = '-';
    LastProperties.LineWidth = 1;
    LastProperties.LineColor = 'k';
    LastProperties.LabelVisible = 'on';
    
    % read out settings if available
    if exist(SettingsPath, 'file') == 2
        S = load(SettingsPath);
        if isfield(S, 'SettingsAddRingModule')
            if iscell(S.SettingsAddRingModule) % deal with settings file of "old" AddRingModule
                % assign the settings values to the LastProperties struct
                LastProperties.LineWidth = S.SettingsAddRingModule{1};
                Col = S.SettingsAddRingModule{2};
                LastProperties.LineColor = Col(1:3);
                LastProperties.LabelVisible = S.SettingsAddRingModule{3};
                LastProperties.LineStyle = S.SettingsAddRingModule{4};
            else % settings of "new" AddRingModule
                % assign the settings values to the LastProperties struct
                LastProperties = S.SettingsAddRingModule.LastProperties;
            end 
        end
    end

    % create a vector which will hold the rings added to the plot
    Rings = InteractiveRing.empty();

    % read the icon
    icon = imread('AddRingIcon.png');
    % add the toggle button to the Toolbar.
    ToggleButton = uitoggletool(Toolbar, 'CData', icon, ...
        'TooltipString', sprintf('<html><b>Add ring(s)</b><br />Left Click: Add one ring<br />Right Click/Ctrl + Click: Add multiple rings</html>'), ...
        'OnCallback', {@AddRingToggledToOn, Figure, AxesHandle}, ...
        'OffCallback', @(src, evt) AddRingToggledToOff, ...
        'Separator', SeparatorOnOff);

    % add a listener to the togglebutton just created. When it gets destroyed 
    % (e.g. because the figure containing the button gets closed), the callback 
    % will be executed (and stores the settings).
    addlistener(ToggleButton, 'ObjectBeingDestroyed', @(src, evt) ToggleButtonDestroyed_Callback);

    function AddRingToggledToOn(src, evt, f, ax)
        % set all other tools to off (pan, zoom, edit, datacursor) to avoid
        % using multiple tools at once
        SwitchOffToolstripButtons(f, src);

        % add a ring
        Rings(end+1) = InteractiveRing(AxesHandle, ...
            LastProperties.LineStyle, LastProperties.LineWidth, ...
            LastProperties.LineColor, LastProperties.LabelVisible);
        % start the interactive placement
        StartInteractivePlacement(Rings(end));
        % add an entry to the context menu to delete all rings
        uimenu(Rings(end).ContextMenu, 'Label', 'Delete All Rings', ...
            'Callback', @(hObject, evt) DeleteAllRings, 'Tag', 'DeleteAllRings');
        % reorder items in the context menu
        Ch = get(Rings(end).ContextMenu, 'Children');
        Order = [2 1 7 3 4 5 6];
        set(Rings(end).ContextMenu, 'Children', Ch(Order));
        
        % set separator above delete ring menu item
        MI = findobj(Rings(end).ContextMenu, 'Tag', 'DeleteRing');
        set(MI, 'Separator', 'on');
        
        % add listeners to the line style, width, color and label
        % visibility --> we use this callback to store the last used values
        addlistener(Rings(end), {'LineStyle', 'LineWidth', 'LineColor', 'LabelVisible'}, ...
            'PostSet', @PropertyValueChanged);
        % when user clicked with left mouse button, place no more rings
        if strcmp(Rings(end).CreationSelectionType, 'normal')
            set(src, 'State', 'off');
        else % user didn't click with left mouse button, so add another ring
            AddRingToggledToOn(src, evt, f, ax);
        end
    end

    % we store the new value in LastProperties
    function PropertyValueChanged(src, evt)
        LastProperties.(src.Name) = get(evt.AffectedObject, src.Name);
    end

    function AddRingToggledToOff
        % when user toggles the button to off, we have to deactive any
        % interactive placement which is still running
        if Rings(end).UnderConstruction
            CancelInteractivePlacement(Rings(end));
            Rings(end) = [];
        end
    end

    function ToggleButtonDestroyed_Callback
        % the settings for this module are stored in the same directory as the file
        % AddRingModule.m
        SettingsPath = fullfile(fileparts(mfilename('fullpath')), 'Settings.mat');
        % we have to save the LastProperties
        SettingsAddRingModule.LastProperties = LastProperties; %#ok, used for saving
        % save the settings. In order to avoid throwing error when no write access
        % in SettingsPath, use try.
        try save(SettingsPath, 'SettingsAddRingModule'); end %#ok
    end
    
    % callback function to delete all rings
    function DeleteAllRings
        delete(Rings);
    end
end