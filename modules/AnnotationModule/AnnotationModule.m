function varargout = AnnotationModule(varargin)
% This is the AnnotationModule for GIDVis. 
%
% Usage:
%     varargout = AnnotationModule(AxesHandle, AnnotationsPath, Btn)
%
% Input:
%     AxesHandle ... axes handle of main window axes
%     AnnotationsPath ... directory where the annotations are stored
%     Btn ... Handle to button used to open the annotation module
%
% Output:
%     varargout ... default GUIDE output
%
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% TODO: checking for image toolbox license

% Last Modified by GUIDE v2.5 20-Sep-2017 13:39:11

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @AnnotationModule_OpeningFcn, ...
                   'gui_OutputFcn',  @AnnotationModule_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before AnnotationModule is made visible.
function AnnotationModule_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to AnnotationModule (see VARARGIN)

% Choose default command line output for AnnotationModule
handles.output = hObject;

% store the main axes handle in the handles structure
handles.MainAxes = varargin{1};

% store the path to the Annotations folder in the handles structure
handles.EACPath = varargin{2};

% store the handle of the button used to open the annotation module in the
% handles structure
handles.ModuleOpenButton = varargin{3};

% set the button's enabled state to off to avoid multiple opening of the
% annotation module
set(handles.ModuleOpenButton, 'Enable', 'off');

% check if folder exists, if not, create it
if ~exist(handles.EACPath, 'dir')
    mkdir(handles.EACPath);
end

% find files in the EACPath
MatFiles = dir(fullfile(handles.EACPath, '*.mat'));

% prepare storage for the annotation collections
handles.EAC = ExtendedAnnotationCollection.empty(0, numel(MatFiles));

% make main axes current (needed so that the annotations are drawn into the
% right axes)
axes(handles.MainAxes);

% load the annotations from the mat files
for iF = 1:numel(MatFiles)
    S = load(fullfile(handles.EACPath, MatFiles(iF).name));
    handles.EAC(iF) = S.EAC;
    for iEA = 1:numel(handles.EAC(iF).Annotations)
        uimenu(handles.EAC(iF).Annotations(iEA).ContextMenu, 'Label', ['Member of ', handles.EAC(iF).CollectionName], 'Enable', 'off');
    end
end

% prepare data field for the table
data = cell(numel(handles.EAC), 2);
if ~isempty(handles.EAC)
    data(:, 1) = {handles.EAC.CollectionName};
    data(:, 2) = {handles.EAC.VisibleBool};
end

% set the data of the table
set(handles.UIT_AvailableCollections, 'Data', data);

% set the column names of the table
set(handles.UIT_AvailableCollections, 'ColumnName', {'Name', 'Visible'});

% set the edit properties of the table
set(handles.UIT_AvailableCollections, 'ColumnEditable', [false, true]);

% prepare storage of additional, new annotations
handles.NewAnnotations = ExtendedAnnotation.empty;

% the settings for this module are stored in the same directory as the file
% AnnotationModule.m. Store its path in the handles structure to reuse it.
handles.SettingsPath = fullfile(fileparts(mfilename('fullpath')), 'Settings.mat');

% read out settings if available
if exist(handles.SettingsPath, 'file') == 2
    load(handles.SettingsPath);
    % store the settings in the handles structure
    handles.Settings = SettingsAnnotationModule;
else
    handles.Settings = [];
end

% add a listener to the figure which is the parent of the axes varargin{1}. 
% When the figure gets destroyed (e.g. because the figure gets closed), the 
% callback will be executed (and closes this window).
% Store the listener in the handles structure to remove it when this window    
% gets closed by user, otherwise the callback is executed even when the
% AnnotationModule window does not exist anymore.
handles.MainFigureDestroyedListener = addlistener(ancestor(varargin{1}, 'figure'), 'ObjectBeingDestroyed', @(src, evt) MainFigureDestroyed_Callback(src, evt, handles.Fig_AnnotationModule));

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes AnnotationModule wait for user response (see UIRESUME)
% uiwait(handles.Fig_AnnotationModule);

function MainFigureDestroyed_Callback(src, evt, HandleToModuleWindow)
% close module window
close(HandleToModuleWindow);

% --- Outputs from this function are returned to the command line.
function varargout = AnnotationModule_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% restore the window position
if isfield(handles.Settings, 'WindowPosition')
    RestorePositionOrDefault(hObject, handles.Settings.WindowPosition, ...
        'center');
end

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes when entered data in editable cell(s) in UIT_AvailableCollections.
function UIT_AvailableCollections_CellEditCallback(hObject, eventdata, handles)
% hObject    handle to UIT_AvailableCollections (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.TABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)
if eventdata.NewData == 1
    handles.EAC(eventdata.Indices(1)).Visible = 'on';
else
    handles.EAC(eventdata.Indices(1)).Visible = 'off';
end

% --- Executes when user attempts to close Fig_AnnotationModule.
function Fig_AnnotationModule_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to Fig_AnnotationModule (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% delete AxesDestroyedListener
delete(handles.MainFigureDestroyedListener);

% set the enable state of the button which was used to open the annotation
% module to 'on', so that the module can be reopened
set(handles.ModuleOpenButton, 'Enable', 'on');

EAs = handles.NewAnnotations;
EAs(~isvalid(EAs)) = [];
Cancelled = false;

if ~isempty(EAs)
    BreakOut = false;
    while ~BreakOut
        prompt = sprintf('You have created %d additional annotation(s). How to proceed?', numel(EAs));
        title = 'Additional Annotations';
        if numel(handles.EAC) > 0
            choice = questdlg(prompt, title, 'Add to existing collection', 'Add to new collection', 'Discard', 'Add to existing collection');
        else
            choice = questdlg(prompt, title, 'Add to new collection', 'Discard', 'Add to new collection');
        end
        switch choice
            case 'Add to existing collection'
                [Selection, ok] = listdlg('PromptString', 'Select Collection:', ...
                    'SelectionMode', 'single', 'ListString', {handles.EAC.CollectionName});
                if ok
                    for iEA = 1:numel(EAs)
                        handles.EAC(Selection).AddAnnotation(EAs(iEA));
                    end
                    BreakOut = true;
                end
            case 'Add to new collection'
                Name = inputdlg('Enter new collection name:', 'Collection Name');
                if ~isempty(Name{:})
                    handles.EAC(end+1) = ExtendedAnnotationCollection(Name{:});
                    for iEA = 1:numel(EAs)
                        handles.EAC(end).AddAnnotation(EAs(iEA));
                    end
                    BreakOut = true;
                end
            otherwise
                delete(EAs);
                BreakOut = true;
        end
    end
end


% save all the annotation collections
for iD = 1:numel(handles.EAC)
    EAC = handles.EAC(iD);
    RemoveInvalid(EAC);
    Str = CreateValidFileName(EAC.CollectionName, ['AnnotationCollection', strrep(num2str(now), '.', 'd')]);
    save(fullfile(handles.EACPath, Str), 'EAC');
    for iEA = 1:numel(EAC.Annotations)
        delete(EAC.Annotations(iEA));
    end
end

% store the settings
SettingsAnnotationModule = handles.Settings;
SettingsAnnotationModule.WindowPosition = get(hObject, 'Position');
% in order to avoid error when no write access for file use try.
try; save(handles.SettingsPath, 'SettingsAnnotationModule'); end

% Hint: delete(hObject) closes the figure
if ~Cancelled
    delete(hObject);
end


% --------------------------------------------------------------------
function UIM_DeleteSelectedCollections_Callback(hObject, eventdata, handles)
% hObject    handle to UIM_DeleteSelectedCollections (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data = get(handles.UIT_AvailableCollections, 'Data');
if ~isfield(handles, 'SelectedCells') || isempty(handles.SelectedCells)
    return
end

rows = handles.SelectedCells(:, 1);
for iR = numel(rows):-1:1
    delete(fullfile(handles.EACPath, [CreateValidFileName(handles.EAC(rows(iR)).CollectionName), '.mat']));
    for iEA = 1:numel(handles.EAC(rows(iR)).Annotations)
        delete(handles.EAC(rows(iR)).Annotations(iEA));
    end
    handles.EAC(rows(iR)) = [];
    data(rows(iR), :) = [];
end
set(handles.UIT_AvailableCollections, 'Data', data);

guidata(hObject, handles);

% --------------------------------------------------------------------
function Cntxt_UIT_AvailableCollections_Callback(hObject, eventdata, handles)
% hObject    handle to Cntxt_UIT_AvailableCollections (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --- Executes when selected cell(s) is changed in UIT_AvailableCollections.
function UIT_AvailableCollections_CellSelectionCallback(hObject, eventdata, handles)
% hObject    handle to UIT_AvailableCollections (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.TABLE)
%	Indices: row and column indices of the cell(s) currently selecteds
% handles    structure with handles and user data (see GUIDATA)
handles.SelectedCells = eventdata.Indices;
guidata(hObject, handles);

function AddExtendedAnnotation(GUIObject, Type)
handles = guidata(GUIObject);
f = ancestor(handles.MainAxes, 'figure');
% plotedit(f, 'off');
OldCursor = get(f, 'Pointer');
set(f, 'Pointer', 'crosshair');
Controls = {'Line', 'Arrow', 'DoubleArrow', 'TextArrow', 'Rectangle', 'Ellipse', 'TextBox'};
for iC = 1:numel(Controls)
    eval(['set(handles.Btn_Add', Controls{iC}, ', ''Enable'', ''off'');']);
end

EA = ExtendedAnnotation(handles.MainAxes, Type);
StartInteractivePlacement(EA);
uiwait(f);
set(f, 'Pointer', OldCursor);
if EA.ConstructionCancelled; return; end
handles.NewAnnotations(end+1) = EA;
for iC = 1:numel(Controls)
    eval(['set(handles.Btn_Add', Controls{iC}, ', ''Enable'', ''on'');']);
end

guidata(GUIObject, handles);

% --- Executes on button press in Btn_AddLine.
function Btn_AddLine_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_AddLine (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
AddExtendedAnnotation(hObject, 'line');

% --- Executes on button press in Btn_AddArrow.
function Btn_AddArrow_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_AddArrow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
AddExtendedAnnotation(hObject, 'arrow');

% --- Executes on button press in Btn_AddDoubleArrow.
function Btn_AddDoubleArrow_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_AddDoubleArrow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
AddExtendedAnnotation(hObject, 'doublearrow');

% --- Executes on button press in Btn_AddTextArrow.
function Btn_AddTextArrow_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_AddTextArrow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
AddExtendedAnnotation(hObject, 'textarrow');

% --- Executes on button press in Btn_AddRectangle.
function Btn_AddRectangle_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_AddRectangle (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
AddExtendedAnnotation(hObject, 'rectangle');

% --- Executes on button press in Btn_AddEllipse.
function Btn_AddEllipse_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_AddEllipse (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
AddExtendedAnnotation(hObject, 'ellipse');

% --- Executes on button press in Btn_AddTextBox.
function Btn_AddTextBox_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_AddTextBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
AddExtendedAnnotation(hObject, 'textbox');
