classdef ExtendedAnnotation < handle
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2018 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    properties (Access = protected)
        OldPointer
        FigureDownID
        FigureMotionID
        FigureUpID
        LastPointerPosition
        FigureResizeID
        KeyReleaseFcnID
        EscKeyPressFcnID
        ShiftHold = false;
        XYStorage = zeros(2, 2);
        KeyPressFcnID
    end
    
    properties (Access = protected, Transient)
        AnnotationDestroyedListener
        AxesChangeListener
        AnnotationHandle
    end
    
    properties (SetAccess = private)
        Type
        AxesHandle
        ContextMenu
    end
    
    properties (Dependent = true)
        XY
        X
        Y
        Visible
    end
    
    properties (SetAccess = private)
        UnderConstruction = false;
        ConstructionCancelled = false;
    end
    
    properties (Dependent = true, Access = protected)
        f
    end
    
    methods
        function val = get.Visible(obj)
            val = get(obj.AnnotationHandle, 'Visible');
        end
        function set.Visible(obj, val)
            set(obj.AnnotationHandle, 'Visible', val)
        end
        
        function val = get.X(obj)
            val = obj.XY(:, 1);
        end
        function set.X(obj, val)
            obj.XY(:, 1) = val;
        end
        
        function val = get.Y(obj)
            val = obj.XY(:, 2);
        end
        function set.Y(obj, val)
            obj.XY(:, 2) = val;
        end
        
        function val = get.f(obj)
            val = ancestor(obj.AxesHandle, 'figure');
        end
        
        function val = get.XY(obj)
            val = obj.XYStorage;
        end
        function set.XY(obj, NewXY)
            NewX = NewXY(:, 1);
            NewY = NewXY(:, 2);
            [xn, yn] = NormalizedUnitsFromDataUnits(obj.AxesHandle, NewX, NewY);
            set(obj.AnnotationHandle, 'Position', [xn(1) yn(1) xn(2)-xn(1) yn(2)-yn(1)]);
            if ~obj.ConstructionCancelled
                set(obj.AnnotationHandle, 'Visible', 'on')
            end
            obj.XYStorage = NewXY;
        end
        
        function StartInteractivePlacement(obj)
            set(obj.f, 'Pointer', 'crosshair');
            obj.FigureDownID = iptaddcallback(obj.f, 'WindowButtonDownFcn', @obj.FigWindowButtonDown_Callback);
            obj.EscKeyPressFcnID = iptaddcallback(obj.f, 'KeyPressFcn', @obj.EscKeyPress_Callback);
            obj.UnderConstruction = true;
            obj.ConstructionCancelled = false;
        end
        
        function obj = ExtendedAnnotation(ax, Type)
            if nargin < 1 || isempty(ax)
                ax = gca;
            end
            if nargin < 2 || isempty(Type)
                Type = 'line';
            end
            obj.AxesHandle = ax;
            obj.Type = Type;
            obj.AnnotationHandle = annotation(obj.f, obj.Type, 'Visible', 'off', 'ButtonDownFcn', @obj.AnnotationHandle_ButtonDownFcn);
            
            obj.ContextMenu = uicontextmenu('Parent', obj.f, 'Callback', @obj.UICntxtVis_Callback);
            if isdeployed
                CreateContextMenu(obj);
            end
            set(obj.AnnotationHandle, 'UIContextMenu', obj.ContextMenu);
            obj.AnnotationDestroyedListener = addlistener(obj.AnnotationHandle, 'ObjectBeingDestroyed', @obj.Delete_Callback);
            obj.OldPointer = get(obj.f, 'Pointer');
        end
        
        function AnnotationHandle_ButtonDownFcn(obj, src, evt)
            if ~strcmp(get(obj.f, 'SelectionType'), 'alt')
                return;
            end
            
            OldUnits = get(obj.f, 'Units');
            set(obj.f, 'Units', 'pixels');
            Pos = get(obj.f, 'CurrentPoint');
            set(obj.f, 'Units', OldUnits);
            set(obj.ContextMenu, 'Visible', 'on', 'Position', Pos);
        end
        
        function delete(obj)
            if strcmp(get(obj.AnnotationHandle, 'Selected'), 'on')
                set(obj.AnnotationHandle, 'Selected', 'off')
            end
            delete(obj.AnnotationDestroyedListener);
            iptremovecallback(obj.f, 'WindowButtonDownFcn', obj.FigureDownID);
            iptremovecallback(obj.f, 'WindowButtonMotionFcn', obj.FigureMotionID);
            iptremovecallback(obj.f, 'WindowButtonUpFcn', obj.FigureUpID);
            iptremovecallback(obj.f, 'ResizeFcn', obj.FigureResizeID);
            iptremovecallback(obj.f, 'KeyReleaseFcn', obj.KeyReleaseFcnID);
            iptremovecallback(obj.f, 'KeyPressFcn', obj.EscKeyPressFcnID);
            iptremovecallback(obj.f, 'KeyPressFcn', obj.KeyPressFcnID);
            try delete(obj.ContextMenu); end %#ok BS. For some reason obj.ContextMenu not accessible in older Matlab versions --> avoid error with try/catch
            delete(obj.AxesChangeListener);
            try delete(obj.AnnotationHandle); end
        end
        
        function CancelInteractivePlacement(varargin)
            obj = varargin{1};
            set(obj.f, 'Pointer', obj.OldPointer);
            iptremovecallback(obj.f, 'WindowButtonDownFcn', obj.FigureDownID);
            set(obj.AnnotationHandle, 'Visible', 'off');
            obj.UnderConstruction = false;
            obj.ConstructionCancelled = true;
            uiresume(obj.f);
        end
        
        function s = saveobj(obj)
%             plotedit(obj.f, 'off');
            set(obj.AnnotationHandle, 'Selected', 'off');
            s.XY = obj.XY;
            s.Type = obj.Type;
            if verLessThan('MATLAB', '8.4')
                Props = fieldnames(get(obj.AnnotationHandle));
            else
                Props = properties(obj.AnnotationHandle);
            end
            s.PropNames = Props;
            s.PropValues = get(obj.AnnotationHandle, Props);
            if isempty(obj.Visible)
                s.Visible = 'on';
            else
                s.Visible = obj.Visible;
            end
        end
        
        function CopyProperties(obj, CopyFrom)
            for iP = 1:numel(CopyFrom.PropNames)
                if strcmp(CopyFrom.PropNames{iP}, 'Parent')
                    continue;
                end
                try
                    set(obj.AnnotationHandle, CopyFrom.PropNames{iP}, CopyFrom.PropValues{iP});
                end
            end
        end
    end
    
    methods (Access = protected)
        function UICntxtVis_Callback(obj, src, evt)
            Ch = allchild(src);
            Avoid = {'ScribeGenericActionCut', 'ScribeGenericActionCopy', ...
                'matlab.graphics.shape.internal.TwoDimensional.pinuicontextmenu.PinToAxes', ...
                'matlab.graphics.shape.internal.TwoDimensional.pinuicontextmenu.Unpin', ...
                'matlab.graphics.shape.internal.OneDimensional.pinuicontextmenu.PinToAxes', ...
                'matlab.graphics.shape.internal.OneDimensional.pinuicontextmenu.Unpin', ...
                'scribe.scribeobject1d.pinuicontextmenu', ...
                'scribe.scribeobject2d.pinuicontextmenu'};
            for iA = 1:numel(Avoid)
                set(findobj(Ch, 'Tag', Avoid{iA}), 'Visible', 'off');
            end
        end
        
        function AnnotationPositionChanged_Callback(obj, src, evt)
            AnnotationPositionChanged(obj);
        end
        
        function AnnotationPositionChanged(obj)
            if obj.UnderConstruction
                return;
            end
            Pos = get(obj.AnnotationHandle, 'Position');
            Pos(3) = Pos(1) + Pos(3);
            Pos(4) = Pos(2) + Pos(4);
            AxPos = plotboxpos(obj.AxesHandle);
            xL = xlim(obj.AxesHandle);
            if strcmp(get(obj.AxesHandle, 'XDir'), 'reverse')
                xL = fliplr(xL);
            end
            yL = ylim(obj.AxesHandle);
            if strcmp(get(obj.AxesHandle, 'YDir'), 'reverse')
                yL = fliplr(yL);
            end
            DUpNU = (xL(2)-xL(1))/AxPos(3);
            DataUnits = NaN(2, 2);
            DataUnits(1, 1) = (Pos(1)-AxPos(1))*DUpNU + xL(1); % x(1)
            DataUnits(2, 1) = (Pos(3)-AxPos(1))*DUpNU + xL(1); % x(2)
            DUpNU = (yL(2)-yL(1))/AxPos(4);
            DataUnits(1, 2) = (Pos(2)-AxPos(2))*DUpNU + yL(1); % y(1)
            DataUnits(2, 2) = (Pos(4)-AxPos(2))*DUpNU + yL(1); % y(1)
            obj.XY = DataUnits;
        end
        
        function EscKeyPress_Callback(obj, src, evt)
            if ~isempty(evt.Modifier)
                return;
            end
            if strcmp(evt.Key, 'escape')
                CancelInteractivePlacement(obj);
            end
        end
        
        function Delete_Callback(obj, src, evt)
            delete(obj);
        end
    
        
        function FigWindowButtonDown_Callback(obj, src, evt)
            CurPos = get(get(src, 'CurrentAxes'), 'CurrentPoint');
            obj.XY = CurPos(1:2, 1:2);
            obj.FigureMotionID = iptaddcallback(src, 'WindowButtonMotionFcn', @obj.FigWindowButtonMotion_Callback);
            obj.FigureUpID = iptaddcallback(src, 'WindowButtonUpFcn', @obj.FigWindowButtonUp_Callback);
            if isempty(obj.KeyPressFcnID)
                obj.KeyPressFcnID = iptaddcallback(obj.f, 'KeyPressFcn', @obj.FigureKeyPressFcn);
            end
            obj.KeyReleaseFcnID = iptaddcallback(obj.f, 'KeyReleaseFcn', @obj.FigureKeyReleaseFcn);
        end
        
        function [OldValues, NewValues] = SnapToAngle(obj, OldValues, NewValues)
            Intervals = (-pi:pi/4:pi)+pi/8;
            angle = atan2(NewValues(2)-OldValues(2), NewValues(1)-OldValues(1));
            switch find(angle < Intervals, 1) - 1
                case {0, 4, 8}
                    NewValues(2) = OldValues(2);
                case {2, 6}
                    NewValues(1) = OldValues(1);
                case 1
                    R = sqrt((NewValues(1)-OldValues(1))^2+(NewValues(2)-OldValues(2))^2)/sqrt(2);
                    NewValues(1) = OldValues(1)-R;
                    NewValues(2) = OldValues(2)-R;
                case 3
                    R = sqrt((NewValues(1)-OldValues(1))^2+(NewValues(2)-OldValues(2))^2)/sqrt(2);
                    NewValues(1) = OldValues(1)+R;
                    NewValues(2) = OldValues(2)-R;
                case 5
                    R = sqrt((NewValues(1)-OldValues(1))^2+(NewValues(2)-OldValues(2))^2)/sqrt(2);
                    NewValues(1) = OldValues(1)+R;
                    NewValues(2) = OldValues(2)+R;
                case 7
                    R = sqrt((NewValues(1)-OldValues(1))^2+(NewValues(2)-OldValues(2))^2)/sqrt(2);
                    NewValues(1) = OldValues(1)-R;
                    NewValues(2) = OldValues(2)+R;
            end
        end
        
        function FigWindowButtonMotion_Callback(obj, src, evt)
            CurPos = get(get(src, 'CurrentAxes'), 'CurrentPoint');
            OldX = obj.X;
            OldY = obj.Y;
            if obj.ShiftHold
                [OldValues, NewValues] = obj.SnapToAngle([OldX(2) OldY(2)], CurPos(1, 1:2));
                OldX(2) = OldValues(1);
                OldY(2) = OldValues(2);
                CurPos(1, 1:2) = NewValues;
            end
            obj.XY = [CurPos(1, 1) CurPos(1, 2); OldX(2) OldY(2)];
        end

        function FigWindowButtonUp_Callback(obj, src, evt)
            iptremovecallback(src, 'WindowButtonDownFcn', obj.FigureDownID);
            iptremovecallback(src, 'WindowButtonMotionFcn', obj.FigureMotionID);
            iptremovecallback(src, 'WindowButtonUpFcn', obj.FigureUpID);
            set(src, 'Pointer', obj.OldPointer);
            obj.XY = [obj.X(:), obj.Y(:)];
            obj.AxesChangeListener = addlistener(obj.AxesHandle, {'XLim', 'YLim', 'XLimMode', 'YLimMode', 'Position', 'PlotBoxAspectRatio', 'DataAspectRatio', 'PlotBoxAspectRatioMode', 'DataAspectRatioMode'}, 'PostSet', @obj.AxesLimitsChange_Callback);
            obj.FigureResizeID = iptaddcallback(obj.f, 'ResizeFcn', @obj.AxesLimitsChange_Callback);
            addlistener(obj.AnnotationHandle, 'Position', 'PostSet', @obj.AnnotationPositionChanged_Callback);
            obj.UnderConstruction = false;
            uiresume(obj.f);
        end
        
        function FigureKeyPressFcn(obj, src, evt)
            if numel(evt.Modifier) == 1 && strcmp(evt.Modifier{:}, 'shift') && strcmp(evt.Key, 'shift')
                obj.ShiftHold = true;
            end
        end
        
        function FigureKeyReleaseFcn(obj, src, evt)
            if numel(evt.Modifier) == 0 && strcmp(evt.Key, 'shift')
                obj.ShiftHold = false;
            end
        end
        
        function AxesLimitsChange_Callback(obj, src, evt)
            obj.XY = [obj.X(:), obj.Y(:)];
        end
        
        function CreateContextMenu(obj)
            uim = obj.ContextMenu;
            switch lower(obj.Type)
                case 'line'
                    Properties = {'LineStyle', 'Line Style', {'-', 'Solid'; '--', 'Dashed'; ':', 'Dotted'; '-.', 'Dash-dotted'}; ...
                        'LineWidth', 'Line Width', 'numeric'; ...
                        'Color', 'Color', 'color'};
                case 'arrow'
                    Properties = {'Color', 'Line Color', 'color'; ...
                        'LineStyle', 'Line Style', {'-', 'Solid'; '--', 'Dashed'; ':', 'Dotted'; '-.', 'Dash-dotted'}; ...
                        'LineWidth', 'Line Width', 'numeric'; ...
                        'HeadStyle', 'Head Style', {'none', 'None'; 'plain', 'Plain'; 'vback2', 'V-back'; 'cback2', 'C-back'; 'diamond', 'Diamond'; 'deltoid', 'Deltoid'}; ...
                        {'HeadLength', 'HeadWidth'}, 'Head Size', 'numeric'};
                case 'doublearrow'
                    Properties = {'Color', 'Color', 'color'; ...
                        'LineStyle', 'Line Style', {'-', 'Solid'; '--', 'Dashed'; ':', 'Dotted'; '-.', 'Dash-dotted'}; ...
                        'LineWidth', 'Line Width', 'numeric'; ...
                        {'Head1Style', 'Head2Style'}, 'Head Style', {'none', 'None'; 'plain', 'Plain'; 'vback2', 'V-back'; 'cback2', 'C-back'; 'diamond', 'Diamond'; 'deltoid', 'Deltoid'}; ...
                        {'Head1Length', 'Head2Length', 'Head1Width', 'Head2Width'}, 'Head Size', 'numeric'};
                case 'textarrow'
                    Properties = {'Interpreter', 'Interpreter', {'tex', 'tex'; 'latex', 'latex'; 'none', 'none'}; ...
                        'Color', 'Text Color', 'color'; ...
                        'TextRotation', 'Text Rotation', 'numeric'; ...
                        'Font', 'Font', 'font'; ...
                        'TextLineWidth', 'Text Box Outline Width', 'numeric'; ...
                        'TextEdgeColor', 'Text Box Outline Color', 'color'; ...
                        'TextBackgroundColor', 'Text Background Color', 'color'; ...
                        'TextMargin', 'Text Margin', 'numeric'; ...
                        'Color', 'Line Color', 'color'; ...
                        'LineStyle', 'Line Style', {'-', 'Solid'; '--', 'Dashed'; ':', 'Dotted'; '-.', 'Dash-dotted'}; ...
                        'LineWidth', 'Line Width', 'numeric'; ...
                        'HeadStyle', 'Head Style', {'none', 'None'; 'plain', 'Plain'; 'vback2', 'V-back'; 'cback2', 'C-back'; 'diamond', 'Diamond'; 'deltoid', 'Deltoid'}; ...
                        {'HeadLength', 'HeadWidth'}, 'Head Size', 'numeric'; ...
                        'String', 'String', 'char'};
                case 'textbox'
                    Properties = {'Interpreter', 'Interpreter', {'tex', 'tex'; 'latex', 'latex'; 'none', 'none'}; ...
                        'Color', 'Text Color', 'color'; ...
                        'Font', 'Font', 'font'; ...
                        'LineStyle', 'Line Style', {'-', 'Solid'; '--', 'Dashed'; ':', 'Dotted'; '-.', 'Dash-dotted'}; ...
                        'LineWidth', 'Line Width', 'numeric'; ...
                        'EdgeColor', 'Box Outline Color', 'color'; ...
                        'BackgroundColor', 'Background Color', 'color'; ...
                        'Margin', 'Margin', 'numeric'; ...
                        'HorizontalAlignment', 'Horizontal Alignment', {'left', 'Left'; 'center', 'Lenter'; 'right', 'Right'}; ...
                        'VerticalAlignment', 'Vertical Alignment', {'top', 'Top'; 'middle', 'Middle'; 'bottom', 'Bottom'}; ...
                        'String', 'String', 'char'};
                case {'rectangle', 'ellipse'}
                    Properties = {'Color', 'Outline Color', 'color'; ...
                        'FaceColor', 'Face Color', 'color'; ...
                        'LineStyle', 'Line Style', {'-', 'Solid'; '--', 'Dashed'; ':', 'Dotted'; '-.', 'Dash-dotted'; 'none', 'none'}; ...
                        'LineWidth', 'Line Width', 'numeric'};
            end
            for iP = 1:size(Properties, 1)
                CreateContextMenuEntry(uim, obj.AnnotationHandle, Properties{iP, 1}, Properties{iP, 2}, Properties{iP, 3});
            end
            uimenu(uim, 'Label', 'Delete', 'Separator', 'on', 'Callback', @obj.Delete_Callback);
        end
    end
    
    methods(Static)
        function obj = loadobj(s)
            if isstruct(s)
                obj = ExtendedAnnotation(gca, s.Type);
                CancelInteractivePlacement(obj);
                obj.CopyProperties(s);
                obj.XY = s.XY;
                set(obj.AnnotationHandle, 'Visible', s.Visible);
                obj.AxesChangeListener = addlistener(obj.AxesHandle, {'XLim', 'YLim', 'XLimMode', 'YLimMode', 'Position', 'PlotBoxAspectRatio', 'DataAspectRatio', 'PlotBoxAspectRatioMode', 'DataAspectRatioMode'}, 'PostSet', @obj.AxesLimitsChange_Callback);
                obj.FigureResizeID = iptaddcallback(obj.f, 'ResizeFcn', @obj.AxesLimitsChange_Callback);
                addlistener(obj.AnnotationHandle, 'Position', 'PostSet', @obj.AnnotationPositionChanged_Callback);
            else
                obj = s;
            end
        end
    end
end