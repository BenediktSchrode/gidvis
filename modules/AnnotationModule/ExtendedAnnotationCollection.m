classdef ExtendedAnnotationCollection < handle
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2018 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    properties
        CollectionName
    end
    
    properties (Dependent = true)
        Visible
    end
    
    properties (SetAccess = protected)
        Annotations = ExtendedAnnotation.empty;
    end
    
    properties (Access = private)
        VisibleStorage = 'on';
    end
    
    properties (Dependent = true)
        VisibleBool
    end
    
    methods
        function val = get.VisibleBool(obj)
            if strcmp(obj.Visible, 'on')
                val = true;
            else
                val = false;
            end
        end
        
        function set.Visible(obj, val)
            for iEA = 1:numel(obj.Annotations)
                obj.Annotations(iEA).Visible = val;
            end
            obj.VisibleStorage = val;
        end
        
        function val = get.Visible(obj)
            val = obj.VisibleStorage;
        end
        
        function obj = ExtendedAnnotationCollection(Name)
            if nargin < 1 || isempty(Name)
                Name = '';
            end
            obj.CollectionName = Name;
        end
        
        function AddAnnotation(obj, EA)
            obj.Annotations(end+1) = EA;
        end
        
        function RemoveAnnotation(obj, EA)
            obj.Annotations(obj.Annotations == EA) = [];
        end
        
        function RemoveInvalid(obj)
            obj.Annotations(~isvalid(obj.Annotations)) = [];
        end
        
        function s = saveobj(obj)
            s.CollectionName = obj.CollectionName;
            s.Visible = obj.Visible;
            s.Annotations = obj.Annotations;
        end
    end
    
    methods (Static)
        function obj = loadobj(s)
            if isstruct(s)
                obj = ExtendedAnnotationCollection(s.CollectionName);
                obj.Visible = s.Visible;
                obj.Annotations = s.Annotations;
            else
                obj = s;
            end
        end
    end
end