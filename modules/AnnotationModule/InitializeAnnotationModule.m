function InitializeAnnotationModule(FigTB, GIDVisFig)
% In deployed state, the plot edit toolbar element is not available. This
% function adds a new button to switch the plotedit mode on/off.
%
% Usage:
%     InitializeAnnotationModule(FigTB, GIDVisFig)
%
% Input:
%     FigTB ... toolbar of the main GIDVis window
%     GIDVisFig ... figure handle of the main GIDVis window
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if isdeployed
    % Create an image for the toggle button for the alternative plotedit
    Img = ones(16, 17, 1).*240./255; % "background" for the image, light gray
    Img(2:14, 6) = 0;
    Img(82:17:225) = 0;
    Img(94:15:150) = 0;
    Img(10, 11:13) = 0;
    Img(12, 9) = 0;
    Img(13:14, 10) = 0;
    Img(15:16, 11) = 0;
    Img(16, 11:12) = 0;
    Img(14:15, 13) = 0;
    Img(12:13, 12) = 0;
    Img(11, 11) = 0;
    Img = Img(:, 2:end);

    Img = repmat(Img, 1, 1, 3);
    % use on/off callbacks instead of callback, since for some cases, the
    % plotedit mode will be turned on/off by some other function. Using
    % on/off callback instead of callback ensures that at least after 
    % clicking the button the correct ploteditmode state is selected.
    uitoggletool(FigTB, 'Tag', 'AlternativePlotEdit', ...
        'OnCallback', @(src, evt) AlternativePlotEdit_OnCallback(GIDVisFig), ...
        'OffCallback', @(src, evt) AlternativePlotEdit_OffCallback(GIDVisFig), ...
        'CData', Img);
end

function AlternativePlotEdit_OnCallback(GIDVisFig)
plotedit(GIDVisFig, 'on');

function AlternativePlotEdit_OffCallback(GIDVisFig)
plotedit(GIDVisFig, 'off');