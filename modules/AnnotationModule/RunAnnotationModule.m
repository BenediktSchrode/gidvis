function RunAnnotationModule(MainAxes, AnnotationsPath, hObject)
% function checking availability of image_toolbox license. If exist, start
% AnnotationModule, else, not.
%
% Usage:
%     RunAnnotationModule(MainAxes, AnnotationsPath, hObject)
%
% Input:
%     MainAxes ... handle of the main GIDVis axes
%     AnnotationsPath ... path where the annotations are stored
%     hObject ... handle to button used to open the annotation module
%
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% check for Image_Toolbox license
[stat, ~] = license('checkout', 'Image_Toolbox');

if ~stat
    msgbox('The annotation module requires the image toolbox to run. No license available.', 'No License', 'error', 'modal');
    return
end

AnnotationModule(MainAxes, AnnotationsPath, hObject);