function varargout = BeamlineSetupModule(varargin)
% This file, together with BeamlineSetupModule.fig, is the
% BeamlineSetupModule of GIDVis.
%
% Usage:
%     varargout = BeamlineSetupModule(MainAxesHandle, BeamtimePath, LFMUI)
%
% Input:
%     MainAxesHandle ... handle of the main axes of GIDVis
%     BeamtimePath ... directory where the beamtime files are stored
%     LFMUI ... any UI element of the Load File module
%
% Output:
%     varargout ... default GUIDE output
%
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2020 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Last Modified by GUIDE v2.5 18-Mar-2020 14:07:44

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @BeamlineSetupModule_OpeningFcn, ...
                   'gui_OutputFcn',  @BeamlineSetupModule_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before BeamlineSetupModule is made visible.
function BeamlineSetupModule_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to BeamlineSetupModule (see VARARGIN)

% Choose default command line output for BeamlineSetupModule
handles.output = hObject;

% the settings for this module are stored in the same directory as this
% file and have the name Settings.mat. Get this path
handles.SettingsPath = fullfile(fileparts(mfilename('fullpath')), 'Settings.mat');

% read out settings if available
if exist(handles.SettingsPath, 'file') == 2
    load(handles.SettingsPath);
    handles.DefaultImportPath = SettingsBeamlineSetupModule.DefaultImportPath;
    handles.DefaultExportPath = SettingsBeamlineSetupModule.DefaultExportPath;
else
    % use some default settings
    handles.DefaultImportPath = pwd;
    handles.DefaultExportPath = pwd;
end

% add a listener to the axes in varargin{1}. When the axes gets destroyed 
% (e.g. because the figure containing the axes gets closed), the callback 
% will be executed (and closes this window).
% Store the listener in the handles structure to remove it when this window    
% gets closed by user, otherwise the callback is executed even when the
% LoadFileModule window does not exist anymore.
handles.MainAxesDestroyedListener = addlistener(varargin{1},...
    'ObjectBeingDestroyed', @(src, evt) MainAxesDestroyed_Callback(src,...
    evt, handles.Figure_BeamlineSetupModule));

% store the MainAxes in the handles structure
handles.MainAxes = varargin{1};
% get Beamtime Folder path of GIDVis.m
handles.BeamtimePath = varargin{2};
% get handleOfLoadFileModule to update Text of selected Beamline in
% LoadFileModule
handles.LoadFileModule = varargin{3};

% update the listbox showing the files
UpdateFilesBeamtimeListbox(handles);

% make the selected beamtime in the LoadFileModule the selected beamtime in
% the BeamlineSetupModule
GD = guidata(handles.LoadFileModule);
set(handles.BeamtimeListbox, 'Value', get(GD.BeamtimeSelection, 'Value'));

% update the Listbox showing the beamlines
UpdateFilesBeamlineListbox(handles);

% make the selected beamline in the LoadFileModule the selected beamline in
% the BeamlineSetupModule
set(handles.BeamlineListbox, 'Value', get(GD.BeamlineSelection, 'Value'));

% call the BeamlineListBox_Callback function to populate the table with
% data
BeamlineListbox_Callback(hObject, [], handles);

% variable in the handles structure to store the current flat field
handles.CurrentFlatField = [];

% read out the detectors
DirPath = fileparts(fileparts(mfilename('fullpath')));
S = load(fullfile(DirPath, 'DetectorModule', 'Detectors.mat'));
Detectors = S.Detectors;

% set the detector names in the popup menu
set(handles.PM_Detector, 'String', {Detectors.Name});

% store the detectors in the handles structure
handles.Detectors = Detectors;

% Update handles structure
guidata(hObject, handles);


% UIWAIT makes BeamlineSetupModule wait for user response (see UIRESUME)
% uiwait(handles.Figure_BeamlineSetupModule);


% --- Outputs from this function are returned to the command line.
function varargout = BeamlineSetupModule_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function UpdateFilesBeamtimeListbox(handles)
% Searches mat files in path and adds found files to the listbox
path = handles.BeamtimePath;

% find mat files in the folder defined by path
MatFiles = dir(fullfile(path, '*.mat'));
% set the value of the listbox to 1 (this makes the first file the selected 
% one). The value of the listbox has to be smaller than the number of 
% elements in it.
set(handles.BeamtimeListbox, 'Value', 1);
% show the files in the listbox
set(handles.BeamtimeListbox, 'String', {MatFiles.name});

function UpdateFilesBeamlineListbox(handles)
% get names of Beamlines in Beamtime
contents = cellstr(get(handles.BeamtimeListbox,'String'));
load(fullfile(handles.BeamtimePath,contents{get(handles.BeamtimeListbox,'Value')}));

% get value of the beamline listbox
value = get(handles.BeamlineListbox, 'Value');

% if the value of the BeamlineListbox is larger than the number of 
% elements in the box, set it to 1
if value > numel(beamtime.SetUps)
    set(handles.BeamlineListbox, 'Value', 1);
end

% show the files in the listbox
set(handles.BeamlineListbox, 'String', {beamtime.SetUps.name});

value = get(handles.BeamlineListbox, 'Value');
try
    data = repmat(struct2cell(beamtime.beamline(value)),[1,2]);
catch
    beamtime.AddSetUp(ExperimentalSetUp());
    data = repmat(struct2cell(beamtime.SetUps(value)),[1,2]);
end

% fill table with data
% set(handles.BeamlineTable, 'Data', data);
UpdateBeamlineTable(handles);

function UpdateBeamlineTable(handles, Changing)
if nargin < 2 || isempty(Changing); Changing = 0; end
% get names of Beamlines in Beamtime
contents = cellstr(get(handles.BeamtimeListbox,'String'));
load(fullfile(handles.BeamtimePath,contents{get(handles.BeamtimeListbox,'Value')}));

% get value of the beamline listbox
value = get(handles.BeamlineListbox, 'Value');

% get the selected ExperimentalSetUp
BL = beamtime.SetUps(value);

set(handles.PM_Geometry, 'String', BL.PossibleGeometries);
if ~Changing
    IndexC = strfind(BL.PossibleGeometries, BL.Geometry);
    Index = find(not(cellfun('isempty', IndexC)));
    set(handles.PM_Geometry, 'Value', Index);
end

GeomInd = get(handles.PM_Geometry, 'Value');
Geoms = BL.PossibleGeometries;

switch Geoms{GeomInd}
    case 'none'
        set(handles.TB_delta, 'Visible', 'off');
        set(handles.TB_gamma, 'Visible', 'off');
        set(handles.TB_outeroffset, 'Visible', 'off');
        set(handles.T_gammadelta, 'Visible', 'off');
        set(handles.T_outeroffset, 'Visible', 'off');
    case 'GammaDelta'
        set(handles.TB_delta, 'Visible', 'on');
        set(handles.TB_gamma, 'Visible', 'on');
        set(handles.TB_outeroffset, 'Visible', 'on');
        set(handles.T_gammadelta, 'Visible', 'on');
        set(handles.T_outeroffset, 'Visible', 'on');
        set(handles.TB_gamma, 'String', BL.gamma);
        set(handles.TB_delta, 'String', BL.delta);
        set(handles.TB_outeroffset, 'String', BL.outer_offset);
end
set(handles.TB_Name, 'String', BL.name);
set(handles.TB_detlenx, 'String', BL.detlenx);
set(handles.TB_detlenz, 'String', BL.detlenz);
set(handles.TB_lambda, 'String', BL.lambda);
set(handles.TB_sdd, 'String', BL.sdd);
set(handles.TB_cpx, 'String', BL.cpx);
set(handles.TB_cpz, 'String', BL.cpz);
set(handles.TB_rx, 'String', BL.rx);
set(handles.TB_ry, 'String', BL.ry);
set(handles.TB_rz, 'String', BL.rz);
set(handles.TB_psx, 'String', BL.psx);
set(handles.TB_psz, 'String', BL.psz);
set(handles.TB_DetectorMaterial, 'String', BL.DetectorMaterial.Formula);
set(handles.TB_DetectorDensity, 'String', BL.DetectorMaterial.Density);
set(handles.TB_DetectorThickness, 'String', BL.DetectorMaterial.Thickness);
set(handles.TB_Medium, 'String', BL.Medium.Formula);
set(handles.TB_MediumDensity, 'String', BL.Medium.Density);
set(handles.TB_FractionHorizontal, 'String', BL.HFraction);

handles.CurrentFlatField = BL.FFImageData;

% update handles structure
guidata(handles.Figure_BeamlineSetupModule, handles);

% --- Executes on selection change in BeamlineListbox.
function BeamlineListbox_Callback(hObject, eventdata, handles)
% hObject    handle to BeamlineListbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns BeamlineListbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from BeamlineListbox

% get data of selected Beamline
contents = cellstr(get(handles.BeamtimeListbox,'String'));
value = get(handles.BeamlineListbox, 'Value');
load(fullfile(handles.BeamtimePath,contents{get(handles.BeamtimeListbox,'Value')}));


try
data = repmat(struct2cell(beamtime.SetUps(value)),[1,2]);

catch
    beamtime.AddSetUp(ExperimentalSetUp());
    data = repmat(struct2cell(beamtime.SetUps(value)),[1,2]);
end

% fill table with data
UpdateBeamlineTable(handles);

% --- Executes during object creation, after setting all properties.
function BeamlineListbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to BeamlineListbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in saveBeamline.
function saveBeamline_Callback(hObject, eventdata, handles)
% hObject    handle to saveBeamline (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% disable the save button
set(hObject, 'Enable', 'off')
drawnow

% load the selected beamtime
contents = cellstr(get(handles.BeamtimeListbox,'String'));
load(fullfile(handles.BeamtimePath, contents{get(handles.BeamtimeListbox,'Value')}));

% get the selected experimental setup
Ind = get(handles.BeamlineListbox, 'Value');
SelBL = beamtime.SetUps(Ind);

% construct path to save file
beamlineFilename = fullfile(handles.BeamtimePath,contents{get(handles.BeamtimeListbox,'Value')});

% check if names are the same (overwrite) or different (new beamline setting)
checkSame = strcmp({beamtime.SetUps.name}, get(handles.TB_Name, 'String'));

if all(checkSame == 0) %new entry
    BL = ExperimentalSetUp;
    BL.name = get(handles.TB_Name, 'String');
    BL.sdd = str2double(get(handles.TB_sdd, 'String'));
    BL.psx = str2double(get(handles.TB_psx, 'String'));
    BL.psz = str2double(get(handles.TB_psz, 'String'));
    BL.detlenx = str2double(get(handles.TB_detlenx, 'String'));
    BL.detlenz = str2double(get(handles.TB_detlenz, 'String'));
    BL.cpx = str2double(get(handles.TB_cpx, 'String'));
    BL.cpz = str2double(get(handles.TB_cpz, 'String'));
    BL.lambda = str2double(get(handles.TB_lambda, 'String'));
    BL.rx = str2double(get(handles.TB_rx, 'String'));
    BL.ry = str2double(get(handles.TB_ry, 'String'));
    BL.rz = str2double(get(handles.TB_rz, 'String'));
    BL.HFraction = str2double(get(handles.TB_FractionHorizontal, 'String'));
    BL.BeamHeight = str2double(get(handles.TB_BeamHeight, 'String'));
    BL.BeamWidth = str2double(get(handles.TB_BeamWidth, 'String'));
    BL.DetectorMaterial.Formula = get(handles.TB_DetectorMaterial, 'String');
    BL.DetectorMaterial.Density = str2num(get(handles.TB_DetectorDensity, 'String'));
    BL.DetectorMaterial.Thickness = str2num(get(handles.TB_DetectorThickness, 'String'));
    BL.Medium.Formula = get(handles.TB_Medium, 'String');
    BL.Medium.Density = str2num(get(handles.TB_MediumDensity, 'String'));
    
    BL.FFImageData = handles.CurrentFlatField;
    
    GeomC = get(handles.PM_Geometry, 'String');
    GeomInd = get(handles.PM_Geometry, 'Value');
    BL.Geometry = GeomC{GeomInd};
    switch GeomC{GeomInd}
        case 'none'
            BL.delta = 0;
            BL.gamma = 0;
            BL.outer_offset = 0;
        case 'GammaDelta'
            BL.delta = str2double(get(handles.TB_delta, 'String'));
            BL.gamma = str2double(get(handles.TB_gamma, 'String'));
            BL.outer_offset = str2double(get(handles.TB_outeroffset, 'String'));
    end

    beamtime.AddSetUp(BL);
    
    % in order to avoid error when no write access for file
    % beamlineFileName, use try.
    try; save(beamlineFilename,'beamtime'); end
    
    % and update Listbox in BeamlineSetupModule
    UpdateFilesBeamlineListbox(handles);    
else
    %Construct a questdlg if setup already exists
    choice = questdlg(['Chosen experimental set up ''' get(handles.TB_Name, 'String') ''' already exists; saving will overwrite existing.'], ...
        'Overwrite?', ...
        'Save & Overwrite', 'Cancel', 'Cancel');
        
    % Handle response
    switch choice
        case 'Save & Overwrite'
            % Overwrite
            BL = ExperimentalSetUp;
            BL.name = get(handles.TB_Name, 'String');
            BL.sdd = str2double(get(handles.TB_sdd, 'String'));
            BL.psx = str2double(get(handles.TB_psx, 'String'));
            BL.psz = str2double(get(handles.TB_psz, 'String'));
            BL.detlenx = str2double(get(handles.TB_detlenx, 'String'));
            BL.detlenz = str2double(get(handles.TB_detlenz, 'String'));
            BL.cpx = str2double(get(handles.TB_cpx, 'String'));
            BL.cpz = str2double(get(handles.TB_cpz, 'String'));
            BL.lambda = str2double(get(handles.TB_lambda, 'String'));
            BL.rx = str2double(get(handles.TB_rx, 'String'));
            BL.ry = str2double(get(handles.TB_ry, 'String'));
            BL.rz = str2double(get(handles.TB_rz, 'String'));
            BL.HFraction = str2double(get(handles.TB_FractionHorizontal, 'String'));
            BL.BeamHeight = str2double(get(handles.TB_BeamHeight, 'String'));
            BL.BeamWidth = str2double(get(handles.TB_BeamWidth, 'String'));
            BL.DetectorMaterial.Formula = get(handles.TB_DetectorMaterial, 'String');
            BL.DetectorMaterial.Density = str2num(get(handles.TB_DetectorDensity, 'String'));
            BL.DetectorMaterial.Thickness = str2num(get(handles.TB_DetectorThickness, 'String'));
            BL.Medium.Formula = get(handles.TB_Medium, 'String');
            BL.Medium.Density = str2num(get(handles.TB_MediumDensity, 'String'));
    
            BL.FFImageData = handles.CurrentFlatField;

            GeomC = get(handles.PM_Geometry, 'String');
            GeomInd = get(handles.PM_Geometry, 'Value');
            BL.Geometry = GeomC{GeomInd};
            switch GeomC{GeomInd}
                case 'none'
                    BL.delta = 0;
                    BL.gamma = 0;
                    BL.outer_offset = 0;
                case 'GammaDelta'
                    BL.delta = str2double(get(handles.TB_delta, 'String'));
                    BL.gamma = str2double(get(handles.TB_gamma, 'String'));
                    BL.outer_offset = str2double(get(handles.TB_outeroffset, 'String'));
            end
            
            beamtime.SetUps(checkSame) = BL;
            
            % in order to avoid error when no write access for file
            % beamlineFileName, use try.
            try; save(beamlineFilename,'beamtime'); end
            UpdateFilesBeamlineListbox(handles);
        case 'Cancel'
            % enable the button
            set(hObject, 'Enable', 'on')
            
            return;
    end 
end

% enable the button
set(hObject, 'Enable', 'on')

% update the entries in the load file module
LoadFileModule('UpdateSelectedBeamtime', handles.LoadFileModule);
% and update the plot
RequestPlotUpdateFromData(handles.MainAxes);



% --- Executes on button press in deleteBeamline.
function deleteBeamline_Callback(hObject, eventdata, handles)
% hObject    handle to deleteBeamline (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%get Name of selected Beamline
contents = cellstr(get(handles.BeamlineListbox,'String'));
% last beamline cannot be deleted
if numel(contents) == 1
    msgbox('You cannot delete the last beamline of a beamtime. Delete the whole beamtime instead.', 'modal');
    return;
end
% get the name of the beamline
beamlineName = contents{get(handles.BeamlineListbox,'Value')};

%load beamtime mat file
contents = cellstr(get(handles.BeamtimeListbox,'String'));
load(fullfile(handles.BeamtimePath,contents{get(handles.BeamtimeListbox,'Value')}));

% Construct a questdlg if you really really REALLY want to delete the selected
% beamline
choice = questdlg(['Are you sure you want to delete beamline ''', beamlineName,'''?'], ...
	'Delete Beamline?', ...
	'Delete','Cancel','Cancel');
% Handle response
switch choice
    case 'Delete'
        % construct path and delete file
        %beamlineFilename = fullfile(handles.BeamtimePath,beamlineName);
        %delete(beamlineFilename);
        beamtime.RemoveSetUpAt(get(handles.BeamlineListbox, 'Value'));
        %and save new beamtime structure
        beamlineFilename = fullfile(handles.BeamtimePath,[beamtime.Name,'.mat']);
        % in order to avoid error when no write access for file
        % beamlineFileName, use try.
        try; save(beamlineFilename,'beamtime'); end
        
        set(hObject, 'Enable', 'off');
        drawnow;
        
        % and update Listboxes in BeamlineSetupModule and LoadFileModule
        UpdateFilesBeamlineListbox(handles);
        LoadFileModule('UpdateSelectedBeamtime', handles.LoadFileModule);
    case 'Cancel'
    return;
end
set(hObject, 'Enable', 'on');
drawnow;



% --- Executes on selection change in BeamtimeListbox.
function BeamtimeListbox_Callback(hObject, eventdata, handles)
% hObject    handle to BeamtimeListbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns BeamtimeListbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from BeamtimeListbox
UpdateFilesBeamlineListbox(handles)

% remove flat field image
handles.CurrentFlatField = [];

% update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function BeamtimeListbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to BeamtimeListbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in deleteBeamtime.
function deleteBeamtime_Callback(hObject, eventdata, handles)
% hObject    handle to deleteBeamtime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

contents = cellstr(get(handles.BeamtimeListbox,'String'));
if numel(contents) == 1
    msgbox('You cannot delete the last beamtime.', 'modal');
    return;
end
beamtimeName = contents{get(handles.BeamtimeListbox,'Value')};
% Construct a questdlg if you really really REALLY want to delete the selected
% beamtime
choice = questdlg(['Are you sure you want to delete beamtime ''', beamtimeName,'''?'], ...
	'Delete Beamtime?', ...
	'Delete','Cancel','Cancel');
% Handle response
switch choice
    case 'Delete'
        beamlineFilename = fullfile(handles.BeamtimePath,beamtimeName);
        delete(beamlineFilename);
        % and update Listboxes in BeamlineSetupModule and LoadFileModule
        UpdateFilesBeamtimeListbox(handles);
        UpdateFilesBeamlineListbox(handles);
        
        % we update the popup menu in the LoadFileModule
        LoadFileModule('PopulateBeamtimePopupBox', handles.LoadFileModule);
        % and the selected beamtime
        LoadFileModule('UpdateSelectedBeamtime', handles.LoadFileModule);
    case 'Cancel'
        return;
end

% --- Executes on button press in newBeamtime.
function newBeamtime_Callback(hObject, eventdata, handles)
% hObject    handle to newBeamtime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%first we need a name for the new beamtime
beamtimeTitle = inputdlg('Enter Beamtime Name','New Beamtime',1);

% user might have cancelled or entered no name
if isempty(beamtimeTitle) || isempty(beamtimeTitle{1}); return; end

% check if it already exists
beamtime = Beamtime(beamtimeTitle{1});
beamlineFilename = fullfile(handles.BeamtimePath,[beamtime.Name,'.mat']);
checkExist = exist(beamlineFilename, 'file') == 2;

if checkExist == 0
    %we create a variable with desired name and store it
    beamtime.AddSetUp(ExperimentalSetUp());
    % in order to avoid error when no write access for file
    % beamlineFileName, use try.
    save(beamlineFilename, 'beamtime')

    %and we update listboxes
    UpdateFilesBeamtimeListbox(handles)
    UpdateFilesBeamlineListbox(handles)
    
    % we update the popup menu in the LoadFileModule
    LoadFileModule('PopulateBeamtimePopupBox', handles.LoadFileModule);
else
    h = errordlg('Beamtime already exist','Be creative...');
end



function MainAxesDestroyed_Callback(src, evt, HandleToBeamlineSetupModule)

% close module window
close(HandleToBeamlineSetupModule);

% --- Executes when user attempts to close Figure_BeamlineSetupModule.
function Figure_BeamlineSetupModule_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to Figure_BeamlineSetupModule (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% save settings
SettingsBeamlineSetupModule.DefaultImportPath = handles.DefaultImportPath;
SettingsBeamlineSetupModule.DefaultExportPath = handles.DefaultExportPath;
save(handles.SettingsPath, 'SettingsBeamlineSetupModule');

% delete Listener to MainWindow
delete(handles.MainAxesDestroyedListener);

% Hint: delete(hObject) closes the figure
delete(hObject);




% --- Executes on button press in importBeamtime.
function importBeamtime_Callback(hObject, eventdata, handles)
% hObject    handle to importBeamtime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[FileName, PathName] = uigetfile({'*.mat', 'mat files'}, 'Select the beamtime file', ...
    handles.DefaultImportPath, 'MultiSelect', 'on');
if isequal(FileName, 0) % user selected cancel
    return;
end

% save the selected directory in the handles structure
handles.DefaultImportPath = PathName;

if ~iscell(FileName); FileName = {FileName}; end

SuccessfullyLoaded = cell(0, 1);

% loop over selected files
for iF = 1:numel(FileName)
    % load the selected file
    S = load(fullfile(PathName, FileName{iF}));
    
    % check if variable beamtime exists in file
    if ~isfield(S, 'beamtime')
        h = msgbox(sprintf('Could not import beamtime file ''%s'', because the selected file does not contain a variable ''beamtime''.', FileName{iF}), 'modal');
        uiwait(h); % we need uiwait here to display a messagebox for every file where import fails
        continue;
    end
    % check if it is of correct class
    if ~isa(S.beamtime, 'Beamtime')
        h = msgbox(sprintf('Could not import beamtime file ''%s'', because variable ''beamtime'' is not of ''Beamtime'' class.', FileName{iF}), 'modal');
        uiwait(h); % we need uiwait here to display a messagebox for every file where import fails
        continue;
    end
    % check if the experimental set ups are not empty
    if isempty(S.beamtime.SetUps)
        h = msgbox(sprintf('Could not import beamtime file ''%s'', because variable ''beamtime'' does not contain any Experimental Set Ups.', FileName{iF}), 'modal');
        uiwait(h); % we need uiwait here to display a messagebox for every file where import fails
        continue;
    end
    
    % passed all tests
    beamtime = S.beamtime;
    % create storage path for beamtime to import
    beamlineFilename = fullfile(handles.BeamtimePath, FileName{iF});
    % check if file already exists
    AlreadyExists = exist(beamlineFilename, 'file') == 2;
    % ask user what to do if file already exists
    if AlreadyExists
        choice = questdlg(sprintf('A beamtime file with name ''%s'' already exists. Do you want to overwrite it?', beamtime.Name), 'Overwrite File?', 'Yes', 'No', 'No');
        if strcmpi(choice, 'no')
            continue;
        end
    end
    % save the imported beamtime
    save(beamlineFilename, 'beamtime');
    % add the beamtime name to the list of successfully imported files
    SuccessfullyLoaded{end+1} = FileName{iF};
    % msgbox('Successfully imported beamtime.', 'modal');
end

if ~isempty(SuccessfullyLoaded)
    % display list of successfully imported files
    msgbox(sprintf('Successfully imported beamtime(s):\n%s', strjoin(SuccessfullyLoaded, '\n')), 'modal');

    % and we update listboxes
    UpdateFilesBeamtimeListbox(handles)
    UpdateFilesBeamlineListbox(handles)

    % we update the popup menu in the LoadFileModule
    LoadFileModule('PopulateBeamtimePopupBox', handles.LoadFileModule);
end

% update handles structure
guidata(hObject, handles);


% --- Executes on selection change in PM_Geometry.
function PM_Geometry_Callback(hObject, eventdata, handles)
% hObject    handle to PM_Geometry (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns PM_Geometry contents as cell array
%        contents{get(hObject,'Value')} returns selected item from PM_Geometry
UpdateBeamlineTable(handles, 1);

% --- Executes during object creation, after setting all properties.
function PM_Geometry_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PM_Geometry (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_outeroffset_Callback(hObject, eventdata, handles)
% hObject    handle to TB_outeroffset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_outeroffset as text
%        str2double(get(hObject,'String')) returns contents of TB_outeroffset as a double


% --- Executes during object creation, after setting all properties.
function TB_outeroffset_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_outeroffset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_gamma_Callback(hObject, eventdata, handles)
% hObject    handle to TB_gamma (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_gamma as text
%        str2double(get(hObject,'String')) returns contents of TB_gamma as a double


% --- Executes during object creation, after setting all properties.
function TB_gamma_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_gamma (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_delta_Callback(hObject, eventdata, handles)
% hObject    handle to TB_delta (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_delta as text
%        str2double(get(hObject,'String')) returns contents of TB_delta as a double


% --- Executes during object creation, after setting all properties.
function TB_delta_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_delta (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_Name_Callback(hObject, eventdata, handles)
% hObject    handle to TB_Name (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_Name as text
%        str2double(get(hObject,'String')) returns contents of TB_Name as a double


% --- Executes during object creation, after setting all properties.
function TB_Name_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_Name (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_cpx_Callback(hObject, eventdata, handles)
% hObject    handle to TB_cpx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_cpx as text
%        str2double(get(hObject,'String')) returns contents of TB_cpx as a double


% --- Executes during object creation, after setting all properties.
function TB_cpx_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_cpx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_cpz_Callback(hObject, eventdata, handles)
% hObject    handle to TB_cpz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_cpz as text
%        str2double(get(hObject,'String')) returns contents of TB_cpz as a double


% --- Executes during object creation, after setting all properties.
function TB_cpz_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_cpz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_sdd_Callback(hObject, eventdata, handles)
% hObject    handle to TB_sdd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_sdd as text
%        str2double(get(hObject,'String')) returns contents of TB_sdd as a double


% --- Executes during object creation, after setting all properties.
function TB_sdd_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_sdd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_rx_Callback(hObject, eventdata, handles)
% hObject    handle to TB_rx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_rx as text
%        str2double(get(hObject,'String')) returns contents of TB_rx as a double


% --- Executes during object creation, after setting all properties.
function TB_rx_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_rx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_ry_Callback(hObject, eventdata, handles)
% hObject    handle to TB_ry (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_ry as text
%        str2double(get(hObject,'String')) returns contents of TB_ry as a double


% --- Executes during object creation, after setting all properties.
function TB_ry_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_ry (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_rz_Callback(hObject, eventdata, handles)
% hObject    handle to TB_rz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_rz as text
%        str2double(get(hObject,'String')) returns contents of TB_rz as a double


% --- Executes during object creation, after setting all properties.
function TB_rz_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_rz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_psx_Callback(hObject, eventdata, handles)
% hObject    handle to TB_psx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_psx as text
%        str2double(get(hObject,'String')) returns contents of TB_psx as a double


% --- Executes during object creation, after setting all properties.
function TB_psx_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_psx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_psz_Callback(hObject, eventdata, handles)
% hObject    handle to TB_psz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_psz as text
%        str2double(get(hObject,'String')) returns contents of TB_psz as a double


% --- Executes during object creation, after setting all properties.
function TB_psz_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_psz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_detlenx_Callback(hObject, eventdata, handles)
% hObject    handle to TB_detlenx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_detlenx as text
%        str2double(get(hObject,'String')) returns contents of TB_detlenx as a double


% --- Executes during object creation, after setting all properties.
function TB_detlenx_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_detlenx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_detlenz_Callback(hObject, eventdata, handles)
% hObject    handle to TB_detlenz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_detlenz as text
%        str2double(get(hObject,'String')) returns contents of TB_detlenz as a double


% --- Executes during object creation, after setting all properties.
function TB_detlenz_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_detlenz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_lambda_Callback(hObject, eventdata, handles)
% hObject    handle to TB_lambda (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_lambda as text
%        str2double(get(hObject,'String')) returns contents of TB_lambda as a double


% --- Executes during object creation, after setting all properties.
function TB_lambda_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_lambda (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit16_Callback(hObject, eventdata, handles)
% hObject    handle to edit16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit16 as text
%        str2double(get(hObject,'String')) returns contents of edit16 as a double


% --- Executes during object creation, after setting all properties.
function edit16_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_DetectorMaterial_Callback(hObject, eventdata, handles)
% hObject    handle to TB_DetectorMaterial (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_DetectorMaterial as text
%        str2double(get(hObject,'String')) returns contents of TB_DetectorMaterial as a double


% --- Executes during object creation, after setting all properties.
function TB_DetectorMaterial_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_DetectorMaterial (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_DetectorDensity_Callback(hObject, eventdata, handles)
% hObject    handle to TB_DetectorDensity (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_DetectorDensity as text
%        str2double(get(hObject,'String')) returns contents of TB_DetectorDensity as a double


% --- Executes during object creation, after setting all properties.
function TB_DetectorDensity_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_DetectorDensity (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_DetectorThickness_Callback(hObject, eventdata, handles)
% hObject    handle to TB_DetectorThickness (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_DetectorThickness as text
%        str2double(get(hObject,'String')) returns contents of TB_DetectorThickness as a double


% --- Executes during object creation, after setting all properties.
function TB_DetectorThickness_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_DetectorThickness (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function TB_Medium_Callback(hObject, eventdata, handles)
% hObject    handle to TB_Medium (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_Medium as text
%        str2double(get(hObject,'String')) returns contents of TB_Medium as a double


% --- Executes during object creation, after setting all properties.
function TB_Medium_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_Medium (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_MediumDensity_Callback(hObject, eventdata, handles)
% hObject    handle to TB_MediumDensity (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_MediumDensity as text
%        str2double(get(hObject,'String')) returns contents of TB_MediumDensity as a double


% --- Executes during object creation, after setting all properties.
function TB_MediumDensity_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_MediumDensity (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Btn_Air.
function Btn_Air_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_Air (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.TB_Medium, 'String', 'N78O21Ar');
set(handles.TB_MediumDensity, 'String', '0.0011839');



function TB_BeamHeight_Callback(hObject, eventdata, handles)
% hObject    handle to TB_BeamHeight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_BeamHeight as text
%        str2double(get(hObject,'String')) returns contents of TB_BeamHeight as a double


% --- Executes during object creation, after setting all properties.
function TB_BeamHeight_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_BeamHeight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_BeamWidth_Callback(hObject, eventdata, handles)
% hObject    handle to TB_BeamWidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_BeamWidth as text
%        str2double(get(hObject,'String')) returns contents of TB_BeamWidth as a double


% --- Executes during object creation, after setting all properties.
function TB_BeamWidth_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_BeamWidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_FractionHorizontal_Callback(hObject, eventdata, handles)
% hObject    handle to TB_FractionHorizontal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_FractionHorizontal as text
%        str2double(get(hObject,'String')) returns contents of TB_FractionHorizontal as a double


% --- Executes during object creation, after setting all properties.
function TB_FractionHorizontal_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_FractionHorizontal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Btn_AddFlatField.
function Btn_AddFlatField_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_AddFlatField (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Supported file types
SupportedFileTypes = SupportedFileFormats('full');

% create path to the DefaultFlatFieldImages directory
DirP = fileparts(handles.BeamtimePath);
DFF = fullfile(DirP, 'DefaultFlatFieldImages');

% let user select a file
[FileName, PathName] = uigetfile(SupportedFileTypes, 'Select file', DFF);

if isequal(FileName, 0); return; end % user cancelled file selection

% read data from selected file
FFImage = ReadDataFromFile(fullfile(PathName, FileName));

% get dimensions of the detector
detlenx = str2double(get(handles.TB_detlenx, 'String'));
detlenz = str2double(get(handles.TB_detlenz, 'String'));

% check the size of the selected flat field image (has to be equal to or
% larger than the detector)
if size(FFImage, 2) < detlenx || size(FFImage, 1) < detlenz
    msgbox('The selected flat field image has not the right dimensions. Aborting.', 'modal');
    return;
end

% read out data from selected file and store it in the handles structure
handles.CurrentFlatField = FFImage;

% update handles structure
guidata(hObject, handles);


% --- Executes on button press in Btn_ShowFlatField.
function Btn_ShowFlatField_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_ShowFlatField (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% get names of Beamlines in Beamtime
contents = cellstr(get(handles.BeamtimeListbox,'String'));
load(fullfile(handles.BeamtimePath,contents{get(handles.BeamtimeListbox,'Value')}));

% get value of the beamline listbox
value = get(handles.BeamlineListbox, 'Value');

% get the selected ExperimentalSetUp
BL = beamtime.SetUps(value);

if isempty(handles.CurrentFlatField) && isempty(BL.FFImageData)
    msgbox('There is no flat field image for this experimental set up. Select a flat field image using ''Add Flat Field Image'' and save the experimental set up.', 'modal');
    return;
end

f = figure;
ax = axes('Parent', f);
if ~isempty(handles.CurrentFlatField)
    imagesc(handles.CurrentFlatField, 'Parent', ax);
    caxis(ax, getColorBarLimits(handles.CurrentFlatField));
else
    imagesc(BL.FFImageData, 'Parent', ax);
    caxis(ax, getColorBarLimits(BL.FFImageData));
end


% --- Executes on button press in Btn_RemoveFlatField.
function Btn_RemoveFlatField_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_RemoveFlatField (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% remove flat field image from handles structure
handles.CurrentFlatField = [];

% get names of Beamlines in Beamtime
contents = cellstr(get(handles.BeamtimeListbox,'String'));
load(fullfile(handles.BeamtimePath,contents{get(handles.BeamtimeListbox,'Value')}));

% get value of the beamline listbox
value = get(handles.BeamlineListbox, 'Value');

% get the selected ExperimentalSetUp
BL = beamtime.SetUps(value);

% remove flat field from selected experimental set up
BL.FFImageData = [];

% update handles structure
guidata(hObject, handles);


% --- Executes on selection change in PM_Detector.
function PM_Detector_Callback(hObject, eventdata, handles)
% hObject    handle to PM_Detector (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns PM_Detector contents as cell array
%        contents{get(hObject,'Value')} returns selected item from PM_Detector


% --- Executes during object creation, after setting all properties.
function PM_Detector_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PM_Detector (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Btn_LoadDetector.
function Btn_LoadDetector_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_LoadDetector (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% get the index of the currently selected detector
ind = get(handles.PM_Detector, 'Value');

% get the detector
D = handles.Detectors(ind);

% write the properties of the detector in the corresponding fields
set(handles.TB_detlenx, 'String', D.detlenx);
set(handles.TB_detlenz, 'String', D.detlenz);
set(handles.TB_psx, 'String', D.psx);
set(handles.TB_psz, 'String', D.psz);
set(handles.TB_DetectorMaterial, 'String', D.DetectorMaterial);
set(handles.TB_DetectorDensity, 'String', D.DetectorMaterialDensity);
set(handles.TB_DetectorThickness, 'String', D.DetectorMaterialThickness);


% --- Executes on button press in exportBeamtime.
function exportBeamtime_Callback(hObject, eventdata, handles)
% hObject    handle to exportBeamtime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% get the list of the beamtimes
contents = cellstr(get(handles.BeamtimeListbox,'String'));

% get the selected beamtime
beamtimeName = contents{get(handles.BeamtimeListbox, 'Value')};

% construct the name of the file
beamlineFilename = fullfile(handles.BeamtimePath, beamtimeName);

% let user select a place to save to
[filename, pathname] = uiputfile({'*.mat', '*.mat file'}, 'Save Beamtime', ...
    fullfile(handles.DefaultExportPath, beamtimeName));

% user cancelled save process
if isequal(filename, 0) || isequal(pathname, 0)
    return;
end

% store the path in the handles structure
handles.DefaultExportPath = pathname;

% copy beamtime file to the place the user selected
[status, msg] = copyfile(beamlineFilename, fullfile(pathname, filename));

% show result of operation
if status
    msgbox('Successfully exported beamtime.', 'modal');
else
    msgbox(sprintf('Error exporting beamtime:\n\n%s', msg), 'modal')
end

% update handles structure
guidata(hObject, handles);
