function varargout = CalibrationWizard(varargin)
% This file, together with CalibrationWizard.fig, is the CalibrationWizard
% of GIDVis.
%
% Usage:
%     varargout = CalibrationWizard(MainAxesHandle, LFMUI)
%
% Input:
%     MainAxesHandle ... handle of the main axes of GIDVis
%     LFMUI ... any UI element of the Load File Module
%
% Output:
%     varargout ... default GUIDE output
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2020 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Last Modified by GUIDE v2.5 12-Mar-2019 11:09:11

% Begin initialization code - DO NOT EDIT
gui_Singleton = 0;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @CalibrationWizard_OpeningFcn, ...
                   'gui_OutputFcn',  @CalibrationWizard_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before CalibrationWizard is made visible.
function CalibrationWizard_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to CalibrationWizard (see VARARGIN)

% Choose default command line output for CalibrationWizard
handles.output = hObject;

% set the order of the panels
handles.PanelOrder = [handles.P_LoadImage, handles.P_Experiment, ...
    handles.P_StartValues, handles.P_ImageBackground, ...
    handles.P_Material, handles.P_Algorithm, ...
    handles.P_RunningOptimization, handles.P_Results];

% make a bool variable, indicating whether switching from one to the next
% panel is allowed, i.e. if SwitchAllowed(1), it is allowed to go from
% panel 1 to panel 2
handles.SwitchAllowed = false(numel(handles.PanelOrder)-1, 1);
handles.SwitchAllowed(2:3) = true;
handles.SwitchAllowed(5:6) = true;


% make the panels the right size and put them in the correct position
% get the size of the window first
OldUnits = get(hObject, 'Units');
set(hObject, 'Units', 'pixels');
WinPos = get(hObject, 'Position');
set(hObject, 'Units', OldUnits);
set(handles.PanelOrder, 'Units', 'pixels', 'Position', [7 40 WinPos(3)-14 WinPos(4)-46], ...
    'Units', 'normalized')

% current index (where are we in the list of the panels)
handles.CurrentIndex = 1;

% cell array containing function to run when clicking on next
handles.NextFunctions = cell(size(handles.PanelOrder));
% add a function which runs before showing the Calculation Panel
handles.NextFunctions{7} = @(x) PerformCalculation(x);
% add a function which runs before showing the Image Background Panel
handles.NextFunctions{4} = @(x) InitializeBackgroundTable(x);
% add a function which runs before showing the Experiment Panel
handles.NextFunctions{2} = @(x) InitializeAngleTable(x);
% add a function which runs before showing the Start Parameters Panel
handles.NextFunctions{3} = @(x) InitializeStartParametersPanel(x);

% get the detectors
D = ReadOutDetectors;

% write the detector names into the listbox
set(handles.LB_Detectors, 'String', {D.Name});

% store the detectors in the handles structure
handles.Detectors = D;

% initialize variable used to stop the calibration process
handles.StopCalibration = false;

% add a listener to the axes in varargin{1}. When the axes gets destroyed 
% (e.g. because the figure containing the axes gets closed), the callback 
% will be executed (and closes this window).
% Store the listener in the handles structure to remove it when this window    
% gets closed by user, otherwise the callback is executed even when the
% LoadFileModule window does not exist anymore.
handles.MainAxesDestroyedListener = addlistener(varargin{1}, 'ObjectBeingDestroyed', @(src, evt) MainAxesDestroyed_Callback(src, evt, handles.Fig_CalibrationWizard));

% the settings for this module are stored in the same directory as the file
% CalibrationWizard.m. Store its path in the handles structure to reuse it.
handles.SettingsPath = fullfile(fileparts(mfilename('fullpath')), 'Settings.mat');

% read out settings if available
if exist(handles.SettingsPath, 'file') == 2
    S = load(handles.SettingsPath);
    CalibrationWizardSettings = S.CalibrationWizardSettings;
    if isfield(CalibrationWizardSettings, 'SelectedImagePath')
        handles.DefaultOpenPath = CalibrationWizardSettings.SelectedImagePath;
    end
    
    set(handles.PM_DeltaGammaOrder, 'Value', CalibrationWizardSettings.PM_DeltaGammaOrder);
    set(handles.RB_GoniometerNone, 'Value', ~CalibrationWizardSettings.CB_DeltaGammaGoniometer);
    set(handles.RB_DeltaGammaGoniometer, 'Value', CalibrationWizardSettings.CB_DeltaGammaGoniometer);
    if CalibrationWizardSettings.LB_Detectors <= numel(D)
        set(handles.LB_Detectors, 'Value', CalibrationWizardSettings.LB_Detectors);
    end
    set(handles.TB_Wavelength, 'String', CalibrationWizardSettings.TB_Wavelength);
    set(handles.TB_alphai, 'String', CalibrationWizardSettings.TB_alphai);
    set(handles.TB_cpx, 'String', CalibrationWizardSettings.TB_cpx);
    set(handles.TB_cpz, 'String', CalibrationWizardSettings.TB_cpz);
    set(handles.TB_sdd, 'String', CalibrationWizardSettings.TB_sdd);
    set(handles.TB_rx, 'String', CalibrationWizardSettings.TB_rx);
    set(handles.TB_ry, 'String', CalibrationWizardSettings.TB_ry);
    set(handles.TB_rz, 'String', CalibrationWizardSettings.TB_rz);
    set(handles.TB_outeroffset, 'String', CalibrationWizardSettings.TB_outeroffset);
    set(handles.TB_cpx, 'Value', CalibrationWizardSettings.CB_cpx);
    set(handles.TB_cpz, 'Value', CalibrationWizardSettings.CB_cpz);
    set(handles.TB_sdd, 'Value', CalibrationWizardSettings.CB_sdd);
    set(handles.TB_rx, 'Value', CalibrationWizardSettings.CB_rx);
    set(handles.TB_ry, 'Value', CalibrationWizardSettings.CB_ry);
    set(handles.TB_rz, 'Value', CalibrationWizardSettings.CB_rz);
    set(handles.TB_outeroffset, 'Value', CalibrationWizardSettings.CB_outeroffset);
    set(handles.TB_dSpacing, 'String', CalibrationWizardSettings.TB_dSpacing);
    set(handles.TB_TStart, 'String', CalibrationWizardSettings.TB_TStart);
    set(handles.TB_NumTempSteps, 'String', CalibrationWizardSettings.TB_NumTempSteps);
    set(handles.TB_TrialsPerTempStep, 'String', CalibrationWizardSettings.TB_TrialsPerTempStep);
    set(handles.TB_ToleranceQ, 'String', CalibrationWizardSettings.TB_ToleranceQ);
    if isfield(CalibrationWizardSettings, 'UIT_Background')
        set(handles.UIT_Background, 'Data', CalibrationWizardSettings.UIT_Background);
    end
    if isfield(CalibrationWizardSettings, 'UIT_GoniometerAngles')
        set(handles.UIT_GoniometerAngles, 'Data', CalibrationWizardSettings.UIT_GoniometerAngles);
    end
    if isfield(CalibrationWizardSettings, 'PM_AddPixels')
        set(handles.PM_AddPixels, 'Value', CalibrationWizardSettings.PM_AddPixels);
    end
else % use some default
    % run Btn_LaB6_Callback once to insert the d spacing of LaB6 in the box
    Btn_LaB6_Callback(handles.Btn_LaB6, [], handles);
end

% set a default path for the uigetfile call where the user has to
% select the image to use for calibration
if ~isfield(handles, 'DefaultOpenPath')
    handles.DefaultOpenPath = pwd;
end

% run the RB_DeltaGammaGoniometer callback once to adapt the visibility of the UI
% controls
P_Geometry_SelectionChangedFcn(handles.P_Geometry, [], handles);

% store GIDVis main axes in handles structure
handles.GIDVisMainAxes = varargin{1};

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes CalibrationWizard wait for user response (see UIRESUME)
% uiwait(handles.Fig_CalibrationWizard);


function MainAxesDestroyed_Callback(src, evt, HandleToModuleWindow)
% close module window
close(HandleToModuleWindow);


function D = ReadOutDetectors
% read out detectors
DirPath = fileparts(fileparts(mfilename('fullpath')));
S = load(fullfile(DirPath, 'DetectorModule', 'Detectors.mat'));
D = S.Detectors;

% --- Outputs from this function are returned to the command line.
function varargout = CalibrationWizard_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function StepBackNext(hObject, Step)
% function to go forward/backwards in the panels

% get handles structure
handles = guidata(hObject);

% check if switching is allowed
if ((Step > 0) && (handles.CurrentIndex + Step > numel(handles.PanelOrder) || ...
        ~handles.SwitchAllowed(handles.CurrentIndex))) || handles.CurrentIndex + Step <= 0
    switch handles.CurrentIndex
        case 1
            msgbox('Please select a file first.', 'modal');
        case 4
            msgbox('Please apply the background threshold first.', 'modal');
        case 7
            msgbox('Please do the calibration by clicking on ''Start Calibration'' first.', 'modal');
        case numel(handles.PanelOrder)
            close(ancestor(hObject, 'figure'));
    end
    return;
end

% check if size of the currently selected detector fits to the image size
if handles.CurrentIndex == 2 && Step > 0
    % get index of selected detector
    DetInd = get(handles.LB_Detectors, 'Value');
    % get selected detector
    Detector = handles.Detectors(DetInd);
    if size(handles.ImageData, 1) ~= Detector.detlenz || ...
            size(handles.ImageData, 2) ~= Detector.detlenx
        msgbox('The size of the loaded image data does not fit the size of the detector. Please select the correct detector.', 'modal');
        return;
    end

    % check if an image with 0 degree in-plane angle and 0 degree
    % out-of-plane angle is included when using a goniometer
    if ~get(handles.RB_GoniometerNone, 'Value')
        % get the table data
        D = get(handles.UIT_GoniometerAngles, 'Data');

        % convert the numeric part of the cell to a matrix
        M = cell2mat(D(:, 2:end));

        % check if one row of the matrix contains only zeros
        if ~any(all(M == 0, 2))
            Sel = questdlg({'It is recommended to use a calibration image with in- and out-of-plane angle equal to zero because the center pixel is defined for this detector position.', ...
                'Do you want to continue anyway?'}, '', 'Yes', 'No', 'No');
            if strcmpi(Sel, 'no')
                return;
            end
        end
    end
end

% disable buttons
set([handles.Btn_Next, handles.Btn_Back], 'Enable', 'off');
drawnow();

% set busy cursor
set(handles.Fig_CalibrationWizard, 'Pointer', 'watch');

% we have to turn off the visibility of the current panel
set(handles.PanelOrder(handles.CurrentIndex), 'Visible', 'off');

% change the current index by step
handles.CurrentIndex = handles.CurrentIndex + Step;

% make the now current panel visible
set(handles.PanelOrder(handles.CurrentIndex), 'Visible', 'on');

% run function
if ~isempty(handles.NextFunctions{handles.CurrentIndex})
    handles.NextFunctions{handles.CurrentIndex}(hObject);
end

% update handles structure
guidata(hObject, handles);

% enable buttons
set([handles.Btn_Next, handles.Btn_Back], 'Enable', 'on');

% check if the current index == 1, then the back button has to be disabled
if handles.CurrentIndex == 1
    set(handles.Btn_Back, 'Enable', 'off')
else
    set(handles.Btn_Back, 'Enable', 'on')
end

% check if the current index == numel(handles.PanelOrder), then the next
% button has to be disabled
if handles.CurrentIndex == numel(handles.PanelOrder)
    set(handles.Btn_Next, 'String', 'Close')
else
    set(handles.Btn_Next, 'String', 'Next')
end

% set busy cursor
set(handles.Fig_CalibrationWizard, 'Pointer', 'arrow');

% --- Executes on button press in Btn_Next.
function Btn_Next_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_Next (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
StepBackNext(hObject, 1)

% --- Executes on button press in Btn_Back.
function Btn_Back_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_Back (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
StepBackNext(hObject, -1)


% --- Executes on button press in Btn_LoadImage.
function Btn_LoadImage_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_LoadImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Supported file types
SupportedFileTypes = SupportedFileFormats('full');

% let user select a file
[FileName, PathName] = uigetfile(SupportedFileTypes, 'Select file', handles.DefaultOpenPath, ...
    'MultiSelect', 'on');

if isequal(FileName, 0); return; end % user cancelled file selection

if ~iscell(FileName); FileName = {FileName}; end

% reset the SwitchAllowed
handles.SwitchAllowed = false(numel(handles.PanelOrder)-1, 1);
handles.SwitchAllowed(2:3) = true;
handles.SwitchAllowed(5:6) = true;

% set the window name with the file name
set(handles.Fig_CalibrationWizard, 'Name', ['Calibration Wizard (', FileName{1}, ')']);

% create the full file path
SelectedImage = fullfile(PathName, FileName);
% update the text showing the currently selected image file
T = sprintf('Currently selected:\n');
for iN = 1:numel(FileName)
    T = sprintf('%s\n%s', T, SelectedImage{iN});
end
set(handles.L_CurrentImage, 'String', T);
% read out the image data
handles.ImageData = ReadDataFromFile(SelectedImage);

% reset the background data
handles.ImageData_BG = cell(0,0);

% store the image path in the handles structure
handles.SelectedImagePath = SelectedImage;

% store the image path in the handles structure for the default selection
% path
handles.DefaultOpenPath = SelectedImage{1};

% user has selected an image, so is now allowed to go to the next panel
handles.SwitchAllowed(handles.CurrentIndex) = true;

% clear the axes with the image background
cla(handles.Ax_ImageBackground);

% update handles structure
guidata(hObject, handles);



function TB_BackgroundThreshold_Callback(hObject, eventdata, handles)
% hObject    handle to TB_BackgroundThreshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_BackgroundThreshold as text
%        str2double(get(hObject,'String')) returns contents of TB_BackgroundThreshold as a double


% --- Executes during object creation, after setting all properties.
function TB_BackgroundThreshold_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_BackgroundThreshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Btn_ApplyBackgroundThreshold.
function Btn_ApplyBackgroundThreshold_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_ApplyBackgroundThreshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% disable the apply button
set(hObject, 'Enable', 'off')
drawnow

% check if there is a license for the image toolbox
stat = license('checkout', 'Image_Toolbox');

N = size(handles.ImageData, 3);
imgH = flipud(findobj(handles.Ax_ImageBackground, 'Type', 'scatter'));
if numel(imgH) > size(handles.ImageData_BG, 2)
    delete(imgH(size(handles.ImageData_BG, 2)+1:end));
end
Cols = [1 0 0; 0 1 0; 0 0 1];
Cols = repmat(Cols, N, 1);

D = get(handles.UIT_Background, 'Data');
Thresholds = D(:, 1);
DS = D(:, 2);
Visible = D(:, 3);

BL_Initial = CreateBLFromGUIValues(hObject);

% get alphai
alpha_i = str2double(get(handles.TB_alphai, 'String'));
chi = 0;

for iF = 1:N
    if ~Visible{iF} && numel(imgH) >= iF && isgraphics(imgH(iF))
        set(imgH(iF), 'Visible', 'off');
        continue;
    end
    D = handles.ImageData(:, :, iF);
    
    if stat
        % use imopen to determine background of image
        background = imopen(D, strel('disk', DS{iF}));
        % subtract background from image
        data_mask = D - background;
    else
        warning('GIDVis:CalibrationWizard:NoImageToolboxLicense', ...
            'Falling back to simple thresholding since no Image Toolbox License was found.');
        data_mask = handles.ImageData(:, :, iF);
    end
    
    % apply threshold to image
    data_mask(data_mask < Thresholds{iF}) = 0;
    data_mask(~(data_mask < Thresholds{iF})) = 1;
    
    % deal with the additional number of pixels to remove next to the gaps
    % get content of popup menu
    Vals = get(handles.PM_AddPixels, 'String');
    % get index of selected element
    ind = get(handles.PM_AddPixels, 'Value');
    % get the selected number
    Val = str2double(Vals{ind});
    % use the Val to remove pixels next to gaps    
    nRows = Val;
    nCols = Val;
    LRow = find(all(D < 0, 2));
    for iN = 1:nRows
        L0 = LRow <= iN;
        Lend = LRow >= (size(D, 1) - iN);
        data_mask(LRow(~L0)-iN, :) = 0;
        data_mask(LRow(~Lend)+iN, :) = 0;
    end
    LColumn = find(all(D < 0, 1));
    for iN = 1:nCols
        L0 = LColumn <= iN;
        Lend = LColumn >= (size(D, 2) - iN);
        data_mask(:, LColumn(~L0)-iN) = 0;
        data_mask(:, LColumn(~Lend)+iN, :) = 0;
    end

    % create x and z values of the detector
    [x, z] = meshgrid(1:size(D, 2), 1:size(D, 1));
    x = x(logical(data_mask));
    z = z(logical(data_mask));

    [qxy, qz] = CalculateQ(BL_Initial(iF), alpha_i, chi, x(:), z(:));

    if ~isempty(imgH) && numel(imgH) >= iF && isgraphics(imgH(iF))
        set(imgH(iF), 'XData', qxy, 'YData', qz, ...
            'CData', Cols(iF, :), 'Visible', 'on')
    else
        imgH(iF) = scatter(handles.Ax_ImageBackground, qxy, qz, 1, Cols(iF, :), 'filled', 'Visible', 'on');
        hold(handles.Ax_ImageBackground, 'on');
    end
    if ~Visible{iF}
        set(imgH(iF), 'Visible', 'off')
    end
    
    % save image data in handles structure;
    handles.ImageData_BG{iF, 1} = logical(data_mask);
    handles.ImageData_BG{iF, 2} = x;
    handles.ImageData_BG{iF, 3} = z;
    handles.ImageData_BG{iF, 4} = data_mask;
end

set(handles.Ax_ImageBackground, 'YDir', 'normal');
xlim(handles.Ax_ImageBackground, 'auto')
ylim(handles.Ax_ImageBackground, 'auto')
caxis(handles.Ax_ImageBackground, 'auto')
set(handles.Ax_ImageBackground, 'XTick', [], 'YTick', []);
axis(handles.Ax_ImageBackground, 'equal');
set(handles.Ax_ImageBackground, 'Box', 'on');

% user applied background, is now allowed to go to next panel
handles.SwitchAllowed(handles.CurrentIndex) = true;

% update handles structure
guidata(hObject, handles);

% re-enable button
set(hObject, 'Enable', 'on')
drawnow();


% --- Executes on button press in RB_DeltaGammaGoniometer.
function RB_DeltaGammaGoniometer_Callback(hObject, eventdata, handles)
% hObject    handle to RB_DeltaGammaGoniometer (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% switch the visibility of GUI elements according to the goniometer
% selection
if get(hObject, 'Value')
    set(handles.L_DeltaGammaOrder, 'Visible', 'on');
    set(handles.PM_DeltaGammaOrder, 'Visible', 'on');
    set(handles.TB_outeroffset, 'Visible', 'on');
    set(handles.L_outeroffset, 'Visible', 'on');
    set(handles.CB_outeroffset, 'Visible', 'on');
    set(handles.UIT_GoniometerAngles, 'Visible', 'on');
else
    set(handles.L_DeltaGammaOrder, 'Visible', 'off');
    set(handles.PM_DeltaGammaOrder, 'Visible', 'off');
    set(handles.TB_outeroffset, 'Visible', 'off');
    set(handles.L_outeroffset, 'Visible', 'off');
    set(handles.CB_outeroffset, 'Visible', 'off');
    set(handles.UIT_GoniometerAngles, 'Visible', 'off');
end

% --- Executes on selection change in PM_DeltaGammaOrder.
function PM_DeltaGammaOrder_Callback(hObject, eventdata, handles)
% hObject    handle to PM_DeltaGammaOrder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns PM_DeltaGammaOrder contents as cell array
%        contents{get(hObject,'Value')} returns selected item from PM_DeltaGammaOrder


% --- Executes during object creation, after setting all properties.
function PM_DeltaGammaOrder_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PM_DeltaGammaOrder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_Delta_Callback(hObject, eventdata, handles)
% hObject    handle to TB_Delta (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_Delta as text
%        str2double(get(hObject,'String')) returns contents of TB_Delta as a double


% --- Executes during object creation, after setting all properties.
function TB_Delta_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_Delta (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_Gamma_Callback(hObject, eventdata, handles)
% hObject    handle to TB_Gamma (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_Gamma as text
%        str2double(get(hObject,'String')) returns contents of TB_Gamma as a double


% --- Executes during object creation, after setting all properties.
function TB_Gamma_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_Gamma (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in LB_Detectors.
function LB_Detectors_Callback(hObject, eventdata, handles)
% hObject    handle to LB_Detectors (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns LB_Detectors contents as cell array
%        contents{get(hObject,'Value')} returns selected item from LB_Detectors


% --- Executes during object creation, after setting all properties.
function LB_Detectors_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LB_Detectors (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_Wavelength_Callback(hObject, eventdata, handles)
% hObject    handle to TB_Wavelength (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_Wavelength as text
%        str2double(get(hObject,'String')) returns contents of TB_Wavelength as a double


% --- Executes during object creation, after setting all properties.
function TB_Wavelength_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_Wavelength (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Btn_InteractiveSelection.
function Btn_InteractiveSelection_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_InteractiveSelection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% get the table data
D = get(handles.UIT_GoniometerAngles, 'Data');

% convert the numeric part of the cell to a matrix
M = cell2mat(D(:, 2:end));

% check for rows containing only zeros
L = all(M == 0, 2);
if ~any(L)
    msgbox('Interactive selection is not available since no image with in- and out-of-plane angle equal to 0 degrees is available.', 'modal');
    return;
end

% call function which allows user to interactively select points on ring to
% find the center pixel
C = CircleSelection(handles.ImageData(:, :, find(L, 1)));
% set the selected center pixel in the textboxes accordingly
set(handles.TB_cpx, 'String', num2str(C(1)));
set(handles.TB_cpz, 'String', num2str(C(2)));

function TB_dSpacing_Callback(hObject, eventdata, handles)
% hObject    handle to TB_dSpacing (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_dSpacing as text
%        str2double(get(hObject,'String')) returns contents of TB_dSpacing as a double


% --- Executes during object creation, after setting all properties.
function TB_dSpacing_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_dSpacing (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Btn_LaB6.
function Btn_LaB6_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_LaB6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% write the d spacing of LaB6 into the text box
set(handles.TB_dSpacing, 'String', '4.15692 2.93939 2.4 2.07846 1.85903 1.69706 1.46969 1.38564 1.31453 1.25336 1.2 1.15292 1.11098 1.03923 1.0082 1.0082 0.979795 0.979795 0.953663 0.929516 0.907114 0.886258 0.848528 0.831384 0.831384');
% source: identifier: LaB6_660a


% --- Executes on button press in Btn_AgBeh.
function Btn_AgBeh_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_AgBeh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% write the d spacing of AgBeh into the text box
set(handles.TB_dSpacing, 'String', '58.33520 29.16760 19.44510 14.58380 11.66700 9.72254 8.33360 7.29190 6.48169 5.83352 5.30320 4.86127');
% source: Blanton, T. N. et. al. Powder Diffraction (2011), 26, 313.
%         identifier: 1507774


% --- Executes on button press in Btn_Si.
function Btn_Si_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_Si (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% write the d spacing of Si into the text box
set(handles.TB_dSpacing, 'String', '3.13556 1.92013 1.63749 1.35774 1.24594 1.10859 1.04519 1.04519 0.960064 0.917997 0.858707 0.828211');
% source: Hom, T. et. al. Jornal of Applied Crystallography (1975), 8, 457
%         identifier: 9011998


function TB_TStart_Callback(hObject, eventdata, handles)
% hObject    handle to TB_TStart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_TStart as text
%        str2double(get(hObject,'String')) returns contents of TB_TStart as a double


% --- Executes during object creation, after setting all properties.
function TB_TStart_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_TStart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_TolerancePx_Callback(hObject, eventdata, handles)
% hObject    handle to TB_TolerancePx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_TolerancePx as text
%        str2double(get(hObject,'String')) returns contents of TB_TolerancePx as a double


% --- Executes during object creation, after setting all properties.
function TB_TolerancePx_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_TolerancePx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_ToleranceQ_Callback(hObject, eventdata, handles)
% hObject    handle to TB_ToleranceQ (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_ToleranceQ as text
%        str2double(get(hObject,'String')) returns contents of TB_ToleranceQ as a double


% --- Executes during object creation, after setting all properties.
function TB_ToleranceQ_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_ToleranceQ (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Btn_AddToBeamtime.
function Btn_AddToBeamtime_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_AddToBeamtime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% prepare inputdlg
prompt = cell(numel(handles.BL_Best), 1);
dlg_title = 'Add to Beamtime';
num_lines = 1;
defaultans = prompt;

for iBL = 1:numel(handles.BL_Best)
    prompt{iBL} = ['Experimental Set Up Name ', num2str(iBL), ':'];
    defaultans{iBL} = ['Default Name ', num2str(iBL)];
end

% ask user for names
answer = inputdlg(prompt, dlg_title, num_lines, defaultans);

if isempty(answer)
    return;
end

if any(cellfun(@isempty, answer))
    msgbox('You have to enter names in all fields!', 'modal')
    return;
end

% find the beamtime path
gd = guidata(handles.GIDVisMainAxes);
BTPath = gd.BeamtimePath;

% let user select a file
[FileName, PathName] = uigetfile({'*.mat', 'mat Files'}, 'Select beamtime', BTPath);
if isequal(FileName, 0)
    return;
end

% load selected Beamtime file
load(fullfile(PathName, FileName));

% check for existence of variable beamtime. This will be available if the
% *.mat file contains a beamtime.
if ~exist('beamtime', 'var')
    msgbox('Selected file is not a valid beamtime.', 'modal');
    return;
end

% check for ExperimentalSetUp of same name
% L = strcmp({beamtime.SetUps.name}, answer{1});
Existing = intersect({beamtime.SetUps.name}, answer);
if ~isempty(Existing)
    Message = sprintf('The following experimental setups already exist in the selected beamtime:\n');
    for iE = 1:numel(Existing)
        Message = sprintf('%s\n%s', Message, Existing{iE});
    end
    Message = sprintf('%s\n\nOverwrite?', Message);
    button = questdlg(Message, 'Overwrite?', 'Yes', 'No', 'No');
    if strcmp(button, 'No')
        return;
    end
end

SetUp = handles.BL_Best;
for iBL = 1:numel(SetUp)
    L = ismember({beamtime.SetUps.name}, answer{iBL});
    SetUp(iBL).name = answer{iBL};
    if any(L)
        beamtime.SetUps(L) = SetUp(iBL);
    else
        beamtime.AddSetUp(SetUp(iBL));
    end
end

% in order to avoid error when no write access for file use try.
Success = true;
try
    save(fullfile(PathName, FileName), 'beamtime');
catch
    Success = false;
end

if Success
    msgbox(sprintf('Successfully saved the set up(s) in beamtime ''%s''.\nUse the Beamtime/Beamline Setup module to adjust further parameters.', beamtime.Name), 'modal');
else
    msgbox('Could not save beamtime.', 'modal');
end

% update beamline listbox in LoadFileModule
LoadFileModule('UpdateSelectedBeamtime', gd.LoadFileModule);

% --- Executes on button press in Btn_CreateBeamtime.
function Btn_CreateBeamtime_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_CreateBeamtime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% prepare inputdlg
prompt = cell(1+numel(handles.BL_Best), 1);
dlg_title = 'Create New Beamtime';
num_lines = 1;
defaultans = prompt;

prompt{1} = 'Beamtime Name:';
defaultans{1} = 'New Beamtime';

for iBL = 1:numel(handles.BL_Best)
    prompt{1+iBL} = ['Experimental Set Up Name ', num2str(iBL), ':'];
    defaultans{1+iBL} = ['Default Name ', num2str(iBL)];
end

% ask user for names
answer = inputdlg(prompt, dlg_title, num_lines, defaultans);

if isempty(answer)
    return;
end

if any(cellfun(@isempty, answer))
    msgbox('You have to enter names in all fields!', 'modal')
    return;
end

% find the beamtime path
gd = guidata(handles.GIDVisMainAxes);
BTPath = gd.BeamtimePath;

if exist(fullfile(BTPath, [answer{1}, '.mat']), 'file')
    button = questdlg(sprintf('Beamtime with name %s already exists. Overwrite?', answer{1}), ...
        'Overwrite?', 'Yes', 'No', 'No');
    if strcmp(button, 'No')
        return;
    end
end

beamtime = Beamtime(answer{1});

SetUp = handles.BL_Best;
for iBL = 1:numel(SetUp)
    SetUp(iBL).name = answer{1+iBL};
    beamtime.AddSetUp(SetUp(iBL));
end


% in order to avoid error when no write access for file use try.
Success = true;
try
    save(fullfile(BTPath, [answer{1}, '.mat']), 'beamtime');
catch
    Success = false;
end

if Success
    msgbox(sprintf('Successfully saved beamtime ''%s''.\nUse the Beamtime/Beamline Setup module to adjust further parameters.', answer{1}), 'modal');
else
    msgbox('Could not save beamtime.', 'modal');
end

% update beamtime listbox in LoadFileModule
LoadFileModule('PopulateBeamtimePopupBox', gd.LoadFileModule);
LoadFileModule('UpdateSelectedBeamtime', gd.LoadFileModule);

% --- Executes on button press in Btn_QViewRegrid.
function Btn_QViewRegrid_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_QViewRegrid (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% disable button
set(hObject, 'Enable', 'off');
drawnow;

% get the best experimental setup
BL = handles.BL_Best;

% get alphai
alphai = str2double(get(handles.TB_alphai, 'String'));
chi = 0;

% calculate the reciprocal data
q_xyFull = [];
q_zFull = [];
IntFull = [];
for iBL = 1:numel(BL)
    [q_xy, q_z] = CalculateQ(BL(iBL), alphai, chi);
    Int = handles.ImageData(:, :, iBL);
    L = Int > 0;
    q_xy = q_xy(L);
    q_z = q_z(L);
    Int = Int(L);
    q_xyFull = [q_xyFull; q_xy(:)];
    q_zFull = [q_zFull; q_z(:)];
    IntFull = [IntFull; Int(:)];
end
[Int, qxy, qz] = regrid(q_xyFull, q_zFull, IntFull, ...
    ceil([size(handles.ImageData, 1) size(handles.ImageData, 2)]./2));

% calculate optimal color limits for the sqrt of the data
ColorLims = getColorBarLimits(sqrt(Int(:)));

% plot the sqrt of the data
f = figure;
ax = axes('Parent', f);
imagesc(qxy, qz, sqrt(Int), 'Parent', ax)
caxis(ax, ColorLims);
set(ax, 'YDir', 'normal')
AddRingsToAxis(ax, ...
    str2num(get(handles.TB_dSpacing, 'String')), ...
    str2double(get(handles.TB_ToleranceQ, 'String')));
xlabel(ax, ['q_{xy} (' char(197) '^{-1})'])
ylabel(ax, ['q_z (' char(197) '^{-1})'])
axis(ax, 'image')

% enable button
set(hObject, 'Enable', 'on');
drawnow;

% --- Executes on button press in Btn_QViewInterpolate.
function Btn_QViewInterpolate_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_QViewInterpolate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% disable button
set(hObject, 'Enable', 'off');
drawnow;

% get the best experimental setup
BL = handles.BL_Best;

% get alphai
alphai = str2double(get(handles.TB_alphai, 'String'));
chi = 0;

% calculate the reciprocal space data
q_xyFull = [];
q_zFull = [];
IntFull = [];
for iBL = 1:numel(BL)
    [q_xy, q_z] = CalculateQ(BL(iBL), alphai, chi);
    Int = handles.ImageData(:, :, iBL);
    L = Int > 0;
    q_xy = q_xy(L);
    q_z = q_z(L);
    Int = Int(L);
    q_xyFull = [q_xyFull; q_xy(:)];
    q_zFull = [q_zFull; q_z(:)];
    IntFull = [IntFull; Int(:)];
end

% perform the interpolation
F = TriScatteredInterp(q_xyFull, q_zFull, IntFull); %#ok. Using TriScatteredInterp instead of scatteredInterpolant for backwards compatibility

% create new data point grid
[x, z] = meshgrid(linspace(min(q_xyFull), max(q_xyFull), BL(1).detlenx), ...
    linspace(min(q_zFull), max(q_zFull), BL(1).detlenz));

IntNew = F(x, z);

IntNew(IntNew < 0) = NaN;

% calculate optimal color limits for the sqrt of the data
ColorLims = getColorBarLimits(sqrt(IntNew(:)));

f = figure;
ax = axes('Parent', f);
imagesc(x(1, :), z(:, 1), sqrt(IntNew), 'Parent', ax);
caxis(ax, ColorLims);
set(ax, 'YDir', 'normal');
AddRingsToAxis(ax, ...
    str2num(get(handles.TB_dSpacing, 'String')), ...
    str2double(get(handles.TB_ToleranceQ, 'String')));
xlabel(ax, ['q_{xy} (' char(197) '^{-1})'])
ylabel(ax, ['q_z (' char(197) '^{-1})'])
axis(ax, 'image')

% enable button
set(hObject, 'Enable', 'on');
drawnow;


% --- Executes on button press in Btn_QViewPcolor.
function Btn_QViewPcolor_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_QViewPcolor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if numel(handles.BL_Best) > 1
    msgbox('pcolor view is only possible for one image.', 'modal');
    return;
end

% disable button
set(hObject, 'Enable', 'off');
drawnow;

% get the best experimental setup
BL = handles.BL_Best;

% get alphai
alphai = str2double(get(handles.TB_alphai, 'String'));

Data = handles.ImageData;
Data(Data < 0) = NaN;

% calculate the reciprocal space data
[q_xy, q_z] = CalculateQ(BL, alphai, 0);
q_xy = reshape(q_xy, size(Data));
q_z = reshape(q_z, size(Data));

% calculate optimal color limits for the sqrt of the data
ColorLims = getColorBarLimits(sqrt(Data(:)));

f = figure;
ax = axes('Parent', f);
pcolor(q_xy, q_z, sqrt(Data), 'Parent', ax);
shading(ax, 'flat');
AddRingsToAxis(ax, ...
    str2num(get(handles.TB_dSpacing, 'String')), ...
    str2double(get(handles.TB_ToleranceQ, 'String')));
xlabel(ax, ['q_{xy} (' char(197) '^{-1})'])
ylabel(ax, ['q_z (' char(197) '^{-1})'])
axis(ax, 'image')

% enable button
set(hObject, 'Enable', 'on');
drawnow;

function InitializeStartParametersPanel(hObject)
% get handles structure
handles = guidata(hObject);

% get the state of the handles.RB_DeltaGammaGoniometer
if get(handles.RB_DeltaGammaGoniometer, 'Value')
    set(handles.TB_outeroffset, 'Visible', 'on')
    set(handles.L_outeroffset, 'Visible', 'on')
    set(handles.CB_outeroffset, 'Visible', 'on')
else
    set(handles.TB_outeroffset, 'Visible', 'off')
    set(handles.L_outeroffset, 'Visible', 'off')
    set(handles.CB_outeroffset, 'Visible', 'off')
end

function InitializeAngleTable(hObject)
% get handles structure
handles = guidata(hObject);

% get the table
UIT = handles.UIT_GoniometerAngles;

% adapt the visibility
if get(handles.RB_DeltaGammaGoniometer, 'Value')
    set(UIT, 'Visible', 'on');
else
    set(UIT, 'Visible', 'off');
end

% get the table's data
D = get(UIT, 'Data');

% check if the first element is empty. This is an indication that it was
% not used before
if isempty(D{1}) || size(handles.ImageData, 3) ~= size(D, 1)
    % initialize the data
    N = size(handles.ImageData, 3);
    D = cell(N, 3);
    for iD = 1:N
        D{iD, 2} = 0;
        D{iD, 3} = 0;
    end
end

% always update the file names
for iD = 1:size(handles.ImageData, 3)
    [~, Name, Ext] = fileparts(handles.SelectedImagePath{iD});
    D{iD, 1} = [Name, Ext];
end
set(UIT, 'Data', D);

function InitializeBackgroundTable(hObject)
% get handles structure
handles = guidata(hObject);

% get the table displaying the background, disksize and visibility
UIT = handles.UIT_Background;

% get the table's data
D = get(UIT, 'Data');

% check if the first element is empty. This is an indication that it was
% not used before
if isempty(D{1}) || size(handles.ImageData, 3) ~= size(D, 1)
    % initialize the data
    N = size(handles.ImageData, 3);
    D = [num2cell(500*ones(N, 1)) num2cell(3*ones(N, 1)) num2cell(true(N, 1))];
    set(UIT, 'Data', D);
end

% if the axes system was not yet used, perform the button click to update
% it
Btn_ApplyBackgroundThreshold_Callback(handles.Btn_ApplyBackgroundThreshold, [], handles);

function PerformCalculation(hObject)
% get handles structure
handles = guidata(hObject);

% get the expected d spacing values
d = str2num(get(handles.TB_dSpacing, 'String'));
% read out the tolerance in q
QTol = str2double(get(handles.TB_ToleranceQ, 'String'));
% get alphai
alphai = str2double(get(handles.TB_alphai, 'String'));

% create an experimental setup from the values in the GUI
BL = CreateBLFromGUIValues(hObject);

% calculate calibration sum and the regridded reciprocal data for display
[CalibSum, qxy2, qz2, Ind] = CalculateCalibrationSum(BL, ...
    alphai, d, QTol, handles.ImageData_BG);

% get the maximum q value which is in the data
maxq = max(sqrt(qxy2.^2 + qz2.^2));

% get the q values from the expected d spacings
qCalibrant = 2*pi./d;

% only consider qCalibrant below the maximum q value
L = qCalibrant <= maxq;

% calculate the difference between the expected q values
DqCalibrant = diff(qCalibrant(L));

% to avoid overlapping of the peak positions and the Q Tolerance ranges,
% the Q Tolerance has to be smaller than the minimum from DqCalibrant
% divided by two
QTolLim = min(DqCalibrant)./2;

% check if the entered Q Tolerance is larger than the limit
if QTol > QTolLim
    msgbox('The value of ''Q Tolerance'' might be too high, resulting in overlapping regions.', 'Warning', 'warn', 'modal');
end

% plot
cla(handles.Ax_CalibSum);
Col = [1 0 0; 0 1 0; 0 0 1];
Col = repmat(Col, numel(BL), 1);
for iB = 1:numel(BL)
    scatter(handles.Ax_CalibSum, qxy2(Ind == iB), qz2(Ind == iB), 1, Col(iB, :), 'filled', 'Visible', 'on');
    hold(handles.Ax_CalibSum, 'on')
end
% add rings to the image
AddRingsToAxis(handles.Ax_CalibSum, d, QTol);
% remove ticks
set(handles.Ax_CalibSum, 'XTick', [], 'YTick', []);
% set axis y direction normal (imagesc reverses it)
set(handles.Ax_CalibSum, 'YDir', 'normal');
% set the axis format
axis(handles.Ax_CalibSum, 'image');
% set the axes title
title(handles.Ax_CalibSum, sprintf('Energy: %g', CalibSum));
hold(handles.Ax_CalibSum, 'off')
% set axes labels
xlabel(handles.Ax_CalibSum, 'q_{xy}');
ylabel(handles.Ax_CalibSum, 'q_z');
% show the legend
legend(handles.Ax_CalibSum, 'Show')


function AddRingsToAxis(Ax, d, QTol)
% create an angle vector for the circle
phi = linspace(0, 2*pi, 300);
% add a hggroup for the rings of the expected position and the rings of the
% expected positions + the q tolerance
HG_Expected = hggroup('Parent', Ax, 'DisplayName', 'Expected');
HG_QTol = hggroup('Parent', Ax, 'DisplayName', 'Q Tolerance');
% make the hggroups show up in the legend
set(get(get(HG_Expected, 'Annotation'), 'LegendInformation'),...
    'IconDisplayStyle', 'on'); 
set(get(get(HG_QTol, 'Annotation'), 'LegendInformation'),...
    'IconDisplayStyle', 'on'); 
for iR = 1:numel(d)
    x = 2*pi/d(iR).*cos(phi);
    y = 2*pi/d(iR).*sin(phi);
    plot(HG_Expected, x, y, 'k', ...
        'XLimInclude', 'off', 'YLimInclude', 'off');
    x = ((2*pi/d(iR))-QTol).*cos(phi);
    y = ((2*pi/d(iR))-QTol).*sin(phi);
    plot(HG_QTol, x, y, 'k--', ...
        'XLimInclude', 'off', 'YLimInclude', 'off');
    x = ((2*pi/d(iR))+QTol).*cos(phi);
    y = ((2*pi/d(iR))+QTol).*sin(phi);
    plot(HG_QTol, x, y, 'k--', ...
        'XLimInclude', 'off', 'YLimInclude', 'off');
end
hold(Ax, 'on')
% plot the center pixel (0, 0)
plot(Ax, 0, 0, 'r*', 'MarkerSize', 12, 'DisplayName', 'Origin');
hold(Ax, 'off')

function PlotRegridded(Ax, qxy, qz, Int, d, CalibSum, QTol)
% plot the image in Ax
imagesc(qxy, qz, Int, 'Parent', Ax);
% add rings to the image
AddRingsToAxis(Ax, d, QTol);
% remove ticks
set(Ax, 'XTick', [], 'YTick', []);
% set axis y direction normal (imagesc reverses it)
set(Ax, 'YDir', 'normal');
% set the axis format
axis(Ax, 'image');
% set the axes title
title(Ax, sprintf('Energy: %g', CalibSum));
hold(Ax, 'off')
% set axes labels
xlabel(Ax, 'q_{xy}');
ylabel(Ax, 'q_z');
% show the legend
legend(Ax, 'Show')


function BL = CreateBLFromGUIValues(hObject)
% get handles structure
handles = guidata(hObject);

% get index of selected detector
DetInd = get(handles.LB_Detectors, 'Value');
% get selected detector
Detector = handles.Detectors(DetInd);

% get all the parameters the user entered
lambda = str2double(get(handles.TB_Wavelength, 'String'));
cpx = str2double(get(handles.TB_cpx, 'String'));
cpz = str2double(get(handles.TB_cpz, 'String'));
sdd = str2double(get(handles.TB_sdd, 'String'));
rx = str2double(get(handles.TB_rx, 'String'));
ry = str2double(get(handles.TB_ry, 'String'));
rz = str2double(get(handles.TB_rz, 'String'));

% initialize an array of experimental setups
BL = repmat(...
    ExperimentalSetUp('Initial Setup', sdd, Detector.psx, Detector.psz, ...
    Detector.detlenx, Detector.detlenz, cpx, cpz, lambda, rx, ry, rz, ...
    0), size(handles.ImageData, 3), 1);

% delta, gamma and outer offset are only relevant if RB_DeltaGammaGoniometer is
% checked
if get(handles.RB_DeltaGammaGoniometer, 'Value')
    D = get(handles.UIT_GoniometerAngles, 'Data');
    for iB = 1:numel(BL)
        BL(iB).Geometry = 'GammaDelta';
        BL(iB).delta = D{iB, 2};
        BL(iB).gamma = D{iB, 3};
        BL(iB).outer_offset = str2double(get(handles.TB_outeroffset, 'String'));
    end
else
    for iB = 1:numel(BL)
        BL(iB).Geometry = 'none';
        BL(iB).delta = 0;
        BL(iB).gamma = 0;
        BL(iB).outer_offset = 0;
    end
end

function [CalibSum, q_xy, q_z, Ind] = CalculateCalibrationSum(...
BL, alphai, ExpectedD, TolQ, ImageData_BG)
% calculate the reciprocal space data for the beamline
% to reduce the number of data points, only pixels where the logical image
% data in handles.ImageData_BG is 1 are calculated
q_xy = [];
q_z = [];
q = [];
Ind = [];

CalibSum = 0;
for iF = 1:size(ImageData_BG, 1)
    [q_xyCurr, q_zCurr, ~, ~, q_Curr] = CalculateQ(BL(iF), alphai, 0, ...
        ImageData_BG{iF, 2}, ImageData_BG{iF, 3});
    q_xy = [q_xy; q_xyCurr];
    q_z = [q_z; q_zCurr];
    q = [q; q_Curr];
    Ind = [Ind; iF.*ones(size(q_Curr))];
    ExpectedQ = 2*pi./ExpectedD;
    ImgD = ImageData_BG{iF, 4};
    linearInd = sub2ind(size(ImageData_BG{iF, 4}), ImageData_BG{iF, 3}, ImageData_BG{iF, 2});
    ImgD = ImgD(linearInd);
    % calculate the calibration sum as a measure how good the expected data
    % fits the experimental data
    for r = ExpectedQ
        L = q_Curr > r-TolQ & q_Curr < r+TolQ;
        Delta = r-q_Curr(L);
        Delta = Delta.*ImgD(L);
        AbsVal = abs(Delta);
        S = sum(AbsVal);
        CalibSum = CalibSum + sum(S);
    end
end

function TB_alphai_Callback(hObject, eventdata, handles)
% hObject    handle to TB_alphai (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_alphai as text
%        str2double(get(hObject,'String')) returns contents of TB_alphai as a double


% --- Executes during object creation, after setting all properties.
function TB_alphai_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_alphai (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_cpx_Callback(hObject, eventdata, handles)
% hObject    handle to TB_cpx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_cpx as text
%        str2double(get(hObject,'String')) returns contents of TB_cpx as a double


% --- Executes during object creation, after setting all properties.
function TB_cpx_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_cpx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in CB_cpx.
function CB_cpx_Callback(hObject, eventdata, handles)
% hObject    handle to CB_cpx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of CB_cpx



function TB_cpz_Callback(hObject, eventdata, handles)
% hObject    handle to TB_cpz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_cpz as text
%        str2double(get(hObject,'String')) returns contents of TB_cpz as a double


% --- Executes during object creation, after setting all properties.
function TB_cpz_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_cpz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in CB_cpz.
function CB_cpz_Callback(hObject, eventdata, handles)
% hObject    handle to CB_cpz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of CB_cpz



function TB_sdd_Callback(hObject, eventdata, handles)
% hObject    handle to TB_sdd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_sdd as text
%        str2double(get(hObject,'String')) returns contents of TB_sdd as a double


% --- Executes during object creation, after setting all properties.
function TB_sdd_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_sdd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in CB_sdd.
function CB_sdd_Callback(hObject, eventdata, handles)
% hObject    handle to CB_sdd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of CB_sdd



function TB_rx_Callback(hObject, eventdata, handles)
% hObject    handle to TB_rx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_rx as text
%        str2double(get(hObject,'String')) returns contents of TB_rx as a double


% --- Executes during object creation, after setting all properties.
function TB_rx_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_rx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in CB_rx.
function CB_rx_Callback(hObject, eventdata, handles)
% hObject    handle to CB_rx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of CB_rx



function TB_ry_Callback(hObject, eventdata, handles)
% hObject    handle to TB_ry (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_ry as text
%        str2double(get(hObject,'String')) returns contents of TB_ry as a double


% --- Executes during object creation, after setting all properties.
function TB_ry_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_ry (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in CB_ry.
function CB_ry_Callback(hObject, eventdata, handles)
% hObject    handle to CB_ry (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of CB_ry



function TB_rz_Callback(hObject, eventdata, handles)
% hObject    handle to TB_rz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_rz as text
%        str2double(get(hObject,'String')) returns contents of TB_rz as a double


% --- Executes during object creation, after setting all properties.
function TB_rz_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_rz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in CB_rz.
function CB_rz_Callback(hObject, eventdata, handles)
% hObject    handle to CB_rz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of CB_rz



function TB_outeroffset_Callback(hObject, eventdata, handles)
% hObject    handle to TB_outeroffset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_outeroffset as text
%        str2double(get(hObject,'String')) returns contents of TB_outeroffset as a double


% --- Executes during object creation, after setting all properties.
function TB_outeroffset_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_outeroffset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in CB_outeroffset.
function CB_outeroffset_Callback(hObject, eventdata, handles)
% hObject    handle to CB_outeroffset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of CB_outeroffset



function TB_DiskSize_Callback(hObject, eventdata, handles)
% hObject    handle to TB_DiskSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_DiskSize as text
%        str2double(get(hObject,'String')) returns contents of TB_DiskSize as a double


% --- Executes during object creation, after setting all properties.
function TB_DiskSize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_DiskSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Btn_StartCalibration.
function Btn_StartCalibration_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_StartCalibration (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% get the ExperimentalSetUp with the initial data
BL_Initial = CreateBLFromGUIValues(hObject);

% enable/disable certain GUI elements
set(handles.Btn_StopCalibration, 'Enable', 'on');
set([handles.Btn_StartCalibration, handles.Btn_SetAsStart, ...
    handles.Btn_Next, handles.Btn_Back], 'Enable', 'off');
handles.StopCalibration = false;

% get the expected d spacing values
d = str2num(get(handles.TB_dSpacing, 'String'));
% read out the tolerance in q
QTol = str2double(get(handles.TB_ToleranceQ, 'String'));
% get alphai
alphai = str2double(get(handles.TB_alphai, 'String'));

% set the parameters for the simulated annealing algorithm
k_max = str2double(get(handles.TB_NumTempSteps, 'String')); % number of temperature steps
L = str2double(get(handles.TB_TrialsPerTempStep, 'String')); % number of trials per temperature step
T_Start = str2double(get(handles.TB_TStart, 'String')); % start temperature

% create the temperature vector
T = linspace(T_Start, T_Start*0.05, k_max);
mu = linspace(1, 0.05, k_max);

% the currently best exp. setup is the initial one
BL_Best = BL_Initial;

% variable to store the best calibration sum
CalibSum_Best = CalculateCalibrationSum(BL_Best, alphai, d, QTol, handles.ImageData_BG);

% initialize vectors
CalibSum = zeros(L*k_max, 1);
cpx_History = CalibSum;
cpz_History = CalibSum;
sdd_History = CalibSum;
rx_History = CalibSum;
ry_History = CalibSum;
rz_History = CalibSum;
outerOffset_History = CalibSum;


CalibSum(1) = CalibSum_Best;
cpx_History(1) = BL_Best(1).cpx;
cpz_History(1) = BL_Best(1).cpz;
sdd_History(1) = BL_Best(1).sdd;
rx_History(1) = BL_Best(1).rx;
ry_History(1) = BL_Best(1).ry;
rz_History(1) = BL_Best(1).rz;
outerOffset_History(1) = BL_Best(1).outer_offset;
% there are maximum seven parameters to vary
PossThrows = 1:7;
% remove these parameters which should not get varied according to the
% users choice with the checkboxes
% this approach makes sure that if only one element is checked, this
% element gets varied in every run. Not only in 1/7 of the runs.
% outer offset should only be optimized if both, the outer offset box and
% the goniometer box are checked
PossThrows = PossThrows(logical([get(handles.CB_cpx, 'Value'), ...
    get(handles.CB_cpz, 'Value'), get(handles.CB_sdd, 'Value'), ...
    get(handles.CB_rx, 'Value'), get(handles.CB_ry, 'Value'), ...
    get(handles.CB_rz, 'Value'), ...
    get(handles.RB_DeltaGammaGoniometer, 'Value') & get(handles.CB_outeroffset, 'Value')]));

Rands = zeros(L*k_max, 7);

% Initialize trial exp. setup
BL_Trial = BL_Best;

% initialize plot of calibration sum
plot(handles.Ax_CalibSum, [1, L*k_max], [CalibSum(1) CalibSum(1)], ...
    'Color', 'r', 'DisplayName', 'Start Energy');
CS_LH = line('XData', [], 'YData', [], 'Parent', handles.Ax_CalibSum, ...
    'Color', 'b', 'DisplayName', 'Current Energy');
xlabel(handles.Ax_CalibSum, 'Trial Number');
ylabel(handles.Ax_CalibSum, 'Energy');

counter = 2;

improved = 0;
rejected = 0;
accepted = 0;

FinalResult = text(L*k_max*0.98, CalibSum(1)*0.98, '', ...
    'VerticalAlignment', 'top', 'HorizontalAlignment', 'right', ...
    'FontSize', 10);

% do the actual optimization
for k = 1:k_max
    set(FinalResult, 'String', ...
        sprintf('Calibration in progress (%.0f%%)\n', k/k_max*100));
    for kk = 1:L
        % throw the dice
        dice = randi(numel(PossThrows), 1);
        
        Rands((k-1)*L+kk, PossThrows(dice)) = normrnd(0, mu(k), 1);
        
        % new trial parameter set with gaussian, i.e. varying one parameter
        % of the best exp. setup
        for iBL = 1:numel(BL_Best)
            switch PossThrows(dice) % use random number as index for PossThrows
                case 1
                    BL_Trial(iBL).cpx = BL_Best(iBL).cpx + Rands((k-1)*L+kk, PossThrows(dice));
                case 2
                    BL_Trial(iBL).cpz = BL_Best(iBL).cpz + Rands((k-1)*L+kk, PossThrows(dice));
                case 3
                    BL_Trial(iBL).sdd = BL_Best(iBL).sdd + Rands((k-1)*L+kk, PossThrows(dice));
                case 4
                    BL_Trial(iBL).rx = BL_Best(iBL).rx + Rands((k-1)*L+kk, PossThrows(dice));
                case 5
                    BL_Trial(iBL).ry = BL_Best(iBL).ry + Rands((k-1)*L+kk, PossThrows(dice));
                case 6
                    BL_Trial(iBL).rz = BL_Best(iBL).rz + Rands((k-1)*L+kk, PossThrows(dice));
                case 7
                    BL_Trial(iBL).outer_offset = BL_Best(iBL).outer_offset + Rands((k-1)*L+kk, PossThrows(dice))*0.1;
            end
        end
        
        % calculate the calibration sum of the current trial exp. setup
        CS = CalculateCalibrationSum(BL_Trial, alphai, d, QTol, handles.ImageData_BG);
                
        dE = CS - CalibSum_Best;
        
        if exp(-dE/T(k)) > 1
            improved = improved + 1;
        end
        
        CalibSum(counter) = CS;
        cpx_History(counter) = BL_Trial(1).cpx;
        cpz_History(counter) = BL_Trial(1).cpz;
        sdd_History(counter) = BL_Trial(1).sdd;
        rx_History(counter) = BL_Trial(1).rx;
        ry_History(counter) = BL_Trial(1).ry;
        rz_History(counter) = BL_Trial(1).rz;
        outerOffset_History(counter) = BL_Trial(1).outer_offset;

        if mod(counter, 50) == 0
            % update the calibration sum plot
            set(CS_LH, 'XData', 1:counter, 'YData', CalibSum(1:counter));
            drawnow;
        end
        
        counter = counter + 1;
        
        % always accept a set with higher calib_sum
        if rand() < exp(-dE/(T(k)))
            BL_Best = BL_Trial;
            CalibSum_Best = CS;
            accepted = accepted + 1;
        else % new settings get rejected
            BL_Trial = BL_Best;
            rejected = rejected + 1;
        end
        
        handles = guidata(hObject);
        if handles.StopCalibration
            break;
        end
    end
end

handles.StopCalibration = false;

% find the experimental setup with the maximum energy
[MinEnergy, Ind] = min(CalibSum(1:counter-1));
for iBL = 1:numel(BL_Best)
    BL_Best(iBL).cpx = cpx_History(Ind);
    BL_Best(iBL).cpz = cpz_History(Ind);
    BL_Best(iBL).sdd = sdd_History(Ind);
    BL_Best(iBL).outer_offset = outerOffset_History(Ind);
    BL_Best(iBL).rx = rx_History(Ind);
    BL_Best(iBL).ry = ry_History(Ind);
    BL_Best(iBL).rz = rz_History(Ind);
end

set(CS_LH, 'XData', 1:counter-1, 'YData', CalibSum(1:counter-1));
hold(handles.Ax_CalibSum, 'on')
plot(handles.Ax_CalibSum, Ind, MinEnergy, 'Marker', 'o', ...
    'MarkerFaceColor', 'r', 'MarkerEdgeColor', 'r');
hold(handles.Ax_CalibSum, 'off')

% store best experimental setup
handles.BL_Best = BL_Best;

% store the history data
handles.CalibSum_History = CalibSum(1:counter-1);
handles.cpx_History = cpx_History(1:counter-1);
handles.cpz_History = cpz_History(1:counter-1);
handles.sdd_History = sdd_History(1:counter-1);
handles.rx_History = rx_History(1:counter-1);
handles.ry_History = ry_History(1:counter-1);
handles.rz_History = rz_History(1:counter-1);
handles.outerOffset_History = outerOffset_History(1:counter-1);

% calculate calibration sum and the regridded reciprocal data for display
[CalibSum, qxy2, qz2, Ind] = CalculateCalibrationSum(BL_Best, alphai, d, ...
    QTol, handles.ImageData_BG);

cla(handles.Ax_CalibSum);
Col = [1 0 0; 0 1 0; 0 0 1];
Col = repmat(Col, numel(BL_Best), 1);
for iB = 1:numel(BL_Best)
    scatter(handles.Ax_CalibSum, qxy2(Ind == iB), qz2(Ind == iB), 1, Col(iB, :), 'filled', 'Visible', 'on');
    hold(handles.Ax_CalibSum, 'on')
end
% add rings to the image
AddRingsToAxis(handles.Ax_CalibSum, d, QTol);
% remove ticks
set(handles.Ax_CalibSum, 'XTick', [], 'YTick', []);
% set axis y direction normal (imagesc reverses it)
set(handles.Ax_CalibSum, 'YDir', 'normal');
% set the axis format
axis(handles.Ax_CalibSum, 'image');
% set the axes title
title(handles.Ax_CalibSum, sprintf('Energy: %g', CalibSum));
hold(handles.Ax_CalibSum, 'off')
% set axes labels
xlabel(handles.Ax_CalibSum, 'q_{xy}');
ylabel(handles.Ax_CalibSum, 'q_z');
% show the legend
legend(handles.Ax_CalibSum, 'Show')

% plot the regridded data
% PlotRegridded(handles.Ax_CalibSum, qxy, qz, Int, d, CalibSum, QTol);

% create text field showing calibration result
Str = sprintf('Final optimization result:\nsdd: %g, outer offset: %g\ncpx: %g, cpz: %g\nrx: %g, ry: %g, rz: %g', ...
    BL_Best(1).sdd, BL_Best(1).outer_offset, BL_Best(1).cpx, BL_Best(1).cpz, ...
    BL_Best(1).rx, BL_Best(1).ry, BL_Best(1).rz);
text(min(qxy2(:))*0.98, max(qz2(:))*0.98, Str, 'Parent', handles.Ax_CalibSum, ...
    'VerticalAlignment', 'top', 'FontSize', 10, ...
    'BackgroundColor', 'w');

% re-enable gui controls
set(handles.Btn_StopCalibration, 'Enable', 'off');
set([handles.Btn_StartCalibration, handles.Btn_SetAsStart, ...
    handles.Btn_Next, handles.Btn_Back], 'Enable', 'on');

handles.SwitchAllowed(handles.CurrentIndex) = true;

% update handles structure
guidata(hObject, handles);


% --- Executes on button press in Btn_StopCalibration.
function Btn_StopCalibration_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_StopCalibration (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.Btn_StopCalibration, 'Enable', 'off');
handles.StopCalibration = true;

guidata(hObject, handles);

% --- Executes on button press in Btn_SetAsStart.
function Btn_SetAsStart_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_SetAsStart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~isfield(handles, 'BL_Best')
    return;
end

if get(handles.CB_cpx, 'Value')
    set(handles.TB_cpx, 'String', num2str(handles.BL_Best(1).cpx));
end
if get(handles.CB_cpz, 'Value')
    set(handles.TB_cpz, 'String', num2str(handles.BL_Best(1).cpz));
end
if get(handles.CB_sdd, 'Value')
    set(handles.TB_sdd, 'String', num2str(handles.BL_Best(1).sdd));
end
if get(handles.CB_rx, 'Value')
    set(handles.TB_rx, 'String', num2str(handles.BL_Best(1).rx));
end
if get(handles.CB_ry, 'Value')
    set(handles.TB_ry, 'String', num2str(handles.BL_Best(1).ry));
end
if get(handles.CB_rz, 'Value')
    set(handles.TB_rz, 'String', num2str(handles.BL_Best(1).rz));
end
if get(handles.CB_outeroffset, 'Value')
    set(handles.TB_outeroffset, 'String', num2str(handles.BL_Best(1).outer_offset));
end


% --- Executes on button press in Btn_ParameterHistory.
function Btn_ParameterHistory_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_ParameterHistory (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[MinEnergy, Ind] = min(handles.CalibSum_History);


figure
ax = subplot(3, 3, 1);
plot(handles.CalibSum_History);
hold(ax, 'on')
plot(Ind, MinEnergy, 'Marker', 'o', 'MarkerFaceColor', 'r', ...
    'MarkerEdgeColor', 'r');
hold(ax, 'off')
xlabel('Trial #')
ylabel('Energy')

ax = subplot(3, 3, 2);
plot(handles.cpx_History, handles.CalibSum_History, '.')
hold(ax, 'on')
plot(handles.cpx_History(Ind), MinEnergy, 'Marker', 'o', 'MarkerFaceColor', 'r', ...
    'MarkerEdgeColor', 'r');
hold(ax, 'off')
xlabel('cpx')
ylabel('Energy')

ax = subplot(3, 3, 3);
plot(handles.cpz_History, handles.CalibSum_History, '.')
hold(ax, 'on')
plot(handles.cpz_History(Ind), MinEnergy, 'Marker', 'o', 'MarkerFaceColor', 'r', ...
    'MarkerEdgeColor', 'r');
hold(ax, 'off')
xlabel('cpz')
ylabel('Energy')

ax = subplot(3, 3, 4);
plot(handles.sdd_History, handles.CalibSum_History, '.')
hold(ax, 'on')
plot(handles.sdd_History(Ind), MinEnergy, 'Marker', 'o', 'MarkerFaceColor', 'r', ...
    'MarkerEdgeColor', 'r');
hold(ax, 'off')
xlabel('sdd')
ylabel('Energy')

ax = subplot(3, 3, 5);
plot(handles.rx_History, handles.CalibSum_History, '.')
hold(ax, 'on')
plot(handles.rx_History(Ind), MinEnergy, 'Marker', 'o', 'MarkerFaceColor', 'r', ...
    'MarkerEdgeColor', 'r');
hold(ax, 'off')
xlabel('rx')
ylabel('Energy')

ax = subplot(3, 3, 6);
plot(handles.ry_History, handles.CalibSum_History, '.')
hold(ax, 'on')
plot(handles.ry_History(Ind), MinEnergy, 'Marker', 'o', 'MarkerFaceColor', 'r', ...
    'MarkerEdgeColor', 'r');
hold(ax, 'off')
xlabel('ry')
ylabel('Energy')

ax = subplot(3, 3, 7);
plot(handles.rz_History, handles.CalibSum_History, '.')
hold(ax, 'on')
plot(handles.rz_History(Ind), MinEnergy, 'Marker', 'o', 'MarkerFaceColor', 'r', ...
    'MarkerEdgeColor', 'r');
hold(ax, 'off')
xlabel('rz')
ylabel('Energy')

% plot the outer offset only if goniometer was used
if get(handles.RB_DeltaGammaGoniometer, 'Value')
    ax = subplot(3, 3, 8);
    plot(handles.outerOffset_History, handles.CalibSum_History, '.')
    hold(ax, 'on')
    plot(handles.outerOffset_History(Ind), MinEnergy, 'Marker', 'o', 'MarkerFaceColor', 'r', ...
        'MarkerEdgeColor', 'r');
    hold(ax, 'off')
    xlabel('Outer Offset')
    ylabel('Energy')
end



function TB_NumTempSteps_Callback(hObject, eventdata, handles)
% hObject    handle to TB_NumTempSteps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_NumTempSteps as text
%        str2double(get(hObject,'String')) returns contents of TB_NumTempSteps as a double


% --- Executes during object creation, after setting all properties.
function TB_NumTempSteps_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_NumTempSteps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_TrialsPerTempStep_Callback(hObject, eventdata, handles)
% hObject    handle to TB_TrialsPerTempStep (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_TrialsPerTempStep as text
%        str2double(get(hObject,'String')) returns contents of TB_TrialsPerTempStep as a double


% --- Executes during object creation, after setting all properties.
function TB_TrialsPerTempStep_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_TrialsPerTempStep (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Btn_AddDetector.
function Btn_AddDetector_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_AddDetector (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
prompt = {'Number of horizontal pixels:', ...
    'Number of vertical pixels:', ...
    'Pixel size horizontally (mm):', ...
    'Pixel size vertically (mm):', ...
    'Name'};
dlg_title = 'Define new detector';
num_lines = 1;
defaultans = {'1475', '1679', '0.172', '0.172', 'Name'};
answer = inputdlg(prompt, dlg_title, num_lines, defaultans);

% user clicked cancel
if isempty(answer); return; end

% at least one parameter is empty
if any(cellfun('isempty', answer))
    msgbox('All values have to be entered.', 'modal');
    return;
end

% try to convert entered values to numbers
Vals = cellfun(@str2num, answer(1:end-1), 'UniformOutput', 0);

% if the conversion failed for at least one element, there will be an empty
% element in Vals
if any(cellfun('isempty', Vals))
    msgbox('Values for pixel numbers and sizes have to be numeric.', 'modal');
    return;
end

% store old detector list
Detectors = handles.Detectors;

% create the detector
Detectors(end+1) = XDetector();
Detectors(end).detlenx = Vals{1};
Detectors(end).detlenz = Vals{2};
Detectors(end).psx = Vals{3};
Detectors(end).psz = Vals{4};
Detectors(end).Name = answer{5};

% sort detectors by name
Names = {Detectors.Name};
[~, ind] = sort(Names);
Detectors = Detectors(ind);

% store detectors
DirPath = fileparts(fileparts(mfilename('fullpath')));
save(fullfile(DirPath, 'DetectorModule', 'Detectors.mat'), 'Detectors');

% store the Detectors in the handles structure
handles.Detectors = Detectors;

% write the detector names into the listbox
set(handles.LB_Detectors, 'String', {handles.Detectors.Name});

% update handles structure
guidata(hObject, handles);


% --- Executes on button press in Btn_RemoveDetector.
function Btn_RemoveDetector_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_RemoveDetector (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% cannot delete last detector
if numel(handles.Detectors) == 1
    msgbox('You cannot delete the last detector.', 'modal')
    return; 
end

% get selected detector
Ind = get(handles.LB_Detectors, 'Value');

% remove selected detector from list
handles.Detectors(Ind) = [];

% store detectors
DirPath = fileparts(fileparts(mfilename('fullpath')));
Detectors = handles.Detectors;
save(fullfile(DirPath, 'DetectorModule', 'Detectors.mat'), 'Detectors');

% check for index of the listbox
while Ind > numel(Detectors)
    Ind = Ind - 1;
end

set(handles.LB_Detectors, 'Value', Ind);

% write the detector names into the listbox
set(handles.LB_Detectors, 'String', {handles.Detectors.Name});

% update handles structure
guidata(hObject, handles);


% --------------------------------------------------------------------
function Cntxt_DetectorProperties_Callback(hObject, eventdata, handles)
% hObject    handle to Cntxt_DetectorProperties (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function CM_DetectorProperties_Callback(hObject, eventdata, handles)
% hObject    handle to CM_DetectorProperties (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% get index of selected detector
Ind = get(handles.LB_Detectors, 'Value');

% get selected detector
D = handles.Detectors(Ind);

% prepare string
Str = sprintf('Name: %s\nNumber of horizontal pixels: %g\nNumber of vertical pixels: %g\nPixel size horizontally (mm): %g\nPixel size vertically (mm): %g\n', ...
    D.Name, D.detlenx, D.detlenz, D.psx, D.psz);

% show information
msgbox(Str, 'modal');


% --- Executes when user attempts to close Fig_CalibrationWizard.
function Fig_CalibrationWizard_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to Fig_CalibrationWizard (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% extract settings
if isfield(handles, 'SelectedImagePath')
    CalibrationWizardSettings.SelectedImagePath = handles.SelectedImagePath{1};
end
CalibrationWizardSettings.UIT_Background = get(handles.UIT_Background, 'Data');
CalibrationWizardSettings.CB_DeltaGammaGoniometer = get(handles.RB_DeltaGammaGoniometer, 'Value');
CalibrationWizardSettings.PM_DeltaGammaOrder = get(handles.PM_DeltaGammaOrder, 'Value');
CalibrationWizardSettings.UIT_GoniometerAngles = get(handles.UIT_GoniometerAngles, 'Data');
CalibrationWizardSettings.LB_Detectors = get(handles.LB_Detectors, 'Value');
CalibrationWizardSettings.TB_Wavelength = get(handles.TB_Wavelength, 'String');
CalibrationWizardSettings.TB_alphai = get(handles.TB_alphai, 'String');
CalibrationWizardSettings.TB_cpx = get(handles.TB_cpx, 'String');
CalibrationWizardSettings.TB_cpz = get(handles.TB_cpz, 'String');
CalibrationWizardSettings.TB_sdd = get(handles.TB_sdd, 'String');
CalibrationWizardSettings.TB_rx = get(handles.TB_rx, 'String');
CalibrationWizardSettings.TB_ry = get(handles.TB_ry, 'String');
CalibrationWizardSettings.TB_rz = get(handles.TB_rz, 'String');
CalibrationWizardSettings.TB_outeroffset = get(handles.TB_outeroffset, 'String');
CalibrationWizardSettings.CB_cpx = get(handles.TB_cpx, 'Value');
CalibrationWizardSettings.CB_cpz = get(handles.TB_cpz, 'Value');
CalibrationWizardSettings.CB_sdd = get(handles.TB_sdd, 'Value');
CalibrationWizardSettings.CB_rx = get(handles.TB_rx, 'Value');
CalibrationWizardSettings.CB_ry = get(handles.TB_ry, 'Value');
CalibrationWizardSettings.CB_rz = get(handles.TB_rz, 'Value');
CalibrationWizardSettings.CB_outeroffset = get(handles.TB_outeroffset, 'Value');
CalibrationWizardSettings.TB_dSpacing = get(handles.TB_dSpacing, 'String');
CalibrationWizardSettings.TB_TStart = get(handles.TB_TStart, 'String');
CalibrationWizardSettings.TB_NumTempSteps = get(handles.TB_NumTempSteps, 'String');
CalibrationWizardSettings.TB_TrialsPerTempStep = get(handles.TB_TrialsPerTempStep, 'String');
CalibrationWizardSettings.TB_ToleranceQ = get(handles.TB_ToleranceQ, 'String');
CalibrationWizardSettings.PM_AddPixels = get(handles.PM_AddPixels, 'Value');

% save settings
% in order to avoid error when no write access for file use try.
try; save(handles.SettingsPath, 'CalibrationWizardSettings'); end

% delete the axes destroyed listener, otherwise the callback function will
% be executed even when the window does not exist anymore
delete(handles.MainAxesDestroyedListener);

% Hint: delete(hObject) closes the figure
delete(hObject);


% --- Executes when selected object is changed in P_Geometry.
function P_Geometry_SelectionChangedFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in P_Geometry 
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(handles.RB_GoniometerNone, 'Value')
    set(handles.L_DeltaGammaOrder, 'Visible', 'off');
    set(handles.PM_DeltaGammaOrder, 'Visible', 'off');
    set(handles.UIT_GoniometerAngles, 'Visible', 'off');
elseif get(handles.RB_DeltaGammaGoniometer, 'Value')
    set(handles.L_DeltaGammaOrder, 'Visible', 'on');
    set(handles.PM_DeltaGammaOrder, 'Visible', 'on');
    set(handles.UIT_GoniometerAngles, 'Visible', 'on');
end


% --- Executes on selection change in PM_AddPixels.
function PM_AddPixels_Callback(hObject, eventdata, handles)
% hObject    handle to PM_AddPixels (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns PM_AddPixels contents as cell array
%        contents{get(hObject,'Value')} returns selected item from PM_AddPixels


% --- Executes during object creation, after setting all properties.
function PM_AddPixels_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PM_AddPixels (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
