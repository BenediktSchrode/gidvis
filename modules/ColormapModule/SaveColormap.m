function SaveColormap(src, ColormapPath)
% This function saves the colormap currently used in the GIDVis main axes.
%
% Usage:
%     SaveColormap(src, ColormapPath)
% 
% Input:
%     src ... button/menu entry/... used to save the colormap
%     ColormapPath ... path to the Colormaps directory
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Name = inputdlg('Please enter a name for the colormap:');
if isempty(Name); return; end

handles = guidata(src);
Ax = handles.MainAxes;
CM = colormap(Ax);

Name = strrep(Name, ' ', '');
if ~isvarname(Name{1})
    msgbox('The name is not valid. It should start with a letter, and be followed by letters or numbers or underscore.', 'modal');
    return;
end

FilePath = fullfile(ColormapPath, [Name{1}, '.m']);
if exist(FilePath, 'file') == 2
    button = questdlg('File already exists. Do you want to overwrite?', ...
        'Replace File?', 'Yes', 'No', 'No');
    if strcmp(button, 'No')
        return;
    end
end
fid = fopen(FilePath, 'w');
fprintf(fid, 'function cm_data = %s(m)\n', Name{1});
fprintf(fid, 'cm = [');
fprintf(fid, '%g\t%g\t%g; ...\n', CM(1:end-1, :)');
fprintf(fid, '%g\t%g\t%g];\n\n', CM(end, :)');
fprintf(fid, 'if nargin < 1\n\tcm_data = cm;\nelse\n\thsv=rgb2hsv(cm);\n\tcm_data=interp1(linspace(0,1,size(cm,1)),hsv,linspace(0,1,m));\n\tcm_data=hsv2rgb(cm_data);\nend');
fclose(fid);

msgbox(sprintf('Successfully saved colormap ''%s''.\n', Name{1}), 'modal');

ToolboxModule('PopulateColormapPM', handles.ToolboxModule, ColormapPath);