classdef Crosshair < handle
    % Represent a crosshair for a figure.
    %
    % This file is part of GIDVis.
    %
    % see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2018 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    properties(Access = private)
        HorizontalAnnotationLine
        VerticalAnnotationLine
        HorizontalAnnotationLine2
        VerticalAnnotationLine2
        HorizontalTextLabel
        VerticalTextLabel
        AxesHandle
        FigureHandle
        OldPointer
    end % properties(Acess = private)
    methods
        function obj = Crosshair(FigureHandle, AxesHandle)
            obj.FigureHandle = FigureHandle;
            obj.AxesHandle = AxesHandle;

            % use annotation line, because then the line can span the whole figure
            % window
            obj.HorizontalAnnotationLine = annotation('line', [0 1], [0.5 0.5], 'Color', 'w', 'Tag', 'CrosshairModuleHorizontal', 'LineWidth', 2);
            obj.VerticalAnnotationLine = annotation('line', [0.5 0.5], [0 1], 'Color', 'w', 'Tag', 'CrosshairModuleVertical', 'LineWidth', 2);
            obj.HorizontalAnnotationLine2 = annotation('line', [0 1], [0.5 0.5], 'Color', 'k', 'Tag', 'CrosshairModuleHorizontal2', 'LineWidth', 1);
            obj.VerticalAnnotationLine2 = annotation('line', [0.5 0.5], [0 1], 'Color', 'k', 'Tag', 'CrosshairModuleVertical2', 'LineWidth', 1);
            obj.HorizontalTextLabel = text(1, 0.5, '0.5', 'BackgroundColor', 'w', 'Tag', 'CrosshairModuleHorizontal3', 'VerticalAlignment', 'bottom', 'HorizontalAlignment', 'right');
            obj.VerticalTextLabel = text(0.5, 0, '0.5', 'BackgroundColor', 'w', 'Tag', 'CrosshairModuleVertical3', 'VerticalAlignment', 'bottom', 'HorizontalAlignment', 'left');
            uistack(obj.HorizontalTextLabel, 'top');
            uistack(obj.VerticalTextLabel, 'top');
            % update the position
            obj.WinButtonMotionFcn([], []);
            % moving the mouse moves the crosshair
            set(obj.FigureHandle, 'WindowButtonMotionFcn', @obj.WinButtonMotionFcn);
            % change the pointer to nothing (NaN)
            obj.OldPointer = get(obj.FigureHandle, 'Pointer');
            set(obj.FigureHandle, 'Pointer', 'custom');
            set(obj.FigureHandle, 'PointerShapeCData', NaN(16, 16));
        end
        
        function delete(obj)
            if ishandle(obj.FigureHandle)
                set(obj.FigureHandle, 'WindowButtonMotionFcn', '');
                set(obj.FigureHandle, 'Pointer', obj.OldPointer);
            end
            if ishandle(obj.HorizontalAnnotationLine)
                delete(obj.HorizontalAnnotationLine);
            end
            if ishandle(obj.VerticalAnnotationLine)
                delete(obj.VerticalAnnotationLine);
            end
            if ishandle(obj.HorizontalAnnotationLine2)
                delete(obj.HorizontalAnnotationLine2);
            end
            if ishandle(obj.VerticalAnnotationLine2)
                delete(obj.VerticalAnnotationLine2);
            end
            if ishandle(obj.HorizontalTextLabel)
                delete(obj.HorizontalTextLabel);
            end
            if ishandle(obj.VerticalTextLabel)
                delete(obj.VerticalTextLabel);
            end
        
        end
    end % methods
    
    methods(Access = private)
        function WinButtonMotionFcn(obj, src, evt)
            % get the current mouse position (in data units)
            CurPoint = get(obj.AxesHandle, 'CurrentPoint');
            % calculate the normalized units from these data units
            [CurX CurY] = obj.NormalizedUnitsFromDataUnits(obj.AxesHandle, CurPoint(1, 1), CurPoint(1, 2));
            XLims = get(obj.AxesHandle, 'XLim');
            YLims = get(obj.AxesHandle, 'YLim');
            TickLength = get(obj.AxesHandle, 'TickLength');
            % set the new positions for the annotation lines
            set(obj.HorizontalAnnotationLine, 'Y', [CurY CurY]);
            set(obj.VerticalAnnotationLine, 'X', [CurX CurX]);
            set(obj.HorizontalAnnotationLine2, 'Y', [CurY CurY]);
            set(obj.VerticalAnnotationLine2, 'X', [CurX CurX]);
            % set the new positions for the text labels
            set(obj.HorizontalTextLabel, 'Position', [XLims(2) CurPoint(1, 2)], 'String', sprintf('%1.4f', CurPoint(1, 2)));
            set(obj.VerticalTextLabel, 'Position', [CurPoint(1, 1) YLims(1)], 'String', sprintf(' %1.4f', CurPoint(1, 1)));
        end
    end % methods(Access = private)
    
    methods (Static, Access = private)
        function [x, y] = NormalizedUnitsFromDataUnits(AxesHandle, xToTransform, yToTransform)
            % Use this function to calculate normalized units from data units

            % store the old axes units
            OldAxUnits = get(AxesHandle, 'Units');
            % set the axes units to normlaized
            set(AxesHandle, 'Units', 'normalized');
            % get the position of the axes
            AxPos = get(AxesHandle, 'Position');
            % get the x and y lims of the axes (is in data units)
            AxLims = axis(AxesHandle);
            % restore the unit of the axes
            set(AxesHandle, 'Units', OldAxUnits);

            % calculate the size of the axes
            AxWidth = diff(AxLims(1:2));
            AxHeight = diff(AxLims(3:4));

            % calculate the values, taking reversed axes into account if neccessary
            if strcmpi(get(AxesHandle, 'XDir'), 'reverse')
                x = 1-((xToTransform-AxLims(1))*AxPos(3)/AxWidth + (1-AxPos(1)-AxPos(3)));
            else
                x = (xToTransform-AxLims(1))*AxPos(3)/AxWidth + AxPos(1);
            end
            if strcmpi(get(AxesHandle, 'YDir'), 'reverse')
                y = 1-((yToTransform-AxLims(3))*AxPos(4)/AxHeight + (1-AxPos(2)-AxPos(4)));
            else
                y = (yToTransform-AxLims(3))*AxPos(4)/AxHeight + AxPos(2);
            end
        end
    end % methods(Access = private)
end % Crosshair