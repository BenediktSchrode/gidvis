function CrosshairModule(Toolbar, FigureHandle, AxesHandle, SeparatorOnOff)
% adds a toggle button to the toolbar toggling a crosshair on and off. The
% crosshair is created in FigureHandle/AxesHandle.
%
% Usage:
%     CrosshairModule(Toolbar, FigureHandle, AxesHandle, SeparatorOnOff)
%
% Input:
%     Toolbar ... handle of the toolbar where the button for the crosshair
%                 should be placed
%     FigureHandle ... handle of the figure where the crosshair should be
%                      created
%     AxesHandle ... handle of the axes where the crosshair should be
%                    created
%     SeparatorOnOff ... string ('on' or 'off') defining whether a
%                        separator should be drawn left of the toggle tool
%                        or not
%
%
% This file is part of GIDVis.
%
% see also: uitoggletool, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% create an image for the toggle button
CrosshairImg = ones(16, 16, 3).*240./255; % "background" for the image, light gray
CrosshairImg(3:14, 8:9, :) = 0; % add vertical lines to image
CrosshairImg(8:9, 3:14, :) = 0; % add horizontal lines to image

% add the toggle button to the Toolbar. In the UserData of the toggle
% button the last used values are stored in a cell: {LineWidth, 
% ColorTriplet, LabelVisibility, LineStyle}.
ToggleButton = uitoggletool(Toolbar, 'CData', CrosshairImg, ...
    'TooltipString', sprintf('Crosshair'), ...
    'OnCallback', {@CrosshairToggledToOn, FigureHandle, AxesHandle}, ...
    'OffCallback', @CrosshairToggledToOff, ...
    'Separator', SeparatorOnOff);

% add a listener to the togglebutton just created. When it gets destroyed 
% (e.g. because the figure containing the button gets closed), the callback 
% will be executed (and deletes the crosshair).
addlistener(ToggleButton, 'ObjectBeingDestroyed', @ToggleButtonDestroyed_Callback);

function ToggleButtonDestroyed_Callback(src, evt)
CH = get(src, 'UserData');
if ~isempty(CH) && isvalid(CH)
    delete(CH);
end

function CrosshairToggledToOn(src, evt, FigureHandle, AxesHandle)
% switch off all other toggle buttons in the toolbar except for src to
% avoid using multiple tools at once
SwitchOffToolstripButtons(FigureHandle, src);

% find the image handle
imgHandle = findobj(FigureHandle, 'Tag', 'MainImage');
if isempty(imgHandle) % occurs e.g. when no GID image is loaded yet
    set(src, 'State', 'off');
    msgbox('You cannot add a crosshair to the currently displayed image. Select a GID image in the "Load File" window (File - Select Folder) first.', 'Cannot Add Crosshair', 'modal');
    return;
end
CH = Crosshair(FigureHandle, AxesHandle);
set(src, 'UserData', CH);

function CrosshairToggledToOff(src, evt)
CH = get(src, 'UserData');
if ~isempty(CH) &&isvalid(CH)
    delete(CH);
end