function HKL = ApplyReflectionConditions(HKL, RefC)
% Applies the reflection condition given in RefC to the HKLs given by HKL. 
% The output is the HKL matrix with HKL values not fulfilling the 
% reflection condition removed.
%
% Usage:
%     HKL = ApplyReflectionConditions(HKL, RefC)
%
% Input:
%     HKL ... n x 3 matrix of h, k, l indices to which the reflection
%             condition should be applied
%     RefC ... reflection condition (can be a cell or a string)
%
% Output:
%     HKL ... m x 3 matrix of h, k, l indices from the input which fulfil
%             the reflection condition
%     
% Examples:
%     % create a GIDVis_Crystal and a matrix of HKL indices
%     Cr = GIDVis_Crystal;
%     HKL = Cr.CreateHKLCombinations(-5:5);
%     % Apply a reflection condition
%     HKLR1 = ApplyReflectionConditions(HKL, '00L: L = 6*n');
%     % Apply a different, more complex reflection condition
%     HKLR2 = ApplyReflectionConditions(HKL, 'HKL: -H+K+L = 3*n');
%     % Apply several reflection conditions at once
%     HKLR3 = ApplyReflectionConditions(HKL, ...
%         {'00L: L = 6*n' 'HKL: -H+K+L=3*n' 'HHL: L = 3*n'});
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if ~iscell(RefC)
    RefC = {RefC};
end


syms n;

H = HKL(:, 1);
K = HKL(:, 2);
L = HKL(:, 3);

for iCon = 1:numel(RefC)
    ReflCon = RefC{iCon};
    ReflCon = strrep(ReflCon, ' ', '');
    Splitted = regexp(ReflCon, ':', 'split');
    LLeft = ones(size(H));
    % left hand side
    LLeft = LLeft & (H == eval(Splitted{1}(1)));
    LLeft = LLeft & (K == eval(Splitted{1}(2)));
    LLeft = LLeft & (L == eval(Splitted{1}(3)));

    % right hand side
    RHS = double(subs(solve(str2sym(Splitted{2}), n), {'H', 'K', 'L'}, {H, K, L}));
    RightL = RHS-ceil(RHS) == 0;

    LRL = LLeft & ~RightL;
    H(LRL) = [];
    K(LRL) = [];
    L(LRL) = [];
    HKL(LRL, :) = [];
end