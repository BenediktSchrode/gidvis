function Col = ColorCode(FullStr)
% Color code for plotting atoms following mostly the CPK convention.
%
% Usage:
%     ColorCode('Element') ... returns rgb color value for chemical element
%     ColorCode({'Element1', 'Element2', ...}) ... returns rgb color values
%         for chemical elements
%     ColorCode() ... plots elements and their colors
%
% Input:
%     FullStr ... string or cell containing the chemical element(s)
% 
% Output:
%     Col ... n x 3 matrix with the rgb colors (between 0 and 1) for the
%             requested n elements
%
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin == 0; PlotPTE; return; end

warning('ColorCode:UsageDiscouraged', ...
    'The usage of ''ColorCode'' is discouraged. Use ''ChEl'' instead.');

if ~iscell(FullStr); FullStr = {FullStr}; end

% get the color(s)
Col = vertcat(ChEl(FullStr).Color);
end

function PlotPTE
% Plots the periodic table of elements with the elements in the color given
% by the ColorCode function

% create a representation of the periodic table
PTE = {'H', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'He';
'Li', 'Be', '', '', '', '', '', '', '', '', '', '', 'B', 'C', 'N', 'O', 'F', 'Ne';
'Na', 'Mg', '', '', '', '', '', '', '', '', '', '', 'Al', 'Si', 'P', 'S', 'Cl', 'Ar';
'K', 'Ca', 'Sc', 'Ti', 'V', 'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu', 'Zn', 'Ga', 'Ge', 'As', 'Se', 'Br', 'Kr';
'Rb', 'Sr', 'Y', 'Zr', 'Nb', 'Mo', 'Tc', 'Ru', 'Rh', 'Pd', 'Ag', 'Cd', 'In', 'Sn', 'Sb', 'Te', 'I', 'Xe';
'Cs', 'Ba', 'La', 'Hf', 'Ta', 'W', 'Re', 'Os', 'Ir', 'Pt', 'Au', 'Hg', 'Tl', 'Pb', 'Bi', 'Po', 'At', 'Rn';
'Fr', 'Ra', 'Ac', 'Rf', 'Db', 'Sg', 'Bh', 'Hs', 'Mt', 'Ds', 'Rg', 'Cn', 'Nh', 'Fl', 'Mc', 'Lv', 'Ts', 'Og';
'', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '';
'', '', 'La', 'Ce', 'Pr', 'Nd', 'Pm', 'Sm', 'Eu', 'Gd', 'Tb', 'Dy', 'Ho', 'Er', 'Tm', 'Yb', 'Lu', '';
'', '', 'Ac', 'Th', 'Pa', 'U', 'Np', 'Pu', 'Am', 'Cm', 'Bk', 'Cf', 'Es', 'Fm', 'Md', 'No', 'Lr', ''};

% create figure and axes
f = figure('Color', 'w', 'Toolbar', 'none', 'MenuBar', 'none');
ax = axes('Parent', f, 'Position', [0 0 1 1]);
% loop over the entries of the PTE cell
for RI = 1:size(PTE, 1)
    for CI = 1:size(PTE, 2)
        El = PTE{RI, CI};
        if isempty(El); continue; end
        % calculate the position of the element
        x = CI;
        y = size(PTE, 1) - RI;
        % get color of the element
        Col = ChEl(El).Color;
        % draw rectangle
        rectangle('Position', [x, y, 1, 1], 'FaceColor', Col, ...
            'Parent', ax);
        % write element name in the rectangle
        t = text(x+0.5, y+0.5, El, 'Parent', ax, ...
            'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle');
        % change fore color to white if the color is dark
        if sum(Col)/3 < 0.3
            set(t, 'Color', 'w');
        end
    end
end
% set axis properties
axis(ax, 'equal')
axis(ax, 'off')
end