function [MarkerMenu, DSMenu, LabelMenu, SFMenu] = CreateContextMenus(FigHandle)
% Creates the menus shown when hitting one of the other properties buttons
%
% Usage:
%     [MarkerMenu, DSMenu, LabelMenu, SFMenu] = CreateContextMenus(FigHandle)
%
% Input:
%     FigHandle ... the figure handle of the Crystal Module (which will be
%                   used as parent for the context menus)
%
% Output:
%     MarkerMenu, DSMenu, LabelMenu, SFMenu ... the handles of the created
%                                               context menus
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Menu for marker
MarkerMenu = uicontextmenu('Parent', FigHandle);
MM = uimenu(MarkerMenu, 'Label', 'Marker', 'Callback', {@MarkerMenu_Callback, FigHandle});
uimenu(MM, 'Label', 'Plus', 'Callback', {@ChangeMarker, FigHandle, '+'}, 'Tag', '+');
uimenu(MM, 'Label', 'Circle', 'Callback', {@ChangeMarker, FigHandle, 'o'}, 'Tag', 'o');
uimenu(MM, 'Label', 'Asterisk', 'Callback', {@ChangeMarker, FigHandle, '*'}, 'Tag', '*');
uimenu(MM, 'Label', 'Cross', 'Callback', {@ChangeMarker, FigHandle, 'x'}, 'Tag', 'x');
uimenu(MM, 'Label', 'Square', 'Callback', {@ChangeMarker, FigHandle, 's'}, 'Tag', 'square');
uimenu(MM, 'Label', 'Diamond', 'Callback', {@ChangeMarker, FigHandle, 'd'}, 'Tag', 'diamond');
uimenu(MM, 'Label', 'Pentagram', 'Callback', {@ChangeMarker, FigHandle, 'p'}, 'Tag', 'pentagram');
uimenu(MM, 'Label', 'Hexagram', 'Callback', {@ChangeMarker, FigHandle, 'h'}, 'Tag', 'hexagram');

MM = uimenu(MarkerMenu, 'Label', 'Face Color', 'Callback', {@Color_Callback, FigHandle, 'BraggFaceColor'});
uimenu(MM, 'Label', 'None', 'Callback', {@ChangeMarkerFaceColor, FigHandle, 'none'}, 'Tag', 'none');
uimenu(MM, 'Label', 'Black', 'Callback', {@ChangeMarkerFaceColor, FigHandle, 'k'}, 'Tag', '0,0,0');
uimenu(MM, 'Label', 'White', 'Callback', {@ChangeMarkerFaceColor, FigHandle, 'w'}, 'Tag', '1,1,1');
uimenu(MM, 'Label', 'Yellow', 'Callback', {@ChangeMarkerFaceColor, FigHandle, 'y'}, 'Tag', '1,1,0');
uimenu(MM, 'Label', 'Magenta', 'Callback', {@ChangeMarkerFaceColor, FigHandle, 'm'}, 'Tag', '1,0,1');
uimenu(MM, 'Label', 'Cyan', 'Callback', {@ChangeMarkerFaceColor, FigHandle, 'c'}, 'Tag', '0,1,1');
uimenu(MM, 'Label', 'Red', 'Callback', {@ChangeMarkerFaceColor, FigHandle, 'r'}, 'Tag', '1,0,0');
uimenu(MM, 'Label', 'Green', 'Callback', {@ChangeMarkerFaceColor, FigHandle, 'g'}, 'Tag', '0,1,0');
uimenu(MM, 'Label', 'Blue', 'Callback', {@ChangeMarkerFaceColor, FigHandle, 'b'}, 'Tag', '0,0,1');
uimenu(MM, 'Label', 'Custom', 'Callback', {@ChangeMarkerFaceColor, FigHandle, ''}, 'Tag', 'custom');

MM = uimenu(MarkerMenu, 'Label', 'Edge Color', 'Callback', {@Color_Callback, FigHandle, 'BraggEdgeColor'});
uimenu(MM, 'Label', 'None', 'Callback', {@ChangeMarkerEdgeColor, FigHandle, 'none'}, 'Tag', 'none');
uimenu(MM, 'Label', 'Black', 'Callback', {@ChangeMarkerEdgeColor, FigHandle, 'k'}, 'Tag', '0,0,0');
uimenu(MM, 'Label', 'White', 'Callback', {@ChangeMarkerEdgeColor, FigHandle, 'w'}, 'Tag', '1,1,1');
uimenu(MM, 'Label', 'Yellow', 'Callback', {@ChangeMarkerEdgeColor, FigHandle, 'y'}, 'Tag', '1,1,0');
uimenu(MM, 'Label', 'Magenta', 'Callback', {@ChangeMarkerEdgeColor, FigHandle, 'm'}, 'Tag', '1,0,1');
uimenu(MM, 'Label', 'Cyan', 'Callback', {@ChangeMarkerEdgeColor, FigHandle, 'c'}, 'Tag', '0,1,1');
uimenu(MM, 'Label', 'Red', 'Callback', {@ChangeMarkerEdgeColor, FigHandle, 'r'}, 'Tag', '1,0,0');
uimenu(MM, 'Label', 'Green', 'Callback', {@ChangeMarkerEdgeColor, FigHandle, 'g'}, 'Tag', '0,1,0');
uimenu(MM, 'Label', 'Blue', 'Callback', {@ChangeMarkerEdgeColor, FigHandle, 'b'}, 'Tag', '0,0,1');
uimenu(MM, 'Label', 'Custom', 'Callback', {@ChangeMarkerEdgeColor, FigHandle, ''}, 'Tag', 'custom');

uimenu(MarkerMenu, 'Label', 'Size', 'Callback', {@ChangeMarkerSize, FigHandle});
uimenu(MarkerMenu, 'Label', 'Mirror Bragg Peaks', 'Callback', {@MirrorBraggPeakMarkers, FigHandle}, 'Tag', 'MirrorBraggPeaks');


%% menu for DS ring
DSMenu = uicontextmenu('Parent', FigHandle);
DS = uimenu(DSMenu, 'Label', 'Line Color', 'Callback', {@Color_Callback, FigHandle, 'DSColor'});
uimenu(DS, 'Label', 'Black', 'Callback', {@ChangeDSRingColor, FigHandle, 'k'}, 'Tag', '0,0,0');
uimenu(DS, 'Label', 'White', 'Callback', {@ChangeDSRingColor, FigHandle, 'w'}, 'Tag', '1,1,1');
uimenu(DS, 'Label', 'Yellow', 'Callback', {@ChangeDSRingColor, FigHandle, 'y'}, 'Tag', '1,1,0');
uimenu(DS, 'Label', 'Magenta', 'Callback', {@ChangeDSRingColor, FigHandle, 'm'}, 'Tag', '1,0,1');
uimenu(DS, 'Label', 'Cyan', 'Callback', {@ChangeDSRingColor, FigHandle, 'c'}, 'Tag', '0,1,1');
uimenu(DS, 'Label', 'Red', 'Callback', {@ChangeDSRingColor, FigHandle, 'r'}, 'Tag', '1,0,0');
uimenu(DS, 'Label', 'Green', 'Callback', {@ChangeDSRingColor, FigHandle, 'g'}, 'Tag', '0,1,0');
uimenu(DS, 'Label', 'Blue', 'Callback', {@ChangeDSRingColor, FigHandle, 'b'}, 'Tag', '0,0,1');
uimenu(DS, 'Label', 'Custom', 'Callback', {@ChangeDSRingColor, FigHandle, ''}, 'Tag', 'custom');

DS = uimenu(DSMenu, 'Label', 'Line Style', 'Callback', {@LineStyle_Callback, FigHandle, 'DSLineStyle'});
uimenu(DS, 'Label', 'Solid', 'Callback', {@ChangeDSRingLineStyle, FigHandle, '-'}, 'Tag', '-');
uimenu(DS, 'Label', 'Dashed', 'Callback', {@ChangeDSRingLineStyle, FigHandle, '--'}, 'Tag', '--');
uimenu(DS, 'Label', 'Dotted', 'Callback', {@ChangeDSRingLineStyle, FigHandle, ':'}, 'Tag', ':');
uimenu(DS, 'Label', 'Dash-Dot', 'Callback', {@ChangeDSRingLineStyle, FigHandle, '-.'}, 'Tag', '-.');

uimenu(DSMenu, 'Label', 'Line Width', 'Callback', {@ChangeDSRingLineWidth, FigHandle});


%% menu for Label
LabelMenu = uicontextmenu('Parent', FigHandle);
uimenu(LabelMenu, 'Label', 'Font Properties', 'Callback', {@ChangeLabelFontProperties, FigHandle});

LM = uimenu(LabelMenu, 'Label', 'Fore Color', 'Callback', {@Color_Callback, FigHandle, 'LabelForeColor'});
uimenu(LM, 'Label', 'Black', 'Callback', {@ChangeLabelForeColor, FigHandle, 'k'}, 'Tag', '0,0,0');
uimenu(LM, 'Label', 'White', 'Callback', {@ChangeLabelForeColor, FigHandle, 'w'}, 'Tag', '1,1,1');
uimenu(LM, 'Label', 'Yellow', 'Callback', {@ChangeLabelForeColor, FigHandle, 'y'}, 'Tag', '1,1,0');
uimenu(LM, 'Label', 'Magenta', 'Callback', {@ChangeLabelForeColor, FigHandle, 'm'}, 'Tag', '1,0,1');
uimenu(LM, 'Label', 'Cyan', 'Callback', {@ChangeLabelForeColor, FigHandle, 'c'}, 'Tag', '0,1,1');
uimenu(LM, 'Label', 'Red', 'Callback', {@ChangeLabelForeColor, FigHandle, 'r'}, 'Tag', '1,0,0');
uimenu(LM, 'Label', 'Green', 'Callback', {@ChangeLabelForeColor, FigHandle, 'g'}, 'Tag', '0,1,0');
uimenu(LM, 'Label', 'Blue', 'Callback', {@ChangeLabelForeColor, FigHandle, 'b'}, 'Tag', '0,0,1');
uimenu(LM, 'Label', 'Custom', 'Callback', {@ChangeLabelForeColor, FigHandle, ''}, 'Tag', 'custom');

LM = uimenu(LabelMenu, 'Label', 'Background Color', 'Callback', {@Color_Callback, FigHandle, 'LabelBackgroundColor'});
uimenu(LM, 'Label', 'None', 'Callback', {@ChangeLabelBackgroundColor, FigHandle, 'none'}, 'Tag', 'none');
uimenu(LM, 'Label', 'Black', 'Callback', {@ChangeLabelBackgroundColor, FigHandle, 'k'}, 'Tag', '0,0,0');
uimenu(LM, 'Label', 'White', 'Callback', {@ChangeLabelBackgroundColor, FigHandle, 'w'}, 'Tag', '1,1,1');
uimenu(LM, 'Label', 'Yellow', 'Callback', {@ChangeLabelBackgroundColor, FigHandle, 'y'}, 'Tag', '1,1,0');
uimenu(LM, 'Label', 'Magenta', 'Callback', {@ChangeLabelBackgroundColor, FigHandle, 'm'}, 'Tag', '1,0,1');
uimenu(LM, 'Label', 'Cyan', 'Callback', {@ChangeLabelBackgroundColor, FigHandle, 'c'}, 'Tag', '0,1,1');
uimenu(LM, 'Label', 'Red', 'Callback', {@ChangeLabelBackgroundColor, FigHandle, 'r'}, 'Tag', '1,0,0');
uimenu(LM, 'Label', 'Green', 'Callback', {@ChangeLabelBackgroundColor, FigHandle, 'g'}, 'Tag', '0,1,0');
uimenu(LM, 'Label', 'Blue', 'Callback', {@ChangeLabelBackgroundColor, FigHandle, 'b'}, 'Tag', '0,0,1');
uimenu(LM, 'Label', 'Custom', 'Callback', {@ChangeLabelBackgroundColor, FigHandle, ''}, 'Tag', 'custom');

LM = uimenu(LabelMenu, 'Label', 'Position');
uimenu(LM, 'Label', 'North', 'Callback', {@ChangeLabelPosition_Callback, FigHandle, 'north'});
uimenu(LM, 'Label', 'East', 'Callback', {@ChangeLabelPosition_Callback, FigHandle, 'east'});
uimenu(LM, 'Label', 'South', 'Callback', {@ChangeLabelPosition_Callback, FigHandle, 'south'});
uimenu(LM, 'Label', 'West', 'Callback', {@ChangeLabelPosition_Callback, FigHandle, 'west'});
uimenu(LM, 'Label', 'North East', 'Callback', {@ChangeLabelPosition_Callback, FigHandle, 'north east'});
uimenu(LM, 'Label', 'South East', 'Callback', {@ChangeLabelPosition_Callback, FigHandle, 'south east'});
uimenu(LM, 'Label', 'South West', 'Callback', {@ChangeLabelPosition_Callback, FigHandle, 'south west'});
uimenu(LM, 'Label', 'North West', 'Callback', {@ChangeLabelPosition_Callback, FigHandle, 'north west'});

%% menu for SF ring
SFMenu = uicontextmenu('Parent', FigHandle);
SF = uimenu(SFMenu, 'Label', 'Line Color', 'Callback', {@Color_Callback, FigHandle, 'SFColor'});
uimenu(SF, 'Label', 'Black', 'Callback', {@ChangeSFRingColor, FigHandle, 'k'}, 'Tag', '0,0,0');
uimenu(SF, 'Label', 'White', 'Callback', {@ChangeSFRingColor, FigHandle, 'w'}, 'Tag', '1,1,1');
uimenu(SF, 'Label', 'Yellow', 'Callback', {@ChangeSFRingColor, FigHandle, 'y'}, 'Tag', '1,1,0');
uimenu(SF, 'Label', 'Magenta', 'Callback', {@ChangeSFRingColor, FigHandle, 'm'}, 'Tag', '1,0,1');
uimenu(SF, 'Label', 'Cyan', 'Callback', {@ChangeSFRingColor, FigHandle, 'c'}, 'Tag', '0,1,1');
uimenu(SF, 'Label', 'Red', 'Callback', {@ChangeSFRingColor, FigHandle, 'r'}, 'Tag', '1,0,0');
uimenu(SF, 'Label', 'Green', 'Callback', {@ChangeSFRingColor, FigHandle, 'g'}, 'Tag', '0,1,0');
uimenu(SF, 'Label', 'Blue', 'Callback', {@ChangeSFRingColor, FigHandle, 'b'}, 'Tag', '0,0,1');
uimenu(SF, 'Label', 'Custom', 'Callback', {@ChangeSFRingColor, FigHandle, ''}, 'Tag', 'custom');

SF = uimenu(SFMenu, 'Label', 'Line Style', 'Callback', {@LineStyle_Callback, FigHandle, 'SFLineStyle'});
uimenu(SF, 'Label', 'Solid', 'Callback', {@ChangeSFRingLineStyle, FigHandle, '-'}, 'Tag', '-');
uimenu(SF, 'Label', 'Dashed', 'Callback', {@ChangeSFRingLineStyle, FigHandle, '--'}, 'Tag', '--');
uimenu(SF, 'Label', 'Dotted', 'Callback', {@ChangeSFRingLineStyle, FigHandle, ':'}, 'Tag', ':');
uimenu(SF, 'Label', 'Dash-Dot', 'Callback', {@ChangeSFRingLineStyle, FigHandle, '-.'}, 'Tag', '-.');

uimenu(SFMenu, 'Label', 'Line Width', 'Callback', {@ChangeSFLineWidth, FigHandle});

function LineStyle_Callback(src, evt, FigHandle, Property)
handles = guidata(FigHandle);
Index = get(handles.PM_CrystalSelection, 'Value');
CP = handles.Crystals{Index, 2};
Val = eval(['CP.', Property, '{1}']);
Cntxts = get(src, 'Children');
set(Cntxts, 'Checked', 'off');
Cntxt = findobj(Cntxts, 'Tag', Val);
set(Cntxt, 'Checked', 'on');


function ChangeLabelPosition_Callback(src, evt, FigHandle, Val)
handles = guidata(FigHandle);
Index = get(handles.PM_CrystalSelection, 'Value');
CP = handles.Crystals{Index, 2};
CP.LabelLocation = Val;

function ChangeSFLineWidth(src, evt, FigHandle)
handles = guidata(FigHandle);
Index = get(handles.PM_CrystalSelection, 'Value');
CP = handles.Crystals{Index, 2};
Def = CP.SFLineWidth;
if isempty(Def); return; end
if iscell(Def); Def = Def{1}; end
Val = AskForNumericValue('Please enter the new line width:', 'New Line Width', Def);
if ~isempty(Val);
    Vals = repmat({Val}, numel(CP), 1);
    [CP.SFLineWidth] = Vals{:};
end

function ChangeSFRingLineStyle(src, evt, FigHandle, Val)
handles = guidata(FigHandle);
Index = get(handles.PM_CrystalSelection, 'Value');
CP = handles.Crystals{Index, 2};
Vals = repmat({Val}, numel(CP), 1);
[CP.SFLineStyle] = Vals{:};

function ChangeSFRingColor(src, evt, FigHandle, Val)
handles = guidata(FigHandle);
Index = get(handles.PM_CrystalSelection, 'Value');
CP = handles.Crystals{Index, 2};
if isempty(Val)
    Val = AskForColorValue('Custom Edge Color', CP.SFColor{1}, true);
    if isempty(Val); return; end
end
Vals = repmat({Val}, numel(CP), 1);
[CP.SFColor] = Vals{:};

function ChangeLabelBackgroundColor(src, evt, FigHandle, Val)
handles = guidata(FigHandle);
Index = get(handles.PM_CrystalSelection, 'Value');
CP = handles.Crystals{Index, 2};
if isempty(Val)
    Val = AskForColorValue('Custom Background Color', CP.LabelBackgroundColor{1}, true);
    if isempty(Val); return; end
end
Vals = repmat({Val}, numel(CP), 1);
[CP.LabelBackgroundColor] = Vals{:};

function ChangeLabelForeColor(src, evt, FigHandle, Val)
handles = guidata(FigHandle);
Index = get(handles.PM_CrystalSelection, 'Value');
CP = handles.Crystals{Index, 2};
if isempty(Val)
    Val = AskForColorValue('Custom Fore Color', CP.LabelForeColor{1}, true);
    if isempty(Val); return; end
end
Vals = repmat({Val}, numel(CP), 1);
[CP.LabelForeColor] = Vals{:};

function ChangeLabelFontProperties(src, evt, FigHandle)
handles = guidata(FigHandle);
Index = get(handles.PM_CrystalSelection, 'Value');
CP = handles.Crystals{Index, 2};
CP.ChangeFontProperties();

function ChangeDSRingLineWidth(src, evt, FigHandle)
handles = guidata(FigHandle);
Index = get(handles.PM_CrystalSelection, 'Value');
CP = handles.Crystals{Index, 2};
Def = CP.DSLineWidth;
if isempty(Def); return; end
if iscell(Def); Def = Def{1}; end
Val = AskForNumericValue('Please enter the new line width:', 'New Line Width', Def);
if ~isempty(Val);
    Vals = repmat({Val}, numel(CP), 1);
    [CP.DSLineWidth] = Vals{:};
end

function ChangeDSRingLineStyle(src, evt, FigHandle, Val)
handles = guidata(FigHandle);
Index = get(handles.PM_CrystalSelection, 'Value');
CP = handles.Crystals{Index, 2};
Vals = repmat({Val}, numel(CP), 1);
[CP.DSLineStyle] = Vals{:};

function ChangeDSRingColor(src, evt, FigHandle, Val)
handles = guidata(FigHandle);
Index = get(handles.PM_CrystalSelection, 'Value');
CP = handles.Crystals{Index, 2};
if isempty(Val)
    Val = AskForColorValue('Custom Edge Color', CP.DSColor{1}, true);
    if isempty(Val); return; end
end
Vals = repmat({Val}, numel(CP), 1);
[CP.DSColor] = Vals{:};

%% Menu for marker

function MarkerMenu_Callback(src, evt, FigHandle)
handles = guidata(FigHandle);
Index = get(handles.PM_CrystalSelection, 'Value');
CP = handles.Crystals{Index, 2};
Val = CP.BraggMarker{1};
Cntxts = get(src, 'Children');
set(Cntxts, 'Checked', 'off');
Cntxt = findobj(Cntxts, 'Tag', Val);
set(Cntxt, 'Checked', 'on');

function Color_Callback(src, evt, FigHandle, Property)
handles = guidata(FigHandle);
Index = get(handles.PM_CrystalSelection, 'Value');
CP = handles.Crystals{Index, 2};
Val = eval(['CP.', Property, '{1}']);
Cntxts = get(src, 'Children');
set(Cntxts, 'Checked', 'off');
if ischar(Val)
    Cntxt = findobj(Cntxts, 'Tag', Val);
else
    Cntxt = findobj(Cntxts, 'Tag', sprintf('%d,%d,%d', Val));
end
if isempty(Cntxt); Cntxt = findobj(Cntxts, 'Tag', 'custom'); end
set(Cntxt, 'Checked', 'on');

function ChangeMarkerEdgeColor(src, evt, FigHandle, Val)
handles = guidata(FigHandle);
Index = get(handles.PM_CrystalSelection, 'Value');
CP = handles.Crystals{Index, 2};
if isempty(Val)
    Val = AskForColorValue('Custom Edge Color', CP.BraggEdgeColor{1}, true);
    if isempty(Val); return; end
end
CP.BraggEdgeColor = Val;

function ChangeMarkerFaceColor(src, evt, FigHandle, Val)
handles = guidata(FigHandle);
Index = get(handles.PM_CrystalSelection, 'Value');
CP = handles.Crystals{Index, 2};
if isempty(Val)
    Val = AskForColorValue('Custom Face Color', CP.BraggFaceColor{1}, true);
    if isempty(Val); return; end
end
CP.BraggFaceColor = Val;

function MirrorBraggPeakMarkers(src, evt, FigHandle)
handles = guidata(FigHandle);
Index = get(handles.PM_CrystalSelection, 'Value');
CP = handles.Crystals{Index, 2};
CP.BraggPeaksMirrored = ~CP.BraggPeaksMirrored;

function ChangeMarker(src, evt, FigHandle, Val)
handles = guidata(FigHandle);
Index = get(handles.PM_CrystalSelection, 'Value');
CP = handles.Crystals{Index, 2};
CP.BraggMarker = Val;

function ChangeMarkerSize(src, evt, FigHandle)
handles = guidata(FigHandle);
Index = get(handles.PM_CrystalSelection, 'Value');
CP = handles.Crystals{Index, 2};
Def = CP.BraggSize;
if isempty(Def); return; end
if iscell(Def); Def = Def{1}; end
Val = AskForNumericValue('Please enter the new marker size:', 'New Marker Size', Def);
if ~isempty(Val);
    CP.BraggSize = Val;
end

function Val = AskForNumericValue(Msg, Title, DefaultAnswer)
answer = inputdlg(Msg, Title, ...
    1, ... %only one line can be entered
    {num2str(DefaultAnswer)}); % default answer is old line width value
if ~isempty(answer) && ~isnan(str2double(answer{:})) % check for valid input
    Val = str2double(answer{:});
else
    Val = [];
end

function Val = AskForColorValue(Title, DefaultAnswer, ForceRGB)
% RGB and eventually A values are between 0 and 1
% default answer: vector with RGB and eventually A value between 0 and 1
% ForceRGB: if true, alpha value will not be asked for. Default: false
if nargin == 2 || isempty(ForceRGB); ForceRGB = false; end
if verLessThan('Matlab', '8.4') || ForceRGB % Matlab versions with HG1
    % create an inputdlg asking for new line color
    answer = inputdlg({'Red value (between 0 and 255)', ...
        'Green value (between 0 and 255)', ...
        'Blue value (between 0 and 255)'}, ...
        Title, ...
        1, ...
        {num2str(DefaultAnswer(1)*255), num2str(DefaultAnswer(2)*255), num2str(DefaultAnswer(3)*255)});
    if isempty(answer); Val = []; return; end % user cancelled dialog
    R = str2double(answer{1});
    G = str2double(answer{2});
    B = str2double(answer{3});
    % check for valid input
    if isnan(R) || isnan(G) || isnan(B); Val = []; return; end
    if R < 0 || R > 255 || G < 0 || G > 255 || B < 0 || B > 255; Val = []; return; end
    % set the line color
    Val = [R G B]./255;
else
    % create an inputdlg asking for new line color
    if numel(DefaultAnswer) == 3; DefaultAnswer = [DefaultAnswer 1]; end
    answer = inputdlg({'Red value (between 0 and 255)', ...
        'Green value (between 0 and 255)', ...
        'Blue value (between 0 and 255)', ...
        'Alpha value (between 0 and 255)'}, ...
        Title, ...
        1, ...
        {num2str(DefaultAnswer(1)*255), num2str(DefaultAnswer(2)*255), num2str(DefaultAnswer(3)*255), num2str(DefaultAnswer(4)*255)});
    if isempty(answer); Val = []; return; end % user cancelled dialog
    R = str2double(answer{1});
    G = str2double(answer{2});
    B = str2double(answer{3});
    A = str2double(answer{4});
    % check for valid input
    if isnan(R) || isnan(G) || isnan(B) || isnan(A); Val = []; return; end
    if R < 0 || R > 255 || G < 0 || G > 255 || B < 0 || B > 255 || A < 0 || A > 255; Val = []; return; end
    % set the line color
    Val = [R G B A]./255;
end