function M = CreateSymmetryMatrix(SymmStr)
% CreateSymmetryMatrix creates the matrix representation of a string
% symmetry operation.
%
% Usage:
%     M = CreateSymmetryMatrix(SymmStr);
%
% Input:
%     SymmStr ... the symmetry operation in string representation.
%
% Output:
%     M ... the symmetry operation in matrix representation.
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% make sure that the variables in the symmetry string are in lower case
SymmStr = lower(SymmStr);

SymmParts = regexp(SymmStr, ',', 'split');
M = zeros(3, 4);
vars = {'x', 'y', 'z'};
for iS = 1:numel(SymmParts)
    Str = SymmParts{iS};

    Matches = regexp(Str, '[-+]?[xyz]{1}', 'match');

    Str2 = Str;
    for iM = 1:numel(Matches)
        for iV = 1:numel(vars)
            if strcmp(Matches{iM}, ['-', vars{iV}])
                M(iS, iV) = -1;
            elseif strcmp(Matches{iM}, vars{iV}) || strcmp(Matches{iM}, ['+', vars{iV}])
                M(iS, iV) = 1;
            end
        end
        Str2 = strrep(Str2, Matches{iM}, '');
    end

    b = str2num(Str2); %#ok BS, since str2double not working for cases such as 1/2
    if isempty(b); b = 0; end
    M(iS, end) = b;
end