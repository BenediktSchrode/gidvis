function OutStr = CreateSymmetryString(MC)
% CreateSymmetryString
%
% Usage:
%     
%
% Input:
%     
%
% Output:
%     
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% make sure that M is a cell
if ~iscell(MC); MC = {MC}; end


OutStr = cell(size(MC));

for iM = 1:numel(MC)
    M = MC{iM};
    FullStr = cell(1, 3);
    
    for Row = 1:size(M, 1)
        Str = '';
        if M(Row, 1) == 1
            Str = 'x';
        elseif M(Row, 1) == -1
            Str = '-x';
        elseif M(Row, 1) > 0
            Str = sprintf('%gx', M(Row, 1));
        elseif M(Row, 1) < 0
            Str = sprintf('%gx', M(Row, 1));
        end

        if M(Row, 2) == 1
            if numel(Str) > 0
                Str = [Str, ' + y'];
            else
                Str = 'y';
            end
        elseif M(Row, 2) == -1
            if numel(Str) > 0
                Str = [Str, ' - y'];
            else
                Str = [Str, '-y'];
            end
        elseif M(Row, 2) > 0
            if numel(Str) > 0
                Str = [Str, sprintf(' + %gy', M(Row, 2))];
            else
                Str = sprintf('%gy', M(Row, 2));
            end
        elseif M(Row, 2) < 0
            if numel(Str) > 0
                Str = [Str, sprintf(' - %gy', abs(M(Row, 2)))];
            else
                Str = sprintf('%gy', M(Row, 2));
            end
        end

        if M(Row, 3) == 1
            if numel(Str) > 0
                Str = [Str, ' + z'];
            else
                Str = 'z';
            end
        elseif M(Row, 3) == -1
            if numel(Str) > 0
                Str = [Str, ' - z'];
            else
                Str = [Str, '-z'];
            end
        elseif M(Row, 3) > 0
            if numel(Str) > 0
                Str = [Str, sprintf(' + %gz', M(Row, 3))];
            else
                Str = sprintf('%gz', M(Row, 3));
            end
        elseif M(Row, 3) < 0
            if numel(Str) > 0
                Str = [Str, sprintf(' - %gz', abs(M(Row, 3)))];
            else
                Str = sprintf('%gz', M(Row, 3));
            end
        end

        if numel(Str > 0) && M(Row, 4) > 0
            Str = [Str, sprintf(' + %g', M(Row, 4))];
        elseif numel(Str > 0) && M(Row, 4) < 0
            Str = [Str, sprintf(' - %g', abs(M(Row, 4)))];
        elseif M(Row, 4) > 0
            Str = sprintf('+%g', M(Row, 4));
        elseif M(Row, 4) < 0
            Str = sprintf('-%g', abs(M(Row, 4)));
        end

        FullStr{Row} = Str;
    end
    
    OutStr{iM} = [FullStr{1}, ', ', FullStr{2}, ', ', FullStr{3}];
end

% if input was a single matrix, make sure to return a string and not a cell
if numel(MC) == 1; OutStr = OutStr{1}; end