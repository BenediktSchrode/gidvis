function CrystalDataView(CP, hObject, ToolboxModuleHandle)
% This function creates the Crystal Data View, a window containing all the
% information of the crystal.
%
% Usage:
%     CrystalDataView(CP, hObject, ToolboxModuleHandle)
%
% Input:
%     CP ... the Crystal_Peak_New object from which the data should be
%            displayed
%     hObject ... the handle of the button used to start the
%                 CrystalDataView
%     ToolboxModuleHandle ... any UI element of the Toolbox Module
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

CrystalHandles = guidata(hObject);

% create the default data view of the crystal
f = DataView(CP);

% turn off visibility
set(f, 'Visible', 'off')
drawnow();

% get the tab group of the result figure
tgroup = get(f, 'Children');

% calculate q values, structure factor + structure factor^2 including
% rotation
q = CP.q;
SF = CP.StructureFactor;
SF2Rot = CP.GetStructureFactors2Rot(CP.hkl, CP.Orientation);
if isempty(SF) % no atoms given for this crystal, SF is empty
    SF = NaN(size(CP.hkl, 1), 1); % make a NaN of appropriate size
    SF2Rot = SF; % make a NaN of appropriate size
end

M=    [...
    CP.hkl(:, 1)'; ...
    CP.hkl(:, 2)'; ...
    CP.hkl(:, 3)'; ...
    q(:, 1).'; ...
    q(:, 2).'; ...
    q(:, 3).'; ...
    sqrt(q(:, 1).^2+q(:, 2).^2).'; ...
    sqrt(q(:, 1).^2+q(:, 2).^2+q(:, 3).^2).'; ... % length of q
    real(SF)'; ...
    imag(SF)'; ...
    abs(SF)'; ...
    abs(SF).^2'; ...
    SF2Rot'; ...
    2*pi./sqrt(q(:, 1).^2+q(:, 2).^2+q(:, 3).^2).'; ... % d spacing
    NaN(size(SF')); ... % x0
    NaN(size(SF')); ... % y0
    NaN(size(SF')); ... % A
    NaN(size(SF')); ...
    NaN(size(SF')); ...
    NaN(size(SF')); ...
    NaN(size(SF'))];  % Box Pos.

% create tab for the scattering data
SFTab = uitab('Parent', tgroup, 'Title', 'Scattering Data');
% get the children of the tabgroup
Ch = get(tgroup, 'Children');
% resort the children of the tabgroup to make the tab for the scattering
% data appearing first
if numel(Ch) > 1
    Ch = [Ch(end); Ch(1:end-1)];
    set(tgroup, 'Children', Ch);
    set(tgroup, 'SelectedTab', Ch(1));
end
% make table for the scattering data
Tab = uitable(SFTab);
set(Tab, 'Data', M');
set(Tab, 'Units', 'normalized');
set(Tab, 'Position', [0 0 1 0.9]);
set(Tab, 'FontSize', 10);
ColNames = {'h', 'k', 'l', 'q_x', 'q_y', 'q_z (theor.)', ...
    'q_xy (theor.)', 'q', 'real(SF)', 'imag(SF)', 'abs(SF)', ...
    'abs(SF)^2', 'abs(SF)^2 (Rotation)', 'd', 'q_xy (exp.)', 'q_z (exp.)', ...
    'A', 'Box Pos. x', 'Box Pos. y', 'Box Pos. W', 'Box Pos. H'};
set(Tab, 'ColumnName', ColNames);
% create string for copy/paste message
T = uicontrol(SFTab, 'Style', 'text', 'String', ...
    'Use Ctrl + A then Ctrl + C in the table below to copy the data to the clipboard.', ...
    'Units', 'normalized', 'Position', [0 0.92 0.5 0.06], 'FontSize', 10);
set(f, 'Color', get(T, 'BackgroundColor'));
% create button to export data
uicontrol(SFTab, 'Style', 'pushbutton', 'String', 'Export to file', ...
    'Units', 'normalized', 'Position', [0.5 0.92 0.15 0.06], ...
    'FontSize', 10, 'Callback', {@WriteValuesToFile, CrystalHandles, Tab});
% create button to determine the experimental position
uicontrol(SFTab, 'Style', 'pushbutton', 'String', 'Det. Exp. Pos.', ...
    'Units', 'normalized', 'Position', [0.67 0.92 0.15 0.06], ...
    'FontSize', 10, 'Callback', {@DetExpPos, CrystalHandles, Tab, ToolboxModuleHandle, f, CP});
% create button to plot a comparison of experimental and theoretical
% intensity values
uicontrol(SFTab, 'Style', 'pushbutton', 'String', 'Plot Comp.', ...
    'Units', 'normalized', 'Position', [0.84 0.92 0.15 0.06], ...
    'FontSize', 10, 'Callback', {@PlotComparison, Tab, CP});

% add a destroyed listener to hObject to close the CrystalDataView
LH = addlistener(hObject, 'ObjectBeingDestroyed', @(src, evt) CloseCrystalDataView(f));
set(f, 'UserData', LH);
% set the CloseRequestFcn callback to remove the listener
set(f, 'CloseRequestFcn', @ViewCloseRequestFcn);

% create a context menu to sort
hcmenu = uicontextmenu('Parent', f);
uimenu(hcmenu, 'Label', 'Sort q ascending', 'Callback', {@SortData, Tab, 8, 'ascend'});
uimenu(hcmenu, 'Label', 'Sort q descending', 'Callback', {@SortData, Tab, 8, 'descend'});
uimenu(hcmenu, 'Label', 'Sort abs(SF)^2 ascending', 'Callback', {@SortData, Tab, 12, 'ascend'});
uimenu(hcmenu, 'Label', 'Sort abs(SF)^2 descending', 'Callback', {@SortData, Tab, 12, 'descend'});
set(Tab, 'UIContextMenu', hcmenu);

% turn on visibility
set(f, 'Visible', 'on')
drawnow();

function ViewCloseRequestFcn(src, evt)
% Request to close the CrystalDataView.
LH = get(src, 'UserData');
delete(LH);
delete(src);

function CloseCrystalDataView(FigureHandle)
% closes the CrystalDataView.
close(FigureHandle);

function SortData(src, evt, Tab, Ind, Dir)
% Sort the data of the table according to the column given by Ind by
% direction Dir.
M = get(Tab, 'Data');
[~, SortInd] = sort(M(:, Ind), Dir);
set(Tab, 'Data', M(SortInd, :));

function PlotComparison(src, evt, TableHandle, CP)
% plot a comparison of measured and expected |SF|^2.

% get data
D = get(TableHandle, 'Data');

% prepare x-axis label
XLabels = cellstr(num2str(D(:, 1:3)));
XLabels = cellfun(@(x) strrep(x, '  ', ' '), XLabels, 'UniformOutput', false); % replace double spaces by single spaces
XLabels = cellfun(@strtrim, XLabels, 'UniformOutput', false); % remove leading and trailing whitespace

f = figure;
ax = subplot(3, 2, 1, 'Parent', f);
bar([CP.PlusMinusqxy.*D(:, 7) D(:, 15)], 'Parent', ax)
title(ax, 'Comparison of q_{xy}');
legend('Theoretical', 'Experimental');
set(ax, 'XTick', 1:numel(XLabels));
set(ax, 'XTickLabel', XLabels);
set(ax, 'XTickLabelRotation', 45);

ax = subplot(3, 2, 2, 'Parent', f);
bar(D(:, [6 16]), 'Parent', ax)
title(ax, 'Comparison of q_z');
legend('Theoretical', 'Experimental');
set(ax, 'XTick', 1:numel(XLabels));
set(ax, 'XTickLabel', XLabels);
set(ax, 'XTickLabelRotation', 45);

ax = subplot(3, 2, 3:4, 'Parent', f);
M = D(:, [12 17]);
% we normalize the data to the largest theoretical where an intensity was
% experimentally found
[~, SortInd] = sort(M(:, 1), 'descend');
ind = NaN;
for iN = 1:numel(SortInd)
    if ~isnan(M(SortInd(iN), 2))
        ind = SortInd(iN);
        break;
    end
end
if isnan(ind); [~, ind] = max(M(:, 1)); end
M(:, 1) = M(:, 1)./M(ind, 1);
M(:, 2) = M(:, 2)./M(ind, 2);
bar(M, 'Parent', ax)
title(ax, 'Comparison of |SF|^2 (normalized)');
legend('Theoretical', 'Experimental');
set(ax, 'XTick', 1:numel(XLabels));
set(ax, 'XTickLabel', XLabels);
set(ax, 'XTickLabelRotation', 45);

ax = subplot(3, 2, 5:6, 'Parent', f);
M = D(:, [13 17]);
% we normalize the data to the largest theoretical where an intensity was
% experimentally found
[~, SortInd] = sort(M(:, 1), 'descend');
ind = NaN;
for iN = 1:numel(SortInd)
    if ~isnan(M(SortInd(iN), 2))
        ind = SortInd(iN);
        break;
    end
end
if isnan(ind); [~, ind] = max(M(:, 1)); end
M(:, 1) = M(:, 1)./M(ind, 1);
M(:, 2) = M(:, 2)./M(ind, 2);
bar(M, 'Parent', ax)
title(ax, 'Comparison of |SF|^2 (normalized, including rotation)');
legend('Theoretical Including Rotation', 'Experimental');
set(ax, 'XTick', 1:numel(XLabels));
set(ax, 'XTickLabel', XLabels);
set(ax, 'XTickLabelRotation', 45);


function DetExpPos(src, evt, CrystalHandles, TableHandle, TBxHandle, CDVF, CP)
% Determine the experimental peak positions. Uses the calculated peak
% positions of the current GIDVis_Crystal as starting point.
%
% see also: PeakFind
CDVHandles = guidata(CDVF);

set(src, 'Enable', 'off')
drawnow;

if ~isfield(CDVHandles, 'PFs')
    prompt = {'Enter the width of the fitting box:', 'Enter the height of the fitting box:'};
    dlg_title = 'Input';
    num_lines = 1;
    defaultans = {'0.1', '20'};
    answer = inputdlg(prompt, dlg_title, num_lines, defaultans);
    W = str2double(answer{1});
    H = str2double(answer{2});
    if isnan(W) || isnan(H)
        msgbox('Please enter valid numbers.', 'modal')
        set(src, 'Enable', 'on')
        return;
    end
end

Space = ToolboxModule('GetSelectedSpace', TBxHandle);

GD = guidata(CrystalHandles.MainAxes);
CorrectedData = GD.CorrectedData;
beamline = LoadFileModule('GetBeamline', GD.LoadFileModule);
Tbx = guidata(GD.ToolboxModule);
omega = ToolboxModule('GetOmega', GD.ToolboxModule);
chi = str2double(get(Tbx.TB_chi, 'String'));

if isa(GD.RawData, 'qData')
    Q = GD.RawData;
    [xData, yData, ~, ~, rho] = Q.getQ(omega, chi);
    data = GD.CorrectedData;
    if strcmpi(Space, 'polar')
        yData = atan2(xData, yData)*180/pi;
        xData = rho;
    end
else
    if strcmpi(Space, 'q')
        data = CorrectedData(1:beamline.detlenz, 1:beamline.detlenx);
        [xData, yData] = CalculateQ(beamline, omega, chi);
    elseif strcmpi(Space, 'polar')
        % crop data
        data = CorrectedData(1:beamline.detlenz, 1:beamline.detlenx);
        [xData, yData] = CalculatePolar(beamline, omega, chi);
    else
        msgbox('Determining the experimental positions only works in q and polar space.', 'modal');
        set(src, 'Enable', 'off')
        return;
    end
end

D = get(TableHandle, 'Data');
qxy = CP.PlusMinusqxy*D(:, 7);
qz = D(:, 6);

if ~isfield(CDVHandles, 'PFs')
    PFs = PeakFind.empty();
    for iQ = 1:numel(qxy)
        if strcmpi(Space, 'q')
            PFs(end+1) = PeakFind(xData(:), yData(:), data(:), qxy(iQ), qz(iQ), W, H, CrystalHandles.MainAxes);
            PFs(end).x0_Expected = qxy(iQ);
            PFs(end).y0_Expected = qz(iQ);
        elseif strcmpi(Space, 'polar')
            rho = sqrt(qxy(iQ).^2+qz(iQ).^2);
            theta = atan2(qxy(iQ), qz(iQ))*180/pi;
            PFs(end+1) = PeakFind(xData(:), yData(:), data(:), rho, theta, W, H, CrystalHandles.MainAxes);
            PFs(end).x0_Expected = rho;
            PFs(end).y0_Expected = theta;
        end
        PFs(end).UserData = CP.hkl(iQ, 1:3);
        PFs(end).PerformFit();
        plot(PFs(end));
        if ~isempty(PFs(end).FitResult)
            if strcmpi(Space, 'q')
                D(iQ, 15) = PFs(end).FitResult.x0;
                D(iQ, 16) = PFs(end).FitResult.y0;
            elseif strcmpi(Space, 'polar')
                % transform the peak positions rho and phi to cartesian
                r0 = PFs(end).FitResult.x0;
                phi0 = PFs(end).FitResult.y0;
                D(iQ, 16) = r0/sqrt(tand(phi0)^2+1);
                D(iQ, 15) = D(iQ, 15)*tand(phi0);
            end
            D(iQ, 17) = PFs(end).FitResult.A;
        end
        D(iQ, 18) = PFs(end).x0-PFs(end).Width/2;
        D(iQ, 19) = PFs(end).y0-PFs(end).Height/2;
        D(iQ, 20) = PFs(end).Width;
        D(iQ, 21) = PFs(end).Height;
    end
    f = ancestor(TableHandle, 'figure');
    addlistener(f, 'ObjectBeingDestroyed', @(src, evt) FigureDestroyed_CB(src, evt, PFs));

    for iPF = 1:numel(PFs)
        addlistener(PFs(iPF), 'FinishedFitting', @(src, evt) FittingFinished(src, TableHandle));
    end
else
    PFs = CDVHandles.PFs;
    for iP = 1:numel(PFs)
        PFs(iP).XData = xData(:);
        PFs(iP).YData = yData(:);
        PFs(iP).ZData = data(:);
        PFs(iP).PerformFit();
        plot(PFs(iP));
        L = D(:, 1) == PFs(iP).UserData(1) & D(:, 2) == PFs(iP).UserData(2) & ...
            D(:, 3) == PFs(iP).UserData(3);
        if ~isempty(PFs(iP).FitResult)
            if strcmpi(Space, 'q')
                D(L, 15) = PFs(iP).FitResult.x0;
                D(L, 16) = PFs(iP).FitResult.y0;
            elseif strcmpi(space, 'polar')
                % transform the peak positions rho and phi to cartesian
                r0 = PFs(iP).FitResult.x0;
                phi0 = PFs(iP).FitResult.y0;
                D(L, 16) = r0/sqrt(tand(phi0)^2+1);
                D(L, 15) = D(L, 15)*tand(phi0);
            end
            D(L, 17) = PFs(iP).FitResult.A;
        else
            D(L, 15:17) = NaN(1, 3);
        end
    end
end

set(TableHandle, 'Data', D);

CDVHandles.PFs = PFs;
guidata(CDVF, CDVHandles);

function FigureDestroyed_CB(src, evt, PFs)
% Callback that the CrystalDataView gets destroyed. Here, all the PeakFind
% boxes have to be deleted.
%
% see also: PeakFind
for iP = 1:numel(PFs)
    delete(PFs(iP));
end

function FittingFinished(src, TableHandle)
% Callback function that the fitting process is finished. Update the data
% table.
plot(src);
D = get(TableHandle, 'Data');
L = D(:, 1) == src.UserData(1) & D(:, 2) == src.UserData(2) & ...
    D(:, 3) == src.UserData(3);
if isempty(src.FitResult)
    D(L, 15:17) = NaN(1, 3);
else
    D(L, 15) = src.FitResult.x0;
    D(L, 16) = src.FitResult.y0;
    D(L, 17) = src.FitResult.A;
end
D(L, 18) = src.x0-src.Width/2;
D(L, 19) = src.y0-src.Height/2;
D(L, 20) = src.Width;
D(L, 21) = src.Height;

set(TableHandle, 'Data', D);

function WriteValuesToFile(src, evt, CrystalHandles, TableHandle)
% Write the table data to a file.

% get latest data of the handles structure
CrystalHandles = guidata(CrystalHandles.Win_CrystalModule);
% open file save dialog
[FileName, PathName] = uiputfile({'*.txt', 'Text File (*.txt)'}, 'Select file to save to ...', CrystalHandles.Settings.ExportDataPath);
if isempty(FileName) || isempty(PathName) || all(FileName == 0) || all(PathName == 0)
    return;
end
% store the path in the handles structure to reopen the dialog at this path
CrystalHandles.Settings.ExportDataPath = PathName;

data = get(TableHandle, 'Data');
Headers = get(TableHandle, 'ColumnName');
fid = fopen(fullfile(PathName, FileName), 'w');
for iH = 1:numel(Headers)
    fprintf(fid, '%s\t', Headers{iH});
end
fprintf(fid, '\n');

if ~iscell(data)
    data = num2cell(data);
end

for iR = 1:size(data, 1)
    for iC = 1:size(data, 2)
        if isnumeric(data{iR, iC})
            if mod(data{iR, iC}, floor(data{iR, iC})) == 0
                fprintf(fid, '%d\t', data{iR, iC});
            else
                fprintf(fid, '%1.6f\t', data{iR, iC});
            end
        elseif ischar(data{iR, iC})
            fprintf(fid, '%s\t', data{iR, iC});
        end
    end
    fprintf(fid, '\n');
end

fclose(fid); 
guidata(CrystalHandles.Win_CrystalModule, CrystalHandles);