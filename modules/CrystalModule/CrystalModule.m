function varargout = CrystalModule(varargin)
% This file, together with CrystalModule.fig, is the UI for the Crystal
% Module.
% 
% Usage:
%     varargout = CrystalModule(MainAxesHandle, CrystalFolder, Btn, TBxUI)
% 
% Input: 
%     MainAxesHandle ... handle to main axes
%     CrystalFolder ... path to Crystals folder
%     Btn ... handle to button which was used to call the CrystalModule
%     TBxUI ... any UI element of the Toolbox Module
%
% Output:
%     varargout ... default GUIDE output
%
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Last Modified by GUIDE v2.5 09-May-2019 16:17:26

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @CrystalModule_OpeningFcn, ...
                   'gui_OutputFcn',  @CrystalModule_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before CrystalModule is made visible.
function CrystalModule_OpeningFcn(hObject, eventdata, handles, varargin)
% Choose default command line output for CrystalModule
handles.output = hObject;

set(get(varargin{1}, 'Parent'), 'Pointer', 'watch');

% store axes handle in handles structure
handles.MainAxes = varargin{1};

% store the path to the Crystals folder in the handles structure
handles.CrystalsPath = varargin{2};

% store the button used to call the module in the handles structure
handles.CallCrystalModuleButton = varargin{3};
% disable this button, to avoid multiple start calls to the module
set(handles.CallCrystalModuleButton, 'Enable', 'off');

% the settings for this module are stored in the same directory as the file
% LoadFileModule.m. Store its path in the handles structure to reuse it.
handles.SettingsPath = fullfile(fileparts(mfilename('fullpath')), 'Settings.mat');

% read out settings if available
if exist(handles.SettingsPath, 'file') == 2
    load(handles.SettingsPath);
    % store the settings in the handles structure
    handles.Settings = SettingsCrystalModule;
else % use some default
    handles.Settings = [];
    handles.Settings.ImportCrystalPath = pwd;
    handles.Settings.ExportDataPath = pwd;
    Pos = get(hObject, 'Position');
	Pos(4) = 628;
    set(hObject, 'Position', Pos);
end

% Read out crystals from the Crystals folder and add them to the handles
% structure:
% handles.Crystals is a cell containing information about the crystals:
% handles.Crystals{i,  1} ... GIDVis_Crystal
% handles.Crystals{i,  2} ... Crystal_Peak_New handle
% handles.Crystals{i,  3} ... String in hkl textbox
% handles.Crystals{i,  4} ... Value of Peak CB
% handles.Crystals{i,  5} ... Value of DS CB
% handles.Crystals{i,  6} ... Value of Label CB
% handles.Crystals{i,  7} ... Value of Struc. Fac. CB
% handles.Crystals{i,  8} ... String in increment textbox
% handles.Crystals{i,  9} ... String in SF multiplier textbox
% handles.Crystals{i, 10} ... Value of PM_SFProportionality
% handles.Crystals{i, 11} ... String in horizontal label offset textbox
% handles.Crystals{i, 12} ... String in vertical label offset textbox

% find mat files in the folder defined by handles.CrystalsPath
MatFiles = dir(fullfile(handles.CrystalsPath, '*.mat'));
if isempty(MatFiles) % no crystals found, create default one
    DefaultCrystal = GIDVis_Crystal;
    
    % save the crystal in the handles structure
    handles.Crystals{1, 1} = DefaultCrystal;
    handles.Crystals{1, 2} = Crystal_Peak_New(handles.MainAxes, handles.Crystals{1, 1}, ReadOutHKLValues(get(handles.TB_hklValues, 'String'), handles.Crystals{1, 1}), ReadOutOrientation(handles));
    handles.Crystals{1, 3} = get(handles.TB_hklValues, 'String');
    handles.Crystals{1, 4} = get(handles.CB_Peaks, 'Value');
    handles.Crystals{1, 5} = get(handles.CB_DSRing, 'Value');
    handles.Crystals{1, 6} = get(handles.CB_Label, 'Value');
    handles.Crystals{1, 7} = get(handles.CB_StructureFactor, 'Value');
    handles.Crystals{1, 8} = get(handles.TB_Increment, 'String');
    handles.Crystals{1, 9} = get(handles.SF_Multiplier, 'String');
    handles.Crystals{1, 10} = get(handles.PM_SFProportionality, 'Value');
    handles.Crystals{1, 11} = get(handles.TB_LabelOffsetHorizontal, 'String');
    handles.Crystals{1, 12} = get(handles.TB_LabelOffsetVertical, 'String');
else
    axes(handles.MainAxes);
    for iF = 1:numel(MatFiles)
        load(fullfile(handles.CrystalsPath, MatFiles(iF).name));
        handles.Crystals(iF, 1:numel(SaveCrystal)) = SaveCrystal;
        if ~isequal(handles.Crystals{iF, 1}.Name, handles.Crystals{iF, 2}.Name)
            delete(handles.Crystals{iF, 2});
            hkl = ReadOutHKLValues(handles.Crystals{iF, 3}, handles.Crystals{iF, 2});
            handles.Crystals{iF, 2} = Crystal_Peak_New(handles.MainAxes, ...
                handles.Crystals{iF, 1}, hkl, [0 0 1]);
            warning('GIDVis:CrystalModule:RestoringCrystal', ...
                'Problems occured while loading ''%s''. Tried restoring.', handles.Crystals{iF, 1}.Name);
        end
    end
end

% sort the crystals by name and set the string in the combobox
Crs = [handles.Crystals{:, 1}];
[~, idx] = sort(cellfun(@lower, {Crs.Name}, 'UniformOutput', false));
handles.Crystals = handles.Crystals(idx, :);
Crs = Crs(idx);
set(handles.PM_CrystalSelection, 'String', {Crs.Name});

% create the menus for the buttons to change the plot properties
[handles.MarkerContextMenu, handles.DSRingContextMenu, ...
    handles.LabelContextMenu, handles.SFRingContextMenu] = ...
    CreateContextMenus(hObject);

% add a listener to the axes in varargin{1}. When the axes gets destroyed 
% (e.g. because the figure containing the axes gets closed), the callback 
% will be executed (and closes this window).
% Store the listener in the handles structure to remove it when this window    
% gets closed by user, otherwise the callback is executed even when the
% LoadFileModule window does not exist anymore.
handles.MainAxesDestroyedListener = addlistener(varargin{1}, 'ObjectBeingDestroyed', @(src, evt) MainAxesDestroyed_Callback(src, evt, handles.Win_CrystalModule));

% add a listener to the Space selection popup menu in the toolbox
GD = guidata(varargin{4});
handles.SpaceListener = addlistener(GD.PM_Space, 'Value', 'PostSet', ...
    @(src, evt) SpaceChanged_Callback(GD.PM_Space, hObject));

% change the space of the crystals if necessary
% in case space is pixel, we don't want to change
Spaces = get(GD.PM_Space, 'String');
Value = get(GD.PM_Space, 'Value');
if strcmpi(Spaces{Value}, 'polar') && ~strcmpi(Spaces{Value}, 'pixel')
    for iCr = 1:size(handles.Crystals, 1)
        handles.Crystals{iCr, 2}.ChangeSpace(Spaces{Value});
    end
end

% store toolbox module handle in handles structure
handles.ToolboxModule = varargin{4};
% Update handles structure
guidata(hObject, handles);

CrystalSelectionChanged(handles);

function SpaceChanged_Callback(SpacePopup, CrystalModuleHObject)
% exectues when the space in the toolbox module gets changed
handles = guidata(CrystalModuleHObject);
Space = ToolboxModule('GetSelectedSpace', handles.ToolboxModule);
if strcmpi(Space, 'q') || strcmpi(Space, 'polar')
    for iCr = 1:size(handles.Crystals, 1)
        handles.Crystals{iCr, 2}.ChangeSpace(Space);
    end
end

function Ori = ReadOutOrientation(handles)
% returns the orientation in a vector
Str = get(handles.TB_Orientation, 'String');
Str = strrep(Str, ',', ' ');
Ori = str2num(Str);

function ToggleSFUIControls(handles)
% get index of selected crystal
Index = get(handles.PM_CrystalSelection, 'Value');

% if no elements are in the UnitCell, no structure factor can be
% calculated. Furthermore, the molviewer cannot be called
% --> disable the corresponding UI elements
if numel(handles.Crystals{Index, 1}.UnitCell) == 0
    set(handles.CB_StructureFactor, 'Value', 0);
    set(handles.CB_StructureFactor, 'Enable', 'off');
    set(handles.SF_Multiplier, 'Enable', 'off');
    set(handles.PM_SFProportionality, 'Enable', 'off');
    set(handles.Btn_StructureFactorProperties, 'Enable', 'off');
    set(handles.ET_SFMultiplier, 'Enable', 'off');
    set(handles.ET_SFProportionality, 'Enable', 'off');
    set(handles.Btn_MolViewer, 'Enable', 'off');
    set(handles.CB_SampleRotation, 'Enable', 'off');
else
    % enable UI elements
    set(handles.CB_StructureFactor, 'Enable', 'on');
    set(handles.SF_Multiplier, 'Enable', 'on');
    set(handles.PM_SFProportionality, 'Enable', 'on');
    set(handles.Btn_StructureFactorProperties, 'Enable', 'on');
    set(handles.ET_SFMultiplier, 'Enable', 'on');
    set(handles.ET_SFProportionality, 'Enable', 'on');
    set(handles.Btn_MolViewer, 'Enable', 'on');
    set(handles.CB_SampleRotation, 'Enable', 'on');
end

function CrystalSelectionChanged(handles)
% when the selected crystal changes

% get index of selected crystal
Index = get(handles.PM_CrystalSelection, 'Value');

% update the UI controls for the Structure Factor
ToggleSFUIControls(handles);

% load the data of the selected crystal in the UI elements
CP = handles.Crystals{Index, 2};
Data = {CP(1).a; CP(1).b; CP(1).c; CP(1).alpha; CP(1).beta; CP(1).gamma};
Data = cellfun(@(x) sprintf('%1.5f', x), Data, 'UniformOutput', 0);
set(handles.UIT_RealSpaceLattice, 'Data', Data);
set(handles.UIP_SelectedCrystal, 'Title', ['Properties of ', handles.Crystals{Index, 1}.Name]);
set(handles.TB_hklValues, 'String', handles.Crystals{Index, 3});
set(handles.CB_Peaks, 'Value', handles.Crystals{Index, 4});
set(handles.CB_DSRing, 'Value', handles.Crystals{Index, 5});
set(handles.CB_Label, 'Value', handles.Crystals{Index, 6});
set(handles.CB_StructureFactor, 'Value', handles.Crystals{Index, 7});
set(handles.TB_Orientation, 'String', sprintf('%d, %d, %d', CP(1).Orientation));
set(handles.TB_Increment, 'String', handles.Crystals{Index, 8});
set(handles.SF_Multiplier, 'String', handles.Crystals{Index, 9});
set(handles.PM_SFProportionality, 'Value', handles.Crystals{Index, 10});
set(handles.TB_LabelOffsetHorizontal, 'String', handles.Crystals{Index, 11});
set(handles.TB_LabelOffsetVertical, 'String', handles.Crystals{Index, 12});
set(handles.CB_SampleRotation, 'Value', CP(1).IncludeSampleRotation);
if CP.PlusMinusqxy == 1
    set(handles.RB_PlusQXY, 'Value', 1);
    set(handles.RB_MinusQXY, 'Value', 0);
else
    set(handles.RB_PlusQXY, 'Value', 0);
    set(handles.RB_MinusQXY, 'Value', 1);
end
if strcmp(CP.OverallVisibility, 'on')
    set(handles.CB_OverallVisibility, 'Value', 1);
else
    set(handles.CB_OverallVisibility, 'Value', 0);
end
CB_OverallVisibility_Callback(handles.CB_OverallVisibility, [], handles);

% if crystal contains atoms in UnitCell, unit cell cannot be changed,
% disable UI elements
Cr = handles.Crystals{Index, 1};
Ctrls = {'UIT_RealSpaceLattice', ...
    'Btn_a_decrease', 'Btn_a_increase', ...
    'Btn_b_decrease', 'Btn_b_increase', ...
    'Btn_c_decrease', 'Btn_c_increase', ...
    'Btn_alpha_decrease', 'Btn_alpha_increase', ...
    'Btn_beta_decrease', 'Btn_beta_increase', ...
    'Btn_gamma_decrease', 'Btn_gamma_increase'};

EnableStr = ConvertToOnOff(~numel(Cr.UnitCell));
for iCtrl = 1:numel(Ctrls)
    eval(['set(handles.', Ctrls{iCtrl}, ', ''Enable'', ''', EnableStr, ''');']);
end

guidata(handles.Win_CrystalModule, handles);


function Str = ConvertToOnOff(Val)
% Turns bool values to 'on' or 'off'
if Val
    Str = 'on';
else
    Str = 'off';
end

function MainAxesDestroyed_Callback(src, evt, HandleToModuleWindow)
% close module window
close(HandleToModuleWindow);

% --- Outputs from this function are returned to the command line.
function varargout = CrystalModule_OutputFcn(hObject, eventdata, handles) 
% Get default command line output from handles structure
varargout{1} = handles.output;

% restore the window position
if isfield(handles.Settings, 'WindowPosition')
    RestorePositionOrDefault(hObject, handles.Settings.WindowPosition, ...
        'center');
end
set(hObject, 'Visible', 'on');

set(get(handles.MainAxes, 'Parent'), 'Pointer', 'arrow');


% --- Executes on selection change in PM_CrystalSelection.
function PM_CrystalSelection_Callback(hObject, eventdata, handles)
CrystalSelectionChanged(handles)

% --- Executes during object creation, after setting all properties.
function PM_CrystalSelection_CreateFcn(hObject, eventdata, handles)
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Btn_AddCrystal.
function Btn_AddCrystal_Callback(hObject, eventdata, handles)
% User wants to add a crystal.
% ask for name
CrystalName = inputdlg('Please enter a name for the crystal:');
if isempty(CrystalName); return; end
% check if this name is already used.
Crs = [handles.Crystals{:, 1}];
if any(strcmpi({Crs.Name}, CrystalName))
    msgbox('A crystal with this name already exists. Cancelling.', 'modal');
    return;
end
% Create crystal
Cr = GIDVis_Crystal;
Cr.Name = CrystalName{:};
% add crystal to handles structure
handles.Crystals{end+1, 1} = Cr;
% create some defaults for the UI elements
HKL = ReadOutHKLValues('-2,-2,-2:2,2,2', Cr);
handles.Crystals{end, 2} = Crystal_Peak_New(handles.MainAxes, Cr, HKL, ReadOutOrientation(handles));
handles.Crystals{end, 3} = '-2,-2,-2:2,2,2';
handles.Crystals{end, 4} = 1;
handles.Crystals{end, 5} = 0;
handles.Crystals{end, 6} = 1;
handles.Crystals{end, 7} = 0;
handles.Crystals{end, 8} = 0.1;
handles.Crystals{end, 9} = 0.01;
handles.Crystals{end, 10} = 1;
handles.Crystals{end, 11} = 0;
handles.Crystals{end, 12} = 0;

% adapt the space of the Crystal_Peak_New object
Space = ToolboxModule('GetSelectedSpace', handles.ToolboxModule);
if ~strcmpi(Space, 'pixel')
    ChangeSpace(handles.Crystals{end, 2}, Space);
end

% resort the crystals by name and set string of combobox
Crs = [handles.Crystals{:, 1}];
[~, idx] = sort(cellfun(@lower, {Crs.Name}, 'UniformOutput', false));
handles.Crystals = handles.Crystals(idx, :);
Crs = Crs(idx);
set(handles.PM_CrystalSelection, 'String', {Crs.Name});
% set the value of the crystal selection combobox to the new crystal
set(handles.PM_CrystalSelection, 'Value', find(ismember({Crs.Name}, Cr.Name)));

guidata(hObject, handles);
CrystalSelectionChanged(handles);

% --- Executes on button press in Btn_DeleteCrystal.
function Btn_DeleteCrystal_Callback(hObject, eventdata, handles)
% user wants to delete selected crystal
if size(handles.Crystals, 1) == 1
    msgbox('You cannot delete all crystals! Close the ''Crystal Module'' window to remove things plotted in the main axes.');
    return;
end
% index of selected crystal
Index = get(handles.PM_CrystalSelection, 'Value');
% selected crystal
Cr = handles.Crystals{Index, 1};
Choice = questdlg(['Do you really want to delete Crystal "', Cr.Name, '"?'], 'Delete?', 'Yes', 'No', 'Yes');
if ~isempty(Choice) && strcmp(Choice, 'Yes')
    % user really wants to delete crystal
    % delete the *.mat file where the crystal is saved
    FN = CreateValidFileName(fullfile(handles.CrystalsPath, [Cr.Name, '.mat']));
    if exist(FN, 'file') == 2
        delete(FN);
    end
    % set value of select crystal combobox to 1 to avoid having a value
    % larger than the number of elements in the box
    set(handles.PM_CrystalSelection, 'Value', 1);
    % remove the plotted things
    delete(handles.Crystals{Index, 2});
    % remove the crystal from the handles structure
    handles.Crystals(Index, :) = [];
    guidata(hObject, handles);
    % remove the crystal from the crystal selection combobox
    Str = get(handles.PM_CrystalSelection, 'String');
    Str(Index) = [];
    set(handles.PM_CrystalSelection, 'String', Str);
    CrystalSelectionChanged(handles);
end

% --- Executes on button press in Btn_ImportCrystal.
function Btn_ImportCrystal_Callback(hObject, eventdata, handles)
% import crystal (only res file allowed)
[FileName, PathName] = uigetfile({'*.res', 'res file'}, 'Select res file', handles.Settings.ImportCrystalPath);
if all(FileName == 0) || all(PathName == 0); return; end % cancelled or invalid selection
handles.Settings.ImportCrystalPath = PathName; % store path in handles structure, to reuse later

% import crystal from file
Cr = GIDVis_Crystal(fullfile(PathName, FileName));
% ask for name, use the TITL from res file as suggestion
CrystalName = inputdlg('Please enter a name for the crystal:', 'Name', 1, {Cr.Name});
if isempty(CrystalName); return; end
Cr.Name = CrystalName{:};
% add crystal to handles structure
handles.Crystals{end+1, 1} = Cr;
% create some defaults for the UI elements
HKL = ReadOutHKLValues('-2,-2,-2:2,2,2', Cr);
handles.Crystals{end, 2} = Crystal_Peak_New(handles.MainAxes, Cr, HKL, ReadOutOrientation(handles));
handles.Crystals{end, 3} = '-2,-2,-2:2,2,2';
handles.Crystals{end, 4} = 1;
handles.Crystals{end, 5} = 0;
handles.Crystals{end, 6} = 1;
handles.Crystals{end, 7} = 0;
handles.Crystals{end, 8} = 0.1;
handles.Crystals{end, 9} = 0.01;
handles.Crystals{end, 10} = 1;
handles.Crystals{end, 11} = 0;
handles.Crystals{end, 12} = 0;

% adapt the space of the Crystal_Peak_New object
Space = ToolboxModule('GetSelectedSpace', handles.ToolboxModule);
if ~strcmpi(Space, 'pixel')
    ChangeSpace(handles.Crystals{end, 2}, Space);
end

% resort the crystals by name and set string of combobox
Crs = [handles.Crystals{:, 1}];
[~, idx] = sort(cellfun(@lower, {Crs.Name}, 'UniformOutput', false));
handles.Crystals = handles.Crystals(idx, :);
Crs = Crs(idx);
set(handles.PM_CrystalSelection, 'String', {Crs.Name});
% set the value of the crystal selection combobox to the new crystal
set(handles.PM_CrystalSelection, 'Value', find(ismember({Crs.Name}, Cr.Name)));

guidata(hObject, handles);

CrystalSelectionChanged(handles);

% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu3.
function popupmenu3_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function popupmenu3_CreateFcn(hObject, eventdata, handles)
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu4.
function popupmenu4_Callback(hObject, eventdata, handles)
% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu4 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu4


% --- Executes during object creation, after setting all properties.
function popupmenu4_CreateFcn(hObject, eventdata, handles)
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu5.
function popupmenu5_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function popupmenu5_CreateFcn(hObject, eventdata, handles)
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when entered data in editable cell(s) in UIT_RealSpaceLattice.
function UIT_RealSpaceLattice_CellEditCallback(hObject, evt, handles)
% hObject    handle to UIT_RealSpaceLattice (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)

% the table uses column format char, so that the number can be formatted as
% wished
set(hObject, 'ColumnFormat', {'char'});
% get selected crystal
CP = handles.Crystals{get(handles.PM_CrystalSelection, 'Value'), 2};
% get data from table
Data = get(hObject, 'Data');
% update data --> use sprintf to format number
Data{evt.Indices(:, 1), evt.Indices(:, 2)} = sprintf('%1.5f', str2double(evt.EditData));
% create a new crystal with the updated data
NewCrystal = GIDVis_Crystal(str2double(Data{1, 1}), str2double(Data{2, 1}), str2double(Data{3, 1}), ...
    str2double(Data{4, 1}), str2double(Data{5, 1}), str2double(Data{6, 1}), CP(1).Name);
% set the data of the table to the updated one
set(hObject, 'Data', Data);
% update the drawings for the crystal
ChangeCrystal(CP, NewCrystal);


% --- Executes when entered data in editable cell(s) in UIT_ReciprocalSpaceLattice.
function UIT_ReciprocalSpaceLattice_CellEditCallback(hObject, eventdata, handles)

% --- Executes when user attempts to close Win_CrystalModule.
function Win_CrystalModule_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to Win_CrystalModule (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% delete the listener to the AxesDestroyed event. Otherwise it will still
% listen and try to execute the MainAxesDestroyed_Callback function, even
% though the Module1 window is already closed.
delete(handles.MainAxesDestroyedListener);

% delete the listener to the changing of the space selection (same reason
% as for the MainAxesDestroyedListener).
delete(handles.SpaceListener);

% re-enable the button which is used to call the crystal module
if ishandle(handles.CallCrystalModuleButton)
    set(handles.CallCrystalModuleButton, 'Enable', 'on');
end

% store settings in CrystalModule folder
SettingsCrystalModule = handles.Settings;
SettingsCrystalModule.WindowPosition = get(hObject, 'Position');

% store loaded crystals in the Crystals folder, given by
% handles.CrystalsPath
if exist(handles.CrystalsPath, 'dir') ~= 7 % if directory does not exist, create it
    mkdir(handles.CrystalsPath);
end
Crystals = handles.Crystals;
for iCr = 1:size(Crystals, 1)
    SaveCrystal = Crystals(iCr, :);
    if isempty(SaveCrystal{2}); continue; end
    % in order to avoid error when no write access for file use try.
    try; save(CreateValidFileName(fullfile(handles.CrystalsPath, [SaveCrystal{1}.Name, '.mat'])), 'SaveCrystal'); end
end
% in order to avoid error when no write access for file use try.
try; save(handles.SettingsPath, 'SettingsCrystalModule'); end

% remove drawings from main axes
for iCr = 1:size(handles.Crystals, 1)
    delete(handles.Crystals{iCr, 2})
end

% close the figure
delete(hObject);


function TB_hklValues_Callback(hObject, eventdata, handles)
% index of selected crystal
Index = get(handles.PM_CrystalSelection, 'Value');
% get the crystal_Peaks of the crystal
CP = handles.Crystals{Index, 2};
% read out new HKL values
hkl = ReadOutHKLValues(get(hObject, 'String'), handles.Crystals{Index});
% update the drawings with the new HKL values
ChangeHKL(CP, hkl, handles.MainAxes);
% store the new values in the handles structure
% handles.Crystals{Index, 2} = CP;
handles.Crystals{Index, 3} = get(hObject, 'String');
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function TB_hklValues_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in CB_Peaks.
function CB_Peaks_Callback(hObject, eventdata, handles)
% toggle the visibility of the Bragg peaks and store updated values in
% handles structure
CP = handles.Crystals{get(handles.PM_CrystalSelection, 'Value'), 2};
CP.BraggVisible = ConvertToOnOff(get(handles.CB_Peaks, 'Value'));
% index of selected crystal
Index = get(handles.PM_CrystalSelection, 'Value');
handles.Crystals{Index, 4} = get(handles.CB_Peaks, 'Value');
guidata(hObject, handles);

% --- Executes on button press in CB_DSRing.
function CB_DSRing_Callback(hObject, eventdata, handles)
% toggle the visibility of the DS rings and store updated values in
% handles structure
CP = handles.Crystals{get(handles.PM_CrystalSelection, 'Value'), 2};
CP.DSVisible = ConvertToOnOff(get(handles.CB_DSRing, 'Value'));
% index of selected crystal
Index = get(handles.PM_CrystalSelection, 'Value');
handles.Crystals{Index, 5} = get(handles.CB_DSRing, 'Value');
guidata(hObject, handles);

% --- Executes on button press in CB_Label.
function CB_Label_Callback(hObject, eventdata, handles)
% toggle the visibility of the labels and store updated values in
% handles structure
CP = handles.Crystals{get(handles.PM_CrystalSelection, 'Value'), 2};
CP.LabelVisible = ConvertToOnOff(get(handles.CB_Label, 'Value'));
% index of selected crystal
Index = get(handles.PM_CrystalSelection, 'Value');
handles.Crystals{Index, 6} = get(handles.CB_Label, 'Value');
guidata(hObject, handles);

function hkl = ReadOutHKLValues(Str, Cr)
% creates HKL combinations from Str.
hkl = NaN(1, 3);
if ~iscell(Str); Str = cellstr(Str); end
for iL = 1:size(Str, 1)
    if isempty(Str{iL})
        continue
    end
    parts = strread(Str{iL}, '%s', 'delimiter', ':');
    hkl_temp = NaN(1, 3);
    for iP = 1:numel(parts)
%         pparts = textscan(parts{iP}, '%f, %f, %f');
%         hkl_temp(iP, :) = cell2mat(pparts);
        pparts = str2num(parts{iP});
        if numel(pparts) ~= 3
            continue;
        end
        hkl_temp(iP, :) = pparts;
    end
    if size(hkl_temp, 1) > 1
        hkl_temp = Cr.CreateHKLCombinations(hkl_temp(1, 1):hkl_temp(2, 1), hkl_temp(1, 2):hkl_temp(2, 2), hkl_temp(1, 3):hkl_temp(2, 3));
    end

    hkl(end+1:end+size(hkl_temp, 1), :) = hkl_temp;
end
hkl(1, :) = [];
% remove 0 0 0
[Found, ind] = ismember([0 0 0], hkl, 'rows');
if Found
    hkl(ind, :) = [];
end


% --- Executes on button press in Btn_MarkerProperties.
function Btn_MarkerProperties_Callback(hObject, eventdata, handles)
Cnt = findobj(get(handles.MarkerContextMenu, 'Children'), 'Tag', 'MirrorBraggPeaks');
CP = handles.Crystals{get(handles.PM_CrystalSelection, 'Value'), 2};
if CP.BraggPeaksMirrored
    set(Cnt, 'Checked', 'on');
else
    set(Cnt, 'Checked', 'off');
end
% calculate place for context menu and make it visible
OldUnits = get(hObject, 'Units');
OldParUnits = get(get(hObject, 'Parent'), 'Units');
set(hObject, 'Units', 'pixels');
set(get(hObject, 'Parent'), 'Units', 'pixels');
Pos = get(hObject, 'Position');
ParPos = get(get(hObject, 'Parent'), 'Position');
set(handles.MarkerContextMenu, 'Visible', 'on', 'Position', [Pos(1)+ParPos(1)+9 Pos(2)+ParPos(2)+6]);
set(hObject, 'Units', OldUnits);
set(get(hObject, 'Parent'), 'Units', OldParUnits);


% --- Executes on button press in Btn_LabelProperties.
function Btn_LabelProperties_Callback(hObject, eventdata, handles)
% calculate place for context menu and make it visible
OldUnits = get(hObject, 'Units');
OldParUnits = get(get(hObject, 'Parent'), 'Units');
set(hObject, 'Units', 'pixels');
set(get(hObject, 'Parent'), 'Units', 'pixels');
Pos = get(hObject, 'Position');
ParPos = get(get(hObject, 'Parent'), 'Position');
set(handles.LabelContextMenu, 'Visible', 'on', 'Position', [Pos(1)+ParPos(1)+9 Pos(2)+ParPos(2)+6]);
set(hObject, 'Units', OldUnits);
set(get(hObject, 'Parent'), 'Units', OldParUnits);


% --- Executes on button press in Btn_DSRingProperties.
function Btn_DSRingProperties_Callback(hObject, eventdata, handles)
% calculate place for context menu and make it visible
OldUnits = get(hObject, 'Units');
OldParUnits = get(get(hObject, 'Parent'), 'Units');
set(hObject, 'Units', 'pixels');
set(get(hObject, 'Parent'), 'Units', 'pixels');
Pos = get(hObject, 'Position');
ParPos = get(get(hObject, 'Parent'), 'Position');
set(handles.DSRingContextMenu, 'Visible', 'on', 'Position', [Pos(1)+ParPos(1)+9 Pos(2)+ParPos(2)+6]);
set(hObject, 'Units', OldUnits);
set(get(hObject, 'Parent'), 'Units', OldParUnits);


% --- Executes on button press in Btn_StructureFactorProperties.
function Btn_StructureFactorProperties_Callback(hObject, eventdata, handles)
% calculate place for context menu and make it visible
OldUnits = get(hObject, 'Units');
OldParUnits = get(get(hObject, 'Parent'), 'Units');
set(hObject, 'Units', 'pixels');
set(get(hObject, 'Parent'), 'Units', 'pixels');
Pos = get(hObject, 'Position');
ParPos = get(get(hObject, 'Parent'), 'Position');
set(handles.SFRingContextMenu, 'Visible', 'on', 'Position', [Pos(1)+ParPos(1)+9 Pos(2)+ParPos(2)+6]);
set(hObject, 'Units', OldUnits);
set(get(hObject, 'Parent'), 'Units', OldParUnits);

function TB_Orientation_Callback(hObject, eventdata, handles)
% index of selected crystal
Index = get(handles.PM_CrystalSelection, 'Value');
% Crystal_Peaks of crystal
CP = handles.Crystals{Index, 2};
% update the drawings for the new orientation and store value in handles
ChangeOrientation(CP, ReadOutOrientation(handles));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function TB_Orientation_CreateFcn(hObject, eventdata, handles)
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in CB_StructureFactor.
function CB_StructureFactor_Callback(hObject, eventdata, handles)
% toggle visibility of SF rings and store updated values in handles
% structure
CP = handles.Crystals{get(handles.PM_CrystalSelection, 'Value'), 2};
CP.SFVisible = ConvertToOnOff(get(handles.CB_StructureFactor, 'Value'));
% index of selected crystal
Index = get(handles.PM_CrystalSelection, 'Value');
handles.Crystals{Index, 7} = get(handles.CB_StructureFactor, 'Value');
guidata(hObject, handles);


% --------------------------------------------------------------------
function UIT_RealSpaceLattice_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to UIT_RealSpaceLattice (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on key press with focus on UIT_RealSpaceLattice and none of its controls.
function UIT_RealSpaceLattice_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to UIT_RealSpaceLattice (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)


% --- Executes when selected cell(s) is changed in UIT_RealSpaceLattice.
function UIT_RealSpaceLattice_CellSelectionCallback(hObject, eventdata, handles)
% hObject    handle to UIT_RealSpaceLattice (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) currently selecteds
% handles    structure with handles and user data (see GUIDATA)



function IncreaseDecreaseLatticeParameter(handles, Index, pm)
% this function changes the lattice parameter at position Index. It either
% adds the increment (pm = 1), or subtracts the increment (pm = -1).
Data = get(handles.UIT_RealSpaceLattice, 'Data');
Val = str2double(Data{Index, 1});
Increment = str2double(get(handles.TB_Increment, 'String'));
if isnan(Increment)
    msgbox('Please enter a valid number for the increment!');
    return;
end
Val = Val + pm*Increment;
Data{Index, 1} = sprintf('%1.5f', Val);
CP = handles.Crystals{get(handles.PM_CrystalSelection, 'Value'), 2};
NewCrystal = GIDVis_Crystal(str2double(Data{1, 1}), str2double(Data{2, 1}), str2double(Data{3, 1}), ...
    str2double(Data{4, 1}), str2double(Data{5, 1}), str2double(Data{6, 1}), CP(1).Name);
set(handles.UIT_RealSpaceLattice, 'Data', Data);
ChangeCrystal(CP, NewCrystal);

% --- Executes on button press in Btn_a_increase.
function Btn_a_increase_Callback(hObject, eventdata, handles)
IncreaseDecreaseLatticeParameter(handles, 1, 1);

% --- Executes on button press in Btn_a_decrease.
function Btn_a_decrease_Callback(hObject, eventdata, handles)
IncreaseDecreaseLatticeParameter(handles, 1, -1);

% --- Executes on button press in Btn_b_increase.
function Btn_b_increase_Callback(hObject, eventdata, handles)
IncreaseDecreaseLatticeParameter(handles, 2, 1);

% --- Executes on button press in Btn_b_decrease.
function Btn_b_decrease_Callback(hObject, eventdata, handles)
IncreaseDecreaseLatticeParameter(handles, 2, -1);


% --- Executes on button press in Btn_c_increase.
function Btn_c_increase_Callback(hObject, eventdata, handles)
IncreaseDecreaseLatticeParameter(handles, 3, 1);


% --- Executes on button press in Btn_c_decrease.
function Btn_c_decrease_Callback(hObject, eventdata, handles)
IncreaseDecreaseLatticeParameter(handles, 3, -1);


% --- Executes on button press in Btn_alpha_increase.
function Btn_alpha_increase_Callback(hObject, eventdata, handles)
IncreaseDecreaseLatticeParameter(handles, 4, 1);


% --- Executes on button press in Btn_alpha_decrease.
function Btn_alpha_decrease_Callback(hObject, eventdata, handles)
IncreaseDecreaseLatticeParameter(handles, 4, -1);


% --- Executes on button press in Btn_beta_increase.
function Btn_beta_increase_Callback(hObject, eventdata, handles)
IncreaseDecreaseLatticeParameter(handles, 5, 1);


% --- Executes on button press in Btn_beta_decrease.
function Btn_beta_decrease_Callback(hObject, eventdata, handles)
IncreaseDecreaseLatticeParameter(handles, 5, -1);


% --- Executes on button press in Btn_gamma_increase.
function Btn_gamma_increase_Callback(hObject, eventdata, handles)
IncreaseDecreaseLatticeParameter(handles, 6, 1);


% --- Executes on button press in Btn_gamma_decrease.
function Btn_gamma_decrease_Callback(hObject, eventdata, handles)
IncreaseDecreaseLatticeParameter(handles, 6, -1);



function TB_Increment_Callback(hObject, eventdata, handles)
% update the stored value for the increment
handles.Crystals{get(handles.PM_CrystalSelection, 'Value'), 8} = get(hObject, 'String');
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function TB_Increment_CreateFcn(hObject, eventdata, handles)
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function SF_Multiplier_Callback(hObject, eventdata, handles)
% check for valid number, if not --> return
if isnan(str2double(get(handles.SF_Multiplier, 'String')))
    return;
end
% index of selected crystal
Index = get(handles.PM_CrystalSelection, 'Value');
% Crystal_Peaks of selected crystal
CP = handles.Crystals{Index, 2};
% update CP with new multiplier and store in handles
ChangeSFMultiplier(CP, str2double(get(handles.SF_Multiplier, 'String')));
handles.Crystals{get(handles.PM_CrystalSelection, 'Value'), 9} = get(hObject, 'String');
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function SF_Multiplier_CreateFcn(hObject, eventdata, handles)
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when Win_CrystalModule is resized.
function Win_CrystalModule_ResizeFcn(hObject, eventdata, handles)
% hObject    handle to Win_CrystalModule (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
FigPos = get(hObject, 'Position');
UIP_CSPos = get(handles.UIP_CrystalSelection, 'Position');
UIP_CSPos(2) = FigPos(4)-125;
UIP_CSPos(3) = FigPos(3)-11;
set(handles.UIP_CrystalSelection, 'Position', UIP_CSPos);
UIP_SCPos = get(handles.UIP_SelectedCrystal, 'Position');
UIP_SCPos(3) = FigPos(3)-11;
UIP_SCPos(4) = FigPos(4)-135;
UIP_SCPos(UIP_SCPos < 0) = 0;
set(handles.UIP_SelectedCrystal, 'Position', UIP_SCPos);

UIP_qxyPMPos = get(handles.UIP_qxyPM, 'Position');
UIP_qxyPMPos(1) = UIP_SCPos(3)-212;
UIP_qxyPMPos(2) = UIP_SCPos(4)-70;
set(handles.UIP_qxyPM, 'Position', UIP_qxyPMPos);

CB_OverallVisPos = get(handles.CB_OverallVisibility, 'Position');
CB_OverallVisPos(1) = UIP_SCPos(3) - 208;
CB_OverallVisPos(2) = UIP_SCPos(4) - 101;
set(handles.CB_OverallVisibility, 'Position', CB_OverallVisPos);

UIP_BPPos = get(handles.UIP_BraggPeak, 'Position');
UIP_BPPos(1) = UIP_SCPos(3)-212;
UIP_BPPos(2) = UIP_SCPos(4)-158;
set(handles.UIP_BraggPeak, 'Position', UIP_BPPos);

UIP_DSPos = get(handles.UIP_DSRing, 'Position');
UIP_DSPos(1) = UIP_SCPos(3)-212;
UIP_DSPos(2) = UIP_SCPos(4)-218;
set(handles.UIP_DSRing, 'Position', UIP_DSPos);

UIP_LPos = get(handles.UIP_Label, 'Position');
UIP_LPos(1) = UIP_SCPos(3)-212;
UIP_LPos(2) = UIP_SCPos(4)-334;
set(handles.UIP_Label, 'Position', UIP_LPos);

UIP_SFPos = get(handles.UIP_SF, 'Position');
UIP_SFPos(1) = UIP_SCPos(3)-212;
UIP_SFPos(2) = UIP_SCPos(4)-483;
set(handles.UIP_SF, 'Position', UIP_SFPos);

Table_Pos = get(handles.UIT_RealSpaceLattice, 'Position');
Table_Pos(2) = UIP_SCPos(4)-149;
Table_Pos(3) = UIP_SCPos(3)-281;
Table_Pos(Table_Pos < 0) = 0;
set(handles.UIT_RealSpaceLattice, 'Position', Table_Pos);

Pos = get(handles.Btn_a_decrease, 'Position');
Pos(1) = UIP_SCPos(3)-271;
Pos(2) = UIP_SCPos(4)-46;
set(handles.Btn_a_decrease, 'Position', Pos);

Pos = get(handles.Btn_a_increase, 'Position');
Pos(1) = UIP_SCPos(3)-249;
Pos(2) = UIP_SCPos(4)-46;
set(handles.Btn_a_increase, 'Position', Pos);

Pos = get(handles.Btn_b_decrease, 'Position');
Pos(1) = UIP_SCPos(3)-271;
Pos(2) = UIP_SCPos(4)-66;
set(handles.Btn_b_decrease, 'Position', Pos);

Pos = get(handles.Btn_b_increase, 'Position');
Pos(1) = UIP_SCPos(3)-249;
Pos(2) = UIP_SCPos(4)-66;
set(handles.Btn_b_increase, 'Position', Pos);

Pos = get(handles.Btn_c_decrease, 'Position');
Pos(1) = UIP_SCPos(3)-271;
Pos(2) = UIP_SCPos(4)-87;
set(handles.Btn_c_decrease, 'Position', Pos);

Pos = get(handles.Btn_c_increase, 'Position');
Pos(1) = UIP_SCPos(3)-249;
Pos(2) = UIP_SCPos(4)-87;
set(handles.Btn_c_increase, 'Position', Pos);

Pos = get(handles.Btn_alpha_decrease, 'Position');
Pos(1) = UIP_SCPos(3)-271;
Pos(2) = UIP_SCPos(4)-107;
set(handles.Btn_alpha_decrease, 'Position', Pos);

Pos = get(handles.Btn_alpha_increase, 'Position');
Pos(1) = UIP_SCPos(3)-249;
Pos(2) = UIP_SCPos(4)-107;
set(handles.Btn_alpha_increase, 'Position', Pos);

Pos = get(handles.Btn_beta_decrease, 'Position');
Pos(1) = UIP_SCPos(3)-271;
Pos(2) = UIP_SCPos(4)-127;
set(handles.Btn_beta_decrease, 'Position', Pos);

Pos = get(handles.Btn_beta_increase, 'Position');
Pos(1) = UIP_SCPos(3)-249;
Pos(2) = UIP_SCPos(4)-127;
set(handles.Btn_beta_increase, 'Position', Pos);

Pos = get(handles.Btn_gamma_decrease, 'Position');
Pos(1) = UIP_SCPos(3)-271;
Pos(2) = UIP_SCPos(4)-148;
set(handles.Btn_gamma_decrease, 'Position', Pos);

Pos = get(handles.Btn_gamma_increase, 'Position');
Pos(1) = UIP_SCPos(3)-249;
Pos(2) = UIP_SCPos(4)-148;
set(handles.Btn_gamma_increase, 'Position', Pos);

Pos = get(handles.T_Increment, 'Position');
Pos(2) = UIP_SCPos(4)-178;
set(handles.T_Increment, 'Position', Pos);

Pos = get(handles.TB_Increment, 'Position');
Pos(2) = UIP_SCPos(4)-180;
if UIP_SCPos(3)-339 > 50
    Pos(3) = UIP_SCPos(3)-339;
end
set(handles.TB_Increment, 'Position', Pos);

Pos = get(handles.T_Orientation, 'Position');
Pos(2) = UIP_SCPos(4)-206;
set(handles.T_Orientation, 'Position', Pos);

Pos = get(handles.TB_Orientation, 'Position');
Pos(2) = UIP_SCPos(4)-208;
if UIP_SCPos(3)-339 > 50
    Pos(3) = UIP_SCPos(3)-339;
end
set(handles.TB_Orientation, 'Position', Pos);

Pos = get(handles.TB_hklValues, 'Position');
Pos(3) = UIP_SCPos(3)-235;
if UIP_SCPos(4)-254 > 30
    Pos(4) = UIP_SCPos(4)-254;
end
Pos(Pos < 0) = 0;
set(handles.TB_hklValues, 'Position', Pos);


% --- Executes on selection change in PM_SFProportionality.
function PM_SFProportionality_Callback(hObject, eventdata, handles)
% selected string in combobox
contents = cellstr(get(hObject,'String'));
% index of selected crystal
Index = get(handles.PM_CrystalSelection, 'Value');
% Crystal_Peaks of selected crystal
CP = handles.Crystals{Index, 2};
% update the CPs with the new proportionality and store the selected value
% in handles structure
ChangeSFProportionality(CP, lower(contents{get(hObject,'Value')}));
handles.Crystals{get(handles.PM_CrystalSelection, 'Value'), 10} = get(hObject, 'Value');
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function PM_SFProportionality_CreateFcn(hObject, eventdata, handles)
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function UpdateLabelOffset_Fcn(handles)
% called when the horizontal or vertical label offset changes
NewValHorizontal = str2double(get(handles.TB_LabelOffsetHorizontal, 'String'));
NewValVertical = str2double(get(handles.TB_LabelOffsetVertical, 'String'));
if isnan(NewValHorizontal) || isnan(NewValVertical)
    return;
end
% get selected crystal and Crystal_Peaks
Index = get(handles.PM_CrystalSelection, 'Value');
CP = handles.Crystals{Index, 2};

% update the labels
CP.LabelOffset = [NewValHorizontal NewValVertical];

% store new values in handles structure
handles.Crystals{Index, 11} = NewValHorizontal;
handles.Crystals{Index, 12} = NewValVertical;

guidata(handles.Win_CrystalModule, handles);


function TB_LabelOffsetHorizontal_Callback(hObject, eventdata, handles)
UpdateLabelOffset_Fcn(handles);

% --- Executes during object creation, after setting all properties.
function TB_LabelOffsetHorizontal_CreateFcn(hObject, eventdata, handles)
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function TB_LabelOffsetVertical_Callback(hObject, eventdata, handles)
UpdateLabelOffset_Fcn(handles);

% --- Executes during object creation, after setting all properties.
function TB_LabelOffsetVertical_CreateFcn(hObject, eventdata, handles)
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Btn_ExportData.
function Btn_ExportData_Callback(hObject, eventdata, handles)
% get crystal and Crystal_Peaks
Index = get(handles.PM_CrystalSelection, 'Value');
CP = handles.Crystals{Index, 2};

set(hObject, 'Enable', 'off');
set(ancestor(hObject, 'figure'), 'Pointer', 'watch');
drawnow
CrystalDataView(CP, hObject, handles.ToolboxModule);
set(ancestor(hObject, 'figure'), 'Pointer', 'arrow');
set(hObject, 'Enable', 'on');

% --- Executes when selected object is changed in UIP_qxyPM.
function UIP_qxyPM_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in UIP_qxyPM 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)

% index of selected crystal
Index = get(handles.PM_CrystalSelection, 'Value');
% Crystal_Peaks of selected crystal
CP = handles.Crystals{Index, 2};
switch get(eventdata.NewValue, 'Tag')
    case 'RB_PlusQXY'
        % update CP with new plus/minus qxy
        ChangePlusMinusqxy(CP, 1);
    case 'RB_MinusQXY'
        % update CP with new plus/minus qxy
        ChangePlusMinusqxy(CP, -1);
end


% --- Executes on button press in Btn_MolViewer.
function Btn_MolViewer_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_MolViewer (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% get crystal and Crystal_Peaks
Index = get(handles.PM_CrystalSelection, 'Value');
CP = handles.Crystals{Index, 2};

set(hObject, 'Enable', 'off');
set(ancestor(hObject, 'figure'), 'Pointer', 'watch');
drawnow
f = plot(CP);
plotPlane(CP, CP.Orientation, 'Parent', findobj(f, 'Type', 'axes'), ...
    'FaceColor', 'r', 'FaceAlpha', 0.6, 'EdgeColor', 'none');
set(ancestor(hObject, 'figure'), 'Pointer', 'arrow');
set(hObject, 'Enable', 'on');


% --------------------------------------------------------------------
function FilterHKLValues_Cntxt_Callback(hObject, eventdata, handles)
% get crystal and Crystal_Peaks
Index = get(handles.PM_CrystalSelection, 'Value');
CP = handles.Crystals{Index, 2};
% calculate q values and structure factor
q = CP.q;
SF = CP.StructureFactor;

% create inputdlg
prompt = {'q_{xy} \geq', 'q_{xy} \leq', 'q_z \geq', 'q_z \leq', ...
    'q \geq', 'q \leq', '\midSF\mid \geq'};
dlg_title = 'Filter hkl';
num_lines = 1;
defAns = {'0', 'inf', '0', 'inf', '0', 'inf', '0'};
options.Resize = 'off';
options.WindowStyle = 'modal';
options.Interpreter = 'tex';

% if no structure factor can be calculated, don't ask for the SF limit
if isempty(SF)
    answer = inputdlg(prompt(1:end-1), dlg_title, num_lines, defAns(1:end-1), options);
else
    answer = inputdlg(prompt, dlg_title, num_lines, defAns, options);
end

if isempty(answer)
    return;
end

[val, status] = cellfun(@str2num, answer, 'UniformOutput', false);
if any([status{:}] == 0)
    msgbox('Incorrect value entered. Aborting.', 'modal')
end

q_xy = sqrt(q(:, 1).^2+q(:, 2).^2);
q_z = q(:, 3);
q = sqrt(q_xy.^2 + q_z.^2);

L = q_xy >= val{1} & q_xy <= val{2} & q_z >= val{3} & q_z <= val{4} ...
    & q > val{5} & q < val{6};
if ~isempty(SF)
    L = L & abs(SF) >= val{7};
end

NewHKLs = CP.hkl(L, :);
set(handles.TB_hklValues, 'String', sprintf('%d, %d, %d\n', NewHKLs'));

TB_hklValues_Callback(handles.TB_hklValues, [], handles);



% --------------------------------------------------------------------
function TB_hklValues_Cntxt_Callback(hObject, eventdata, handles)

% --------------------------------------------------------------------
function ApplyReflectionCondition_Cntxt_Callback(hObject, eventdata, handles)
% get crystal and Crystal_Peaks
Index = get(handles.PM_CrystalSelection, 'Value');
CP = handles.Crystals{Index, 2};

RefC = inputdlg('Please enter the reflection condition', ...
    'Reflection Condition', 1);

if isempty(RefC)
    return;
end

% split the RefC, in case user has entered semicolon delimited reflection
% conditions
RefC = split(RefC{1}, ';');

% get hkl values
HKL = CP.hkl;

% apply the condition
NewHKLs = ApplyReflectionConditions(HKL, RefC);

% put them in the text box
set(handles.TB_hklValues, 'String', sprintf('%d, %d, %d\n', NewHKLs'));

TB_hklValues_Callback(handles.TB_hklValues, [], handles);


% --------------------------------------------------------------------
function PasteFromPowderCell_Cntxt_Callback(hObject, eventdata, handles)
% hObject    handle to PasteFromPowderCell_Cntxt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data = clipboard('paste');
if isempty(data)
    msgbox('The clipboard is empty. Copy the powder diffraction hkl list from PowderCell first, then use this function.', 'Import not successful', 'modal')
    return;
end
ind = strfind(data, '_');
if isempty(ind)
    msgbox('Error during import. Copy the powder diffraction hkl list from PowderCell first, then use this function.', 'Import not successful', 'modal')
    return;
end
data = data(ind(end)+2:end);
M = str2num(data);
if isempty(M)
    msgbox('Error during import. Copy the powder diffraction hkl list from PowderCell first, then use this function.', 'Import not successful', 'modal')
    return;
end
if size(M, 2) ~= 9
    msgbox('Error during import. Copy the powder diffraction hkl list from PowderCell first, then use this function.', 'Import not successful', 'modal')
    return;
end
    
NewHKLs = M(:, 1:3);
Str = sprintf('%d, %d, %d\n', NewHKLs');
set(handles.TB_hklValues, 'String', Str(1:end-1));

TB_hklValues_Callback(handles.TB_hklValues, [], handles);


% --- Executes on button press in CB_OverallVisibility.
function CB_OverallVisibility_Callback(hObject, eventdata, handles)
% hObject    handle to CB_OverallVisibility (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of CB_OverallVisibility
% index of selected crystal
Index = get(handles.PM_CrystalSelection, 'Value');
% Crystal_Peaks of selected crystal
CP = handles.Crystals{Index, 2};

Parents = {'UIP_BraggPeak', 'UIP_DSRing', 'UIP_Label', 'UIP_SF'};

if get(hObject, 'Value') == 1
    CP.OverallVisibility = 'on';
    for iP = 1:numel(Parents)
        eval(['Children = get(handles.', Parents{iP}, ', ''Children'');']);
        set(Children, 'Enable', 'on');
    end
    ToggleSFUIControls(handles);
else
    CP.OverallVisibility = 'off';
    for iP = 1:numel(Parents)
        eval(['Children = get(handles.', Parents{iP}, ', ''Children'');']);
        set(Children, 'Enable', 'off');
    end
end

% --- Executes on button press in Btn_CreateCrystal.
function Btn_CreateCrystal_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_CreateCrystal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
answer = inputdlg('Please enter a name for the crystal:', 'Name');
if (iscell(answer) && isempty(answer)) || isempty(answer{:})
    return;
end
Cr = GIDVis_Crystal(5, 5, 5, 90, 90, 90, answer{:});
f = Cr.EditUnitCell();
uiwait(f);

% add crystal to handles structure
handles.Crystals{end+1, 1} = Cr;
% create some defaults for the UI elements
HKL = ReadOutHKLValues('-2,-2,-2:2,2,2', Cr);
handles.Crystals{end, 2} = Crystal_Peak_New(handles.MainAxes, Cr, HKL, ReadOutOrientation(handles));
handles.Crystals{end, 3} = '-2,-2,-2:2,2,2';
handles.Crystals{end, 4} = 1;
handles.Crystals{end, 5} = 0;
handles.Crystals{end, 6} = 1;
handles.Crystals{end, 7} = 0;
handles.Crystals{end, 8} = 0.1;
handles.Crystals{end, 9} = 0.01;
handles.Crystals{end, 10} = 1;
handles.Crystals{end, 11} = 0;
handles.Crystals{end, 12} = 0;

% adapt the space of the Crystal_Peak_New object
Space = ToolboxModule('GetSelectedSpace', handles.ToolboxModule);
if ~strcmpi(Space, 'pixel')
    ChangeSpace(handles.Crystals{end, 2}, Space);
end

% resort the crystals by name and set string of combobox
Crs = [handles.Crystals{:, 1}];
[~, idx] = sort(cellfun(@lower, {Crs.Name}, 'UniformOutput', false));
handles.Crystals = handles.Crystals(idx, :);
Crs = Crs(idx);
set(handles.PM_CrystalSelection, 'String', {Crs.Name});
% set the value of the crystal selection combobox to the new crystal
set(handles.PM_CrystalSelection, 'Value', find(ismember({Crs.Name}, Cr.Name)));

guidata(hObject, handles);

CrystalSelectionChanged(handles);


% --------------------------------------------------------------------
function RemoveBraggDuplicates_Cntxt_Callback(hObject, eventdata, handles)
% hObject    handle to RemoveBraggDuplicates_Cntxt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% get crystal and Crystal_Peaks
Index = get(handles.PM_CrystalSelection, 'Value');
CP = handles.Crystals{Index, 2};

% get hkl values
HKL = CP.hkl;

NewHKLs = RemoveBraggPeakDuplicates(CP, HKL, CP.Orientation);

set(handles.TB_hklValues, 'String', sprintf('%d, %d, %d\n', NewHKLs'));
TB_hklValues_Callback(handles.TB_hklValues, [], handles);


% --------------------------------------------------------------------
function Resethkl_Cntxt_Callback(hObject, eventdata, handles)
% hObject    handle to Resethkl_Cntxt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.TB_hklValues, 'String', '-2,-2,-2:2,2,2');
TB_hklValues_Callback(handles.TB_hklValues, [], handles);


% --- Executes on button press in CB_SampleRotation.
function CB_SampleRotation_Callback(hObject, eventdata, handles)
% hObject    handle to CB_SampleRotation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% toggle IncludeSampleRotation property
CP = handles.Crystals{get(handles.PM_CrystalSelection, 'Value'), 2};
CP.IncludeSampleRotation = get(handles.CB_SampleRotation, 'Value');
