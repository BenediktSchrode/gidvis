classdef (ConstructOnLoad) Crystal_Peak_New < GIDVis_Crystal & handle
% this class is responsible for plotting the Bragg peaks, labels, SF rings
% and DS rings
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2018 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    properties (Transient = true, Constant = true, GetAccess = private)
        phi = linspace(0, 2*pi, 1000); % used for calculation of rings (DS and SF)
        LabelStringFormat = '%g,%g,%g'; % formatting of the hkl values
    end
    
    properties (SetAccess = private)
        hkl % Matrix, size nx3 with hkl values
        Orientation % orientation of the crystal, 1x3
        SFMultiplier = 0.01; % multiplier for the structure factor
        SFProportionality = 'area'; % SF can be proportional to area or radius
        PlusMinusqxy = 1; % Show the peak positions for +q_xy (1) or -q_xy (-1)
        Space = 'q'; % Space: 'q' or 'polar'
    end
    
    properties (Dependent = true)
        BraggVisible = 'on'; % visibility of the Bragg peak markers
        BraggSize = 10; % size of the Bragg peak markers
        BraggEdgeColor = 'k'; % edge color of the Bragg peak markers
        BraggFaceColor = 'w'; % face color of the Bragg peak markers
        BraggMarker = 'o'; % marker for the Bragg peaks
        
        DSVisible = 'off'; % visibility of the Debye-Scherrer (DS) rings
        DSLineWidth = 1; % line width of the DS rings
        DSLineStyle = '-'; % line style of the DS rings
        DSColor = 'k'; % line color of the DS rings
        
        LabelVisible = 'on'; % visibility of the labels for the Bragg peaks
        LabelForeColor = 'k'; % fore color of the labels
        LabelBackgroundColor = 'none'; % background color of the labels
        LabelFontSize = 10; % font size of the labels
        LabelFontWeight = 'normal'; % font weight of the labels
        LabelFontAngle = 'normal'; % font angle of the labels
        LabelFontName = 'Helvetica'; % font name of the labels
        
        SFVisible = 'off'; % visibility of the structure factor (SF) rings
        SFLineWidth = 1; % line width of the SF rings
        SFLineStyle = '-'; % line style of the SF rings
        SFColor = 'k'; % line color of the SF rings
        
        BraggPeaksMirrored = false; % determines if the Bragg peak markers are shown for only +q_xy or for +q_xy and -q_xy
        
        OverallVisibility = true; % determines the visibility of obj.HGGroup
    end
    
    properties (Dependent = true, Transient = true)
        q % q values of the hkl values at the given Orientation
        StructureFactor % SF of the hkl values at the given Orientation
    end
    
    properties (Transient = true, Access = private)
        BraggPeak_LH % line handles for the Bragg peaks: cell of size nx1.
        DS_LH % line handles of the DS rings: cell of size nx1.
        SF_LH % line handles of the SF rings: cell of size nx1.
        Label_H % line handles of the labels: cell of size nx1.
        HGGroup % hggroup, acting as parent of the plot elements
    end
    
    properties (Access = private)
        LabelOffsetInternal = [.06 .04]; % internally used to store the label offset
        LabelLocationInternal = {}; % internally used to store the label location: cell of size nx1.
        IncludeSampleRotationInternal = false; % internally used to store whether the sample rotation should be included or not
        BraggPeaksMirroredInternal = false; % internally used to store whether Bragg peaks should be mirrored or not
    end
    
    properties (Dependent = true)
        LabelOffset; % two element vector with LabelOffset(1) being the offset in horizontal, LabelOffset(2) the offset in vertical direction. In data units.
        LabelLocation; % cell of size nx1, containg the label locations. Can be 'north', 'east', 'south', 'west', 'north east', 'south east', 'south west', 'north west', 'center'.
        IncludeSampleRotation; % bool whether the sample rotation should be included or not
    end
    
    methods
        function obj = Crystal_Peak_New(AxesHandle, Crystal, hkl, Orientation) % constructor
            % no input arguments, use some default
            % the case of no input arguments usually only occurs when
            % loadobj is executed. After loadobj, reload is executed where
            % the default properties get overwritten by the "correct"
            % values
            if nargin == 0
                AxesHandle = gca;
                Crystal = GIDVis_Crystal;
                hkl = [0 0 0];
                Orientation = [0 0 0];
            end
            
            % call the superclass constructor
            obj@GIDVis_Crystal(Crystal);

%             CrProps = {'Name', 'a', 'b', 'c', 'alpha', 'beta', 'gamma', 'SpacegroupNumber', ...
%                 'Delta_a', 'Delta_b', 'Delta_c', 'Delta_alpha', 'Delta_beta', 'Delta_gamma', ...
%                 'Latt', 'UnitCell', 'AsymmetricUnit', 'Symm'};
%             for iP = 1:numel(CrProps)
%                 eval(['obj.', CrProps{iP}, ' = Crystal.', CrProps{iP}, ';']);
%             end
            
            % assign properties
            obj.Orientation = Orientation;
            
            % update the HKL values
            ChangeHKL(obj, hkl, AxesHandle);
            if nargin == 0
%                 delete(obj.HGGroup)
            end
        end % constructor
        
        function delete(obj)
            % deletes all line handles + hggroup
            try %#ok
                delete(obj.BraggPeak_LH);
                delete(obj.DS_LH);
                delete(obj.SF_LH);
                delete(obj.Label_H);
                delete(obj.HGGroup);
            end
        end
        
        function ChangeSpace(obj, NewSpace)
            % Call this function to change the space.
            %
            % ChangeSpace(obj, 'q')
            % ChangeSpace(obj, 'polar')
            if ~ismember(lower(NewSpace), {'q', 'polar'})
                error(['Cannot change space to ', NewSpace, '. Supported values are ''q'' and ''polar''.']);
            end
            obj.Space = lower(NewSpace);
            UpdatePlot(obj);
        end
        
        function ChangePlusMinusqxy(obj, NewPlusMinusqxy)
            % Call this function to change the plus/minus of q_xy.
            %
            % ChangePlusMinusqxy(obj, 1)
            % ChangePlusMinusqxy(obj, -1)
            if ~ismember(NewPlusMinusqxy, [1 -1])
                error(['Cannot change PlusMinusqxy to ', NewPlusMinusqxy, '. Supported values are 1 and -1.'])
            end
            obj.PlusMinusqxy = NewPlusMinusqxy;
            UpdatePlot(obj);
        end
        
        function ChangeSFProportionality(obj, NewProportionality)
            % Call this function to change the structure factor
            % proportionality.
            %
            % ChangeSFProportionality(obj, 'radius') or
            % ChangeSFProportionality(obj, 'area')
            if ~ismember(NewProportionality, {'radius', 'area'})
                error(['Cannot change proportionality to ', NewProportionality, '. Supported values are ''radius'' and ''area''.'])
            end
            obj.SFProportionality = NewProportionality;
            % update the plot
            UpdatePlot(obj);
        end
        
        function ChangeSFMultiplier(obj, NewMultiplier)
            % Call this function to change the structure factor
            % multiplier.
            obj.SFMultiplier = NewMultiplier;
            UpdatePlot(obj);
        end
        
        function ChangeParent(obj, AxesHandle)
            % Call this function to transfer the plotted things from one
            % axes to another.
            %
            % ChangeParent(obj, NewAxesHandle);
            if ~strcmp(get(AxesHandle, 'Type'), 'axes')
                error('Input argument must be a valid axes handle.');
            end
            % Set the parent of the obj.HGGroup to the new axes. This will
            % move all plotted elements to the new axes.
            set(obj.HGGroup, 'Parent', AxesHandle);
            % get all children of the obj.HGGroup
            Ch = get(obj.HGGroup, 'Children');
            % get the parent figure of the new axes handle
            FigureHandle = ancestor(AxesHandle, 'figure');
            % loop over all children
            for iC = 1:numel(Ch)
                % get the context menu
                Ctx = get(Ch(iC), 'UIContextMenu');
                if isempty(Ctx); continue; end
                % set the context menu's parent to the new figure
                set(Ctx, 'Parent', FigureHandle);
            end
        end
        
        function ChangeOrientation(obj, NewOrientation)
            % Call this function to change the orientation of the crystal.
            % The plot will be updated to the new orientation.
            %
            % ChangeOrientation(obj, NewOrientation) where NewOrientation
            % is a 1x3 vector.
            if ~all(size(NewOrientation) == [1 3]);
                error('NewOrientation has to be a 1x3 vector.');
            end
            obj.Orientation = NewOrientation;
            UpdatePlot(obj);
        end
        
        function ChangeCrystal(obj, NewCrystal)
            % Call this function to change the crystal. The plot will be
            % updated to the new crystal.
            
            if ~strcmp(class(NewCrystal), 'GIDVis_Crystal')
                error('Input has to be of class ''GIDVis_Crystal''.');
            end
            obj.a = NewCrystal.a;
            obj.b = NewCrystal.b;
            obj.c = NewCrystal.c;
            obj.alpha = NewCrystal.alpha;
            obj.beta = NewCrystal.beta;
            obj.gamma = NewCrystal.gamma;
            obj.Name = NewCrystal.Name;
            
            obj.SpacegroupNumber = NewCrystal.SpacegroupNumber;
            obj.Delta_a = NewCrystal.Delta_a;
            obj.Delta_b = NewCrystal.Delta_b;
            obj.Delta_c = NewCrystal.Delta_c;
            obj.Delta_alpha = NewCrystal.Delta_alpha;
            obj.Delta_beta = NewCrystal.Delta_beta;
            obj.Delta_gamma = NewCrystal.Delta_gamma;
            obj.UnitCell = NewCrystal.UnitCell;
            obj.Latt = NewCrystal.Latt;
            obj.AsymmetricUnit = NewCrystal.AsymmetricUnit;
            obj.Symm = NewCrystal.Symm;
            obj.Setting = NewCrystal.Setting;

            
            UpdatePlot(obj);
        end
        
        function ChangeHKL(obj, NewHKL, AxesHandle)
            % Call this function to change the HKL values plotted. NewHKL
            % is a matrix of size nx3. HKL values already plotted but not
            % contained in NewHKL anymore will be removed from the plot.
            % HKL values not plotted but contained in NewHKL will be added
            % to the plot.
            % The newly plotted HKL values will have the same properties
            % (Line widht, marker, colors, ...) as the first one in the
            % list (if available). With the exception of the label
            % location. Here the newly added values will get the label
            % location appearing the most often in the old values.
            %
            % AxesHandle is not a necessary input argument. Will be taken
            % from the old HKL values if not provided.
            %
            % Usage:
            % ChangeHKL(obj, NewHKL, AxesHandle)
            
            % get axes handle from old HKL values if not provided
            if nargin < 3 || isempty(AxesHandle)
                AxesHandle = get(get(obj.BraggPeak_LH(1), 'Parent'), 'Parent');
            end
                
            % find out which HKL values need to be added
            oldHKL = obj.hkl;
            if ~isempty(oldHKL)
                ToAdd = setdiff(NewHKL, oldHKL, 'rows');
            else
                ToAdd = NewHKL;
            end
            
            % if no HGGroup exists, create one
            if isempty(obj.HGGroup);
                obj.HGGroup = hggroup('Parent', AxesHandle, 'DisplayName', obj.Name);
                set(get(get(obj.HGGroup, 'Annotation'), 'LegendInformation'), ...
                    'IconDisplayStyle', 'on'); %Include this hggroup in the legend
            end
            
            % add hkls
            q_r = PeakPositionsQ(obj, ToAdd, obj.Orientation);
            q_xy = obj.PlusMinusqxy*sqrt(q_r(:, 1).^2 + q_r(:, 2).^2);
            q_z = q_r(:, 3);
            
            % plot the DS Ring
            r = sqrt(q_xy.^2+q_z.^2);
            X = r*cos(obj.phi);
            Y = r*sin(obj.phi);
            NewDS_LH = line(X', Y', 'Parent', obj.HGGroup, ...
                'LineWidth', 0.5, 'Color', 'k', 'LineStyle', '-', ...
                'Visible', 'off', ...
                'Tag', 'DebyeScherrerRing', ...
                'XLimInclude', 'off', 'YLimInclude', 'off');

            % plot the Structure Factor Ring
            SF = GetStructureFactors(obj, ToAdd);
            if ~isempty(SF)
                if obj.IncludeSampleRotation
                    SF = GetStructureFactors2Rot(obj, ToAdd, obj.Orientation);
                else
                    SF = abs(SF).^2;
                end
                switch lower(obj.SFProportionality)
                    case 'radius'
                        r = SF;
                    case 'area'
                        r = sqrt(SF/pi); % Area = r^2*pi ==> r = sqrt(Area/pi)
                end
                X_SF = obj.SFMultiplier.*r*cos(obj.phi)+repmat(q_xy, 1, numel(obj.phi));
                Y_SF = obj.SFMultiplier.*r*sin(obj.phi)+repmat(q_z, 1, numel(obj.phi));
                
                NewSF_LH = line(X_SF', Y_SF', ...
                    'Parent', obj.HGGroup, ...
                    'LineWidth', 0.5, ...
                    'LineStyle', '-', ...
                    'Color', 'k', ...
                    'Visible', 'off', ...
                    'Tag', 'StructureFactor', ...
                    'XLimInclude', 'off', 'YLimInclude', 'off');
            end

            % plot the Bragg Peak
            NewBraggPeak_LH = line(NaN(2, numel(q_xy)), NaN(2, numel(q_z)), ...
                'LineStyle', 'none', 'Marker', 'o', ...
                'MarkerFaceColor', 'r', 'MarkerEdgeColor', 'k', ...
                'Parent', obj.HGGroup, 'Tag', 'BraggPeak', ...
                'XLimInclude', 'off', 'YLimInclude', 'off');
            set(NewBraggPeak_LH, {'XData'}, num2cell(q_xy), {'YData'}, num2cell(q_z));
            
            % create the text label
            StrCell = regexp(sprintf([obj.LabelStringFormat, '\n'], ToAdd'), '\n', 'split');
            NewLabel_H = text(q_xy+obj.LabelOffset(1), q_z, ...
                StrCell(1:end-1)', ...
                'FontSize', 10, ...
                'BackgroundColor', 'none', ...
                'Color', 'k', ...
                'Visible', 'on', ...
                'Parent', obj.HGGroup, ...
                'Clipping', 'on', ...
                'Tag', 'TextLabel', ...
                'XLimInclude', 'off', 'YLimInclude', 'off');
            
            NewLabelLocationInternal = repmat({'east'}, numel(q_z), 1);
                        
            % create the context menu
            hcmenu = uicontextmenu('Parent', get(AxesHandle, 'Parent'), 'Callback', @obj.ContextMenu_Callback);
            uimenu(hcmenu, 'Tag', 'Description_Ctxt', 'Label', '', 'Enable', 'off');
            uimenu(hcmenu, 'Tag', 'DSVisible_Cntxt', 'Label', 'DS Ring Visibility', 'Callback', {@obj.DSRingVisibility_Callback}, 'Separator', 'on');
            uimenu(hcmenu, 'Tag', 'LabelVisibile_Cntxt', 'Label', 'Label Visibility', 'Callback', {@obj.LabelVisibility_Callback});
            SF_Cntxt = uimenu(hcmenu, 'Tag', 'SFVisible_Cntxt', 'Label', 'SF Visibility', 'Callback', {@obj.SFVisibility_Callback}, 'Enable', 'off');
            LabelLoc = uimenu(hcmenu, 'Label', 'Label Location', 'Tag', 'LabelLoc_Cntxt');
            uimenu(LabelLoc, 'Tag', 'LabelLoc_north_Cntxt', 'Label', 'North', 'Callback', {@obj.ChangeLabelLocation_Callback, 'north'});
            uimenu(LabelLoc, 'Tag', 'LabelLoc_east_Cntxt', 'Label', 'East', 'Callback', {@obj.ChangeLabelLocation_Callback, 'east'});
            uimenu(LabelLoc, 'Tag', 'LabelLoc_south_Cntxt', 'Label', 'South', 'Callback', {@obj.ChangeLabelLocation_Callback, 'south'});
            uimenu(LabelLoc, 'Tag', 'LabelLoc_west_Cntxt', 'Label', 'West', 'Callback', {@obj.ChangeLabelLocation_Callback, 'west'});
            uimenu(LabelLoc, 'Tag', 'LabelLoc_north east_Cntxt', 'Label', 'North East', 'Callback', {@obj.ChangeLabelLocation_Callback, 'north east'}, 'Separator', 'on');
            uimenu(LabelLoc, 'Tag', 'LabelLoc_south east_Cntxt', 'Label', 'South East', 'Callback', {@obj.ChangeLabelLocation_Callback, 'south east'});
            uimenu(LabelLoc, 'Tag', 'LabelLoc_south west_Cntxt', 'Label', 'South West', 'Callback', {@obj.ChangeLabelLocation_Callback, 'south west'});
            uimenu(LabelLoc, 'Tag', 'LabelLoc_north west_Cntxt', 'Label', 'North West', 'Callback', {@obj.ChangeLabelLocation_Callback, 'north west'});
            uimenu(LabelLoc, 'Tag', 'LabelLoc_center_Cntxt', 'Label', 'Center', 'Callback', {@obj.ChangeLabelLocation_Callback, 'center'}, 'Separator', 'on');
            
            CntxtMenuCell = num2cell(repmat(hcmenu, numel(q_z), 1));
            
            % set the context menu of the handles
            set(NewBraggPeak_LH, {'UIContextMenu'}, CntxtMenuCell);
            set(NewLabel_H, {'UIContextMenu'}, CntxtMenuCell);
            set(NewDS_LH, {'UIContextMenu'}, CntxtMenuCell);
            if ~isempty(SF)
                set(NewSF_LH, {'UIContextMenu'}, CntxtMenuCell);
            end
            
            % change attributes of the new line handles
            if ~isempty(oldHKL)
                set(NewDS_LH, {'LineWidth', 'LineStyle', 'Color', 'Visible'}, ...
                    repmat({obj.DSLineWidth{1}, obj.DSLineStyle{1}, obj.DSColor{1}, obj.DSVisible{1}}, size(ToAdd, 1), 1));

                if ~isempty(SF)
                    set(NewSF_LH, {'Visible', 'LineWidth', 'LineStyle', 'Color'}, ...
                        repmat({obj.SFVisible{1}, obj.SFLineWidth{1}, obj.SFLineStyle{1}, obj.SFColor{1}}, size(ToAdd, 1), 1));
                end

                set(NewBraggPeak_LH, {'Visible', 'MarkerSize', 'MarkerEdgeColor', 'MarkerFaceColor', 'Marker'}, ...
                    repmat({obj.BraggVisible{1}, obj.BraggSize{1}, obj.BraggEdgeColor{1}, obj.BraggFaceColor{1}, obj.BraggMarker{1}}, size(ToAdd, 1), 1));

                set(NewLabel_H, {'Visible', 'FontSize', 'Color', 'BackgroundColor'}, ...
                    repmat({obj.LabelVisible{1}, obj.LabelFontSize{1}, obj.LabelForeColor{1}, obj.LabelBackgroundColor{1}}, size(ToAdd, 1), 1));
            end
            
            % add the handles to the obj
            NumH = numel(NewDS_LH);
            obj.DS_LH(end+1:end+NumH) = NewDS_LH;
            if ~isempty(SF)
                set(SF_Cntxt, 'Enable', 'on');
                obj.SF_LH(end+1:end+NumH) = NewSF_LH;
            end
            obj.Label_H(end+1:end+NumH) = NewLabel_H;
            obj.BraggPeak_LH(end+1:end+NumH) = NewBraggPeak_LH;
            obj.LabelLocationInternal(end+1:end+NumH) = NewLabelLocationInternal;
            
            % add the new hkls to the obj
            obj.hkl(end+1:end+size(ToAdd, 1), :) = ToAdd;
            
            if ~isempty(oldHKL)
                % set the newly added label's locations to the location
                % appearing the most often in the old ones
                [a, ~, c] = unique(obj.LabelLocation(1:end-size(ToAdd, 1)));
                d = hist(c, length(a));
                [~, ind] = max(d);
                oldLocs = obj.LabelLocation(1:end-size(ToAdd, 1));
                if size(oldLocs, 1) == 1; oldLocs = oldLocs'; end
                obj.LabelLocation = [oldLocs; repmat(a(ind), size(ToAdd, 1), 1)];
            end
            
            % find hkl values which need to be deleted
            if ~isempty(oldHKL)
                [~, IndToDel] = setdiff(oldHKL, NewHKL, 'rows');
            else
                IndToDel = [];
            end
            
            % delete hkls which have to be deleted
            obj.hkl(IndToDel, :) = [];
            delete(obj.BraggPeak_LH(IndToDel));
            obj.BraggPeak_LH(IndToDel) = [];
            
            delete(obj.DS_LH(IndToDel));
            obj.DS_LH(IndToDel) = [];
            
            if ~isempty(obj.SF_LH)
                delete(obj.SF_LH(IndToDel));
                obj.SF_LH(IndToDel) = [];
            end
            
            delete(obj.Label_H(IndToDel));
            obj.Label_H(IndToDel) = [];
            
            if ~isempty(obj.LabelLocation)
                obj.LabelLocation(IndToDel) = [];
            end
        end

        function q = get.q(obj)
            % returns the q values for the hkl values and the orientation
            % in obj.
            q = PeakPositionsQ(obj, obj.hkl, obj.Orientation);
        end
        
        function SF = get.StructureFactor(obj)
            % returns the structure factor for the hkl values in obj.
            SF = GetStructureFactors(obj, obj.hkl);
        end
        
        function set.OverallVisibility(obj, val)
            set(obj.HGGroup, 'Visible', val);
        end
        
        function val = get.OverallVisibility(obj)
            val = get(obj.HGGroup, 'Visible');
        end

        function set.IncludeSampleRotation(obj, NewVal)
            % store the new IncludeSampleRotation value internally
            obj.IncludeSampleRotationInternal = NewVal;
            UpdatePlot(obj);
        end
        function Val = get.IncludeSampleRotation(obj)
            Val = obj.IncludeSampleRotationInternal;
        end

        function set.LabelOffset(obj, NewLabelOffset)
            % This function is called when the label offset gets set.
            
            % get the q values.
            q_r = obj.q;
            q_xy = obj.PlusMinusqxy*sqrt(q_r(:, 1).^2+q_r(:, 2).^2);
            q_z = q_r(:, 3);
            
            % transform to polar space if necessary
            if strcmp(obj.Space, 'q')
                xPos = q_xy;
                yPos = q_z;
            else
                xPos = sqrt(q_xy.^2+q_z.^2);
                yPos = atan2(q_xy, q_z)*180/pi;
            end
            
            if ~isempty(obj.Label_H) % if there are no labels, we don't have to do anything
                % cell containing the possible locations
                C = {'north', 'east', 'south', 'west', ...
                    'north east', 'south east', 'south west', 'north west', ...
                    'center'};
                % contains the "direction" of the offset. E.g., for label
                % location 'north', we have to add 0 offset in horizontal
                % direction, but 1 offset in vertical direction --> first
                % row of OffsetMat = [0 1].
                OffsetMat = [0 1; 1 0; 0 -1; -1 0; ...
                    1 1; 1 -1; -1 -1; -1 1; ...
                    0 0];
                % Horizontal alignments for the labels depending on their
                % location. E.g., for north location, the horizontal
                % alignment has to be center.
                HorizAl = {'center', 'left', 'center', 'right', ...
                    'left', 'left', 'right', 'right', ...
                    'center'};
                % Vertical alignments for the labels depending on their
                % location. E.g., for north location, the vertical
                % alignment has to be bottom.
                VertAl = {'bottom', 'middle', 'top', 'middle', ...
                    'middle', 'middle', 'middle', 'middle', ...
                    'middle'};
                
                % loop over all possible locations
                for iLocations = 1:numel(C)
                    % find the labels with location C{iLocations}
                    L = strcmp(obj.LabelLocation, C{iLocations});
                    % set their Position, horizontal and vertical
                    % alignments accordingly
                    set(obj.Label_H(L), {'Position'}, num2cell([xPos(L)+OffsetMat(iLocations, 1)*NewLabelOffset(1) yPos(L)+OffsetMat(iLocations, 2)*NewLabelOffset(2)], 2));
                    set(obj.Label_H(L), {'HorizontalAlignment'}, repmat(HorizAl(iLocations), sum(L), 1));
                    set(obj.Label_H(L), {'VerticalAlignment'}, repmat(VertAl(iLocations), sum(L), 1));
                end
            end
            % store the new label offset internally
            obj.LabelOffsetInternal = NewLabelOffset;
        end
        function Val = get.LabelOffset(obj)
            Val = obj.LabelOffsetInternal;
        end
        
        
        
        function ChangeFontProperties(obj)
            S.FontName = obj.LabelFontName{1};
            S.FontSize = obj.LabelFontSize{1};
            S.FontWeight = obj.LabelFontWeight{1};
            S.FontAngle = obj.LabelFontAngle{1};
            
            Res = uisetfont(S);
            if isequal(Res, 0); return; end
            obj.LabelFontName = Res.FontName;
            obj.LabelFontSize = Res.FontSize;
            obj.LabelFontWeight = Res.FontWeight;
            obj.LabelFontAngle = Res.FontAngle;
        end
        
        function set.SFVisible(obj, Val)
            % Set the SF visibility. 
            % Input can either be a cell of size nx1 containing 'on' and 
            % 'off', with n being the number of hkl values plotted, or 'on' 
            % or 'off'.
            if ~iscell(Val); Val = {Val}; end
            set(obj.SF_LH, {'Visible'}, Val(:));
        end
        function Val = get.SFVisible(obj)
            Val1 = get(obj.SF_LH, 'Visible');
            if ~iscell(Val1)
                Val = {Val1}; 
            else
                Val = Val1;
            end
        end

        function set.LabelVisible(obj, Val)
            % Set the label visibility. 
            % Input can either be a cell of size nx1 containing 'on' and 
            % 'off', with n being the number of hkl values plotted, or 'on' 
            % or 'off'.
            if ~iscell(Val); Val = {Val}; end
            set(obj.Label_H, {'Visible'}, Val(:));
        end
        function Val = get.LabelVisible(obj)
            Val1 = get(obj.Label_H, 'Visible');
            if ~iscell(Val1)
                Val = {Val1}; 
            else
                Val = Val1;
            end
        end
        
        function set.LabelFontName(obj, Val)
            if ~iscell(Val); Val = {Val(:)'}; end
            if size(Val, 2) ~= 1; Val = Val'; end
            set(obj.Label_H, {'FontName'}, Val);
        end
        function Val = get.LabelFontName(obj)
            Val1 = get(obj.Label_H, 'FontName');
            if ~iscell(Val1)
                Val = {Val1};
            else
                Val = Val1;
            end
        end
        
        function set.LabelFontAngle(obj, Val)
            if ~iscell(Val); Val = {Val(:)'}; end
            if size(Val, 2) ~= 1; Val = Val'; end
            set(obj.Label_H, {'FontAngle'}, Val);
        end
        function Val = get.LabelFontAngle(obj)
            Val1 = get(obj.Label_H, 'FontAngle');
            if ~iscell(Val1)
                Val = {Val1};
            else
                Val = Val1;
            end
        end
        
        function set.LabelFontWeight(obj, Val)
            if ~iscell(Val); Val = {Val(:)'}; end
            if size(Val, 2) ~= 1; Val = Val'; end
            set(obj.Label_H, {'FontWeight'}, Val);
        end
        function Val = get.LabelFontWeight(obj)
            Val1 = get(obj.Label_H, 'FontWeight');
            if ~iscell(Val1)
                Val = {Val1};
            else
                Val = Val1;
            end
        end
        
        function set.LabelFontSize(obj, Val)
            % Set the label font size. 
            % Input can either be a matrix of size nx1 containing the new
            % font sizes, or a single numeric value.
            if ~iscell(Val); Val = num2cell(Val(:)); end
            set(obj.Label_H, {'FontSize'}, Val);
        end
        function Val = get.LabelFontSize(obj)
            Val1 = get(obj.Label_H, 'FontSize');
            if ~iscell(Val1)
                Val = {Val1}; 
            else
                Val = Val1;
            end
        end
        
        function set.LabelForeColor(obj, Val)
            if ~iscell(Val)
                if ischar(Val) % color string as input, e.g. 'r' or 'g'
                    Val = {Val}; 
                else % rgb vector as input
                    Val = num2cell(Val(:), 1);
                end
            end
            set(obj.Label_H, {'Color'}, Val(:));
        end
        function Val = get.LabelForeColor(obj)
            Val1 = get(obj.Label_H, 'Color');
            if ~iscell(Val1)
                Val = {Val1}; 
            else
                Val = Val1;
            end
        end
        
        function set.LabelBackgroundColor(obj, Val)
           	if ~iscell(Val)
                if ischar(Val) % color string as input, e.g. 'r' or 'g'
                    Val = {Val}; 
                else % rgb vector as input
                    Val = num2cell(Val(:), 1);
                end
            end
            set(obj.Label_H, {'BackgroundColor'}, Val(:));
        end
        function Val = get.LabelBackgroundColor(obj)
            Val1 = get(obj.Label_H, 'BackgroundColor');
            if ~iscell(Val1)
                Val = {Val1}; 
            else
                Val = Val1;
            end
        end
        
        function set.DSVisible(obj, Val)
            if ~iscell(Val); Val = {Val}; end
            set(obj.DS_LH, {'Visible'}, Val(:));
        end
        function Val = get.DSVisible(obj)
            Val1 = get(obj.DS_LH, 'Visible');
            if ~iscell(Val1)
                Val = {Val1}; 
            else
                Val = Val1;
            end
        end
        
        function set.DSLineWidth(obj, Val)
            if ~iscell(Val); Val = num2cell(Val(:)); end
            set(obj.DS_LH, {'LineWidth'}, Val);
        end
        function Val = get.DSLineWidth(obj)
            Val1 = get(obj.DS_LH, 'LineWidth');
            if ~iscell(Val1)
                Val = {Val1}; 
            else
                Val = Val1;
            end
        end
        
        function set.DSLineStyle(obj, Val)
            if ~iscell(Val); Val = {Val}; end
            set(obj.DS_LH, {'LineStyle'}, Val(:));
        end
        function Val = get.DSLineStyle(obj)
            Val1 = get(obj.DS_LH, 'LineStyle');
            if ~iscell(Val1)
                Val = {Val1}; 
            else
                Val = Val1;
            end
        end
        
        function set.DSColor(obj, Val)
            if ~iscell(Val)
                if ischar(Val) % color string as input, e.g. 'r' or 'g'
                    Val = {Val}; 
                else % rgb vector as input
                    Val = num2cell(Val(:), 1);
                end
            end
            set(obj.DS_LH, {'Color'}, Val(:));
        end
        function Val = get.DSColor(obj)
            Val1 = get(obj.DS_LH, 'Color');
            if ~iscell(Val1)
                Val = {Val1}; 
            else
                Val = Val1;
            end
        end

        function set.LabelLocation(obj, Val)
            if ~iscell(Val); Val = {Val}; end
            if numel(Val) == 1
                obj.LabelLocationInternal = repmat(Val, size(obj.hkl, 1), 1);
            else
                obj.LabelLocationInternal = Val;
            end
            obj.LabelOffset = obj.LabelOffset;
        end
        function Val = get.LabelLocation(obj)
            Val1 = obj.LabelLocationInternal;
            if ~iscell(Val1)
                Val = {Val1}; 
            else
                Val = Val1;
            end
        end

        function set.BraggVisible(obj, Val)
            if ~iscell(Val); Val = {Val}; end
            set(obj.BraggPeak_LH, {'Visible'}, Val(:));
        end
        function Val = get.BraggVisible(obj)
            Val1 = get(obj.BraggPeak_LH, 'Visible');
            if ~iscell(Val1)
                Val = {Val1}; 
            else
                Val = Val1;
            end
        end
        
        function set.BraggSize(obj, Val)
            if ~iscell(Val); Val = num2cell(Val(:)); end
            set(obj.BraggPeak_LH, {'MarkerSize'}, Val);
        end
        function Val = get.BraggSize(obj)
            Val1 = get(obj.BraggPeak_LH, 'MarkerSize');
            if ~iscell(Val1)
                Val = {Val1}; 
            else
                Val = Val1;
            end
        end
        
        function set.BraggEdgeColor(obj, Val)
            if ~iscell(Val)
                if ischar(Val) % color string as input, e.g. 'r' or 'g'
                    Val = {Val}; 
                else % rgb vector as input
                    Val = num2cell(Val(:), 1);
                end
            end
            set(obj.BraggPeak_LH, {'MarkerEdgeColor'}, Val(:));
        end
        function Val = get.BraggEdgeColor(obj)
            Val1 = get(obj.BraggPeak_LH, 'MarkerEdgeColor');
            if ~iscell(Val1)
                Val = {Val1}; 
            else
                Val = Val1;
            end
        end
        
        function set.BraggFaceColor(obj, Val)
            if ~iscell(Val)
                if ischar(Val) % color string as input, e.g. 'r' or 'g'
                    Val = {Val}; 
                else % rgb vector as input
                    Val = num2cell(Val(:), 1);
                end
            end
            set(obj.BraggPeak_LH, {'MarkerFaceColor'}, Val(:));
        end
        function Val = get.BraggFaceColor(obj)
            Val1 = get(obj.BraggPeak_LH, 'MarkerFaceColor');
            if ~iscell(Val1)
                Val = {Val1}; 
            else
                Val = Val1;
            end
        end
        
        function set.BraggMarker(obj, Val)
            if ~iscell(Val); Val = {Val}; end
            set(obj.BraggPeak_LH, {'Marker'}, Val(:));
        end
        function Val = get.BraggMarker(obj)
            Val1 = get(obj.BraggPeak_LH, 'Marker');
            if ~iscell(Val1)
                Val = {Val1}; 
            else
                Val = Val1;
            end
        end
               
        function set.SFLineWidth(obj, Val)
            if ~iscell(Val); Val = num2cell(Val(:)); end
            set(obj.SF_LH, {'LineWidth'}, Val);
        end
        function Val = get.SFLineWidth(obj)
            Val1 = get(obj.SF_LH, 'LineWidth');
            if ~iscell(Val1)
                Val = {Val1}; 
            else
                Val = Val1;
            end
        end
        
        function set.SFLineStyle(obj, Val)
            if ~iscell(Val); Val = {Val}; end
            set(obj.SF_LH, {'LineStyle'}, Val(:));
        end
        function Val = get.SFLineStyle(obj)
            Val1 = get(obj.SF_LH, 'LineStyle');
            if ~iscell(Val1)
                Val = {Val1}; 
            else
                Val = Val1;
            end
        end
        
        function set.SFColor(obj, Val)
            if ~iscell(Val)
                if ischar(Val) % color string as input, e.g. 'r' or 'g'
                    Val = {Val}; 
                else % rgb vector as input
                    Val = num2cell(Val(:), 1);
                end
            end
            set(obj.SF_LH, {'Color'}, Val(:));
        end
        function Val = get.SFColor(obj)
            Val1 = get(obj.SF_LH, 'Color');
            if ~iscell(Val1)
                Val = {Val1}; 
            else
                Val = Val1;
            end
        end
        
        function set.BraggPeaksMirrored(obj, Val)
            obj.BraggPeaksMirroredInternal = Val;
            UpdatePlot(obj);
        end
        function Val = get.BraggPeaksMirrored(obj)
            Val = obj.BraggPeaksMirroredInternal;
        end
    end % methods
    
    methods (Access = private)
        function DSRingVisibility_Callback(obj, src, evt) %#ok
            % This function is called by the context menu to change the
            % DS ring visibility of the clicked element.
            
            % get the line handle of the clicked element
            ClickedLineHandle = get(get(get(src, 'Parent'), 'Parent'), 'CurrentObject');
            switch get(ClickedLineHandle, 'Tag')
                case 'BraggPeak'
                    L = obj.BraggPeak_LH == ClickedLineHandle;
                case 'TextLabel'
                    L = obj.Label_H == ClickedLineHandle;
                case 'StructureFactor'
                    L = obj.SF_LH == ClickedLineHandle;
                case 'DebyeScherrerRing'
                    L = obj.DS_LH == ClickedLineHandle;
            end
            % toggle the visibility
            if strcmp(get(obj.DS_LH(L), 'Visible'), 'on')
                set(obj.DS_LH(L), 'Visible', 'off');
            else
                set(obj.DS_LH(L), 'Visible', 'on');
            end
        end
        
        function ChangeLabelLocation_Callback(obj, src, evt, NewLocation) %#ok
            % This function is called by the context menu to change the
            % label location of the clicked element.
            
            % get the line handle of the clicked element
            ClickedLineHandle = get(get(get(get(src, 'Parent'), 'Parent'), 'Parent'), 'CurrentObject');
            switch get(ClickedLineHandle, 'Tag')
                case 'BraggPeak'
                    L = obj.BraggPeak_LH == ClickedLineHandle;
                case 'TextLabel'
                    L = obj.Label_H == ClickedLineHandle;
                case 'StructureFactor'
                    L = obj.SF_LH == ClickedLineHandle;
                case 'DebyeScherrerRing'
                    L = obj.DS_LH == ClickedLineHandle;
            end
            % set the label location of the clicked line handle to its new
            % value
            obj.LabelLocation{L} = NewLocation;
        end
        
        function SFVisibility_Callback(obj, src, evt) %#ok
            % This function is called by the context menu to change the
            % SF ring visibility of the clicked element.
            
            % get the line handle of the clicked element
            ClickedLineHandle = get(get(get(src, 'Parent'), 'Parent'), 'CurrentObject');
            switch get(ClickedLineHandle, 'Tag')
                case 'BraggPeak'
                    L = obj.BraggPeak_LH == ClickedLineHandle;
                case 'TextLabel'
                    L = obj.Label_H == ClickedLineHandle;
                case 'StructureFactor'
                    L = obj.SF_LH == ClickedLineHandle;
                case 'DebyeScherrerRing'
                    L = obj.DS_LH == ClickedLineHandle;
            end
            % toggle the visibility
            if strcmp(get(obj.SF_LH(L), 'Visible'), 'on')
                set(obj.SF_LH(L), 'Visible', 'off');
            else
                set(obj.SF_LH(L), 'Visible', 'on');
            end
        end
        
        function LabelVisibility_Callback(obj, src, evt) %#ok
            % This function is called by the context menu to change the
            % label visibility of the clicked element.
            
            % get the line handle of the clicked element
            ClickedLineHandle = get(get(get(src, 'Parent'), 'Parent'), 'CurrentObject');
            switch get(ClickedLineHandle, 'Tag')
                case 'BraggPeak'
                    L = obj.BraggPeak_LH == ClickedLineHandle;
                case 'TextLabel'
                    L = obj.Label_H == ClickedLineHandle;
                case 'StructureFactor'
                    L = obj.SF_LH == ClickedLineHandle;
                case 'DebyeScherrerRing'
                    L = obj.DS_LH == ClickedLineHandle;
            end
            % toggle the visibility
            if strcmp(get(obj.Label_H(L), 'Visible'), 'on')
                set(obj.Label_H(L), 'Visible', 'off');
            else
                set(obj.Label_H(L), 'Visible', 'on');
            end
        end
        
        function ContextMenu_Callback(obj, src, evt) %#ok
            % Gets called just before the context menu opens. Check/uncheck
            % the context menu entries according to visibility/label
            % location.
            Children = get(src, 'Children');
            Description_Ctxt = findobj(Children, 'Tag', 'Description_Ctxt');
            DS_Cntxt = findobj(Children, 'Tag', 'DSVisible_Cntxt');
            Label_Cntxt = findobj(Children, 'Tag', 'LabelVisibile_Cntxt');
            SF_Cntxt = findobj(Children, 'Tag', 'SFVisible_Cntxt');
            LabelLoc_Cntxt = findobj(Children, 'Tag', 'LabelLoc_Cntxt');
            LabelLoc_Cntxts = get(LabelLoc_Cntxt, 'Children');
            set(LabelLoc_Cntxts, {'Checked'}, repmat({'off'}, numel(LabelLoc_Cntxts), 1));
            
            ClickedLineHandle = get(get(src, 'Parent'), 'CurrentObject');
            switch get(ClickedLineHandle, 'Tag')
                case 'BraggPeak'
                    L = obj.BraggPeak_LH == ClickedLineHandle;
                case 'TextLabel'
                    L = obj.Label_H == ClickedLineHandle;
                case 'StructureFactor'
                    L = obj.SF_LH == ClickedLineHandle;
                case 'DebyeScherrerRing'
                    L = obj.DS_LH == ClickedLineHandle;
            end
            set(DS_Cntxt, 'Checked', obj.DSVisible{L});
            set(Label_Cntxt, 'Checked', obj.LabelVisible{L});
            if strcmp(get(SF_Cntxt, 'Enable'), 'on')
                set(SF_Cntxt, 'Checked', obj.SFVisible{L});
            end
            Loc_Cntxt = findobj(LabelLoc_Cntxts, 'Tag', ['LabelLoc_', obj.LabelLocation{L}, '_Cntxt']);
            set(Loc_Cntxt, 'Checked', 'on');
            set(Description_Ctxt, 'Label', sprintf(['%s (', obj.LabelStringFormat, ')'], obj.Name, obj.hkl(L, :)));
        end
        
        function UpdatePlot(obj)
            % We have to update: Bragg Peak Position, DS rings, SF rings,
            % label positions
            q_r = obj.q;
            q_xy = obj.PlusMinusqxy.*sqrt(q_r(:, 1).^2 + q_r(:, 2).^2);
            q_z = q_r(:, 3);
            
            % update the DS Rings
            r = sqrt(q_xy.^2+q_z.^2);
            if strcmp(obj.Space, 'q')
                X = r*cos(obj.phi);
                Y = r*sin(obj.phi);
            else
                X = repmat(r, 1, 2);
                Y = repmat([-180 180], numel(r), 1);
            end
            set(obj.DS_LH, {'XData'}, num2cell(X, 2), {'YData'}, num2cell(Y, 2));

            % update the Bragg Peak
            if strcmp(obj.Space, 'q')
                if obj.BraggPeaksMirrored
                    % repeat the q values in case the Bragg peaks should be
                    % mirrored
                    set(obj.BraggPeak_LH, {'XData'}, num2cell([q_xy -q_xy], 2), ...
                        {'YData'}, num2cell([q_z q_z], 2));
                else
                    set(obj.BraggPeak_LH, {'XData'}, num2cell(q_xy), {'YData'}, num2cell(q_z));
                end
            else
                rho = sqrt(q_xy.^2+q_z.^2);
                theta = atan2(q_xy, q_z)*180/pi;
                if obj.BraggPeaksMirrored
                    % repeat the rho/theta values in case the Bragg peaks
                    % should be mirrored
                    set(obj.BraggPeak_LH, {'XData'}, num2cell([rho rho], 2), ...
                        {'YData'}, num2cell([theta -theta], 2));
                else
                    set(obj.BraggPeak_LH, {'XData'}, num2cell(rho), {'YData'}, num2cell(theta));
                end
            end

            % update the Structure Factor Ring
            SF = obj.StructureFactor;
            if isempty(SF) && ~isempty(obj.SF_LH) % new crystal has no SF, old one had
                delete(obj.SF_LH);
                obj.SF_LH = [];
            elseif ~isempty(SF)
                if obj.IncludeSampleRotation
                    SF = GetStructureFactors2Rot(obj, obj.hkl, obj.Orientation);
                else
                    SF = abs(SF).^2;
                end
                switch lower(obj.SFProportionality)
                    case 'radius'
                        r = SF;
                    case 'area'
                        r = sqrt(SF/pi); % Area = r^2*pi ==> r = sqrt(Area/pi)
                end
                if strcmp(obj.Space, 'q')
                    X_SF = obj.SFMultiplier.*r*cos(obj.phi)+repmat(q_xy, 1, numel(obj.phi));
                    Y_SF = obj.SFMultiplier.*r*sin(obj.phi)+repmat(q_z, 1, numel(obj.phi));
                else
                    X_SF = obj.SFMultiplier.*r*cos(obj.phi)+repmat(rho, 1, numel(obj.phi));
                    Y_SF = obj.SFMultiplier.*r*sin(obj.phi)+repmat(theta, 1, numel(obj.phi));
                end
                
                if isempty(obj.SF_LH) % create new SF line handles
                    obj.SF_LH = line(X_SF', Y_SF', ...
                        'Parent', get(obj.BraggPeak_LH(1), 'Parent'), ...
                        'LineWidth', 0.5, ...
                        'LineStyle', '-', ...
                        'Color', 'k', ...
                        'Visible', 'on');
                else
                    set(obj.SF_LH, {'XData'}, num2cell(X_SF, 2), {'YData'}, num2cell(Y_SF, 2));
                end
            end
            
            % update the text labels
            obj.LabelOffset = obj.LabelOffset;
        end
    end % methods (Access = private)
    
    %% Save/load methods
    methods
        function S = saveobj(obj)
            % Save property values in struct
            % Return struct for save function to write to MAT-file
            CrProps = {'Name', 'a', 'b', 'c', 'alpha', 'beta', 'gamma', 'SpacegroupNumber', ...
                'Delta_a', 'Delta_b', 'Delta_c', 'Delta_alpha', 'Delta_beta', 'Delta_gamma', ...
                'Latt', 'UnitCell', 'AsymmetricUnit', 'Symm', 'Setting', 'SFAC', 'UNIT'};
            for iP = 1:numel(CrProps)
                eval(['S.', CrProps{iP}, ' = obj.', CrProps{iP}, ';']);
            end
            
            S.hkl = obj.hkl;
            S.Orientation = obj.Orientation;
            
            S.BraggVisible = obj.BraggVisible;
            S.BraggSize = obj.BraggSize;
            S.BraggEdgeColor = obj.BraggEdgeColor;
            S.BraggFaceColor = obj.BraggFaceColor;
            S.BraggMarker = obj.BraggMarker;

            S.DSVisible = obj.DSVisible;
            S.DSLineWidth = obj.DSLineWidth;
            S.DSLineStyle = obj.DSLineStyle;
            S.DSColor = obj.DSColor;

            S.LabelVisible = obj.LabelVisible;
            S.LabelFontSize = obj.LabelFontSize;
            S.LabelForeColor = obj.LabelForeColor;
            S.LabelBackgroundColor = obj.LabelBackgroundColor;
            S.LabelOffset = obj.LabelOffset;
            S.LabelLocation = obj.LabelLocation;
            S.LabelFontWeight = obj.LabelFontWeight;
            S.LabelFontAngle = obj.LabelFontAngle;
            S.LabelFontName = obj.LabelFontName;

            S.SFVisible = obj.SFVisible;
            S.SFLineWidth = obj.SFLineWidth;
            S.SFLineStyle = obj.SFLineStyle;
            S.SFColor = obj.SFColor;
            S.SFMultiplier = obj.SFMultiplier;
            S.SFProportionality = obj.SFProportionality;
            
            S.OverallVisibility = obj.OverallVisibility;

            S.IncludeSampleRotation = obj.IncludeSampleRotation;
        end
        
        function obj = reload(obj, S)
%             obj = Crystal_Peak_New(gca, GIDVis_Crystal(S.a, S.b, S.c, S.alpha, S.beta, S.gamma, S.Name, S.SpacegroupNumber, S.AsymmetricUnit, S.Setting, S.SFAC, S.UNIT), S.hkl, S.Orientation);

            CrProps = {'Name', 'a', 'b', 'c', 'alpha', 'beta', 'gamma', 'SpacegroupNumber', ...
                'Delta_a', 'Delta_b', 'Delta_c', 'Delta_alpha', 'Delta_beta', 'Delta_gamma', ...
                'Latt', 'UnitCell', 'AsymmetricUnit', 'Symm', 'Setting', 'SFAC', 'UNIT'};
            for iP = 1:numel(CrProps)
                eval(['obj.', CrProps{iP}, ' = S.', CrProps{iP}, ';']);
            end
            
            if isfield(S, 'Spacegroup') && ~isempty(S.Spacegroup)
                obj.Spacegroup = S.Spacegroup;
            end
            
            obj.SFVisible = S.SFVisible;
            obj.SFLineWidth = S.SFLineWidth;
            obj.SFLineStyle = S.SFLineStyle;
            obj.SFColor = S.SFColor;
            obj.SFMultiplier = S.SFMultiplier;
            obj.SFProportionality = S.SFProportionality;
            
            obj.BraggVisible = S.BraggVisible;
            obj.BraggSize = S.BraggSize;
            obj.BraggEdgeColor = S.BraggEdgeColor;
            obj.BraggFaceColor = S.BraggFaceColor;
            obj.BraggMarker = S.BraggMarker;

            obj.DSVisible = S.DSVisible;
            obj.DSLineWidth = S.DSLineWidth;
            obj.DSLineStyle = S.DSLineStyle;
            obj.DSColor = S.DSColor;

            obj.LabelVisible = S.LabelVisible;
            obj.LabelFontSize = S.LabelFontSize;
            obj.LabelForeColor = S.LabelForeColor;
            obj.LabelBackgroundColor = S.LabelBackgroundColor;
            obj.LabelOffset = S.LabelOffset;
            obj.LabelLocation = S.LabelLocation;
            obj.LabelFontWeight = S.LabelFontWeight;
            obj.LabelFontAngle = S.LabelFontAngle;
            obj.LabelFontName = S.LabelFontName;
            
            if isfield(S, 'OverallVisibility')
                obj.OverallVisibility = S.OverallVisibility;
            end
            if isfield(S, 'IncludeSampleRotation')
                obj.IncludeSampleRotation = S.IncludeSampleRotation;
            end
        end
    end
    methods (Static)
        function obj = loadobj(S)
            % Constructs a Crystal_Peak_New object
            % loadobj used when a superclass object is saved directly
            % Calls reload to assign property values retrived from struct
            % loadobj must be Static so it can be called without object
%             obj = reload(Crystal_Peak_New(gca, GIDVis_Crystal(S.a, S.b, S.c, S.alpha, S.beta, S.gamma, S.Name, S.SpacegroupNumber, S.AsymmetricUnit, S.Setting, S.SFAC, S.UNIT), S.hkl, S.Orientation), S);
            obj = reload(Crystal_Peak_New(gca, S, S.hkl, S.Orientation), S);
            UpdatePlot(obj);
        end
    end
end % classdef