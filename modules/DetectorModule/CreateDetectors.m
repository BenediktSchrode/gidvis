% This file is used to create the detectors available in GIDVis. This file
% is run if there is no Detectors.mat file in the DetectorModule directory.
% If there is already a file, this function should not be run to avoid
% overwriting of user entered values.
%
% The detectors stored in the Detectors.mat file are used within GIDVis at
% the following places:
%     - Detector Module: To add, delete and edit detectors
%     - Calibration Wizard: Select a detector
%     - Beamline Setup Module: Load the properties of a detector
%     - Setup Simulation Module: Simulate the space map for different
%       settings
% The file Detectors.mat should only be modified by using the Detector
% Module (except on first GIDVis start).
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2019 Benedikt Schrode, Christian Roethel, Stefan          %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear Detectors

% Define the detectors. They will appear inside GIDVis in the same order as
% they appear here. So make sure that you sort new detectors correctly into
% this list and to follow the scheme.
Detectors(1) = XDetector('Eiger 500K');
Detectors(2) = XDetector('Eiger 1M');
Detectors(3) = XDetector('Eiger 4M');
Detectors(4) = XDetector('Eiger 9M');
Detectors(5) = XDetector('Eiger 16M');
Detectors(6) = XDetector('Pilatus 100K');
Detectors(7) = XDetector('Pilatus 300K');
Detectors(8) = XDetector('Pilatus 300K-W');
Detectors(9) = XDetector('Pilatus 1M');
Detectors(10) = XDetector('Pilatus 2M');
Detectors(11) = XDetector('Rayonix SX165 2x2');
Detectors(12) = XDetector('Rayonix SX165 4x4');
Detectors(13) = XDetector('Rayonix SX165 8x8');
Detectors(14) = XDetector('Vantec 2000');


% get the file paths of this file
filepath = mfilename('fullpath');
% get the parent directory
PDir = fileparts(filepath);

% save the Detectors structure into a file in the modules/DetectorModule
% directory
save(fullfile(PDir, 'Detectors.mat'), 'Detectors');