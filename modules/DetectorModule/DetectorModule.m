function varargout = DetectorModule(varargin)
% This file, together with DetectorModule.fig, is the UI for the Detector
% Module.
% 
% Usage:
%     varargout = DetectorModule(MainAxesHandle)
% 
% Input: 
%     MainAxesHandle ... handle to main axes
%
% Output:
%     varargout ... default GUIDE output
%
%
% This file is part of GIDVis.
%
% see also: XDetector, InitializeDetectorModule, CreateDetectors, 
% GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Edit the above text to modify the response to help DetectorModule

% Last Modified by GUIDE v2.5 09-Apr-2019 13:00:46

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @DetectorModule_OpeningFcn, ...
                   'gui_OutputFcn',  @DetectorModule_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before DetectorModule is made visible.
function DetectorModule_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to DetectorModule (see VARARGIN)

% Choose default command line output for DetectorModule
handles.output = hObject;

% get the filepath of this file
filepath = mfilename('fullpath');
% get the parent directory
PDir = fileparts(filepath);

% load detectors
S = load(fullfile(PDir, 'Detectors.mat'));
D = S.Detectors;

% put the detector names into the popupmenu
set(handles.PM_Detector, 'String', {D.Name});

% store detectors in handles structure
handles.Detectors = D;

% add a listener to the axes in varargin{1}. When the axes gets destroyed 
% (e.g. because the figure containing the axes gets closed), the callback 
% will be executed (and closes this window).
% Store the listener in the handles structure to remove it when this window    
% gets closed by user, otherwise the callback is executed even when the
% DetectorModule window does not exist anymore.
handles.MainAxesDestroyedListener = addlistener(varargin{1}, 'ObjectBeingDestroyed', @(src, evt) MainAxesDestroyed_Callback(src, evt, handles.Fig_DetectorModule));

% Update handles structure
guidata(hObject, handles);

% call the callback of the detector popupmenu once to update the display
PM_Detector_Callback(handles.PM_Detector, [], handles);

% UIWAIT makes DetectorModule wait for user response (see UIRESUME)
% uiwait(handles.Fig_DetectorModule);

function MainAxesDestroyed_Callback(src, evt, HandleToModuleWindow)
% close module window
close(HandleToModuleWindow);

% --- Outputs from this function are returned to the command line.
function varargout = DetectorModule_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in PM_Detector.
function PM_Detector_Callback(hObject, eventdata, handles)
% hObject    handle to PM_Detector (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns PM_Detector contents as cell array
%        contents{get(hObject,'Value')} returns selected item from PM_Detector

% get selected index
ind = get(handles.PM_Detector, 'Value');

% get detector
D = handles.Detectors(ind);

% assign the properties to the corresponding fields
set(handles.TB_detlenx, 'String', D.detlenx);
set(handles.TB_detlenz, 'String', D.detlenz);
set(handles.TB_psx, 'String', D.psx);
set(handles.TB_psz, 'String', D.psz);
set(handles.TB_InactiveRows, 'String', num2str(D.InactiveRows));
set(handles.TB_InactiveColumns, 'String', num2str(D.InactiveColumns));
set(handles.TB_Material, 'String', D.DetectorMaterial);
set(handles.TB_Thickness, 'String', D.DetectorMaterialThickness);
set(handles.TB_Density, 'String', D.DetectorMaterialDensity);

% get the content of the shape popupmenu
content = get(handles.PM_Shape, 'String');
% get the index corresponding to the shape of the detector
indS = find(strcmp(D.Shape, content));
% set the index
set(handles.PM_Shape, 'Value', indS);

% --- Executes during object creation, after setting all properties.
function PM_Detector_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PM_Detector (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function DetectorPropertiesChanged(hObject)
% get handles structure
handles = guidata(hObject);
% get selected detector index
ind = get(handles.PM_Detector, 'Value');
% get the currently selected detector
D = handles.Detectors(ind);

% set the properties of the detector
try
    D.detlenx = str2double(get(handles.TB_detlenx, 'String'));
catch ex
    msgbox(ex.message, 'modal')
    set(handles.TB_detlenx, 'String', D.detlenx);
end
try
    D.detlenz = str2double(get(handles.TB_detlenz, 'String'));
catch ex
    msgbox(ex.message, 'modal')
    set(handles.TB_detlenz, 'String', D.detlenz);
end
try
    D.psx = str2double(get(handles.TB_psx, 'String'));
catch ex
    msgbox(ex.message, 'modal')
    set(handles.TB_psx, 'String', D.psx);
end
try
    D.psz = str2double(get(handles.TB_psz, 'String'));
catch ex
    msgbox(ex.message, 'modal')
    set(handles.TB_psz, 'String', D.psz);
end
try
    D.InactiveRows = str2num(get(handles.TB_InactiveRows, 'String'));
    set(handles.TB_InactiveRows, 'String', D.InactiveRows);
catch ex
    msgbox(ex.message, 'modal')
    set(handles.TB_InactiveRows, 'String', D.InactiveRows);
end
try
    D.InactiveColumns = str2num(get(handles.TB_InactiveColumns, 'String'));
    set(handles.TB_InactiveColumns, 'String', D.InactiveColumns);
catch ex
    msgbox(ex.message, 'modal')
    set(handles.TB_InactiveColumns, 'String', D.InactiveColumns);
end
try
    D.DetectorMaterial = get(handles.TB_Material, 'String');
catch ex
    msgbox(ex.message, 'modal')
    set(handles.TB_Material, 'String', D.DetectorMaterial);
end
try
    D.DetectorMaterialThickness = str2num(get(handles.TB_Thickness, 'String'));
    set(handles.TB_Thickness, 'String', D.DetectorMaterialThickness);
catch ex
    msgbox(ex.message, 'modal')
    set(handles.TB_Thickness, 'String', D.DetectorMaterialThickness);
end
try
    D.DetectorMaterialDensity = str2num(get(handles.TB_Density, 'String'));
    set(handles.TB_Density, 'String', D.DetectorMaterialDensity);
catch ex
    msgbox(ex.message, 'modal')
    set(handles.TB_Density, 'String', D.DetectorMaterialDensity);
end
content = get(handles.PM_Shape, 'String');
D.Shape = content{get(handles.PM_Shape, 'Value')};

% store the detector in the handles structure
handles.Detectors(ind) = D;

% update handles structure
guidata(hObject, handles);


function TB_detlenx_Callback(hObject, eventdata, handles)
% hObject    handle to TB_detlenx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_detlenx as text
%        str2double(get(hObject,'String')) returns contents of TB_detlenx as a double
DetectorPropertiesChanged(hObject)

% --- Executes during object creation, after setting all properties.
function TB_detlenx_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_detlenx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_detlenz_Callback(hObject, eventdata, handles)
% hObject    handle to TB_detlenz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_detlenz as text
%        str2double(get(hObject,'String')) returns contents of TB_detlenz as a double
DetectorPropertiesChanged(hObject)

% --- Executes during object creation, after setting all properties.
function TB_detlenz_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_detlenz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_psx_Callback(hObject, eventdata, handles)
% hObject    handle to TB_psx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_psx as text
%        str2double(get(hObject,'String')) returns contents of TB_psx as a double
DetectorPropertiesChanged(hObject)

% --- Executes during object creation, after setting all properties.
function TB_psx_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_psx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_psz_Callback(hObject, eventdata, handles)
% hObject    handle to TB_psz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_psz as text
%        str2double(get(hObject,'String')) returns contents of TB_psz as a double
DetectorPropertiesChanged(hObject)

% --- Executes during object creation, after setting all properties.
function TB_psz_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_psz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_InactiveRows_Callback(hObject, eventdata, handles)
% hObject    handle to TB_InactiveRows (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_InactiveRows as text
%        str2double(get(hObject,'String')) returns contents of TB_InactiveRows as a double
DetectorPropertiesChanged(hObject)

% --- Executes during object creation, after setting all properties.
function TB_InactiveRows_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_InactiveRows (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_InactiveColumns_Callback(hObject, eventdata, handles)
% hObject    handle to TB_InactiveColumns (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_InactiveColumns as text
%        str2double(get(hObject,'String')) returns contents of TB_InactiveColumns as a double
DetectorPropertiesChanged(hObject)

% --- Executes during object creation, after setting all properties.
function TB_InactiveColumns_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_InactiveColumns (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in PM_Shape.
function PM_Shape_Callback(hObject, eventdata, handles)
% hObject    handle to PM_Shape (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns PM_Shape contents as cell array
%        contents{get(hObject,'Value')} returns selected item from PM_Shape
DetectorPropertiesChanged(hObject)

% --- Executes during object creation, after setting all properties.
function PM_Shape_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PM_Shape (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_Material_Callback(hObject, eventdata, handles)
% hObject    handle to TB_Material (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_Material as text
%        str2double(get(hObject,'String')) returns contents of TB_Material as a double
DetectorPropertiesChanged(hObject)

% --- Executes during object creation, after setting all properties.
function TB_Material_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_Material (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_Thickness_Callback(hObject, eventdata, handles)
% hObject    handle to TB_Thickness (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_Thickness as text
%        str2double(get(hObject,'String')) returns contents of TB_Thickness as a double
DetectorPropertiesChanged(hObject)

% --- Executes during object creation, after setting all properties.
function TB_Thickness_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_Thickness (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_Density_Callback(hObject, eventdata, handles)
% hObject    handle to TB_Density (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_Density as text
%        str2double(get(hObject,'String')) returns contents of TB_Density as a double
DetectorPropertiesChanged(hObject)

% --- Executes during object creation, after setting all properties.
function TB_Density_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_Density (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Btn_AddDetector.
function Btn_AddDetector_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_AddDetector (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
DetectorName = inputdlg('Enter Detector Name', 'New Detector', 1);

if isempty(DetectorName); return; end

DetectorName = DetectorName{1};

D = handles.Detectors;
% check if detector name already exists
if any(strcmp(DetectorName, {D.Name}))
    msgbox('Detector with this name already exists. Aborting.', 'modal');
    return;
end

% create a detector
NewD = XDetector(DetectorName, 1000, 500, 0.1, 0.1, [], [], ...
    'rectangular', '', [], []);

% append it to the already existing detectors
handles.Detectors(end+1) = NewD;

% set the names in the detector popupmenu
set(handles.PM_Detector, 'String', {handles.Detectors.Name});

% set the index
set(handles.PM_Detector, 'Value', numel(handles.Detectors));

% update handles structure
guidata(hObject, handles);

% run the PM_Detector_Callback to update the display
PM_Detector_Callback(handles.PM_Detector, [], handles);


% --- Executes on button press in Btn_DeleteDetector.
function Btn_DeleteDetector_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_DeleteDetector (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% get the detectors
D = handles.Detectors;

% do not allow deleting the last detector
if numel(D) == 1
    msgbox('You cannot delete the last detector.', 'modal')
    return;
end

% get the selected index
ind = get(handles.PM_Detector, 'Value');

% set the index to 1 to avoid out-of-range
set(handles.PM_Detector, 'Value', 1);

% delete the detector
D(ind) = [];

% store D in handles structure
handles.Detectors = D;

% set the names in the detector popupmenu
set(handles.PM_Detector, 'String', {handles.Detectors.Name});

% update handles structure
guidata(hObject, handles);

% run the PM_Detector_Callback to update the display
PM_Detector_Callback(handles.PM_Detector, [], handles);


% --- Executes on button press in Btn_SaveExit.
function Btn_SaveExit_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_SaveExit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% disable the save and exit button
set(hObject, 'Enable', 'off');
drawnow;

% get the file path of this file
filepath = mfilename('fullpath');
% get the parent directory
PDir = fileparts(filepath);

% get the detectors
Detectors = handles.Detectors;

% save the Detectors structure into a file in the modules/CalibrationWizard
% directory
save(fullfile(PDir, 'Detectors.mat'), 'Detectors');

% close the module
close(ancestor(hObject, 'figure'))


% --- Executes when user attempts to close Fig_DetectorModule.
function Fig_DetectorModule_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to Fig_DetectorModule (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% delete the listener to the AxesDestroyed event. Otherwise it will still
% listen and try to execute the MainAxesDestroyed_Callback function, even
% though the Module1 window is already closed.
delete(handles.MainAxesDestroyedListener);

% Hint: delete(hObject) closes the figure
delete(hObject);


% --- Executes on button press in Btn_PlotPixels.
function Btn_PlotPixels_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_PlotPixels (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% get the detector
D = handles.Detectors(get(handles.PM_Detector, 'Value'));

% create a plot to show the pixel distribution
f = figure;
ax = axes('Parent', f);
imagesc(ax, Pixels(D));
xlabel(ax, 'Pixel x');
ylabel(ax, 'Pixel z');
title(ax, D.Name);
axis(ax, 'image')
colormap(parula(2))
cb = colorbar(ax);
caxis([0 1])
set(cb, 'YTick', [0.25 0.75])
set(cb, 'YTickLabel', {'inactive', 'active'})


% --- Executes on button press in Btn_Exit.
function Btn_Exit_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_Exit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(ancestor(hObject, 'figure'))
