function InitializeDetectorModule
% This function checks if the file Detectors.mat is available. If not, it
% runs the function CreateDetectors to create the file with the detectors
% in it. If available, nothing is run to avoid overwriting values the user
% might have entered/changed.
%
% see also: CreateDetectors, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2019 Benedikt Schrode, Christian Roethel, Stefan          %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% get the file paths of this file
filepath = mfilename('fullpath');
% get the parent directory
PDir = fileparts(filepath);

% create the file path where the file Detectors.mat should be
FilePath = fullfile(PDir, 'Detectors.mat');

% if it does not exist, create the detectors list
if exist(FilePath, 'file') ~= 2
    CreateDetectors
end