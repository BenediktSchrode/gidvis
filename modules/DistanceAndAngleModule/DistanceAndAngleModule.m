function DistanceAndAngleModule(FigTB, AxesHandle, SeparatorOnOff)
% This function adds a toggle button to the figure toolbar given by FigTB.
% This button switches on/off the distance and angle mode.
%
% Usage:
%     DistanceAndAngleModule(FigTB, AxesHandle, SeparatorOnOff)
%
% Input:
%     FigTB ... handle to the toolbar where the button should be added to
%     AxesHandle ... handle of the axes where distances and angles should
%                    be measured
%     SeparatorOnOff ... string ('on' or 'off') defining whether a
%                        separator should be drawn left of the toggle tool
%                        or not
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Load the image for the toggle button
Img = imread('DistanceAndAngleIcon.png');

% add the toggle button to the toolbar
ToggleButton = uitoggletool(FigTB, ...
    'OnCallback', @(src, evt) DAAOn(src, evt, AxesHandle), ...
    'Tag', 'DistanceAndAngleToggleButton', ...
    'CData', Img, ...
    'Separator', SeparatorOnOff, ...
    'TooltipString', 'Distance and Angle');

% add a listener to the ToggleButton's ObjectBeingDestroyed event. Here we
% close the result figure if one is still open.
addlistener(ToggleButton, 'ObjectBeingDestroyed', @ToggleButtonDestroyed_Callback);

% the settings for this module are stored in the same directory as the file
% DistanceAndAngleModule.m. Get the path to the Settings.mat file.
SettingsPath = fullfile(fileparts(mfilename('fullpath')), 'Settings.mat');

% read out settings if available
if exist(SettingsPath, 'file') == 2
    S = load(SettingsPath);
    Mode = S.SettingsDAAM.Mode;
    Color = S.SettingsDAAM.Color;
else % use default values
    Mode = 'horizontal';
    Color = [0 0 1];
end

function ToggleButtonDestroyed_Callback(src, evt)
if strcmpi(get(src, 'State'), 'on') % if the distance and angle mode is still on,
    set(src, 'State', 'off'); % turn it off by setting the toggle button state to 'off'
end
end

function DAAOn(src, evt, AxesHandle)
% executes when the toggle button gets turned on

% check if there is a license for the image toolbox
[stat, msg] = license('checkout', 'Image_Toolbox');
if ~stat
    set(src, 'State', 'off');
    msgbox(msg, 'License Checkout Failed', 'modal');
    return;
end

% find the image handle
imgHandle = findobj(AxesHandle, 'Tag', 'MainImage');
if isempty(imgHandle) % occurs e.g. when no GID image is loaded yet
    set(src, 'State', 'off');
    msgbox('You cannot measure distances and angles of the currently displayed image. Select a GID image in the "Load File" window (File - Select Folder) first.', 'Cannot Perform Measurement', 'modal');
    return;
end

% set the OffCallback of the toggle button.
set(src, 'OffCallback', @(src, evt) DAAOff(src, evt, [], get(AxesHandle, 'Parent')));

% switch off all other toggle buttons in the toolbar except for src to
% avoid using multiple tools at once
SwitchOffToolstripButtons(ancestor(AxesHandle, 'figure'), src);

% create an imdistangleline on the axes
h = imdistangleline(AxesHandle, Mode, Color);

% user might have aborted creation using 'Esc' --> h will be empty
if isempty(h)
    set(src, 'State', 'off');
    return;
end

% update the OffCallback with the handle to the imdistangleline
set(src, 'OffCallback', @(src, evt) DAAOff(src, evt, h, get(AxesHandle, 'Parent')));

% add listener to ObjectBeingDestroyed of the imline, in order to set the
% button's state to off as soon as the imline gets deleted.
addlistener(h, 'ObjectBeingDestroyed', @(obj, event) ImlineDeletion(obj, event, src));
end

function ImlineDeletion(ImlineHandle, event, ToolbarButtonHandle)
Mode = ImlineHandle.MeasurementMode;
Color = getColor(ImlineHandle);
if ishandle(ToolbarButtonHandle)
    set(ToolbarButtonHandle, 'State', 'off');
end

% create the settings structure
SettingsDAAM.Mode = Mode;
SettingsDAAM.Color = Color;

% in order to avoid error when no write access for file use try.
try; save(SettingsPath, 'SettingsDAAM'); end
end

function DAAOff(src, evt, DistAngleLineHandle, GIDVisFigureHandle)
% delete the listener to the data change
if ~isempty(get(src, 'UserData'))
    delete(get(src, 'UserData'))
end
set(src, 'UserData', []);

% try to delete the DistAngleLineHandle
try
    delete(DistAngleLineHandle);
end

% in case we're still waiting for the imline, we have to abort this
stat = get(GIDVisFigureHandle, 'waitstatus');
if strcmp(stat, 'waiting')
    % use java robot to simulate 'Esc' key press (only way I know to abort
    % imroi stuff) #BS
    robot = java.awt.Robot;
    robot.keyPress    (java.awt.event.KeyEvent.VK_ESCAPE);
    robot.keyRelease  (java.awt.event.KeyEvent.VK_ESCAPE);   
end

end
end