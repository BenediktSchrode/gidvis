classdef imdistangleline < imline
    %IMDISTANGLELINE Create an interactive, draggable line to measure
    %distances and angles
    %
    %   This class combines features of imline (interactive placement) with 
    %   the distance measurement tool imdistline
    %
    %
    % This file is part of GIDVis.
    %
    % see also: imdistline, imline, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2018 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    properties (Access = protected)
        LabelHandle % handle of the label showing distance and angle
        VerHorLineUpperHandle % handle to the upper vertical/horizontal line
        CircleLineUpperHandle % handle to the upper circle segment line
        VerHorLineLowerHandle % handle to the lower vertical/horizontal line
        CircleLineLowerHandle % handle to the lower circle segment line
    end
    
    properties (SetAccess = protected)
        MeasurementMode = 'horizontal';
    end
    
    methods
        function obj = imdistangleline(ax, Mode, Color)
            % Create an imdistangleline object.
            %
            %
            % imdistangleline() creates an imdistangleline object in the
            % current axes.
            %
            % imdistangleline(ax) creates an imdistangleline object in the
            % axes specified by ax.
            %
            % imdistangleline(ax, Mode) creates an imdistangleline object
            % in the axes specified by ax with MeasurementMode Mode.
            %
            % h = imdistangleline(...) returns the handle to the
            % imdistangleline object.
            
            % if no input given for ax, we use gca.
            if nargin < 1 || isempty(ax)
                ax = gca;
            end
            % if no input given for Mode we use horizontal
            if nargin < 2 || isempty(Mode)
                Mode = 'horizontal';
            end
            % if no input given for color, use the standard blue
            if nargin < 3 || isempty(Color)
                Color = [0.2824 0.2824 0.9725];
            end
            
            % we have to draw the lines before the imline. Otherwise the
            % one end of the imline is below the lines and cannot be
            % dragged properly.
            
            % draw the lower lines, they are below the upper lines
            % lower horizontal line
            HLLH = line([0 0], [0 0], 'Color', 'w', 'LineWidth', 1.6, ...
                'XLimInclude', 'off', 'YLimInclude', 'off');
            
            % lower circle line
            CLLH = line([0 0], [0 0], 'Color', 'w', 'LineWidth', 1.6, ...
                'XLimInclude', 'off', 'YLimInclude', 'off');
            
            % draw the upper lines on top of the lower lines
            % upper horizontal line
            HLUH = line([0 0], [0 0], 'LineWidth', 1, ...
                'XLimInclude', 'off', 'YLimInclude', 'off');
            
            % upper circle line
            CLUH = line([0 0], [0 0], 'LineWidth', 1, ...
                'XLimInclude', 'off', 'YLimInclude', 'off');
            
            % call the superclass (imline) constructor. This will start
            % interactive placement of the line
            obj = obj@imline(ax);
            
            % user might have cancelled placement by 'Esc' key
            if isempty(obj)
                % delete the already created lines
                delete(HLLH);
                delete(CLLH);
                delete(HLUH);
                delete(CLUH);
                return;
            end
            
            % set the color of the imline object
            setColor(obj, Color);
            
            % get the position
            Pos = getPosition(obj);
            if strcmpi(get(ax, 'YDir'), 'reverse')
                % to measure into the right direction, flip the position
                Pos = flipud(Pos);
                setPosition(obj, Pos);
            end
            
            % assign the already created lines to the obj
            obj.VerHorLineLowerHandle = HLLH;
            obj.CircleLineLowerHandle = CLLH;
            obj.VerHorLineUpperHandle = HLUH;
            obj.CircleLineUpperHandle = CLUH;
            
            % set the color of the upper lines to the same as the imline
            set([obj.VerHorLineUpperHandle, obj.CircleLineUpperHandle], ...
                'Color', getColor(obj));
            
            % place a label
            obj.LabelHandle = text(0, 0, '', ...
                'BackgroundColor', 'w', 'FontSize', 10);
            
            % set the MeasurementMode
            obj.MeasurementMode = Mode;
            
            % call the NewPositionCallback function once to update label
            % position and its content
            pos = getPosition(obj);
            NewPositionCallback(pos, obj);
            
            % add a callback for new position of the line. In it, update
            % label position and its content
            addNewPositionCallback(obj, @(pos) NewPositionCallback(pos, obj));
            
            % add a context menu entry to change the label visibility to 
            % the already existing context menu of the imline
            cm = getContextMenu(obj);
            LVM = uimenu(cm, 'Label', 'Show Distance Label', 'Tag', 'uicnt_ShowDistanceLabel', 'Callback', @obj.UIDistance_Callback);
            uimenu(LVM, 'Label', 'Label On', 'Tag', 'uicnt_LabelVisibilityOn', ...
                'Callback', {@obj.UILabel_Callback, 'on'});
            uimenu(LVM, 'Label', 'Label Off', 'Tag', 'uicnt_LabelVisibilityOff', ...
                'Callback', {@obj.UILabel_Callback, 'off'});
            
            % create a context menu to copy the length of the line to the
            % clipboard
            uimenu(cm, 'Label', 'Copy Length', 'Tag', 'uicnt_CopyLength', ...
                'Callback', @obj.UICopyDistance_Callback);
            
            % create a context menu to copy the angle to the clipboard
            uimenu(cm, 'Label', 'Copy Angle', 'Tag', 'uicnt_CopyAngle', ...
                'Callback', @obj.UICopyAngle_Callback);
            
            % create a context menu to place one of the drag markers at the
            % origin (0, 0)
            uimenu(cm, 'Label', 'Place at origin', 'Tag', 'uicnt_PlaceAtOrigin', ...
                'Callback', @obj.UIPlaceAtOrigin_Callback);
            
            % create a context menu to change the measurement mode
            uim = uimenu(cm, 'Label', 'Measurement Mode', ...
                'Tag', 'uicnt_MeasurementMode', ...
                'Callback', @obj.UIMeasurementMode);
            uimenu(uim, 'Label', 'To Horizontal', 'Tag', 'uicnt_Horizontal', ...
                'Callback', {@obj.UIChangeMeasurementMode, 'Horizontal'});
            uimenu(uim, 'Label', 'To Vertical', 'Tag', 'uicnt_Vertical', ...
                'Callback', {@obj.UIChangeMeasurementMode, 'Vertical'});
            
            % reorder the context menu
            Ch = get(cm, 'Children');
            if numel(Ch) == 8
                set(cm, 'Children', Ch([6 7 5 1 2 8 4 3])); 
            elseif numel(Ch) == 7
                set(cm, 'Children', Ch([5 4 6 1 7 3 2]));
            end
            
            % get the context menu item to change the color
            ColorChange = findobj(Ch, 'Tag', 'set color cmenu item');
            % get the children of these items. Their callback will change
            % the color of the imline
            ColorChangeChildren = get(ColorChange, 'Children');

            % add a callback to the already existing callback of the uimenu
            % items. In there, change the color of the horizontal and
            % circle line
            for iC = 1:numel(ColorChangeChildren)
                iptaddcallback(ColorChangeChildren(iC), 'Callback', @obj.AdditionalSetColor);
            end
            
            % set the context menu also for the label and the lines
            set(obj.LabelHandle, 'UIContextMenu', cm);
            set(obj.VerHorLineUpperHandle, 'UIContextMenu', cm);
            set(obj.CircleLineUpperHandle, 'UIContextMenu', cm);
            set(obj.VerHorLineLowerHandle, 'UIContextMenu', cm);
            set(obj.CircleLineLowerHandle, 'UIContextMenu', cm);
        end
        
        function Res = getLabelVisible(obj)
            Res = get(obj.LabelHandle, 'Visible');
        end
        
        function setLabelVisible(obj, NewStatus)
            obj.ChangeLabelVisible(NewStatus);
        end
        
        function Val = getLength(obj)
            Val = CalculateLength(obj);
        end
        
        function Val = getAngle(obj)
            Val = CalculateAngle(obj);
        end
        
        function delete(obj)
            % in the delete callback, we have to delete all the plotted
            % elements
            if ishghandle(obj.h_group)
                delete(obj.h_group);
            end
            if ishghandle(obj.LabelHandle)
                delete(obj.LabelHandle);
            end
            if ishghandle(obj.VerHorLineUpperHandle)
                delete(obj.VerHorLineUpperHandle);
            end
            if ishghandle(obj.CircleLineUpperHandle)
                delete(obj.CircleLineUpperHandle);
            end
            if ishghandle(obj.VerHorLineLowerHandle)
                delete(obj.VerHorLineLowerHandle);
            end
            if ishghandle(obj.CircleLineLowerHandle)
                delete(obj.CircleLineLowerHandle);
            end
        end
    end
    
    methods (Access = protected)
        function AdditionalSetColor(obj, src, evt)
            Col = getColor(obj);
            set(obj.VerHorLineUpperHandle, 'Color', Col);
            set(obj.CircleLineUpperHandle, 'Color', Col);
        end
        
        function UICopyAngle_Callback(obj, src, evt)
            A = obj.getAngle();
            clipboard('copy', A)
        end
        
        function UICopyDistance_Callback(obj, src, evt)
            L = obj.getLength();
            clipboard('copy', L)
        end
        
        function UIPlaceAtOrigin_Callback(obj, src, evt)
            % get the position
            pos = getPosition(obj);
            ax = ancestor(obj.CircleLineLowerHandle, 'axes');
            % get the clicked position
            ClickedPos = get(ax, 'CurrentPoint');
            ClickedPos = ClickedPos(1, 1:2);
            % get the point of the imline closest to the clicked position
            Delta = sqrt(sum((pos - repmat(ClickedPos, 2, 1)).^2, 2));
            [~, ind] = min(Delta);
            % place the point closest to the clicked position at the origin
            pos(ind, :) = zeros(1, 2);
            % set the position
            setPosition(obj, pos);
        end
        
        function UIMeasurementMode(obj, src, evt)
            MenuHor = findobj(get(src, 'Children'), 'Tag', 'uicnt_Horizontal');
            MenuVer = findobj(get(src, 'Children'), 'Tag', 'uicnt_Vertical');
            if strcmpi(obj.MeasurementMode, 'Horizontal')
                set(MenuHor, 'Checked', 'on');
                set(MenuVer, 'Checked', 'off');
            elseif strcmpi(obj.MeasurementMode, 'Vertical')
                set(MenuVer, 'Checked', 'on');
                set(MenuHor, 'Checked', 'off');
            end
        end
        
        function UIChangeMeasurementMode(obj, src, evt, Mode)
            obj.MeasurementMode = Mode;
            NewPositionCallback(getPosition(obj), obj);
        end
        
        function UIDistance_Callback(obj, src, evt)
            C = get(src, 'Children');
            Vis = getLabelVisible(obj);
            OnCnt = findobj(C, 'Tag', 'uicnt_LabelVisibilityOn');
            OffCnt = findobj(C, 'Tag', 'uicnt_LabelVisibilityOff');
            if strcmp(Vis, 'on')
                set(OnCnt, 'Checked', 'on');
                set(OffCnt, 'Checked', 'off');
            else
                set(OnCnt, 'Checked', 'off');
                set(OffCnt, 'Checked', 'on');
            end
        end
        
        function UILabel_Callback(obj, src, evt, NewStatus)
            obj.ChangeLabelVisible(NewStatus);
        end
        
        function ChangeLabelVisible(obj, NewStatus)
            set(obj.LabelHandle, 'Visible', NewStatus);
        end
        
        function ToggleLabelVisible(obj, evt)
            if strcmp(getLabelVisible(obj), 'on')
                setLabelVisible(obj, 'off')
            else
                setLabelVisible(obj, 'on')
            end
        end
        
        function NewPositionCallback(pos, obj)
            % get length, angle and axes handle
            L = CalculateLength(obj);
            Angle = CalculateAngle(obj);
            ax = ancestor(obj.VerHorLineUpperHandle, 'axes');
            
            if strcmpi(obj.MeasurementMode, 'horizontal')
                % draw the horizontal line
                if strcmp(get(get(obj, 'Parent'), 'YDir'), 'reverse')
                    pos = flipud(pos);
                end
                miny = pos(1, 2);
                
                if pos(1, 1) > pos(2, 1)
                    x = [pos(1, 1) - L, pos(1, 1)];
                else
                    x = [pos(1, 1), pos(1, 1) + L];
                end
                set(obj.VerHorLineUpperHandle, ...
                    'XData', x, ...
                    'YData', repmat(miny, 1, 2));
                set(obj.VerHorLineLowerHandle, ...
                    'XData', x, ...
                    'YData', repmat(miny, 1, 2));
            
                % draw the circle segment with radius 4/5*L
                M = pos(1, :);
                x = 4/5*L.*cosd(linspace(0, Angle, 100));
                y = 4/5*L.*sind(linspace(0, Angle, 100));
                if pos(1, 1) > pos(2, 1)
                    x = -x;
                end
                if strcmp(get(get(obj, 'Parent'), 'YDir'), 'reverse')
                    y = -y;
                end
                x = x + M(1);
                y = y + M(2);

                set(obj.CircleLineUpperHandle, 'XData', x, 'YData', y);
                set(obj.CircleLineLowerHandle, 'XData', x, 'YData', y);
                
                % set the label position
                set(obj.LabelHandle, 'Position', [mean(pos(:, 1)), mean(pos(:, 2))]);
                
                % adjust alignment of the label
                if strcmpi(get(ax, 'XDir'), 'reverse')
                    pos(:, 1) = -pos(:, 1);
                end
                if strcmpi(get(ax, 'YDir'), 'reverse')
                    pos(:, 2) = -pos(:, 2);
                end
                if pos(2, 1) > pos(1, 1)
                    if pos(2, 2) > pos(1, 2)
                        set(obj.LabelHandle, 'VerticalAlignment', 'bottom', 'HorizontalAlignment', 'right');
                    else
                        set(obj.LabelHandle, 'VerticalAlignment', 'top', 'HorizontalAlignment', 'right');
                    end
                else
                    if pos(2, 2) > pos(1, 2)
                        set(obj.LabelHandle, 'VerticalAlignment', 'bottom', 'HorizontalAlignment', 'left');
                    else
                        set(obj.LabelHandle, 'VerticalAlignment', 'top', 'HorizontalAlignment', 'left');
                    end
                end
            elseif strcmpi(obj.MeasurementMode, 'vertical')
                Angle = 90-Angle;
                % draw the vertical line
                if strcmp(get(get(obj, 'Parent'), 'YDir'), 'reverse')
                    pos = flipud(pos);
                end
                minx = pos(1, 1);
                if strcmp(get(get(obj, 'Parent'), 'YDir'), 'reverse')
                    y = [pos(1, 2), pos(1, 2) - L];
                else
                    y = [pos(1, 2), pos(1, 2) + L];
                end
                set(obj.VerHorLineUpperHandle, ...
                    'XData', repmat(minx, 1, 2), ...
                    'YData', y);
                set(obj.VerHorLineLowerHandle, ...
                    'XData', repmat(minx, 1, 2), ...
                    'YData', y);
                
                % draw the circle segment with radius 4/5*L
                M = pos(1, :);
                x = 4/5*L.*cosd(linspace(90, -Angle+90, 100));
                y = 4/5*L.*sind(linspace(90, -Angle+90, 100));
                if pos(1, 1) > pos(2, 1)
                    x = -x;
                end
                if strcmp(get(get(obj, 'Parent'), 'YDir'), 'reverse')
                    y = -y;
                end
                x = x + M(1);
                y = y + M(2);

                set(obj.CircleLineUpperHandle, 'XData', x, 'YData', y);
                set(obj.CircleLineLowerHandle, 'XData', x, 'YData', y);
                
                % set the label position
                set(obj.LabelHandle, 'Position', [mean(pos(:, 1)), mean(pos(:, 2))], ...
                    'VerticalAlignment', 'top');
                
                % adjust alignment of the label
                if strcmpi(get(ax, 'XDir'), 'reverse')
                    pos(:, 1) = -pos(:, 1);
                end
                if strcmpi(get(ax, 'YDir'), 'reverse')
                    pos(:, 2) = -pos(:, 2);
                end
                if pos(2, 1) > pos(1, 1)
                    if pos(2, 2) > pos(1, 2)
                        set(obj.LabelHandle, 'HorizontalAlignment', 'left')
                    else
                        set(obj.LabelHandle, 'HorizontalAlignment', 'right')
                    end
                else
                    if pos(2, 2) > pos(1, 2)
                        set(obj.LabelHandle, 'HorizontalAlignment', 'right')
                    else
                        set(obj.LabelHandle, 'HorizontalAlignment', 'left')
                    end
                end
            end
            
            % update the label text
            set(obj.LabelHandle, 'String', sprintf('L = %g,\nat %g%s', L, Angle, char(176)));
        end
        
        function L = CalculateLength(obj)
            L = sqrt(sum(diff(getPosition(obj)).^2));
        end
        
        function angle = CalculateAngle(obj)
            % get the position
            pos = getPosition(obj);
            
            angle = atan2(diff(pos(:, 2)), diff(pos(:, 1))) * 180/pi;
            if angle > 90
                angle = 180 - angle;
            elseif angle < -90
                angle = -(angle + 180);
            end
        end
    end
end