function hMruMenu = AddExportLineDataButton(f)
% adds a button to the File menu to export the x, y and if necessary, the z
% data of plot objects of type line using the ExportLineDataModule.
%
% Clicking on the created button will open the ExportLineDataModule window.
%
% Usage:
%     hMruMenu = AddExportLineDataButton(f)
%
% Input:
%     f ... handle of the figure where the export line data button should
%           be added to
%
% Output:
%     hMruMenu ... handle to the created uimenu
%
% Example:
%     f = figure; plot(1:10); AddExportLineDataButton(f);
%
%
% This file is part of GIDVis.
%
% see also: ExportLineDataModule, uimenu, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% find the File menu bar
hFileMenu = findall(f, 'tag', 'figMenuFile');
% add button to it
hMruMenu = uimenu('Label', 'Export Line Data...', 'Parent', hFileMenu, ...
    'Callback', {@ExportLineData_Callback, f}, 'Tag', 'ExportLineDataButton');

% reorder the items, so that the Export Line Data button is at 6th position
hAllMenuItems = allchild(hFileMenu);
hAllMenuItemsNewOrder = fliplr(hAllMenuItems([2:end-4,1,end-3,end-2,end-1,end]));
set(hFileMenu, 'Children',fliplr(hAllMenuItems([2:end-5,1,end-4,end-3,end-2,end-1,end])));  % place in 2nd position, just above the "Open" item

function ExportLineData_Callback(src, evt, f)
ExportLineDataModule(f);