function varargout = ExportLineDataModule(varargin)
% Open window to select line data to export.
%
% This function lists all available lines in a figure f and allows the user
% to select the data he wants to export. To avoid a line from showing up in
% the list, set its 'HandleVisibility' property to 'off'.
%
%
% Usage:
%     varargout = ExportLineDataModule(f)
% 
% Input:
%     f ... handle to the figure from where lines should be exported
%
% Output:
%     varargout ... default GUIDE output
%
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Last Modified by GUIDE v2.5 14-Oct-2019 17:16:47

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ExportLineDataModule_OpeningFcn, ...
                   'gui_OutputFcn',  @ExportLineDataModule_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ExportLineDataModule is made visible.
function ExportLineDataModule_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ExportLineDataModule (see VARARGIN)

% Choose default command line output for ExportLineDataModule
handles.output = hObject;

% the settings for this module are stored in the same directory as the file
% LoadFileModule.m. Store its path in the handles structure to reuse it.
handles.SettingsPath = fullfile(fileparts(mfilename('fullpath')), 'Settings.mat');

if exist(handles.SettingsPath, 'file') == 2
    load(handles.SettingsPath);
    % store the settings in the handles structure
    handles.Settings = SettingsExportLineDataModule;
else % use some default
    handles.Settings = [];
    handles.Settings.SavePath = pwd;
    handles.Settings.SaveFilterIndex = 1;
end


Axs = findobj(get(varargin{1}, 'Children'), 'Type', 'axes');
LegendHandles = NaN(size(Axs));
C = cell(1, 4);
NoNameCnt = 1;
for iA = numel(Axs):-1:1
    Ax = Axs(iA);
    Ls = findobj(get(Ax, 'Children'), 'Type', 'line');
    for iL = numel(Ls):-1:1
        L = Ls(iL);
        if strcmp(get(get(L, 'Parent'), 'Tag'), 'legend')
            continue;
        end
        DN = get(L, 'DisplayName');
        C{end+1, 1} = true;
        if isempty(DN)
            C{end, 2} = ['data ', num2str(NoNameCnt)];
            set(L, 'DisplayName', C{end, 2});
            C{end, 4} = '';
            NoNameCnt = NoNameCnt + 1;
        else
            C{end, 2} = get(L, 'DisplayName');
            C{end, 4} = C{end, 2};
        end
        C{end, 3} = L;
    end
    legend(Ax, 'show')
end

C(1, :) = [];
set(handles.UIT_Lines, 'Data', C(:, 1:2));
set(handles.UIT_Lines, 'ColumnName', {'', 'Display Name'});

handles.LineData = C;

% store the handles of the legends. Will be empty if none exists.
handles.LegendHandles = LegendHandles;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ExportLineDataModule wait for user response (see UIRESUME)
% uiwait(handles.Fig_ExportLineData);


% --- Outputs from this function are returned to the command line.
function varargout = ExportLineDataModule_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% restore the options used for Text File Export
if isfield(handles.Settings, 'RB_TabSeparated_Value')
    set(handles.RB_TabSeparated, 'Value', handles.Settings.RB_TabSeparated_Value);
    set(handles.RB_SpaceSeparated, 'Value', ~handles.Settings.RB_TabSeparated_Value);
end
if isfield(handles.Settings, 'CB_IncludeHeaderLine_Value')
    set(handles.CB_IncludeHeaderLine, 'Value', handles.Settings.CB_IncludeHeaderLine_Value);
end

% restore window position
if isfield(handles.Settings, 'WindowPosition')
%     set(hObject, 'Position', handles.Settings.WindowPosition);
    RestorePositionOrDefault(hObject, handles.Settings.WindowPosition, ...
        'center');
end
set(hObject, 'Visible', 'on');


% --- Executes on button press in Btn_Export.
function Btn_Export_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_Export (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% get the data from UIT_Lines
data = get(handles.UIT_Lines, 'Data');
% if none is selected, return
if ~any([data{:, 1}])
    return;
end

% get the last used save path
SavePath = handles.Settings.SavePath;

FilterIndex = handles.Settings.SaveFilterIndex;
SupportedTypes = {'*.txt', 'Text File (*.txt)'; '*.xls', 'Excel Spreadsheet 97-2003 (*.xls)'; '*.xlsx', 'Excel Spreadsheet (*.xlsx)'};
% to restore the file type, we have to "sort" the SupportedTypes in a way
% that the wanted file type is the first entry
Sorter = 1:size(SupportedTypes, 1);
Sorter(FilterIndex) = [];
Sorter = [FilterIndex, Sorter];
SupportedTypesSorted = SupportedTypes(Sorter, :);

% open save dialog, using SavePath as default path & name
[FileName, PathName, FilterIndex] = uiputfile(SupportedTypesSorted, 'Select file to save to ...', SavePath);
if isempty(FileName) || isempty(PathName) || all(FileName == 0) || all(PathName == 0)
    return;
end

set(ancestor(hObject, 'figure'), 'Pointer', 'watch');
set(handles.UIT_Lines, 'Enable', 'off');
set(handles.Btn_Export, 'Enable', 'off');
set(handles.Btn_ExportAndClose, 'Enable', 'off');
drawnow();

% get the file name without the extension
[~, FileName, Ext] = fileparts(FileName);
% update the SavePath variable and
SavePath = fullfile(PathName, FileName);
% save it in the settings
handles.Settings.SavePath = SavePath;
% save the selected filter index in the settings
handles.Settings.SaveFilterIndex = find(ismember(SupportedTypes, ['*', Ext]));

% read out the line data of the selected lines
C = handles.LineData;
Result = {};
cnt = 1;
for iL = 1:size(C, 1)
    if ~data{iL, 1}
        continue;
    end
    x = get(C{iL, 3}, 'XData');
    y = get(C{iL, 3}, 'YData');
    z = get(C{iL, 3}, 'ZData');
    Str = get(C{iL, 3}, 'DisplayName');
    pos = strfind(Str, char(10));
    if ~isempty(pos)
        Str = Str(1:pos-1);
    end
    Result{1, cnt} = [Str, ' x'];
    Result{1, cnt+1} = [Str, ' y'];
    if ~isempty(z)
        Result{1, cnt+2} = [Str, ' z'];
    end
    Result(2:numel(x)+1, cnt) = num2cell(x(:));
    Result(2:numel(y)+1, cnt+1) = num2cell(y(:));
    if ~isempty(z)
    Result(2:numel(z)+1, cnt+2) = num2cell(z(:));
    end
    cnt = cnt+2;
    if ~isempty(z)
        cnt = cnt + 1;
    end
end

switch SupportedTypesSorted{FilterIndex, 1}
    case '*.txt'
        % save the options used for Text File Export
        handles.Settings.RB_TabSeparated_Value = get(handles.RB_TabSeparated, 'Value');
        handles.Settings.CB_IncludeHeaderLine_Value = get(handles.CB_IncludeHeaderLine, 'Value');
        % write the data to the file given by SavePath
        % open file
        fid = fopen([SavePath, '.txt'], 'w');
        % write header line if wanted
        if get(handles.CB_IncludeHeaderLine, 'Value')
            str = sprintf('%s\t', Result{1, :});
            fprintf(fid, str(1:end-1));
            fprintf(fid, '\n');
        end
        for iR = 2:size(Result, 1)
            if get(handles.RB_TabSeparated, 'Value')
                str = sprintf('%d\t', Result{iR, :});
            elseif get(handles.RB_SpaceSeparated, 'Value')
                str = sprintf('%d ', Result{iR, :});
            end
            fprintf(fid, str(1:end-1));
            fprintf(fid, '\n');
        end
        fclose(fid);
    case '*.xls'
        [status, msg] = xlswrite([SavePath, '.xls'], Result);
        if ~status
            msgbox({'Writing the *.xls file failed.', '', msg}, 'modal')
        end
    case '*.xlsx'
        [status, msg] = xlswrite([SavePath, '.xlsx'], Result);
        if ~status
            msgbox({'Writing the *.xlsx file failed.', '', msg}, 'modal')
        end
end

guidata(hObject, handles);

set(ancestor(hObject, 'figure'), 'Pointer', 'arrow');
set(handles.UIT_Lines, 'Enable', 'on');
set(handles.Btn_Export, 'Enable', 'on');
set(handles.Btn_ExportAndClose, 'Enable', 'on');


% --- Executes when user attempts to close Fig_ExportLineData.
function Fig_ExportLineData_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to Fig_ExportLineData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% store settings in CrystalModule folder
SettingsExportLineDataModule = handles.Settings;
SettingsExportLineDataModule.WindowPosition = get(hObject, 'Position');

try
    save(handles.SettingsPath, 'SettingsExportLineDataModule');
catch e
    msgbox(sprintf('Could not save file:\n%s', e.message), 'Error Saving File', ...
        'error', 'modal');
end

% Hint: delete(hObject) closes the figure
delete(hObject);


% --- Executes when Fig_ExportLineData is resized.
function Fig_ExportLineData_ResizeFcn(hObject, eventdata, handles)
% hObject    handle to Fig_ExportLineData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Pos = get(hObject, 'Position');

if Pos(3) < 100 || Pos(4) < 240
    return;
end

PosUIP_Selection = get(handles.UIP_Selection, 'Position');
PosUIP_Selection(3) = Pos(3) - 12;
PosUIP_Selection(4) = Pos(4) - 126;
set(handles.UIP_Selection, 'Position', PosUIP_Selection);

PosUIT_Lines = get(handles.UIT_Lines, 'Position');
PosUIT_Lines(3) = PosUIP_Selection(3) - 14;
PosUIT_Lines(4) = PosUIP_Selection(4) - 97;
set(handles.UIT_Lines, 'Position', PosUIT_Lines);

BtnSelectAll_Pos = get(handles.Btn_SelectAll, 'Position');
BtnSelectAll_Pos(2) = PosUIP_Selection(4)-47;
set(handles.Btn_SelectAll, 'Position', BtnSelectAll_Pos);

BtnDeselectAll_Pos = get(handles.Btn_DeselectAll, 'Position');
BtnDeselectAll_Pos(2) = PosUIP_Selection(4)-47;
set(handles.Btn_DeselectAll, 'Position', BtnDeselectAll_Pos);

BtnSelectNumber_Pos = get(handles.Btn_SelectNumber, 'Position');
BtnSelectNumber_Pos(2) = PosUIP_Selection(4)-79;
set(handles.Btn_SelectNumber, 'Position', BtnSelectNumber_Pos);

BtnDeselectNumber_Pos = get(handles.Btn_DeselectNumber, 'Position');
BtnDeselectNumber_Pos(2) = PosUIP_Selection(4)-79;
set(handles.Btn_DeselectNumber, 'Position', BtnDeselectNumber_Pos);

RBGOptions_Pos = get(handles.RBG_Options, 'Position');
RBGOptions_Pos(3) = Pos(3)-12;
set(handles.RBG_Options, 'Position', RBGOptions_Pos);

set(handles.UIT_Lines, 'ColumnWidth', {30 PosUIT_Lines(3) - 32});


% --- Executes on button press in Btn_ExportAndClose.
function Btn_ExportAndClose_Callback(hObject, eventdata, handles)
% call the "normal" export button callback
Btn_Export_Callback(handles.Btn_Export, [], handles)

% close the window
close(handles.Fig_ExportLineData);


% --- Executes on button press in Btn_SelectAll.
function Btn_SelectAll_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_SelectAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% get the data from UIT_Lines
data = get(handles.UIT_Lines, 'Data');

% make the first column of data a logical true
data(:, 1) = {true};

% reassign the data to the table
set(handles.UIT_Lines, 'Data', data);


% --- Executes on button press in Btn_DeselectAll.
function Btn_DeselectAll_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_DeselectAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% get the data from UIT_Lines
data = get(handles.UIT_Lines, 'Data');

% make the first column of data a logical false
data(:, 1) = {false};

% reassign the data to the table
set(handles.UIT_Lines, 'Data', data);


% --- Executes on button press in Btn_SelectNumber.
function Btn_SelectNumber_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_SelectNumber (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% get the data from UIT_Lines
data = get(handles.UIT_Lines, 'Data');

% create inputdialog
prompt = {'Select from:', 'Select to:'};
dlg_title = 'Input';
num_lines = 1;
defaultans = {'1', '1'};
answer = inputdlg(prompt, dlg_title, num_lines, defaultans);

% user clicked cancel
if isempty(answer); return; end

% convert answers to numbers and check if they are valid
Values = str2double(answer);
if any(isnan(Values)) || any(Values <= 0)
    msgbox('Please enter numerical values larger than zero only.', 'modal');
    return
end

% Assign to new variable
StartValue = Values(1);
% make sure that end value is not larger than the number of elements
EndValue = min([Values(2) size(data, 1)]);

% make the first column of data a logical true
data(StartValue:EndValue, 1) = {true};

% reassign the data to the table
set(handles.UIT_Lines, 'Data', data);


% --- Executes on button press in Btn_DeselectNumber.
function Btn_DeselectNumber_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_DeselectNumber (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% get the data from UIT_Lines
data = get(handles.UIT_Lines, 'Data');

% create inputdialog
prompt = {'Deselect from:', 'Deselect to:'};
dlg_title = 'Input';
num_lines = 1;
defaultans = {'1', '1'};
answer = inputdlg(prompt, dlg_title, num_lines, defaultans);

% user clicked cancel
if isempty(answer); return; end

% convert answers to numbers and check if they are valid
Values = str2double(answer);
if any(isnan(Values)) || any(Values <= 0)
    msgbox('Please enter numerical values larger than zero only.', 'modal');
    return
end

% Assign to new variable
StartValue = Values(1);
% make sure that end value is not larger than the number of elements
EndValue = min([Values(2) size(data, 1)]);

% make the first column of data a logical false
data(StartValue:EndValue, 1) = {false};

% reassign the data to the table
set(handles.UIT_Lines, 'Data', data);


% --- Executes on button press in CB_IncludeHeaderLine.
function CB_IncludeHeaderLine_Callback(hObject, eventdata, handles)
% hObject    handle to CB_IncludeHeaderLine (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of CB_IncludeHeaderLine
