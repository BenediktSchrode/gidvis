function ExportData(MainAxesHandle, Regridded)
% Export the data currently shown in the main axes, i.e. corrections
% applied if selected.
% 
% Usage:
%     ExportData(MainAxesHandle, Regridded)
%
% Input:
%     MainAxesHandle ... handle of the main axes
%     Regridded ... if true, the regridded data is exported, otherwise the
%                   not regridded data
%
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if Regridded
    % find the main image
    Img = findobj(MainAxesHandle, 'Tag', 'MainImage');
    % return if empty
    if isempty(Img); return; end

    X = get(Img, 'XData');
    Y = get(Img, 'YData');
    Z = get(Img, 'CData');

    if numel(X) == 2
        X = linspace(X(1), X(2), size(Z, 2));
    end
    if numel(Y) == 2
        Y = linspace(Y(1), Y(2), size(Z, 1));
    end
    [X, Y] = meshgrid(X, Y);
else
    GD = guidata(MainAxesHandle);
    Z = GD.CorrectedData;
    beamline = LoadFileModule('GetBeamline', GD.LoadFileModule);
    Z = Z(1:beamline.detlenz, 1:beamline.detlenx);
    
    ToolboxData = guidata(GD.ToolboxModule); % from the ToolboxModule we
    omega = ToolboxModule('GetOmega', GD.ToolboxModule); % get omega
    chi = str2double(get(ToolboxData.TB_chi, 'String')); % and chi
    contents = get(ToolboxData.PM_Space, 'String');
    Space = contents{get(ToolboxData.PM_Space, 'Value')};
    
    if strcmpi(Space, 'q')
        [X, Y] = CalculateQ(beamline, omega, chi);
    elseif strcmpi(Space, 'polar')
        [X, Y] = CalculatePolar(beamline, omega, chi);
    else
        X = 1:size(Z, 2);
        Y = 1:size(Z, 1);
        [X, Y] = meshgrid(X, Y);
    end
end

M = [X(:) Y(:) Z(:)]';

% the settings for this module are stored in the same directory as this
% file. Get the path.
SettingsPath = fullfile(fileparts(mfilename('fullpath')), 'Settings.mat');

% read out settings if available
if exist(SettingsPath, 'file') == 2
    S = load(SettingsPath);
    DefaultPath = S.SettingsExportMapModule.DefaultPath;
else
    DefaultPath = pwd;
end

SupportedTypes = {'*.txt', 'Text File (*.txt)'};

% open save dialog
[FileName, PathName] = uiputfile(SupportedTypes, 'Select file to save to ...', DefaultPath);
if isempty(FileName) || isempty(PathName) || all(FileName == 0) || all(PathName == 0)
    return;
end

SavePath = fullfile(PathName, FileName);

% write the data to the file given by SavePath
fid = fopen(SavePath, 'w');
fprintf(fid, '%s\t%s\t%s\n', ...
    get(get(MainAxesHandle, 'XLabel'), 'String'), ...
    get(get(MainAxesHandle, 'YLabel'), 'String'), ...
    'Intensity');
fprintf(fid, '%.6f\t%.6f\t%.6f\n', M);
fclose(fid);

[filepath, name] = fileparts(SavePath);
SettingsExportMapModule.DefaultPath = fullfile(filepath, name); %#ok. Used for saving.
save(SettingsPath, 'SettingsExportMapModule');