function GenerateCode(MainAxesHandle)
% Generates example code to load a data file and perform basic plots.
%
% Usage:
%     GenerateCode(MainAxesHandle)
%
% Input:
%     MainAxesHandle ... handle of the main GIDVis axes
%
% 
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

GD = guidata(MainAxesHandle);
LFM = guidata(GD.LoadFileModule);
TBX = guidata(GD.ToolboxModule);
    
% get filepath
SelectedFile = LoadFileModule('GetSelectedFilePath', GD.LoadFileModule);
SelectedFile = SelectedFile(1);

% get file extension
FileExt = SelectedFile.Ext;

% get omega, chi
omega = ToolboxModule('GetOmega', GD.ToolboxModule);
chi = str2double(get(TBX.TB_chi, 'String'));

% get regrid resolution
RegridResolution = [str2double(get(TBX.TB_RegridResolution1, 'String')), ...
    str2double(get(TBX.TB_RegridResolution2, 'String'))];

% get intensity scaling
contents = get(TBX.PM_Scaling, 'String');
Scaling = contents{get(TBX.PM_Scaling, 'Value')};

% the cell Str contains the line to write
Str{1} = '%% Auto-generated code from GIDVis';
Str{end+1} = '% Load data file and perform basic plotting.';
Str{end+1} = '%';
Str{end+1} = '% To run this code, the GIDVis code has to be on the MATLAB path.';
Str{end+1} = '%';
Str{end+1} = sprintf('%% File created on %s', datestr(now));
Str{end+1} = '';
Str{end+1} = '% create the LFMElement containing the information on the file to plot';

ElCode = SelectedFile.CreateCode;
Str(end+1:end+numel(ElCode)) = ElCode;

Str{end+1} = '';

Str{end+1} = '% define sample angles';
Str{end+1} = sprintf('omega = %g;', omega);
Str{end+1} = sprintf('chi = %g;', chi);
Str{end+1} = '';
Str{end+1} = '% resolution for the regridding';
Str{end+1} = sprintf('RegridResolution = [%.0f %.0f];', RegridResolution);
Str{end+1} = '';

if strcmpi(FileExt, '.giddat')
    Str{end+1} = '% import the data';
    Str{end+1} = 'Q = qData(El.Path);';
    Str{end+1} = '';
    
    Str{end+1} = '%% q Space (Regridded)';
    Str{end+1} = '% calculate the regridded data points in q space';
    Str{end+1} = '[q_xyR, q_zR, IntR] = RegriddedValues(Q, omega, chi, RegridResolution);';
    if strcmpi(Scaling, 'log')
        Str{end+1} = '';
        Str{end+1} = '% take the logarithm of the intensity';
        Str{end+1} = 'IntR = log(IntR);';
    elseif strcmpi(Scaling, 'sqrt')
        Str{end+1} = '';
        Str{end+1} = '% take the square root of the intensity';
        Str{end+1} = 'IntR = sqrt(IntR);';
    end
    Str{end+1} = '';
    Str{end+1} = '% basic plotting';
    Str{end+1} = 'figure';
    Str{end+1} = 'imagesc(q_xyR, q_zR, IntR);';
    Str{end+1} = 'ColorLims = getColorBarLimits(IntR);';
    Str{end+1} = 'caxis(ColorLims);';
    Str{end+1} = 'set(gca, ''YDir'', ''normal'');';
    Str{end+1} = '';
    
    Str{end+1} = '%% Polar Space (Regridded)';
    Str{end+1} = '% calculate the regridded data points in polar space';
    Str{end+1} = '[rhoR, PsiR, IntPR] = RegriddedValuesPolar(Q, omega, chi, RegridResolution);';
    if strcmpi(Scaling, 'log')
        Str{end+1} = '';
        Str{end+1} = '% take the logarithm of the intensity';
        Str{end+1} = 'IntPR = log(IntPR);';
    elseif strcmpi(Scaling, 'sqrt')
        Str{end+1} = '';
        Str{end+1} = '% take the square root of the intensity';
        Str{end+1} = 'IntPR = sqrt(IntPR);';
    end
    Str{end+1} = '';
    Str{end+1} = '% basic plotting';
    Str{end+1} = 'figure';
    Str{end+1} = 'imagesc(rhoR, PsiR, IntPR);';
    Str{end+1} = 'ColorLims = getColorBarLimits(IntPR);';
    Str{end+1} = 'caxis(ColorLims);';
    Str{end+1} = 'set(gca, ''YDir'', ''normal'');';
else
    % get beamtime path, name and beamline index
    BeamtimePath = GD.BeamtimePath;
    Beamtimes = get(LFM.BeamtimeSelection, 'String');
    SelectedBeamtime = Beamtimes{get(LFM.BeamtimeSelection, 'Value')};
    BeamlineIndex = get(LFM.BeamlineSelection, 'Value');

    Str{end+1} = '% load the beamtime';
    Str{end+1} = sprintf('S = load(''%s'');', fullfile(BeamtimePath, SelectedBeamtime));
    Str{end+1} = '% read out the experimental setup';
    Str{end+1} = sprintf('BL = S.beamtime.SetUps(%.0f);', BeamlineIndex);
    Str{end+1} = '';
    Str{end+1} = '% read out the intensity data and the header information';
    Str{end+1} = '[PixelIntensity, Header] = ReadDataFromFile(El);';
    Str{end+1} = '% crop the intensity to the size of the exerimental setup';
    Str{end+1} = 'PixelIntensity = PixelIntensity(1:BL.detlenz, 1:BL.detlenx);';
    Str{end+1} = '% replace intensity values < 0, e.g. detector gaps, by NaN';
    Str{end+1} = 'PixelIntensity(PixelIntensity < 0) = NaN;';
    
    Str{end+1} = '';
    Str{end+1} = '% display the header information in the command line';
    Str{end+1} = 'disp(Header{1})';
    Str{end+1} = '';
    
    Str{end+1} = '%% Apply intensity corrections (comment/uncomment as you want)';
    Str{end+1} = '% calculate the corrections';
    Str{end+1} = '[LC, PC, SAC, SDDC, DEC] = CorrectionFactors(BL, omega, chi);';
    Str{end+1} = '% apply corrections';
    Str{end+1} = 'PixelIntensity = PixelIntensity./LC;';
    Str{end+1} = 'PixelIntensity = PixelIntensity./PC;';
    Str{end+1} = 'PixelIntensity = PixelIntensity.*SAC;';
    Str{end+1} = 'PixelIntensity = PixelIntensity.*SDDC;';
    Str{end+1} = 'PixelIntensity = PixelIntensity.*DEC;';
    
    if strcmpi(Scaling, 'log')
        Str{end+1} = '';
        Str{end+1} = '%% apply the intensity scaling to the intensity data';
        Str{end+1} = 'PixelIntensity = log(PixelIntensity);';
    elseif strcmpi(Scaling, 'sqrt')
        Str{end+1} = '';
        Str{end+1} = '%% apply the intensity scaling to the intensity data';
        Str{end+1} = 'PixelIntensity = sqrt(PixelIntensity);';
    end

    Str{end+1} = '';
    Str{end+1} = '%% Pixel Space';
    Str{end+1} = '% create x and z vector';
    Str{end+1} = 'x = 1:size(PixelIntensity, 2);';
    Str{end+1} = 'z = 1:size(PixelIntensity, 1);';
    Str{end+1} = '';
    Str{end+1} = '% basic plotting';
    Str{end+1} = 'figure';
    Str{end+1} = 'imagesc(x, z, PixelIntensity)';
    Str{end+1} = 'ColorLims = getColorBarLimits(PixelIntensity);';
    Str{end+1} = 'caxis(ColorLims);';
    Str{end+1} = 'title(''Image in pixel space'')';
    Str{end+1} = 'xlabel(''Pixel'')';
    Str{end+1} = 'ylabel(''Pixel'')';
    
    Str{end+1} = '';
    Str{end+1} = '%% q Space (Regridded)';
    Str{end+1} = '% get the regridded data points in reciprocal space';
    Str{end+1} = '[qxy, qz, IntQ] = CalculateQRegridded(BL, omega, chi, PixelIntensity, RegridResolution);';
    Str{end+1} = '';
    Str{end+1} = '% basic plotting';
    Str{end+1} = 'figure';
    Str{end+1} = 'imagesc(qxy, qz, IntQ);';
    Str{end+1} = 'ColorLims = getColorBarLimits(IntQ);';
    Str{end+1} = 'caxis(ColorLims);';
    Str{end+1} = 'set(gca, ''YDir'', ''normal'');';
    Str{end+1} = 'title(''Image converted to reciprocal space'')';
    Str{end+1} = 'xlabel(''q_{xy} (1/A)'')';
    Str{end+1} = 'ylabel(''q_z (1/A)'')';

    Str{end+1} = '';
    Str{end+1} = '%% Polar Space (Regridded)';
    Str{end+1} = '% get the regridded data points in polar space';
    Str{end+1} = '[R, phi, IntP] = CalculatePolarRegridded(BL, omega, chi, PixelIntensity, RegridResolution);';
    Str{end+1} = '';
    Str{end+1} = '% basic plotting';
    Str{end+1} = 'figure';
    Str{end+1} = 'imagesc(R, phi, IntP);';
    Str{end+1} = 'ColorLims = getColorBarLimits(IntP);';
    Str{end+1} = 'caxis(ColorLims);';
    Str{end+1} = 'set(gca, ''YDir'', ''normal'');';
    Str{end+1} = 'title(''Image converted to polar space'')';
    Str{end+1} = 'xlabel(''q (1/A)'')';
    Str{end+1} = 'ylabel(''\Psi (degree)'')';
end

% the settings for this module are stored in the same directory as this
% file. Get the path.
SettingsPath = fullfile(fileparts(mfilename('fullpath')), 'Settings.mat');

% read out settings if available
if exist(SettingsPath, 'file') == 2
    S = load(SettingsPath);
    DefaultPath = S.SettingsExportMapModule.DefaultPath;
else
    DefaultPath = pwd;
end

SupportedTypes = {'*.m', 'MATLAB Code File (*.m)'};

% open save dialog
[FileName, PathName] = uiputfile(SupportedTypes, 'Select file to save to ...', DefaultPath);
if isempty(FileName) || isempty(PathName) || all(FileName == 0) || all(PathName == 0)
    return;
end

SavePath = fullfile(PathName, FileName);

% write the string to the file given by SavePath
fid = fopen(SavePath, 'w');
for iS = 1:numel(Str)
    fprintf(fid, '%s\n', Str{iS});
end
fclose(fid);

[filepath, name] = fileparts(SavePath);
SettingsExportMapModule.DefaultPath = fullfile(filepath, name);
save(SettingsPath, 'SettingsExportMapModule');