function varargout = FootprintModule(varargin)
% This file, together with FootprintModule.fig, is the FootprintModule of
% GIDVis.
%
% Usage:
%     varargout = FootprintModule(MainAxesHandle)
%
% Input:
%     MainAxesHandle ... handle of the main axes of GIDVis
%
% Output:
%     varargout ... default GUIDE output
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Last Modified by GUIDE v2.5 11-Dec-2017 11:59:19

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @FootprintModule_OpeningFcn, ...
                   'gui_OutputFcn',  @FootprintModule_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before FootprintModule is made visible.
function FootprintModule_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to FootprintModule (see VARARGIN)

% Choose default command line output for FootprintModule
handles.output = hObject;

hold(handles.MainAxes, 'on');
handles.Sample = fill(NaN, NaN, 'c', 'EdgeColor', 'none', ...
    'FaceAlpha', 1, 'Parent', handles.MainAxes);
handles.Beam = fill(NaN, NaN, 'g', 'EdgeColor', 'none', ...
    'FaceAlpha', 0.7, 'Parent', handles.MainAxes);
handles.BeamknifeShadow = fill(NaN, NaN, [211 211 211]./255, ...
    'EdgeColor', 'none', 'FaceAlpha', 0.7, 'Parent', handles.MainAxes);
handles.FinalArea = fill(NaN, NaN, 'r', 'EdgeColor', 'none', ...
    'FaceAlpha', 0.7, 'Parent', handles.MainAxes, 'Marker', '*', ...
    'MarkerEdgeColor', 'k');
handles.Beamknife = line('XData', NaN, 'YData',  NaN, 'Color', 'k', ...
    'Parent', handles.MainAxes);
legend(handles.MainAxes, 'Sample', 'Beam', ...
    'Beamknife Shadow on Sample', ...
    'Final Area (Active Area \cap Sample \cap Not Beamknife Shadow)', 'Beamknife', ...
    'Location', 'NorthWest');

axis(handles.MainAxes, 'equal')

% find figure toolbar
FigTB = findall(hObject, 'tag', 'FigureToolBar');
% remove some of the toolbar buttons (including separators if necessary)
set(findall(FigTB, 'Tag', 'Standard.EditPlot'), 'Visible', 'off', 'Separator', 'off');
set(findall(FigTB, 'Tag', 'Standard.NewFigure'), 'Visible', 'off');
set(findall(FigTB, 'Tag', 'Standard.FileOpen'), 'Visible', 'off');
set(findall(FigTB, 'Tag', 'Exploration.Rotate'), 'Visible', 'off');
set(findall(FigTB, 'Tag', 'Exploration.Brushing'), 'Visible', 'off');
set(findall(FigTB, 'Tag', 'DataManager.Linking'), 'Visible', 'off', 'Separator', 'off');
set(findall(FigTB, 'Tag', 'Standard.PrintFigure'), 'Visible', 'off');
set(findall(FigTB, 'Tag', 'Standard.SaveFigure'), 'Visible', 'off');
set(findall(FigTB, 'Tag', 'Plottools.PlottoolsOn'), 'Visible', 'off');
set(findall(FigTB, 'Tag', 'Plottools.PlottoolsOff'), 'Visible', 'off', 'Separator', 'off');
set(findall(FigTB, 'Tag', 'Annotation.InsertColorbar'), 'Visible', 'off', 'Separator', 'off');

% remove more separators
set(findall(FigTB, 'Tag', 'Exploration.ZoomIn'), 'Separator', 'off');

% add a listener to the axes in varargin{1}. When the axes gets destroyed
% (e.g. because the figure containing the axes gets closed), the callback
% will be executed (and closes this window). Store the listener in the
% handles structure to remove it when this window gets closed by user,
% otherwise the callback is executed even when the FootprintModule window
% does not exist anymore.
handles.MainAxesDestroyedListener = addlistener(varargin{1}, 'ObjectBeingDestroyed', @(src, evt) MainAxesDestroyed_Callback(src, evt, handles.Fig_FootprintModule));

% the settings for this module are stored in the same directory as the file
% FootprintModule.m. Store its path in the handles structure to reuse it.
handles.SettingsPath = fullfile(fileparts(mfilename('fullpath')), 'Settings.mat');

% read out settings if available
if exist(handles.SettingsPath, 'file') == 2
    S = load(handles.SettingsPath);
    % Update the control elements according to the values from the settings
    % file
    S = S.SettingsFootprintModule;
    set(handles.TB_SIh, 'String', S.SIh);
    set(handles.TB_SIw, 'String', S.SIw);
    set(handles.TB_SampleLength, 'String', S.SampleLength);
    set(handles.TB_SampleWidth, 'String', S.SampleWidth);
    set(handles.TB_SampleRot, 'String', S.SampleRot);
    set(handles.TB_alphai, 'String', S.alphai);
    set(handles.TB_BeamknifeDist, 'String', S.BeamknifeDist);
    set(handles.CB_SampleVis, 'Value', S.SampleVis);
    set(handles.CB_BeamVis, 'Value', S.BeamVis);
    set(handles.CB_ShadowVis, 'Value', S.ShadowVis);
    set(handles.CB_FinalAreaVis, 'Value', S.FinalAreaVis);
    % store the last window position in the handles structure
    handles.LastWindowPosition = S.WindowPosition;
end

% Update handles structure
guidata(hObject, handles);

UpdatePlot(handles);

% UIWAIT makes FootprintModule wait for user response (see UIRESUME)
% uiwait(handles.Fig_FootprintModule);

function MainAxesDestroyed_Callback(src, evt, HandleToModuleWindow)
% close module window
close(HandleToModuleWindow);

% --- Outputs from this function are returned to the command line.
function varargout = FootprintModule_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% restore the last window position, if available
if isfield(handles, 'LastWindowPosition')
    RestorePositionOrDefault(hObject, handles.LastWindowPosition);
end
set(hObject, 'Visible', 'on');


function GUI_ElementCallback(hObject, eventdata, handles)
UpdatePlot(handles);

% --- Executes during object creation, after setting all properties.
function TB_SOh_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_SOh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function TB_SOw_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_SOw (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function TB_SIh_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_SIh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function TB_SIw_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_SIw (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function TB_SampleLength_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_SampleLength (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function TB_SampleWidth_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_SampleWidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function TB_SampleRot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_SampleRot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function TB_qxy_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_qxy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function TB_qz_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_qz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function TB_lambda_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_lambda (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function UpdatePlot(handles)
SI_h = str2double(get(handles.TB_SIh, 'String'));
SI_w = str2double(get(handles.TB_SIw, 'String'));
SampleLength = str2double(get(handles.TB_SampleLength, 'String'));
SampleWidth = str2double(get(handles.TB_SampleWidth, 'String'));
SampleRot = str2double(get(handles.TB_SampleRot, 'String'));
alpha_i = str2double(get(handles.TB_alphai, 'String'))*pi/180;
BeamknifeDistance = str2double(get(handles.TB_BeamknifeDist, 'String'));

RotationMatrix = @(alpha) [cosd(alpha) -sind(alpha); sind(alpha) cosd(alpha)];

% sample coordinates
xy_Sample = [-SampleLength/2, SampleLength/2, SampleLength/2, -SampleLength/2; ...
    -SampleWidth/2, -SampleWidth/2, SampleWidth/2, SampleWidth/2]'*RotationMatrix(SampleRot);

% Size of the footprint of the incoming beam (Moser, eq. 2.36)
BeamFP_L = SI_h/sin(alpha_i); % length
BeamFP_W = SI_w; % width

% vertices for rectangle of beam footprint
xy_Beam = [-BeamFP_L/2, BeamFP_L/2, BeamFP_L/2, -BeamFP_L/2; -BeamFP_W/2, -BeamFP_W/2, BeamFP_W/2, BeamFP_W/2]';

% line points for beamknife
xy_Beamknife = [0 0; -1.5*SampleWidth 1.5*SampleLength]';

% vertices for rectangle of beamknife shadow
x = 2*BeamknifeDistance/(sin(alpha_i));
if x > SampleLength
    x_BeamknifeShadow = NaN;
    y_BeamknifeShadow = NaN;
else
    x_BeamknifeShadow = [-SampleLength/2 -x/2 -x/2 -SampleLength/2; ...
        SampleLength/2 x/2 x/2 SampleLength/2]';
    y_BeamknifeShadow = [-SampleWidth/2 -SampleWidth/2 SampleWidth/2 SampleWidth/2; ...
        -SampleWidth/2 -SampleWidth/2 SampleWidth/2 SampleWidth/2]';
end
    
% Calculate overlapping polygons
FinalArea = ConvexPolyOverlap(xy_Beam, xy_Sample, false);
if x <= SampleLength
    FinalArea = ConvexPolyOverlap(FinalArea, [-x/2 x/2 x/2 -x/2; -SampleWidth/2 -SampleWidth/2 SampleWidth/2 SampleWidth/2]', false);
end

% set the data of the elements accordingly
set(handles.Sample, 'XData', xy_Sample(:, 1), 'YData', xy_Sample(:, 2));
set(handles.Beam, 'XData', xy_Beam(:, 1), 'YData', xy_Beam(:, 2));
set(handles.Beamknife, 'XData', xy_Beamknife(:, 1), 'YData', xy_Beamknife(:, 2));
set(handles.BeamknifeShadow, 'XData', x_BeamknifeShadow, 'YData', y_BeamknifeShadow)
set(handles.FinalArea, 'XData', FinalArea(:, 1), 'YData', FinalArea(:, 2));

% update the result label
Str = sprintf('Footprint Size: %g x %g\nUnshadowed Area Size: %g x %g\nFinal Area Size: %g', ...
    BeamFP_L, BeamFP_W, x, SampleWidth, polyarea(FinalArea(:, 1), FinalArea(:, 2)));
set(handles.Text_Result, 'String', Str);


function TB_alphai_Callback(hObject, eventdata, handles)
% hObject    handle to TB_alphai (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_alphai as text
%        str2double(get(hObject,'String')) returns contents of TB_alphai as a double


% --- Executes during object creation, after setting all properties.
function TB_alphai_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_alphai (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in CB_SampleVis.
function CB_SampleVis_Callback(hObject, eventdata, handles)
% hObject    handle to CB_SampleVis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of CB_SampleVis
if get(hObject, 'Value')
    set(handles.Sample, 'Visible', 'on');
else
    set(handles.Sample, 'Visible', 'off');
end


% --- Executes on button press in CB_BeamVis.
function CB_BeamVis_Callback(hObject, eventdata, handles)
% hObject    handle to CB_BeamVis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of CB_BeamVis
if get(hObject, 'Value')
    set(handles.Beam, 'Visible', 'on');
else
    set(handles.Beam, 'Visible', 'off');
end


% --- Executes on button press in CB_DetectorVis.
function CB_DetectorVis_Callback(hObject, eventdata, handles)
% hObject    handle to CB_DetectorVis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of CB_DetectorVis
if get(hObject, 'Value')
    set(handles.Detector, 'Visible', 'on');
else
    set(handles.Detector, 'Visible', 'off');
end


% --- Executes on button press in CB_ActiveArea.
function CB_ActiveArea_Callback(hObject, eventdata, handles)
% hObject    handle to CB_ActiveArea (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of CB_ActiveArea
if get(hObject, 'Value')
    set(handles.ActiveArea, 'Visible', 'on');
else
    set(handles.ActiveArea, 'Visible', 'off');
end


% --- Executes on button press in CB_FinalAreaVis.
function CB_FinalAreaVis_Callback(hObject, eventdata, handles)
% hObject    handle to CB_FinalAreaVis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of CB_FinalAreaVis
if get(hObject, 'Value')
    set(handles.FinalArea, 'Visible', 'on');
else
    set(handles.FinalArea, 'Visible', 'off');
end



function TB_BeamknifeDist_Callback(hObject, eventdata, handles)
% hObject    handle to TB_BeamknifeDist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_BeamknifeDist as text
%        str2double(get(hObject,'String')) returns contents of TB_BeamknifeDist as a double
UpdatePlot(handles);

% --- Executes during object creation, after setting all properties.
function TB_BeamknifeDist_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_BeamknifeDist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in CB_ShadowVis.
function CB_ShadowVis_Callback(hObject, eventdata, handles)
% hObject    handle to CB_ShadowVis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of CB_ShadowVis
if get(hObject, 'Value')
    set(handles.BeamknifeShadow, 'Visible', 'on')
else
    set(handles.BeamknifeShadow, 'Visible', 'off')
end


% --- Executes when Fig_FootprintModule is resized.
function Fig_FootprintModule_SizeChangedFcn(hObject, eventdata, handles)
% hObject    handle to Fig_FootprintModule (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% get the position property of the window
Pos = get(hObject, 'Position');
% the top three elements on the left only change in their y coordinate when 
% resizing
WPos = get(handles.Text_Explanation, 'Position');
WPos(2) = Pos(4) - 87;
set(handles.Text_Explanation, 'Position', WPos);
WPos = get(handles.UIP_Parameters, 'Position');
WPos(2) = Pos(4) - 352;
set(handles.UIP_Parameters, 'Position', WPos);
WPos = get(handles.UIP_Visibilities, 'Position');
WPos(2) = Pos(4) - 457;
set(handles.UIP_Visibilities, 'Position', WPos);
% the result label does not change y, but changes in height
if Pos(4) >= 600
    WPos = get(handles.Text_Result, 'Position');
    WPos(4) = Pos(4) - 486;
    set(handles.Text_Result, 'Position', WPos);
end
% the axes system changes in its height and width
WPos = get(handles.MainAxes, 'Position');
WPos(3) = Pos(3) - 407;
WPos(4) = Pos(4) - 69;
set(handles.MainAxes, 'Position', WPos);


% --- Executes when user attempts to close Fig_FootprintModule.
function Fig_FootprintModule_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to Fig_FootprintModule (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% delete the listener to the AxesDestroyed event. Otherwise it will still
% listen and try to execute the MainAxesDestroyed_Callback function, even
% though the Module window is already closed.
delete(handles.MainAxesDestroyedListener);

% create the settings structure
SettingsFootprintModule.WindowPosition = get(hObject, 'Position');
SettingsFootprintModule.SIh = get(handles.TB_SIh, 'String');
SettingsFootprintModule.SIw = get(handles.TB_SIw, 'String');
SettingsFootprintModule.SampleLength = get(handles.TB_SampleLength, 'String');
SettingsFootprintModule.SampleWidth = get(handles.TB_SampleWidth, 'String');
SettingsFootprintModule.SampleRot = get(handles.TB_SampleRot, 'String');
SettingsFootprintModule.alphai = get(handles.TB_alphai, 'String');
SettingsFootprintModule.BeamknifeDist = get(handles.TB_BeamknifeDist, 'String');
SettingsFootprintModule.SampleVis = get(handles.CB_SampleVis, 'Value');
SettingsFootprintModule.BeamVis = get(handles.CB_BeamVis, 'Value');
SettingsFootprintModule.ShadowVis = get(handles.CB_ShadowVis, 'Value');
SettingsFootprintModule.FinalAreaVis = get(handles.CB_FinalAreaVis, 'Value'); %#ok. Used for saving.
% in order to avoid error when no write access for file use try.
try; save(handles.SettingsPath, 'SettingsFootprintModule'); end

% Hint: delete(hObject) closes the figure
delete(hObject);
