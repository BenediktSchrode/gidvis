function GridModule(FigTB, AxesHandle, SeparatorOnOff)
% This function adds two toggle buttons to the figure toolbar FigTB, to 
% toggle the major and minor grid in the axes AxesHandle on and off.
%
% Usage:
%     GridModule(FigTB, AxesHandle, SeparatorOnOff)
%
% Input:
%     FigTB ... handle of the toolbar where the buttons should be added to
%     AxesHandle ... handle of the axes where the grids should be added to
%     SeparatorOnOff ... string ('on' or 'off') defining whether a
%                        separator should be drawn left of the toggle tool
%                        or not
%
% 
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Create an image for the toggle button for the major grid
MajorGridImg = ones(16, 16, 3).*240./255; % "background" for the image, light gray
MajorGridImg(3:14, [4, 13], :) = 0; % add vertical lines to image
MajorGridImg([4, 13], 3:14, :) = 0; % add horizontal lines to image
% add the toggle button to the toolbar
uitoggletool(FigTB, ...
    'OnCallback', @(src, evt) ToggleMainAxesGrid(src, evt, AxesHandle), ...
    'OffCallback', @(src, evt) ToggleMainAxesGrid(src, evt, AxesHandle), ...
    'CData', MajorGridImg, ...
    'Tag', 'ToggleMainAxesMajorGrid', ...
    'TooltipString', 'Toggle Major Grid On/Off', ...
    'Separator', SeparatorOnOff);

% Create an image for the toggle button for the minor grid. Start from
% image for major grid.
MinorGridImg = MajorGridImg;
MinorGridImg(3:14, [7, 10], :) = 0; % add additional vertical lines
MinorGridImg([7, 10], 3:14, :) = 0; % add additional horizontal lines
% add the toggle button to the toolbar
uitoggletool(FigTB, ...
    'OnCallback', @(src, evt) ToggleMainAxesMinorGrid(src, evt, AxesHandle), ...
    'OffCallback', @(src, evt) ToggleMainAxesMinorGrid(src, evt, AxesHandle), ...
    'CData', MinorGridImg, ...
    'Tag', 'ToggleMainAxesMinorGrid', ...
    'TooltipString', 'Toggle Minor Grid On/Off');

function ToggleMainAxesGrid(src, evt, AxesHandle)
imgHandle = findobj(AxesHandle, 'Tag', 'MainImage');
if isempty(imgHandle) % occurs e.g. when no GID image is loaded yet
    set(src, 'State', 'off');
    msgbox('You cannot add a grid to the currently displayed image. Select a GID image in the "Load File" window (File - Select Folder) first.', 'Cannot Add Grid', 'modal');
    return;
end
grid(AxesHandle);
if ~verLessThan('matlab', '8.4')
    AxesHandle.GridAlpha = 0.7;
end

function ToggleMainAxesMinorGrid(src, evt, AxesHandle)
imgHandle = findobj(AxesHandle, 'Tag', 'MainImage');
if isempty(imgHandle) % occurs e.g. when no GID image is loaded yet
    set(src, 'State', 'off');
    msgbox('You cannot add a grid to the currently displayed image. Select a GID image in the "Load File" window (File - Select Folder) first.', 'Cannot Add Grid', 'modal');
    return;
end
grid(AxesHandle, 'minor');
if ~verLessThan('matlab', '8.4')
    AxesHandle.MinorGridAlpha = 1;
end