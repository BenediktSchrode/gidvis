function [xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(xdata, ydata, zdata, ImrectPos)
% Calculates the integrated line scan profiles for the data given in xdata,
% ydata, zdata in the rectangle given by ImrectPos.
%
% Performs a fit on this data.
%
% Requires OneDimGaussianFit.m for the fitting process.
%
% Usage:
%     [xc, yc, zc1, zc2, cfun1, cfun2, DeltaFit1, DeltaFit2, ResString1, ResString2] = GetIntegratedLineScanProfiles(xdata, ydata, zdata, ImrectPos)
%
% Input:
%     xdata ... x values of the data to get integrated line scan profiles
%               from
%     ydata ... y values of the data to get integrated line scan profiles
%               from
%     zdata ... z values of the data to get integrated line scan profiles
%               from
%     ImrectPos ... position of the imrect from where the integrated line
%                   scan profiles should be obtained from
%
% Output:
%     xc ... vector containing the x vector
%     yc ... vector containing the y vector
%     zc1 ... vector containing the extracted intensities for the summation
%             from top to bottom.
%     zc2 ... vector containing the extracted intensities for the summation
%             from left to right.
%     FitRes1 ... result of the fit performed on xc and zc1.
%     FitRes2 ... result of the fit performed on yc and zc2.
%     FitFun ... function handle of the function used to fit the data.
%     DeltaFit1 ... the 95% confidence deviation of FitRes1.
%     DeltaFit2 ... the 95% confidence deviation of FitRes2.
%
%
% This file is part of GIDVis.
%
% see also: OneDimGaussianFit, imrect, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% check size of input arguments
sz = size(zdata);
if numel(xdata) ~= sz(2) || numel(ydata) ~= sz(1)
    error('GIDVis:GetIntegratedLineScanProfiles', 'Dimension mismatch.');
end

% get the boundaries of the rectangle
xmin = ImrectPos(1);
ymin = ImrectPos(2);
xmax = xmin + ImrectPos(3);
ymax = ymin + ImrectPos(4);

% find the selected data
Lx = xdata >= xmin & xdata <= xmax;
Ly = ydata >= ymin & ydata <= ymax;

xc = xdata(Lx);
yc = ydata(Ly);    

% calculate the summed intensities
% during this process we're omitting the NaN values (which are e.g. due to
% detector gaps). Of course, columns/rows with lots of NaN values then have
% a lower sum, that's why we are averaging then with the number of not NaN
% values.
FilteredData = zdata(Ly, Lx);
if isempty(FilteredData)
    SumTopBottom = NaN;
    SumLeftRight = NaN;
    xc = NaN;
    yc = NaN;
else
    SumTopBottom = sum(FilteredData, 1, 'omitnan');
    SumLeftRight = sum(FilteredData, 2, 'omitnan');
    NotNaNVals = ~isnan(FilteredData);
    NumTB = sum(NotNaNVals, 1);
    NumLR = sum(NotNaNVals, 2);
    SumTopBottom = SumTopBottom./NumTB;
    SumLeftRight = SumLeftRight./NumLR;
end

zc1 = SumTopBottom;
zc2 = SumLeftRight;

[stat, ~] = license('checkout', 'Curve_Fitting_Toolbox');
if stat && nargout > 4 && ~isempty(xc)
    % perform 1 dim Gaussian fitting
    [cfun1, ~, ~, DeltaFit1] = OneDimGaussianFit(xc, SumTopBottom);
    [cfun2, ~, ~, DeltaFit2] = OneDimGaussianFit(yc, SumLeftRight');
else
    cfun1 = [];
    DeltaFit1 = [];
    cfun2 = [];
    DeltaFit2 = [];
end

ResString1 = '';
if ~isempty(cfun1)
    ResString1 = formula(cfun1);
    names = coeffnames(cfun1);
    vals = coeffvalues(cfun1);
    for iC = 1:numel(vals)
        ResString1 = sprintf('%s\n%s = %g +/- %g', ResString1, names{iC}, vals(iC), DeltaFit1(iC));
    end
end

ResString2 = '';
if ~isempty(cfun2)
    ResString2 = formula(cfun2);
    names = coeffnames(cfun2);
    vals = coeffvalues(cfun2);
    for iC = 1:numel(vals)
        ResString2 = sprintf('%s\n%s = %g +/- %g', ResString2, names{iC}, vals(iC), DeltaFit2(iC));
    end
end