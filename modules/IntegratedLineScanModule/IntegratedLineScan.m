function IntegratedLineScan(FigTB, AxesHandle, SeparatorOnOff)
% This function adds a toggle button to the figure toolbar given by FigTB.
% This button switches on/off the integrated line scan mode.
%
% Usage:
%     IntegratedLineScan(FigTB, AxesHandle, SeparatorOnOff)
%
% Input:
%     FigTB ... toolbar where the button for the integrated line scan
%               should be added to
%     AxesHandle ... handle to the axes where the integrated line scan
%                    should be obtained from
%     SeparatorOnOff ... string ('on' or 'off') defining whether a
%                        separator should be drawn left of the toggle tool
%                        or not
% 
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Load the image for the toggle button
Img = imread('IntegratedLineScanIcon.png');

% add the toggle button to the toolbar
ToggleButton = uitoggletool(FigTB, ...
    'OnCallback', @(src, evt) IntegratedLineScanOn(src, evt, AxesHandle), ...
    'Tag', 'LineProfileToggleButton', ...
    'Separator', SeparatorOnOff, ...
    'CData', Img, ...
    'TooltipString', 'Integrated Line Scan');

% add a listener to the ToggleButton's ObjectBeingDestroyed event. Here we
% close the result figure if one is still open.
addlistener(ToggleButton, 'ObjectBeingDestroyed', @ToggleButtonDestroyed_Callback);

function ToggleButtonDestroyed_Callback(src, evt)
if strcmpi(get(src, 'State'), 'on') % if the line profile mode is still on,
    set(src, 'State', 'off'); % turn it off by setting the toggle button state to 'off'
end

function XYCDataChanged_Callback(ImrectHandle, ResultFigHandle, BtnHandle)
NewPositionDoCalculation(ImrectHandle, ResultFigHandle, BtnHandle)

function IntegratedLineScanOn(src, evt, AxesHandle)
% executes when the toggle button gets turned on

% check if there is a license for the image toolbox
[stat, msg] = license('checkout', 'Image_Toolbox');
if ~stat
    set(src, 'State', 'off');
    msgbox(msg, 'License Checkout Failed', 'modal');
    return;
end

% find the image handle
imgHandle = findobj(AxesHandle, 'Tag', 'MainImage');
if isempty(imgHandle) % occurs e.g. when no GID image is loaded yet
    set(src, 'State', 'off');
    msgbox('You cannot create an integrated line scan of the currently displayed image. Select a GID image in the "Load File" window (File - Select Folder) first.', 'Cannot Create Integrated Line Scan', 'modal');
    return;
end

% switch off all other toggle buttons in the toolbar except for src to
% avoid having multiple tools selected at once.
SwitchOffToolstripButtons(ancestor(AxesHandle, 'figure'), src);

% create a results figure. In its CloseRequestFcn, we will switch the
% integrated line scan profile mode to 'off' and delete the imrect.
f = figure('NumberTitle', 'off', 'Name', 'Integrated Line Scan', 'Visible', 'off');
AddExportLineDataButton(f);

% set the OffCallback of the toggle button. This has to be done here,
% because it needs the result figure as an input argument, so cannot be
% done during the creation of the toggle button.
set(src, 'OffCallback', @(src, evt) IntegratedLineScanOff(src, evt, f, get(AxesHandle, 'Parent')));

% create an imrect on the axes
h = imCustomRect(AxesHandle);

% user might have aborted creation using 'Esc' --> h will be empty
if isempty(h)
    set(src, 'State', 'off');
    return;
end

% for numeric limits GUI:
h.addCustomMenuEntry('Set Position',@(varargin)ROINumericPosition(h,varargin{:}));
% for exporting the handle in the base workspace:
if ~isdeployed()
    h.addCustomMenuEntry('Save in workspace as ''lastROI''',@(varargin)assignin('base','lastROI',h));
end

% add a button to the figure which can be used to update the data
Btn = uicontrol(f, 'Style', 'pushbutton', 'String', 'Update Data', ...
    'FontSize', 10, 'Units', 'normalized', 'Position', [0.01 .93 .15 .06], ...
    'Callback', @(src, evt) NewPositionDoCalculation(h, f, src));

% adding a uicontrol to a figure removes toolbar, restore it
set(f, 'toolbar', 'figure');

% add listener to ObjectBeingDestroyed of the imrect, in order to set the
% button's state to off as soon as the imrect gets deleted.
addlistener(h, 'ObjectBeingDestroyed', @(obj, event) ImrectDeletion(obj, event, src, f));

% call the NewPositionDoCalculation function once, to plot the integrated line
% scan
NewPositionDoCalculation(h, f, Btn);
 
% find the MainImage in the AxesHandle
hMainImage = findobj(AxesHandle, 'Tag', 'MainImage');
if ~isempty(hMainImage) && isempty(get(src, 'UserData'))
    % create a listener to the XData, YData and CData of MainImage. The
    % callback function will be executed if any of these three properties
    % changes. In the callback, the result window will be updated.
    DataListener = addlistener(hMainImage, 'CData', 'PostSet', @(src, evt) XYCDataChanged_Callback(h, f, Btn));
    % store the listener in the userdata of the toggle button
    set(src, 'UserData', DataListener);
end
set(f, 'CloseRequestFcn', {@IntegratedLineScanFigure_CloseRequestFcn, src, h}, ...
    'Visible', 'on');

function ImrectDeletion(ImrectHandle, event, ToolbarButtonHandle, FigureHandle)
if isgraphics(ToolbarButtonHandle)
    set(ToolbarButtonHandle, 'State', 'off');
else
    delete(FigureHandle);
end

function IntegratedLineScanOff(src, evt, FigureHandle, GIDVisFigureHandle)
% if the result figure is still open, close it
if ishandle(FigureHandle);
    close(FigureHandle);
end
% delete the listener to the data change
if ~isempty(get(src, 'UserData'))
    delete(get(src, 'UserData'))
end
set(src, 'UserData', []);
% reset the WindowButtonUpFcn, so not listening to this event anymore
set(GIDVisFigureHandle, 'WindowButtonUpFcn', '');

% in case we're still waiting for the imrect, we have to abort this
stat = get(GIDVisFigureHandle, 'waitstatus');
if strcmp(stat, 'waiting')
    % use java robot to simulate 'Esc' key press (only way I know to abort
    % imroi stuff) #BS
    robot = java.awt.Robot;
    robot.keyPress    (java.awt.event.KeyEvent.VK_ESCAPE);
    robot.keyRelease  (java.awt.event.KeyEvent.VK_ESCAPE);   
end

function IntegratedLineScanFigure_CloseRequestFcn(src, evt, ToggleButton, h)
% close the result figure
delete(src);
% set the ToggleButton state to 'off'
set(ToggleButton, 'State', 'off');
% try to delete the imline
try
    delete(h);
end

function NewPositionDoCalculation(ImrectHandle, ResultFigHandle, BtnHandle)
if ~isempty(BtnHandle)
    set(BtnHandle, 'Enable', 'off');
end
% get the position of the imrect
NewPos = getPosition(ImrectHandle);

% get the parent of the ImrectHandle --> this is the main axes
ax = get(ImrectHandle, 'Parent');
% find the MainImage in this axes
h = findobj(ax, 'Tag', 'MainImage');
if isempty(h); return; end
% extract the data from the MainImage
x = get(h, 'XData');
y = get(h, 'YData');
z = get(h, 'CData');

% read out the current axes labels of the main axes given by ax
xlab = get(get(ax, 'XLabel'), 'String');
ylab = get(get(ax, 'YLabel'), 'String');

% In some cases the xdata has only 2 values, the lower limit and the upper
% limit. Create an x vector for these cases.
if numel(x) == 2
    x = linspace(x(1), x(2), size(z, 2));
end
% In some cases the ydata has only 2 values, the lower limit and the upper
% limit. Create a y vector for these cases.
if numel(y) == 2
    y = linspace(y(1), y(2), size(z, 1));
end

% we have to extract the intensity scaling, in order to get the correct z
% data values
MainGIDVisHandle = guidata(ax);
Scaling = ToolboxModule('GetSelectedScaling', MainGIDVisHandle.ToolboxModule);
switch lower(Scaling)
    case 'sqrt'
        z = z.^2;
    case 'log'
        z = exp(z);
    case 'linear'
        
    otherwise
        warning('GIDVis:IntegratedLineScanModule:ScalingUnknown', 'Scaling ''%s'' unknown.', Scaling)
end

% get the position of the rectangle
Pos = getPosition(ImrectHandle);
[xc, yc, SumTopBottom, SumLeftRight] = GetIntegratedLineScanProfiles(x, y, z, Pos);

sAx1 = subplot(2, 1, 1, 'Parent', ResultFigHandle);
lh1 = plot(sAx1, xc, SumTopBottom, 'DisplayName', 'Sum vertically');
FittingWithCustomBorders(lh1, 'r', @OneDimGaussianFit);
xlabel(sAx1, xlab);
ylabel(sAx1, 'Summed Intensity');
title(sAx1, sprintf('Position: %g, %g, %g, %g', NewPos(1), NewPos(2), NewPos(1)+NewPos(3), NewPos(2)+NewPos(4)));
sAx2 = subplot(2, 1, 2, 'Parent', get(sAx1, 'Parent'));
lh2 = plot(sAx2, yc, SumLeftRight, 'DisplayName', 'Sum horizontally');
FittingWithCustomBorders(lh2, 'k', @OneDimGaussianFit);
xlabel(sAx2, ylab);
ylabel(sAx2, 'Summed Intensity');

if ~isempty(BtnHandle)
    set(BtnHandle, 'Enable', 'on');
end