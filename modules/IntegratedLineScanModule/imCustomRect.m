classdef imCustomRect < imrect
    %IMCUSTOMRECT Custom extension of the built-in function "imrect"
    %   It doesn't really do much, it just expose some built-in
    %   methods or property or add trivial ones
    %
    % This file is part of GIDVis.
    %
    % see also: GIDVisLicense
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2019 Jacopo Remondina, University of Milano - Bicocca     %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Changes made to original file:
% - Adaptions to work with chars and strings (2019/03/25 BS)
    
    properties
        UserData=[];
    end
    
    methods
        function obj = imCustomRect(varargin)
            %IMCUSTOMRECT Construct an instance of this class extending
            %imrect
            obj@imrect(varargin{:});
            if isempty(obj)
                return
            end
            ContextMenu=obj.api.getContextMenu();
            ContextMenu.Tag='ROI:imCustomRect:menu'; % just in case I need to identify it
        end
        
        function addCustomMenuEntry(obj,entryLabel,entryFunction)
            if (isstring(entryLabel) && isscalar(entryLabel)) || ischar(entryLabel)
                %ok
            else
                warning('The first input (entryLabel) must be a string scalar');
                return
            end
            if isa(entryFunction,'function_handle') && isscalar(entryFunction)
                %ok
            else
                warning('The second input (entryFunction) must be a scalar function handle');
                return
            end
            ContextMenu=obj.api.getContextMenu();
            newEntry=uimenu(ContextMenu...
                ,'Label',entryLabel...
                ,'MenuSelectedFcn',entryFunction...
                ); %#ok<NASGU>
        end
        
        function api=getApi(obj)
            % just allow to retrieve the API (it was sealed in the original
            % imrect function)
            api=obj.api;
        end
        
        function deleteListener=getDeleteListener(obj)
            deleteListener=obj.graphicsDeletedListener;
        end
    end
end

