function [xc, zc, cfun, deltaCoeff, ResString] = GetLineProfile(xdata, ydata, zdata, StartPoint, EndPoint)
% Calculates the line profile for the data given by xdata, ydata, zdata
% between the start point StartPoint and the end point EndPoint and
% performs a fit on the data.
%
% Requires OneDimGaussianFit.m for the fitting process.
%
% Usage:
%     [xc, zc, cfun, deltaCoeff, ResString] = GetLineProfile(xdata, ydata, zdata, StartPoint, EndPoint)
%
% Input:
%     xdata ... x values of the data to get line profiles from
%     ydata ... y values of the data to get line profiles from
%     zdata ... z values of the data to get line profiles from
%     StartPoint ... two-element vector containing the start point (xStart, 
%                    yStart)of the line profile
%     EndPoint ... two-element vector containing the end point (xEnd, yEnd)
%                  of the line profile
%
% Output:
%     xc ... vector containing the distance along the profile
%     zc ... vector containing the extracted intensities along the line
%            profile for the points given in xc.
%     FitRes ... result of the fit performed on the extracted data.
%     FitFun ... function handle of the function used to fit the data.
%     DeltaFit ... the 95% confidence deviation of the fit coefficients
%
% This file is part of GIDVis.
%
% see also: OneDimGaussianFit, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% check size of input arguments
sz = size(zdata);
if numel(xdata) ~= sz(2) || numel(ydata) ~= sz(1)
    error('GIDVis:GetLineProfile', 'Dimension mismatch.');
end

% calculate the line profile
[cx, cy, zc] = improfile(xdata, ydata, zdata, [StartPoint(1), EndPoint(1)], [StartPoint(2), EndPoint(2)]);

% needed for the calculation of the x value for the plot of the line
% profile
cx0 = cx-StartPoint(1);
cy0 = cy-StartPoint(2);

xc = sqrt(cx0.^2+cy0.^2);

[stat, ~] = license('checkout', 'Curve_Fitting_Toolbox');
if stat && nargout > 2
    % perform 1 dim Gaussian fit
    [cfun, ~, ~, deltaCoeff] = OneDimGaussianFit(xc, zc);
else
    cfun = [];
    deltaCoeff = [];
end

if ~isempty(cfun)
    % create the result string
    if nargout >= 5
        ResString = '';
        if ~isempty(cfun)
            ResString = formula(cfun);
            names = coeffnames(cfun);
            vals = coeffvalues(cfun);
            for iC = 1:numel(vals)
                ResString = sprintf('%s\n%s = %g +/- %g', ResString, names{iC}, vals(iC), deltaCoeff(iC));
            end
        end
    end
else
    ResString = 'Could not perform fit, no Curve Fitting Toolbox license available.';
end