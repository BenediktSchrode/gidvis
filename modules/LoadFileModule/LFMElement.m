classdef LFMElement
    % Class representing the elements displayed in the Load File Module.
    %
    % Due to the fact that one file can contain multiple measurements (e.g.
    % for Eiger h5 files), it is not enough to present files in the Load
    % File Module. Thus, the display name of LFMElement is used. The data
    % of the LFMElement can then be read using ReadDataFromFile.
    %
    % see also: LoadFileModule, ReadDataFromFile, SupportedFileFormats,
    % GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2020 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    properties
        Path % Full path including file name and extension or directory path
        Type = 'file'; % 'file' or 'dir'
        EnableSave = false; % Boolean whether the Save Separately should be enabled
        EnableDelete = true; % Boolean whether the Delete Selected Element(s) should be enabled
        h5Location % Only for Eiger files: location of data in the h5 file
        h5ChunkNumber % Only for Eiger files: slice number
        h5Master % Only for Eiger files: Path to the Eiger master file, including file name and extension
        h5ImageNumber % Only for Eiger files: Image number
    end
    
    properties (Dependent = true)
        Name % File name including extension (but not the directory path) or name of the directory
        Ext % extension of the file (empty if Type is 'dir')
        DisplayName % display name as it will be displayed in the Load File Module
    end
    
    methods
        function obj = LFMElement(Path, Sorting)
            % Constructor of class LFMElement.
            %
            % Usage:
            %     obj = LFMElement(Path, Sorting);
            % 
            % Input:
            %     Path ... path to a directory or a file
            %     Sorting ... 'name' or 'date'. Sorts the files and files
            %                 by their name or by date. Directories will
            %                 always be at the top of the returned output
            %                 and sorted by their name. Parent directory
            %                 will always be first entry.
            %                 Defaulted to 'name' if not given.
            %
            % Output:
            %     obj ... vector of LFMElement containing the directories
            %             and supported files.
            if nargin == 0; return; end
            if nargin < 2 || isempty(Sorting); Sorting = 'name'; end
            
            obj = LFMElement.empty(0, 1);
            
            dirStruct = dir(Path);
            
            % directory does not exist or cannot be accessed
            if isempty(dirStruct); return; end
            
            % get the supported file types
            SupportedFileTypes = SupportedFileFormats();
            
            % remove directory . from listing
            L = strcmp({dirStruct.name}, '.');
            dirStruct = dirStruct(~L);
            
            % get the directories from listing
            Dirs = dirStruct([dirStruct.isdir]);
            % get the parent directory (i.e. the directory with name ..)
            L = strcmp({Dirs.name}, '..');
            ParentDir = Dirs(L);
            % remove the parent directory from the directories (it will be added after
            % the sorting again, otherwise it would be sorted and not the first
            % directory in the list)
            Dirs = Dirs(~L);
            % sort directories alphabetically. Use lower, otherwise it would be
            % ABC...abc instead of AaBbCc...
            [~, idx] = sort(lower({Dirs.name}));
            % add the parent directory at the beginning of the directories
            Dirs = [ParentDir; Dirs(idx)];

            % get the files (== everything in AllInDir that's not a directory)
            Files = dirStruct(~[dirStruct.isdir]);

            % get the extensions of all files
            Exts = cellfun(@GetFileExtension, {Files.name}, 'UniformOutput', false);
            % remove not supported file types from listing
            Files = Files(ismember(lower(Exts), lower(SupportedFileTypes)));
            if strcmpi(Sorting, 'name')
                % sort the files by name
                [~, idx] = sort(lower({Files.name}));
            elseif strcmpi(Sorting, 'date')
                % sort the files by date, i.e. by datenum in the structure
                [~, idx] = sort([Files.datenum]);
            end
            Files = Files(idx);
            
            % concatenate the directories and the files
            dirStruct = [Dirs; Files];
            
            for iF = 1:numel(dirStruct)
                if dirStruct(iF).isdir % deal with directories
                    if strcmp(dirStruct(iF).name, '..')
                        obj(end+1, 1).Type = 'dir';
                        obj(end).Path = '..';
                    else
                        obj(end+1, 1).Type = 'dir';
                        obj(end).Path = fullfile(dirStruct(iF).folder, ...
                            dirStruct(iF).name);
                    end
                else % deal with files
                    % get the file extension
                    [~, ~, Ext] = fileparts(dirStruct(iF).name);
                    % act based on the file extension
                    switch Ext
                        case '.h5'
                            % Deal with Eiger *.h5 files: they contain
                            % multiple measurements per file
                            
                            % only analyze the Eiger master file
                            if ~contains(lower(dirStruct(iF).name), '_master.h5')
                                continue
                            end
                            % get the information of the master file
                            S = h5info(fullfile(dirStruct(iF).folder, ...
                                dirStruct(iF).name), '/entry/data');
                            % loop over links (they contain the information
                            % about the data files and where to find the
                            % data in the data files (the so-called
                            % datasetname))
                            for iL = 1:numel(S.Links)
                                % get the value of the link (contains name
                                % of data file and datasetname)
                                Val = S.Links(iL).Value;
                                % get full path to data file
                                DataFilePath = fullfile(dirStruct(iF).folder, Val{1});
                                % check for existence
                                if exist(DataFilePath, 'file') ~= 2
                                    warning('GIDVis:LoadFileModule:MissingDataFile', ...
                                        'File ''%s'' is referenced in h5 master file but does not exist in directory ''%s''.', ...
                                        Val{1}, dirStruct(iF).folder);
                                    continue;
                                end
                                % try to read data file, it might be
                                % corrupted.
                                try
                                    S2 = h5info(DataFilePath, Val{2});
                                catch ex
                                    if strcmpi('MATLAB:imagesci:h5info:fileOpenErr', ex.identifier)
                                        msgbox(sprintf('File ''%s'' could not be found.', Val{1}), 'modal');
                                        continue;
                                    end
                                end
                                % extract the number of measurements stored
                                % in this data file
                                sz = S2.Dataspace.Size(3);
                                % create numbers representing the
                                % measurements (they do not start from one
                                % but the value which is in the file)
                                ImgNr = S2.Attributes(1).Value:S2.Attributes(2).Value;
                                % loop over each measurement in this data
                                % file
                                for iS = 1:sz
                                    % extract the information and store it
                                    % in obj
                                    obj(end+1, 1).Path = fullfile(DataFilePath);
                                    obj(end).h5Location = Val{2};
                                    obj(end).h5ChunkNumber = iS;
                                    obj(end).h5Master = fullfile(dirStruct(iF).folder, dirStruct(iF).name);
                                    obj(end).h5ImageNumber = ImgNr(iS);
                                    % the Save Separately context menu
                                    % entry should be enabled
                                    obj(end).EnableSave = true;
                                    % the Delete context menu entry should
                                    % be disabled
                                    obj(end).EnableDelete = false;
                                end
                            end
                        otherwise
                            % one measurement per data file, only set the
                            % path property
                            obj(end+1, 1).Path = fullfile(dirStruct(iF).folder, ...
                                dirStruct(iF).name);
                    end
                end
            end
        end
        
        function Str = get.DisplayName(obj)
            % Returns the DisplayName of the LFMElement depending on the
            % Type, i.e. file or directory, and the file (currently Eiger
            % or any other)
            if strcmpi(obj.Type, 'dir')
                Str = ['> ', obj.Name];
            elseif strcmpi(obj.Type, 'file')
                if strcmpi(obj.Ext, '.h5')
                    [~, FN] = fileparts(obj.h5Master);
                    Str = [strrep(FN, '_master.h5', '.h5'), ' | Image Number ', num2str(obj.h5ImageNumber)];
                else
                    Str = obj.Name;
                end
            end
        end
        
        function Str = NameSuggestion(obj, Prefix)
            % Returns a name suggestion.
            if nargin < 2 || isempty(Prefix); Prefix = ''; end
            if strcmpi(obj.Ext, '.h5')
                [DirName, FN] = fileparts(obj.h5Master);
                Str = fullfile(DirName, [Prefix, strrep(FN, '_master', ''), ' Image Number ', num2str(obj.h5ImageNumber), '.mat']);
            else
                [DirName, FN] = fileparts(obj.Path);
                Str = fullfile(DirName, [Prefix, FN, '.mat']);
            end
        end
        
        function Val = get.Ext(obj)
            % Returns the file extension of the LFMElement. Empty if the
            % element is of type 'dir'.
            if strcmp(obj.Type, 'file')
                [~, ~, Val] = fileparts(obj.Path);
            else
                Val = '';
            end
        end
        
        function Str = get.Name(obj)
            [~, FileName, Extension] = fileparts(obj.Path);
            Str = [FileName, Extension];
        end
        
        function Str = CreateCode(obj)
            % creates MATLAB code to generate the LFMElement
            if numel(obj) > 1
                error('GIDVis:LFMElement:CreateCode:ElementCount', ...
                    'The input argument should be one element');
            end
            mco = ?LFMElement;
            
            Str = {'El = LFMElement;'};

            for iP = 1:numel(mco.PropertyList)
                if ~strcmp(mco.PropertyList(iP).SetAccess, 'public'); continue; end
                if mco.PropertyList(iP).Dependent; continue; end
                if any(contains({'EnableSave', 'EnableDelete'}, mco.PropertyList(iP).Name)); continue; end
                if mco.PropertyList(iP).HasDefault && isequal(mco.PropertyList(iP).DefaultValue, obj.(mco.PropertyList(iP).Name)); continue; end
                if isempty(obj.(mco.PropertyList(iP).Name)); continue; end
                Val = obj.(mco.PropertyList(iP).Name);

                Str{end+1, 1} = sprintf('El.%s = %s;', mco.PropertyList(iP).Name, obj.PrintValue(Val));
            end
        end
    end
    
    methods (Access = private, Static = true)
        function Str = PrintValue(Val)
            Str = '';

            if iscell(Val) && numel(Val) == 1; Val = Val{1}; end

            if ischar(Val)
                Str = sprintf('%s''%s''', Str, Val);
            elseif isstring(Val)
                Str = sprintf('%s"%s"', Str, Val);
            elseif isnumeric(Val) && numel(Val) == 1
                Str = sprintf('%s%g', Str, Val);
            elseif (isnumeric(Val) && numel(Val) > 1) || ...
                    iscell(Val)
                if iscell(Val)
                    Brackets = '{}';
                elseif isnumeric(Val)
                    Brackets = '[]';
                end
                Str = sprintf('%s%s', Str, Brackets(1));
                for iR = 1:size(Val, 1)
                    for iC = 1:size(Val, 2)
                        Str = sprintf('%s%s', Str, PrintValue(Val(iR, iC)));
                        if iC ~= size(Val, 2); Str = sprintf('%s ', Str); end
                    end
                    if iR ~= size(Val, 1)
                        Str = sprintf('%s; ', Str);
                    else
                        Str = sprintf('%s%s', Str, Brackets(2));
                    end
                end
            elseif isstruct(Val)
                Str = sprintf('%sstruct(', Str);
                Names = fieldnames(Val);
                for iN = 1:numel(Names)
                    Str = sprintf('%s%s', Str, PrintValue(Names{iN}));
                    Str = sprintf('%s, ', Str);
                    Str = sprintf('%s%s', Str, PrintValue(Val.(Names{iN})));
                    if iN ~= numel(Names)
                        Str = sprintf('%s, ', Str);
                    else
                        Str = sprintf('%s)', Str);
                    end
                end
            end
        end
    end
end

