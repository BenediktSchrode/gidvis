function varargout = LoadFileModule(varargin)
% This is the LoadFileModule for GIDVis. 
% Here you can select a path. Tif files in this path are then shown in a
% listbox. Select a file in the listbox.
% Furthermore, this window shows the selected beamline.
%
% Usage:
%     varargout = LoadFileModule(MainAxesHandle, BeamtimePath)
%
% Input:
%      MainAxesHandle ... AxesHandle of main window axes
%      BeamtimePath ... Folder where the beamtimes are stored
%
% Output:
%     varargout ... default GUIDE output
%
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2020 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @LoadFileModule_OpeningFcn, ...
                   'gui_OutputFcn',  @LoadFileModule_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before LoadFileModule is made visible.
function LoadFileModule_OpeningFcn(hObject, eventdata, handles, varargin)
% Choose default command line output for LoadFileModule
handles.output = hObject;

% store the main axes in the handles structure
handles.MainAxes = varargin{1};

% the settings for this module are stored in the same directory as the file
% LoadFileModule.m. Store its path in the handles structure to reuse it.
handles.SettingsPath = fullfile(fileparts(mfilename('fullpath')), 'Settings.mat');

% get the GIDVis root directory
if ~isdeployed
    handles.RootDirectory = fileparts(varargin{2});
else
    handles.RootDirectory = ctfroot;
end

% get the GIDVis demo directory
if ~isdeployed
    handles.DemoDirectory = fullfile(pwd, 'Demo');
else
    S = dir(fullfile(ctfroot, 'Demo', 'Demo.mat'));
    if ~isempty(S)
        handles.DemoDirectory = S.folder;
    else
        handles.DemoDirectory = pwd;
    end
end

% read out settings if available
if exist(handles.SettingsPath, 'file') == 2
    load(handles.SettingsPath);
    % store the settings in the handles structure
    handles.Settings = SettingsLoadFileModule;
    if exist(handles.Settings.CurrentPath, 'dir') ~= 7
        handles.Settings.CurrentPath = pwd;
    end
    if isfield(handles.Settings, 'AutoDetectBeamlineValue')
        set(handles.CB_AutoDetectBeamline, 'Value', handles.Settings.AutoDetectBeamlineValue);
        CB_AutoDetectBeamline_Callback(handles.CB_AutoDetectBeamline, [], handles);
    end
    if isfield(handles.Settings, 'SortByNameValue')
        set(handles.RB_SortByName, 'Value', handles.Settings.SortByNameValue);
    end
    if isfield(handles.Settings, 'SelectResultFileValue')
        handles.SelectResultFile = handles.Settings.SelectResultFileValue;
    else
        handles.SelectResultFile = true;
    end
else % use some default
    handles.Settings.CurrentPath = handles.DemoDirectory;
    handles.Settings.SelectedBeamtime = 1;
    handles.Settings.SelectedBeamline = 1;
    handles.SelectResultFile = 'on';
end

% create context menu for file list box
hcmenu = uicontextmenu('Parent', hObject);
uimenu(hcmenu, 'Label', 'Merge Selected Images', 'Callback', @CombineSelectedImages_Callback);
uimenu(hcmenu, 'Label', 'Generate Pole Figure(s)', 'Callback', @GeneratePoleFigures_Callback);

uim = uimenu(hcmenu, 'Label', 'Z Projection');
uimenu(uim, 'Label', 'Average Selected Images', 'Callback', @(src, evt) SumAverageSelectedImages_Callback(src, evt, 'average'));
uimenu(uim, 'Label', 'Sum Selected Images', 'Callback', @(src, evt) SumAverageSelectedImages_Callback(src, evt, 'sum'));
uimenu(uim, 'Label', 'Extract Highest Intensities', 'Callback', @ExtractHighestIntensities_Callback);
uimenu(uim, 'Label', 'Select Result File After Operation', 'Separator', 'on', ...
    'Tag', 'SelectResultFileZProj', 'Callback', @SelectResultFile_Callback);

uim = uimenu(hcmenu, 'Label', 'Math');
uimenu(uim, 'Label', 'Add', 'Callback', @(src, evt) MathSelectedImages_Callback(src, evt, 'add'));
uimenu(uim, 'Label', 'Subtract', 'Callback', @(src, evt) MathSelectedImages_Callback(src, evt, 'subtract'));
uimenu(uim, 'Label', 'Multiply', 'Callback', @(src, evt) MathSelectedImages_Callback(src, evt, 'multiply'));
uimenu(uim, 'Label', 'Divide', 'Callback', @(src, evt) MathSelectedImages_Callback(src, evt, 'divide'));
uimenu(uim, 'Label', 'Select Result File After Operation', 'Separator', 'on', ...
    'Tag', 'SelectResultFileMath', 'Callback', @SelectResultFile_Callback);

uim = uimenu(hcmenu, 'Label', 'File Dependent Corrections');
uimenu(uim, 'Label', 'Footprint Correction', 'Callback', @ApplyFootprintCorrection_Callback);
uimenu(uim, 'Label', 'Divide by Header Information', 'Callback', @DivideByHeaderInformation_Callback);
uimenu(uim, 'Label', 'Select Result File After Operation', 'Separator', 'on', ...
    'Tag', 'SelectResultFileFileDep', 'Callback', @SelectResultFile_Callback);

uimenu(hcmenu, 'Label', 'Create GIF from Selected Images', 'Callback', @CreateGIFFromSelectedImages_Callback);
uimenu(hcmenu, 'Label', 'Save as Separate File(s)', 'Callback', @SaveSeparately_Callback, 'Tag', 'uim_SaveSeparately');
uimenu(hcmenu, 'Label', 'Copy Name', 'Callback', @CopyName_Callback, 'Separator', 'on');
uimenu(hcmenu, 'Label', 'Copy Path', 'Callback', @CopyPath_Callback);
if ~isdeployed
    uimenu(hcmenu, 'Label', 'Copy LFMElement', 'Callback', @CopyLFMElement_Callback);
end
uimenu(hcmenu, 'Label', 'Delete Selected Element(s)', 'Callback', @DeleteSelectedElements_Callback, 'Tag', 'uim_DeleteElements');
uimenu(hcmenu, 'Label', 'File Properties', 'Callback', @FileProperties_Callback, 'Tag', 'uim_FileProperties');

% set the context menu to the files list box
set(handles.LB_Files, 'UIContextMenu', hcmenu);

% make a callback for the context menu (used to decide which children of
% the uicontextmenu to enable/disable/check)
set(hcmenu, 'Callback', @LB_Files_ContextMenu_Callback);

% update the listbox showing the files
Elements = UpdateFilesListbox(handles, handles.Settings.CurrentPath);
handles.Elements = Elements;

% default value for handles.LastDirectoryClickTime
handles.LastDirectoryClickTime = zeros(1, 6);

% default value for handles.LastSelectedIndex
handles.LastSelectedIndex = -1;

% default value for handles.ReturnKeyPressed (used to detect pressing
% return key on a directory in LB_Files)
handles.ReturnKeyPressed = false;

% get Beamline Folder path of GIDVis.m
handles.BeamtimePath = varargin{2};
% Update handles structure
guidata(hObject, handles);
% update the Popupbox showing the files
% load the contents for the beamtime selection box
PopulateBeamtimePopupBox(handles.Fig_LoadFileModule);
% set the values of the beamtime and beamline selection box
set(handles.BeamtimeSelection, 'Value', handles.Settings.SelectedBeamtime);
set(handles.BeamlineSelection, 'Value', handles.Settings.SelectedBeamline);
% Update according to the selected beamtime/beamline
UpdateSelectedBeamtime(handles.Fig_LoadFileModule);
% UpdateSelectedBeamtime changes the handles structure, so reload it
handles = guidata(hObject);


% add a listener to the axes in varargin{1}. When the axes gets destroyed 
% (e.g. because the figure containing the axes gets closed), the callback 
% will be executed (and closes this window).
% Store the listener in the handles structure to remove it when this window    
% gets closed by user, otherwise the callback is executed even when the
% LoadFileModule window does not exist anymore.
handles.MainAxesDestroyedListener = addlistener(varargin{1}, 'ObjectBeingDestroyed', @(src, evt) MainAxesDestroyed_Callback(src, evt, handles.Fig_LoadFileModule));

% used to decide whether to make the window only invisible/really close it
handles.CloseReally = false;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes LoadFileModule wait for user response (see UIRESUME)
% uiwait(handles.Fig_LoadFileModule);

function CopyLFMElement_Callback(src, evt)
% get selected elements
Elements = GetSelectedFilePath(src);

if numel(Elements) ~= 1
    msgbox('Please select exactly one element from the list.', 'modal')
    return;
end

% get the code representation
Str = CreateCode(Elements);

% create one string out of the cell
Output = strjoin(Str, '\n');

% copy code representation to the clipboard
clipboard('copy', Output)



function SelectResultFile_Callback(src, evt)
% get handles structure
handles = guidata(src);
% change the state
handles.SelectResultFile = ~handles.SelectResultFile;
% Update handles structure
guidata(src, handles);

function SaveSeparately_Callback(src, evt)
% get selected elements
Elements = GetSelectedFilePath(src);
% get indices of selected elements
handles = guidata(src);
SelInd = get(handles.LB_Files, 'Value');
% get the directory
D = fileparts(Elements(1).Path);
% loop over the single elements
for iF = 1:numel(Elements)
    % update status label
    set(handles.TB_Status, 'String', sprintf('Working on file %d of %d.', iF, numel(Elements)));
    drawnow;
    % read data and header
    [M, H] = ReadDataFromFile(Elements(iF));
    H = H{1};
    H = [{'File Information', sprintf('Data of file ''%s''.', Elements(iF).DisplayName)}; H];
    % create the file name including path to save to
    Path = Elements(iF).NameSuggestion;
    % save the data including the header
    SaveDataToMat(Path, M, H);
end

[~, NameOfFirst, Ext] = fileparts(Elements(1).NameSuggestion);
NameOfFirst = [NameOfFirst, Ext];

% Update the listbox containing the files
Elements = UpdateFilesListbox(handles, get(handles.TB_CurrentPath, 'String'));
handles.Elements = Elements;

% select the extracted file
idx = find(strcmp({Elements.DisplayName}, NameOfFirst));
if ~isempty(idx)
    set(handles.LB_Files, 'Value', idx);
else
    % try to restore the listbox's selected element
    set(handles.LB_Files, 'Value', SelInd);
end

% update handles structure
guidata(src, handles);

% update the plot
RequestPlotUpdateFromFilePath(handles.MainAxes);

% update status label
set(handles.TB_Status, 'String', 'Saving finished.');
drawnow;

function LB_Files_ContextMenu_Callback(src, evt)
% get handles structure
handles = guidata(src);
% get selected elements
Elements = GetSelectedFilePath(src);
% check if all selected files have the EnableSave property true, then
% enable the Save Separately context menu entry
Ch = findobj(src, 'Tag', 'uim_SaveSeparately');
if all([Elements.EnableSave])
    set(Ch, 'Enable', 'on');
else
    set(Ch, 'Enable', 'off');
end
% check if any of the elements has the EnableDelete property false, then
% disbale the Delete Selected Element(s) context menu entry
Ch = findobj(src, 'Tag', 'uim_DeleteElements');
if any([Elements.EnableDelete] == false)
    set(Ch, 'Enable', 'off');
else
    set(Ch, 'Enable', 'on');
end
% find the context menu entries for the selection of the result file
Ch = findobj(src, 'Tag', 'SelectResultFileZProj', '-or', ...
    'Tag', 'SelectResultFileMath', '-or', ...
    'Tag', 'SelectResultFileFileDep');
% set their checked value according to the handles structure
if handles.SelectResultFile
    set(Ch, 'Checked', 'on');
else
    set(Ch, 'Checked', 'off');
end

function CreateGIFFromSelectedImages_Callback(src, evt)
handles = guidata(src);

% get selected elements
Elements = GetSelectedFilePath(src);

% check for directories in the selection
if any(strcmp({Elements.Type}, 'dir'))
    msgbox('At least one directory is contained in the selection. Please select only files!', 'modal')
    return;
end

% check for number of selected items
NumSel = numel(Elements);
if NumSel == 1
    msgbox('Select two or more files using Ctrl and/or Shift key.', 'modal');
    return;
end

% get the extensions of the selected files
Exts = {Elements.Ext};
if any(strcmp(Exts, '.GIDDat'))
    msgbox('At the moment only raw data can be used for GIF creation, *.GIDDat files are not raw data.', 'modal');
    return;
end

CreateGIF(Elements, handles.MainAxes);

function CombineSelectedImages_Callback(src, evt)
handles = guidata(src);

% get selected elements
Elements = GetSelectedFilePath(src);

% check for directories in the selection
if any(strcmp({Elements.Type}, 'dir'))
    msgbox('At least one directory is contained in the selection. Please select only files!', 'modal')
    return;
end

% check for number of selected items
NumSel = numel(Elements);
if NumSel == 1
    msgbox('Select two or more files using Ctrl and/or Shift key.', 'modal');
    return;
end

% get the extensions of the selected files
Exts = {Elements.Ext};
if any(strcmp(Exts, '.GIDDat'))
    msgbox('Only raw data can be merged, *.GIDDat files are not raw data.', 'modal');
    return;
end

[~, SavePath] = MergeModule(Elements, handles.MainAxes, handles.BeamtimePath);

if isempty(SavePath); return; end

DirPath = fileparts(SavePath);
if strcmpi(DirPath, get(handles.TB_CurrentPath, 'String'))
    Elements = UpdateFilesListbox(handles, get(handles.TB_CurrentPath, 'String'));
    handles.Elements = Elements;
    Files = {Elements.Path};
    idx = find(strcmp(Files, SavePath));
    set(handles.LB_Files, 'Value', idx);
    
    guidata(src, handles);
    
    RequestPlotUpdateFromFilePath(handles.MainAxes);
end

function CopyName_Callback(src, evt)
% get selected elements
Elements = GetSelectedFilePath(src);

if numel(Elements) ~= 1
    msgbox('Please select exactly one element from the list.', 'modal')
    return;
end

Output = '';
if strcmp(Elements.Type, 'dir') % Directory selected
    % get the name of the selected directory
    P = Elements.Path;
    ind = find(P == filesep, 1, 'last');
    if ~isempty(ind) && ind + 1 <= numel(P)
        Output = P(ind+1:end);
    end
else
    % get the name of the selected file
    Output = Elements.Name;
end

% copy FileName to clipboard
clipboard('copy', Output)

function CopyPath_Callback(src, evt)
% get selected elements
Elements = GetSelectedFilePath(src);

if numel(Elements) ~= 1
    msgbox('Please select exactly one element from the list.', 'modal')
    return;
end

% get the name of the selected file
Output = Elements.Path;
% copy file name including whole path to clipboard
clipboard('copy', Output)

function FileProperties_Callback(src, evt)
handles = guidata(src);

% get selected elements
Elements = GetSelectedFilePath(src);
% get the number of selected files
NumSel = numel(Elements);
    
if NumSel == 1
    % get the name of the selected file
    FileName = Elements.Name;
    % get the file extension
    Ext = Elements.Ext;

    % get file properties
    L = dir(Elements.Path);

    % find the main image
    h = findobj(handles.MainAxes, 'Tag', 'MainImage');
    % read out the image data
    ImageCData = get(h, 'CData');
    ImageXData = get(h, 'XData');
    ImageYData = get(h, 'YData');
    if numel(ImageXData) == 2
        ImageXData = linspace(ImageXData(1), ImageXData(2), size(ImageCData, 2));
    end
    if numel(ImageYData) == 2
        ImageYData = linspace(ImageYData(1), ImageYData(2), size(ImageCData, 1));
    end
    % get the currently selected scaling
    GD = guidata(handles.MainAxes);
    Scaling = ToolboxModule('GetSelectedScaling', GD.ToolboxModule);
    % find the maximum intensity value
    [MaxVal, IndMaxVal] = max(ImageCData(:));
    [IndIMax, IndJMax] = ind2sub(size(ImageCData), IndMaxVal);
    % find the minimum intensity value
    [MinVal, IndMinVal] = min(ImageCData(:));
    [IndIMin, IndJMin] = ind2sub(size(ImageCData), IndMinVal);
    % adapt the intensity values according to the selected scaling
    switch lower(Scaling)
        case 'sqrt'
            MaxVal = MaxVal^2;
            MinVal = MinVal^2;
        case 'log'
            MaxVal = exp(MaxVal);
            MinVal = exp(MinVal);
        case 'linear'
            % nothing to do
        otherwise
            warning('GIDVis:LoadFileModule:UnknownScaling', 'Scaling unknown.')
    end

    % create the content for the file properties
    PropertiesCell = {'File Name', FileName; ...
    'Modification date', L.date; ...
    'Maximum intensity (green dot)', sprintf('%g (at %g, %g)', MaxVal, ImageXData(IndJMax), ImageYData(IndIMax)); ...
    'Minimum intensity (red dot)', sprintf('%g (at %g, %g)', MinVal, ImageXData(IndJMin), ImageYData(IndIMin))};

    % add file properties depending on file extension
    switch lower(Ext)
        case '.giddat'
            MainGIDVisHandle = guidata(handles.MainAxes);
            % check for existence and integrity of MainGIDVisHandle.RawData
            if ~isfield(MainGIDVisHandle, 'RawData') || ...
                    isempty(MainGIDVisHandle.RawData) || ...
                    ~isa(MainGIDVisHandle.RawData, 'qData')
                Q = qData(fullfile(get(handles.TB_CurrentPath, 'String'), FileName));
            else
                Q = MainGIDVisHandle.RawData;
            end
            PropertiesCell = [PropertiesCell; {'File Information', Q.DataInformation}];
        otherwise
            [~, header] = ReadDataFromFile(Elements);
            H = header{1};
            for iH = 1:size(H, 1)
                if isnumeric(H{iH, 2})
                    if numel(H{iH, 2}) == 1 % scalar
                        % just add the name and value to the cell
                        PropertiesCell = [PropertiesCell; {H{iH, 1}, H{iH, 2}}];
                    elseif any(size(H{iH, 2}) == 1) % vector
                        if numel(H{iH, 2}) <= 10
                            % add the name and the vector values in format
                            % similar to num2str.
                            Values = H{iH, 2};
                            ValueString = sprintf(repmat('%g ', 1, numel(Values)), ...
                                Values);
                            PropertiesCell = [PropertiesCell; {H{iH, 1}, ValueString(1:end-1)}];
                        else
                            PropertiesCell = [PropertiesCell; {H{iH, 1}, sprintf('%g x %g vector', size(H{iH, 2}))}];
                        end
                    else % matrix
                        % print the size of the matrix
                        PropertiesCell = [PropertiesCell; {H{iH, 1}, sprintf('%g x %g matrix', size(H{iH, 2}))}];
                    end
                elseif ischar(H{iH, 2})
                    PropertiesCell = [PropertiesCell; {H{iH, 1}, H{iH, 2}}];
                end
            end
    end

    MessageString = '';
    for iH = 1:size(PropertiesCell, 1)
        if isnumeric(PropertiesCell{iH, 2})
            MessageString = sprintf(['%s<br><b>%s:</b>', repmat(' %g', 1, numel(PropertiesCell{iH, 2}))], MessageString, PropertiesCell{iH, 1}, PropertiesCell{iH, 2});
        elseif ischar(PropertiesCell{iH, 2})
            if ~isempty(strfind(PropertiesCell{iH, 2}, char(10)))
                % apparently the < symbol is the only symbol which needs to
                % be replaced by its html entity.
                ReplacedString = strrep(PropertiesCell{iH, 2}, '<', '&lt;');
                % Replace linebreaks n by html line breaks <br>
                ReplacedString = strrep(ReplacedString, char(10), '<br>');
                MessageString = sprintf('%s<br><p><b>%s:</b><br>%s</p>', MessageString, PropertiesCell{iH, 1}, ReplacedString);
            else
                ReplacedString = strrep(PropertiesCell{iH, 2}, '<', '&lt;');
                MessageString = sprintf('%s<br><b>%s:</b> %s', MessageString, PropertiesCell{iH, 1}, ReplacedString);
            end
        end
    end
    
    MessageString = ['<div style="font-family:sans-serif"><font size="5">' MessageString '</font></div>'];
    h = scrollmsgbox(MessageString, 'File Properties', 'Style', 'html');

    % plot the points of lowest and highest intensity in the main image
    hold(handles.MainAxes, 'on');
    LH(1) = plot(ImageXData(IndJMax), ImageYData(IndIMax), 'o', ...
        'MarkerFaceColor', 'g', 'MarkerEdgeColor', 'g', ...
        'Parent', handles.MainAxes, 'DisplayName', 'Maximum Intensity');
    LH(2) = plot(ImageXData(IndJMin), ImageYData(IndIMin), 'o', ...
        'MarkerFaceColor', 'r', 'MarkerEdgeColor', 'r', ...
        'Parent', handles.MainAxes, 'DisplayName', 'Minimum Intensity');
    hold(handles.MainAxes, 'off');
    addlistener(h, 'ObjectBeingDestroyed', @(src, evt) FilePropertyWindowDestroyed_Callback(LH));
else
    if any(strcmp({Elements.Type}, 'dir'))
        msgbox('At least one directory is contained in the selection. Please select only files!', 'modal')
        return;
    end
    
    Choices = {};
    
    % loop over selected files
    for iF = 1:numel(Elements)
        % update status label
        set(handles.TB_Status, 'String', sprintf('Reading file %d of %d.', iF, NumSel));
        drawnow;
        % get file extension
        Ext = Elements(iF).Ext;
        % read out file info
        [~, header] = ReadDataFromFile(Elements(iF));
        FullHeader = header{1};
        if isempty(FullHeader); continue; end
        [~, idx] = sort(lower(FullHeader(:, 1)));
        FullHeader = FullHeader(idx, :);
        
        if isempty(Choices)
            Choices = MultiSelectionDialog('Select elements to plot:', ...
                FullHeader(:, 1), 'Resize', 'on', ...
                'Name', 'Selection Required', 'NumberTitle', 'off');
            if isempty(Choices); return; end
            Values = cell(numel(Elements), numel(Choices));
        end
        
        for iC = 1:numel(Choices)
            Choice = Choices{iC};
            ind = ismember(FullHeader(:, 1), Choice);
            if sum(ind) == 0
                warning('GIDVis:LoadFileModule:NoPropertyFound', ...
                    'Could not find property ''%s'' for file ''%s''.', ...
                    Choice, Elements(iF).DisplayName);
                Values{iF, iC} = NaN;
                continue;
            end
            if isempty(FullHeader{ind, 2})
                warning('GIDVis:LoadFileModule:PropertyEmpty', ...
                    'Property ''%s'' for file ''%s'' is empty.', ...
                    Choice, Elements(iF).DisplayName);
                Values{iF, iC} = NaN;
                continue;
            end
            if isnumeric(FullHeader{ind, 2})
                Value = FullHeader{ind, 2};
                Values{iF, iC} = Value(:);
            elseif ischar(FullHeader{ind, 2})
                % try converting string to a number
                Values{iF, iC} = str2double(FullHeader{ind, 2});
                % if success, continue in the loop
                if ~isnan(Values{iF, iC}); continue; end
                % try converting string to a date
                try
                    Values{iF, iC} = datetime(FullHeader{ind, 2});
                end
                % if success, continue in the loop
                if ~isnan(Values{iF, iC}); continue; end
            end
        end
    end
    
    if isempty(Choices)
        msgbox('No header information could be extracted.', 'modal');
    else
        EmptyValues = cellfun(@isempty, Values);
        Values(EmptyValues) = {NaN};
        % first plot all elements which have scalar values
        L = all(cellfun(@numel, Values) <= 1);
        if sum(L) > 0
            PlotValues = reshape([Values{:, L}], [], sum(L));
            f = figure;
            if any(isnan(PlotValues(:)))
                plot(PlotValues, 'o-');
            else
                plot(PlotValues, '-');
            end
            xlabel('File Number');
            l = legend(Choices(L));
            set(l, 'Interpreter', 'none');
            AddExportLineDataButton(f);
        end
        % plot all the elements which do not have scalar values
        if sum(~L) > 0
            for iN = 1:numel(Choices)
                if L(iN); continue; end % skip scalar values
                PlotValues = [Values{:, iN}];
                f = figure;
                ax = axes('Parent', f);
                imagesc(ax, PlotValues);
                title(ax, Choices{iN}, 'Interpreter', 'none');
                xlabel(ax, 'File Number');
                ylabel(ax, 'Position in vector/matrix')
                set(ax, 'YDir', 'normal')
                colorbar(ax)
            end
        end
    end

    % update status label
    set(handles.TB_Status, 'String', 'Process completed.');
    drawnow;
end

function FilePropertyWindowDestroyed_Callback(LH)
delete(LH);

function DeleteSelectedElements_Callback(src, evt)
handles = guidata(src);

% get selected elements
Elements = GetSelectedFilePath(src);

button = questdlg('Do you really want to delete the selected elements?', 'Delete?', 'Yes', 'No', 'Yes');
if strcmp(button, 'No') || isempty(button)
    return;
end

SelInd = get(handles.LB_Files, 'Value');

% get MATLAB search path
SearchPath = path;
SearchPathCell = regexp(SearchPath, pathsep, 'split');

% loop over selected elements
for iF = 1:numel(Elements)
    % avoid removing the .. dir
    if strcmp(Elements(iF).DisplayName, '> ..')
        continue;
    end
    % update status label
    set(handles.TB_Status, 'String', sprintf('Deleting element %d of %d.', iF, numel(Elements)));
    drawnow;
    % check if selected item is a directory or a file
    if strcmp(Elements(iF).Type, 'dir')
        % if the folder we want to delete is on the search path, remove it
        % before deleting
        if any(ismember(SearchPathCell, Elements(iF).Path))
            rmpath(Elements(iF).Path);
        end
        try
            rmdir(Elements(iF).Path, 's');
        catch ex
            warning('GIDVis:LoadFileModule', 'Could not delete directory ''%s''.', Elements(iF).Path);
        end
    else
        delete(Elements(iF).Path);
    end
end

% Update the listbox containing the files
Elements = UpdateFilesListbox(handles, get(handles.TB_CurrentPath, 'String'));
handles.Elements = Elements;

% try to restore the listbox's selected element
set(handles.LB_Files, 'Value', min([numel(Elements), SelInd]));
% update the plot
LB_Files_Callback(handles.LB_Files, [], handles);

% update status label
set(handles.TB_Status, 'String', 'Process completed.');
drawnow;

guidata(src, handles);

function ExtractHighestIntensities_Callback(src, evt)
handles = guidata(src);

% get selected elements
Elements = GetSelectedFilePath(src);

if any(strcmp({Elements.Type}, 'dir'))
    msgbox('At least one directory is contained in the selection. Please select only files!', 'modal')
    return;
end

NumSel = numel(Elements);
if NumSel == 1 || NumSel == 0
    msgbox('Select two or more files using Ctrl and/or Shift key.', 'modal');
    return;
end

% store the indices of the selected elements
SelInd = get(handles.LB_Files, 'Value');

% get the extensions of the selected files
Exts = {Elements.Ext};
if any(strcmp(Exts, '.GIDDat'))
    msgbox('Only information from raw data files can be extracted, *.GIDDat files are not raw data.', 'modal');
    return;
end

% update status label
set(handles.TB_Status, 'String', sprintf('Reading file 1 of %d.', NumSel));
drawnow;
% read out data of first file (to get array data of correct size)
data = ReadDataFromFile(Elements(1));
data(data < 0) = NaN;

% loop over selected files beginning with the second (first one was treated
% separately)
for iF = 2:NumSel
    % update status label
    set(handles.TB_Status, 'String', sprintf('Reading file %d of %d.', iF, NumSel));
    drawnow;
    % read out intensity
    datanew = ReadDataFromFile(Elements(iF));
    % replace intensities smaller 0 by NaN
    datanew(datanew < 0) = NaN;
    % find intensities from datanew which are larger than intensities in
    % data or where data is NaN but datanew is not NaN
    L = (datanew > data) | (isnan(data) & ~isnan(datanew));
    % replace the data by datanew, where datanew > data
    data(L) = datanew(L);
end

% data = double(data);

% update status label
set(handles.TB_Status, 'String', 'Calculation in progress.');
drawnow;

% get the handles structure of the main window
handlesMainWindow = guidata(handles.MainAxes);
% set the RawData of this handles structure to our previously calculated
% data
handlesMainWindow.RawData = data;
% update the handles structure of the main window
guidata(handles.MainAxes, handlesMainWindow);
% run function RequestPlotUpdateFromData. This function takes the data from
% handlesMainWindow.RawData and plots it
RequestPlotUpdateFromData(handles.MainAxes);

% save the data as a mat file
% update status label
set(handles.TB_Status, 'String', 'Saving');
drawnow;
% find a name suggestion: remove everything from the filename after the
% last underscore, add MAXINT_ in front
FN = Elements(1).Name;
Ind = strfind(FN, '_');
if isempty(Ind) || (numel(Ind) == 1 && Ind == 1)
    DefaultPathName = fullfile(pwd, 'MAXINT_');
else
    DefaultPathName = fullfile(get(handles.TB_CurrentPath, 'String'), ['MAXINT_', FN(1:Ind(end)-1)]);
end
[FileName, PathName, FilterIndex] = uiputfile({'*.mat', 'Mat File'}, 'Save maximum intensity image', DefaultPathName);
if FilterIndex ~= 0
    SaveDataToMat(fullfile(PathName, FileName), data, ...
        'File created by extracting the highest intensities of multiple images.');
    % if the file is saved to the currently selected path, update the
    % listbox and set the value of the listbox to this file
    CurrentPath = fileparts(Elements(1).Path);
    if strcmpi(PathName(1:end-1), CurrentPath)
        Elements = UpdateFilesListbox(handles, get(handles.TB_CurrentPath, 'String'));
        handles.Elements = Elements;
        if handles.SelectResultFile
            idx = find(strcmp({Elements.Path}, fullfile(PathName, FileName)));
            set(handles.LB_Files, 'Value', idx);
        else
            set(handles.LB_Files, 'Value', SelInd);
        end
    end
end

% update status label
set(handles.TB_Status, 'String', 'Process completed.');
drawnow;

guidata(src, handles);

function DivideByHeaderInformation_Callback(src, evt)
handles = guidata(src);

% get selected elements
Elements = GetSelectedFilePath(src);

if any(strcmp({Elements.Type}, 'dir'))
    msgbox('At least one directory is contained in the selection. Please select only files!', 'modal')
    return;
end

% get the extensions of the selected files
Exts = {Elements.Ext};
if any(strcmp(Exts, '.GIDDat'))
    msgbox('''Divide by Header Information'' can only be applied to raw data, *.GIDDat files are not raw data.', 'modal');
    return;
end

% get the indices of the selected files
SelInd = get(handles.LB_Files, 'Value');

% initialize variable to store selected header
SelHeader = '';

% loop over files
for iF = 1:numel(Elements)
    % update status label
    set(handles.TB_Status, 'String', sprintf('Working on file %d of %d.', iF, numel(Elements)));
    drawnow;
    % read out data of file
    [data, header] = ReadDataFromFile(Elements(iF));
    header = header{1};
    % get file name
    FN = Elements(iF).Name;
    
    % let user select which parameter to use
    if isempty(SelHeader)
        % there might be no information extracted from the header. In this
        % case, continue with the next file
        if isempty(header); continue; end
        % look for numeric values in the header
        LNumeric = cellfun(@isnumeric, header(:, 2));
        % look for values which have numel 1
        LNumel = cellfun(@numel, header(:, 2)) == 1;
        % combine both to get values which are numeric scalars
        L = LNumeric & LNumel;
        
        indx = listdlg('ListString', header(L, 1), 'PromptString', ...
            'Select the header item to use for correction:', 'SelectionMode', ...
            'single', 'ListSize', [350 350]);
        if isempty(indx)
            set(handles.TB_Status, 'String', 'Process aborted.');
            return
        end
        NumHeader = header(L, :);
        SelHeader = NumHeader{indx, 1};
    end
    % find the corresponding value
    ind = ismember(header(:, 1), SelHeader);
    if sum(ind) == 0 || isempty(header{ind, 2})
        warning('GIDVis:LoadFileModule:DivideByHeaderInformation:HeaderNotAvailable', ...
            'Could not find requested header information (''%s'') in file ''%s''.', SelHeader, Elements(iF).DisplayName);
        continue;
    end
    % check for numeric data for selected header value
    if ~isnumeric(header{ind, 2})
        warning('GIDVis:LoadFileModule:DivideByHeaderInformation:HeaderNotNumeric', ...
            'The requested value of the header element ''%s'' in file ''%s'' is not numeric.', SelHeader, Elements(iF).DisplayName);
        continue;
    end
    % divide intensity data by value
    data = data./header{ind, 2};

    % find a name suggestion: add HEADCORR_ in front of file name
    DefaultPathName = Elements(iF).NameSuggestion('HEADCORR_');
    SaveDataToMat(DefaultPathName, data, ...
        [{'File Information'}, {sprintf('Data file created by dividing intensity by %s for file %s.', ...
        SelHeader, Elements(iF).DisplayName)}; header]);
end

[~, NameOfFirst] = fileparts(Elements(1).NameSuggestion('HEADCORR_'));

Elements = UpdateFilesListbox(handles, get(handles.TB_CurrentPath, 'String'));
handles.Elements = Elements;

% in case no header could be extracted from any file, SelHeader will be
% empty
if isempty(SelHeader)
    msgbox('No header information could be extracted from any of the selected files. No header correction possible.', 'modal')
end

% select the corrected file
idx = find(strcmp({Elements.DisplayName}, [NameOfFirst, '.mat']));
if handles.SelectResultFile && ~isempty(idx)
    set(handles.LB_Files, 'Value', idx);
else
    % restore the listbox's selected element
    set(handles.LB_Files, 'Value', SelInd);
end

% update status label
set(handles.TB_Status, 'String', 'Process completed.');
drawnow;

% update handles structure
guidata(src, handles);

% update the plot
RequestPlotUpdateFromFilePath(handles.MainAxes);


function ApplyFootprintCorrection_Callback(src, evt)
handles = guidata(src);

% get selected elements
Elements = GetSelectedFilePath(src);

if any(strcmp({Elements.Type}, 'dir'))
    msgbox('At least one directory is contained in the selection. Please select only files!', 'modal')
    return;
end

% get the extensions of the selected files
Exts = {Elements.Ext};
if any(strcmp(Exts, '.GIDDat'))
    msgbox('The footprint correction can only be applied to raw data, *.GIDDat files are not raw data.', 'modal');
    return;
end

SelInd = get(handles.LB_Files, 'Value');

% get beamline
BL = GetBeamline(src);

% get handles structure of main GIDVis window
GD = guidata(handles.MainAxes);

% get omega, chi, sample length and sample width
omega = ToolboxModule('GetOmega', GD.ToolboxModule);

% default values for sample width and length
SW = 20;
SL = 20;

% default values for beam height and beam width
BH = BL.BeamHeight;
BW = BL.BeamWidth;

prompt = {'omega (degree)', 'Sample Width (mm)', ...
    'Sample Length (mm)', 'Beam Height (mm)', 'Beam Width (mm)', ...
    'phi (degree) (phi_start:phi_step:phi_end)'};
defaultans = {num2str(omega), num2str(SW), num2str(SL), ...
    num2str(BH), num2str(BW), '0:2:358'};
num_lines = 1;
dlg_title = 'Input';
answer = inputdlg(prompt, dlg_title, num_lines, defaultans);
if isempty(answer); return; end

Values = cellfun(@str2double, answer(1:end-1));
if any(isnan(Values))
    msgbox('Please enter numeric values in all boxes.', 'modal');
    return;
end
try
    phi = eval(answer{end});
catch
    msgbox('Error calculating phi. Please use the format phi_start:phi_step:phi_end.', 'modal');
    return;
end
if numel(Elements) ~= numel(phi)
    msgbox(sprintf('The number of phi values differs from the number of files selected (%g vs. %g). Please select as many files as there are phi values', ...
        numel(phi), numel(Elements)), 'modal');
    return;
end

omega = Values(1);
SW = Values(2);
SL = Values(3);
BH = Values(4);
BW = Values(5);

FPC = IlluminatedArea(omega, SW, SL, phi, BH, BW);

for iF = 1:numel(Elements)
    % update status label
    set(handles.TB_Status, 'String', sprintf('Working on file %d of %d.', iF, numel(Elements)));
    drawnow;
    % read out data of file
    [data, Header] = ReadDataFromFile(Elements(iF));
    % apply correction factors
    data = data./FPC(iF);
    
    % find a name suggestion: add FPCORR_ in front of file name
    DefaultPathName = Elements(iF).NameSuggestion('FPCORR_');
    FileInformation = sprintf(['Data file created by performing footprint correction on %s.\n\n', ...
        'Parameters used:\n', ...
        'omega = %g degree\nSample width = %g mm\nSample length = %g mm\nphi = %g degree\nBeam height = %g mm\nBeam width = %g mm\n\n', ...
        'Timestamp of correction: %s'], ...
        Elements(iF).DisplayName, omega, SW, SL, phi(iF), BH, BW, datestr(now));
    SaveDataToMat(DefaultPathName, data, ...
        [{'File Information', FileInformation}; Header{1}]);
end

[~, NameOfFirst] = fileparts(Elements(1).NameSuggestion('FPCORR_'));

% Update the listbox containing the files
Elements = UpdateFilesListbox(handles, get(handles.TB_CurrentPath, 'String'));
handles.Elements = Elements;

% select the footprint corrected file
idx = find(strcmp({Elements.DisplayName}, [NameOfFirst, '.mat']));
if handles.SelectResultFile && ~isempty(idx)
    set(handles.LB_Files, 'Value', idx);
else
    % restore the listbox's selected element
    set(handles.LB_Files, 'Value', SelInd);
end

% update status label
set(handles.TB_Status, 'String', 'Process completed.');
drawnow;

% update handles structure
guidata(src, handles);

% update the plot
RequestPlotUpdateFromFilePath(handles.MainAxes);

function MathSelectedImages_Callback(src, evt, Type)
handles = guidata(src);

% get selected elements
Elements = GetSelectedFilePath(src);

if any(strcmp({Elements.Type}, 'dir'))
    msgbox('At least one directory is contained in the selection. Please select only files!', 'modal')
    return;
end

% get the extensions of the selected files
Exts = {Elements.Ext};
if any(strcmp(Exts, '.GIDDat'))
    msgbox('Mathematical operations can only be applied to raw data, *.GIDDat files are not raw data.', 'modal');
    return;
end

SelInd = get(handles.LB_Files, 'Value');

prompt = 'Please enter the value:';
defaultans = {'0'};
num_lines = 1;
dlg_title = 'Input';
answer = inputdlg(prompt, dlg_title, num_lines, defaultans);
if isempty(answer); return; end

Value = str2double(answer{1});
if isnan(Value)
    msgbox('Please enter a numeric value.', 'modal');
    return;
end

for iF = 1:numel(Elements)
    % update status label
    set(handles.TB_Status, 'String', sprintf('Working on file %d of %d.', iF, numel(Elements)));
    drawnow;
    % read out data of file
    [data, Header] = ReadDataFromFile(Elements(iF));
    % apply the math operation
    switch lower(Type)
        case 'add'
            data = data + Value;
        case 'subtract'
            data = data - Value;
        case 'multiply'
            data = data.*Value;
        case 'divide'
            data = data./Value;
    end
    
    % find a name suggestion: add MATHType_ in front of file name
    DefaultPathName = Elements(iF).NameSuggestion(['MATH', upper(Type(1:3)), '_']);
    FileInformation = sprintf(['Data file created by performing mathematical operation on %s.\n\n', ...
        'Parameters used:\n', ...
        'Type: %s\nValue = %g\n\n', ...
        'Timestamp of operation: %s'], ...
        Elements(iF).DisplayName, Type, Value, datestr(now));
    SaveDataToMat(DefaultPathName, data, ...
        [{'File Information', FileInformation}; Header{1}]);
end

[~, NameOfFirst] = fileparts(Elements(1).NameSuggestion(['MATH', upper(Type(1:3)), '_']));

% Update the listbox containing the files
Elements = UpdateFilesListbox(handles, get(handles.TB_CurrentPath, 'String'));
handles.Elements = Elements;

% select the result file
idx = find(strcmp({Elements.DisplayName}, [NameOfFirst, '.mat']));
if handles.SelectResultFile && ~isempty(idx)
    set(handles.LB_Files, 'Value', idx);
else
    % restore the listbox's selected element
    set(handles.LB_Files, 'Value', SelInd);
end

% update status label
set(handles.TB_Status, 'String', 'Process completed.');
drawnow;

% update handles structure
guidata(src, handles);

% update the plot
RequestPlotUpdateFromFilePath(handles.MainAxes);


function SumAverageSelectedImages_Callback(src, evt, Type)
if nargin < 3 || isempty(Type)
    Type = 'sum';
end
handles = guidata(src);

% get selected elements
Elements = GetSelectedFilePath(src);
if any(strcmp({Elements.Type}, 'dir'))
    msgbox('At least one directory is contained in the selection. Please select only files!', 'modal')
    return;
end

if numel(Elements) == 1
    msgbox('Select two or more files using Ctrl and/or Shift key.', 'modal');
    return;
end

% store the indices of the selected elements
SelInd = get(handles.LB_Files, 'Value');

% get the extensions of the selected files
Exts = {Elements.Ext};
if any(strcmp(Exts, '.GIDDat'))
    msgbox('Only raw data can be used, *.GIDDat files are not raw data.', 'modal');
    return;
end

% update status label
set(handles.TB_Status, 'String', sprintf('Reading file 1 of %d.', numel(Elements)));
drawnow;
% read out data of first file (to get array data of correct size)
data = ReadDataFromFile(Elements(1));
data(data < 0) = NaN;

% loop over selected files beginning with the second (first one was treated
% separately)
for iF = 2:numel(Elements)
    % update status label
    set(handles.TB_Status, 'String', sprintf('Reading file %d of %d.', iF, numel(Elements)));
    drawnow;
    % read out intensity
    datanew = ReadDataFromFile(Elements(iF));
    % replace intensities smaller 0 by NaN
    datanew(datanew < 0) = NaN;
    % add the intensities together
    data = data + datanew;
end

if strcmpi(Type, 'average')
    data = data./numel(Elements);
end

% update status label
set(handles.TB_Status, 'String', 'Calculation in progress.');
drawnow;

% get the handles structure of the main window
handlesMainWindow = guidata(handles.MainAxes);
% set the RawData of this handles structure to our previously calculated
% data
handlesMainWindow.RawData = data;
% update the handles structure of the main window
guidata(handles.MainAxes, handlesMainWindow);
% run function RequestPlotUpdateFromData. This function takes the data from
% handlesMainWindow.RawData and plots it
RequestPlotUpdateFromData(handles.MainAxes);

% save the data as a mat file
% update status label
set(handles.TB_Status, 'String', 'Saving');
drawnow;
% find a name suggestion: remove everything from the filename after the
% last underscore, add SUMMED_ or _AVG in front
if strcmpi(Type, 'sum')
    Prefix = 'SUMMED_';
    InfoString = 'File created by summing multiple images.';
elseif strcmpi(Type, 'average')
    Prefix = 'AVG_';
    InfoString = 'File created by averaging multiple images.';
end
FN = Elements(1).Name;
Ind = strfind(FN, '_');
if isempty(Ind) || (numel(Ind) == 1 && Ind == 1)
    DefaultPathName = fullfile(pwd, Prefix);
else
    DefaultPathName = fullfile(get(handles.TB_CurrentPath, 'String'), [Prefix, FN(1:Ind(end)-1)]);
end
[FileName, PathName, FilterIndex] = uiputfile({'*.mat', 'Mat File'}, 'Save result image', DefaultPathName);
if FilterIndex ~= 0
    SaveDataToMat(fullfile(PathName, FileName), data, InfoString);
    % if the file is saved to the currently selected path, update the
    % listbox and set the value of the listbox to this file
    CurrentPath = fileparts(Elements(1).Path);
    if strcmpi(PathName(1:end-1), CurrentPath)
        Elements = UpdateFilesListbox(handles, get(handles.TB_CurrentPath, 'String'));
        handles.Elements = Elements;
        if handles.SelectResultFile
            idx = find(strcmp({Elements.Path}, fullfile(PathName, FileName)));
            set(handles.LB_Files, 'Value', idx);
        else
            set(handles.LB_Files, 'Value', SelInd);
        end
    end
end

% update status label
set(handles.TB_Status, 'String', 'Process completed.');
drawnow;

guidata(src, handles);

function MainAxesDestroyed_Callback(src, evt, HandleToModuleWindow)
handles = guidata(HandleToModuleWindow);
% Is called when the axes in the main window is destroyed, e. g. when the
% main window gets closed.
handles.CloseReally = true;

% Update handles structure
guidata(HandleToModuleWindow, handles);

% close module window
close(HandleToModuleWindow);


% --- Outputs from this function are returned to the command line.
function varargout = LoadFileModule_OutputFcn(hObject, eventdata, handles) 
% restore the window position
if isfield(handles.Settings, 'WindowPosition')
    RestorePositionOrDefault(hObject, handles.Settings.WindowPosition);
    Fig_LoadFileModule_ResizeFcn(hObject, [], handles);
end

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in Btn_ChangeCurrentPath.
function Btn_ChangeCurrentPath_Callback(hObject, eventdata, handles)
% Gives the user the possibility to change the current path via a dialog.

% create a dialog to select a path from. Get the default path from the
% settings
SelectedPath = uigetdir(handles.Settings.CurrentPath);
if SelectedPath ~= 0 % user didn't cancel selection
    Elements = UpdateFilesListbox(handles, SelectedPath);
    handles.Elements = Elements;
    handles.Settings.CurrentPath = SelectedPath;
    
    % set the LastSelectedIndex to empty, to force an update of the plot in
    % every case, no matter what file will be selected next
    handles.LastSelectedIndex = [];

    % Update handles structure
    guidata(hObject, handles);
end


% --- Executes on selection change in LB_Files.
function LB_Files_Callback(hObject, eventdata, handles)
% This function checks if the newly selected item is different from before.
% If so, it will call the function to update the main window axes.

Value = get(hObject, 'Value');
% if multi select or listbox is empty, return
if numel(Value) > 1 || numel(get(hObject, 'String')) == 0
    % update status label
    set(handles.TB_Status, 'String', sprintf('%d elements selected.', numel(Value)));
    return;
else
    set(handles.TB_Status, 'String', '');
end

% time in seconds between two clicks to be recognized as a double click
% (0.5 seconds is the default on Windows)
DeltaTDoubleClick = 0.5;

if strcmp(handles.Elements(Value).Type, 'dir') % selected directory
    T2 = clock; % get the current time
    elT = etime(T2, handles.LastDirectoryClickTime); % calculate the time difference to last click
    if (~isempty(handles.LastSelectedIndex) && ...
            Value == handles.LastSelectedIndex && ...
            elT < DeltaTDoubleClick) || ... % user performed double click
            (~isempty(handles.LastSelectedIndex) && ...
            Value == handles.LastSelectedIndex && ...
            handles.ReturnKeyPressed) % user pressed return key
        if Value == 1 % user wants to go to parent directory
            OldPath = get(handles.TB_CurrentPath, 'String');
            NewPath = fileparts(get(handles.TB_CurrentPath, 'String'));
        else % any other directory selected
            NewPath = handles.Elements(Value).Path;
        end
        % update the listbox elements
        Elements = UpdateFilesListbox(handles, NewPath);
        % update the handles structure
        handles.Elements = Elements;
        
        if Value == 1
            % if we're going to the parent directory, try to restore the
            % selected element. It should be the directory we are leaving
            idx = find(ismember({handles.Elements.Path}, OldPath));
            handles.LastSelectedIndex = idx;
            set(handles.LB_Files, 'Value', idx);
        else
            % set the LastSelectedIndex to 1 to make sure a redraw is started
            % no matter what file is selected next (value 1 corresponds to 1st
            % entry in listbox, which usually is the parent directory)
            handles.LastSelectedIndex = 1;
        end
        
        handles.Settings.CurrentPath =  NewPath;
    else % single click on directory (no need for special behavior) except
        % updating handles structure
        handles.LastSelectedIndex = Value;
    end
    % store the time of the last click on a directory in the handles
    % structure. This is needed to differentiate a single from a double
    % click
    handles.LastDirectoryClickTime = T2;
else % user selected a file
    % check if selected file is different from previous one
    if isempty(handles.LastSelectedIndex) || handles.LastSelectedIndex ~= Value
        % update status label
        set(handles.TB_Status, 'String', 'Reading file.');
        drawnow;

        % if the AutoDetectBeamline checkbox is checked, we read out the
        % information from the file name (in case we're not dealing with a
        % *.GIDDat file)
        if get(handles.CB_AutoDetectBeamline, 'Value')
            % get the file name
            FileName = GetSelectedFilePath(hObject);
            % read out file name information
            Params = str2keyvaluepairs(FileName.DisplayName);

            % the info about the beamlines is stored in the handles structure
            MaxInd = ScoreStruct(Params, handles.BeamlineInfo);
            set(handles.BeamlineSelection, 'Value', MaxInd);
        end
        
        % Call the function in GIDVis.m to update the plot window
        Success = RequestPlotUpdateFromFilePath(handles.MainAxes);
        
        % if the plot update was not successful, set the index of the file
        % list box to the value it was before
        if ~Success
            if isfield(handles, 'LastSelectedIndex') && ...
                    ~isempty(handles.LastSelectedIndex) && ...
                    handles.LastSelectedIndex > 0 && ...
                    handles.LastSelectedIndex <= numel(get(hObject, 'String'))
                set(hObject, 'Value', handles.LastSelectedIndex);
                Value = handles.LastSelectedIndex;
            else
                set(hObject, 'Value', 1);
                Value = 1;
            end
        end
        
        % update status label
        set(handles.TB_Status, 'String', 'Loading completed.');
        drawnow;
    end
    % store the selected index in the handles structure, to avoid replotting
    % when it does not change
    handles.LastSelectedIndex = Value;
end

handles.ReturnKeyPressed = false;

% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function LB_Files_CreateFcn(hObject, eventdata, handles)
% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when user attempts to close Fig_LoadFileModule.
function Fig_LoadFileModule_CloseRequestFcn(hObject, eventdata, handles)
if handles.CloseReally % is true when main window gets closed
    % clean up everything: delete listeners, store settings

    % delete the listener to the AxesDestroyed event. Otherwise it will still
    % listen and try to execute the MainAxesDestroyed_Callback function, even
    % though the Module1 window is already closed.
    delete(handles.MainAxesDestroyedListener);

    % get the current path
    handles.Settings.CurrentPath = get(handles.TB_CurrentPath, 'String');
    
    % store the settings
    SettingsLoadFileModule = handles.Settings;
    SettingsLoadFileModule.WindowPosition = get(hObject, 'Position');
    SettingsLoadFileModule.SelectedBeamtime = get(handles.BeamtimeSelection,'Value');
    SettingsLoadFileModule.SelectedBeamline = get(handles.BeamlineSelection,'Value');
    SettingsLoadFileModule.AutoDetectBeamlineValue = get(handles.CB_AutoDetectBeamline, 'Value');
    SettingsLoadFileModule.SortByNameValue = get(handles.RB_SortByName, 'Value');
    SettingsLoadFileModule.SelectResultFileValue = handles.SelectResultFile;
    % in order to avoid error when no write access for file use try.
    try; save(handles.SettingsPath, 'SettingsLoadFileModule'); end
    
    delete(hObject);
else % user clicked the 'X' on the load file module window
    % instead of closing, make window invisible
    set(hObject, 'Visible', 'off');
end


function Path = GetSelectedFilePath(LoadFileModuleHandle)
handles = guidata(LoadFileModuleHandle);
contents = get(handles.LB_Files, 'String');
SelectedValue = get(handles.LB_Files, 'Value');
if isempty(SelectedValue) || isempty(contents)
    Path = [];
else
    Path = handles.Elements(get(handles.LB_Files, 'Value'));
end


% --- Executes when Fig_LoadFileModule is resized.
function Fig_LoadFileModule_ResizeFcn(hObject, eventdata, handles)
% resize gui elements: The current path panel and the beamline panel only
% change their width, their height stays constant. The Listbox changes it's
% height and width.
WinPos = get(hObject, 'Position');
if WinPos(4) < 430
    return;
end
UIP_CPPos = get(handles.UIP_CurrentPath, 'Position');
UIP_FilesPos = get(handles.UIP_Files, 'Position');
T_SortFilesPos = get(handles.T_SortFiles, 'Position');
UIB_SortFilesPos = get(handles.UIB_SortFiles, 'Position');
LB_FilesPos = get(handles.LB_Files, 'Position');
UIP_BeamtimePos = get(handles.UIP_CurrentBeamtime, 'Position');
UIP_BeamlinePos = get(handles.UIP_CurrentBeamline, 'Position');
TB_StatusPos = get(handles.TB_Status, 'Position');

set(handles.UIP_CurrentPath, 'Position', ...
    [UIP_CPPos(1), WinPos(4)-94, WinPos(3)-12, UIP_CPPos(4)]);
set(handles.UIP_Files, 'Position', ...
    [UIP_FilesPos(1), UIP_FilesPos(2), WinPos(3)-12, WinPos(4)-277]);
set(handles.T_SortFiles, 'Position', ...
    [T_SortFilesPos(1), UIP_FilesPos(4)-43, T_SortFilesPos(3), T_SortFilesPos(4)]);
set(handles.UIB_SortFiles, 'Position', ...
    [UIB_SortFilesPos(1) UIP_FilesPos(4)-47, UIB_SortFilesPos(3), UIB_SortFilesPos(4)]);
set(handles.LB_Files, 'Position', ...
    [LB_FilesPos(1) LB_FilesPos(2) UIP_FilesPos(3)-17 UIP_FilesPos(4)-59]);
set(handles.UIP_CurrentBeamtime, 'Position', ...
    [UIP_BeamtimePos(1), UIP_BeamtimePos(2), WinPos(3)-12, UIP_BeamtimePos(4)]);
set(handles.UIP_CurrentBeamline, 'Position', ...
    [UIP_BeamlinePos(1), UIP_BeamlinePos(2), WinPos(3)-12, UIP_BeamlinePos(4)]);
set(handles.TB_Status, 'Position', ...
    [TB_StatusPos(1), TB_StatusPos(2), WinPos(3)-12, TB_StatusPos(4)]);

function Elements = UpdateFilesListbox(handles, path)
% Searches supported files in path, adds directory and found files to the 
% listbox and changes the text in the current directory text field

% it is important that the output argument is used and then saved in the
% handles structure
if nargout ~= 1
    warning('GIDVis:LoadFileModule:UpdateFilesListbox', 'Use this function like this:\n\tElements = UpdateFilesListbox(handles, NewPath);\n\thandles.Elements = Elements;\n');
end

% get the supported file types
SupportedFileTypes = SupportedFileFormats();

% update status label
set(handles.TB_Status, 'String', 'Reading directory ...');
drawnow;

if get(handles.RB_SortByName, 'Value')
    % sort the files by name
    Sorting = 'name';
else
    % sort the files by date
    Sorting = 'date';
end

Elements = LFMElement(path, Sorting);

if numel(Elements) == 0
    msgbox('Directory does not exist or cannot be accessed.', 'Directory not found', 'warn', 'modal');
end

% set the value of the listbox to 1 (this makes the first element the 
% selected one). The value of the listbox has to be smaller than the number 
% of elements in it.
set(handles.LB_Files, 'Value', 1);
% show the files in the listbox
set(handles.LB_Files, 'String', {Elements.DisplayName});
% set the textbox containing the current directory to the path
set(handles.TB_CurrentPath, 'String', path);

% update handles
guidata(handles.LB_Files, handles);

% update status label
set(handles.TB_Status, 'String', 'Loading completed.');
drawnow;

function PopulateBeamtimePopupBox(LoadFileModuleUIElement)
% Search mat files in path and add found files to the listbox
% get the handles structure
handles = guidata(LoadFileModuleUIElement);
% find mat files in the beamtime path
MatFiles = dir(fullfile(handles.BeamtimePath, '*.mat'));
% if no beamtime file is found, create a default
if numel(MatFiles) == 0
    SetUp = ExperimentalSetUp('Default', 150, 0.172, 0.172, 1475, 1679, ...
                800, 800, 1.4, 0, 0, 0, 0);
    beamtime = Beamtime('Default', SetUp);
    if ~exist(handles.BeamtimePath, 'dir')
        mkdir(handles.BeamtimePath);
    end
    save(fullfile(handles.BeamtimePath, 'Default.mat'), 'beamtime');
    MatFiles(1).name = beamtime.Name;
end
% show the files in the popup menu
set(handles.BeamtimeSelection, 'String', {MatFiles.name});

function UpdateSelectedBeamtime(LoadFileModuleUIElement)
% called when the selected beamtime changes
% get the handles structure
handles = guidata(LoadFileModuleUIElement);
% get the beamtime file names from the popup menu
content = get(handles.BeamtimeSelection, 'String');
% check if the value of the beamtime box is out of range
if get(handles.BeamtimeSelection, 'Value') > numel(content)
    set(handles.BeamtimeSelection, 'Value', numel(content));
end
% load the selected beamtime
vars = load(fullfile(handles.BeamtimePath, content{get(handles.BeamtimeSelection, 'Value')}));
% save the beamtime in the handles structure
if isfield(vars, 'beamtime')
    handles.Beamtime = vars.beamtime;
end
% update the beamline selection box
% avoid problems when the value is larger than the number of exp. set ups
if get(handles.BeamlineSelection, 'Value') > numel(handles.Beamtime.SetUps)
    set(handles.BeamlineSelection, 'Value', 1);
end
% show the names of the experimental set ups in the beamline selection box
set(handles.BeamlineSelection, 'String', {handles.Beamtime.SetUps.name});

% extract information from the experimental set up names
BeamlineInfo = cell(numel(handles.Beamtime.SetUps), 1);
for iB = 1:numel(handles.Beamtime.SetUps)
    BeamlineInfo{iB} = str2keyvaluepairs(handles.Beamtime.SetUps(iB).name);
end

% store the information in the handles structure
handles.BeamlineInfo = BeamlineInfo;

% update the handles structure
guidata(LoadFileModuleUIElement, handles);

% function UpdateFilesBeamtimePopupbox(LoadFileModuleHandle)
% handles = guidata(LoadFileModuleHandle);
% % Searches mat files in path and adds found files to the listbox
% path = handles.BeamtimePath;
% % find mat files in the folder defined by path
% MatFiles = dir(fullfile(path, '*.mat'));
% % show the files in the popup menu
% set(handles.BeamtimeSelection, 'String', {MatFiles.name});
% % load the selected beamtime
% vars = load(fullfile(path, MatFiles(get(handles.BeamtimeSelection, 'Value')).name));
% % save the beamtime in the handles structure
% if isfield(vars, 'beamtime')
%     handles.Beamtime = vars.beamtime;
% end
% % update the handles structure
% guidata(LoadFileModuleHandle, handles);
% UpdateFilesBeamlinePopupbox(LoadFileModuleHandle)

% function UpdateFilesBeamlinePopupbox(LoadFileModuleHandle)
% handles = guidata(LoadFileModuleHandle);
% % contents = cellstr(get(handles.BeamtimeSelection,'String'));
% % load the selected beamtime file
% % load(fullfile(handles.BeamtimePath, contents{get(handles.BeamtimeSelection, 'Value')}));
% % avoid problems when the value is larger than the number of exp. set ups
% set(handles.BeamlineSelection, 'Value', 1);
% % show the names of the experimental set ups in the beamline selection box
% set(handles.BeamlineSelection, 'String', {handles.Beamtime.SetUps.name});
% 
% % extract information from the experimental set up names
% BeamlineInfo = cell(numel(handles.Beamtime.SetUps), 1);
% for iB = 1:numel(handles.Beamtime.SetUps)
%     BeamlineInfo{iB} = BeamlineParametersFromString(handles.Beamtime.SetUps(iB).name);
% end
% 
% % store the information in the handles structure
% handles.BeamlineInfo = BeamlineInfo;
% 
% guidata(LoadFileModuleHandle, handles);
% >>>>>>> remotes/origin/master

function beamline = GetBeamline(LoadFileModuleHandle)
handles = guidata(LoadFileModuleHandle);
beamline = handles.Beamtime.SetUps(get(handles.BeamlineSelection, 'Value'));

% --- Executes on selection change in BeamlineSelection.
function BeamlineSelection_Callback(hObject, eventdata, handles)
% hObject    handle to BeamlineSelection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns BeamlineSelection contents as cell array
%        contents{get(hObject,'Value')} returns selected item from BeamlineSelection

RequestPlotUpdateFromData(handles.MainAxes);


% --- Executes during object creation, after setting all properties.
function BeamlineSelection_CreateFcn(hObject, eventdata, handles)
% hObject    handle to BeamlineSelection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function TB_CurrentPath_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_CurrentPath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_CurrentPath_Callback(hObject, eventdata, handles)
% check for special directory
NewPath = get(handles.TB_CurrentPath, 'String');
if strcmpi(NewPath, '%root%') && isfield(handles, 'RootDirectory')
    set(handles.TB_CurrentPath, 'String', handles.RootDirectory);
elseif strcmpi(NewPath, '%demo%') && isfield(handles, 'DemoDirectory')
    set(handles.TB_CurrentPath, 'String', handles.DemoDirectory);
end

% update the files listbox with the new path
Elements = UpdateFilesListbox(handles, get(handles.TB_CurrentPath, 'String'));
handles.Elements = Elements;
handles.Settings.CurrentPath = get(handles.TB_CurrentPath, 'String');

% set the LastSelectedIndex to empty, to force an update of the plot in
% every case, no matter what file will be selected next
handles.LastSelectedIndex = [];
guidata(hObject, handles);


% --- Executes on key press with focus on LB_Files and none of its controls.
function LB_Files_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to LB_Files (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
if isempty(eventdata.Modifier) && strcmp(eventdata.Key, 'return')
    handles.ReturnKeyPressed = true;
    guidata(hObject, handles);
elseif isempty(eventdata.Modifier) && strcmp(eventdata.Key, 'backspace')
    OldPath = get(handles.TB_CurrentPath, 'String');
    NewPath = fileparts(OldPath);
    Elements = UpdateFilesListbox(handles, NewPath);
    handles.Elements = Elements;
    guidata(hObject, handles);
    % We're going to the parent directory, so try to restore the
    % selected element. It should be the directory we are leaving
    idx = find(ismember({handles.Elements.Path}, OldPath));
    handles.LastSelectedIndex = idx;
    set(handles.LB_Files, 'Value', idx);
elseif isempty(eventdata.Modifier) && strcmp(eventdata.Key, 'delete')
    % call the delete selected elements callback
    DeleteSelectedElements_Callback(hObject, []);
end

% --- Executes on selection change in BeamtimeSelection.
function BeamtimeSelection_Callback(hObject, eventdata, handles)
% hObject    handle to BeamtimeSelection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns BeamtimeSelection contents as cell array
%        contents{get(hObject,'Value')} returns selected item from BeamtimeSelection
UpdateSelectedBeamtime(hObject);
handles = guidata(hObject);

RequestPlotUpdateFromData(handles.MainAxes);

% --- Executes during object creation, after setting all properties.
function BeamtimeSelection_CreateFcn(hObject, eventdata, handles)
% hObject    handle to BeamtimeSelection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function GeneratePoleFigures_Callback(src, evt)
%executes on file selection - right click - generate polefigure
handles = guidata(src);

% get selected elements
Elements = GetSelectedFilePath(src);

if any(strcmp({Elements.Type}, 'dir'))
    msgbox('At least one directory is contained in the selection. Please select only files!', 'modal')
    return;
end

if numel(Elements) == 1
    msgbox('Select two or more files using Ctrl and/or Shift key.', 'modal');
    return;
end

Exts = {Elements.Ext};
if any(strcmp(Exts, '.GIDDat'))
    msgbox('Only raw data can be used, *.GIDDat files are not raw data.', 'modal');
    return;
end

%open PoleFigGUI
poleFigGen(handles.MainAxes, src, handles.LB_Files, handles.TB_CurrentPath, handles.TB_Status)


guidata(src, handles);


% --- Executes on button press in CB_AutoDetectBeamline.
function CB_AutoDetectBeamline_Callback(hObject, eventdata, handles)
% hObject    handle to CB_AutoDetectBeamline (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of CB_AutoDetectBeamline
if get(hObject, 'Value')
    set(handles.BeamlineSelection, 'Enable', 'off');
else
    set(handles.BeamlineSelection, 'Enable', 'on');
end


% --- Executes when selected object is changed in UIB_SortFiles.
function UIB_SortFiles_SelectionChangedFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in UIB_SortFiles 
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% call the TB_CurrentPath_Callback function, which will update the files
% list box
TB_CurrentPath_Callback(handles.TB_CurrentPath, [], handles);


% --------------------------------------------------------------------
function Cnt_DemoDir_Callback(hObject, eventdata, handles)
% hObject    handle to Cnt_DemoDir (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% set the string in the current path textbox to %demo%
set(handles.TB_CurrentPath, 'String', '%demo%');
% call the callback of the current path textbox. This will resolve the path
% and update the file list
TB_CurrentPath_Callback(handles.TB_CurrentPath, [], handles);

% --------------------------------------------------------------------
function Cnt_RootDir_Callback(hObject, eventdata, handles)
% hObject    handle to Cnt_RootDir (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% set the string in the current path textbox to %root%
set(handles.TB_CurrentPath, 'String', '%root%');
% call the callback of the current path textbox. This will resolve the path
% and update the file list
TB_CurrentPath_Callback(handles.TB_CurrentPath, [], handles);

% --------------------------------------------------------------------
function Cnt_CurrentPath_Callback(hObject, eventdata, handles)
% hObject    handle to Cnt_CurrentPath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
