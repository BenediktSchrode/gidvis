function varargout = MainAxesSettingsModule(varargin)
% MAINAXESSETTINGSMODULE MATLAB code for MainAxesSettingsModule.fig
%
% Module to adapt the axes labels of the main axes.
%
% Usage:
%     varargout = MainAxesSettingsModule(MainAxesHandle, TBxUI)
% 
% Input:
%     MainAxesHandle ... handle of the main GIDVis axes
%     TBxUI ... any UI element of the Toolbox Module
%
% Output:
%     varargout ... default GUIDE output
%
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @MainAxesSettingsModule_OpeningFcn, ...
                   'gui_OutputFcn',  @MainAxesSettingsModule_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before MainAxesSettingsModule is made visible.
function MainAxesSettingsModule_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to MainAxesSettingsModule (see VARARGIN)

% Choose default command line output for MainAxesSettingsModule
handles.output = hObject;

% store the input arguments in the handles structure for later reference
handles.MainAxes = varargin{1};
handles.ToolboxModule = varargin{2};

% Settings for this module are stored in the same path as the
% MainAxesSettingsModule.m file. 
SettingsPath = fullfile(fileparts(mfilename('fullpath')), 'Settings.mat');
% if settings file exists, erad out and apply
if exist(SettingsPath, 'file') == 2
    S = load(SettingsPath);
    set(handles.TB_qAxHorizontal, 'String', S.qSpaceLabelHorizontal);
    set(handles.TB_qAxVertical, 'String', S.qSpaceLabelVertical);
    set(handles.TB_PixelAxHorizontal, 'String', S.PixelSpaceLabelHorizontal);
    set(handles.TB_PixelAxVertical, 'String', S.PixelSpaceLabelVertical);
    if isfield(S, 'PolarSpaceLabelHorizontal')
        set(handles.TB_PolarAxHorizontal, 'String', S.PolarSpaceLabelHorizontal);
    end
    if isfield(S, 'PolarSpaceLabelVertical')
        set(handles.TB_PolarAxVertical, 'String', S.PolarSpaceLabelVertical);
    end
else
    % we have to create a settings file with some default values
    SettingsPath = fullfile(fileparts(mfilename('fullpath')), 'Settings.mat');
    qSpaceLabelHorizontal = ['q_{xy} (', char(197), '^{-1})'];
    qSpaceLabelVertical = ['q_z (', char(197), '^{-1})'];
    PixelSpaceLabelHorizontal = 'pixel (horizontal)';
    PixelSpaceLabelVertical = 'pixel (vertical)';
    PolarSpaceLabelHorizontal = ['q (', char(197), '^{-1})'];
    PolarSpaceLabelVertical = [char(936), ' (degree)'];
    WindowPosition = get(hObject, 'Position'); %#ok. Used for saving
    
    % in order to avoid error when no write access for file use try.
    try; save(SettingsPath, 'qSpaceLabelHorizontal', 'qSpaceLabelVertical', ...
        'PixelSpaceLabelHorizontal', 'PixelSpaceLabelVertical', ...
        'PolarSpaceLabelHorizontal', 'PolarSpaceLabelVertical', ...
        'WindowPosition'); end
    
    % set the string according to the values
    set(handles.TB_qAxHorizontal, 'String', qSpaceLabelHorizontal);
    set(handles.TB_qAxVertical, 'String', qSpaceLabelVertical);
    set(handles.TB_PixelAxHorizontal, 'String', PixelSpaceLabelHorizontal);
    set(handles.TB_PixelAxVertical, 'String', PixelSpaceLabelVertical);
    set(handles.TB_PolarAxHorizontal, 'String', PolarSpaceLabelHorizontal);
    set(handles.TB_PolarAxVertical, 'String', PolarSpaceLabelVertical);
end

% get the position property of the main axes
MainAxesPosition = get(handles.MainAxes, 'Position');
set(handles.TB_MainAxesX, 'String', num2str(MainAxesPosition(1)));
set(handles.TB_MainAxesY, 'String', num2str(MainAxesPosition(2)));
set(handles.TB_MainAxesWidth, 'String', num2str(MainAxesPosition(3)));
set(handles.TB_MainAxesHeight, 'String', num2str(MainAxesPosition(4)));

% add a listener to the axes in varargin{1}. When the axes gets destroyed 
% (e.g. because the figure containing the axes gets closed), the callback 
% will be executed (and closes this window).
% Store the listener in the handles structure to remove it when this window    
% gets closed by user, otherwise the callback is executed even when the
% LoadFileModule window does not exist anymore.
handles.MainAxesDestroyedListener = addlistener(handles.MainAxes, 'ObjectBeingDestroyed', @(src, evt) MainAxesDestroyed_Callback(src, evt, handles.Fig_MainAxesSettingsModule));


% Update handles structure
guidata(hObject, handles);

% UIWAIT makes MainAxesSettingsModule wait for user response (see UIRESUME)
% uiwait(handles.Fig_MainAxesSettingsModule);


function MainAxesDestroyed_Callback(src, evt, HandleToModuleWindow)
% close module window
close(HandleToModuleWindow);


% --- Outputs from this function are returned to the command line.
function varargout = MainAxesSettingsModule_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

set(hObject, 'Visible', 'on');



function TB_qAxHorizontal_Callback(hObject, eventdata, handles)
% hObject    handle to TB_qAxHorizontal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_qAxHorizontal as text
%        str2double(get(hObject,'String')) returns contents of TB_qAxHorizontal as a double


% --- Executes during object creation, after setting all properties.
function TB_qAxHorizontal_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_qAxHorizontal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_qAxVertical_Callback(hObject, eventdata, handles)
% hObject    handle to TB_qAxVertical (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_qAxVertical as text
%        str2double(get(hObject,'String')) returns contents of TB_qAxVertical as a double


% --- Executes during object creation, after setting all properties.
function TB_qAxVertical_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_qAxVertical (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Btn_HorizontalAngstroem.
function Btn_HorizontalAngstroem_Callback(hObject, eventdata, handles)
% Insert Angstroem symbol at the end of the text
set(handles.TB_qAxHorizontal, ...
	'String', [get(handles.TB_qAxHorizontal, 'String'), char(197)]);
		
% --- Executes on button press in Btn_VerticalAngstroem.
function Btn_VerticalAngstroem_Callback(hObject, eventdata, handles)
% Insert Angstroem symbol at the end of the text
set(handles.TB_qAxVertical, ...
	'String', [get(handles.TB_qAxVertical, 'String'), char(197)]);


function TB_PixelAxHorizontal_Callback(hObject, eventdata, handles)
% hObject    handle to TB_PixelAxHorizontal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_PixelAxHorizontal as text
%        str2double(get(hObject,'String')) returns contents of TB_PixelAxHorizontal as a double


% --- Executes during object creation, after setting all properties.
function TB_PixelAxHorizontal_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_PixelAxHorizontal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_PixelAxVertical_Callback(hObject, eventdata, handles)
% hObject    handle to TB_PixelAxVertical (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_PixelAxVertical as text
%        str2double(get(hObject,'String')) returns contents of TB_PixelAxVertical as a double


% --- Executes during object creation, after setting all properties.
function TB_PixelAxVertical_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_PixelAxVertical (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Btn_OK.
function Btn_OK_Callback(hObject, eventdata, handles)
% Here we have to update the axes labels and the settings file

% get the toolbox data
ToolboxData = guidata(handles.ToolboxModule);

% read out current space
contents = get(ToolboxData.PM_Space, 'String');
Space = contents{get(ToolboxData.PM_Space, 'Value')};

% set the axes labels according to the selected space and the strings
% entered in the corresponding textbox
if strcmpi(Space, 'q')
    set(get(handles.MainAxes, 'XLabel'), 'String', get(handles.TB_qAxHorizontal, 'String'))
    set(get(handles.MainAxes, 'YLabel'), 'String', get(handles.TB_qAxVertical, 'String'))
elseif strcmpi(Space, 'pixel')
    set(get(handles.MainAxes, 'XLabel'), 'String', get(handles.TB_PixelAxHorizontal, 'String'))
    set(get(handles.MainAxes, 'YLabel'), 'String', get(handles.TB_PixelAxVertical, 'String'))
elseif strcmpi(Space, 'polar')
    set(get(handles.MainAxes, 'XLabel'), 'String', get(handles.TB_PolarAxHorizontal, 'String'))
    set(get(handles.MainAxes, 'YLabel'), 'String', get(handles.TB_PolarAxVertical, 'String'))
end

% read out and save the values
SettingsPath = fullfile(fileparts(mfilename('fullpath')), 'Settings.mat');
qSpaceLabelHorizontal = get(handles.TB_qAxHorizontal, 'String'); %#ok Schrode
qSpaceLabelVertical = get(handles.TB_qAxVertical, 'String'); %#ok Schrode
PixelSpaceLabelHorizontal = get(handles.TB_PixelAxHorizontal, 'String'); %#ok Schrode
PixelSpaceLabelVertical = get(handles.TB_PixelAxVertical, 'String'); %#ok Schrode
PolarSpaceLabelHorizontal = get(handles.TB_PolarAxHorizontal, 'String'); %#ok Schrode
PolarSpaceLabelVertical = get(handles.TB_PolarAxVertical, 'String'); %#ok Schrode

% in order to avoid error when no write access for file use try.
try; save(SettingsPath, 'qSpaceLabelHorizontal', 'qSpaceLabelVertical', ...
    'PixelSpaceLabelHorizontal', 'PixelSpaceLabelVertical', ...
    'PolarSpaceLabelHorizontal', 'PolarSpaceLabelVertical', '-append'); end

% close this window (using close instead of delete to run the
% _CloseRequestFcn)
close(handles.Fig_MainAxesSettingsModule);


% --- Executes when user attempts to close Fig_MainAxesSettingsModule.
function Fig_MainAxesSettingsModule_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to Fig_MainAxesSettingsModule (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% delete the listener to the AxesDestroyed event. Otherwise it will still
% listen and try to execute the MainAxesDestroyed_Callback function, even
% though the Module1 window is already closed.
delete(handles.MainAxesDestroyedListener);

% store the window position in the settings file
SettingsPath = fullfile(fileparts(mfilename('fullpath')), 'Settings.mat');
WindowPosition = get(hObject, 'Position'); %#ok Schrode
% in order to avoid error when no write access for file use try.
try; save(SettingsPath, 'WindowPosition', '-append'); end

% Hint: delete(hObject) closes the figure
delete(hObject);

function [Horiz, Vert] = getPolarAxesLabels
% function which can be used from any other function to get the horizontal
% and vertical axes labels which need to be used when plotting polar space.
%
% Usage: [HorizontalAxesLabel, VerticalAxesLabel] = MainAxesSettingsModule('getQAxesLabels')
SettingsPath = fullfile(fileparts(mfilename('fullpath')), 'Settings.mat');
if exist(SettingsPath, 'file') == 2
    S = load(SettingsPath);
    if isfield(S, 'PolarSpaceLabelHorizontal')
        Horiz = S.PolarSpaceLabelHorizontal;
    else
        Horiz = ['q (', char(197), '^{-1})'];
    end
    if isfield(S, 'PolarSpaceLabelVertical')
        Vert = S.PolarSpaceLabelVertical;
    else
        Vert = '\Psi (degree)';
    end
else
    Horiz = ['q (', char(197), '^{-1})'];
    Vert = '\Psi (degree)';
end


function [Horiz, Vert] = getQAxesLabels
% function which can be used from any other function to get the horizontal
% and vertical axes labels which need to be used when plotting q space.
%
% Usage: [HorizontalAxesLabel, VerticalAxesLabel] = MainAxesSettingsModule('getQAxesLabels')
SettingsPath = fullfile(fileparts(mfilename('fullpath')), 'Settings.mat');

if exist(SettingsPath, 'file') == 2
    load(SettingsPath, 'qSpaceLabelHorizontal', 'qSpaceLabelVertical');
    Horiz = qSpaceLabelHorizontal;
    Vert = qSpaceLabelVertical;
else
    Horiz = ['q_{xy} (', char(197), '^{-1})'];
    Vert = ['q_z (', char(197), '^{-1})'];
end


function [Horiz, Vert] = getPixelAxesLabels
% function which can be used from any other function to get the horizontal
% and vertical axes labels which need to be used when plotting pixel space.
%
% Usage: [HorizontalAxesLabel, VerticalAxesLabel] = MainAxesSettingsModule('getQAxesLabels')
SettingsPath = fullfile(fileparts(mfilename('fullpath')), 'Settings.mat');

if exist(SettingsPath, 'file') == 2 
    load(SettingsPath, 'PixelSpaceLabelHorizontal', 'PixelSpaceLabelVertical');
    Horiz = PixelSpaceLabelHorizontal;
    Vert = PixelSpaceLabelVertical;
else
    Horiz = 'pixel (horizontal)';
    Vert = 'pixel (vertical)';
end


function TB_PolarAxHorizontal_Callback(hObject, eventdata, handles)
% hObject    handle to TB_PolarAxHorizontal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_PolarAxHorizontal as text
%        str2double(get(hObject,'String')) returns contents of TB_PolarAxHorizontal as a double


% --- Executes during object creation, after setting all properties.
function TB_PolarAxHorizontal_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_PolarAxHorizontal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_PolarAxVertical_Callback(hObject, eventdata, handles)
% hObject    handle to TB_PolarAxVertical (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_PolarAxVertical as text
%        str2double(get(hObject,'String')) returns contents of TB_PolarAxVertical as a double


% --- Executes during object creation, after setting all properties.
function TB_PolarAxVertical_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_PolarAxVertical (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function UpdateMainAxesSize(handles)
Ax = handles.MainAxes;
x = str2double(get(handles.TB_MainAxesX, 'String'));
y = str2double(get(handles.TB_MainAxesY, 'String'));
w = str2double(get(handles.TB_MainAxesWidth, 'String'));
h = str2double(get(handles.TB_MainAxesHeight, 'String'));
set(Ax, 'Position', [x, y, w, h]);


function TB_MainAxesX_Callback(hObject, eventdata, handles)
% hObject    handle to TB_MainAxesX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_MainAxesX as text
%        str2double(get(hObject,'String')) returns contents of TB_MainAxesX as a double
UpdateMainAxesSize(handles);

% --- Executes during object creation, after setting all properties.
function TB_MainAxesX_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_MainAxesX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_MainAxesY_Callback(hObject, eventdata, handles)
% hObject    handle to TB_MainAxesY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_MainAxesY as text
%        str2double(get(hObject,'String')) returns contents of TB_MainAxesY as a double
UpdateMainAxesSize(handles);


% --- Executes during object creation, after setting all properties.
function TB_MainAxesY_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_MainAxesY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_MainAxesWidth_Callback(hObject, eventdata, handles)
% hObject    handle to TB_MainAxesWidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_MainAxesWidth as text
%        str2double(get(hObject,'String')) returns contents of TB_MainAxesWidth as a double
UpdateMainAxesSize(handles);


% --- Executes during object creation, after setting all properties.
function TB_MainAxesWidth_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_MainAxesWidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_MainAxesHeight_Callback(hObject, eventdata, handles)
% hObject    handle to TB_MainAxesHeight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_MainAxesHeight as text
%        str2double(get(hObject,'String')) returns contents of TB_MainAxesHeight as a double
UpdateMainAxesSize(handles);


% --- Executes during object creation, after setting all properties.
function TB_MainAxesHeight_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_MainAxesHeight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Btn_MainAxesFont.
function Btn_MainAxesFont_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_MainAxesFont (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
S = uisetfont(handles.MainAxes);
