function MapVisibilityModule(FigTB, AxesHandle, SeparatorOnOff)
% This function adds a toggle button to the figure toolbar given by FigTB.
% This button switches the visibility of the map on/off.
%
% Usage:
%     MapVisibilityModule(FigTB, AxesHandle, SeparatorOnOff)
%
% Input:
%     FigTB ... handle of the toolbar where the button should be added to
%     AxesHandle ... handle of the main GIDVis axes
%     SeparatorOnOff ... string ('on' or 'off') defining whether a
%                        separator should be drawn left of the toggle tool
%                        or not
%
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Load the image for the toggle button
Img = imread('MapVisibilityIcon.png');

% add the toggle button to the toolbar
ToggleButton = uitoggletool(FigTB, ...
    'State', 'off', ...
    'OnCallback', @(src, evt) MapVisibilityOn(src, evt, AxesHandle), ...
    'OffCallback', @(src, evt) MapVisibilityOff(src, evt, AxesHandle), ...
    'Tag', 'MapVisibilityToggleButton', ...
    'CData', Img, ...
    'Separator', SeparatorOnOff, ...
    'TooltipString', 'Map Visibility');

function MapVisibilityOn(src, evt, AxesHandle)
% executes when the toggle button gets turned on
imgHandle = findobj(AxesHandle, 'Tag', 'MainImage');
if isempty(imgHandle) % occurs e.g. when no GID image is loaded yet
    set(src, 'State', 'off');
    msgbox('You cannot toggle the visibility of the currently displayed image. Select a GID image in the "Load File" window (File - Select Folder) first.', 'Cannot Toggle Map Visibility', 'modal');
    return;
end
set(imgHandle, 'Visible', 'off');


function MapVisibilityOff(src, evt, AxesHandle)
% executes when the toggle button gets turned off
imgHandle = findobj(AxesHandle, 'Tag', 'MainImage');
set(imgHandle, 'Visible', 'on');