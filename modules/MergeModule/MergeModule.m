function varargout = MergeModule(varargin)
% MERGEMODULE MATLAB code for MergeModule.fig
%
% Usage:
%     [DOut, SP] = MergeModule(FilePaths, MainWindowGUIElementHandle, BeamtimeFolderPath)
%     [DOut, SP] = MergeModule(qDataObject, MainWindowGUIElementHandle, BeamtimeFolderPath)
%
% Input:
%     FilePaths ... Cell of file paths
%     qDataObject ... a qData object
%     MainWindowGUIElementHandle ... handle of any UI element of the
%                                    main window
%     BeamtimeFolderPath ... directory where the beamtime files are stored
%
% Output:
%     DOut ... default GUIDE output
%     SP ... in case the merged file was saved, SP contains the save path.
%            Otherwise it is empty
%
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @MergeModule_OpeningFcn, ...
                   'gui_OutputFcn',  @MergeModule_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

function [data, Choices] = PerformAutoAssign(handles)
% function which tries to assign the correct experimental settings to the
% files loaded according to the file names

% get available beamtimes
Beamtimes = dir(fullfile(handles.BeamtimeFolder, '*.mat'));
% get the beamtime file names
BeamtimeChoices = {Beamtimes.name};
% set the string property of the beamtime selection menu to the file names
set(handles.PM_Beamtime, 'String', BeamtimeChoices);
% load the selected beamtime
load(fullfile(handles.BeamtimeFolder, BeamtimeChoices{get(handles.PM_Beamtime, 'Value')}))
% get the names of the experimental set ups in the beamtime
Choices = {beamtime.SetUps.name};

% extract information from the experimental set up names
BeamlineInfo = cell(numel(beamtime.SetUps), 1);
for iB = 1:numel(beamtime.SetUps)
    BeamlineInfo{iB} = str2keyvaluepairs(beamtime.SetUps(iB).name);
end

% get file names and extensions from the loaded files
Files = {handles.Elements.Name};
% cell containing the file names + extension (column 1) and the name of the
% experimental set up for this file (column 2)
data = cell(numel(Files), 2);
data(:, 1) = Files;
data(:, 2) = Choices(1); % default is the first experimental set up
for iF = 1:numel(Files)
    NameInfo = str2keyvaluepairs(Files{iF});
    MaxInd = ScoreStruct(NameInfo, BeamlineInfo);
    data(iF, 2) = Choices(MaxInd);
end

% --- Executes just before MergeModule is made visible.
function MergeModule_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to MergeModule (see VARARGIN)

% Choose default command line output for MergeModule
handles.output = hObject;

% guidata of the main GIDVis window
GD = guidata(varargin{2});
% find LoadFileModuleHandle
LFM = guidata(GD.LoadFileModule);

% store the main GIDVis window element in the handles structure
handles.MainWindowGUIElement = varargin{2};
% store a LoadFileModule window element in the handles structure
handles.LoadFileModuleFig = LFM.Fig_LoadFileModule;

if isa(varargin{1}, 'LFMElement')
    handles.Elements = varargin{1};
    % set the selected beamtime to the one selected in the LoadFileModule
    set(handles.PM_Beamtime, 'Value', get(LFM.BeamtimeSelection, 'Value'));
    handles.BeamtimeFolder = varargin{3};
    
    % try to auto assign the correct experimental settings to the files
    % using their file name
    [data, Choices] = PerformAutoAssign(handles);
    
    % find the toolbox handle
    TBx = guidata(GD.ToolboxModule);
    % set the values in omega, chi, and regridding resolution the same as
    % in the toolbox
    set(handles.TB_omega, 'String', ToolboxModule('GetOmega', GD.ToolboxModule));
    set(handles.TB_chi, 'String', get(TBx.TB_chi, 'String'));
    set(handles.TB_RegridResolution1, 'String', get(TBx.TB_RegridResolution1, 'String'));
    set(handles.TB_RegridResolution2, 'String', get(TBx.TB_RegridResolution2, 'String'));
elseif isa(varargin{1}, 'qData') % qData
end

% add a listener to the axes in varargin{1}. When the axes gets destroyed 
% (e.g. because the figure containing the axes gets closed), the callback 
% will be executed (and closes this window).
% Store the listener in the handles structure to remove it when this window    
% gets closed by user, otherwise the callback is executed even when the
% LoadFileModule window does not exist anymore.
GD = guidata(varargin{2});
handles.MainAxesDestroyedListener = addlistener(GD.MainAxes, 'ObjectBeingDestroyed', @(src, evt) MainAxesDestroyed_Callback(src, evt, handles.Fig_MergeModule));

% the settings for this module are stored in the same directory as the file
% MergeModule.m. Store its path in the handles structure to reuse it.
handles.SettingsPath = fullfile(fileparts(mfilename('fullpath')), 'Settings.mat');

% read out settings if available
if exist(handles.SettingsPath, 'file') == 2
    load(handles.SettingsPath);
    % store the settings in the handles structure
    handles.Settings = SettingsMergeModule;
    if ~isfield(handles.Settings, 'SavePath')
        handles.Settings.SavePath = get(LFM.TB_CurrentPath, 'String');
    end
else % use some default
    handles.Settings = [];
    handles.Settings.SavePath = get(LFM.TB_CurrentPath, 'String');
end

set(handles.UIT_Assignment, 'Data', data);
set(handles.UIT_Assignment, 'ColumnFormat', {'char', Choices});
set(handles.UIT_Assignment, 'ColumnEditable', [false true]);
set(handles.UIT_Assignment, 'ColumnName', {'File', 'Beamline'});

% Update handles structure
guidata(hObject, handles);

% restore the window position
if isfield(handles, 'Settings') && isfield(handles.Settings, 'WindowPosition')
    RestorePositionOrDefault(hObject, handles.Settings.WindowPosition);
end

% UIWAIT makes MergeModule wait for user response (see UIRESUME)
uiwait(handles.Fig_MergeModule);

function MainAxesDestroyed_Callback(src, evt, HandleToModuleWindow)
% Is called when the axes in the main window is destroyed, e. g. when the
% main window gets closed.
% close module window
close(HandleToModuleWindow);

% --- Outputs from this function are returned to the command line.
function varargout = MergeModule_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


if ishandle(hObject)
    set(hObject, 'Visible', 'on');
end

% Get default command line output from handles structure
varargout{1} = handles.output;
if isfield(handles, 'Settings') && isfield(handles.Settings, 'SavePath')
    varargout{2} = handles.Settings.SavePath;
else
    varargout{2} = '';
end

close(hObject);
delete(hObject);


function TB_omega_Callback(hObject, eventdata, handles)
% hObject    handle to TB_omega (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_omega as text
%        str2double(get(hObject,'String')) returns contents of TB_omega as a double


% --- Executes during object creation, after setting all properties.
function TB_omega_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_omega (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_chi_Callback(hObject, eventdata, handles)
% hObject    handle to TB_chi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_chi as text
%        str2double(get(hObject,'String')) returns contents of TB_chi as a double


% --- Executes during object creation, after setting all properties.
function TB_chi_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_chi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_RegridResolution1_Callback(hObject, eventdata, handles)
% hObject    handle to TB_RegridResolution1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_RegridResolution1 as text
%        str2double(get(hObject,'String')) returns contents of TB_RegridResolution1 as a double


% --- Executes during object creation, after setting all properties.
function TB_RegridResolution1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_RegridResolution1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_RegridResolution2_Callback(hObject, eventdata, handles)
% hObject    handle to TB_RegridResolution2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_RegridResolution2 as text
%        str2double(get(hObject,'String')) returns contents of TB_RegridResolution2 as a double


% --- Executes during object creation, after setting all properties.
function TB_RegridResolution2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_RegridResolution2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Btn_Preview.
function Btn_Preview_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_Preview (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(hObject, 'Enable', 'off');
drawnow;

qDataObj = Merge(handles);
handles.OldQDataObj = qDataObj;
guidata(hObject, handles);
omega = str2double(get(handles.TB_omega, 'String'));
chi = str2double(get(handles.TB_chi, 'String'));
RegridResolution = [str2double(get(handles.TB_RegridResolution1, 'String')), ...
    str2double(get(handles.TB_RegridResolution2, 'String'))];
% plot(qDataObj);
[qxyR, qzR, IntR] = RegriddedValues(qDataObj, omega, chi, RegridResolution);
f = figure;
ax = axes('Parent', f);
imagesc(ax, qxyR, qzR, IntR);
set(ax, 'YDir', 'normal');
caxis(ax, getColorBarLimits(IntR(:)));
set(hObject, 'Enable', 'on');

function qDataObj = Merge(handles)
data = get(handles.UIT_Assignment, 'Data');
Assignment = NaN(numel(handles.Elements), 1);
BeamtimeChoices = get(handles.PM_Beamtime, 'String');
load(fullfile(handles.BeamtimeFolder, BeamtimeChoices{get(handles.PM_Beamtime, 'Value')}));
for iD = 1:size(data, 1)
    Assignment(iD) = find(strcmp({beamtime.SetUps.name}, data{iD, 2}));
end
qDataObj = qData(handles.Elements, beamtime, ...
    Assignment);


% --- Executes on button press in Btn_MergeSave.
function Btn_MergeSave_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_MergeSave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(hObject, 'Enable', 'off');
drawnow;

qDataObj = Merge(handles);
FullPath = LoadFileModule('GetSelectedFilePath', handles.LoadFileModuleFig);
[Path, Name] = fileparts(FullPath(1).Path);
Ind = strfind(Name, '_');
if isempty(Ind) || (numel(Ind) == 1 && Ind == 1)
    DefaultPathName = fullfile(Path, [Name, '.GIDDat']);
else
    DefaultPathName = fullfile(Path, [Name(1:Ind(end)-1), '.GIDDat']);
end
Path = SaveqDataWithUI(qDataObj, DefaultPathName);
if ~isempty(Path)
    handles.Settings.SavePath = Path;
    guidata(hObject, handles);
    uiresume(get(hObject, 'Parent'));
end
set(hObject, 'Enable', 'on');


% --- Executes when user attempts to close Fig_MergeModule.
function Fig_MergeModule_CloseRequestFcn(hObject, eventdata, handles)
% Retrieve information for settings and store them in file
SettingsMergeModule = handles.Settings;
SettingsMergeModule.WindowPosition = get(hObject, 'Position');
% in order to avoid error when no write access for file use try.
try; save(handles.SettingsPath, 'SettingsMergeModule'); end

% delete the listener to the AxesDestroyed event. Otherwise it will still
% listen and try to execute the MainAxesDestroyed_Callback function, even
% though the Module1 window is already closed.
try % in older Matlab versions, this function is called multiple times during close,
    % to avoid an error when trying to delete the listener a second time,
    % place it in a try, see Issue 31. BS.
    delete(handles.MainAxesDestroyedListener);
end

uiresume();

% Hint: delete(hObject) closes the figure
% delete(hObject);


% --- Executes when Fig_MergeModule is resized.
function Fig_MergeModule_ResizeFcn(hObject, eventdata, handles)
% hObject    handle to Fig_MergeModule (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
OldUnits = get(handles.UIP_Assignment, 'Units');
set(handles.UIP_Assignment, 'Units', 'pixels');
Ext = get(handles.UIP_Assignment, 'Position');
set(handles.UIP_Assignment, 'Units', OldUnits);
set(handles.UIT_Assignment, 'ColumnWidth', repmat({ceil(Ext(3)/2-30)}, 1, 2));


% --- Executes on selection change in PM_Beamtime.
function PM_Beamtime_Callback(hObject, eventdata, handles)
% hObject    handle to PM_Beamtime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns PM_Beamtime contents as cell array
%        contents{get(hObject,'Value')} returns selected item from PM_Beamtime
[data, Choices] = PerformAutoAssign(handles);
set(handles.UIT_Assignment, 'Data', data);
set(handles.UIT_Assignment, 'ColumnFormat', {'char', Choices});

% --- Executes during object creation, after setting all properties.
function PM_Beamtime_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PM_Beamtime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
