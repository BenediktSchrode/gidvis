function vshifted = ShiftAndInsert(v, n, InsertVal)
% ShiftAndInsert shifts a vector by inserting a value at the beginning/end
% and removing values which "fall out".
%
% Usage:
%     ShiftAndInsert(v, n, InsertVal) shifts vector v by n positions by
%     inserting the value given by InsertVal. InsertVal will be defaulted
%     to 0 when not given.
%
% Input:
%     v ... vector to be shifted
%     n ... how many positions the vector should be shifted
%     InsertVal ... value inserted from the left/right to shift the
%                   vector's elements
%
% Output:
%     vshifted ... the shifted vector
%
% Example:
%     v = [1 0 0 0 0];
%     v1 = ShiftAndInsert(v, 1, NaN);
%     v2 = ShiftAndInsert(v, 2, NaN);
%     v3 = ShiftAndInsert(v, -2, NaN);
%
% v1 will be [NaN 1 0 0 0] because the old values get shifted one to the
% right and a NaN is inserted from the left.
% v2 will be [NaN NaN 1 0 0] because the old values get shifted two to the
% right and a NaN is inserted from the left.
% v3 will be [0 0 0 NaN NaN] because the old values get shifted two to the
% left (the one is "falling out"), and two NaNs get inserted from the
% right.
%
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% check for default value of InsertVal
if nargin == 2 || isempty(InsertVal)
    InsertVal = 0;
end

% store size of vector
sz = size(v);

% make vector size numel*1
v = v(:);

% for abs(n) > 1, we call ShiftAndInsert recursively
if n > 1
    v = ShiftAndInsert(v, n-1, InsertVal);
elseif n < -1
    v = ShiftAndInsert(v, n+1, InsertVal);
end

% the shift process
if sign(n) == 1
    vshifted = [InsertVal; v]; % insert value at beginning of vector
    vshifted = vshifted(1:end-1); % last value of new vector "falls out"
elseif sign(n) == -1
    vshifted = [v; InsertVal]; % insert value at end of vector
    vshifted = vshifted(2:end); % first value of new vector "falls out"
end

% reshape the vector to its original size
vshifted = reshape(vshifted, sz(1), sz(2));