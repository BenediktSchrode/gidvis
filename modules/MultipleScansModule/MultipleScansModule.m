function varargout = MultipleScansModule(varargin)
% Can be used to extract intensities, line profiles or integrated line
% scans from all or selected files in the current directory.
%
% Usage:
%     varargout = MultipleScansModule(AxesHandle, LFMUI, TBxUI)
%
% Input:
%     AxesHandle ... main axes handle of GIDVis
%     LFMUI ... any UI element of the Load File Module
%     TBxUI ... any UI element of the Toolbox Module
% 
% Output:
%     varargout ... default GUIDE output
%
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Last Modified by GUIDE v2.5 06-Dec-2017 15:13:51

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @MultipleScansModule_OpeningFcn, ...
                   'gui_OutputFcn',  @MultipleScansModule_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before MultipleScansModule is made visible.
function MultipleScansModule_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to MultipleScansModule (see VARARGIN)

% Choose default command line output for MultipleScansModule
handles.output = hObject;

handles.MainAxesHandle = varargin{1};
handles.LoadFileModuleHandle = varargin{2};
handles.ToolboxModuleHandle = varargin{3};

handles.OpenedFigures = [];

handles.CancelProcess = 0;

% the settings for this module are stored in the same directory as the file
% MultipleScansModule.m. Store its path in the handles structure to reuse it.
handles.SettingsPath = fullfile(fileparts(mfilename('fullpath')), 'Settings.mat');

% read out settings if available
if exist(handles.SettingsPath, 'file') == 2
    load(handles.SettingsPath);
    % store the settings in the handles structure
    handles.Settings = SettingsMultipleScansModule;
    set(handles.RB_AllFiles, 'Value', SettingsMultipleScansModule.RB_AllFiles_Value);
    set(handles.RB_OnlySelectedFiles, 'Value', SettingsMultipleScansModule.RB_OnlySelectedFiles_Value);
    set(handles.RB_Stepsize, 'Value', SettingsMultipleScansModule.RB_Stepsize_Value);
    set(handles.TB_Stepsize, 'String', SettingsMultipleScansModule.TB_Stepsize_String);
    set(handles.TB_StartIndex, 'String', SettingsMultipleScansModule.TB_StartIndex_String);
    set(handles.CB_ShowSingleFits, 'Value', SettingsMultipleScansModule.CB_ShowSingleFits_Value);
    % check if there is a license for the image toolbox
    [stat, ~] = license('checkout', 'Image_Toolbox');
    if isfield(SettingsMultipleScansModule, 'LatestSelectionType') && ...
            isfield(SettingsMultipleScansModule, 'LatestPosition') && ...
            ~isempty(SettingsMultipleScansModule.LatestPosition)
        if stat
            switch SettingsMultipleScansModule.LatestSelectionType
                case 'PointSelection'
                    handles.SelectionHandle = impoint(handles.MainAxesHandle, SettingsMultipleScansModule.LatestPosition(1), SettingsMultipleScansModule.LatestPosition(2));
                case 'LineSelection'
                    handles.SelectionHandle = imline(handles.MainAxesHandle, SettingsMultipleScansModule.LatestPosition);
                case {'AreaSelectionSummation', 'AreaSelectionFit'}
                    handles.SelectionHandle = imrect(handles.MainAxesHandle, SettingsMultipleScansModule.LatestPosition);
            end
        end
        handles.SelectionType = SettingsMultipleScansModule.LatestSelectionType;
    end
    if isfield(SettingsMultipleScansModule, 'TB_Start_String')
        set(handles.TB_Start, 'String', SettingsMultipleScansModule.TB_Start_String);
    end
    if isfield(SettingsMultipleScansModule, 'TB_End_String')
        set(handles.TB_End, 'String', SettingsMultipleScansModule.TB_End_String);
    end
    if isfield(SettingsMultipleScansModule, 'TB_NumSteps_String')
        set(handles.TB_NumSteps, 'String', SettingsMultipleScansModule.TB_NumSteps_String);
    end
    if isfield(SettingsMultipleScansModule, 'CB_UseAxesOptions_Value')
        set(handles.CB_UseAxesOptions, 'Value', SettingsMultipleScansModule.CB_UseAxesOptions_Value);
    end
    if isfield(SettingsMultipleScansModule, 'CB_PerformFit_Value')
        set(handles.CB_PerformFit, 'Value', SettingsMultipleScansModule.CB_PerformFit_Value);
    end
    
    if isfield(handles, 'SelectionHandle')
        addNewPositionCallback(handles.SelectionHandle, @(pos) UpdateCurrentSelection(handles));
    end
    
    UIP_Files_SelectionChangeFcn(handles.RB_Stepsize, [], handles);
end

% add a listener to the axes in varargin{1}. When the axes gets destroyed 
% (e.g. because the figure containing the axes gets closed), the callback 
% will be executed (and closes this window).
% Store the listener in the handles structure to remove it when this window    
% gets closed by user, otherwise the callback is executed even when the
% LoadFileModule window does not exist anymore.
handles.MainAxesDestroyedListener = addlistener(varargin{1}, 'ObjectBeingDestroyed', @(src, evt) MainAxesDestroyed_Callback(src, evt, handles.Fig_MultipleScansModule));

UpdateCurrentSelection(handles);

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes MultipleScansModule wait for user response (see UIRESUME)
% uiwait(handles.Fig_MultipleScansModule);

function MainAxesDestroyed_Callback(src, evt, HandleToMultipleScansModule)
% close module window
close(HandleToMultipleScansModule);

function UpdateCurrentSelection(handles)
if ~isfield(handles, 'SelectionHandle') || ~isfield(handles, 'SelectionType')
    set(handles.TB_CurrentSelection, 'String', {'Current Selection:', 'none'});
    return;
end

Pos = getPosition(handles.SelectionHandle);
SelType = handles.SelectionType;
switch SelType
    case 'PointSelection'
        SelTypeW = 'Point Selection';
        set(handles.CB_ShowSingleFits, 'Enable', 'off');
        set(handles.CB_PerformFit, 'Enable', 'off');
    case 'LineSelection'
        SelTypeW = 'Line Selection';
        set(handles.CB_ShowSingleFits, 'Enable', 'on');
        set(handles.CB_PerformFit, 'Enable', 'on');
    case 'AreaSelectionFit'
        SelTypeW = 'Area Selection (Fit)';
        set(handles.CB_ShowSingleFits, 'Enable', 'on');
        set(handles.CB_PerformFit, 'Enable', 'on');
    case 'AreaSelectionSummation'
        SelTypeW = 'Area Selection (Summation)';
        set(handles.CB_ShowSingleFits, 'Enable', 'off');
        set(handles.CB_PerformFit, 'Enable', 'off');
    otherwise
        SelTypeW = 'Unknown selection Type';
end
switch SelType
    case 'PointSelection'
        PosW = sprintf('[x, y] = [%g, %g]', Pos(1), Pos(2));
    case {'AreaSelectionFit', 'AreaSelectionSummation', 'LineSelection'}
        PosW = sprintf('[x1, y1, x2, y2] = [%g, %g, %g, %g]', ...
            Pos(1), Pos(2), Pos(1)+Pos(3), Pos(2)+Pos(4));
end
set(handles.TB_CurrentSelection, 'String', {'Current Selection:', ...
    SelTypeW, PosW});

% --- Outputs from this function are returned to the command line.
function varargout = MultipleScansModule_OutputFcn(hObject, eventdata, handles) 
% restore the window position
if isfield(handles, 'Settings') && isfield(handles.Settings, 'WindowPosition')
    RestorePositionOrDefault(hObject, handles.Settings.WindowPosition);
end
set(hObject, 'Visible', 'on');

% Get default command line output from handles structure
varargout{1} = handles.output;

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in Btn_PointSelection.
function Btn_PointSelection_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_PointSelection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if isfield(handles, 'SelectionHandle')
    try
        delete(handles.SelectionHandle);
    end
end

% check if there is a license for the image toolbox
[stat, msg] = license('checkout', 'Image_Toolbox');
if ~stat
    msgbox(msg, 'License Checkout Failed', 'modal');
    return;
end

handles.SelectionHandle = impoint(handles.MainAxesHandle);
handles.SelectionType = 'PointSelection';
UpdateCurrentSelection(handles);
addNewPositionCallback(handles.SelectionHandle, @(pos) UpdateCurrentSelection(handles));
guidata(hObject, handles);

% --- Executes on button press in Btn_LineSelection.
function Btn_LineSelection_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_LineSelection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if isfield(handles, 'SelectionHandle')
    try
        delete(handles.SelectionHandle);
    end
end

% check if there is a license for the image toolbox
[stat, msg] = license('checkout', 'Image_Toolbox');
if ~stat
    msgbox(msg, 'License Checkout Failed', 'modal');
    return;
end

handles.SelectionHandle = imline(handles.MainAxesHandle);
handles.SelectionType = 'LineSelection';
UpdateCurrentSelection(handles);
addNewPositionCallback(handles.SelectionHandle, @(pos) UpdateCurrentSelection(handles));
guidata(hObject, handles);

% --- Executes on button press in Btn_AreaSelection.
function Btn_AreaSelection_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_AreaSelection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if isfield(handles, 'SelectionHandle')
    try
        delete(handles.SelectionHandle);
    end
end

% check if there is a license for the image toolbox
[stat, msg] = license('checkout', 'Image_Toolbox');
if ~stat
    msgbox(msg, 'License Checkout Failed', 'modal');
    return;
end

handles.SelectionHandle = imrect(handles.MainAxesHandle);
handles.SelectionType = 'AreaSelectionFit';
UpdateCurrentSelection(handles);
addNewPositionCallback(handles.SelectionHandle, @(pos) UpdateCurrentSelection(handles));
guidata(hObject, handles);

% --- Executes on button press in Btn_Start.
function Btn_Start_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_Start (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% get position of the selection
try
    Pos = getPosition(handles.SelectionHandle);
catch
    msgbox('Create a selection using ''Point Selection'', ''Line Selection'', ''Area Selection (Fit)'' or ''Area Selection (Summation)'' before pressing ''Start''.', 'modal');
    return;
end

% get the files which have to be used
Files = FilesToEvaluate(handles);

% create the x vector
if get(handles.CB_UseAxesOptions, 'Value')
    xToPlot = linspace(str2double(get(handles.TB_Start, 'String')), ...
        str2double(get(handles.TB_End, 'String')), ...
        str2double(get(handles.TB_NumSteps, 'String')));
else
    xToPlot = 1:numel(Files);
end

% check for equal number of files and axes values
if numel(Files) ~= numel(xToPlot)
    msgbox(sprintf('The number of files does not match the number of axes values calculated (%d files, %d axes values). Cancelling.', numel(Files), numel(xToPlot)), 'modal')
    return;
end

% get the value if fit should be performed or not
if get(handles.CB_PerformFit, 'Value')
    PerformFit = true;
else
    PerformFit = false;
end

SetEnabled(handles, 'off');
set(handles.Btn_Cancel, 'Visible', 'on');
handles.CancelProcess = 0;
guidata(hObject, handles);

% update status label
set(handles.T_Status, 'String', 'Collecting data ...');
set(handles.T_Status, 'Visible', 'on');

% get the load file module handles structure
LFM = guidata(handles.LoadFileModuleHandle);

% get the currently selected directory
DirPath = get(LFM.TB_CurrentPath, 'String');

% get the toolbox module handles structure
TBM = guidata(handles.ToolboxModuleHandle);
% get the parameters from the ToolboxModule
contents = get(TBM.PM_Space, 'String');
Space = contents{get(TBM.PM_Space, 'Value')};
omega = ToolboxModule('GetOmega', handles.ToolboxModuleHandle);
chi = str2double(get(TBM.TB_chi, 'String'));
RegridResolution = [str2double(get(TBM.TB_RegridResolution1, 'String')), ...
    str2double(get(TBM.TB_RegridResolution2, 'String'))];

SingleFits = get(handles.CB_ShowSingleFits, 'Value');
OpenedFigs = [];
beamline = LoadFileModule('GetBeamline', handles.LoadFileModuleHandle);
if strcmpi(Space, 'q')
    % since the q values will not change (the beamline is fixed), we can
    % calculate them here and do not have to calculate them for every file
    % separately
    [qxy, qz] = CalculateQ(beamline, omega, chi);
elseif strcmpi(Space, 'polar')
    % since the q values will not change (the beamline is fixed), we can
    % calculate them here and do not have to calculate them for every file
    % separately
    [rho, theta] = CalculatePolar(beamline, omega, chi);
end

% Calculate the correction factors. They do not change depending on the
% file because the beamline is fixed. Use NaN for the data because we don't
% need it. After loading a file, the correction factors are used on this
% raw intensity data
[~, LC, PC, SAC, SDDC, DEC] = CorrectPixelData(handles.ToolboxModuleHandle, ...
    LoadFileModule('GetBeamline', handles.LoadFileModuleHandle), NaN(beamline.detlenz, beamline.detlenx));

% get the value of the correction factor check boxes
Lorentz = get(TBM.CB_LorentzCorrection, 'Value');
Polarization = get(TBM.CB_PolarizationCorrection, 'Value');
SolidAngle = get(TBM.CB_SolidAngleCorrection, 'Value');
PixelDistance = get(TBM.CB_PixelDistanceCorrection, 'Value');
DetectorEff = get(TBM.CB_DetectorEfficiencyCorrection, 'Value');
FlatField = get(TBM.CB_FlatFieldCorrection, 'Value');

% create a figure for all extracted lines
if strcmp(handles.SelectionType, 'LineSelection')
    FigAllLines = figure('Name', 'All Extracted Lines');
    AxAllLines = axes(FigAllLines);
    hold(AxAllLines, 'on');
    AddExportLineDataButton(FigAllLines);
    OpenedFigs(end+1) = FigAllLines;
elseif strcmp(handles.SelectionType, 'AreaSelectionFit')
    FigAllLines = figure('Name', 'All Extracted Lines');
    UpperAxAllLines = subplot(2, 1, 1, 'Parent', FigAllLines);
    LowerAxAllLines = subplot(2, 1, 2, 'Parent', FigAllLines);
    hold(UpperAxAllLines, 'on');
    hold(LowerAxAllLines, 'on');
    AddExportLineDataButton(FigAllLines);
    OpenedFigs(end+1) = FigAllLines;
end

% initialize variables
switch handles.SelectionType
    case {'PointSelection', 'AreaSelectionSummation'}
        Result = NaN(numel(Files), 1);
    case 'LineSelection'
        delta = NaN(numel(Files), 5);
        Result = delta;
        cfun = cell(numel(Files), 1);
    case 'AreaSelectionFit'
        delta = NaN(numel(Files), 10);
        Result = delta;
        cfun1 = cell(numel(Files), 1);
        cfun2 = cfun1;
    otherwise 
        fprintf('UNKNOWN\n')
end

% loop over all files
for iF = 1:numel(Files)
    % update the status label
    set(handles.T_Status, 'String', sprintf('Processing file %g of %g.', iF, numel(Files)));
    drawnow();
    
    % read the data of file iF
    data = ReadDataFromFile(Files(iF));
    
    if isa(data, 'qData')
        [qxy, qz] = getQ(data, omega, chi);
        data = CorrectQData(handles.ToolboxModuleHandle, data);
        % regrid only in the case where it is necessary (LineSelection and
        % AreaSelectionFit)
        if strcmp(handles.SelectionType, 'LineSelection') || ...
                strcmp(handles.SelectionType, 'AreaSelectionFit')
            [data, x, y] = regrid(qxy, qz, data(:), RegridResolution);
        else % all other selection types can work with not regridded data
            data = data(:);
            x = qxy;
            y = qz;
        end
    else
        % crop the data if any of the corrections have to be applied
        if any([numel(LC) numel(PC) numel(SAC) numel(SDDC) numel(DEC)] > 1) || FlatField
            data = data(1:beamline.detlenz, 1:beamline.detlenx);
        end

        % apply the corrections depending on the checkbox values
        if Lorentz; data = data./LC; end
        if Polarization; data = data./PC; end
        if SolidAngle; data = data.*SAC; end
        if PixelDistance; data = data.*SDDC; end
        if DetectorEff; data = data.*DEC; end
        % apply the flat field correction
        if FlatField
            if isempty(beamline.FFImageData)
                warning('GIDVis:MultipleScansModule', ['No flat field image for experimental set up ', beamline.name, '.']);
            else
                data = data.*beamline.FFImageData(1:beamline.detlenz, 1:beamline.detlenx);
            end
        end
    
        % replace dead pixels/missing areas by NaN
        data(data < 0) = NaN;

        if strcmpi(Space, 'q')
            % crop data according to beamline file
            data = data(1:beamline.detlenz, 1:beamline.detlenx);
            % regrid only in the case where it is necessary (LineSelection and
            % AreaSelectionFit)
            if strcmp(handles.SelectionType, 'LineSelection') || ...
                    strcmp(handles.SelectionType, 'AreaSelectionFit')
                [data, x, y] = regrid(qxy, qz, data(:), RegridResolution);
            else % all other selection types can work with not regridded data
                data = data(:);
                x = qxy;
                y = qz;
            end

        elseif strcmpi(Space, 'pixel')
            x = 1:size(data, 2);
            y = 1:size(data, 1);

            if ~strcmp(handles.SelectionType, 'LineSelection') && ...
                    ~strcmp(handles.SelectionType, 'AreaSelectionFit')
                [X, Y] = meshgrid(x, y);
                x = X(:);
                y = Y(:);
                data = data(:);
            end
        elseif strcmpi(Space, 'polar')
            % crop data according to beamline file
            data = data(1:beamline.detlenz, 1:beamline.detlenx);
            % regrid only in the case where it is necessary (LineSelection and
            % AreaSelectionFit)
            if strcmp(handles.SelectionType, 'LineSelection') || ...
                    strcmp(handles.SelectionType, 'AreaSelectionFit')
                [data, x, y] = regrid(rho, theta, data(:), RegridResolution);
            else % all other selection types can work with not regridded data
                data = data(:);
                x = rho;
                y = theta;
            end
        end
    end
    if numel(x) == 2
        x = linspace(x(1), x(2), size(data, 2));
    end
    if numel(y) == 2
        y = linspace(y(1), y(2), size(data, 1));
    end
    
    handles = guidata(hObject);
    if handles.CancelProcess
        break;
    end
    
    switch handles.SelectionType
        case 'PointSelection'
            % extract intensity of data point closest to selected point
            % calculate distances of every data point to selected point
            % (leave out sqrt, does not play role here but reduces number
            % of operations needed for calculation)
            D = (x-Pos(1)).^2+(y-Pos(2)).^2;
            [~, L] = min(D);
            if numel(L) == 1
                Result(iF) = data(L);
            end
        case 'LineSelection'
            % get the line profile data
            if PerformFit
                [xc, zc, cfunCurr, deltaCurr, ResString] = GetLineProfile(x, y, data(1:numel(y), 1:numel(x)), Pos(1, :), Pos(2, :));
                if isempty(deltaCurr); continue; end
                delta(iF, :) = deltaCurr;
                Result(iF, :) = coeffvalues(cfunCurr);
                cfun{iF} = cfunCurr;
            else
                [xc, zc] = GetLineProfile(x, y, data(1:numel(y), 1:numel(x)), Pos(1, :), Pos(2, :));
            end
            plot(xc, zc, 'Parent', AxAllLines, 'DisplayName', [num2str(iF), ': ', Files(iF).DisplayName]);
            if SingleFits
                f = figure('Name', Files(iF).DisplayName);
                OpenedFigs(end+1) = f;
                ax = axes;
                plot(xc, zc, 'b', 'DisplayName', 'data');
                if PerformFit && ~isempty(cfunCurr)
                    x_fit = linspace(min(xc), max(xc), 300);
                    y_fit = feval(cfunCurr, x_fit);
                    hold(ax, 'on');
                    plot(x_fit, y_fit, 'r', 'DisplayName', ResString);
                    hold(ax, 'off');
                    legend(ax, 'show', 'Location', 'best');
                end
                AddExportLineDataButton(f);
            end
        case 'AreaSelectionFit'
            if PerformFit
                [xc, yc, SumTopBottom, SumLeftRight, ...
                    cfunCurr1, cfunCurr2, ...
                    deltacoeffTB, deltacoeffLR, ...
                    ResStr1, ResStr2] = GetIntegratedLineScanProfiles(x, y, data(1:numel(y), 1:numel(x)), Pos);
                cfun1{iF} = cfunCurr1;
                cfun2{iF} = cfunCurr2;
            else
                [xc, yc, SumTopBottom, SumLeftRight] = GetIntegratedLineScanProfiles(x, y, data(1:numel(y), 1:numel(x)), Pos);
            end
            if isempty(xc); continue; end
            plot(xc, SumTopBottom, 'Parent', UpperAxAllLines, 'DisplayName', [num2str(iF), ': ', Files(iF).DisplayName]);
            plot(yc, SumLeftRight, 'Parent', LowerAxAllLines, 'DisplayName', [num2str(iF), ': ', Files(iF).DisplayName]);
            if PerformFit
                coeffTopBottom = coeffvalues(cfunCurr1);
                coeffLeftRight = coeffvalues(cfunCurr2);
                Result(iF, 1:numel(coeffTopBottom)) = coeffTopBottom;
                Result(iF, numel(coeffTopBottom)+1:numel(coeffTopBottom)+numel(coeffLeftRight)) = coeffLeftRight;
                delta(iF, 1:numel(coeffTopBottom)) = deltacoeffTB;
                delta(iF, numel(coeffTopBottom)+1:numel(coeffTopBottom)+numel(coeffLeftRight)) = deltacoeffLR;
            end
            if SingleFits
                f = figure('Name', Files(iF).DisplayName);
                OpenedFigs(end+1) = f;
                ax = subplot(2, 1, 1);
                plot(xc, SumTopBottom, 'b', 'DisplayName', 'data');
                if PerformFit
                    x_fit = linspace(min(xc), max(xc), 300);
                    y_fit = linspace(min(yc), max(yc), 300);
                    hold(ax, 'on');
                    plot(x_fit, feval(cfunCurr1, x_fit), 'r', 'DisplayName', ResStr1);
                    hold(ax, 'off');
                    xlabel(ax, 'x');
                    legend(ax, 'show', 'Location', 'best');
                end
                
                ax = subplot(2, 1, 2);
                plot(yc, SumLeftRight, 'b', 'DisplayName', 'data');
                if PerformFit
                    hold(ax, 'on');
                    plot(y_fit, feval(cfunCurr2, y_fit), 'r', 'DisplayName', ResStr2);
                    hold(ax, 'off');
                    xlabel(ax, 'y');
                    legend(ax, 'show', 'Location', 'best');
                end
                
                AddExportLineDataButton(f);
            end
        case 'AreaSelectionSummation'
            xmin = Pos(1);
            ymin = Pos(2);
            xmax = xmin + Pos(3);
            ymax = ymin + Pos(4);

            % find the selected data
            Lx = x >= xmin & x <= xmax;
            Ly = y >= ymin & y <= ymax;
            LTotal = Lx & Ly;
            Result(iF) = nansum(nansum(data(LTotal)));
    end
    
    handles = guidata(hObject);
    if handles.CancelProcess
        break;
    end
end

if ~isempty(Files)
    % update the status label
    set(handles.T_Status, 'String', 'Plotting results ...');
    drawnow();
    handles.OpenedFigures(end+1:end+numel(OpenedFigs)) = OpenedFigs;
    switch handles.SelectionType
        case 'PointSelection'
            f = figure;
            handles.OpenedFigures(end+1) = f;
            ax = axes('Parent', f);
            lh = plot(xToPlot(1:numel(Result)), Result, 'o-', 'DisplayName', 'Intensity');
            AddExportLineDataButton(f);
            AddZScanDragger(ax, [0 max(Result)]);
            AddSigmoidFit(ax, lh);
            AddGaussFit(ax, lh);
        case 'LineSelection'
            if PerformFit
                f = figure('Name', 'Result');
                handles.OpenedFigures(end+1) = f;
                names = '';
                for iR = 1:size(Result, 1)
                    if isempty(cfun{iR}); continue; end
                    names = coeffnames(cfun{iR});
                    break;
                end
                if ~isempty(names)
                    for iR = 1:size(Result, 2)
                        ax = subplot(size(Result, 2), 1, iR);
                        if iR == 1 && ~isempty(cfun{iR}); title(sprintf('Fit function: %s', formula(cfun{iR}))); end
                        errorbar(xToPlot(1:size(Result, 1)), Result(:, iR), delta(:, iR), 'o-', 'DisplayName', names{iR}, 'Parent', ax)
                        ylabel(names{iR});
                        if min(Result(:, iR)) ~= max(Result(:, iR))
                            ylim([min(Result(:, iR)) max(Result(:, iR))])
                        end
                    end
                    if ~get(handles.CB_UseAxesOptions, 'Value')
                        xlabel('File number')
                    end
                end
                AddExportLineDataButton(f);
            end
        case 'AreaSelectionFit'
            if PerformFit && ~all(isnan(Result(:)))
                fTB = figure('Name', 'Result Vertically');
                handles.OpenedFigures(end+1) = fTB;
                fLR = figure('Name', 'Result Horizontally');
                handles.OpenedFigures(end+1) = fLR;
                names1 = '';
                names2 = '';
                for iR = 1:size(Result, 1)
                    if ~isempty(names1) && ~isempty(names2); break; end
                    if ~isempty(cfun1{iR})
                        names1 = coeffnames(cfun1{iR});
                    end
                    if ~isempty(cfun2{iR})
                        names2 = coeffnames(cfun2{iR});
                    end
                end
                names = [names1 names2];
                for iR = 1:size(Result, 2)/2
                    ax = subplot(size(Result, 2)/2, 1, iR, 'Parent', fTB);
                    errorbar(xToPlot(1:size(Result, 1)), Result(:, iR), delta(:, iR), ...
                        'o-', 'Parent', ax, 'DisplayName', sprintf('Fit Parameter %s', names{iR}));
                    ylabel(ax, names{iR});
                    if min(Result(:, iR)) ~= max(Result(:, iR))
                        ylim(ax, [min(Result(:, iR)) max(Result(:, iR))]);
                    end
                    if iR == 1 && ~isempty(cfun1{iR}); title(ax, ['Fit function: ', formula(cfun1{iR})]); end
                    if iR == size(Result, 2)/2 && ~get(handles.CB_UseAxesOptions, 'Value'); xlabel(ax, 'File number'); end

                    ax = subplot(size(Result, 2)/2, 1, iR, 'Parent', fLR);
                    errorbar(xToPlot(1:size(Result, 1)), Result(:, iR+size(Result, 2)/2), delta(:, iR+size(Result, 2)/2), ...
                        'o-', 'Parent', ax, 'DisplayName', sprintf('Fit Parameter %s', names{iR}));
                    ylabel(ax, names{iR});
                    if min(Result(:, iR+size(Result, 2)/2)) ~= max(Result(:, iR+size(Result, 2)/2))
                        ylim(ax, [min(Result(:, iR+size(Result, 2)/2)) max(Result(:, iR+size(Result, 2)/2))]);
                    end
                    if iR == 1 && ~isempty(cfun2{iR}); title(ax, ['Fit function: ', formula(cfun2{iR})]); end
                    if iR == size(Result, 2)/2 && ~get(handles.CB_UseAxesOptions, 'Value'); xlabel(ax, 'File number'); end
                end
                AddExportLineDataButton(fTB);
                AddExportLineDataButton(fLR);
            end
        case 'AreaSelectionSummation'
            f = figure;
            handles.OpenedFigures(end+1) = f;
            ax = axes('Parent', f);
            lh = plot(xToPlot(1:numel(Result)), Result, 'o-', 'DisplayName', 'Summed Intensity');
            ylabel('Intensity')
            AddExportLineDataButton(f);
            AddZScanDragger(ax, [0 max(Result)]);
            AddSigmoidFit(ax, lh);
            AddGaussFit(ax, lh);
    end
else
    msgbox('No files selected.', 'modal');
end

set(handles.T_Status, 'Visible', 'off');
SetEnabled(handles, 'on');
set(handles.Btn_Cancel, 'Visible', 'off');
guidata(hObject, handles);

function AddGaussFit(ax, lh)
uicontrol(ancestor(ax, 'figure'), 'Style', 'checkbox', ...
    'Units', 'normalized', 'Position', [0.44 0.94 0.2 0.05], ...
    'FontSize', 10, 'String', 'Gauss Fit', ...
    'Callback', @(src, evt) AddGaussFitBtn_Callback(src, lh));

function AddGaussFitBtn_Callback(src, lh)
UD = get(src, 'UserData');
if isempty(UD)
    F = FittingWithCustomBorders(lh, 'r', @OneDimGaussianFit);
    F.UIFunctionChange = 'off';
    set(src, 'UserData', F);
else
    if get(src, 'Value')
        UD.Visible = 'on';
    else
        UD.Visible = 'off';
    end
end

function AddSigmoidFit(ax, lh)
uicontrol(ancestor(ax, 'figure'), 'Style', 'checkbox', ...
    'Units', 'normalized', 'Position', [0.22 0.94 0.2 0.05], ...
    'FontSize', 10, 'String', 'Sigmoid Fit', ...
    'Callback', @(src, evt) AddSigmoidFitBtn_Callback(src, lh));

function AddSigmoidFitBtn_Callback(src, lh)
UD = get(src, 'UserData');
if isempty(UD)
    F = FittingWithCustomBorders(lh, 'k', @SigmoidFit);
    F.UIFunctionChange = 'off';
    set(src, 'UserData', F);
else
    if get(src, 'Value')
        UD.Visible = 'on';
    else
        UD.Visible = 'off';
    end
end

function AddZScanDragger(ax, yLims)
h = ZScanDragger(ax, [], yLims);
SetVisibility(h, 'off')
uicontrol(ancestor(ax, 'figure'), 'Style', 'checkbox', ...
    'Units', 'normalized', 'Position', [0.66 0.94 0.32 0.05], ...
    'FontSize', 10, 'String', 'Extract Sample Height', ...
    'Callback', @(src, evt) ToggleVis(src, h));

function ToggleVis(CBHandle, DraggerHandle)
if get(CBHandle, 'Value')
    SetVisibility(DraggerHandle, 'on')
else
    SetVisibility(DraggerHandle, 'off')
end

function Files = FilesToEvaluate(handles)
% get the load file module handles structure
LFM = guidata(handles.LoadFileModuleHandle);

% read out file names
Elements = LFM.Elements;
L = strcmp({Elements.Type}, 'file');
Files = Elements(L);

if isempty(Files); return; end

if get(handles.RB_OnlySelectedFiles, 'Value')
    NumDirs = sum(strcmp({Elements.Type}, 'dir'));
    Indices = get(LFM.LB_Files, 'Value') - NumDirs;
    Files = Files(Indices);
elseif get(handles.RB_Stepsize, 'Value')
    Stepsize = str2double(get(handles.TB_Stepsize, 'String'));
    StartIndex = str2double(get(handles.TB_StartIndex, 'String'));
    if isnan(Stepsize) || isnan(StartIndex)
        msgbox('The step size and start index values have to be numeric.', 'modal');
        SetEnabled(handles, 'on');
        set(handles.T_Status, 'Visible', 'off');
        return;
    end
    if StartIndex < 1
        msgbox('Start index has to be larger than or equal to one.', 'modal');
        SetEnabled(handles, 'on');
        set(handles.T_Status, 'Visible', 'off');
        return;
    end
    Files = Files(StartIndex:Stepsize:end);
end

% --- Executes when user attempts to close Fig_MultipleScansModule.
function Fig_MultipleScansModule_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to Fig_MultipleScansModule (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% delete the selection in the field if necessary
if isfield(handles, 'SelectionHandle')
    LatestClass = class(handles.SelectionHandle);
    LatestPosition = getPosition(handles.SelectionHandle);
    try
        delete(handles.SelectionHandle);
    end
else
    LatestClass = '';
    LatestPosition = [];
end

% store the settings
SettingsMultipleScansModule.RB_AllFiles_Value = get(handles.RB_AllFiles, 'Value');
SettingsMultipleScansModule.RB_OnlySelectedFiles_Value = get(handles.RB_OnlySelectedFiles, 'Value');
SettingsMultipleScansModule.RB_Stepsize_Value = get(handles.RB_Stepsize, 'Value');
SettingsMultipleScansModule.TB_Stepsize_String = get(handles.TB_Stepsize, 'String');
SettingsMultipleScansModule.TB_StartIndex_String = get(handles.TB_StartIndex, 'String');
SettingsMultipleScansModule.CB_ShowSingleFits_Value = get(handles.CB_ShowSingleFits, 'Value');
SettingsMultipleScansModule.WindowPosition = get(hObject, 'Position');
if isfield(handles, 'SelectionType')
    SettingsMultipleScansModule.LatestSelectionType = handles.SelectionType;
end
SettingsMultipleScansModule.LatestPosition = LatestPosition;
SettingsMultipleScansModule.TB_Start_String = get(handles.TB_Start, 'String');
SettingsMultipleScansModule.TB_End_String = get(handles.TB_End, 'String');
SettingsMultipleScansModule.TB_NumSteps_String = get(handles.TB_NumSteps, 'String');
SettingsMultipleScansModule.CB_UseAxesOptions_Value = get(handles.CB_UseAxesOptions, 'Value');
SettingsMultipleScansModule.CB_PerformFit_Value = get(handles.CB_PerformFit, 'Value');
% in order to avoid error when no write access for file use try.
try; save(handles.SettingsPath, 'SettingsMultipleScansModule'); end

% delete the axes destroyed listener
delete(handles.MainAxesDestroyedListener);

% Hint: delete(hObject) closes the figure
delete(hObject);


function SetEnabled(handles, State)
Ctrls = {'Btn_PointSelection', 'Btn_LineSelection', ...
    'Btn_AreaSelection', 'RB_AllFiles', 'RB_OnlySelectedFiles', ...
    'Btn_Start', 'Btn_CloseResultFigures', 'CB_ShowSingleFits', ...
    'TB_StartIndex', 'Btn_NumberName', 'RB_Stepsize', ...
    'Btn_AreaSelectionSummation', 'TB_Stepsize', ...
    'CB_UseAxesOptions', 'TB_Start', 'TB_End', 'TB_NumSteps', ...
    'CB_PerformFit'};

for iC = 1:numel(Ctrls)
   eval(['set(handles.', Ctrls{iC}, ', ''Enable'', ''', State, ''');']);
end

if strcmp(State, 'on')
    UpdateCurrentSelection(handles);
end

drawnow();

% --- Executes on button press in Btn_CloseResultFigures.
function Btn_CloseResultFigures_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_CloseResultFigures (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(handles.OpenedFigures(ishandle(handles.OpenedFigures)));
handles.OpenedFigures = [];

% --- Executes during object creation, after setting all properties.
function TB_Stepsize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_Stepsize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when selected object is changed in UIP_Files.
function UIP_Files_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in UIP_Files 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)
if get(handles.RB_Stepsize, 'Value')
    set(handles.TB_Stepsize, 'Enable', 'on');
    set(handles.TB_StartIndex, 'Enable', 'on');
else
    set(handles.TB_Stepsize, 'Enable', 'off');
    set(handles.TB_StartIndex, 'Enable', 'off');
end


% --- Executes during object creation, after setting all properties.
function TB_StartIndex_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_StartIndex (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in Btn_NumberName.
function Btn_NumberName_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_NumberName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% get the files which have to be used
Files = FilesToEvaluate(handles);

% create the vector according to the axes options settings
if get(handles.CB_UseAxesOptions, 'Value')
    AxesValues = linspace(str2double(get(handles.TB_Start, 'String')), ...
        str2double(get(handles.TB_End, 'String')), ...
        str2double(get(handles.TB_NumSteps, 'String')));
else
    AxesValues = 1:numel(Files);
end
AxesValues = AxesValues(:);

% check for as many values in AxesValues as files
if numel(Files) ~= numel(AxesValues)
    msgbox(sprintf('The number of files does not match the number of axes values calculated (%d files, %d axes values). Cancelling.', numel(Files), numel(AxesValues)), 'modal')
    return;
end

% apply the same sorting to the file names and the extracted vector
M = [{Files.DisplayName}' num2cell(AxesValues)];

f = figure('Visible', 'off', 'Toolbar', 'none', 'MenuBar', 'none', ...
    'WindowStyle', 'modal', 'Name', 'File List', 'NumberTitle', 'off');
Tab = uitable(f);
set(Tab, 'Data', M);
set(Tab, 'Units', 'normalized');
set(Tab, 'Position', [0 0 1 1]);
set(Tab, 'FontSize', 10);
ColNames = {'Sorted File List', 'Axes Value'};
set(Tab, 'ColumnName', ColNames);
set(Tab, 'ColumnWidth', {200 100})
set(f, 'Visible', 'on');


% --- Executes during object creation, after setting all properties.
function Fig_MultipleScansModule_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Fig_MultipleScansModule (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in Btn_AreaSelectionSummation.
function Btn_AreaSelectionSummation_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_AreaSelectionSummation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if isfield(handles, 'SelectionHandle')
    try
        delete(handles.SelectionHandle);
    end
end

% check if there is a license for the image toolbox
[stat, msg] = license('checkout', 'Image_Toolbox');
if ~stat
    msgbox(msg, 'License Checkout Failed', 'modal');
    return;
end

handles.SelectionHandle = imrect(handles.MainAxesHandle);
handles.SelectionType = 'AreaSelectionSummation';
UpdateCurrentSelection(handles);
addNewPositionCallback(handles.SelectionHandle, @(pos) UpdateCurrentSelection(handles));
guidata(hObject, handles);


% --- Executes on button press in Btn_Cancel.
function Btn_Cancel_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_Cancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% update the status label
set(handles.T_Status, 'String', 'Cancelling ...');
drawnow();
handles.CancelProcess = 1;
guidata(hObject, handles);



function TB_RegEx_Callback(hObject, eventdata, handles)
% hObject    handle to TB_RegEx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_RegEx as text
%        str2double(get(hObject,'String')) returns contents of TB_RegEx as a double


% --- Executes during object creation, after setting all properties.
function TB_RegEx_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_RegEx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_Start_Callback(hObject, eventdata, handles)
% hObject    handle to TB_Start (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_Start as text
%        str2double(get(hObject,'String')) returns contents of TB_Start as a double


% --- Executes during object creation, after setting all properties.
function TB_Start_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_Start (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_End_Callback(hObject, eventdata, handles)
% hObject    handle to TB_End (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_End as text
%        str2double(get(hObject,'String')) returns contents of TB_End as a double


% --- Executes during object creation, after setting all properties.
function TB_End_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_End (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_NumSteps_Callback(hObject, eventdata, handles)
% hObject    handle to TB_NumSteps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_NumSteps as text
%        str2double(get(hObject,'String')) returns contents of TB_NumSteps as a double


% --- Executes during object creation, after setting all properties.
function TB_NumSteps_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_NumSteps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in CB_UseAxesOptions.
function CB_UseAxesOptions_Callback(hObject, eventdata, handles)
% hObject    handle to CB_UseAxesOptions (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of CB_UseAxesOptions
if get(hObject, 'Value')
    set(handles.TB_Start, 'Enable', 'on');
    set(handles.TB_End, 'Enable', 'on');
    set(handles.TB_NumSteps, 'Enable', 'on');
else
    set(handles.TB_Start, 'Enable', 'off');
    set(handles.TB_End, 'Enable', 'off');
    set(handles.TB_NumSteps, 'Enable', 'off');
end


% --- Executes on button press in CB_PerformFit.
function CB_PerformFit_Callback(hObject, eventdata, handles)
% hObject    handle to CB_PerformFit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of CB_PerformFit
