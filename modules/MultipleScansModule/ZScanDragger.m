classdef ZScanDragger < handle
    % ZScanDragger adds draggable points to a plot, well suited for
    % determination of the correct height from a z scan.
    %
    % This file is part of GIDVis.
    %
    % see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2018 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    properties (Access = protected)
        HGGroup % hggroup where elements are plotted to
        LineHandle % handle for the line
        MarkerHandle % handle for the draggable points
        TextHandle % handle for the text box showing the x and y value
        AxesHandle % handle of the axes the ZScanDragger was added to
    end
    
    properties (Dependent = true)
        FigureHandle % parent figure of the ZScanDragger
    end
    
    methods
        function obj = ZScanDragger(AxesHandle, xI, yI)
            % Creates the ZScanDragger object.
            %
            % Usage:
            % ZScanDragger(AxesHandle, xI, yI);
            % where AxesHandle is the axes to plot, xI is the initial
            % center point in x direction, and yI is a two element vector
            % with the initial upper and lower bounds.
            %
            % If AxesHandle is not given, it is defaulted to gca.
            % If xI is not given, it is defaulted to mean(xLim) of the
            % axes.
            % If yI is not given, it is defaulted to yLim of the axes.
            
            % check input argument 1 (axes handle)
            if nargin < 1 || isempty(AxesHandle)
                AxesHandle = gca;
            end
            % store it
            obj.AxesHandle = AxesHandle;
            % turn on hold for this axes
            hold(obj.AxesHandle, 'on');
            % create the hggroup
            obj.HGGroup = hggroup('Parent', obj.AxesHandle);
            % read out x and y limit of the axes
            xL = xlim(obj.AxesHandle);
            yL = ylim(obj.AxesHandle);
            % check input arguments 2 (xI) and 3 (yI)
            if nargin < 2 || isempty(xI)
                xI = mean(xL);
            end
            if nargin < 3 || isempty(yI)
                yI = yL;
            end
            % create the line (the NaNs break the line in multiple parts)
            obj.LineHandle = plot([xL(1), xI, xI, xL(2), NaN, xL(1), xL(2)], ...
                [yI(2), yI(2), yI(1), yI(1), NaN, mean(yI), mean(yI)], 'k--', ...
                'XLimInclude', 'off', 'YLimInclude', 'off', ...
                'Parent', obj.HGGroup, 'HandleVisibility', 'off');
            % create the text box showing the x/y value
            obj.TextHandle = text(xI, mean(yI), ...
                sprintf(' x: %g\n y: %g', xI, mean(yI)), 'Clipping', 'on', ...
                'Parent', obj.HGGroup, 'HandleVisibility', 'off');
            % create the draggable marker points
            obj.MarkerHandle = plot([xI, xI, xI], [yI(1), mean(yI), yI(2)], ...
                'ok', 'MarkerFaceColor', 'r', 'XLimInclude', 'off', ...
                'YLimInclude', 'off', 'Clipping', 'off', ...
                'Parent', obj.HGGroup, 'HandleVisibility', 'off');
            % set hold to off
            hold(obj.AxesHandle, 'off');
            
            % set the ButtonDownFcn of the marker points --> this enables
            % dragging
            set(obj.MarkerHandle, 'ButtonDownFcn', @(src, evt) obj.MarkerButtonDownFcn);
        end
        
        % get function for the figure handle
        function out = get.FigureHandle(obj)
            % the figure is the figure ancestor of the axes stored in obj
            out = ancestor(obj.AxesHandle, 'figure');
        end
        
        function SetVisibility(obj, Val)
            if strcmp(Val, 'on')
                set(findobj(obj.HGGroup), 'Visible', 'on')
            else
                set(findobj(obj.HGGroup), 'Visible', 'off')
            end
        end
    end
    
    methods (Access = private)
        % function executed when user clicks on marker
        function MarkerButtonDownFcn(obj)
            % only left mouse button clicks count
            if ~strcmp(get(obj.FigureHandle, 'SelectionType'), 'normal'); return; end
            % find the clicked marker (it's the one closest to the current
            % point, i.e. the mouse pointer location)
            ax = get(obj.FigureHandle, 'CurrentAxes');
            CP = get(ax, 'CurrentPoint');
            x = get(obj.MarkerHandle, 'XData');
            y = get(obj.MarkerHandle, 'YData');
            Distance = sqrt((x-CP(1, 1)).^2 + (y-CP(1, 2)).^2);
            [~, Number] = min(Distance);
            % set the WindowButtonMotionFcn --> user drags marker point
            set(obj.FigureHandle, 'WindowButtonMotionFcn', @(src, evt) obj.MarkerDragFunction(Number));
            % set the WindowButtonUpFcn --> user releases the marker point
            set(obj.FigureHandle, 'WindowButtonUpFcn', @(src, evt) obj.WindowButtonUpFcn);
        end
        
        % executed when the user releases the marker point
        function WindowButtonUpFcn(obj)
            % delete the WindowButtonMotionFcn
            set(obj.FigureHandle, 'WindowButtonMotionFcn', '');
        end
        
        % executed when the user drags the marker point
        function MarkerDragFunction(obj, Number) % number gives which point is dragged
            % get the xlimit of the axes
            xL = xlim(obj.AxesHandle);
            % get the pointer location
            CP = get(obj.AxesHandle, 'CurrentPoint');
            % recalculate the xI value depending on the marker which is
            % dragged
            if Number == 2
                xI = CP(1, 1);
            else
                xI = get(obj.MarkerHandle, 'XData');
                xI = xI(1);
            end
            % recalculate the yI values depending on the marker which is
            % dragged
            yI = get(obj.MarkerHandle, 'YData');
            if Number ~= 2
                yI(Number) = CP(1, 2);
            end
            yI = [yI(1) yI(3)];
            % set the updated data
            set(obj.LineHandle, 'XData', [xL(1), xI, xI, xL(2), NaN, xL(1), xL(2)], ...
                'YData', [yI(2), yI(2), yI(1), yI(1), NaN, mean(yI), mean(yI)]);
            set(obj.TextHandle, 'Position', [xI, mean(yI)], ...
                'String', sprintf(' x: %g\n y: %g', xI, mean(yI)));
            set(obj.MarkerHandle, 'XData', [xI, xI, xI], 'YData', [yI(1), mean(yI), yI(2)]);
        end
    end
end