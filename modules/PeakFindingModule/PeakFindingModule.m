function varargout = PeakFindingModule(varargin)
% This file, together with PeakFindingModule.fig, is the PeakFindingModule
% of GIDVis.
%
% Usage:
%     varargout = PeakFindingModule(MainAxesHandle);
%
% Input:
%     MainAxesHandle ... handle of the main GIDVis axes
%
% Output:
%     varargout ... default GUIDE output
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @PeakFindingModule_OpeningFcn, ...
                   'gui_OutputFcn',  @PeakFindingModule_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before PeakFindingModule is made visible.
function PeakFindingModule_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to PeakFindingModule (see VARARGIN)

set(handles.UIT_PeakDisplay, 'Data', []);

% initialize an array for the peak list
handles.PeakList = PeakFind.empty();

% store the main axes
handles.MainAxes = varargin{1};

% the settings for this module are stored in the same directory as the file
% PeakFindingModule.m. Store its path in the handles structure to reuse it.
handles.SettingsPath = fullfile(fileparts(mfilename('fullpath')), 'Settings.mat');

% read out settings if available
if exist(handles.SettingsPath, 'file') == 2
    load(handles.SettingsPath);
    % store the settings in the handles structure
    handles.Settings = SettingsPeakFindingModule;
    set(handles.TB_BoxWidth, 'String', SettingsPeakFindingModule.BoxWidth);
    set(handles.TB_BoxHeight, 'String', SettingsPeakFindingModule.BoxHeight);
end

% add a listener to the axes in varargin{1}. When the axes gets destroyed 
% (e.g. because the figure containing the axes gets closed), the callback 
% will be executed (and closes this window).
% Store the listener in the handles structure to remove it when this window    
% gets closed by user, otherwise the callback is executed even when the
% this window does not exist anymore.
handles.MainAxesDestroyedListener = addlistener(handles.MainAxes, 'ObjectBeingDestroyed', @(src, evt) MainAxesDestroyed_Callback(src, evt, handles.Fig_PeakFindingModule));

% Choose default command line output for PeakFindingModule
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes PeakFindingModule wait for user response (see UIRESUME)
% uiwait(handles.Fig_PeakFindingModule);

function MainAxesDestroyed_Callback(src, evt, HandleToModuleWindow)
% close module window
close(HandleToModuleWindow);

% --- Outputs from this function are returned to the command line.
function varargout = PeakFindingModule_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% restore the window position
if isfield(handles, 'Settings') && isfield(handles.Settings, 'WindowPosition')
    RestorePositionOrDefault(hObject, handles.Settings.WindowPosition);
end

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in Btn_AddBoxes.
function Btn_AddBoxes_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_AddBoxes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

SetEnable(handles, 'off')
set(handles.T_Enter, 'Visible', 'on');

axes(handles.MainAxes);
[x0v, y0v] = ginput;

% get the x, y and z data
[handles.XData, handles.YData, handles.ZData] = GetXYZData(handles);

W = str2double(get(handles.TB_BoxWidth, 'String'));
H = str2double(get(handles.TB_BoxHeight, 'String'));
PFs = handles.PeakList;
for iC = 1:numel(x0v)
    PFs(end+1) = PeakFind(handles.XData(:), handles.YData(:), ...
        handles.ZData(:), x0v(iC), y0v(iC), W, H, handles.MainAxes);
    PFs(end).PerformFit;
    plot(PFs(end));
    addlistener(PFs(end), 'FinishedFitting', @(src, evt) FitDone(src, hObject));
end

handles.PeakList = PFs;
UpdatePeakDisplay(handles);

SetEnable(handles, 'on')
set(handles.T_Enter, 'Visible', 'off');

guidata(hObject, handles);

function SetEnable(handles, State)
UICs = [handles.Btn_AddBoxes, handles.TB_BoxWidth, handles.TB_BoxHeight, ...
    handles.UIT_PeakDisplay, handles.Btn_DeletePoints, ...
    handles.Btn_ImportPoints, handles.Btn_ExportPoints, ...
    handles.Btn_ExportResult, handles.Btn_Refit];
for iU = 1:numel(UICs)
    set(UICs(iU), 'Enable', State);
end

function FitDone(PF, hObject)
handles = guidata(hObject);
plot(PF);
UpdatePeakDisplay(handles);

function UpdatePeakDisplay(handles)
Coeffs = {'x0', 'y0', 'A', 'sigmax', 'sigmay'};
set(handles.UIT_PeakDisplay, 'ColumnName', Coeffs);
D = cell(numel(handles.PeakList), numel(Coeffs));
for iP = 1:numel(handles.PeakList)
    FR = handles.PeakList(iP).FitResult;
    if isempty(FR)
        D(iP, :) = num2cell(NaN(1, numel(Coeffs)));
    else
        CN = coeffnames(FR);
        CV = coeffvalues(FR);
        CI = confint(FR);
        for iC = 1:numel(Coeffs)
            ind = find(ismember(CN, Coeffs{iC}));
            D{iP, iC} = sprintf('%g +/- %g', CV(ind), ...
                (CI(2, ind)-CI(1, ind)));
        end
    end
end

set(handles.UIT_PeakDisplay, 'Data', D);

function TB_BoxWidth_Callback(hObject, eventdata, handles)
% hObject    handle to TB_BoxWidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_BoxWidth as text
%        str2double(get(hObject,'String')) returns contents of TB_BoxWidth as a double


% --- Executes during object creation, after setting all properties.
function TB_BoxWidth_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_BoxWidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_BoxHeight_Callback(hObject, eventdata, handles)
% hObject    handle to TB_BoxHeight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_BoxHeight as text
%        str2double(get(hObject,'String')) returns contents of TB_BoxHeight as a double


% --- Executes during object creation, after setting all properties.
function TB_BoxHeight_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_BoxHeight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Btn_DeletePoints.
function Btn_DeletePoints_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_DeletePoints (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
SetEnable(handles, 'off')

axes(handles.MainAxes);

h = imfreehand(handles.MainAxes);
if isempty(h)
    SetEnable(handles, 'on')
end

PFs = handles.PeakList;
x0PFs = [PFs.x0];
y0PFs = [PFs.y0];
SelectionPos = getPosition(h);
in = inpolygon(x0PFs, y0PFs, SelectionPos(:, 1), SelectionPos(:, 2));
for iS = numel(in):-1:1
    if in(iS)
        delete(PFs(iS));
        PFs(iS) = [];
    end
end

handles.PeakList = PFs;
UpdatePeakDisplay(handles);

delete(h);

SetEnable(handles, 'on')

guidata(hObject, handles);


% --- Executes on button press in Btn_ExportResult.
function Btn_ExportResult_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_ExportResult (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
PFs = handles.PeakList;

[FileName,PathName] = uiputfile({'*.txt', 'Text File'});
if isequal(FileName, 0) || isequal(PathName, 0); return; end

fid = fopen(fullfile(PathName, FileName), 'w');

HeaderWritten = false;
for iP = 1:numel(PFs)
    FR = PFs(iP).FitResult;
    if isempty(FR); continue; end
    if ~HeaderWritten
        CN = coeffnames(FR);
        for iC = 1:numel(CN)
            fprintf(fid, '%s\t%s\t', CN{iC}, ['Delta ', CN{iC}]);
        end
        fprintf(fid, 'x_Box\ty_Box\tW_Box\tH_Box\n');
        HeaderWritten = true;
    end
    CV = coeffvalues(FR);
    CI = confint(FR);
    for iC = 1:numel(CV)
        fprintf(fid, '%g\t%g\t', CV(iC), (CI(2, iC)-CI(1, iC))/2);
    end
    fprintf(fid, '%g\t%g\t%g\t%g\n', PFs(iP).x0-PFs(iP).Width/2, ...
        PFs(iP).y0-PFs(iP).Height/2, PFs(iP).Width, PFs(iP).Height);
end
fclose(fid);


% --- Executes when user attempts to close Fig_PeakFindingModule.
function Fig_PeakFindingModule_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to Fig_PeakFindingModule (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

for iP = 1:numel(handles.PeakList)
    delete(handles.PeakList(iP));
end

delete(handles.MainAxesDestroyedListener);

% store the settings
SettingsPeakFindingModule.WindowPosition = get(hObject, 'Position');
SettingsPeakFindingModule.BoxWidth = get(handles.TB_BoxWidth, 'String');
SettingsPeakFindingModule.BoxHeight = get(handles.TB_BoxHeight, 'String');
% in order to avoid error when no write access for file use try.
try; save(handles.SettingsPath, 'SettingsPeakFindingModule'); end

% Hint: delete(hObject) closes the figure
delete(hObject);


% --- Executes on button press in Btn_ImportPoints.
function Btn_ImportPoints_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_ImportPoints (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[FileName, PathName] = uigetfile({'*.txt', 'Text File'}, 'Select the point file');
if isequal(FileName, 0); return; end
M = importdata(fullfile(PathName, FileName));
if isstruct(M); M = M.data; end
if ~isnumeric(M); return; end
if size(M, 2) ~= 2 && size(M, 2) ~= 4
    msgbox('In the file there should be two or four columns.', 'modal');
    return;
end

SetEnable(handles, 'off')

% get the x, y and z data
[handles.XData, handles.YData, handles.ZData] = GetXYZData(handles);

W = str2double(get(handles.TB_BoxWidth, 'String'));
H = str2double(get(handles.TB_BoxHeight, 'String'));
PFs = handles.PeakList;
for iC = 1:size(M, 1)
    if size(M, 2) == 4
        PFs(end+1) = PeakFind(handles.XData(:), handles.YData(:), ...
            handles.ZData(:), M(iC, 1)+M(iC, 3)/2, M(iC, 2)+M(iC, 4)/2, ...
            M(iC, 3), M(iC, 4), handles.MainAxes);
    else
        PFs(end+1) = PeakFind(handles.XData(:), handles.YData(:), ...
            handles.ZData(:), M(iC, 1)+W/2, M(iC, 2)+H/2, W, H, ...
            handles.MainAxes);
    end
    PFs(end).PerformFit;
    plot(PFs(end));
    drawnow;
    addlistener(PFs(end), 'FinishedFitting', @(src, evt) FitDone(src, hObject));
end

handles.PeakList = PFs;
UpdatePeakDisplay(handles);

SetEnable(handles, 'on')
guidata(hObject, handles);


% --- Executes on button press in Btn_ExportPoints.
function Btn_ExportPoints_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_ExportPoints (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
PFs = handles.PeakList;

[FileName,PathName] = uiputfile({'*.txt', 'Text File'});
if isequal(FileName, 0) || isequal(PathName, 0); return; end

fid = fopen(fullfile(PathName, FileName), 'w');
fprintf(fid, 'x\ty\tWidth\tHeight\n');
for iP = 1:numel(PFs)
    fprintf(fid, '%g\t%g\t%g\t%g\n', PFs(iP).x0-PFs(iP).Width/2, ...
        PFs(iP).y0-PFs(iP).Height/2, PFs(iP).Width, PFs(iP).Height);
end
fclose(fid);


function [xData, yData, data] = GetXYZData(handles)
GD = guidata(handles.MainAxes);
Tbx = guidata(GD.ToolboxModule);
omega = ToolboxModule('GetOmega', GD.ToolboxModule);
chi = str2double(get(Tbx.TB_chi, 'String'));
Space = ToolboxModule('GetSelectedSpace', GD.ToolboxModule);

if isa(GD.RawData, 'qData')
    Q = GD.RawData;
    [xData, yData, ~, ~, rho] = Q.getQ(omega, chi);
    data = GD.CorrectedData;
    if strcmpi(Space, 'polar')
        yData = atan2(xData, yData)*180/pi;
        xData = rho;
    end
else
    RawData = GD.RawData;
    beamline = LoadFileModule('GetBeamline', GD.LoadFileModule);

    if strcmpi(Space, 'q')
        data = RawData(1:beamline.detlenz, 1:beamline.detlenx);
        % calculate q data
        [xData, yData] = CalculateQ(beamline, omega, chi);
    elseif strcmpi(Space, 'pixel')
        sz = size(RawData);
        [xData, yData] = meshgrid(1:sz(2), 1:sz(1));
        data = RawData;
    elseif strcmpi(Space, 'polar')
        % crop data
        data = RawData(1:beamline.detlenz, 1:beamline.detlenx);
        % calculate data
        [xData, yData] = CalculatePolar(beamline, omega, chi);
    end

    % set dead area/pixels to NaN
    data(data<0) = NaN;

    % perform intensity correction on the data
    % apply the corrections to the data
    data = CorrectPixelData(GD.ToolboxModule, ...
        LoadFileModule('GetBeamline', GD.LoadFileModule), data);
end

% --- Executes on button press in Btn_Refit.
function Btn_Refit_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_Refit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[handles.XData, handles.YData, handles.ZData] = GetXYZData(handles);
PFs = handles.PeakList;

for iP = 1:numel(PFs)
    PFs(iP).XData = handles.XData(:);
    PFs(iP).YData = handles.YData(:);
    PFs(iP).ZData = handles.ZData(:);
    PFs(iP).PerformFit();
    plot(PFs(iP));
end

handles.PeakList = PFs;
UpdatePeakDisplay(handles);
guidata(hObject, handles);
