function [PreFix, PostFix, Format] = PFDialog(DefaultPre, DefaultPost, DefaultFormat)
% Dialog to set the file name prefix, file name postfix and the format to
% save the pole figures.
%
% Usage:
%     [PreFix, PostFix, Format] = PFDialog(DefaultPre, DefaultPost, DefaultFormat)
%
% Input:
%     DefaultPre ... the default prefix for the file names
%     DefaultPost ... the default postfix for the file names
%     DefaultFormat ... the default format as string, e.g. 'RWA', 'MAT', 
%                       'TXT'
%
% Output:
%     PreFix ... the prefix the user entered
%     PostFix ... the postfix the user entered
%     Format ... the format the user selected as string, e.g. 'RWA', 'MAT', 
%                'TXT'
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin < 1; DefaultPre = ''; end
if nargin < 2; DefaultPost = ''; end
if nargin < 3; DefaultFormat = 'RWA'; end

% pre-assign the output arguments (if user cancels the dialog with the X in
% the top right corner, they are used as output arguments)
PreFix = '';
PostFix = '';
Format = '';

% Cell of possible formats
Formats = {'RWA', 'MAT', 'TXT'};

% create dialog window
d = dialog('Name', 'Input');
% adapt size of window
P = get(d, 'Position');
set(d, 'Position', [P(1) P(2) 250 350], 'Resize', 'on');

% create label and input field for the prefix
uicontrol('Parent', d, 'Style', 'text', 'Position', [20 315 210 25], ...
    'String', 'Enter file name pre fix:', 'FontSize', 10, ...
    'HorizontalAlignment', 'left');
txt_Prefix = uicontrol('Parent', d, 'Style', 'edit', 'Position', [20 295 210 25], ...
    'String', DefaultPre, 'FontSize', 10, 'HorizontalAlignment', 'left', ...
    'Callback', @(src, evt) UpdatePreview_Callback);

% create label and input field for the postfix
uicontrol('Parent', d, 'Style', 'text', 'Position', [20 260 210 25], ...
    'String', 'Enter file name post fix:', 'FontSize', 10, ...
    'HorizontalAlignment', 'left');
txt_Postfix = uicontrol('Parent', d, 'Style', 'edit', 'Position', [20 240 210 25], ...
    'String', DefaultPost, 'FontSize', 10, 'HorizontalAlignment', 'left', ...
    'Callback', @(src, evt) UpdatePreview_Callback);

% create label and popup menu for the format
uicontrol('Parent', d, 'Style', 'text', 'Position', [20 205 210 25], ...
    'String', 'Select file format:', 'FontSize', 10, ...
    'HorizontalAlignment', 'left');
ind = find(ismember(Formats, DefaultFormat));
PM_Format = uicontrol('Parent', d, 'Style', 'popup', 'Position', [20 185 210 25], ...
    'String', Formats, 'FontSize', 10, 'Value', ind);

% create preview text label
txt_Preview = uicontrol('Parent', d, 'Style', 'text', 'Position', [20 75 210 100], ...
    'String', '', 'FontSize', 10, ...
    'HorizontalAlignment', 'left');
UpdatePreview_Callback();

% create Cancel and OK buttons
uicontrol('Parent', d, 'Style', 'pushbutton', 'Position', [20 20 100 25], ...
    'String', 'OK', 'FontSize', 10, 'Callback', @OK_Callback);
uicontrol('Parent', d, 'Style', 'pushbutton', 'Position', [130 20 100 25], ...
    'String', 'Cancel', 'FontSize', 10, 'Callback', @Cancel_Callback);

set(get(d, 'Children'), 'Units', 'normalized')

% block execution until the dialog is closed
uiwait(d)

    function UpdatePreview_Callback
        % callback to update the preview string
        set(txt_Preview, 'String', ...
            {'File name preview:'; ...
            ['''', get(txt_Prefix, 'String'), ...
            'q_xdyyyyy_+q_xdyyyyy', ...
            get(txt_Postfix, 'String'), '''']; ...
            'where xdyyyyy are the q value and delta q.'});
    end

    function OK_Callback(src, evt)
        % callback for the OK button
        PreFix = get(txt_Prefix, 'String');
        PostFix = get(txt_Postfix, 'String');
        Format = Formats{get(PM_Format, 'Value')};
        delete(d);
    end

    function Cancel_Callback(src, evt)
        % callback for the Cancel button
        PreFix = '';
        PostFix = '';
        Format = '';
        delete(d);
    end
end