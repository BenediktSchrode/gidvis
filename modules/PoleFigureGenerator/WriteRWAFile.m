function WriteRWAFile(FilePath, phi, psi, data, q)
% function to write the pole figure data to an rwa file.
% We have to interpolate the data to a grid with equidistant psi/phi
% values.
%
% Usage:
%     WriteRWAFile(FilePath, phi, psi, data, q)
%
% Input:
%     FilePath ... path to store the *.rwa file to
%     phi ... phi values of the pole figure
%     psi ... psi values of the pole figure
%     data ... intensity data of the pole figure
%     q ... q value for which the pole figure was calculated (corresponding 
%           to a 2Theta angle on a classical texture goniometer)
%
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% for the interpolation the data close to 360� should also be influenced by
% the data at 0� and vice versa. So we expand the phi values by adding
% phi-360 and phi+360 on each side.
phi = [phi-360 phi phi+360];

% step size for the new psi/phi grid
phi_step = 1;
psi_step = 1;

% we perform the interpolation. Since we have increased the size of the phi
% vector, we need to do the same with the psi and data vector, but here we
% can just use repmat. Using TriScatteredInterp instead of
% scatteredInterpolant for backwards compatibility.
scatInt2 = TriScatteredInterp(repmat(abs(psi(:)), 3, 1), phi(:), ...
    repmat(double(data(:)), 3, 1)); %#ok.

% create mesh of new psi/phi grid
[PSI, PHI] = meshgrid(0:psi_step:90, phi_step/2:1:360-phi_step/2);
% perform actual interpolation
data2 = scatInt2(PSI, PHI);
% set NaN values to -1, so that stereopole can read the data
data2(isnan(data2)) = -1;

% this is a default data header for rwa files.
rwa_header = ['FileName, %s.RWA, FileDate, %s, FileTime, %s\n', ...
    'SampleName, N. A., HKL,  N. A.\n', ...
    'TubeType, N. A., FocusType, N. A., Wavelength, %s, GenkVmA, N. A., N. A.\n', ...
    'ReceivingSlit, N. A., N. A., Geometry, N. A.\n', ...
    'InstrumentType, N. A., TwoTheta, %f, Oscillation, N. A.\n', ...
    'PSI, 0.0, 90.0, %f\n', ...
    'PHI, 0.0, 360.0, %f\n', ...
    'TimePerStep, N. A., NumberDataPoints, %d\n', ...
    'ScanData\n'];

% unfortunately Stereopole does not read out the wavelength from the rwa
% file, but uses the hard coded value for chrome. So we have to use this as
% well.
WaveLength = 2.28975;
% calculate the TwoTheta value of the q given
TwoTheta = asin(WaveLength*q/(4*pi))*2*180/pi;
% get the file name
[~, FileName] = fileparts(FilePath);

% open file to write
fid = fopen(FilePath, 'w');
% write header
fprintf(fid, rwa_header, FileName, datestr(now, 'dd-mmm-yyyy'), datestr(now, 'HH:MM'), ...
    num2str(WaveLength), TwoTheta, psi_step, phi_step, numel(data2));
% close file
fclose(fid);
% append the comma delimited intensity values
dlmwrite(FilePath, round(data2'), '-append', 'delimiter', ',');