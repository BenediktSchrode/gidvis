function result = calculatePoleFigure(q_pole, intensity_data, azi_step, q_range, beamline, num_of_files, qxy, qz, qx, qy, q, omega, chi, q_direction, LC, PC, SAC, SDDC, DEC, FF)
% This function calculates the pole figures.
%
% Usage:
%     result = calculatePoleFigure(q_pole, intensity_data, azi_step, q_range, beamline, num_of_files, qxy, qz, qx, qy, q, omega, chi, q_direction, LC, PC, SAC, SDDC, DEC, FF)
% 
% Input:
%     q_pole ... q value for which the pole figure has to be calculated
%     intensity_data ... intensity data of all the files
%     azi_step ... azimuthal (rotation around surface normal) step size
%     q_range ... size of the q_range which should be included in the
%                 calculation
%     beamline ... experimental set-up
%     num_of_files ... number of files read
%     qxy ... qxy values of all the data
%     qz ... qz values of all the data
%     qx ... qx values of all the data
%     qy ... qy values of all the data
%     q ... q values of all the data
%     omega ... omega angle
%     chi ... chi angle
%     q_direction ... +1 or -1, depending on which side to use for the PF
%                     calculation (qx > 0 or qx < 0)
%     LC ... Lorentz correction
%     PC ... polarization correction
%     SAC ... solid angle correction
%     SDDC ... pixel distance correction
%     DEC ... detector efficiency correction
%     FF ... flat field image correction
%
% Output:
%     result ... structure containing the pole figure data and information
%
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% select q values within correct range, we take only values of a half circle
% (qy < 0 and qx < 0)
if q_direction == 1
    q_mask = q>=q_pole-q_range & q<=q_pole+q_range & qz > 0 & qy < 0 & qx > 0;
else
    q_mask = q>=q_pole-q_range & q<=q_pole+q_range & qz > 0 & qy < 0 & qx < 0;
end

% calculate psi and phi angle with respect to sample surface
% psi is the angle between the scattering vector q and the z-axis, which is given by tan(psi) = qxy/qz.
% phi is the angle between the x-axis and the in-plane component of the scattering vector q, i.e. tan(phi) = qy/qx.
psi = atan(qxy(q_mask)./qz(q_mask))*180/pi;
phi = atan(qy(q_mask)./qx(q_mask))*180/pi;

% we have to store the untransformed psi angle
psi_original = psi;

% get the number of data points which will be extracted from each file
pts_per_file = sum(q_mask);

% extract the data points given by the mask from the intensity data matrix
dat2 = double(intensity_data(q_mask, :));
% extract the correct correction factor values
LC = LC(:); PC = PC(:); SAC = SAC(:); SDDC = SDDC(:); DEC = DEC(:); FF = FF(:);
if numel(LC) > 1; LC = repmat(LC(q_mask), 1, num_of_files); end
if numel(PC) > 1; PC = repmat(PC(q_mask), 1, num_of_files); end
if numel(SAC) > 1; SAC = repmat(SAC(q_mask), 1, num_of_files); end
if numel(SDDC) > 1; SDDC = repmat(SDDC(q_mask), 1, num_of_files); end
if numel(DEC) > 1; DEC = repmat(DEC(q_mask), 1, num_of_files); end
if numel(FF) > 1; FF = repmat(FF(q_mask), 1, num_of_files); end
% apply the correction factors to the intensity data
dat2 = dat2./LC./PC.*SAC.*SDDC.*DEC.*FF;
% transform to vector
dat2 = dat2(:);
% the psi angle is the same for every file
psi_pole = repmat(psi_original, num_of_files, 1);
% for phi, we have to add the azimuthal step
phi_pole = -repmat(phi, 1, num_of_files)+repmat(((0:num_of_files-1)*azi_step), pts_per_file, 1);
phi_pole = phi_pole(:);

% apply stereographic projection to psi
psi_transformed = sind(180-psi_pole)./(1-cosd(180-psi_pole))*90;

% for a visualization in an xy grid, we calculate the x and the y
% coordinates of the psi/phi value pairs
x_pole = psi_transformed.*cosd(phi_pole);
y_pole = psi_transformed.*sind(phi_pole);
% phi might go above 360, use modulo to reduce it to the range 0 to 360
phi_pole = mod(phi_pole, 360);

% we use TriScatteredInterp to interpolate the data onto the new grid
% (defined by x_pole, y_pole)
scatInt = TriScatteredInterp(x_pole(:),y_pole(:), double(dat2));
grid_res = 501;
[X, Y] = meshgrid(linspace(-90, 90, grid_res), linspace(-90, 90, grid_res));
data = scatInt(X, Y);
        
% store the results in result struct
result.intensity = data;
result.psi = Y;
result.phi = X;
% store some additional parameters
result.q_pole = q_pole;
result.azi_step = azi_step;
result.q_range = q_range;
result.q_direction = q_direction;
result.omega = omega;
result.chi = chi;
% these three variables are needed for exporting
result.psi_pole = psi_pole(:);
result.phi_pole = phi_pole(:);
result.data_pole = dat2(:);