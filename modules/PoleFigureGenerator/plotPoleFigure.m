function plotPoleFigure(result, selection)
% This function plots the pole figures.
%
% Usage:
%     plotPoleFigure(result, selection)
%
% Input:
%     result ... array of struct with pole figure results as obtained from
%                the calculatePoleFigure function
%     selection ... index of the pole figure to plot
% 
%
% This file is part of GIDVis.
%
% see also: calculatePoleFigure, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% checkout data for requested plot
gg = result(selection).phi; % phi (round and round)
hh = result(selection).psi; % psi (from center to edge)
dat2 = result(selection).intensity; % brings the color to your pole

% calculate the color bar limits
limits = getColorBarLimits(dat2);

% find the plot window, or make a new one
h = findobj('type', 'Figure', 'Tag', 'PoleImage');
if isempty(h)
    h = figure('Name', 'Pole Figure Plot', 'NumberTitle', 'off', 'Tag', 'PoleImage');
    % add an axes to the plot
    poleaxes = axes('Parent', h);
    
    % further down we have to draw the polar axes grid
    DrawGrid = true;
    
    % add ui controls to adapt color limits
    uicontrol('Style', 'text', 'String', 'Color Limits:', 'Units', ...
        'normalized', 'Position', [0.02 0.94 0.15 0.04], 'FontSize', 10, ...
        'HorizontalAlignment', 'left');
    TB_ColorMin = uicontrol('Style', 'edit', 'String', num2str(limits(1)), 'Units', ...
        'normalized', 'Position', [0.18 0.93 0.2 0.06], 'FontSize', 10, ...
        'Tag', 'TB_ColorMin');
    TB_ColorMax = uicontrol('Style', 'edit', 'String', num2str(limits(2)), 'Units', ...
        'normalized', 'Position', [0.40 0.93 0.2 0.06], 'FontSize', 10, ...
        'Tag', 'TB_ColorMax');
    Btn_ColorAuto = uicontrol('Style', 'pushbutton', 'String', 'Auto', ...
        'Units', 'normalized', 'Position', [0.62 0.93 0.2 0.06], ...
        'FontSize', 10, 'Tag', 'Btn_ColorAuto');
    CB_ColorLock = uicontrol('Style', 'checkbox', 'String', 'Lock', ...
        'Units', 'normalized', 'Position', [0.84 0.94 0.15 0.04], ...
        'FontSize', 10, 'Tag', 'CB_ColorLock');
    set([TB_ColorMin, TB_ColorMax], 'Callback', @(src, evt) SetColorLimits(TB_ColorMin, TB_ColorMax, poleaxes));
else
    % the figure already exists, just get the axes we have to plot to
    poleaxes = get(h, 'CurrentAxes');
    % there should be a grid already
    DrawGrid = false;
    
    TB_ColorMin = findobj(h, 'Tag', 'TB_ColorMin');
    TB_ColorMax = findobj(h, 'Tag', 'TB_ColorMax');
    CB_ColorLock = findobj(h, 'Tag', 'CB_ColorLock');
    Btn_ColorAuto = findobj(h, 'Tag', 'Btn_ColorAuto');
end

% set the callback of the automatic button
set(Btn_ColorAuto, 'Callback', @(src, evt) AutoColorLimits(TB_ColorMin, TB_ColorMax, limits, poleaxes));

% bring figure to front
set(0, 'currentfigure', h);
% check if there's already a pole figure plotted in the axes
pc = findobj(poleaxes, 'Tag', 'PoleFigurePlot');
if isempty(pc) % nothing plotted yet, create new pole figure plot with pcolor
    pc = pcolor(poleaxes, gg, hh, dat2);
    set(pc, 'Tag', 'PoleFigurePlot'); % set the tag, so that it can be found later
else
    % just update the elements x, y, and cdata
    set(pc, 'XData', gg, 'YData', hh, 'CData', dat2);
end

% change axes appearance
axis(poleaxes, 'equal')
shading(poleaxes, 'flat')

% set colormap (with 256 colors)
colormap(inferno(256));

% write new color limit values into the boxes if not locked
if ~get(CB_ColorLock, 'Value')
    set(TB_ColorMin, 'String', num2str(limits(1)));
    set(TB_ColorMax, 'String', num2str(limits(2)));
end
SetColorLimits(TB_ColorMin, TB_ColorMax, poleaxes)

% draw the polar axes grid if necessary
if DrawGrid
    % perform calculations for drawing circles
    phi = linspace(0, 2*pi, 500);
    cphi = cos(phi);
    sphi = sin(phi);

    hold(poleaxes, 'on');

    for k = [15 30 45 60 75 90] % draw ring at these psi values
        % we have to calculate the radius according to projection
        R = sind(180-k)/(1-cosd(180-k))*90;
        % calculate x and y coordinates
        x = R.*cphi;
        y = R.*sphi;

        % plot the ring
        plot(x, y, '-w', 'Linewidth', 1.0, 'Parent', poleaxes) 
    end
    
    % plot the radial lines
    for k = 0:30:360
        y = 90.*sind(k);
        x = 90.*cosd(k);
        plot([0 x], [0 y], '-w', 'LineWidth', 1.0, 'Parent', poleaxes);
    end
    
    hold(poleaxes, 'off');
end

% overwrite the data cursor to display correct values
dcm_obj = datacursormode(h);
set(dcm_obj, 'UpdateFcn', @ppDataCursorUpdateFunction);
end

function AutoColorLimits(TB_ColorMin, TB_ColorMax, limits, poleaxes)
set(TB_ColorMin, 'String', num2str(limits(1)));
set(TB_ColorMax, 'String', num2str(limits(2)));
SetColorLimits(TB_ColorMin, TB_ColorMax, poleaxes);
end

function SetColorLimits(TB_ColorMin, TB_ColorMax, ax)
    MinVal = str2double(get(TB_ColorMin, 'String'));
    MaxVal = str2double(get(TB_ColorMax, 'String'));
    caxis(ax, [MinVal MaxVal]);
end

function txt = ppDataCursorUpdateFunction(empt, obj)
%changes string for DataCursor

% get the position of the data cursor
pos = obj.Position;
% calculate azimuthal angle (range -180 to 180)
phi = atan2(pos(2), pos(1))*180/pi;
% shift range of phi to the right values
if phi < 0
    phi = 360 + phi;
end

% this is the radius, but it's a transformed one
R = sqrt(pos(1)^2+pos(2)^2);
% to get psi, undo transformation
psi = atand(R/90)*2;

Tar = obj.Target;

if isfield(get(Tar), 'CData') % we're dealing with an image (e.g. user clicked map)
    CDat = get(Tar, 'CData'); % get the cdata (e.g. intensity of map)
    Ind = get(obj, 'DataIndex'); % get the indices from the data cursor
    
    % in newer Matlab version, Ind is a linear index. Transform it to
    % row/column index
    if numel(Ind) == 1
        [I1, I2] = ind2sub(size(CDat), Ind);
        Ind = [I2, I1];
    end
    
    % extract the intensity value
    Intensity = CDat(Ind(2), Ind(1));

    % make string
    txt = {sprintf('psi: %g', psi),...
       sprintf('phi: %g', phi), ...
       sprintf('Intensity: %g', Intensity)};
else
    txt = {sprintf('psi: %g', psi),...
       sprintf('phi: %g', phi)};
end
end