function varargout = poleFigGen(varargin)
% This file, together with poleFigGen.fig, is the pole figure generator of
% GIDVis.
%
% Usage:
%     varargout = poleFigGen(MainAxesHandle, src, LB_Files, TB_CurrentPath, TB_Status)
%
% Input:
%     MainAxesHandle ... handle of the GIDVis main axes
%     src ... handle to the button which was used to start the pole figure
%             generator
%     LB_Files ... handle of the listbox containing the files
%     TB_CurrentPath ... handle to the text box containing the current path
%     TB_Status ... text where the progress messages are written to during 
%                   file import
%
% Output:
%     varargout ... default GUIDE output
%
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Last Modified by GUIDE v2.5 23-Mar-2018 12:51:07

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @poleFigGen_OpeningFcn, ...
                   'gui_OutputFcn',  @poleFigGen_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before poleFigGen is made visible.
function poleFigGen_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to poleFigGen (see VARARGIN)

% Choose default command line output for poleFigGen
handles.output = hObject;

%get the arguments
handles.LFM_Control = varargin{2};
handles.LB_Files = varargin{3};
Selection = LoadFileModule('GetSelectedFilePath', handles.LFM_Control);
handles.NumFiles = numel(Selection);
handles.TB_CurrentPath = varargin{4};

handles.TB_Status = varargin{5};
set(handles.TB_Status, 'String', sprintf('Reading files'));
drawnow;

% the settings for this module are stored in the same directory as the file
% LoadFileModule.m. Store its path in the handles structure to reuse it.
handles.SettingsPath = fullfile(fileparts(mfilename('fullpath')), 'Settings.mat');

% store the file names in the handles structure
handles.SelectedFiles = Selection;

%load files
data = ReadDataFromFile(Selection, [], 1, handles.TB_Status);
if ~iscell(data)
%     data(data < 0) = 0;
    data(isnan(data)) = NaN;
end
% intensity = reshape(data, size(data, 1)*size(data, 2), [], 1);
handles.intensity = data;

set(handles.TB_Status, 'String', sprintf('Done'));
drawnow;

% create a hggroup in the MainAxes where the rings for visualization of the
% pole figure calculation are drawn to
handles.HGVisualize = hggroup('Parent', varargin{1}, 'DisplayName', 'Pole Figure Area');


% add a listener to the axes in varargin{1}. When the axes gets destroyed 
% (e.g. because the figure containing the axes gets closed), the callback 
% will be executed (and closes this window).
% Store the listener in the handles structure to remove it when this window    
% gets closed by user, otherwise the callback is executed even when the
% LoadFileModule window does not exist anymore.
handles.MainAxesDestroyedListener = addlistener(varargin{1},...
    'ObjectBeingDestroyed', @(src, evt) MainAxesDestroyed_Callback(src,...
    evt, handles.Figure_poleFigGen));

% we prepare a waitbartext for all the long calculations
set(handles.waitbarText, 'String', '');

% read out settings if available
if exist(handles.SettingsPath, 'file') == 2
    load(handles.SettingsPath);
    % store the window position and the save path in the handles structure
    handles.WindowPosition = SettingsPoleFigureModule.WindowPosition;
    handles.DefaultSavePath = SettingsPoleFigureModule.DefaultSavePath;
    % set the text box values according to last user entries
    set(handles.q_start, 'String', SettingsPoleFigureModule.q_start);
    set(handles.q_end, 'String', SettingsPoleFigureModule.q_end);
    set(handles.q_step, 'String', SettingsPoleFigureModule.q_step);
    set(handles.q_range, 'String', SettingsPoleFigureModule.q_range);
    set(handles.singlePole, 'Value', SettingsPoleFigureModule.singlePole);
    set(handles.multiPole, 'Value', SettingsPoleFigureModule.multiPole);
    set(handles.q_plus, 'Value', SettingsPoleFigureModule.q_plus);
    set(handles.q_minus, 'Value', SettingsPoleFigureModule.q_minus);
    if SettingsPoleFigureModule.singlePole
        singlePole_Callback(handles.singlePole, [], handles)
    else
        multiPole_Callback(handles.multiPole, [], handles)
    end
    % get the pre and post fix of the rwa files
    if isfield(SettingsPoleFigureModule, 'FileSavePreFix')
        handles.FileSavePreFix = SettingsPoleFigureModule.FileSavePreFix;
    else
        handles.FileSavePreFix = '';
    end
    if isfield(SettingsPoleFigureModule, 'FileSavePostFix')
        handles.FileSavePostFix = SettingsPoleFigureModule.FileSavePostFix;
    else
        handles.FileSavePostFix = '';
    end
else
    handles.DefaultSavePath = cd();
    handles.FileSavePreFix = '';
    handles.FileSavePostFix = '';
    singlePole_Callback(handles.singlePole, [], handles)
end

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes poleFigGen wait for user response (see UIRESUME)
% uiwait(handles.Figure_poleFigGen);


% --- Outputs from this function are returned to the command line.
function varargout = poleFigGen_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% restore the window position
if isfield(handles, 'WindowPosition')
    RestorePositionOrDefault(hObject, handles.WindowPosition);
end


% --- Executes on button press in calculate.
function calculate_Callback(hObject, eventdata, handles)
% hObject    handle to calculate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% disable Calculate, clear list and export buttons while calculating
set([hObject, handles.clear_list, handles.Btn_ExportSelected], 'Enable', 'off');

% update status label
set(handles.waitbarText, 'String', 'Getting input values');
drawnow 

% first we organize some data
beamline = LoadFileModule('GetBeamline', handles.LFM_Control);
data = handles.intensity;
data = data(1:beamline.detlenz, 1:beamline.detlenx, :);
intensity = reshape(data, beamline.detlenx*beamline.detlenz, [], 1);

% some data prehandling
num_of_files = handles.NumFiles; % number of files in the selected folder
azi_step = 360/num_of_files; %default value for azimuthal steps

% still missing input values q_start, q_end, stepsize and q_range
q_range = str2double(get(handles.q_range,'string'));
if get(handles.singlePole, 'Value')
    % Create the vector containing the q values for which pole figures
    % should be calculated. Use str2num to allow the user to enter multipe
    % values separated by ;
    q_pole = str2num(get(handles.q_start, 'String'));
    if isempty(q_pole); msgbox('Invalid value entered for q value(s).', 'modal'); end
    % q_pole has to be of dimension 1 x N
    q_pole = reshape(q_pole, 1, numel(q_pole));
else
    q_start = str2double(get(handles.q_start,'String'));
    q_end = str2double(get(handles.q_end,'String'));
    stepsize = str2double(get(handles.q_step,'String'));
    if isnan(q_start); msgbox('Invalid value entered for q_start.', 'modal'); end
    if isnan(q_end); msgbox('Invalid value entered for q_end.', 'modal'); end
    if isnan(stepsize); msgbox('Invalid value entered for q stepsize.', 'modal'); end
    q_pole = q_start:stepsize:q_end;
    if isnan(q_pole); q_pole = []; end % if any of the conversions to a number failed, q_pole will be NaN. Then, set q_pole to empty.
end

if isempty(q_pole) % happens if any of the conversions to a number failed
    % enable clear list and export buttons again if there are
    % pole figures in the list already
    if ~isempty(get(handles.listFigures, 'String'))
        set([handles.clear_list, handles.Btn_ExportSelected], 'Enable', 'on');
    end
    set(hObject, 'Enable', 'on');
    set(handles.waitbarText, 'String', 'Done');
    % leave the callback
    return;
end

q_direction = get(handles.q_plus,'Value');
q_pole(q_pole <= 0) = []; % remove q <= 0 from list, pole figures cannot be calculated for these q values
q_pole = unique(q_pole);

LB_mat  = q_pole';
if q_direction == 1
    LB_mat(:,2) = 1;
else
    LB_mat(:,2) = -1;
end
LB_mat(:,3) = q_range;

try
    list = handles.list;
    [difference,diffNum] = setdiff(LB_mat, list, 'rows');
    q_pole = q_pole(diffNum);
    q_pole_list = [list;difference]; % for listbox population
catch
    list = LB_mat;
    q_pole_list = list;
end


% %prepare string for listbox
% LB_cell = cellstr(num2str(q_pole'));
% if q_direction == 1
%     LB_cell(:,2) = cellstr('+q');
% else
%     LB_cell(:,2) = cellstr('-q');
% end
% LB_cell(:,3) = cellstr(num2str(q_range));
% 
% [m n] = size(LB_cell);
% cellCols = mat2cell(LB_cell,m,ones(1,n)); %// group by columns
% cellColsSpace(1:2:2*size(LB_cell,2)-1) = cellCols; %// make room (columns)...
% cellColsSpace(2:2:2*size(LB_cell,2)-1) = {{' '}}; %// ...to introduce spaces
% %// Note that a cell within cell is needed, because of how strcat works.
% LB_Entries = strcat(cellColsSpace{:});

% we check for already calculated poleFigures


% and calculate only whats not already calculated
% try
%     list = handles.list;
%     [difference,diffNum] = setdiff(LB_Entries, list);
%     q_pole = q_pole(diffNum);
%     q_pole_list = [list;difference]; % for listbox population
% catch
%     list = LB_Entries;
%     q_pole_list = list;
% end

% q_pole = setdiff(q_pole, list);


% now we have everything to calculate some pole figures
try
    cut = 0;
    result = handles.result;
catch
    result = struct('intensity',[],'psi',[],'phi',[], 'psi_pole', [], ...
        'phi_pole', [], 'data_pole', [], 'q_pole', [], 'azi_step', [], ...
        'q_range', [], 'q_direction', [], 'omega', [], 'chi', []); %preallocate result structure
    cut = 1; %later we cut away the first empty entry from preallocation
end

%we need some data from the ToolboxModule
gui_ToolboxModule = findall(0, 'Type', 'figure', 'Tag', 'Figure_ToolboxModule');
ToolboxData = guidata(gui_ToolboxModule);

% from the ToolboxModule we get omega and chi
omega = ToolboxModule('GetOmega', gui_ToolboxModule);
chi = str2double(get(ToolboxData.TB_chi, 'String'));

% lets get some q values
[qxy, qz, qx, qy, q] = CalculateQ(beamline, omega, chi);

% get the values of the correction factor check boxes
Lorentz = get(ToolboxData.CB_LorentzCorrection, 'Value');
Polarization = get(ToolboxData.CB_PolarizationCorrection, 'Value');
SolidAngle = get(ToolboxData.CB_SolidAngleCorrection, 'Value');
PixelDistance = get(ToolboxData.CB_PixelDistanceCorrection, 'Value');
DetectorEff = get(ToolboxData.CB_DetectorEfficiencyCorrection, 'Value');
FlatField = get(ToolboxData.CB_FlatFieldCorrection, 'Value');

% check the values of the checkboxes
if any([Lorentz Polarization SolidAngle PixelDistance DetectorEff FlatField])
    % calculate the correction factors
    [LC, PC, SAC, SDDC, DEC] = CorrectionFactors(beamline, omega, chi);
    % get the flat field image
    FF = beamline.FFImageData;
    % crop the flat field image to the size given by detlenx and detlenz
    if numel(FF) > 1
        FF = FF(1:beamline.detlenz, 1:beamline.detlenx);
    end
    % we only need the correction factors where the checkbox is checked,
    % set the others to 1
    if ~Lorentz; LC = 1; end
    if ~Polarization; PC = 1; end
    if ~SolidAngle; SAC = 1; end
    if ~PixelDistance; SDDC = 1; end
    if ~DetectorEff; DEC = 1; end
    if ~FlatField || isempty(FF); FF = 1; end
else % no corrections have to be applied, set them all to 1
    LC = 1;
    PC = 1;
    SAC = 1;
    SDDC = 1;
    DEC = 1;
    FF = 1;
end

for i = 1:length(q_pole)
    
    if i > 1
        time(i-1) = toc;

        set(handles.waitbarText, 'String', ['Calculating Pole Figure ', num2str(i),' of ', num2str(length(q_pole)), ' | Remaining Time: ', num2str(mean(time).*(length(q_pole)-i),'%.0f'),' s']);
        drawnow
    elseif i == 1
        set(handles.waitbarText, 'String', ['Calculating Pole Figure ', num2str(i),' of ', num2str(length(q_pole))]);
        drawnow
    end
    
    
    tic
    % new calculations will be added at the end, later we sort
    result(end+1) = calculatePoleFigure(q_pole(i), intensity, azi_step, ...
        q_range, beamline, num_of_files, qxy, qz, qx, qy, q, ...
        omega, chi, q_direction, LC, PC, SAC, SDDC, DEC, FF);
    
end

if cut
    result(1) = [];
end
% we sort the result so we keep the right order
[~, I] = sortrows(q_pole_list);
result = result(I);
B = q_pole_list(I,:);

% prepare the string for the list box
strC = cell(size(B, 1), 1);
for iR = 1:size(B, 1)
    str = sprintf('%7.5f    ', B(iR, 1));
    if B(iR, 2) == 1
        str = [str, '+q_xy    '];
    else
        str = [str, '-q_xy    '];
    end
    str = [str, sprintf('%7.5f', B(iR, 3))];
    strC{iR} = str;
end

% populate Listbox with calculated Figures
set(handles.listFigures, 'String', strC);
set(handles.listFigures, 'Value', 1);


% enable Calculate, clear list and export buttons again
set([hObject, handles.clear_list, handles.Btn_ExportSelected], 'Enable', 'on');
set(handles.waitbarText, 'String', 'Done');
drawnow

% store some data for the next calculation, so we dont need to reload files
handles.result = result;
handles.list = B;
set(handles.clear_list, 'Enable', 'on');
guidata(hObject, handles);

function q_start_Callback(hObject, eventdata, handles)
% hObject    handle to q_start (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of q_start as text
%        str2double(get(hObject,'String')) returns contents of q_start as a double


% if single PoleFigure calculation is set, set q_end value to q_start value
toggleState = get(handles.singlePole, 'Value');

if toggleState
    set(handles.q_end, 'String', get(hObject, 'String'));
end

% --- Executes during object creation, after setting all properties.
function q_start_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q_start (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function q_end_Callback(hObject, eventdata, handles)
% hObject    handle to q_end (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of q_end as text
%        str2double(get(hObject,'String')) returns contents of q_end as a double


% --- Executes during object creation, after setting all properties.
function q_end_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q_end (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function q_step_Callback(hObject, eventdata, handles)
% hObject    handle to q_step (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of q_step as text
%        str2double(get(hObject,'String')) returns contents of q_step as a double


% --- Executes during object creation, after setting all properties.
function q_step_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q_step (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function MainAxesDestroyed_Callback(src, evt, HandleToPoleFigGen)

% close module window
close(HandleToPoleFigGen);

% --- Executes when user attempts to close Figure_poleFigGen.
function Figure_poleFigGen_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to Figure_poleFigGen (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% delete Listener to MainWindow
delete(handles.MainAxesDestroyedListener);

% find the pole figure plot window
h = findobj('type', 'Figure', 'Tag', 'PoleImage');
% close the pole figure plot window
if ~isempty(h); delete(h(ishandle(h))); end

% delete lines from GIDVis main axes
delete(get(handles.HGVisualize, 'Children'));
delete(handles.HGVisualize);

% store the settings
SettingsPoleFigureModule.q_start = get(handles.q_start, 'String');
SettingsPoleFigureModule.q_end = get(handles.q_end, 'String');
SettingsPoleFigureModule.q_step = get(handles.q_step, 'String');
SettingsPoleFigureModule.q_range = get(handles.q_range, 'String');
SettingsPoleFigureModule.singlePole = get(handles.singlePole, 'Value');
SettingsPoleFigureModule.multiPole = get(handles.multiPole, 'Value');
SettingsPoleFigureModule.WindowPosition = get(hObject, 'Position');
SettingsPoleFigureModule.q_plus = get(handles.q_plus, 'Value');
SettingsPoleFigureModule.q_minus = get(handles.q_minus, 'Value');
SettingsPoleFigureModule.DefaultSavePath = handles.DefaultSavePath;
SettingsPoleFigureModule.FileSavePreFix = handles.FileSavePreFix;
SettingsPoleFigureModule.FileSavePostFix = handles.FileSavePostFix;
% in order to avoid error when no write access for file use try.
try; save(handles.SettingsPath, 'SettingsPoleFigureModule'); end

% Hint: delete(hObject) closes the figure
delete(hObject);



% --- Executes on button press in singlePole.
function singlePole_Callback(hObject, eventdata, handles)
% hObject    handle to singlePole (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of singlePole

set(handles.txt_q_start, 'String', 'q value(s)');
set(handles.q_end, 'Enable', 'off', 'String', get(handles.q_start, 'String'));
set(handles.q_step, 'Enable', 'off', 'String', '1');
set(handles.txt_Explanation, 'Visible', 'on');

% --- Executes on button press in multiPole.
function multiPole_Callback(hObject, eventdata, handles)
% hObject    handle to multiPole (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of multiPole

set(handles.txt_q_start, 'String', 'q_start');
set(handles.q_end, 'Enable', 'on');
set(handles.q_step, 'Enable', 'on');
set(handles.txt_Explanation, 'Visible', 'off');

% --- Executes when selected object is changed in ButtonGroup.
function ButtonGroup_SelectionChangedFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in ButtonGroup 
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in listFigures.
function listFigures_Callback(hObject, eventdata, handles)
% hObject    handle to listFigures (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listFigures contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listFigures


selection = get(hObject,'Value'); % get selected item
if numel(selection) > 1; return; end
if ~isfield(handles, 'result'); return; end

if isempty(handles.result(selection).intensity)
    msgbox('Selected pole figure does not contain any data. No plot possible.')
    return;
end

set(handles.waitbarText, 'String', 'Plotting...');
drawnow();

plotPoleFigure(handles.result, selection);

% delete the already plotted lines which visualize the area used for
% calculation of the pole figure
delete(get(handles.HGVisualize, 'Children'));
RInner = handles.list(selection, 1)-handles.list(selection, 3);
ROuter = handles.list(selection, 1)+handles.list(selection, 3);
phi = linspace(0, 180, 200);
cphi = cosd(phi);
sphi = sind(phi);
x = [ROuter*cphi, RInner*fliplr(cphi), ROuter*cphi(1)];
y = [ROuter*sphi, RInner*fliplr(sphi), ROuter*sphi(1)];
plot(x, y, 'k', 'Parent', handles.HGVisualize, 'LineWidth', 1.6);
plot(x, y, 'w', 'Parent', handles.HGVisualize, 'LineWidth', 1);


set(handles.waitbarText, 'String', 'Done');
drawnow();


% --- Executes during object creation, after setting all properties.
function listFigures_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listFigures (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in clear_list.
function clear_list_Callback(hObject, eventdata, handles)
% hObject    handle to clear_list (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles = rmfield(handles, {'result','list'});
set(handles.listFigures, 'String', []);
set([handles.clear_list, handles.Btn_ExportSelected], 'Enable', 'off');
delete(get(handles.HGVisualize, 'Children'));
guidata(hObject, handles);



function q_range_Callback(hObject, eventdata, handles)
% hObject    handle to q_range (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of q_range as text
%        str2double(get(hObject,'String')) returns contents of q_range as a double


% --- Executes during object creation, after setting all properties.
function q_range_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q_range (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in q_plus.
function q_plus_Callback(hObject, eventdata, handles)
% hObject    handle to q_plus (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of q_plus


% --- Executes on button press in q_minus.
function q_minus_Callback(hObject, eventdata, handles)
% hObject    handle to q_minus (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of q_minus


% --- Executes on button press in Btn_ExportSelected.
function Btn_ExportSelected_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_ExportSelected (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

selection = get(handles.listFigures, 'Value'); % get selected item(s)
if isempty(selection); return; end % nothing selected

% let user selected a directory to save files to
Path = uigetdir(handles.DefaultSavePath);

if isequal(Path, 0); return; end % user cancelled saving

% store the selected path in the handles structure
handles.DefaultSavePath = Path;

% get file pre and post fix and the format
[PreFix, PostFix, Format] = PFDialog(handles.FileSavePreFix, handles.FileSavePostFix);

% user might cancel
if isempty(PreFix) && isempty(PostFix) && isempty(Format); return; end

% disable Calculate, clear list and export buttons while exporting
set([handles.calculate, handles.clear_list, handles.Btn_ExportSelected], 'Enable', 'off');

% save the answer in the handles structure
handles.FileSavePreFix = PreFix;
handles.FileSavePostFix = PostFix;

% update status text
set(handles.waitbarText, 'String', 'Exporting files ...');
drawnow();

% loop over selected files
for iS = 1:numel(selection)
    % update status text
    set(handles.waitbarText, 'String', sprintf('Exporting file %d of %d.', iS, numel(selection)));
    drawnow();
    
    % get result struct for this pole figure
    Res = handles.result(selection(iS));
    % get phi, psi and data
    phi = Res.phi_pole;
    psi = Res.psi_pole;
    data = Res.data_pole;
    
    % get the properties used to create this pole figure
    ListEntry = handles.list(selection(iS), :);
    % create a file name out of the pole figure properties
    FilePath = [PreFix, 'q_', strrep(sprintf('%.5f_', ListEntry(1)), '.', 'd')];
    if ListEntry(2) == 1
        FilePath = [FilePath, '+q_'];
    else
        FilePath = [FilePath, '-q_'];
    end
    FilePath = [FilePath, strrep(sprintf('%.5f', ListEntry(3)), '.', 'd')];
    FilePath = fullfile(Path, [FilePath, PostFix]);
    
    % write the data to the file, depending on the choice of the Format
    % variable
    switch lower(Format)
        case 'rwa'
            FilePath = [FilePath, '.rwa'];
            WriteRWAFile(FilePath, phi, psi, data, ListEntry(1));
        case 'mat'
            FilePath = [FilePath, '.mat'];
            PFData = Res;
            save(FilePath, 'PFData');
        case 'txt'
            FilePath = [FilePath, '.txt'];
            M = [Res.psi_pole Res.phi_pole Res.data_pole];
            fid = fopen(FilePath, 'w');
            fprintf(fid, 'Psi\tPhi\tIntensity\n');
            fprintf(fid, '%g\t%g\t%g\n', M');
            fclose(fid);
    end
end

set(handles.waitbarText, 'String', 'Export successful');

% enable Calculate, clear list and export buttons again
set([handles.calculate, handles.clear_list, handles.Btn_ExportSelected], 'Enable', 'on');

guidata(hObject, handles);


% --- Executes on button press in Btn_FileList.
function Btn_FileList_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_FileList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% get the beamline
beamline = LoadFileModule('GetBeamline', handles.LFM_Control);

%we need some data from the ToolboxModule
gui_ToolboxModule = findall(0, 'Type', 'figure', 'Tag', 'Figure_ToolboxModule');
ToolboxData = guidata(gui_ToolboxModule);

% from the ToolboxModule we get omega and chi
omega = ToolboxModule('GetOmega', gui_ToolboxModule);
chi = str2double(get(ToolboxData.TB_chi, 'String'));

% lets get some q values
[qxy, qz, qx, qy, q] = CalculateQ(beamline, omega, chi);

f = figure('Name', 'File List', 'Toolbar', 'none', 'MenuBar', 'none', ...
    'WindowStyle', 'modal');
ax = axes('Parent', f, 'Units', 'normalized', 'Position', [.25 .02 .73 .86]);
LB = uicontrol(f, 'Style', 'listbox', 'Units', 'normalized', ...
    'Position', [0.02 0.02 .21 .96], 'FontSize', 10, ...
    'String', {handles.SelectedFiles.DisplayName}, 'Min', 0, 'Max', 2, 'FontSize', 10);
uicontrol(f, 'Style', 'text', 'Units', 'normalized', ...
    'Position', [.25 .94 .1 .04], 'String', 'q Value:', 'FontSize', 10);
q_slider = uicontrol(f, 'Style', 'slider', 'Units', 'normalized', ...
    'Position', [.37 .94 .2 .04], 'Min', 0, 'Max', 10, 'Value', 1, ...
    'SliderStep', [0.001 0.01]);
q_DirPlus = uicontrol(f, 'Style', 'radiobutton', 'Units', 'normalized', ...
    'Position', [.6 .94 .1 .04], 'String', '+q', 'Value', 1);
q_DirMinus = uicontrol(f, 'Style', 'radiobutton', 'Units', 'normalized', ...
    'Position', [.72 .94 .1 .04], 'String', '-q', 'Value', 0);
set(q_DirPlus, 'Callback', @(src, evt) FileList_qDir_Callback(LB, ax, q, qxy, qx, qy, qz, q_slider, handles, q_DirPlus, q_DirMinus, 1));
set(q_DirMinus, 'Callback', @(src, evt) FileList_qDir_Callback(LB, ax, q, qxy, qx, qy, qz, q_slider, handles, q_DirPlus, q_DirMinus, -1));
set([LB, q_slider], 'Callback', @(src, evt) LB_FileList_Callback(LB, ax, q, qxy, qx, qy, qz, q_slider, handles, q_DirPlus));

% Call the LB_FileList_Callback once to update the plot
LB_FileList_Callback(LB, ax, q, qxy, qx, qy, qz, q_slider, handles, q_DirPlus);

function FileList_qDir_Callback(LB, ax, q, qxy, qx, qy, qz, q_slider, handles, q_DirPlus, q_DirMinus, Dir)
if Dir > 0
    set(q_DirPlus, 'Value', 1);
    set(q_DirMinus, 'Value', 0);
else
    set(q_DirPlus, 'Value', 0);
    set(q_DirMinus, 'Value', 1);
end
LB_FileList_Callback(LB, ax, q, qxy, qx, qy, qz, q_slider, handles, q_DirPlus);

function LB_FileList_Callback(hObject, AxesHandle, q, qxy, qx, qy, qz, q_slider, handles, q_DirPlus)
% get user entered values
q_range = str2double(get(handles.q_range,'string'));
% q_direction = get(handles.q_plus,'Value');
q_direction = get(q_DirPlus, 'Value');

q_pole = get(q_slider, 'Value');

% select q values within correct range, we take only values of a half circle
% (qy < 0 and qx < 0)
if q_direction == 1
    q_mask = q>=q_pole-q_range & q<=q_pole+q_range & qz > 0 & qy < 0 & qx > 0;
else
    q_mask = q>=q_pole-q_range & q<=q_pole+q_range & qz > 0 & qy < 0 & qx < 0;
end

% calculate psi and phi angle with respect to sample surface
% psi is the angle between qxy and qz; phi is the angle between qx and qy
psi = atan(qxy(q_mask)./qz(q_mask))*180/pi;
phi = atan(qy(q_mask)./qx(q_mask))*180/pi;

% apply stereographic projection to psi
% psi_transformed = sind(180-psi)./(1-cosd(180-psi))*90;
psi_transformed = psi;

PhiStep = 360/numel(get(hObject, 'String'));
Index = get(hObject, 'Value');
delete(get(AxesHandle, 'Children'));
for Ind = 1:numel(Index)
    phi_ = -phi+(Index(Ind)-1)*PhiStep;
    phi_ = mod(phi_, 360);
    polar(AxesHandle, phi_*pi/180, psi_transformed, '.-');
    hold(AxesHandle, 'on');
end
hold(AxesHandle, 'off')
title(AxesHandle, ['q = ', num2str(q_pole)]);




% --- Executes on button press in CB_Visualize.
function CB_Visualize_Callback(hObject, eventdata, handles)
% hObject    handle to CB_Visualize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of CB_Visualize
if get(hObject, 'Value')
    set(handles.HGVisualize, 'Visible', 'on')
else
    set(handles.HGVisualize, 'Visible', 'off')
end
