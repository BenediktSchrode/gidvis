function [qStep, YAxLabel, Filter] = MultiPowderPlotDialog(FileNames)
% This function is used to set the settings for the Powder Plot when
% multiple files have been selected. The q step size, the y-axis label
% properties and filters can be set.
%
% Usage:
%     [qStep, YAxLabel, Filter] = MultiPowderPlotDialog(FileNames, Directory)
%
% Input:
%     FileNames ... LFMElement vector
%
% Output:
%     qStep ... step size of q
%     YAxLabel ... structure with up to two fields: (1) Type and (2) Value.
%                  Type gives what kind of y-axis label is requested. It
%                  can be 'FileNumber', 'FileProperty' of 'FileName'. In
%                  case Type is 'FileProperty' or 'FileName', the field
%                  'Value' gives the name of the property or additional
%                  regex string respectively.
%     Filter ... structure with two fields: (1) qxy and (2) qz. The fields
%                contain the string description of how to filter.
%
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode                                %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% pre-assign output arguments
qStep = [];
YAxLabel = [];
Filter = [];

% read properties of first image
[~, Props] = ReadDataFromFile(FileNames(1));
Props1 = Props{1};
if isempty(Props1)
    PropertyList = {};
else
    [~, ind] = sort(lower(Props1(:, 1)));
    PropertyList = Props1(ind, 1);
end

% create dialog window
d = dialog('Name', 'Input', 'Resize', 'on');
% adapt size of window
P = get(d, 'Position');
set(d, 'Position', [P(1) P(2) 250 395]);

UIP = uipanel('Parent', d, 'Units', 'pixels', 'Position', [20 335 210 55], ...
    'Title', 'Enter q step size:', 'FontSize', 10);
txt_qStep = uicontrol('Parent', UIP, 'Style', 'edit', 'Position', [6 6 198 25], ...
    'String', 0.005, 'FontSize', 10, 'HorizontalAlignment', 'left');

RBGroup = uibuttongroup('Parent', d, 'Units', 'pixels', 'Position', [20 120 210 205], ...
    'Title', 'Y-axis labelling:', 'FontSize', 10);
RB_FileNumber = uicontrol('Parent', RBGroup, 'Style', 'radiobutton', ...
    'Units', 'pixels', 'Position', [6 160 210 25], ...
    'String', 'File Number', 'FontSize', 10, 'Tag', 'RB_FileNumber');
RB_Property = uicontrol('Parent', RBGroup, 'Style', 'radiobutton', ...
    'Units', 'pixels', 'Position', [6 135 210 25], ...
    'String', 'File Property', 'FontSize', 10, 'Tag', 'RB_Property');
CB_Property = uicontrol('Parent', RBGroup, 'Style', 'popupmenu', ...
    'Units', 'pixels', 'Position', [24 110 175 25], 'FontSize', 10);
if ~isempty(PropertyList)
    set(CB_Property, 'String', PropertyList);
else
    set(CB_Property, 'String', 'No Properties Found.');
end
RB_FileName = uicontrol('Parent', RBGroup, 'Style', 'radiobutton', ...
    'Units', 'pixels', 'Position', [6 80 210 25], ...
    'String', 'File (Display) Name', 'FontSize', 10, 'Tag', 'RB_FileName');
uicontrol('Parent', RBGroup, 'Style', 'text', 'Units', 'pixels', ...
    'Position', [24 51 60 25], 'String', 'Regex:', 'FontSize', 10, ...
    'HorizontalAlignment', 'left');
txt_Regex = uicontrol('Parent', RBGroup, 'Style', 'edit', ...
    'Units', 'pixels', 'Position', [85 55 114 25], 'String', '([\d]+)\.', ...
    'HorizontalAlignment', 'left', 'FontSize', 10, ...
    'Callback', @(src, evt) ApplyRegex());
txt_Preview = uicontrol('Parent', RBGroup, 'Style', 'text', 'Units', 'pixels', ...
    'Position', [24 25 175 25], 'String', 'Preview:', 'FontSize', 10, ...
    'HorizontalAlignment', 'left');
uicontrol('Parent', RBGroup, 'Style', 'pushbutton', 'Units', 'pixels', ...
    'Position', [24 5 175 25], 'String', 'Open regex101.com', ...
    'FontSize', 10, 'Callback', @(src, evt) OpenRegexWebsite);

UIP = uipanel('Parent', d, 'Units', 'pixels', 'Position', [20 55 210 55], ...
    'Title', 'Filtering:', 'FontSize', 10);
qzFilter = uicontrol('Style', 'popup', 'Parent', UIP, 'Units', 'pixels', ...
    'String', {'q_z >= 0', 'q_z <= 0', 'all q_z'}, ...
    'Position', [6 6 75 25]);
uicontrol('Style', 'text', 'Parent', UIP, 'Units', 'pixels', ...
    'String', 'and', 'Position', [86 2 38 25], 'FontSize', 10);
qxyFilter = uicontrol('Style', 'popup', 'Parent', UIP, 'Units', 'pixels', ...
    'String', {'all q_xy', 'q_xy >= 0', 'q_xy <= 0'}, ...
    'Position', [124 6 75 25]);

% create Cancel and OK buttons
uicontrol('Parent', d, 'Style', 'pushbutton', 'Position', [20 20 100 25], ...
    'String', 'OK', 'FontSize', 10, 'Callback', @OK_Callback);
uicontrol('Parent', d, 'Style', 'pushbutton', 'Position', [130 20 100 25], ...
    'String', 'Cancel', 'FontSize', 10, 'Callback', @Cancel_Callback);

set(RBGroup, 'SelectionChangedFcn', @RBChanged);

evt.NewValue = RB_FileNumber;

RBChanged(RBGroup, evt);
ApplyRegex();

Ch = findobj(d, '-not', 'Type', 'figure');
set(Ch, 'Units', 'normalized');

% block execution until the dialog is closed
uiwait(d)

function OK_Callback(src, evt)
    % callback for the OK button
    qStep = get(txt_qStep, 'String');
    if get(RB_FileNumber, 'Value')
        YAxLabel.Type = 'FileNumber';
        YAxLabel.Value = '';
    elseif get(RB_Property, 'Value')
        YAxLabel.Type = 'FileProperty';
        YAxLabel.Value = PropertyList{get(CB_Property, 'Value')};
    elseif get(RB_FileName, 'Value')
        YAxLabel.Type = 'FileName';
        YAxLabel.Value = get(txt_Regex, 'String');
    end
    Values1 = get(qzFilter, 'String');
    Values2 = get(qxyFilter, 'String');
    Filter.qxy = Values2{get(qxyFilter, 'Value')};
    Filter.qz = Values1{get(qzFilter, 'Value')};
    
    delete(d);
end

function Cancel_Callback(src, evt)
    % callback for the Cancel button
    qStep = [];
    YAxLabel = [];
    Filter = [];
    delete(d);
end

function RBChanged(src, evt)
    switch get(evt.NewValue, 'Tag')
        case 'RB_FileNumber'
            set([CB_Property txt_Regex], 'Enable', 'off');
        case 'RB_Property'
            set(CB_Property, 'Enable', 'on');
            set(txt_Regex, 'Enable', 'off');
        case 'RB_FileName'
            set(txt_Regex, 'Enable', 'on');
            set(CB_Property, 'Enable', 'off');
    end
    if isempty(PropertyList)
        set(RB_Property, 'Enable', 'off');
    end
end

function ApplyRegex()
    FN = FileNames(1).DisplayName;
    Exp = get(txt_Regex, 'String');
    R = regexp(FN, Exp, 'tokens');
    if isempty(R)
        set(txt_Preview, 'String', 'Preview: <no match>');
    else
        R1 = R{1};
        set(txt_Preview, 'String', sprintf('Preview: %s', strjoin(R1)));
    end
end

function OpenRegexWebsite()
    RegExp = urlencode(get(txt_Regex, 'String'));
    FNs = {FileNames.DisplayName};
    TestStrings = urlencode(strjoin(FNs(1:min([10 numel(FNs)])), '\r\n'));
    
    FinalUrl = sprintf('https://regex101.com/?regex=%s&testString=%s', RegExp, TestStrings);
    
    web(FinalUrl, '-browser');
end

end