function [powder_intensity, q_powder, xxBin] = PowderCalcFast(points, q_data, intensity)
%POWDERCALCFAST similar to the regrid function, but in 1D only
%   a plot q vs. Intensity is calculated
%
% Usage:
%     [powder_intensity, q_powder, xxBin] = PowderCalcFast(points, q_data, intensity)
%
% Input:
%     points ... scalar value determining how many points the x-axis should
%                have OR vector of x values, where the data should be
%                binned to
%     q_data ... the corresponding q value for every intensity value
%     intensity .... intensity map
%
% Output:
%     powder_intensity ... the intensity values
%     q_powder ... the q values
%     xxBin ... indices of the bins the data point falls into
%
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

xx = q_data;
zz = intensity;

if numel(points) == 1 && floor(points) == ceil(points)
    minX = min(xx);
    maxX = max(xx);

    targetSize = [points,1];
    q_powder = linspace(minX, maxX, points)';
else
    minX = min(points);
    maxX = max(points);
    
    targetSize = [numel(points), 1];
    q_powder = points;
end

if targetSize(1) == 1
    q_powder = (maxX-minX)/2+minX;
end

if minX == maxX
    xxBin = ones(size(xx));
else
    xxBin = round( (xx-minX)/(maxX-minX)*(targetSize(1)-1) ) +1;
end
xxBin(xxBin > targetSize(1)) = targetSize(1);
xxBin(xxBin < 1) = 1;

% map by using accumarray, take the mean of each bin 
powder_intensity = accumarray(xxBin(:),zz,targetSize)./accumarray(xxBin(:),1,targetSize);
end

