function PowderPlot(Figure, FigTB, AxesHandle, LoadFileModuleHandle, ToolBoxModuleHandle, SeparatorOnOff)
% This function adds a toggle button to the figure toolbar given by FigTB.
% This button makes a Linescan over the whole image (Powder Simulation).
%
% Usage:
%     PowderPlot(Figure, FigTB, AxesHandle, LoadFileModuleHandle, TBxUI, SeparatorOnOff)
%
% Input:
%     Figure ... handle of the main GIDVis figure
%     FigTB ... handle of the figure toolbar where the button should be
%               added to
%     AxesHandle ... handle of the main GIDVis axes
%     TBxUI ... UI element of the ToolboxModule
%     SeparatorOnOff ... string ('on' or 'off') defining whether a
%                        separator should be drawn left of the toggle tool
%                        or not
%
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Load the image for the push button
Img = imread('Powder.png', 'BackgroundColor', [1 1 1]);

% to make sure we add the ui control at the correct position in the
% toolbar, update the graphics
drawnow;

% add the uisplittool to the toolbar
SplitTool = uisplittool(FigTB, ...
    'ClickedCallback', @(src, evt) PowderPlotOn(src, evt, AxesHandle, LoadFileModuleHandle, ToolBoxModuleHandle), ...
    'Tag', 'PowderPlotButton', ...
    'Separator', SeparatorOnOff, ...
    'CData', Img, ...
    'TooltipString', 'LineScan over whole Image');
jSplitTool = get(SplitTool, 'JavaContainer');
drawnow(); % required to use get/set on jSplitTool
jMenu = get(jSplitTool, 'MenuComponent');
jCurrent = jMenu.add('Current File');
jSelected = jMenu.add('All Selected Files');
set(jCurrent, 'ActionPerformedCallback', {@PowderPlotOn, AxesHandle, LoadFileModuleHandle, ToolBoxModuleHandle});
set(jSelected, 'ActionPerformedCallback', {@MultiPowderPlot, AxesHandle, LoadFileModuleHandle, ToolBoxModuleHandle});


function MultiPowderPlot(src, evt, AxesHandle, LoadFileModuleHandle, ToolBoxModuleHandle)
% executes when All Selected Files gets pushed

% get the handles structure of the LoadFileModule
LFMHandles = guidata(LoadFileModuleHandle);

% get selected files
Elements = LoadFileModule('GetSelectedFilePath', LoadFileModuleHandle);
if any(strcmp({Elements.Type}, 'dir'))
    msgbox('At least one directory is contained in the selection. Please select only files!', 'modal')
    return;
end

% get the number of selected files
NumSel = numel(Elements);

% ask for the settings to use for the multi powder plot
[stepsizeS, YAxLabel, Filter] = MultiPowderPlotDialog(Elements);
if isempty(stepsizeS); return; end
stepsize = str2double(stepsizeS);
if isnan(stepsize); return; end

% update status label
set(LFMHandles.TB_Status, 'String', sprintf('Reading file 1 of %d.', NumSel));
drawnow;

% get selected beamline
beamline = LoadFileModule('GetBeamline', LoadFileModuleHandle);

% get handles structure of toolbox module
GD = guidata(LFMHandles.MainAxes);
GDTBx = guidata(GD.ToolboxModule);
omega = ToolboxModule('GetOmega', GD.ToolboxModule); % get omega
chi = str2double(get(GDTBx.TB_chi, 'String')); % and chi

% prepare result plot (imagesc)
f = figure('Visible', 'off');
ax = subplot(1, 2, 1, 'Parent', f);
imgH = imagesc(ax, 'XData', [], 'YData', [], 'CData', []);
% labels
xlabel(ax, 'q (A^{-1})')
ylabel(ax, 'File Number');

% make a cell holding the y-tick labels
YTickLabels = cell(numel(Elements), 1);

% loop over selected files
for iF = 1:numel(Elements)
    % update status label
    set(LFMHandles.TB_Status, 'String', sprintf('Reading file %d of %d.', iF, NumSel));
    drawnow;
    
    switch Elements(iF).Ext
        case '.GIDDat'
            Q = qData(Elements(iF).Path);

            [qxy, qz, ~, ~, q] = getQ(Q, omega, chi);
            data = CorrectQData(GDTBx.Figure_ToolboxModule, Q);
        otherwise
            [data, header] = ReadDataFromFile(Elements(iF));
            data = CorrectPixelData(GDTBx.Figure_ToolboxModule, beamline, data);
            
            % if requested as the y-axis label, extract the value of a
            % property
            if strcmp(YAxLabel.Type, 'FileProperty')
                % find property value and store it
                if ~isempty(header)
                    header = header{1};
                    Props = header(:, 1);
                    L = strcmp(Props, YAxLabel.Value);
                    if any(L)
                        YTickLabels{iF} = header{L, 2};
                    end
                end
            end
            
            % get the dimensions of the current file
            sz = size(data);
            % check if the data is large enough compared to the detector
            if sz(1) < beamline.detlenz || sz(2) < beamline.detlenx
                close(f);
                msgbox({'The detector of the currently selected experimental set up is larger than the dimension of the image file.', ...
                    'Make sure you have selected the correct experimental set up.'}, 'modal');
                return;
            end
            
            % crop the data according to the beamline parameters
            data = data(1:beamline.detlenz, 1:beamline.detlenx);
            data = data(:);
            
            % lets get some q values
            [qxy, qz, ~, ~, q] = CalculateQ(beamline, omega, chi);
    end
    
    if strcmp(YAxLabel.Type, 'FileName')
        % extract part of file name and store it
        R = regexp(Elements(iF).DisplayName, YAxLabel.Value, 'tokens');
        if ~isempty(R)
            YTickLabels{iF} = strjoin(R{1});
        end
    elseif strcmp(YAxLabel.Type, 'FileNumber')
        % store the file number for later during export
        YTickLabels{iF} = iF;
    end
    
    % create filters for qxy
    switch Filter.qxy
        case 'q_xy >= 0'
            Lxy = qxy >= 0;
        case 'q_xy <= 0'
            Lxy = qxy <= 0;
        case 'all q_xy'
            Lxy = true(size(qxy));
        otherwise
            error('GIDVis:PowderPlot:UnknownFilter', 'Unknown qxy filter.')
    end
    % create filters for qz
    switch Filter.qz
        case 'q_z >= 0'
            Lz = qz >= 0;
        case 'q_z <= 0'
            Lz = qz <= 0;
        case 'all q_z'
            Lz = true(size(qz));
        otherwise
            error('GIDVis:PowderPlot:UnknownFilter', 'Unknown qz filter.')
    end
    
    % apply filters for both qxy and qz to data and the corresponding q
    % values
    data = data(Lxy & Lz);
    q = q(Lxy & Lz);
    
    % remove intensity values which are NaN and the corresponding q values
    L = isnan(data) | data < 0;
    data(L) = [];
    q(L) = [];
    
    if iF == 1 % we're using the q values of the first image for all other images as well
        % generate an x-axis out of the q values
        x_data = min(q):stepsize:max(q);
        set(f, 'Visible', 'on')
    end
    
    % calculate powder pattern
    % use x_data to regrid all the data points onto the same q vector (the
    % one from the first image)
    [powder_intensity, q_powder] = PowderCalcFast(x_data, q, data);
    
    % plot the powder pattern together with the old data
    if isvalid(imgH)
        set(imgH, 'XData', q_powder, 'YData', 1:iF, ...
            'CData', [get(imgH, 'CData'); powder_intensity']);
    else
        set(LFMHandles.TB_Status, 'String', 'Process aborted.');
        return;
    end
end

uicontrol('Parent', f, 'Style', 'pushbutton',  'String', 'Export Data', ...
    'Units', 'normalized', 'Position', [0.1322 0.93 0.2 0.06], ...
    'Callback', @(src, evt) ExportMultiPowderPlotData(imgH, Files(SelInd), YTickLabels, YAxLabel), ...
    'FontSize', 10);

% update status label
set(LFMHandles.TB_Status, 'String', 'Process completed.');
drawnow;

% make waterfall plot
ax2 = subplot(1, 2, 2, 'Parent', f);
waterfall(ax2, get(imgH, 'XData'), get(imgH, 'YData'), get(imgH, 'CData'));
xlabel(ax2, 'q (A^{-1})');
ylabel(ax2, 'File Number');
zlabel(ax2, 'Intensity');

% deal with axis labels
if strcmp(YAxLabel.Type, 'FileProperty') || strcmp(YAxLabel.Type, 'FileName')
    % store old y-axis limits
    YLims = ylim(ax);
    % make sure the y-axis starts at 0
    ylim(ax, [0 YLims(2)]);
    % get the y-ticks
    YTicks = get(ax, 'YTick');
    YTicks = unique([1 YTicks numel(Elements)]);
    set(ax, 'YTick', YTicks);
    L = YTicks > 0 & YTicks <= numel(Elements) & (ceil(YTicks) == YTicks);
    set(ax, 'YTick', YTicks(L), 'YTickLabels', YTickLabels(YTicks(L)));
    
    % store old y-axis limits
    YLims = ylim(ax2);
    % make sure the y-axis starts at 0
    ylim(ax2, [0 YLims(2)]);
    % get the y-ticks
    YTicks = get(ax2, 'YTick');
    YTicks = unique([1 YTicks numel(Elements)]);
    set(ax2, 'YTick', YTicks);
    L = YTicks > 0 & YTicks <= numel(Elements) & (ceil(YTicks) == YTicks);
    set(ax2, 'YTick', YTicks(L), 'YTickLabels', YTickLabels(YTicks(L)));
    if strcmp(YAxLabel.Type, 'FileProperty')
        ylabel(ax, YAxLabel.Value, 'Interpreter', 'none');
        ylabel(ax2, YAxLabel.Value, 'Interpreter', 'none');
    else
        ylabel(ax, '');
        ylabel(ax2, '');
    end
end

end

function ExportMultiPowderPlotData(PlotHandle, Elements, YTickLabels, YAxLabel)
x = get(PlotHandle, 'XData');
z = get(PlotHandle, 'CData')';
x = x(:);

Path = Elements(1).Path;

M = [x z];

[FileName, PathName] = uiputfile({'*.txt', 'Text File (*.txt)'}, 'Save ...', fileparts(Path));

if isequal(FileName, 0) || isequal(PathName, 0)
    return;
end

fid = fopen(fullfile(PathName, FileName), 'w');
% prepare string
if strcmp(YAxLabel.Type, 'FileProperty') || strcmp(YAxLabel.Type, 'FileName')
    fprintf(fid, 'Axis Values (%s, %s):\t', YAxLabel.Type, YAxLabel.Value);
else
    fprintf(fid, 'Axis Values (File Number):\t');
end
if isnumeric(YTickLabels{1})
    fprintf(fid, '%g\t', YTickLabels{:});
elseif ischar(YTickLabels{1})
    fprintf(fid, '%s\t', YTickLabels{:});
end
fprintf(fid, '\n');
fprintf(fid, repmat('-', 1, 50));
fprintf(fid, '\n');
fprintf(fid, 'q\t');
FileNames = {Elements.DisplayName};
fprintf(fid, '%s\t', FileNames{:});
fprintf(fid, '\n');
for iR = 1:size(M, 1)
    fprintf(fid, '%g\t', M(iR, :));
    fprintf(fid, '\n');
end
fclose(fid);
end


function PowderPlotOn(src, evt, AxesHandle, LoadFileModuleHandle, ToolBoxModuleHandle)
% executes when the uisplittool or Current File gets pushed

% find the MainImage in the AxesHandle
hMainImage = findobj(AxesHandle, 'Tag', 'MainImage');
if isempty(hMainImage) %if we have something to integrate...
    return;
end
% contents = get(LoadFileData.LB_Files, 'String');
% filename = contents{get(LoadFileData.LB_Files, 'Value')};
% [~, ~, Ext] = fileparts(filename);
Element = LoadFileModule('GetSelectedFilePath', LoadFileModuleHandle);
MainGIDVisHandle = guidata(AxesHandle);

switch Element(1).Ext
    case '.GIDDat'
        Q = MainGIDVisHandle.RawData;
        
        ToolboxData = guidata(ToolBoxModuleHandle); % from the ToolboxModule we
        omega = ToolboxModule('GetOmega', ToolBoxModuleHandle); % get omega
        chi = str2double(get(ToolboxData.TB_chi, 'String')); % and chi
        
        [qxy, qz, ~, ~, q] = getQ(Q, omega, chi);
        data = MainGIDVisHandle.CorrectedData;
        
        % remove intensity values which are NaN and the corresponding q
        % values
        L = isnan(data);
        data(L) = [];
        q(L) = [];
        qxy(L) = [];
        qz(L) = [];
    otherwise
        beamline = LoadFileModule('GetBeamline', LoadFileModuleHandle);
        data = MainGIDVisHandle.CorrectedData;
        % get the dimensions of the current file
        sz = size(data);
        % check if the data is large enough compared to the detector
        if sz(1) < beamline.detlenz || sz(2) < beamline.detlenx
            msgbox({'The detector of the currently selected experimental set up is larger than the dimension of the image file.', ...
                'Make sure you have selected the correct experimental set up.'}, 'modal');
            return;
        end
        % crop the data according to the beamline parameters
        data = data(1:beamline.detlenz, 1:beamline.detlenx);
        data = data(:);
        
        % get intensity values < 0 (dead area/pixels)
        L = isnan(data) | data < 0;
        data(L) = [];

        ToolboxData = guidata(ToolBoxModuleHandle); % from the ToolboxModule we
        omega = ToolboxModule('GetOmega', ToolBoxModuleHandle); % get omega
        chi = str2double(get(ToolboxData.TB_chi, 'String')); % and chi

        % lets get some q values
        [qxy, qz, ~, ~, q] = CalculateQ(beamline, omega, chi);
        % remove the q values corresponding to dead area/pixels
        q(L) = [];
        qz(L) = [];
        qxy(L) = [];
end

%define the stepsize of the plot
stepsize = 0.005;

%we generate a x-axis out of the q values
x_data = min(q):stepsize:max(q);
numx = numel(x_data);

% Create a figure and axes
pp = figure('Visible', 'off', 'NumberTitle', 'off');
set(pp, 'Name', ['Powder Plot: ', Element(1).DisplayName]);
ppax = axes('Units','normalized');
set(ppax, 'Position', [.13 .11 .775 .715])
powderplot = plot(NaN, NaN);
xlabel(ppax, 'q');
ylabel(ppax, 'Intensity');

% Create slider and label for changing the number of x-axis data points
sld = uicontrol('Style', 'slider', 'Min', 1, 'Max', numx*2, ...
    'Value', numx, 'Units', 'normalized', ...
    'Position', [0.3 0.95 0.3 0.04], ...
    'Callback', @(src, evt) UpdateDataPlot); 
uicontrol('Style', 'text', 'Units', 'normalized', ...
    'Position', [0.03 0.935 0.25 0.04], 'HorizontalAlignment', 'left', ...
    'String', 'number of x-axis data points');

% create popupmenu to change between q and Two Theta on the x-axis
XAxisValue = uicontrol('Style', 'popup', 'Units', 'normalized', ...
    'String', {'q', 'Two Theta (Cu Ka1)', 'Two Theta (Cr Ka1)'}, ...
    'Position', [0.77 0.95 0.22 0.04], ...
    'Callback', @(src, evt) UpdateDataPlot);
uicontrol('Style','text', 'Units','normalized', ...
    'Position', [0.65 0.935 0.11 0.04],...
    'HorizontalAlignment', 'left', 'String', 'x-axis value');

% create ui elements for filtering
uicontrol('Style', 'text', 'Units', 'normalized', ...
    'HorizontalAlignment', 'left', ...
    'Position', [0.03 0.885 0.25 0.04], ...
    'String', 'Use only data points with ');
qzFilter = uicontrol('Style', 'popup', ...
    'String', {'q_z >= 0', 'q_z <= 0', 'all q_z'}, 'Units', 'normalized', ...
    'Position', [0.3 .9 .145 .04], 'Callback', @(src, evt) UpdateDataPlot);
qxyFilter = uicontrol('Style', 'popup', ...
    'String', {'all q_xy', 'q_xy >= 0', 'q_xy <= 0'}, ...
    'Units', 'normalized', 'Position', [0.455 .9 .145 .04], ...
    'Callback', @(src, evt) UpdateDataPlot);

% create UI element for showing the fit
CB_Fit = uicontrol('Style', 'pushbutton', 'Units', 'normalized', ...
    'Position', [0.65 0.885 0.11 0.05], 'String', 'Show Fit', ...
    'Callback', @(src, evt) ShowFitCallback);

AddExportLineDataButton(pp);

% call the UpdateDataPlot function to update the data and the plot
UpdateDataPlot()

% Make figure visble after adding all components and the plot update
set(pp, 'Visible', 'on');

function ShowFitCallback
    % function to handle change in the checkbox to show the fit
    set(CB_Fit, 'Enable', 'off');
    drawnow;
    FittingWithCustomBorders(powderplot, [], @OneDimGaussianFit);
end

function UpdateDataPlot
    % calculate the powder pattern with the requested number of x-axis
    % values
    val = round(get(sld,'Value'));
    
    % evaluate the filters
    values = get(qzFilter, 'String');
    ind = get(qzFilter, 'Value');
    switch values{ind}
        case 'q_z >= 0'
            Lqz = qz >= 0;
        case 'q_z <= 0'
            Lqz = qz <= 0;
        case 'all q_z'
            Lqz = true(size(qz));
        otherwise
            error('Unknown option.')
    end
    values = get(qxyFilter, 'String');
    ind = get(qxyFilter, 'Value');
    switch values{ind}
        case 'q_xy >= 0'
            Lqxy = qxy >= 0;
        case 'q_xy <= 0'
            Lqxy = qxy <= 0;
        case 'all q_xy'
            Lqxy = true(size(qxy));
        otherwise
            error('Unknown option.')
    end
    
    if sum(Lqz) == 0 || sum(Lqxy) == 0
        powder_intensity = NaN;
        q_powder = NaN;
        msgbox('No data points left after filtering.', 'modal')
    else
        [powder_intensity, q_powder] = PowderCalcFast(val, q(Lqz & Lqxy), data(Lqz & Lqxy));
    end
    
    % transform q to 2Theta if necessary
    Options = get(XAxisValue, 'String');
    Val = get(XAxisValue, 'Value');
    switch Options{Val}
        case 'q'
            % we don't have to transform anything, just put the q_powder
            % into a new variable
            xToPlot = q_powder;
            % create label
            xlabelString = [Options{Val} ' (' char(197) '^{-1})'];
        case 'Two Theta (Cu Ka1)'
            % transform q data to Two Theta using wavelength of the copper
            % K alpha line
            xToPlot = 2*asind(1.54.*q_powder./(4*pi));
            % create label
            xlabelString = [Options{Val} ' (degree)'];
        case 'Two Theta (Cr Ka1)'
            % transform q data to Two Theta using wavelength of the copper
            % K alpha line
            xToPlot = 2*asind(2.29.*q_powder./(4*pi));
            % create label
            xlabelString = [Options{Val} ' (degree)'];
        otherwise
            error('Unknown option.')
    end
    
    set(powderplot, 'XData', xToPlot, 'YData', powder_intensity);
    
    % update the x-axis label
    xlabel(ppax, xlabelString);
    
    % set the displayname (will be used in legend and also when exporting
    % the line data)
    set(powderplot, 'DisplayName', ['Powder ', Options{Val}]);
end
end
end