function LoadProject(WorkingPath, ToolboxModuleHandle, MainAxesHandle, LoadFileModuleHandle)
% LoadProject lets user select a project to load, loads the data and
% applies it to GIDVis.
%
% Usage:
%     LoadProject(WorkingPath, TBxUI, MainAxesHandle, LFMUI)
%
% Input:
%     WorkingPath ... the GIDVis root directory
%     TBxUI ... UI element of the toolbox module
%     MainAxesHandle ... handle of the main GIDVis axes
%     LFMUI ... UI element of the Load File module
%
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

imgHandle = findobj(MainAxesHandle, 'Tag', 'MainImage');
if isempty(imgHandle) % occurs e.g. when no GID image is loaded yet
    msgbox('Please select a GID image in the "Load File" window (File - Select Folder) before loading a project.', 'Cannot Load Project', 'modal');
    return;
end

% the settings for this module part are stored in the same directory as the 
% file LoadProject.m. Read out this settings file.
Path = fullfile(fileparts(mfilename('fullpath')), 'SettingsLoadProject.mat');
if exist(Path, 'file')
    S = load(Path);
    if isfield(S, 'SettingsLoadProject')
        SettingsLoadProject = S.SettingsLoadProject;
    else
        SettingsLoadProject.DefaultLoadPath = WorkingPath;
    end
else
    SettingsLoadProject.DefaultLoadPath = WorkingPath;
end

% find project to load
[FileName, PathName] = uigetfile({'*.GIDzip', 'GIDVis Project File'}, 'Select Project to Load', SettingsLoadProject.DefaultLoadPath, 'MultiSelect', 'off');
if isequal(FileName, 0) || isequal(PathName, 0)
    return;
end
DefaultLoadPath = fullfile(PathName, FileName);

% we have to unzip the selected project folder. We use the tempdir for
% this.
temp = tempdir;
if isempty(temp); temp = WorkingPath; end
[~, FileNameWOExt] = fileparts(FileName);
Files = unzip(fullfile(PathName, FileName), temp);

% find the number of additional files added to the project. They are files
% with extensions other than .Annotation, .Crystal, .Beamtime, .AddSet
KnownExts = {'.Annotation', '.Crystal', '.Beamtime', '.AddSet'};
FileExts = cellfun(@GetFileExtension, Files, 'UniformOutput', false);
for iC = 1:numel(KnownExts)
    L = strcmp(FileExts, KnownExts{iC});
    FileExts(L) = [];
end
NumOthers = numel(FileExts);

if NumOthers > 0
    % ask user where to save additional files to
    OthersPath = uigetdir('', 'Select Directory for Additional Files');
    if isequal(OthersPath, 0)
        return;
    end
end

FoundAdditionalSettings = false;

% loop over all files from the unzipped project file
for iF = 1:numel(Files)
    File = Files{iF};
    [~, filename, Ext] = fileparts(File);
    
    switch Ext
        case '.Annotation'
            % path where the annotation files are stored
            AnnotationPath = fullfile(WorkingPath, 'Annotations');
            % check for existence of directory, if not available create it
            if ~exist(AnnotationPath, 'dir'); mkdir(AnnotationPath); end
            if exist(fullfile(AnnotationPath, [filename, '.mat']), 'file')
                button = questdlg(sprintf('Annotation file ''%s'' already exists. Do you want to overwrite it?', filename), ...
                    'Overwrite File?', 'Yes', 'No', 'Yes');
                if strcmp(button, 'No')
                    continue;
                end
            end
            % move the file from the temp dir to its destination
            movefile(File, fullfile(WorkingPath, 'Annotations', [filename, '.mat']));
        case '.Crystal'
            % directory where the crystals are stored
            CrystalPath = fullfile(WorkingPath, 'Crystals');
            % check for existence of directory, if not available create it
            if ~exist(CrystalPath, 'dir'); mkdir(CrystalPath); end
            if exist(fullfile(CrystalPath, [filename, '.mat']), 'file')
                button = questdlg(sprintf('Crystal file ''%s'' already exists. Do you want to overwrite it?', filename), ...
                    'Overwrite File?', 'Yes', 'No', 'Yes');
                if strcmp(button, 'No')
                    continue;
                end
            end
            % move the file from the temp dir to its destination
            movefile(File, fullfile(WorkingPath, 'Crystals', [filename, '.mat']));
        case '.Beamtime'
            % directory where the beamtimes are stored
            BeamtimePath = fullfile(WorkingPath, 'Beamtimes');
            % check for existence of directory, if not available create it
            if ~exist(BeamtimePath, 'dir'); mkdir(BeamtimePath); end
            if exist(fullfile(BeamtimePath, [filename, '.mat']), 'file')
                button = questdlg(sprintf('Beamtime file ''%s'' already exists. Do you want to overwrite it?', filename), ...
                    'Overwrite File?', 'Yes', 'No', 'Yes');
                if strcmp(button, 'No')
                    continue;
                end
            end
            % move the file from the temp dir to its destination
            movefile(File, fullfile(WorkingPath, 'Beamtimes', [filename, '.mat']));
        case '.AddSet'
            % we are dealing with the AdditionalSettings file
            S = load(File, '-mat'); % load it and check for presence of variable SaveOptions
            if ~isfield(S, 'SaveOptions')
                continue;
            end
            FoundAdditionalSettings = true;
        otherwise
            % file is an additionally added file, move it to the directory
            % the user selected before
            movefile(File, fullfile(OthersPath, [filename, Ext]));
    end
end

if FoundAdditionalSettings
    SO = S.SaveOptions;
    
    % update the Beamtime popup menu in the LoadFileModule in case a new
    % beamtime was added by loading the project file
    LoadFileModule('PopulateBeamtimePopupBox', LoadFileModuleHandle);
    
    % get the handles structure of the LoadFileModule
    GD = guidata(LoadFileModuleHandle);
    % set the values in the LoadFileModule according to the values
    % in SaveOptions
    contents = get(GD.BeamtimeSelection, 'String');
    Ind = find(strcmp(contents, SO.Beamtime));
    if ~isempty(Ind)
        set(GD.BeamtimeSelection, 'Value', Ind)
    else
        warning('GIDVis:LoadProject:NoBeamtime', 'Could not find beamtime %s.', SO.Beamtime);
    end
    % update the beamline box
    LoadFileModule('UpdateSelectedBeamtime', LoadFileModuleHandle);

    % set the beamline
    contents = get(GD.BeamlineSelection, 'String');
    Ind = find(strcmp(contents, SO.Beamline));
    if ~isempty(Ind)
        set(GD.BeamlineSelection, 'Value', Ind)
    else
        warning('GIDVis:LoadProject:NoBeamline', 'Could not find beamline %s.', SO.Beamline);
    end

    % get the handles structure of the ToolboxModule
    GD = guidata(ToolboxModuleHandle);
    % set the values in the ToolboxModule according to the values
    % in SaveOptions
    set(GD.TB_ColorLimMin, 'String', num2str(SO.ColorLimits(1)));
    set(GD.TB_ColorLimMax, 'String', num2str(SO.ColorLimits(2)));
    set(GD.TB_HorizontalMin, 'String', num2str(SO.XLim(1)));
    set(GD.TB_HorizontalMax, 'String', num2str(SO.XLim(2)));
    set(GD.TB_VerticalMin, 'String', num2str(SO.YLim(1)));
    set(GD.TB_VerticalMax, 'String', num2str(SO.YLim(2)));
    if isfield(SO, 'alpha_i')
        % the omega was once called alpha_i, handle this case
        set(GD.TB_omega, 'String', num2str(SO.alpha_i));
    elseif isfield(SO, 'omega')
        set(GD.TB_omega, 'String', num2str(SO.omega));
    end
    set(GD.TB_chi, 'String', num2str(SO.chi));
    set(GD.TB_RegridResolution1, 'String', num2str(SO.RegriddingResolution(1)));
    set(GD.TB_RegridResolution2, 'String', num2str(SO.RegriddingResolution(2)));
    contents = get(GD.PM_Scaling, 'String');
    Ind = find(strcmp(contents, SO.IntensityScaling));
    if ~isempty(Ind); set(GD.PM_Scaling, 'Value', Ind); end
    contents = get(GD.PM_ColorMap, 'String');
    Ind = find(strcmp(contents, SO.ColorMap));
    if ~isempty(Ind); set(GD.PM_ColorMap, 'Value', Ind); end
    contents = get(GD.PM_AxesFormat, 'String');
    Ind = find(strcmp(contents, SO.AxesFormat));
    if ~isempty(Ind); set(GD.PM_AxesFormat, 'Value', Ind); end
    contents = get(GD.PM_Space, 'String');
    Ind = find(strcmp(contents, SO.Space));
    if ~isempty(Ind); set(GD.PM_Space, 'Value', Ind); end
    
    % correction check boxes: have to check for existence of the field in
    % the structure, since they were added later only
    if isfield(SO, 'LorentzCorrection')
        set(GD.CB_LorentzCorrection, 'Value', SO.LorentzCorrection);
    end
    if isfield(SO, 'SolidAngleCorrection')
        set(GD.CB_SolidAngleCorrection, 'Value', SO.SolidAngleCorrection);
    end
    if isfield(SO, 'PolarizationCorrection')
        set(GD.CB_PolarizationCorrection, 'Value', SO.PolarizationCorrection);
    end
    if isfield(SO, 'PixelDistanceCorrection')
        set(GD.CB_PixelDistanceCorrection, 'Value', SO.PixelDistanceCorrection);
    end
    if isfield(SO, 'DetectorEfficiencyCorrection')
        set(GD.CB_DetectorEfficiencyCorrection, 'Value', SO.DetectorEfficiencyCorrection);
    end
    if isfield(SO, 'FlatFieldCorrection')
        set(GD.CB_FlatFieldCorrection, 'Value', SO.FlatFieldCorrection);
    end
    
    % store the value of the LockColorScaling checkbox
    OldVal = get(GD.CB_LockColorScaling, 'Value');
    % set the LockColorScaling checkbox to checked. This is needed
    % because otherwise the color limits will be changed during
    % RequestPlotUpdateFromData
    set(GD.CB_LockColorScaling, 'Value', 1);

    % Update the plot
    RequestPlotUpdateFromData(MainAxesHandle)
    % restore the old value of the LockColorScaling checkbox
    set(GD.CB_LockColorScaling, 'Value', OldVal);
    % set the axes limits (they were probably changed during
    % RequestPlotUpdateFromData)
    set(MainAxesHandle, 'XLim', SO.XLim);
    set(MainAxesHandle, 'YLim', SO.YLim);

    % set the color limits they were not set, only the values are
    % written in the corresponding textboxes
    if ~all(isnan(SO.ColorLimits))
        caxis(MainAxesHandle, SO.ColorLimits);
    end
end

rmdir(fullfile(temp, FileNameWOExt), 's');

% save settings
SettingsLoadProject.DefaultLoadPath = DefaultLoadPath; %#ok. Used for saving
save(Path, 'SettingsLoadProject');

function Ext = GetFileExtension(Path)
[~, ~, Ext] = fileparts(Path);