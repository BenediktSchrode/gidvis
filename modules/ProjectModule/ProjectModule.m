function varargout = ProjectModule(varargin)
% This file, together with ProjectModule.fig, is the Project Module of
% GIDVis.
%
% Usage:
%     varargout = ProjectModule(LFMUI, TBxUI, DirectoryPath, MainAxesHandle)
%
% Input:
%     LFMUI ... UI element of the Load File module
%     TBxUI ... UI element of the toolbox module
%     DirectoryPath ... the GIDVis root directory
%     MainAxesHandle ... handle of the main GIDVis axes
% 
% Output:
%    varargout ... default GUIDE output
%
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Last Modified by GUIDE v2.5 20-Oct-2017 11:56:56

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ProjectModule_OpeningFcn, ...
                   'gui_OutputFcn',  @ProjectModule_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ProjectModule is made visible.
function ProjectModule_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ProjectModule (see VARARGIN)

% Choose default command line output for ProjectModule
handles.output = hObject;

% store the LoadFileModule Handle in the handles structure
handles.LoadFileModule = varargin{1};

% store the ToolboxModule Handle in the handles structure
handles.ToolboxModule = varargin{2};

% store the path in the handles structure
handles.WorkingPath = varargin{3};

% we look for files in Annotaions, Beamtimes and Crystals directories.
% They will be shown in a table where user can select which files to
% include in the project.
D = cell(0, 4);
Dirs = {'Annotations', 'Beamtimes', 'Crystals'; ...
        '*.mat',       '*.mat',     '*.mat'};

for iD = 1:size(Dirs, 2)
    Dir = Dirs{1, iD};
    Ext = Dirs{2, iD};
    Files = dir(fullfile(handles.WorkingPath, Dir, Ext));
    if isempty(Files)
        continue;
    end
    D = [D; repmat({Dir(1:end-1)}, numel(Files), 1) {Files.name}' num2cell(false(numel(Files), 1)) repmat({fullfile(handles.WorkingPath, Dir)}, numel(Files), 1)];
end

% set the data of the table
set(handles.UIT_ElementsToInclude, 'Data', D, ...
    'ColumnEditable', [false, false, true, false], ...
    'ColumnName', {'Type', 'Name', 'Include?', 'Directory'}, ...
    'ColumnWidth', num2cell([100 200 100 0]));

% add a listener to the axes in varargin{4}. When the axes gets destroyed 
% (e.g. because the figure containing the axes gets closed), the callback 
% will be executed (and closes this window).
% Store the listener in the handles structure to remove it when this window    
% gets closed by user, otherwise the callback is executed even when the
% ProjectModule window does not exist anymore.
handles.MainAxesDestroyedListener = addlistener(varargin{4}, 'ObjectBeingDestroyed', @(src, evt) MainAxesDestroyed_Callback(src, evt, handles.Figure_ProjectModule));

% the settings for this module are stored in the same directory as the file
% ProjectModule.m. Store its path in the handles structure to reuse it.
handles.SettingsPath = fullfile(fileparts(mfilename('fullpath')), 'Settings.mat');

% read out settings if available
if exist(handles.SettingsPath, 'file') == 2
    load(handles.SettingsPath);
    % store the settings in the handles structure
    handles.Settings = SettingsProjectModule;
else % use some default
    handles.Settings = [];
    handles.Settings.ProjectSavePath = handles.WorkingPath;
    handles.Settings.AdditionalFilesLoadPath = handles.WorkingPath;
end

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ProjectModule wait for user response (see UIRESUME)
% uiwait(handles.Figure_ProjectModule);

function MainAxesDestroyed_Callback(src, evt, HandleToModuleWindow)
% close module window
close(HandleToModuleWindow);

% --- Outputs from this function are returned to the command line.
function varargout = ProjectModule_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if isfield(handles, 'Settings') && isfield(handles.Settings, 'WindowPosition')
    RestorePositionOrDefault(hObject, handles.Settings.WindowPosition);
end
set(hObject, 'Visible', 'on');

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in LB_FilesToInclude.
function LB_FilesToInclude_Callback(hObject, eventdata, handles)
% hObject    handle to LB_FilesToInclude (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns LB_FilesToInclude contents as cell array
%        contents{get(hObject,'Value')} returns selected item from LB_FilesToInclude


% --- Executes during object creation, after setting all properties.
function LB_FilesToInclude_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LB_FilesToInclude (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function Str = FileNameWithExtension(Path)
[~, Name, Ext] = fileparts(Path);
Str = [Name, Ext];

% --- Executes on button press in Btn_AddFile.
function Btn_AddFile_Callback(hObject, eventdata, handles)
% user can add custom files to the project. 

% Use uigetfile for selection.
[FileName, PathName] = uigetfile(fullfile(handles.Settings.AdditionalFilesLoadPath, '*.*'), 'Select Files to Include', 'MultiSelect', 'on');
if isequal(FileName, 0) || isequal(PathName, 0)
    return;
end

% store the path in the handles structure, for later reuse
handles.Settings.AdditionalFilesLoadPath = PathName;

% in case the listbox already contains files, store the old string
OldString = get(handles.LB_FilesToInclude, 'String');
% extract the filenames including extension
if isempty(OldString)
    OldFileNames = '';
else
    OldFileNames = cellfun(@FileNameWithExtension, OldString, 'UniformOutput', false);
end
% FileName has to be a cell
if ~iscell(FileName); FileName = {FileName}; end

% files with the same name, even from different directories, cannot be
% included. Check for same file names
Res = intersect(FileName, OldFileNames);
if numel(Res) > 0
    Str = '';
    for iR = 1:numel(Res)
        Str = [Str, ', ', Res{iR}];
        L = strcmp(FileName, Res{iR});
        FileName(L) = [];
    end
    msgbox(['Files with the same name but from different directories cannot be included in the project. ', ...
        'The following files are not added: ' Str(3:end)], 'modal');
end

% add the PathName to the FileName
FileName = cellfun(@(S) fullfile(PathName, S), FileName, 'UniformOutput', false);
% find out if user has selected files which are already contained in the
% listbox. Only files in ToAdd need to be added.
ToAdd = setdiff(FileName, OldString);
% concatenate the old and the new files
NewString = sort([OldString; ToAdd']);
% set the string of the listbox
set(handles.LB_FilesToInclude, 'String', NewString);

guidata(hObject, handles);

% --- Executes on button press in Btn_RemoveFile.
function Btn_RemoveFile_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_RemoveFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% remove the selected files from the listbox
Val = get(handles.LB_FilesToInclude, 'Value');
Data = get(handles.LB_FilesToInclude, 'String');
Data(Val) = [];
set(handles.LB_FilesToInclude, 'Value', 1); % avoids issues when Value > number of files in listbox
set(handles.LB_FilesToInclude, 'String', Data);

% --- Executes on button press in Btn_SaveProject.
function Btn_SaveProject_Callback(hObject, eventdata, handles)
% store the project.

% The idea is to create a zip file from the selected files.

% Let user choose place to save the project.
[FileName, PathName] = uiputfile({'*.GIDzip', 'GID zip file'}, 'Select Place to Save', handles.Settings.ProjectSavePath);
if isequal(FileName, 0) || isequal(PathName, 0)
    return;
end

% store the path in the handles structure for later re-use
handles.Settings.ProjectSavePath = fullfile(PathName, FileName);

% get the file name without extension
[~, FileNameWOExt] = fileparts(FileName);

% extract information from the Toolbox module
GD = guidata(handles.ToolboxModule);
contents = get(GD.PM_Scaling, 'String');
SaveOptions.IntensityScaling = contents{get(GD.PM_Scaling, 'Value')};
contents = get(GD.PM_ColorMap, 'String');
SaveOptions.ColorMap = contents{get(GD.PM_ColorMap, 'Value')};
contents = get(GD.PM_AxesFormat, 'String');
SaveOptions.AxesFormat = contents{get(GD.PM_AxesFormat, 'Value')};
contents = get(GD.PM_Space, 'String');
SaveOptions.Space = contents{get(GD.PM_Space, 'Value')};
SaveOptions.ColorLimits = [str2double(get(GD.TB_ColorLimMin, 'String')), str2double(get(GD.TB_ColorLimMax, 'String'))];
SaveOptions.XLim = [str2double(get(GD.TB_HorizontalMin, 'String')), str2double(get(GD.TB_HorizontalMax, 'String'))];
SaveOptions.YLim = [str2double(get(GD.TB_VerticalMin, 'String')), str2double(get(GD.TB_VerticalMax, 'String'))];
SaveOptions.omega = str2double(get(GD.TB_omega, 'String'));
SaveOptions.chi = str2double(get(GD.TB_chi, 'String'));
SaveOptions.RegriddingResolution = [str2double(get(GD.TB_RegridResolution1, 'String')), str2double(get(GD.TB_RegridResolution2, 'String'))];

% adding selected corrections
SaveOptions.LorentzCorrection = get(GD.CB_LorentzCorrection, 'Value');
SaveOptions.SolidAngleCorrection = get(GD.CB_SolidAngleCorrection, 'Value');
SaveOptions.PolarizationCorrection = get(GD.CB_PolarizationCorrection, 'Value');
SaveOptions.PixelDistanceCorrection = get(GD.CB_PixelDistanceCorrection, 'Value');
SaveOptions.DetectorEfficiencyCorrection = get(GD.CB_DetectorEfficiencyCorrection, 'Value');
SaveOptions.FlatFieldCorrection = get(GD.CB_FlatFieldCorrection, 'Value');

% extract the name of the selected beamtime/beamline from the
% LoadFileModule
GD = guidata(handles.LoadFileModule);
contents = get(GD.BeamtimeSelection, 'String');
SaveOptions.Beamtime = contents{get(GD.BeamtimeSelection, 'Value')};
contents = get(GD.BeamlineSelection, 'String');
SaveOptions.Beamline = contents{get(GD.BeamlineSelection, 'Value')}; %#ok. Is used for saving

% get the checked elements from the first data table
Data1 = get(handles.UIT_ElementsToInclude, 'Data');
L = [Data1{:, 3}];
% combine the file names with their directories
Paths1 = cellfun(@(S1, S2) fullfile(S1, S2), Data1(L, 4), Data1(L, 2), 'UniformOutput', false);
% get the file paths from the list box (include custom files)
Paths2 = get(handles.LB_FilesToInclude, 'String');
% concatenate
Paths = [Paths1; Paths2];

% The zipped folder contains all the selected files. To differentiate
% beamtimes, crystals, annotations, etc. we will use different file
% extensions for them
temp = tempdir;
if isempty(temp); temp = handles.WorkingPath; end
FolderPath = fullfile(temp, FileNameWOExt);
mkdir(FolderPath);

% save the SaveOptions in AdditionalSettings.AddSet
% Additional settings contain the values from the toolbox, but it can be
% expanded to hold other information as well.
save(fullfile(FolderPath, 'AdditionalSettings.AddSet'), 'SaveOptions');

% define the file extensions for Annotations, Beamtimes, Crystals
Exts = {'Annotations', 'Beamtimes', 'Crystals'; ...
        '.Annotation', '.Beamtime', '.Crystal'};

% loop over all selected files (including Beamtimes, Crystals, Annotations)
for iF = 1:numel(Paths)
    File = Paths{iF};
    [filepath, filename, Ext] = fileparts(File);
    % get the name of the parent directory
    [~, ParentName] = fileparts(filepath);
    
    switch ParentName
        case {'Annotations', 'Crystals', 'Beamtimes'}
            % find the extension
            L = strcmp(Exts(1, :), ParentName);
            % copy the file to the temp directory
            copyfile(File, fullfile(FolderPath, [filename, Exts{2, L}]), 'f');
        otherwise
            % just copy the file to the temp directory
            copyfile(File, fullfile(FolderPath, [filename, Ext]));
    end
end

% zip the temp directory.
zip(fullfile(PathName, FileName), FolderPath);
% zip() adds the extension .zip to the file name. Rename the file so that
% it has GIDzip extension --> can be filtered when loading a project
movefile(fullfile(PathName, [FileName, '.zip']), fullfile(PathName, FileName), 'f');
% remove the temporary directory
rmdir(FolderPath, 's');

guidata(hObject, handles);

msgbox('Project successfully saved.', 'modal')
close(handles.Figure_ProjectModule);


% --- Executes when user attempts to close Figure_ProjectModule.
function Figure_ProjectModule_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to Figure_ProjectModule (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% delete the listener to the AxesDestroyed event. Otherwise it will still
% listen and try to execute the MainAxesDestroyed_Callback function, even
% though the Module window is already closed.
delete(handles.MainAxesDestroyedListener);

% store settings
SettingsProjectModule.WindowPosition = get(hObject, 'Position');
SettingsProjectModule.AdditionalFilesLoadPath = handles.Settings.AdditionalFilesLoadPath;
SettingsProjectModule.ProjectSavePath = handles.Settings.ProjectSavePath;

% in order to avoid error when no write access for file use try.
try; save(handles.SettingsPath, 'SettingsProjectModule'); end

% Hint: delete(hObject) closes the figure
delete(hObject);
