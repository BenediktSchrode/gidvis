function SavePlotModule(AxesHandle)
% executes when the save image menu entry is clicked
%
% Usage:
%     SavePlotModule(AxesHandle)
%
% Input:
%     AxesHandle ... handle of the main GIDVis axes
%
%
% This file is part of GIDVis.
%
% see also: exportsetupdlg, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% find the MainImage in the AxesHandle
hMainImage = findobj(AxesHandle, 'Tag', 'MainImage');
if ~isempty(hMainImage)
    set(AxesHandle, 'ActivePositionProperty', 'outerposition');
    exportsetupdlg(ancestor(AxesHandle, 'figure'))
else
    msgbox('Please load a GID map first.', 'modal');
end