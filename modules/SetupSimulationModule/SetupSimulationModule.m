function varargout = SetupSimulationModule(varargin)
% This file, together with SetupSimulationModule.fig, is the UI for the 
% Setup Simulation Module.
% 
% Usage:
%     varargout = SetupSimulationModule(MainAxesHandle, ToolboxModuleHandle, LoadFileModuleHandle)
% 
% Input: 
%     MainAxesHandle ... handle of the main axes
%     ToolboxModuleHandle ... handle of a UI element in the Toolbox Module
%     LoadFileModuleHandle ... handle of a UI element in the Load File 
%                              Module
%
% Output:
%     varargout ... default GUIDE output
%
%
% This file is part of GIDVis.
%
% see also: XDetector, GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2020 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Edit the above text to modify the response to help SetupSimulationModule

% Last Modified by GUIDE v2.5 14-Apr-2019 17:45:49

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @SetupSimulationModule_OpeningFcn, ...
                   'gui_OutputFcn',  @SetupSimulationModule_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before SetupSimulationModule is made visible.
function SetupSimulationModule_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to SetupSimulationModule (see VARARGIN)

% Choose default command line output for SetupSimulationModule
handles.output = hObject;

% get the filepath of this file
filepath = mfilename('fullpath');
% get the parent's parent directory
PDir = fileparts(fileparts(filepath));

% load detectors
load(fullfile(PDir, 'DetectorModule', 'Detectors.mat'), 'Detectors')

% set the detector names in the popup menu
set(handles.PM_Detector, 'String', {Detectors.Name});

% store the detectors in the handles structure
handles.Detectors = Detectors;

% set the text label for the wavelength
set(handles.L_Wavelength, 'String', ['Wavelength (', char(197), ')']);

% add a listener to the axes in varargin{1}. When the axes gets destroyed 
% (e.g. because the figure containing the axes gets closed), the callback 
% will be executed (and closes this window).
% Store the listener in the handles structure to remove it when this window    
% gets closed by user, otherwise the callback is executed even when the
% SetupSimulationModule window does not exist anymore.
handles.MainAxesDestroyedListener = addlistener(varargin{1}, 'ObjectBeingDestroyed', @(src, evt) MainAxesDestroyed_Callback(src, evt, handles.Fig_SetupSimulationModule));

% store the axes
handles.MainAxes = varargin{1};

% store the toolbox handle
handles.ToolboxModule = varargin{2};

% store the load file handle
handles.LoadFileModule = varargin{3};

% the settings for this module are stored in the same directory as the file
% SetupSimulationModule.m. Store its path in the handles structure to reuse
% it.
handles.SettingsPath = fullfile(fileparts(mfilename('fullpath')), 'Settings.mat');

% read out settings if available
if exist(handles.SettingsPath, 'file') == 2
    S0 = load(handles.SettingsPath, 'SettingsSetupSimulation');
    S = S0.SettingsSetupSimulation;
    % set the values in the ui elements
    if numel(handles.Detectors) >= S.DetectorIndex
        set(handles.PM_Detector, 'Value', S.DetectorIndex);
    end
    set(handles.TB_detlenx, 'String', S.detlenx);
    set(handles.TB_detlenz, 'String', S.detlenz);
    set(handles.TB_psx, 'String', S.psx);
    set(handles.TB_psz, 'String', S.psz);
    set(handles.TB_DeadRows, 'String', S.DeadRows);
    set(handles.TB_DeadColumns, 'String', S.DeadColumns);
    set(handles.CB_RoundArea, 'Value', S.RoundActiveArea);
    set(handles.TB_cpx, 'String', S.cpx);
    set(handles.TB_cpz, 'String', S.cpz);
    set(handles.TB_sdd, 'String', S.sdd);
    set(handles.TB_ry, 'String', S.ry);
    set(handles.TB_omega, 'String', S.omega);
    set(handles.TB_chi, 'String', S.chi);
    set(handles.TB_Wavelength, 'String', S.Wavelength);
    set(handles.TB_gamma, 'String', S.gamma);
    set(handles.TB_delta, 'String', S.delta);
    set(handles.CB_BoundariesOnly, 'Value', S.BoundariesOnly);
else % use some default
    % call the detector selection popup menu callback once to update the
    % textboxes showing the detector properties
    PM_Detector_Callback(handles.PM_Detector, [], handles)
end

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes SetupSimulationModule wait for user response (see UIRESUME)
% uiwait(handles.Fig_SetupSimulationModule);

function MainAxesDestroyed_Callback(src, evt, HandleToModuleWindow)
% close module window
close(HandleToModuleWindow);

% --- Outputs from this function are returned to the command line.
function varargout = SetupSimulationModule_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if strcmpi(ToolboxModule('GetSelectedSpace', handles.ToolboxModule), 'pixel')
    msgbox('Since the Setup Simulation uses reciprocal space data, it should not be used with space ''Pixel''. Change the space in the ''Toolbox'' window.', 'modal');
end

% Call the UpdateSimulation function to update the plot
UpdateSimulation(hObject)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in PM_Detector.
function PM_Detector_Callback(hObject, eventdata, handles)
% hObject    handle to PM_Detector (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns PM_Detector contents as cell array
%        contents{get(hObject,'Value')} returns selected item from PM_Detector

% get selected detector index
ind = get(handles.PM_Detector, 'Value');
D = handles.Detectors(ind);

% set the values of the textboxes accordingly
set(handles.TB_detlenx, 'String', D.detlenx);
set(handles.TB_detlenz, 'String', D.detlenz);
set(handles.TB_DeadRows, 'String', num2str(D.InactiveRows));
set(handles.TB_DeadColumns, 'String', num2str(D.InactiveColumns));
set(handles.TB_psx, 'String', D.psx);
set(handles.TB_psz, 'String', D.psz);
if strcmp(D.Shape, 'rectangular')
    set(handles.CB_RoundArea, 'Value', 0);
elseif strcmp(D.Shape, 'round')
    set(handles.CB_RoundArea, 'Value', 1);
end

% --- Executes during object creation, after setting all properties.
function PM_Detector_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PM_Detector (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_Wavelength_Callback(hObject, eventdata, handles)
% hObject    handle to TB_Wavelength (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_Wavelength as text
%        str2double(get(hObject,'String')) returns contents of TB_Wavelength as a double


% --- Executes during object creation, after setting all properties.
function TB_Wavelength_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_Wavelength (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_cpx_Callback(hObject, eventdata, handles)
% hObject    handle to TB_cpx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_cpx as text
%        str2double(get(hObject,'String')) returns contents of TB_cpx as a double


% --- Executes during object creation, after setting all properties.
function TB_cpx_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_cpx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_cpz_Callback(hObject, eventdata, handles)
% hObject    handle to TB_cpz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_cpz as text
%        str2double(get(hObject,'String')) returns contents of TB_cpz as a double


% --- Executes during object creation, after setting all properties.
function TB_cpz_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_cpz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_sdd_Callback(hObject, eventdata, handles)
% hObject    handle to TB_sdd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_sdd as text
%        str2double(get(hObject,'String')) returns contents of TB_sdd as a double


% --- Executes during object creation, after setting all properties.
function TB_sdd_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_sdd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_ry_Callback(hObject, eventdata, handles)
% hObject    handle to TB_ry (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_ry as text
%        str2double(get(hObject,'String')) returns contents of TB_ry as a double


% --- Executes during object creation, after setting all properties.
function TB_ry_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_ry (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_omega_Callback(hObject, eventdata, handles)
% hObject    handle to TB_omega (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_omega as text
%        str2double(get(hObject,'String')) returns contents of TB_omega as a double


% --- Executes during object creation, after setting all properties.
function TB_omega_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_omega (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_chi_Callback(hObject, eventdata, handles)
% hObject    handle to TB_chi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_chi as text
%        str2double(get(hObject,'String')) returns contents of TB_chi as a double


% --- Executes during object creation, after setting all properties.
function TB_chi_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_chi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function UpdateSimulation(hObject)
% get handles structure
handles = guidata(hObject);

% get detector values
detlenx = str2double(get(handles.TB_detlenx, 'String'));
detlenz = str2double(get(handles.TB_detlenz, 'String'));
psx = str2double(get(handles.TB_psx, 'String'));
psz = str2double(get(handles.TB_psz, 'String'));
DeadRows = str2num(get(handles.TB_DeadRows, 'String'));
DeadColumns = str2num(get(handles.TB_DeadColumns, 'String'));

% check for valid input
if any(isnan([detlenx detlenz psx psz DeadRows DeadColumns]))
    msgbox('Please enter only numbers for the detector properties.', ...
        'modal')
    return;
end

if get(handles.CB_RoundArea, 'Value')
    Shape = 'round';
else
    Shape = 'rectangular';
end

D = XDetector('Simulation', detlenx, detlenz, psx, psz, ...
    DeadRows, DeadColumns, Shape, '', [], []);

% create pixel representation of the detector
M = Pixels(D, 0);

% get the experimental settings
lambda = str2double(get(handles.TB_Wavelength, 'String'));
cpx = str2double(get(handles.TB_cpx, 'String'));
cpz = str2double(get(handles.TB_cpz, 'String'));
sdd = str2double(get(handles.TB_sdd, 'String'));
ry = str2double(get(handles.TB_ry, 'String'));

% get sample angles
omega = str2double(get(handles.TB_omega, 'String'));
chi = str2double(get(handles.TB_chi, 'String'));

% get goniometer angles
gamma = str2num(get(handles.TB_gamma, 'String'));
delta = str2num(get(handles.TB_delta, 'String'));

% check for valid input
if any(isnan([lambda cpx cpz sdd ry omega chi gamma delta])) || ...
        isempty(gamma) || isempty(delta)
    msgbox('Please enter only numbers for the experimental settings.', ...
        'modal')
    return;
end

% disable button
set(handles.Btn_Update, 'Enable', 'off');
drawnow;

% create experimental setup and assign experimental properties
BL = ExperimentalSetUp;
BL.lambda = lambda;
BL.cpx = cpx;
BL.cpz = cpz;
BL.sdd = sdd;
BL.ry = ry;
BL.detlenx = size(M, 2);
BL.detlenz = size(M, 1);
BL.psx = psx;
BL.psz = psz;
BL.Geometry = 'GammaDelta';

% get goniometer angles
IPA = gamma;
OPA = delta;
[IPA, OPA] = meshgrid(IPA, OPA);

% delete things which have already been plotted
if ~isfield(handles, 'HG_Sim') || ~isgraphics(handles.HG_Sim)
    handles.HG_Sim = hggroup('Parent', handles.MainAxes);
else
    delete(get(handles.HG_Sim, 'Children'));
end

OnlyBounds = get(handles.CB_BoundariesOnly, 'Value');

if OnlyBounds % find the boundaries of the detector panels
    % create x and z values of each pixel
    [x, z] = meshgrid(1:BL.detlenx, 1:BL.detlenz);
    % sum over the nearest neighbors
    F = conv2(M, ones(3), 'same');
    % find pixels which are not the boundary pixels
    LBounds = F == 9 | F <= 3;
    % remove non-boundary pixels
    x(LBounds) = [];
    z(LBounds) = [];
else
    % find pixels which are not 0 (i.e. not gaps of detector)
    L = M(:) ~= 0;
end

% reset the color order
set(handles.MainAxes, 'ColorOrderIndex', 1);

% get the selected space
Space = ToolboxModule('GetSelectedSpace', handles.ToolboxModule);

% update the plot
for iG = 1:numel(IPA)
    % assign goniometer angles
    BL.gamma = IPA(iG);
    BL.delta = OPA(iG);
    if OnlyBounds
        if strcmpi(Space, 'q') || strcmpi(Space, 'pixel')
            % transform pixel to reciprocal space
            [xToPlot, yToPlot] = CalculateQ(BL, omega, chi, x, z);
        elseif strcmpi(Space, 'polar')
            [xToPlot, yToPlot] = CalculatePolar(BL, omega, chi, x, z);
        else
            error('SetupSimulationModule:UnknownSpace', 'The selected space is not known.');
        end
    else
        if strcmpi(Space, 'q') || strcmpi(Space, 'pixel')
            [xToPlot, yToPlot] = CalculateQ(BL, omega, chi);
            xToPlot(~L) = [];
            yToPlot(~L) = [];
        elseif strcmpi(Space, 'polar')
            [xToPlot, yToPlot] = CalculatePolar(BL, omega, chi);
            xToPlot(~L) = [];
            yToPlot(~L) = [];
        else
            error('SetupSimulationModule:UnknownSpace', 'The selected space is not known.');
        end
    end
    plot(handles.HG_Sim, xToPlot, yToPlot, '.', ...
        'DisplayName', sprintf('IP = %g�, OOP = %g�', IPA(iG), OPA(iG)));
        
    hold(handles.MainAxes, 'on')
end
hold(handles.MainAxes, 'off');

% update handles structure
guidata(hObject, handles);

% reenable button
set(handles.Btn_Update, 'Enable', 'on');
drawnow;


function TB_gamma_Callback(hObject, eventdata, handles)
% hObject    handle to TB_gamma (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_gamma as text
%        str2double(get(hObject,'String')) returns contents of TB_gamma as a double


% --- Executes during object creation, after setting all properties.
function TB_gamma_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_gamma (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_delta_Callback(hObject, eventdata, handles)
% hObject    handle to TB_delta (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_delta as text
%        str2double(get(hObject,'String')) returns contents of TB_delta as a double


% --- Executes during object creation, after setting all properties.
function TB_delta_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_delta (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Btn_Update.
function Btn_Update_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_Update (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
UpdateSimulation(hObject);



function TB_detlenx_Callback(hObject, eventdata, handles)
% hObject    handle to TB_detlenx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_detlenx as text
%        str2double(get(hObject,'String')) returns contents of TB_detlenx as a double


% --- Executes during object creation, after setting all properties.
function TB_detlenx_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_detlenx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_detlenz_Callback(hObject, eventdata, handles)
% hObject    handle to TB_detlenz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_detlenz as text
%        str2double(get(hObject,'String')) returns contents of TB_detlenz as a double


% --- Executes during object creation, after setting all properties.
function TB_detlenz_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_detlenz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_DeadRows_Callback(hObject, eventdata, handles)
% hObject    handle to TB_DeadRows (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_DeadRows as text
%        str2double(get(hObject,'String')) returns contents of TB_DeadRows as a double


% --- Executes during object creation, after setting all properties.
function TB_DeadRows_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_DeadRows (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_DeadColumns_Callback(hObject, eventdata, handles)
% hObject    handle to TB_DeadColumns (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_DeadColumns as text
%        str2double(get(hObject,'String')) returns contents of TB_DeadColumns as a double


% --- Executes during object creation, after setting all properties.
function TB_DeadColumns_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_DeadColumns (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_psx_Callback(hObject, eventdata, handles)
% hObject    handle to TB_psx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_psx as text
%        str2double(get(hObject,'String')) returns contents of TB_psx as a double


% --- Executes during object creation, after setting all properties.
function TB_psx_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_psx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_psz_Callback(hObject, eventdata, handles)
% hObject    handle to TB_psz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TB_psz as text
%        str2double(get(hObject,'String')) returns contents of TB_psz as a double


% --- Executes during object creation, after setting all properties.
function TB_psz_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_psz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in CB_RoundArea.
function CB_RoundArea_Callback(hObject, eventdata, handles)
% hObject    handle to CB_RoundArea (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of CB_RoundArea


% --- Executes when user attempts to close Fig_SetupSimulationModule.
function Fig_SetupSimulationModule_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to Fig_SetupSimulationModule (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% delete the listener to the AxesDestroyed event. Otherwise it will still
% listen and try to execute the MainAxesDestroyed_Callback function, even
% though the Module1 window is already closed.
delete(handles.MainAxesDestroyedListener);

% delete all the things plotted in the main axes
if isfield(handles, 'HG_Sim')
    delete(handles.HG_Sim);
end

% store the settings
SettingsSetupSimulation.DetectorIndex = get(handles.PM_Detector, 'Value');
SettingsSetupSimulation.detlenx = get(handles.TB_detlenx, 'String');
SettingsSetupSimulation.detlenz = get(handles.TB_detlenz, 'String');
SettingsSetupSimulation.psx = get(handles.TB_psx, 'String');
SettingsSetupSimulation.psz = get(handles.TB_psz, 'String');
SettingsSetupSimulation.DeadRows = get(handles.TB_DeadRows, 'String');
SettingsSetupSimulation.DeadColumns = get(handles.TB_DeadColumns, 'String');
SettingsSetupSimulation.RoundActiveArea = get(handles.CB_RoundArea, 'Value');
SettingsSetupSimulation.cpx = get(handles.TB_cpx, 'String');
SettingsSetupSimulation.cpz = get(handles.TB_cpz, 'String');
SettingsSetupSimulation.sdd = get(handles.TB_sdd, 'String');
SettingsSetupSimulation.ry = get(handles.TB_ry, 'String');
SettingsSetupSimulation.omega = get(handles.TB_omega, 'String');
SettingsSetupSimulation.chi = get(handles.TB_chi, 'String');
SettingsSetupSimulation.Wavelength = get(handles.TB_Wavelength, 'String');
SettingsSetupSimulation.gamma = get(handles.TB_gamma, 'String');
SettingsSetupSimulation.delta = get(handles.TB_delta, 'String');
SettingsSetupSimulation.BoundariesOnly = get(handles.CB_BoundariesOnly, 'Value');

% Store the settings. In order to avoid error when no write permission is
% available, use try.
try; save(handles.SettingsPath, 'SettingsSetupSimulation'); end

% Hint: delete(hObject) closes the figure
delete(hObject);


% --- Executes on button press in CB_BoundariesOnly.
function CB_BoundariesOnly_Callback(hObject, eventdata, handles)
% hObject    handle to CB_BoundariesOnly (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of CB_BoundariesOnly


% --- Executes on button press in Btn_LoadCurrentBeamline.
function Btn_LoadCurrentBeamline_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_LoadCurrentBeamline (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% get the selected beamline
BL = LoadFileModule('GetBeamline', handles.LoadFileModule);
% assign the detector properties to the text boxes
set(handles.TB_detlenx, 'String', BL.detlenx);
set(handles.TB_detlenz, 'String', BL.detlenz);
set(handles.TB_psx, 'String', BL.psx);
set(handles.TB_psz, 'String', BL.psz);
% assign the experimental settings to the text boxes
set(handles.TB_cpx, 'String', BL.cpx);
set(handles.TB_cpz, 'String', BL.cpz);
set(handles.TB_sdd, 'String', BL.sdd);
set(handles.TB_ry, 'String', BL.ry);
set(handles.TB_Wavelength, 'String', BL.lambda);
if strcmp(BL.Geometry, 'none')
    set(handles.TB_gamma, 'String', 0);
    set(handles.TB_delta, 'String', 0);
elseif strcmp(BL.Geometry, 'GammaDelta')
    set(handles.TB_gamma, 'String', BL.Gamma);
    set(handles.TB_delta, 'String', BL.Delta);
end
