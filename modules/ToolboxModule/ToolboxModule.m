function varargout = ToolboxModule(varargin)
% This file, together with ToolboxModule.fig, is the Toolbox Module of
% GIDVis.
%
% Usage:
%     ToolboxModule(MainAxesHandle, ColormapsPath)
%
% Input:
%     MainAxesHandle ... handle of the main GIDVis axes
%     ColormapsPath ... path of the Colormaps directory
%
% Output:
%     varargout ... default GUIDE output
%
% This file is part of GIDVis.
%
% see also: GIDVisLicense

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       This file is part of GIDVis                       %
%                                                                         %
% Copyright (C) 2017-2019 Benedikt Schrode, Christian Roethel, Stefan     %
% Pachmajer                                                               %
%                                                                         %
% GIDVis is free software: you can redistribute it and/or modify it under %
% the terms of the GNU General Public License as published by the Free    %
% Software Foundation, either version 3 of the License, or (at your       %
% option) any later version.                                              %
%                                                                         %
% GIDVis is distributed in the hope that it will be useful, but WITHOUT   %
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   %
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    %
% for more details.                                                       %
%                                                                         %
% You should have received a copy of the GNU General Public License along %
% with GIDVis, you can find it in GIDVisLicense.m.                        %
% If not, see <http://www.gnu.org/licenses/>.                             %
%                                                                         %
%                                                                         %
% Please visit https://www.if.tugraz.at/amd/GIDVis/ for more information. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Last Modified by GUIDE v2.5 07-Nov-2018 14:42:29

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ToolboxModule_OpeningFcn, ...
                   'gui_OutputFcn',  @ToolboxModule_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ToolboxModule is made visible.
function ToolboxModule_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ToolboxModule (see VARARGIN)

% Choose default command line output for ToolboxModule
handles.output = hObject;

% store the main axes handle from varargin{1} in the handles structure
handles.MainAxes = varargin{1};

% store the colormaps path in the handles structure
handles.ColormapsPath = varargin{2};

% add a listener to the axes in varargin{1}. When the axes gets destroyed 
% (e.g. because the figure containing the axes gets closed), the callback 
% will be executed (and closes this window).
% Store the listener in the handles structure to remove it when this window    
% gets closed by user, otherwise the callback is executed even when the
% LoadFileModule window does not exist anymore.
handles.MainAxesDestroyedListener = addlistener(varargin{1}, 'ObjectBeingDestroyed', @(src, evt) MainAxesDestroyed_Callback(src, evt, handles.Figure_ToolboxModule));

% used to decide whether to make the window only invisible/really close it
handles.CloseReally = false;

PopulateColormapPM(hObject, handles.ColormapsPath);

% the settings for this module are stored in the same directory as the file
% ToolboxModule.m. Store its path in the handles structure to reuse it.
handles.SettingsPath = fullfile(fileparts(mfilename('fullpath')), 'Settings.mat');

% read out settings if available
if exist(handles.SettingsPath, 'file') == 2
    load(handles.SettingsPath);
    % store the settings in the handles structure
    handles.Settings = SettingsToolboxModule;
    % restore the entry fields
    if isfield(handles.Settings, 'PM_Scaling_Value')
        set(handles.PM_Scaling, 'Value', handles.Settings.PM_Scaling_Value);
    end
    if isfield(handles.Settings, 'PM_Space_Value')
        set(handles.PM_Space, 'Value', handles.Settings.PM_Space_Value);
    end
    if isfield(handles.Settings, 'TB_omega_String')
        set(handles.TB_omega, 'String', handles.Settings.TB_omega_String);
    end
    if isfield(handles.Settings, 'TB_chi_String')
        set(handles.TB_chi, 'String', handles.Settings.TB_chi_String);
    end
    if isfield(handles.Settings, 'TB_RegridResolution1_String')
        set(handles.TB_RegridResolution1, 'String', handles.Settings.TB_RegridResolution1_String);
    end
    if isfield(handles.Settings, 'TB_RegridResolution2_String')
        set(handles.TB_RegridResolution2, 'String', handles.Settings.TB_RegridResolution2_String);
    end
    if isfield(handles.Settings, 'PM_ColorMap_Value') && handles.Settings.PM_ColorMap_Value <= numel(get(handles.PM_ColorMap, 'String'));
        set(handles.PM_ColorMap, 'Value', handles.Settings.PM_ColorMap_Value);
    end
    if isfield(handles.Settings, 'PM_AxesFormat_Value')
        set(handles.PM_AxesFormat, 'Value', handles.Settings.PM_AxesFormat_Value);
        contents = cellstr(get(handles.PM_AxesFormat, 'String'));
        Str = contents{get(handles.PM_AxesFormat, 'Value')};
        axis(handles.MainAxes, Str)
    end
else
    handles.Settings = [];
    % start with inferno colormap selected
    Maps = get(handles.PM_ColorMap, 'String');
    Index = find(not(cellfun('isempty', strfind(Maps, 'inferno'))));
    if ~isempty(Index)
        set(handles.PM_ColorMap, 'Value', Index);
        handles.Settings.PM_ColorMap_Value = Index;
    end
end

% get selected space
Spaces = get(handles.PM_Space, 'String');
Value = get(handles.PM_Space, 'Value');

% turn correction factors checkboxes enabled state to off and their value
% to 0 for pixel space
if strcmpi(Spaces{Value}, 'pixel')
    set([handles.CB_LorentzCorrection, handles.CB_SolidAngleCorrection, ...
       handles.CB_PolarizationCorrection, handles.CB_PixelDistanceCorrection, ...
       handles.CB_DetectorEfficiencyCorrection, handles.CB_FlatFieldCorrection], ...
       'Value', 0, 'Enable', 'off');
else
    set([handles.CB_LorentzCorrection, handles.CB_SolidAngleCorrection, ...
       handles.CB_PolarizationCorrection, handles.CB_PixelDistanceCorrection, ...
       handles.CB_DetectorEfficiencyCorrection, handles.CB_FlatFieldCorrection], ...
       'Enable', 'on');
end

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ToolboxModule wait for user response (see UIRESUME)
% uiwait(handles.Figure_ToolboxModule);

function PopulateColormapPM(hObject, ColormapsPath)
handles = guidata(hObject);
% some of the built-in colormaps
BuiltInColormaps = {'jet', 'hsv', 'hot', 'cool', 'spring', 'summer', ...
    'autumn', 'winter', 'gray', 'bone', 'copper', 'pink'};

if ~verLessThan('Matlab', '8.4') % add parula to list of built-in colormaps
    BuiltInColormaps{end+1} = 'parula';
end
% get the list of custom color maps in folder Colormaps
CustomColormapsF = dir(fullfile(ColormapsPath, '*.m'));
% get the names (include extension)
CustomColormaps = {CustomColormapsF.name};
for iC = 1:numel(CustomColormaps) % remove extension from name
    [~, FileName] = fileparts(CustomColormaps{iC});
    CustomColormaps{iC} = FileName;
end
Colormaps = [CustomColormaps, BuiltInColormaps];
% sort the colormaps by name (use lower() to correctly sort lower and upper
% case names)
[~, ind] = sort(lower(Colormaps));
Colormaps = Colormaps(ind);
% put them in popup menu
set(handles.PM_ColorMap, 'String', Colormaps);


function MainAxesDestroyed_Callback(src, evt, HandleToToolboxWindow)
handles = guidata(HandleToToolboxWindow);
% Is called when the axes in the main window is destroyed, e. g. when the
% main window gets closed.
handles.CloseReally = true;

% Update handles structure
guidata(HandleToToolboxWindow, handles);

% close module window
close(HandleToToolboxWindow);

% --- Outputs from this function are returned to the command line.
function varargout = ToolboxModule_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% restore the window position
if isfield(handles.Settings, 'WindowPosition')
%     set(hObject, 'Position', handles.Settings.WindowPosition);
    RestorePositionOrDefault(hObject, handles.Settings.WindowPosition, ...
        'east');
end

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in PM_Scaling.
function PM_Scaling_Callback(hObject, eventdata, handles)
% Request plot update to update scaling
RequestPlotUpdateFromData(handles.MainAxes);

% get selected space
contents = get(hObject, 'String');
Space = contents(get(hObject, 'Value'));

% update the axes label
if strcmpi(Space, 'q')
    [Horiz, Vert] = MainAxesSettingsModule('getQAxesLabels');
    set(get(AxesHandle, 'XLabel'), 'String', Horiz);
    set(get(AxesHandle, 'YLabel'), 'String', Vert);
elseif strcmpi(Space, 'pixel')
    [Horiz, Vert] = MainAxesSettingsModule('getPixelAxesLabels');
    set(get(AxesHandle, 'XLabel'), 'String', Horiz);
    set(get(AxesHandle, 'YLabel'), 'String', Vert);
end

% store scaling in handles structure
handles.Settings.PM_Scaling_Value = get(hObject, 'Value');

% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function PM_Scaling_CreateFcn(hObject, eventdata, handles)
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in PM_ColorMap.
function PM_ColorMap_Callback(hObject, eventdata, handles)
% get the requested colormap
contents = get(hObject, 'String');
ReqColormap = contents{get(hObject, 'Value')};
% update the color map of the main axes. Since the value in ReqColormap is
% a string but we have to call a function to inside colormap(), use eval
colormap(handles.MainAxes, eval([ReqColormap, '(256)']));

handles.Settings.PM_ColorMap_Value = get(hObject, 'Value');

% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in CB_LockColorScaling.
function CB_LockColorScaling_Callback(hObject, eventdata, handles)


function TB_HorizontalMin_Callback(hObject, eventdata, handles)
% call function where axes limits get updated
SetAxesLimits(handles)

% --- Executes during object creation, after setting all properties.
function TB_HorizontalMin_CreateFcn(hObject, eventdata, handles)
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_HorizontalMax_Callback(hObject, eventdata, handles)
% call function where axes limits get updated
SetAxesLimits(handles);


% --- Executes during object creation, after setting all properties.
function TB_HorizontalMax_CreateFcn(hObject, eventdata, handles)
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_VerticalMin_Callback(hObject, eventdata, handles)
% call function where axes limits get updated
SetAxesLimits(handles);


% --- Executes during object creation, after setting all properties.
function TB_VerticalMin_CreateFcn(hObject, eventdata, handles)
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function TB_VerticalMax_Callback(hObject, eventdata, handles)
% call function where axes limits get updated
SetAxesLimits(handles);


function SetAxesLimits(handles)
% this function sets the axes limits according to the values entered in the
% corresponding textboxes
NewHorizontalLimits = [str2double(get(handles.TB_HorizontalMin, 'String')), ...
    str2double(get(handles.TB_HorizontalMax, 'String'))];
if ~any(isnan(NewHorizontalLimits)) && diff(NewHorizontalLimits) > 0
    set(handles.MainAxes, 'XLim', NewHorizontalLimits);
end

NewVerticalLimits = [str2double(get(handles.TB_VerticalMin, 'String')), ...
    str2double(get(handles.TB_VerticalMax, 'String'))];
if ~any(isnan(NewVerticalLimits)) && diff(NewVerticalLimits) > 0
    set(handles.MainAxes, 'YLim', NewVerticalLimits);
end


% --- Executes during object creation, after setting all properties.
function TB_VerticalMax_CreateFcn(hObject, eventdata, handles)
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Btn_AxesLimitsFull.
function Btn_AxesLimitsFull_Callback(hObject, eventdata, handles)
% set the axes limits to the min/max values of the GID image data, which is
% the image data with Tag MainImage.
h = findobj(handles.MainAxes, 'Tag', 'MainImage');
if isempty(h); return; end
XData = get(h, 'XData');
YData = get(h, 'YData');
set(handles.TB_HorizontalMin, 'String', min(XData(:)));
set(handles.TB_HorizontalMax, 'String', max(XData(:)));
set(handles.TB_VerticalMin, 'String', min(YData(:)));
set(handles.TB_VerticalMax, 'String', max(YData(:)));
SetAxesLimits(handles)


% --- Executes on selection change in PM_Space.
function PM_Space_Callback(hObject, eventdata, handles)
% get selected space
Spaces = get(hObject, 'String');
Value = get(hObject, 'Value');

% turn correction factors checkboxes enabled state to off and their value
% to 0 for pixel space
if strcmpi(Spaces{Value}, 'pixel')
    set([handles.CB_LorentzCorrection, handles.CB_SolidAngleCorrection, ...
       handles.CB_PolarizationCorrection, handles.CB_PixelDistanceCorrection, ...
       handles.CB_DetectorEfficiencyCorrection, handles.CB_FlatFieldCorrection], ...
       'Value', 0, 'Enable', 'off');
else
    set([handles.CB_LorentzCorrection, handles.CB_SolidAngleCorrection, ...
       handles.CB_PolarizationCorrection, handles.CB_PixelDistanceCorrection, ...
       handles.CB_DetectorEfficiencyCorrection, handles.CB_FlatFieldCorrection], ...
       'Enable', 'on');
end

% Call function to update plot with new space
RequestPlotUpdateFromData(handles.MainAxes);
% store selected value in handles structure
handles.Settings.PM_Space_Value = get(hObject, 'Value');

% update the axes limits
Btn_AxesLimitsFull_Callback(handles.Btn_AxesLimitsFull, [], handles)

% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function PM_Space_CreateFcn(hObject, eventdata, handles)
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_omega_Callback(hObject, eventdata, handles)
% call function to update plot with new omega
RequestPlotUpdateFromData(handles.MainAxes);
% store omega in settings structure
handles.Settings.TB_omega_String = get(hObject, 'String');

% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function TB_omega_CreateFcn(hObject, eventdata, handles)
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function TB_chi_Callback(hObject, eventdata, handles)
% call function to update plot with new chi value
RequestPlotUpdateFromData(handles.MainAxes);
% store value in settings
handles.Settings.TB_chi_String = get(hObject, 'String');

% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function TB_chi_CreateFcn(hObject, eventdata, handles)
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when user attempts to close Figure_ToolboxModule.
function Figure_ToolboxModule_CloseRequestFcn(hObject, eventdata, handles)
if handles.CloseReally % is true when main window gets closed
    % clean up everything: delete listeners, store settings

    % delete the listener to the AxesDestroyed event. Otherwise it will still
    % listen and try to execute the MainAxesDestroyed_Callback function, even
    % though the Module1 window is already closed.
    delete(handles.MainAxesDestroyedListener);
    
    % store the settings
    SettingsToolboxModule = handles.Settings;
    SettingsToolboxModule.WindowPosition = get(hObject, 'Position');
    % in order to avoid exception when no write access, use try.
    try; save(handles.SettingsPath, 'SettingsToolboxModule'); end
    
    delete(hObject);
else % user clicked the 'X' on the toolbox module window
    % instead of closing, make window invisible
    set(hObject, 'Visible', 'off');
end


function TB_RegridResolution1_Callback(hObject, eventdata, handles)
% update plot with the new regrid resolution
RequestPlotUpdateFromData(handles.MainAxes);
% save the value in the settings
handles.Settings.TB_RegridResolution1_String = get(hObject, 'String');

% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function TB_RegridResolution1_CreateFcn(hObject, eventdata, handles)
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_RegridResolution2_Callback(hObject, eventdata, handles)
% update the plot with the new regrid resolution
RequestPlotUpdateFromData(handles.MainAxes);
% save the value in the settings
handles.Settings.TB_RegridResolution2_String = get(hObject, 'String');

% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function TB_RegridResolution2_CreateFcn(hObject, eventdata, handles)
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% The Following functions Get... can be used to access the selected
% values/entered values of the toolbox module from outside this gui.
% For example, to retrieve the selected scaling from outside this gui, use:
%
% Scaling = ToolboxModule('GetSelectedScaling', handleToToolboxModule);
%
% where handleToToolboxModule is usually be created by:
%
% handleToToolboxModule = ToolboxModule(axes);

function Scaling = GetSelectedScaling(ToolboxModuleHandle)
handles = guidata(ToolboxModuleHandle);
contents = get(handles.PM_Scaling, 'String');
Scaling = contents{get(handles.PM_Scaling, 'Value')};


function Space = GetSelectedSpace(ToolboxModuleHandle)
handles = guidata(ToolboxModuleHandle);
contents = get(handles.PM_Space, 'String');
Space = contents{get(handles.PM_Space, 'Value')};


function omega = GetOmega(ToolboxModuleHandle)
handles = guidata(ToolboxModuleHandle);
omega = str2double(get(handles.TB_omega, 'String'));


function chi = GetChi(ToolboxModuleHandle)
handles = guidata(ToolboxModuleHandle);
chi = str2double(get(handles.TB_chi, 'String'));


function RegridResolution = GetRegridResolution(ToolboxModuleHandle)
handles = guidata(ToolboxModuleHandle);
RegridResolution = [str2double(get(handles.TB_RegridResolution1, 'String')), ...
    str2double(get(handles.TB_RegridResolution2, 'String'))];


function CBValue = GetLockColorScaling(ToolboxModuleHandle)
handles = guidata(ToolboxModuleHandle);
CBValue = get(handles.CB_LockColorScaling, 'Value');


% the ultimate get all values function
function AllValues = getAllValues(ToolboxModuleHandle)
handles = guidata(ToolboxModuleHandle);

contents = get(handles.PM_Scaling, 'String');
Scaling = contents{get(handles.PM_Scaling, 'Value')};

contents = get(handles.PM_Space, 'String');
Space = contents{get(handles.PM_Space, 'Value')};

omega = str2double(get(handles.TB_omega, 'String'));

% --- Executes during object creation, after setting all properties.
function PM_ColorMap_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PM_ColorMap (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_ColorLimMin_Callback(hObject, eventdata, handles)
SetColorLimits(handles);

% --- Executes during object creation, after setting all properties.
function TB_ColorLimMin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_ColorLimMin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function SetColorLimits(handles)
MinVal = str2double(get(handles.TB_ColorLimMin, 'String'));
MaxVal = str2double(get(handles.TB_ColorLimMax, 'String'));
if ~isnan(MinVal) && ~isnan(MaxVal)
    caxis(handles.MainAxes, [MinVal; MaxVal]);
    handles.Settings.ColorLimMin_String = MinVal;
    handles.Settings.ColorLimMax_String = MaxVal;
    
    % Update handles structure
    guidata(handles.Figure_ToolboxModule, handles);
end


function TB_ColorLimMax_Callback(hObject, eventdata, handles)
SetColorLimits(handles);


% --- Executes during object creation, after setting all properties.
function TB_ColorLimMax_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB_ColorLimMax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in PM_AxesFormat.
function PM_AxesFormat_Callback(hObject, eventdata, handles)
% read out the selected value from the Popup menu and set the axes
% accordingly
contents = cellstr(get(hObject, 'String'));
Str = contents{get(hObject, 'Value')};
% get the current axes limits
Limits = axis(handles.MainAxes);
% set the axes format
axis(handles.MainAxes, Str)
% restore the axes limits
axis(handles.MainAxes, Limits);
% save the value in the settings
handles.Settings.PM_AxesFormat_Value = get(hObject, 'Value');
% update axes limits
SetAxesLimits(handles);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function PM_AxesFormat_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PM_AxesFormat (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Btn_AutoColorLims.
function Btn_AutoColorLims_Callback(hObject, eventdata, handles)
% Calculates the color limits based on the data shown and sets them
% find the MainImage handle
h = findobj(handles.MainAxes, 'Tag', 'MainImage');
if isempty(h)
    return;
end
% read out the data
data = get(h, 'CData');
% calculate the color limit
ColorLims = getColorBarLimits(data);
% write the new values into the color limit textboxes
set(handles.TB_ColorLimMin, 'String', ColorLims(1));
set(handles.TB_ColorLimMax, 'String', ColorLims(2));
% update handles structure
guidata(hObject, handles);
% call function which sets the color limit
SetColorLimits(handles)

function UpdateWithNewCorrection(hObject, handles)
% get parent window
f = ancestor(hObject, 'figure');
% get current cursor
OldPointer = get(f, 'Pointer');
% set wait cursor
set(f, 'Pointer', 'watch');
% get children of the parent. These are all the correction checkboxes
CBs = get(get(hObject, 'Parent'), 'Children');
% set all checkboxes to disables
set(CBs, 'Enable', 'off');
drawnow
% call function to update plot
RequestPlotUpdateFromData(handles.MainAxes);
% set old cursor again
set(f, 'Pointer', OldPointer);
% set all checkboxes to enabled again
set(CBs, 'Enable', 'on')
drawnow


% --- Executes on button press in CB_LorentzCorrection.
function CB_LorentzCorrection_Callback(hObject, eventdata, handles)
% hObject    handle to CB_LorentzCorrection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
UpdateWithNewCorrection(hObject, handles);


% --- Executes on button press in CB_PolarizationCorrection.
function CB_PolarizationCorrection_Callback(hObject, eventdata, handles)
% hObject    handle to CB_PolarizationCorrection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
UpdateWithNewCorrection(hObject, handles);


% --- Executes on button press in CB_SolidAngleCorrection.
function CB_SolidAngleCorrection_Callback(hObject, eventdata, handles)
% hObject    handle to CB_SolidAngleCorrection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
UpdateWithNewCorrection(hObject, handles);


% --- Executes on button press in CB_PixelDistanceCorrection.
function CB_PixelDistanceCorrection_Callback(hObject, eventdata, handles)
% hObject    handle to CB_PixelDistanceCorrection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
UpdateWithNewCorrection(hObject, handles);


% --- Executes on button press in CB_DetectorEfficiencyCorrection.
function CB_DetectorEfficiencyCorrection_Callback(hObject, eventdata, handles)
% hObject    handle to CB_DetectorEfficiencyCorrection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
UpdateWithNewCorrection(hObject, handles);


% --- Executes on button press in CB_FlatFieldCorrection.
function CB_FlatFieldCorrection_Callback(hObject, eventdata, handles)
% hObject    handle to CB_FlatFieldCorrection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% get handles structure of the main GIDVis window
GD = guidata(handles.MainAxes);

if ~isfield(GD, 'RawData')
    return;
end

if isa(GD.RawData, 'qData')
    q = GD.RawData;
    % get the experimental setups used for the merging of this image
    C = {q.Beamtime.SetUps(q.Assignment).FFImageData};
    % check if the flat-field images is empty
    L = cellfun(@isempty, C);
    if any(L)
        msgbox(sprintf(['No flat-field image available for the experimental setup(s) %s used during the merging of the data files.\n', ...
            'Use File - Beamtime/Experimental Setup in the main GIDVis window to add flat field image(s) and then merge the original data files with the updated experimental setup(s).'], ...
            strjoin({q.Beamtime.SetUps(q.Assignment(L)).name}, ', ')), 'modal');
        % set the value of the checkbox to 0 (i.e. uncheck it)
        set(hObject, 'Value', 0);
    else
        % apply corrections
        UpdateWithNewCorrection(hObject, handles);
    end
else
    % get beamline
    BL = LoadFileModule('GetBeamline', GD.LoadFileModule);

    % check if a flat-field image is available
    if ~isempty(BL.FFImageData)
        % apply corrections
        UpdateWithNewCorrection(hObject, handles);
    else
        % inform user that there is no flat-field image and how to add one
        msgbox(sprintf(['No flat-field image available for the experimental setup %s.\n', ...
            'Use File - Beamtime/Experimental Setup in the main GIDVis window to add one.'], ...
            BL.name), 'modal');
        % set the value of the checkbox to 0 (i.e. uncheck it)
        set(hObject, 'Value', 0);
    end
end


% --------------------------------------------------------------------
function Menu_ShowCorrectionImage_Callback(hObject, eventdata, handles)
% hObject    handle to Menu_ShowCorrectionImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% store the current object of the context menu's parent in the context
% menu's user data. The current object is the UIControl which the right
% click comes from. This will then be evaluated in the
% MenuItem_ShowCorrectionImage_Callback to show the image of the right
% correction factor
set(hObject, 'UserData', get(get(hObject, 'Parent'), 'CurrentObject'));

% --------------------------------------------------------------------
function MenuItem_ShowCorrectionImage_Callback(hObject, eventdata, handles)
% hObject    handle to MenuItem_ShowCorrectionImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% get handles structure of the main GIDVis window
GD = guidata(handles.MainAxes);
% get beamline
BL = LoadFileModule('GetBeamline', GD.LoadFileModule);

RegridResolution = [str2double(get(handles.TB_RegridResolution1, 'String')), ...
    str2double(get(handles.TB_RegridResolution2, 'String'))];

% get omega, chi, sample length and sample width
omega = str2double(get(handles.TB_omega, 'String'));
chi = str2double(get(handles.TB_chi, 'String'));

UIC = get(get(get(hObject, 'Parent'), 'UserData'), 'Tag');

if ~strcmp(UIC, 'CB_FlatFieldCorrection')
    f = figure('Visible', 'off');
    ax = axes('Parent', f);
    h = imagesc(NaN, 'Parent', ax);
end

switch UIC
    case 'CB_LorentzCorrection'
        CorrF = CorrectionFactors(BL, omega, chi);
        title(ax, 'Lorentz Correction')
    case 'CB_PolarizationCorrection'
        [~, CorrF] = CorrectionFactors(BL, omega, chi);
        title(ax, 'Polarization Correction');
    case 'CB_SolidAngleCorrection'
        [~, ~, CorrF] = CorrectionFactors(BL, omega, chi);
        title(ax, 'Solid Angle Correction')
    case 'CB_PixelDistanceCorrection'
        [~, ~, ~, CorrF] = CorrectionFactors(BL, omega, chi);
        title(ax, 'Pixel Distance Correction')
    case 'CB_DetectorEfficiencyCorrection'
        [~, ~, ~, ~, CorrF] = CorrectionFactors(BL, omega, chi);
        title(ax, 'Detector Efficiency Correction')
    case 'CB_FlatFieldCorrection'
        CorrF = BL.FFImageData;
        if isempty(CorrF)
            msgbox('No flat field correction image available for this experimental set up.', 'modal')
            return;
        end
        f = figure;
        ax = axes('Parent', f);
        imagesc(ax, CorrF);
        title(ax, 'Flat Field Image')
        xlabel(ax, 'Pixel')
        ylabel(ax, 'Pixel')
end

if ~strcmp(UIC, 'CB_FlatFieldCorrection')
    if isempty(CorrF)
        msgbox('Could not calculate the required correction factor. Make sure all necessary informations are entered in the Beamline Setup Module.');
        close(f);
        return;
    end
    [qxy, qz, Int] = CalculateQRegridded(BL, omega, chi, CorrF, RegridResolution);
    set(h, 'XData', qxy, 'YData', qz, 'CData', Int);
    caxis(ax, getColorBarLimits(Int(:)));
    xlim(ax, qxy)
    ylim(ax, qz)
    set(gca, 'YDir', 'normal')
    xlabel(ax, 'q_{xy}')
    ylabel(ax, 'q_z')
    colorbar(ax)
    set(f, 'Visible', 'on')
end


% --- Executes on button press in Btn_Imcontrast.
function Btn_Imcontrast_Callback(hObject, eventdata, handles)
% hObject    handle to Btn_Imcontrast (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% check if a map is already loaded, otherwise user cannot adapt the color
% limits interactively
h = findobj(handles.MainAxes, 'Tag', 'MainImage');
if isempty(h)
    msgbox('Please select a data file first.', 'modal');
    return;
end
h = Myimcontrast(handles.MainAxes);
set(h, 'CloseRequestFcn', @(src, evt) Myimcontrast_CloseRequestFcn(h, handles));


function Myimcontrast_CloseRequestFcn(h, handles)
CL = caxis(handles.MainAxes);
set(handles.TB_ColorLimMin, 'String', num2str(CL(1)));
set(handles.TB_ColorLimMax, 'String', num2str(CL(2)));
delete(h);
